.class public final Lcom/android/settings_ex/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings_ex/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final CheckBox01:I = 0x7f0b01d0

.field public static final FileOperationProgressFilename:I = 0x7f0b016f

.field public static final FileOperationProgressMessage:I = 0x7f0b016e

.field public static final FileOperationProgressPerFilePercentage:I = 0x7f0b0171

.field public static final FileOperationProgressPerFileProgressBar:I = 0x7f0b0170

.field public static final FileOperationProgressTitle:I = 0x7f0b016d

.field public static final FileOperationProgressTotalNumber:I = 0x7f0b0173

.field public static final FileOperationProgressTotalPercentage:I = 0x7f0b0174

.field public static final FileOperationProgressTotalProgressBar:I = 0x7f0b0172

.field public static final IndexedTextView:I = 0x7f0b03ed

.field public static final LinearLayout_bt:I = 0x7f0b004d

.field public static final ListLayout:I = 0x7f0b03e7

.field public static final ScrollView01:I = 0x7f0b0020

.field public static final StartBackupButton:I = 0x7f0b0039

.field public static final StartRestoreButton:I = 0x7f0b0349

.field public static final TextView01:I = 0x7f0b01cd

.field public static final TextView02:I = 0x7f0b01ce

.field public static final TextView03:I = 0x7f0b01cf

.field public static final about_settings:I = 0x7f0b0517

.field public static final accessibility_settings:I = 0x7f0b0500

.field public static final accessibilityfragment:I = 0x7f0b0009

.field public static final account:I = 0x7f0b01bd

.field public static final account_add:I = 0x7f0b04d9

.field public static final account_management:I = 0x7f0b0530

.field public static final account_settings:I = 0x7f0b04d7

.field public static final account_summary:I = 0x7f0b001a

.field public static final account_title:I = 0x7f0b0019

.field public static final accounts:I = 0x7f0b01fc

.field public static final accounts_label:I = 0x7f0b01fb

.field public static final action:I = 0x7f0b030c

.field public static final action_button:I = 0x7f0b0103

.field public static final action_delete:I = 0x7f0b0546

.field public static final action_select_all:I = 0x7f0b0547

.field public static final activate_this_device:I = 0x7f0b0515

.field public static final active_layout:I = 0x7f0b00f9

.field public static final add_menu_item:I = 0x7f0b0415

.field public static final add_msg:I = 0x7f0b00fe

.field public static final add_msg_expander:I = 0x7f0b00fd

.field public static final admin:I = 0x7f0b0182

.field public static final admin_description:I = 0x7f0b00fc

.field public static final admin_icon:I = 0x7f0b00fa

.field public static final admin_name:I = 0x7f0b00fb

.field public static final admin_policies:I = 0x7f0b0100

.field public static final admin_warning:I = 0x7f0b00ff

.field public static final advanced_fields:I = 0x7f0b0427

.field public static final airplane_mode:I = 0x7f0b04e2

.field public static final airplane_mode_info:I = 0x7f0b001f

.field public static final alarm_line:I = 0x7f0b02c7

.field public static final alarm_mute_button:I = 0x7f0b02c9

.field public static final alarm_section:I = 0x7f0b02c8

.field public static final alarm_volume_seekbar:I = 0x7f0b02ca

.field public static final all_details:I = 0x7f0b002b

.field public static final allsahrecast_help_image:I = 0x7f0b03ec

.field public static final allshare_cast_settings:I = 0x7f0b04ed

.field public static final allshare_welcome_check:I = 0x7f0b0022

.field public static final allshare_welcome_check_text:I = 0x7f0b0024

.field public static final allshare_welcome_check_view:I = 0x7f0b0023

.field public static final allshare_welcome_text:I = 0x7f0b0021

.field public static final allsharecast_help_scroll:I = 0x7f0b0402

.field public static final allsharecast_is_readonly:I = 0x7f0b0401

.field public static final alphanumeric_pin:I = 0x7f0b0057

.field public static final also_erases_external:I = 0x7f0b01f9

.field public static final also_erases_external_encrypted:I = 0x7f0b01fa

.field public static final android_beam_explained:I = 0x7f0b0025

.field public static final android_beam_image:I = 0x7f0b0026

.field public static final animation_view:I = 0x7f0b0066

.field public static final anonymous:I = 0x7f0b0456

.field public static final apn_radiobutton:I = 0x1010001

.field public static final appGauge:I = 0x7f0b02ed

.field public static final appIcon:I = 0x7f0b02eb

.field public static final app_background:I = 0x7f0b00d4

.field public static final app_detail:I = 0x7f0b00d0

.field public static final app_disabled:I = 0x7f0b01f7

.field public static final app_foreground:I = 0x7f0b00d3

.field public static final app_icon:I = 0x7f0b00d1

.field public static final app_name:I = 0x7f0b01e5

.field public static final app_on_sdcard:I = 0x7f0b01f5

.field public static final app_pie_chart:I = 0x7f0b00d5

.field public static final app_settings:I = 0x7f0b00d6

.field public static final app_size:I = 0x7f0b01f6

.field public static final app_snippet:I = 0x7f0b0197

.field public static final app_switches:I = 0x7f0b00d7

.field public static final app_titles:I = 0x7f0b00d2

.field public static final application_settings:I = 0x7f0b050f

.field public static final application_size_prefix:I = 0x7f0b019e

.field public static final application_size_text:I = 0x7f0b019f

.field public static final apply:I = 0x7f0b00bc

.field public static final asensor:I = 0x7f0b0027

.field public static final asensor_button:I = 0x7f0b002a

.field public static final asensor_text:I = 0x7f0b0028

.field public static final asensor_view:I = 0x7f0b0029

.field public static final ask_compatibility_cb:I = 0x7f0b01b1

.field public static final attempts:I = 0x7f0b032c

.field public static final audio_output_mode:I = 0x7f0b0109

.field public static final audio_output_mode_scroll:I = 0x7f0b0108

.field public static final auto_date_time_checkbox:I = 0x7f0b00e3

.field public static final auto_date_time_layout:I = 0x7f0b00e1

.field public static final auto_date_time_summary:I = 0x7f0b00df

.field public static final auto_date_time_title:I = 0x7f0b00e2

.field public static final auto_detail_custom_layout:I = 0x7f0b02a5

.field public static final auto_detail_seekbar:I = 0x7f0b02a3

.field public static final auto_detail_seekbar_layout:I = 0x7f0b02a2

.field public static final auto_detail_text:I = 0x7f0b02b0

.field public static final auto_launch:I = 0x7f0b01ae

.field public static final auto_launch_title:I = 0x7f0b01ad

.field public static final auto_mode:I = 0x7f0b020a

.field public static final auto_timezone_checkbox:I = 0x7f0b00e7

.field public static final auto_timezone_layout:I = 0x7f0b00e4

.field public static final auto_timezone_summary:I = 0x7f0b00e6

.field public static final auto_timezone_title:I = 0x7f0b00e5

.field public static final autoaccept_text:I = 0x7f0b0049

.field public static final automatic_mode:I = 0x7f0b029e

.field public static final automatic_noti:I = 0x7f0b029f

.field public static final available:I = 0x7f0b03f9

.field public static final available_size:I = 0x7f0b0342

.field public static final available_size_comment:I = 0x7f0b0341

.field public static final back:I = 0x7f0b0491

.field public static final background:I = 0x7f0b026e

.field public static final backgroundText:I = 0x7f0b034e

.field public static final backspace:I = 0x7f0b0119

.field public static final backup_comment:I = 0x7f0b018c

.field public static final backup_folders_of_external_device:I = 0x7f0b0343

.field public static final backup_header:I = 0x7f0b018b

.field public static final backup_list:I = 0x7f0b0344

.field public static final backup_private_data:I = 0x7f0b0037

.field public static final backup_pw_cancel_button:I = 0x7f0b036a

.field public static final backup_pw_set_button:I = 0x7f0b036b

.field public static final backup_section:I = 0x7f0b04da

.field public static final backup_shared_data:I = 0x7f0b002f

.field public static final backup_size_title:I = 0x7f0b002c

.field public static final backup_start_btn:I = 0x7f0b018d

.field public static final band:I = 0x7f0b003d

.field public static final banner:I = 0x7f0b03a4

.field public static final battery_history_chart:I = 0x1010002

.field public static final battery_settings:I = 0x7f0b0510

.field public static final block_settings:I = 0x7f0b04fb

.field public static final bluetooth_device_picker_fragment:I = 0x7f0b004b

.field public static final bluetooth_map_remember_choice:I = 0x7f0b004c

.field public static final bluetooth_pb_remember_choice:I = 0x7f0b0054

.field public static final bluetooth_sap_remember_choice:I = 0x7f0b0059

.field public static final bluetooth_scan_dialog:I = 0x7f0b005b

.field public static final bluetooth_scan_dialog_button:I = 0x7f0b0357

.field public static final bluetooth_scan_dialog_fragment_layout:I = 0x7f0b005a

.field public static final bluetooth_settings:I = 0x7f0b04e1

.field public static final bottomButtonPanel:I = 0x7f0b0490

.field public static final bottom_btn:I = 0x7f0b04a5

.field public static final bottom_buttons:I = 0x7f0b04b9

.field public static final bottom_divider:I = 0x7f0b00b0

.field public static final bottom_icon:I = 0x7f0b0012

.field public static final bottom_padding:I = 0x7f0b04b8

.field public static final bottom_padding_layout:I = 0x7f0b04b7

.field public static final bright_adjust_checkbox:I = 0x7f0b0228

.field public static final bright_adjustment_header_summary:I = 0x7f0b0227

.field public static final bright_adjustment_header_title:I = 0x7f0b0226

.field public static final brightness_level:I = 0x7f0b02a1

.field public static final bssid:I = 0x7f0b04c4

.field public static final btnSelectBackGroundForColor:I = 0x7f0b0220

.field public static final btnSelectTextForColor:I = 0x7f0b021f

.field public static final btn_account_and_sync:I = 0x7f0b0164

.field public static final btn_alignCenter:I = 0x7f0b01df

.field public static final btn_alignLeft:I = 0x7f0b01e0

.field public static final btn_alignRight:I = 0x7f0b01de

.field public static final btn_battery:I = 0x7f0b015d

.field public static final btn_bluetooth:I = 0x7f0b0081

.field public static final btn_brightness:I = 0x7f0b0161

.field public static final btn_data:I = 0x7f0b0085

.field public static final btn_data_usage:I = 0x7f0b015a

.field public static final btn_directcall:I = 0x7f0b03b2

.field public static final btn_font_size:I = 0x7f0b0157

.field public static final btn_gps:I = 0x7f0b0089

.field public static final btn_powersaving:I = 0x7f0b03c6

.field public static final btn_ringtone:I = 0x7f0b014d

.field public static final btn_sbeam:I = 0x7f0b03be

.field public static final btn_settings:I = 0x7f0b0167

.field public static final btn_smartalert:I = 0x7f0b03c2

.field public static final btn_smartstay:I = 0x7f0b03b6

.field public static final btn_sync:I = 0x7f0b008d

.field public static final btn_turnoff:I = 0x7f0b007a

.field public static final btn_voicecmd:I = 0x7f0b03ba

.field public static final btn_volume:I = 0x7f0b0153

.field public static final btn_wallpaper:I = 0x7f0b0150

.field public static final btn_wifi:I = 0x7f0b007d

.field public static final bua_plus:I = 0x7f0b04d8

.field public static final button:I = 0x7f0b01c3

.field public static final buttonContainer:I = 0x7f0b006d

.field public static final buttonPanel:I = 0x7f0b0101

.field public static final button_bar:I = 0x7f0b012d

.field public static final button_cancel:I = 0x7f0b01d8

.field public static final button_edit:I = 0x7f0b0302

.field public static final button_img:I = 0x7f0b0306

.field public static final button_layout:I = 0x7f0b01d7

.field public static final button_ok:I = 0x7f0b01d9

.field public static final button_sellect_all:I = 0x7f0b012f

.field public static final button_text:I = 0x7f0b0358

.field public static final buttons_spacer_left:I = 0x7f0b0290

.field public static final bytes:I = 0x7f0b00c3

.field public static final ca_cert:I = 0x7f0b0453

.field public static final cac_bt_connect_view:I = 0x7f0b0050

.field public static final cache_header:I = 0x7f0b01a9

.field public static final cache_panel:I = 0x7f0b01a8

.field public static final cache_size:I = 0x7f0b01aa

.field public static final cache_size_text:I = 0x7f0b01ab

.field public static final call:I = 0x7f0b032a

.field public static final call_settings:I = 0x7f0b04f7

.field public static final cancel:I = 0x7f0b0124

.field public static final cancel_button:I = 0x7f0b0051

.field public static final carrier:I = 0x7f0b0116

.field public static final category_name:I = 0x7f0b022b

.field public static final cellinfo:I = 0x7f0b0323

.field public static final center_button:I = 0x7f0b000e

.field public static final center_icon:I = 0x7f0b0011

.field public static final center_img:I = 0x7f0b000f

.field public static final center_text:I = 0x7f0b0010

.field public static final cert_remove_button:I = 0x7f0b0398

.field public static final cfi:I = 0x7f0b0329

.field public static final chart:I = 0x7f0b00c4

.field public static final check:I = 0x7f0b003c

.field public static final checkBox:I = 0x7f0b00c0

.field public static final check_reminder_MPCS_warning:I = 0x7f0b0435

.field public static final checkbox:I = 0x7f0b0106

.field public static final checkbox1:I = 0x7f0b0048

.field public static final checkbox_background:I = 0x7f0b02b9

.field public static final checkbox_cpu:I = 0x7f0b02b7

.field public static final checkbox_display:I = 0x7f0b02b8

.field public static final checkbox_haptic:I = 0x7f0b02ba

.field public static final checkbox_private:I = 0x7f0b0038

.field public static final checkbox_selectall:I = 0x7f0b0482

.field public static final checkbox_shared:I = 0x7f0b0030

.field public static final checkbox_wifi_ap_channel_2g:I = 0x7f0b0428

.field public static final checkbox_wifi_ap_channel_5g:I = 0x7f0b0429

.field public static final ckbDate:I = 0x7f0b021d

.field public static final ckbTime:I = 0x7f0b021a

.field public static final clear:I = 0x7f0b030d

.field public static final clear_activities_button:I = 0x7f0b01af

.field public static final clear_cache_button:I = 0x7f0b01ac

.field public static final client_layout:I = 0x7f0b051e

.field public static final client_title:I = 0x7f0b051d

.field public static final clock_colon:I = 0x7f0b0216

.field public static final clock_hour01:I = 0x7f0b0214

.field public static final clock_hour02:I = 0x7f0b0215

.field public static final clock_minute01:I = 0x7f0b0217

.field public static final clock_minute02:I = 0x7f0b0218

.field public static final clock_myprofile_body_text:I = 0x7f0b01d6

.field public static final close_button:I = 0x7f0b00de

.field public static final color_bar:I = 0x7f0b034c

.field public static final comp_description:I = 0x7f0b0351

.field public static final con_type:I = 0x7f0b013f

.field public static final confcancle:I = 0x7f0b051b

.field public static final config_list:I = 0x7f0b0445

.field public static final confirm_new_backup_pw:I = 0x7f0b0369

.field public static final confok:I = 0x7f0b051c

.field public static final connecting_status:I = 0x7f0b04b6

.field public static final connecting_status_layout:I = 0x7f0b04b5

.field public static final contact:I = 0x7f0b0378

.field public static final contact_presence_layout:I = 0x7f0b01d3

.field public static final content:I = 0x7f0b0078

.field public static final content_image_area:I = 0x7f0b0073

.field public static final content_imageview:I = 0x7f0b0074

.field public static final content_padding:I = 0x7f0b04b1

.field public static final content_playbtn:I = 0x7f0b0075

.field public static final content_textview:I = 0x7f0b0076

.field public static final contents:I = 0x7f0b029a

.field public static final control_buttons_panel:I = 0x7f0b0198

.field public static final controls:I = 0x7f0b0293

.field public static final controls_title:I = 0x7f0b0294

.field public static final create:I = 0x7f0b0377

.field public static final current_backup_pw:I = 0x7f0b0365

.field public static final current_location:I = 0x7f0b035f

.field public static final cursor_panel_position:I = 0x7f0b022e

.field public static final custom_dialog_warning_textView:I = 0x7f0b00bf

.field public static final custombar0:I = 0x7f0b02a6

.field public static final custombar1:I = 0x7f0b02a7

.field public static final custombar2:I = 0x7f0b02a8

.field public static final custombar3:I = 0x7f0b02a9

.field public static final custombar4:I = 0x7f0b02aa

.field public static final custombar5:I = 0x7f0b02ab

.field public static final custombar6:I = 0x7f0b02ac

.field public static final custombar7:I = 0x7f0b02ad

.field public static final custombar8:I = 0x7f0b02ae

.field public static final custombar9:I = 0x7f0b02af

.field public static final customizable_key:I = 0x7f0b052b

.field public static final customizedkey_settings:I = 0x7f0b052f

.field public static final cycle_day:I = 0x7f0b00cd

.field public static final cycles:I = 0x7f0b00ce

.field public static final cycles_spinner:I = 0x7f0b00cf

.field public static final data:I = 0x7f0b01bc

.field public static final data_buttons_panel:I = 0x7f0b01a7

.field public static final data_size_prefix:I = 0x7f0b01a3

.field public static final data_size_text:I = 0x7f0b01a4

.field public static final data_usage_menu_auto_sync:I = 0x7f0b053e

.field public static final data_usage_menu_help:I = 0x7f0b0542

.field public static final data_usage_menu_metered:I = 0x7f0b0541

.field public static final data_usage_menu_restrict_background:I = 0x7f0b053c

.field public static final data_usage_menu_roaming:I = 0x7f0b053b

.field public static final data_usage_menu_show_ethernet:I = 0x7f0b0540

.field public static final data_usage_menu_show_wifi:I = 0x7f0b053f

.field public static final data_usage_menu_split_4g:I = 0x7f0b053d

.field public static final data_usage_settings:I = 0x7f0b04e3

.field public static final date:I = 0x7f0b00a9

.field public static final date_picker:I = 0x7f0b00f4

.field public static final date_picker_title:I = 0x7f0b00f3

.field public static final date_time_auto:I = 0x7f0b00f1

.field public static final date_time_auto_button:I = 0x7f0b00f2

.field public static final date_time_img:I = 0x7f0b00f8

.field public static final date_time_settings:I = 0x7f0b0513

.field public static final date_time_settings_fragment:I = 0x7f0b00ee

.field public static final dbm:I = 0x7f0b0320

.field public static final defaultView:I = 0x7f0b030e

.field public static final delete_admin:I = 0x7f0b0188

.field public static final delete_area:I = 0x7f0b01ec

.field public static final desc:I = 0x7f0b00b9

.field public static final description:I = 0x7f0b00c1

.field public static final description_text:I = 0x7f0b0356

.field public static final detail_series:I = 0x7f0b00c7

.field public static final details:I = 0x7f0b0292

.field public static final development_settings:I = 0x7f0b0514

.field public static final development_settings_orange:I = 0x7f0b053a

.field public static final deviceDetails:I = 0x7f0b0298

.field public static final device_address:I = 0x7f0b0498

.field public static final device_list_layout:I = 0x7f0b03fc

.field public static final device_list_subtitle:I = 0x7f0b03f8

.field public static final device_manager:I = 0x7f0b050e

.field public static final device_name:I = 0x7f0b043c

.field public static final device_name_edit_text_popup:I = 0x7f0b0107

.field public static final device_name_title:I = 0x7f0b0495

.field public static final device_section:I = 0x7f0b052a

.field public static final devider:I = 0x7f0b0305

.field public static final dhcp_radio:I = 0x7f0b0140

.field public static final dialog_hotspot_button_continue:I = 0x7f0b0438

.field public static final disconnects:I = 0x7f0b032e

.field public static final display_settings:I = 0x7f0b04f3

.field public static final divider:I = 0x7f0b0136

.field public static final divider_view:I = 0x7f0b020b

.field public static final dmr_settings:I = 0x7f0b0529

.field public static final dns1:I = 0x7f0b0465

.field public static final dns2:I = 0x7f0b0466

.field public static final dnsCheckState:I = 0x7f0b033c

.field public static final dns_check_toggle:I = 0x7f0b033b

.field public static final dns_servers:I = 0x7f0b03da

.field public static final dns_text:I = 0x7f0b0147

.field public static final do_not_show:I = 0x7f0b01c2

.field public static final do_not_show_again:I = 0x7f0b010a

.field public static final do_not_show_again_for_scroll:I = 0x7f0b038d

.field public static final do_not_show_this_again:I = 0x7f0b0485

.field public static final dock_audio_media_enable_cb:I = 0x7f0b012a

.field public static final dock_settings:I = 0x7f0b04ff

.field public static final docomocloud_settings:I = 0x7f0b04db

.field public static final docomoservice_settings:I = 0x7f0b04f0

.field public static final dont_disturb:I = 0x7f0b048d

.field public static final dontshow:I = 0x7f0b0225

.field public static final dontshowtext:I = 0x7f0b0224

.field public static final dormant_mode:I = 0x7f0b04f8

.field public static final driving_mode:I = 0x7f0b04f9

.field public static final dualSlot_settings:I = 0x7f0b04e4

.field public static final dualmode_settings:I = 0x7f0b0536

.field public static final dummy_for_left_edge:I = 0x7f0b04b0

.field public static final eap:I = 0x7f0b0450

.field public static final eap_not_supported:I = 0x7f0b0446

.field public static final eap_not_supported_for_add_network:I = 0x7f0b0447

.field public static final easy_mode_settings:I = 0x7f0b04f6

.field public static final easymode_help_layout:I = 0x7f0b0137

.field public static final ed_EditMyProfile:I = 0x7f0b035a

.field public static final edit_menu_item:I = 0x7f0b0301

.field public static final edit_name:I = 0x7f0b020c

.field public static final edit_passwd:I = 0x7f0b0519

.field public static final edit_pattern_name:I = 0x7f0b0239

.field public static final editor:I = 0x7f0b03ca

.field public static final edittext:I = 0x7f0b010b

.field public static final eight:I = 0x7f0b0122

.field public static final email:I = 0x7f0b0185

.field public static final emergencyCall:I = 0x7f0b0053

.field public static final emergencyCallButton:I = 0x7f0b00ac

.field public static final emptyTemplateIcon:I = 0x7f0b0411

.field public static final emptyTemplateIcon_land:I = 0x7f0b040f

.field public static final emptyTemplateIcon_port:I = 0x7f0b040b

.field public static final empty_list_templateView:I = 0x7f0b0412

.field public static final empty_list_textView:I = 0x7f0b0410

.field public static final empty_message:I = 0x7f0b03eb

.field public static final empty_screen:I = 0x7f0b0131

.field public static final empty_screen_landscape:I = 0x7f0b040c

.field public static final empty_screen_portrait:I = 0x7f0b0408

.field public static final empty_template_land:I = 0x7f0b040d

.field public static final empty_template_land2:I = 0x7f0b040e

.field public static final empty_template_port:I = 0x7f0b0409

.field public static final empty_template_port2:I = 0x7f0b040a

.field public static final enable_compatibility_cb:I = 0x7f0b01b2

.field public static final enable_disable:I = 0x7f0b00bb

.field public static final enc_messages:I = 0x7f0b00ba

.field public static final encroid:I = 0x7f0b00ae

.field public static final enterprise_wrapper:I = 0x7f0b0142

.field public static final erase_external:I = 0x7f0b01ff

.field public static final erase_external_container:I = 0x7f0b01fe

.field public static final erase_external_option_text:I = 0x7f0b01fd

.field public static final erase_external_storage_description_textview:I = 0x7f0b0201

.field public static final erase_external_storage_textview:I = 0x7f0b0200

.field public static final error:I = 0x7f0b009b

.field public static final eth_con_type:I = 0x7f0b013e

.field public static final eth_dev_list_text:I = 0x7f0b013c

.field public static final eth_dev_spinner:I = 0x7f0b013d

.field public static final eth_dns_edit:I = 0x7f0b0148

.field public static final eth_gw_edit:I = 0x7f0b014a

.field public static final ethernet_settings:I = 0x7f0b0525

.field public static final exclusionlist:I = 0x7f0b030b

.field public static final execute_decrypt:I = 0x7f0b009e

.field public static final execute_encrypt:I = 0x7f0b00ab

.field public static final execute_master_clear:I = 0x7f0b0203

.field public static final execute_media_format:I = 0x7f0b0204

.field public static final execute_media_format_sd:I = 0x7f0b0205

.field public static final exit:I = 0x7f0b0493

.field public static final external_code_size_prefix:I = 0x7f0b01a0

.field public static final external_code_size_text:I = 0x7f0b01a1

.field public static final external_data_size_prefix:I = 0x7f0b01a5

.field public static final external_data_size_text:I = 0x7f0b01a6

.field public static final factory_reset:I = 0x7f0b00af

.field public static final fast_encryption:I = 0x7f0b00aa

.field public static final fast_text:I = 0x7f0b0383

.field public static final feature_settings_fragment:I = 0x7f0b016b

.field public static final fields:I = 0x7f0b0424

.field public static final fileOperationDialog:I = 0x7f0b016c

.field public static final finger_air_view_settings:I = 0x7f0b0506

.field public static final finish_button:I = 0x7f0b0017

.field public static final finish_button_area:I = 0x7f0b0016

.field public static final five:I = 0x7f0b011f

.field public static final flash_noti:I = 0x7f0b02b2

.field public static final folder:I = 0x7f0b034a

.field public static final fontSize:I = 0x7f0b0127

.field public static final font_noti:I = 0x7f0b02b4

.field public static final footerLeftButton:I = 0x7f0b006b

.field public static final footerRightButton:I = 0x7f0b006c

.field public static final footerText:I = 0x7f0b006a

.field public static final footer_layout:I = 0x7f0b03e5

.field public static final foregroundText:I = 0x7f0b034d

.field public static final four:I = 0x7f0b011e

.field public static final freeStorageText:I = 0x7f0b01f1

.field public static final gateway:I = 0x7f0b0463

.field public static final gesture_screen_crop:I = 0x7f0b0243

.field public static final gesture_screen_crop_play:I = 0x7f0b0244

.field public static final gesture_text_selection:I = 0x7f0b0246

.field public static final gesture_text_selection_play:I = 0x7f0b0247

.field public static final gestures_capture_screen_title:I = 0x7f0b0241

.field public static final gestures_cut_out_image_title:I = 0x7f0b0242

.field public static final gestures_go_back_title:I = 0x7f0b023b

.field public static final gestures_open_menu_title:I = 0x7f0b023c

.field public static final gestures_open_quick_command_img:I = 0x7f0b0240

.field public static final gestures_open_quick_command_msg:I = 0x7f0b023f

.field public static final gestures_open_quick_command_title:I = 0x7f0b023e

.field public static final gestures_open_quick_note_title:I = 0x7f0b023d

.field public static final gestures_select_text_title:I = 0x7f0b0245

.field public static final global_roaming_settings:I = 0x7f0b04e5

.field public static final gprs:I = 0x7f0b0326

.field public static final gps_image:I = 0x7f0b02e8

.field public static final gradientColorView:I = 0x7f0b01e2

.field public static final grid:I = 0x7f0b00c5

.field public static final gsensor:I = 0x7f0b0177

.field public static final gsensor_button:I = 0x7f0b017a

.field public static final gsensor_text:I = 0x7f0b0178

.field public static final gsensor_view:I = 0x7f0b0179

.field public static final gsm:I = 0x7f0b0325

.field public static final guide_bubble_summary:I = 0x7f0b0006

.field public static final guide_content_textview:I = 0x7f0b01ea

.field public static final guide_fragment_container:I = 0x7f0b0007

.field public static final guide_hand_pointer:I = 0x7f0b0003

.field public static final guide_punch:I = 0x7f0b0004

.field public static final guide_root:I = 0x7f0b0005

.field public static final guide_text:I = 0x7f0b020e

.field public static final guide_tip_title_button:I = 0x7f0b0002

.field public static final guide_view_container:I = 0x7f0b017b

.field public static final gw_text:I = 0x7f0b0149

.field public static final haptic_feedback_vibration_seekbar:I = 0x7f0b02e2

.field public static final haptic_feedback_vibration_seekbar_text:I = 0x7f0b02e1

.field public static final hdmi_settings:I = 0x7f0b0538

.field public static final headerText:I = 0x7f0b004f

.field public static final header_admin:I = 0x7f0b0181

.field public static final header_guest:I = 0x7f0b0183

.field public static final header_nearby_settings:I = 0x7f0b0528

.field public static final heading:I = 0x7f0b048f

.field public static final health:I = 0x7f0b0042

.field public static final help:I = 0x7f0b049f

.field public static final help_container:I = 0x7f0b038b

.field public static final help_content:I = 0x7f0b0175

.field public static final help_edit_bubble_summary:I = 0x7f0b017e

.field public static final help_image_view:I = 0x7f0b0386

.field public static final help_img:I = 0x7f0b023a

.field public static final help_instruction:I = 0x7f0b0139

.field public static final help_item:I = 0x7f0b0070

.field public static final help_message_view:I = 0x7f0b0385

.field public static final help_ok_button:I = 0x7f0b0252

.field public static final help_tap_bubble_picker:I = 0x7f0b017d

.field public static final help_tap_bubble_summary:I = 0x7f0b017c

.field public static final help_text:I = 0x7f0b0233

.field public static final help_try_btn:I = 0x7f0b0387

.field public static final hidden_ssid:I = 0x7f0b04c5

.field public static final hide_ssid:I = 0x7f0b0421

.field public static final hint:I = 0x7f0b009a

.field public static final home_button:I = 0x7f0b050a

.field public static final home_icon:I = 0x7f0b0508

.field public static final home_label:I = 0x7f0b0509

.field public static final home_settings:I = 0x7f0b04f1

.field public static final home_settings_category:I = 0x7f0b0523

.field public static final home_summary:I = 0x7f0b0180

.field public static final homesettingsfragment:I = 0x7f0b017f

.field public static final homesync_account_management_settings:I = 0x7f0b0531

.field public static final homesync_backup_and_restore_settings:I = 0x7f0b0532

.field public static final horizontal:I = 0x7f0b0000

.field public static final hostname:I = 0x7f0b0309

.field public static final hover_info_tip:I = 0x7f0b024b

.field public static final hover_info_tip_play:I = 0x7f0b024c

.field public static final hover_zoom_in:I = 0x7f0b024e

.field public static final hover_zoom_in_play:I = 0x7f0b024f

.field public static final hovering_button:I = 0x7f0b0248

.field public static final hovering_icon_label_title:I = 0x7f0b0250

.field public static final hovering_info_preview_title:I = 0x7f0b024a

.field public static final hovering_list_scrolling_title:I = 0x7f0b0251

.field public static final hovering_pointer_title:I = 0x7f0b0249

.field public static final hovering_zoom_in_title:I = 0x7f0b024d

.field public static final hs20_picker_dialog_btn_panel:I = 0x7f0b0195

.field public static final hs20_picker_dialog_cancel:I = 0x7f0b0193

.field public static final hs20_picker_dialog_devider:I = 0x7f0b0192

.field public static final hs20_picker_dialog_fragment:I = 0x7f0b0191

.field public static final hs20_picker_dialog_scan:I = 0x7f0b0194

.field public static final httpClientTest:I = 0x7f0b031f

.field public static final icon:I = 0x7f0b005c

.field public static final icon_add:I = 0x7f0b0416

.field public static final icon_addition:I = 0x7f0b01e9

.field public static final icon_trash:I = 0x7f0b0414

.field public static final id_date:I = 0x7f0b034b

.field public static final id_name:I = 0x7f0b003a

.field public static final id_size:I = 0x7f0b003b

.field public static final identity:I = 0x7f0b0455

.field public static final imageLayout:I = 0x7f0b03e8

.field public static final image_size:I = 0x7f0b0189

.field public static final image_view:I = 0x7f0b0354

.field public static final imei:I = 0x7f0b0319

.field public static final img:I = 0x7f0b010c

.field public static final img_account_and_sync:I = 0x7f0b0165

.field public static final img_ani:I = 0x7f0b0480

.field public static final img_battery:I = 0x7f0b015e

.field public static final img_bluetooth:I = 0x7f0b0082

.field public static final img_brightness:I = 0x7f0b0162

.field public static final img_data:I = 0x7f0b0086

.field public static final img_data_usage:I = 0x7f0b015b

.field public static final img_directcall:I = 0x7f0b03b3

.field public static final img_font_size:I = 0x7f0b0158

.field public static final img_gps:I = 0x7f0b008a

.field public static final img_powersaving:I = 0x7f0b03c7

.field public static final img_ringtone:I = 0x7f0b014e

.field public static final img_sbeam:I = 0x7f0b03bf

.field public static final img_settings:I = 0x7f0b0168

.field public static final img_smartalert:I = 0x7f0b03c3

.field public static final img_smartstay:I = 0x7f0b03b7

.field public static final img_sync:I = 0x7f0b008e

.field public static final img_turnoff:I = 0x7f0b007b

.field public static final img_voicecmd:I = 0x7f0b03bb

.field public static final img_volume:I = 0x7f0b0154

.field public static final img_wallpaper:I = 0x7f0b0151

.field public static final img_wifi:I = 0x7f0b007e

.field public static final ims_reg_required:I = 0x7f0b0334

.field public static final incoming_call_vibration_seekbar:I = 0x7f0b02df

.field public static final incoming_call_vibration_seekbar_text:I = 0x7f0b02de

.field public static final ind_bluetooth:I = 0x7f0b0084

.field public static final ind_brightness:I = 0x7f0b0403

.field public static final ind_data:I = 0x7f0b0088

.field public static final ind_directcall:I = 0x7f0b03b5

.field public static final ind_gps:I = 0x7f0b008c

.field public static final ind_powersaving:I = 0x7f0b03c9

.field public static final ind_sbeam:I = 0x7f0b03c1

.field public static final ind_smartalert:I = 0x7f0b03c5

.field public static final ind_smartstay:I = 0x7f0b03b9

.field public static final ind_sync:I = 0x7f0b0090

.field public static final ind_voicecmd:I = 0x7f0b03bd

.field public static final ind_wifi:I = 0x7f0b0080

.field public static final info:I = 0x7f0b041f

.field public static final info_msg:I = 0x7f0b04d1

.field public static final info_size:I = 0x7f0b01a2

.field public static final info_size_dots:I = 0x7f0b019c

.field public static final initiate_decrypt:I = 0x7f0b00a7

.field public static final initiate_encrypt:I = 0x7f0b00b8

.field public static final initiate_master_clear:I = 0x7f0b0202

.field public static final initiate_media_format:I = 0x7f0b0206

.field public static final initiate_media_format_sd:I = 0x7f0b0207

.field public static final ink_help:I = 0x7f0b0196

.field public static final input_and_control:I = 0x7f0b0501

.field public static final inputmethod_pref:I = 0x7f0b02e4

.field public static final inputmethod_settings:I = 0x7f0b02e5

.field public static final intent:I = 0x7f0b01bb

.field public static final international_roaming_setting:I = 0x7f0b04e7

.field public static final ip_fields:I = 0x7f0b045f

.field public static final ip_settings:I = 0x7f0b0460

.field public static final ipaddr:I = 0x7f0b04c6

.field public static final ipaddr_edit:I = 0x7f0b0144

.field public static final ipaddr_text:I = 0x7f0b0143

.field public static final ipaddress:I = 0x7f0b0462

.field public static final ipsec_ca_cert:I = 0x7f0b03d5

.field public static final ipsec_identifier:I = 0x7f0b03d0

.field public static final ipsec_peer:I = 0x7f0b03d4

.field public static final ipsec_psk:I = 0x7f0b03cf

.field public static final ipsec_secret:I = 0x7f0b03d1

.field public static final ipsec_server_cert:I = 0x7f0b03d6

.field public static final ipsec_user:I = 0x7f0b03d2

.field public static final ipsec_user_cert:I = 0x7f0b03d3

.field public static final is_register:I = 0x7f0b0187

.field public static final iwlan_add_network:I = 0x7f0b01c6

.field public static final iwlan_network_info:I = 0x7f0b01c5

.field public static final iwlan_pdg_address:I = 0x7f0b01c8

.field public static final iwlan_pdg_name:I = 0x7f0b01c7

.field public static final iwlan_skt_network_info:I = 0x7f0b01c4

.field public static final keyPad:I = 0x7f0b011a

.field public static final keyboard:I = 0x7f0b0062

.field public static final kies_via_wifi:I = 0x7f0b04ee

.field public static final kind_editors:I = 0x7f0b0379

.field public static final kind_header:I = 0x7f0b0376

.field public static final l2tp:I = 0x7f0b03cd

.field public static final l2tp_secret:I = 0x7f0b03ce

.field public static final l_anonymous:I = 0x7f0b046e

.field public static final l_ca_cert:I = 0x7f0b046b

.field public static final l_identity:I = 0x7f0b046d

.field public static final l_method:I = 0x7f0b0467

.field public static final l_phase1:I = 0x7f0b0468

.field public static final l_phase2:I = 0x7f0b046a

.field public static final l_user_cert:I = 0x7f0b046c

.field public static final label:I = 0x7f0b01cb

.field public static final language_settings:I = 0x7f0b0503

.field public static final lanunch_page_buddy:I = 0x7f0b0091

.field public static final lanunch_page_buddy_help_1:I = 0x7f0b0092

.field public static final lanunch_page_buddy_help_2:I = 0x7f0b0093

.field public static final lanunch_page_buddy_help_3:I = 0x7f0b0096

.field public static final lanunch_page_buddy_tip:I = 0x7f0b0094

.field public static final lanunch_page_buddy_tip_play:I = 0x7f0b0095

.field public static final launch_count:I = 0x7f0b03a2

.field public static final layout:I = 0x7f0b02ee

.field public static final layout_container:I = 0x7f0b035b

.field public static final layout_device_name:I = 0x7f0b0497

.field public static final layout_deviceinfo:I = 0x7f0b0494

.field public static final layout_root:I = 0x7f0b000b

.field public static final layout_selectall:I = 0x7f0b0481

.field public static final layout_weak_signal:I = 0x1010003

.field public static final learn_about_tilt_btn:I = 0x7f0b0067

.field public static final led_indicator_settings:I = 0x7f0b04f4

.field public static final left:I = 0x7f0b029b

.field public static final leftSpacer:I = 0x7f0b0102

.field public static final left_button:I = 0x7f0b0272

.field public static final level:I = 0x7f0b0040

.field public static final linBackground:I = 0x7f0b0210

.field public static final linCoverDateAndTimeCheckBoxes:I = 0x7f0b0211

.field public static final linCoverDateView:I = 0x7f0b021b

.field public static final linCoverTimeView:I = 0x7f0b0212

.field public static final link_message:I = 0x7f0b01c1

.field public static final link_speed:I = 0x7f0b04c9

.field public static final linked_contact_minus_imageView:I = 0x7f0b01d4

.field public static final linked_contacts_name_layout:I = 0x7f0b01d2

.field public static final list:I = 0x7f0b0363

.field public static final list_container:I = 0x7f0b01ed

.field public static final list_selectall:I = 0x7f0b0361

.field public static final listview_client:I = 0x7f0b051f

.field public static final listview_log:I = 0x7f0b0522

.field public static final loading_container:I = 0x7f0b01f2

.field public static final locale:I = 0x7f0b0176

.field public static final locale_picker_fragment:I = 0x7f0b01d5

.field public static final location:I = 0x7f0b0321

.field public static final location_consent:I = 0x7f0b0340

.field public static final location_edit_area:I = 0x7f0b035c

.field public static final location_settings:I = 0x7f0b050c

.field public static final location_textview:I = 0x7f0b035d

.field public static final lockPattern:I = 0x7f0b0069

.field public static final lock_screen_menu_settings:I = 0x7f0b04f2

.field public static final lockinfoText:I = 0x7f0b0117

.field public static final log_layout:I = 0x7f0b0521

.field public static final log_title:I = 0x7f0b0520

.field public static final login:I = 0x7f0b03dc

.field public static final lte_ram_dump:I = 0x7f0b0336

.field public static final mac_address:I = 0x7f0b0407

.field public static final mac_address_0:I = 0x7f0b043e

.field public static final mac_address_1:I = 0x7f0b043f

.field public static final mac_address_2:I = 0x7f0b0440

.field public static final mac_address_3:I = 0x7f0b0441

.field public static final mac_address_4:I = 0x7f0b0442

.field public static final mac_address_5:I = 0x7f0b0443

.field public static final mac_address_title:I = 0x7f0b0496

.field public static final macaddr:I = 0x7f0b04c7

.field public static final main:I = 0x7f0b0079

.field public static final main_layout:I = 0x7f0b01eb

.field public static final main_layout2:I = 0x7f0b022a

.field public static final main_layout3:I = 0x7f0b022c

.field public static final main_layout4:I = 0x7f0b022d

.field public static final manage_account_list:I = 0x7f0b0184

.field public static final manual_radio:I = 0x7f0b0141

.field public static final manufacturer_settings:I = 0x7f0b052d

.field public static final mapview_select:I = 0x7f0b0360

.field public static final media_mute_button:I = 0x7f0b02bd

.field public static final media_volume_seekbar:I = 0x7f0b02be

.field public static final menu_cancel:I = 0x7f0b0543

.field public static final menu_del:I = 0x7f0b0544

.field public static final menu_done:I = 0x7f0b0545

.field public static final menu_settings_title:I = 0x7f0b0223

.field public static final message:I = 0x7f0b004a

.field public static final message1:I = 0x7f0b0047

.field public static final message_below_pin:I = 0x7f0b0058

.field public static final message_container:I = 0x7f0b0390

.field public static final message_view:I = 0x7f0b0355

.field public static final method:I = 0x7f0b0451

.field public static final misc_checkbox:I = 0x7f0b036c

.field public static final misc_filename:I = 0x7f0b036d

.field public static final misc_filesize:I = 0x7f0b036e

.field public static final mobileap_help_image:I = 0x7f0b0431

.field public static final mobiledata_checkbox:I = 0x7f0b0209

.field public static final mobiledata_text:I = 0x7f0b0208

.field public static final mode_list:I = 0x7f0b0114

.field public static final more_control_buttons_panel:I = 0x7f0b0199

.field public static final motion_settings:I = 0x7f0b0534

.field public static final mount_button:I = 0x7f0b03a5

.field public static final mouse_hovering_settings:I = 0x7f0b0535

.field public static final mppe:I = 0x7f0b03cc

.field public static final multi_connect_footer:I = 0x7f0b03e6

.field public static final mwi:I = 0x7f0b0328

.field public static final myprofile_bg:I = 0x7f0b020f

.field public static final name:I = 0x7f0b0105

.field public static final nearby_setting:I = 0x7f0b04ec

.field public static final neighboring:I = 0x7f0b0322

.field public static final netmask_edit:I = 0x7f0b0146

.field public static final netmask_text:I = 0x7f0b0145

.field public static final network:I = 0x7f0b0327

.field public static final network_prefix_length:I = 0x7f0b0464

.field public static final network_setup:I = 0x7f0b047a

.field public static final network_state:I = 0x7f0b04c1

.field public static final network_switches:I = 0x7f0b00d9

.field public static final network_switches_container:I = 0x7f0b00d8

.field public static final networkid:I = 0x7f0b04c8

.field public static final never_show_flash_noti_dialog_again:I = 0x7f0b02b3

.field public static final new_backup_pw:I = 0x7f0b0367

.field public static final next_btn:I = 0x7f0b000a

.field public static final next_btn_arrow:I = 0x7f0b000d

.field public static final next_btn_text:I = 0x7f0b000c

.field public static final next_button:I = 0x7f0b0052

.field public static final next_button_image:I = 0x7f0b04aa

.field public static final next_button_text:I = 0x7f0b04a9

.field public static final nfc_explained:I = 0x7f0b0222

.field public static final nfc_setting:I = 0x7f0b04ea

.field public static final nfc_sticker_welcome_image:I = 0x7f0b03f2

.field public static final ngc_sticker_welcome_text:I = 0x7f0b03f3

.field public static final nine:I = 0x7f0b0123

.field public static final no:I = 0x7f0b00be

.field public static final no_device:I = 0x7f0b03ea

.field public static final no_device_icon:I = 0x7f0b03e9

.field public static final no_device_text:I = 0x7f0b03fe

.field public static final no_item:I = 0x7f0b004e

.field public static final no_usb:I = 0x7f0b02b5

.field public static final noitem_image_view:I = 0x7f0b012e

.field public static final none_security_message:I = 0x7f0b0436

.field public static final noti_charges_donotshow:I = 0x7f0b010e

.field public static final noti_charges_donotshow_text:I = 0x7f0b010f

.field public static final noti_charges_msg:I = 0x7f0b010d

.field public static final notificaiton_panel_area:I = 0x7f0b0318

.field public static final notificaiton_panel_bar_layout:I = 0x7f0b0310

.field public static final notificaiton_panel_layout:I = 0x7f0b030f

.field public static final notificationIcon:I = 0x7f0b0308

.field public static final notification_description_text:I = 0x7f0b02c4

.field public static final notification_mute_button:I = 0x7f0b02c5

.field public static final notification_panel_title:I = 0x7f0b0317

.field public static final notification_section:I = 0x7f0b02c3

.field public static final notification_switch:I = 0x7f0b019a

.field public static final notification_vibration_seekbar:I = 0x7f0b02e0

.field public static final notification_volume_seekbar:I = 0x7f0b02c6

.field public static final notificiation_category_name:I = 0x7f0b0229

.field public static final number:I = 0x7f0b031a

.field public static final number_picker:I = 0x7f0b0232

.field public static final oem_info:I = 0x7f0b033d

.field public static final ok:I = 0x7f0b0126

.field public static final ok_button:I = 0x7f0b04d2

.field public static final old_password:I = 0x7f0b009d

.field public static final old_password_prompt:I = 0x7f0b009c

.field public static final one:I = 0x7f0b011b

.field public static final onehand_settings:I = 0x7f0b0502

.field public static final operator:I = 0x7f0b031b

.field public static final operator_settings:I = 0x7f0b0526

.field public static final options:I = 0x7f0b03d8

.field public static final options_for_same_files:I = 0x7f0b0345

.field public static final outerlayout:I = 0x7f0b013a

.field public static final overwrite_radiobutton:I = 0x7f0b0346

.field public static final owner_info_edit_text:I = 0x7f0b0235

.field public static final owner_info_edit_text_popup:I = 0x7f0b0236

.field public static final p2p_help:I = 0x7f0b049e

.field public static final p2p_nodevice:I = 0x7f0b0483

.field public static final p2p_nodevice_padding:I = 0x7f0b04a0

.field public static final package_name:I = 0x7f0b03a1

.field public static final packages_section:I = 0x7f0b0296

.field public static final packages_section_title:I = 0x7f0b0295

.field public static final pager:I = 0x7f0b01f3

.field public static final panel:I = 0x7f0b0312

.field public static final panel_bar:I = 0x7f0b0314

.field public static final panel_border:I = 0x7f0b0313

.field public static final panel_fake:I = 0x7f0b0315

.field public static final panel_icon:I = 0x7f0b022f

.field public static final panel_image:I = 0x7f0b0230

.field public static final panel_text:I = 0x7f0b0231

.field public static final panel_view:I = 0x7f0b0311

.field public static final passwd_checkbox:I = 0x7f0b051a

.field public static final password:I = 0x7f0b03de

.field public static final passwordEntry:I = 0x7f0b009f

.field public static final password_entry:I = 0x7f0b0060

.field public static final password_layout:I = 0x7f0b044d

.field public static final password_text:I = 0x7f0b044c

.field public static final pen_keeper_Popup_msg:I = 0x7f0b0111

.field public static final pen_keeper_do_not_show_again:I = 0x7f0b0112

.field public static final pen_keeper_noti_scroll:I = 0x7f0b0110

.field public static final pen_settings_menu:I = 0x7f0b0537

.field public static final percent:I = 0x7f0b02ec

.field public static final percentage_bar_chart:I = 0x7f0b02e6

.field public static final permissions_section:I = 0x7f0b01b5

.field public static final persist_network:I = 0x7f0b049c

.field public static final personal_section:I = 0x7f0b052e

.field public static final personalization:I = 0x7f0b04ef

.field public static final phase1:I = 0x7f0b0469

.field public static final phase2:I = 0x7f0b0452

.field public static final pinDisplay:I = 0x7f0b0118

.field public static final pin_values_hint:I = 0x7f0b0056

.field public static final pingHostname:I = 0x7f0b031e

.field public static final pingIpAddr:I = 0x7f0b031d

.field public static final ping_test:I = 0x7f0b031c

.field public static final pkg_list:I = 0x7f0b03a0

.field public static final popup_picker_t_r:I = 0x7f0b0008

.field public static final port:I = 0x7f0b030a

.field public static final power:I = 0x7f0b003f

.field public static final power_saving_message:I = 0x7f0b02b6

.field public static final power_saving_message_2:I = 0x7f0b02bb

.field public static final power_saving_mode:I = 0x7f0b04fe

.field public static final power_saving_tips_auto_sync_1:I = 0x7f0b0289

.field public static final power_saving_tips_auto_sync_title:I = 0x7f0b0288

.field public static final power_saving_tips_bluetooth_1:I = 0x7f0b0283

.field public static final power_saving_tips_bluetooth_2:I = 0x7f0b0284

.field public static final power_saving_tips_bluetooth_3:I = 0x7f0b0285

.field public static final power_saving_tips_bluetooth_title:I = 0x7f0b0282

.field public static final power_saving_tips_brightness_1:I = 0x7f0b0280

.field public static final power_saving_tips_brightness_2:I = 0x7f0b0281

.field public static final power_saving_tips_brightness_title:I = 0x7f0b027f

.field public static final power_saving_tips_gps_2:I = 0x7f0b028b

.field public static final power_saving_tips_gps_3:I = 0x7f0b028c

.field public static final power_saving_tips_gps_title:I = 0x7f0b028a

.field public static final power_saving_tips_live_wallpaper_1:I = 0x7f0b028e

.field public static final power_saving_tips_live_wallpaper_title:I = 0x7f0b028d

.field public static final power_saving_tips_mobile_data_1:I = 0x7f0b0287

.field public static final power_saving_tips_mobile_data_title:I = 0x7f0b0286

.field public static final power_saving_tips_screen_1:I = 0x7f0b027c

.field public static final power_saving_tips_screen_2:I = 0x7f0b027d

.field public static final power_saving_tips_screen_3:I = 0x7f0b027e

.field public static final power_saving_tips_screen_title:I = 0x7f0b027b

.field public static final power_settings:I = 0x7f0b0511

.field public static final pref_all:I = 0x7f0b02f1

.field public static final pref_left_button:I = 0x7f0b02f2

.field public static final pref_radio:I = 0x7f0b02f3

.field public static final pref_right_button1:I = 0x7f0b02f4

.field public static final pref_right_button2:I = 0x7f0b02f6

.field public static final pref_right_separator2:I = 0x7f0b02f5

.field public static final preference_area:I = 0x7f0b0374

.field public static final preferredNetworkType:I = 0x7f0b0332

.field public static final prefs:I = 0x7f0b0370

.field public static final preview:I = 0x7f0b0128

.field public static final preview_image:I = 0x7f0b0138

.field public static final previous_btn:I = 0x7f0b04a6

.field public static final previous_btn_img:I = 0x7f0b04a7

.field public static final previous_btn_text:I = 0x7f0b04a8

.field public static final privacy_settings:I = 0x7f0b04dd

.field public static final private_data_header:I = 0x7f0b0036

.field public static final profileExpand:I = 0x7f0b0299

.field public static final progress:I = 0x7f0b03a7

.field public static final progress_bar:I = 0x7f0b00a1

.field public static final progress_info:I = 0x7f0b005e

.field public static final progress_layout:I = 0x7f0b048a

.field public static final prompt1:I = 0x7f0b0364

.field public static final prompt2:I = 0x7f0b0366

.field public static final prompt3:I = 0x7f0b0368

.field public static final providerIcon:I = 0x7f0b0018

.field public static final provider_icon:I = 0x7f0b0392

.field public static final provider_id:I = 0x7f0b0394

.field public static final proxy_exclusionlist:I = 0x7f0b045e

.field public static final proxy_fields:I = 0x7f0b045b

.field public static final proxy_hostname:I = 0x7f0b045c

.field public static final proxy_port:I = 0x7f0b045d

.field public static final proxy_settings:I = 0x7f0b0459

.field public static final proxy_settings_fields:I = 0x7f0b0457

.field public static final proxy_settings_title:I = 0x7f0b0458

.field public static final proxy_warning_limited_support:I = 0x7f0b045a

.field public static final qcommand_add_msg1:I = 0x7f0b0265

.field public static final qcommand_add_msg2:I = 0x7f0b0266

.field public static final qcommand_add_msg3:I = 0x7f0b0267

.field public static final qcommand_add_msg4:I = 0x7f0b0268

.field public static final qcommand_add_title:I = 0x7f0b0264

.field public static final qcommand_delete_msg1:I = 0x7f0b026a

.field public static final qcommand_delete_msg2:I = 0x7f0b026b

.field public static final qcommand_delete_msg3:I = 0x7f0b026c

.field public static final qcommand_delete_msg4:I = 0x7f0b026d

.field public static final qcommand_delete_title:I = 0x7f0b0269

.field public static final qcommand_inapp_title:I = 0x7f0b025e

.field public static final qcommand_open_title:I = 0x7f0b0253

.field public static final qcommand_predefined_msg1:I = 0x7f0b0258

.field public static final qcommand_predefined_msg2:I = 0x7f0b025b

.field public static final qcommand_predefined_title:I = 0x7f0b0257

.field public static final qcommand_use_title:I = 0x7f0b0263

.field public static final qcommand_write_title:I = 0x7f0b0256

.field public static final quick_settings_layout:I = 0x7f0b0316

.field public static final quickcommand_call:I = 0x7f0b0254

.field public static final quickcommand_call_play:I = 0x7f0b0255

.field public static final quickcommand_execute_a:I = 0x7f0b0259

.field public static final quickcommand_execute_a_play:I = 0x7f0b025a

.field public static final quickcommand_execute_b:I = 0x7f0b025c

.field public static final quickcommand_execute_b_play:I = 0x7f0b025d

.field public static final quickcommand_search_apps_1:I = 0x7f0b025f

.field public static final quickcommand_search_apps_1_play:I = 0x7f0b0260

.field public static final quickcommand_search_apps_2:I = 0x7f0b0261

.field public static final quickcommand_search_apps_2_play:I = 0x7f0b0262

.field public static final radioButton:I = 0x7f0b0297

.field public static final radio_power:I = 0x7f0b0333

.field public static final radiobutton:I = 0x7f0b02ef

.field public static final received:I = 0x7f0b0330

.field public static final recommend_applications:I = 0x7f0b0097

.field public static final recommend_applications_tip:I = 0x7f0b0098

.field public static final recommend_applications_tip_play:I = 0x7f0b0099

.field public static final refresh_smsc:I = 0x7f0b0339

.field public static final remember:I = 0x7f0b033e

.field public static final remove_menu_item:I = 0x7f0b0413

.field public static final reset_noti:I = 0x7f0b02bc

.field public static final resets:I = 0x7f0b032b

.field public static final resource:I = 0x7f0b01be

.field public static final restore_comment:I = 0x7f0b018f

.field public static final restore_header:I = 0x7f0b018e

.field public static final restore_start_btn:I = 0x7f0b0190

.field public static final revert_menu_item:I = 0x7f0b0417

.field public static final revert_menu_text:I = 0x7f0b0418

.field public static final right:I = 0x7f0b029c

.field public static final rightSpacer:I = 0x7f0b0104

.field public static final right_button:I = 0x7f0b0271

.field public static final ringer_description_text:I = 0x7f0b02c0

.field public static final ringer_mute_button:I = 0x7f0b02c1

.field public static final ringer_section:I = 0x7f0b02bf

.field public static final ringer_volume_seekbar:I = 0x7f0b02c2

.field public static final roaming:I = 0x7f0b0324

.field public static final routes:I = 0x7f0b03db

.field public static final row:I = 0x7f0b01d1

.field public static final rssi:I = 0x7f0b04c3

.field public static final running_processes:I = 0x7f0b01f8

.field public static final s_beam_explained:I = 0x7f0b0352

.field public static final s_beam_image:I = 0x7f0b0353

.field public static final s_motion_settings:I = 0x7f0b0504

.field public static final safety_assurance_settings:I = 0x7f0b04fc

.field public static final samba_settings:I = 0x7f0b0527

.field public static final samsung_account:I = 0x7f0b033f

.field public static final save:I = 0x7f0b0129

.field public static final save_login:I = 0x7f0b03df

.field public static final save_menu_item:I = 0x7f0b0419

.field public static final save_menu_text:I = 0x7f0b041a

.field public static final sbeam_setting:I = 0x7f0b04eb

.field public static final scale:I = 0x7f0b0041

.field public static final scan_button:I = 0x7f0b04a4

.field public static final scan_list:I = 0x7f0b04ca

.field public static final scanning:I = 0x7f0b03fb

.field public static final scanning_progress:I = 0x7f0b02e9

.field public static final scanning_progress_bar:I = 0x7f0b04ae

.field public static final scanning_text:I = 0x7f0b02ea

.field public static final scloud_settings:I = 0x7f0b04dc

.field public static final screen_compatibility_section:I = 0x7f0b01b0

.field public static final scroll_option_list:I = 0x7f0b0380

.field public static final sdcard_enc_confirm_button:I = 0x7f0b00b3

.field public static final sdcard_enc_confirm_message:I = 0x7f0b00b2

.field public static final sdcard_enc_confirm_message_title:I = 0x7f0b00b1

.field public static final sdcard_encrypt_text_alarm:I = 0x7f0b00b4

.field public static final search:I = 0x7f0b035e

.field public static final search_domains:I = 0x7f0b03d9

.field public static final searching_for_networks:I = 0x7f0b048c

.field public static final searching_progress:I = 0x7f0b048b

.field public static final secondlockText:I = 0x7f0b0061

.field public static final security:I = 0x7f0b0422

.field public static final security_fields:I = 0x7f0b044b

.field public static final security_msg:I = 0x7f0b0423

.field public static final security_settings:I = 0x7f0b050d

.field public static final security_settings_billing_desc:I = 0x7f0b01b6

.field public static final security_settings_billing_list:I = 0x7f0b01b7

.field public static final security_settings_desc:I = 0x7f0b01b9

.field public static final security_settings_list:I = 0x7f0b01ba

.field public static final security_settings_premium_sms_list:I = 0x7f0b01b8

.field public static final security_text:I = 0x7f0b044f

.field public static final security_warning_content:I = 0x7f0b0113

.field public static final seekbar:I = 0x7f0b029d

.field public static final seekbar_layout:I = 0x7f0b02a0

.field public static final seekbar_set_linear:I = 0x7f0b0381

.field public static final seekbar_split_text:I = 0x7f0b02a4

.field public static final select_newer_files_radiobutton:I = 0x7f0b0348

.field public static final selectall_checkbox:I = 0x7f0b0362

.field public static final selected_backup_data_size:I = 0x7f0b002d

.field public static final sendbroadcast:I = 0x7f0b01bf

.field public static final sens_text_frame:I = 0x7f0b02e7

.field public static final sensitivity_seekbar:I = 0x7f0b0384

.field public static final sent:I = 0x7f0b032f

.field public static final sentSinceReceived:I = 0x7f0b0331

.field public static final series:I = 0x7f0b00c6

.field public static final server:I = 0x7f0b03cb

.field public static final service:I = 0x7f0b0350

.field public static final set_time_from_button:I = 0x7f0b0133

.field public static final set_time_from_title:I = 0x7f0b0132

.field public static final set_time_to_button:I = 0x7f0b0135

.field public static final set_time_to_title:I = 0x7f0b0134

.field public static final settings_button:I = 0x7f0b02f0

.field public static final setup_button:I = 0x7f0b00ec

.field public static final setup_fields:I = 0x7f0b0479

.field public static final setup_signature:I = 0x7f0b006f

.field public static final setup_signature_title:I = 0x7f0b006e

.field public static final seven:I = 0x7f0b0121

.field public static final share_section:I = 0x7f0b04e9

.field public static final shared_data_header:I = 0x7f0b002e

.field public static final shared_zone_path_documents:I = 0x7f0b0034

.field public static final shared_zone_path_files:I = 0x7f0b0035

.field public static final shared_zone_path_music:I = 0x7f0b0031

.field public static final shared_zone_path_photo:I = 0x7f0b0032

.field public static final shared_zone_path_video:I = 0x7f0b0033

.field public static final short_camera_body_text:I = 0x7f0b01e4

.field public static final shortcut:I = 0x7f0b02fc

.field public static final shortcut_header_summary:I = 0x7f0b01e8

.field public static final shortcut_header_title:I = 0x7f0b01e7

.field public static final shortcut_icon:I = 0x7f0b01e3

.field public static final shortcut_position:I = 0x7f0b01e6

.field public static final show_advanced:I = 0x7f0b0426

.field public static final show_options:I = 0x7f0b03d7

.field public static final show_owner_info_on_lockscreen_checkbox:I = 0x7f0b0234

.field public static final show_owner_info_on_lockscreen_checkbox_popup:I = 0x7f0b0237

.field public static final show_password:I = 0x7f0b0425

.field public static final show_password_layout:I = 0x7f0b0472

.field public static final signStatusIcon:I = 0x7f0b0014

.field public static final signal:I = 0x7f0b02ff

.field public static final signal_radiobutton:I = 0x1010004

.field public static final signature_identity_list:I = 0x7f0b01b4

.field public static final signature_section:I = 0x7f0b01b3

.field public static final sim_alert_message_area:I = 0x7f0b0375

.field public static final simcard_management:I = 0x7f0b04fa

.field public static final six:I = 0x7f0b0120

.field public static final size:I = 0x7f0b018a

.field public static final skip_all_radiobutton:I = 0x7f0b0347

.field public static final skip_button:I = 0x7f0b0064

.field public static final slow_text:I = 0x7f0b0382

.field public static final smart_alert_message:I = 0x7f0b02d1

.field public static final smart_network_title:I = 0x7f0b037b

.field public static final smart_network_toggle:I = 0x7f0b037d

.field public static final smart_network_toggle_off:I = 0x7f0b037f

.field public static final smart_network_toggle_on:I = 0x7f0b037e

.field public static final smart_rotation_noti1:I = 0x7f0b02d2

.field public static final smart_rotation_noti2:I = 0x7f0b02d3

.field public static final smart_rotation_noti3:I = 0x7f0b02d4

.field public static final smart_rotation_noti4:I = 0x7f0b02d5

.field public static final smart_rotation_noti5:I = 0x7f0b02d6

.field public static final smart_rotation_noti6:I = 0x7f0b02d7

.field public static final smart_screen:I = 0x7f0b0505

.field public static final smart_screen_noti0:I = 0x7f0b0388

.field public static final smart_screen_noti1:I = 0x7f0b0389

.field public static final smart_screen_noti_for_scroll0:I = 0x7f0b038a

.field public static final smart_screen_noti_for_scroll1:I = 0x7f0b038c

.field public static final smart_stay_noti0:I = 0x7f0b02d8

.field public static final smart_stay_noti1:I = 0x7f0b02d9

.field public static final smart_stay_noti2:I = 0x7f0b02db

.field public static final smart_stay_noti3:I = 0x7f0b02dc

.field public static final smart_stay_noti4:I = 0x7f0b02dd

.field public static final smart_stay_noti_extra:I = 0x7f0b02da

.field public static final sms_over_ims:I = 0x7f0b0335

.field public static final smsc:I = 0x7f0b033a

.field public static final smsc_label:I = 0x7f0b0337

.field public static final snippet:I = 0x7f0b034f

.field public static final sound_settings:I = 0x7f0b04f5

.field public static final spacerBottom:I = 0x7f0b005f

.field public static final spinner_fontsize:I = 0x7f0b01dd

.field public static final spinner_fontstyle:I = 0x7f0b0221

.field public static final splitColorView:I = 0x7f0b01e1

.field public static final ssid:I = 0x7f0b0420

.field public static final ssid_layout:I = 0x7f0b044a

.field public static final ssid_text:I = 0x7f0b0449

.field public static final startactivity:I = 0x7f0b01c0

.field public static final staticip:I = 0x7f0b0461

.field public static final status:I = 0x7f0b003e

.field public static final statusIcon:I = 0x7f0b001b

.field public static final stepIndicator:I = 0x7f0b00c2

.field public static final stop_button:I = 0x7f0b0270

.field public static final stop_button_layout:I = 0x7f0b026f

.field public static final storageChartLabel:I = 0x7f0b01f0

.field public static final storage_color_bar:I = 0x7f0b01ee

.field public static final storage_settings:I = 0x7f0b0512

.field public static final storage_usb:I = 0x7f0b0548

.field public static final subtitle:I = 0x7f0b00cc

.field public static final successes:I = 0x7f0b032d

.field public static final summary:I = 0x7f0b028f

.field public static final summary_text:I = 0x7f0b016a

.field public static final supplicant_state:I = 0x7f0b04c2

.field public static final sweep_left:I = 0x7f0b00c8

.field public static final sweep_limit:I = 0x7f0b00cb

.field public static final sweep_right:I = 0x7f0b00c9

.field public static final sweep_warning:I = 0x7f0b00ca

.field public static final switchWidget:I = 0x7f0b02e3

.field public static final switch_ime_button:I = 0x7f0b00ad

.field public static final syncDescription:I = 0x7f0b038e

.field public static final syncStatusIcon:I = 0x7f0b0013

.field public static final sync_active:I = 0x7f0b02fe

.field public static final sync_failed:I = 0x7f0b02fd

.field public static final sync_settings_error_info:I = 0x7f0b0015

.field public static final system_list:I = 0x7f0b039b

.field public static final system_mute_button:I = 0x7f0b02cb

.field public static final system_permissions:I = 0x7f0b050b

.field public static final system_progress:I = 0x7f0b039a

.field public static final system_section:I = 0x7f0b0533

.field public static final system_tab:I = 0x7f0b0399

.field public static final system_update:I = 0x7f0b0516

.field public static final system_volume_seekbar:I = 0x7f0b02cc

.field public static final t_roaming_settings:I = 0x7f0b04e6

.field public static final tab_custom_layout:I = 0x7f0b001c

.field public static final tab_custom_view_icon:I = 0x7f0b001d

.field public static final tab_custom_view_text:I = 0x7f0b001e

.field public static final table:I = 0x7f0b013b

.field public static final tabs:I = 0x7f0b01f4

.field public static final tabs_container:I = 0x7f0b00dd

.field public static final technology:I = 0x7f0b0045

.field public static final temperature:I = 0x7f0b0044

.field public static final temperature_warning:I = 0x7f0b02b1

.field public static final text:I = 0x7f0b0055

.field public static final text1:I = 0x7f0b03e1

.field public static final text2:I = 0x7f0b03e2

.field public static final textMessage:I = 0x7f0b03e0

.field public static final textView1:I = 0x7f0b012b

.field public static final textView2:I = 0x7f0b0488

.field public static final text_account_and_sync:I = 0x7f0b0166

.field public static final text_battery:I = 0x7f0b015f

.field public static final text_bluetooth:I = 0x7f0b0083

.field public static final text_brightness:I = 0x7f0b0163

.field public static final text_data:I = 0x7f0b0087

.field public static final text_data_usage:I = 0x7f0b015c

.field public static final text_directcall:I = 0x7f0b03b4

.field public static final text_font_size:I = 0x7f0b0159

.field public static final text_gps:I = 0x7f0b008b

.field public static final text_layout:I = 0x1010000

.field public static final text_name:I = 0x7f0b020d

.field public static final text_powersaving:I = 0x7f0b03c8

.field public static final text_ringtone:I = 0x7f0b014f

.field public static final text_sbeam:I = 0x7f0b03c0

.field public static final text_settings:I = 0x7f0b0169

.field public static final text_smartalert:I = 0x7f0b03c4

.field public static final text_smartstay:I = 0x7f0b03b8

.field public static final text_sync:I = 0x7f0b008f

.field public static final text_turnoff:I = 0x7f0b007c

.field public static final text_voicecmd:I = 0x7f0b03bc

.field public static final text_volume:I = 0x7f0b0155

.field public static final text_wallpaper:I = 0x7f0b0152

.field public static final text_wifi:I = 0x7f0b007f

.field public static final three:I = 0x7f0b011d

.field public static final time:I = 0x7f0b00a8

.field public static final time_format_checkbox:I = 0x7f0b00eb

.field public static final time_format_layout:I = 0x7f0b00e8

.field public static final time_format_summary:I = 0x7f0b00ea

.field public static final time_format_title:I = 0x7f0b00e9

.field public static final time_picker:I = 0x7f0b00f6

.field public static final time_picker_title:I = 0x7f0b00f5

.field public static final time_zone_button:I = 0x7f0b00f0

.field public static final timeout_list:I = 0x7f0b0439

.field public static final timezone_auto_time:I = 0x7f0b00ef

.field public static final timezone_dropdown_anchor:I = 0x7f0b00ed

.field public static final timezone_summary:I = 0x7f0b00e0

.field public static final tips_background_colour:I = 0x7f0b0278

.field public static final tips_background_cpu:I = 0x7f0b0274

.field public static final tips_background_display:I = 0x7f0b0276

.field public static final tips_power_saving_haptic:I = 0x7f0b027a

.field public static final tips_title_background_colour:I = 0x7f0b0277

.field public static final tips_title_background_cpu:I = 0x7f0b0273

.field public static final tips_title_background_display:I = 0x7f0b0275

.field public static final tips_title_power_saving_haptic:I = 0x7f0b0279

.field public static final title:I = 0x7f0b005d

.field public static final titleDivider:I = 0x7f0b01ca

.field public static final title_area:I = 0x7f0b0391

.field public static final title_progress:I = 0x7f0b03fa

.field public static final title_text:I = 0x7f0b00f7

.field public static final title_textview:I = 0x7f0b0071

.field public static final title_textview2:I = 0x7f0b0072

.field public static final toggle_list_check:I = 0x7f0b0130

.field public static final topDisplayGroup:I = 0x7f0b0115

.field public static final topLayout:I = 0x7f0b0068

.field public static final top_divider:I = 0x7f0b00a0

.field public static final top_divider_no_progress:I = 0x7f0b04af

.field public static final top_level:I = 0x7f0b036f

.field public static final top_padding:I = 0x7f0b04ac

.field public static final top_padding_layout:I = 0x7f0b04ab

.field public static final torchlight_settings:I = 0x7f0b04fd

.field public static final total_size_prefix:I = 0x7f0b019b

.field public static final total_size_text:I = 0x7f0b019d

.field public static final trash_user:I = 0x7f0b02fb

.field public static final trusted_credential_status:I = 0x7f0b0397

.field public static final trusted_credential_subject_primary:I = 0x7f0b0395

.field public static final trusted_credential_subject_secondary:I = 0x7f0b0396

.field public static final try_btn:I = 0x7f0b0077

.field public static final tts_engine_pref:I = 0x7f0b02f7

.field public static final tts_engine_pref_text:I = 0x7f0b02f9

.field public static final tts_engine_radiobutton:I = 0x7f0b02f8

.field public static final tts_engine_settings:I = 0x7f0b02fa

.field public static final tutorial_content_textview:I = 0x7f0b0065

.field public static final tvEditmyprofile:I = 0x7f0b01db

.field public static final tvFontSize:I = 0x7f0b01dc

.field public static final tvPreviewText:I = 0x7f0b01da

.field public static final tv_device_name:I = 0x7f0b043d

.field public static final tv_mac_address:I = 0x7f0b0444

.field public static final tv_profileLabel:I = 0x7f0b0359

.field public static final two:I = 0x7f0b011c

.field public static final two_buttons_panel:I = 0x7f0b0291

.field public static final txtDate:I = 0x7f0b021c

.field public static final txtTapToAdd:I = 0x7f0b012c

.field public static final txtTimeAmPm:I = 0x7f0b0219

.field public static final txtTimeAmPmfirst:I = 0x7f0b0213

.field public static final type:I = 0x7f0b0238

.field public static final typeSpinner:I = 0x7f0b039f

.field public static final type_security:I = 0x7f0b044e

.field public static final type_ssid:I = 0x7f0b0448

.field public static final unmount_button:I = 0x7f0b03a6

.field public static final update:I = 0x7f0b04bf

.field public static final update_smsc:I = 0x7f0b0338

.field public static final upgrade_btn:I = 0x7f0b00dc

.field public static final uptime:I = 0x7f0b0046

.field public static final usage_disclaimer:I = 0x7f0b00db

.field public static final usage_summary:I = 0x7f0b00da

.field public static final usage_time:I = 0x7f0b03a3

.field public static final usb_settings:I = 0x7f0b0539

.field public static final usedStorageText:I = 0x7f0b01ef

.field public static final user_cert:I = 0x7f0b0454

.field public static final user_dict_settings_add_dialog_top:I = 0x7f0b03a8

.field public static final user_dictionary_add_locale:I = 0x7f0b03b0

.field public static final user_dictionary_add_locale_label:I = 0x7f0b03af

.field public static final user_dictionary_add_shortcut:I = 0x7f0b03ad

.field public static final user_dictionary_add_shortcut_label:I = 0x7f0b03ac

.field public static final user_dictionary_add_word_grid:I = 0x7f0b03aa

.field public static final user_dictionary_add_word_grid_1:I = 0x7f0b03ab

.field public static final user_dictionary_add_word_grid_2:I = 0x7f0b03ae

.field public static final user_dictionary_add_word_text:I = 0x7f0b03a9

.field public static final user_id:I = 0x7f0b0393

.field public static final user_list:I = 0x7f0b039e

.field public static final user_name:I = 0x7f0b0518

.field public static final user_progress:I = 0x7f0b039d

.field public static final user_settings:I = 0x7f0b052c

.field public static final user_tab:I = 0x7f0b039c

.field public static final username:I = 0x7f0b03dd

.field public static final uspwidget_root_layout:I = 0x7f0b03b1

.field public static final value:I = 0x7f0b01c9

.field public static final vertical:I = 0x7f0b0001

.field public static final viewBelowPreaviewText:I = 0x7f0b021e

.field public static final view_separator:I = 0x7f0b0186

.field public static final voice_input_control_settings:I = 0x7f0b0507

.field public static final voltage:I = 0x7f0b0043

.field public static final vpn_create:I = 0x7f0b0549

.field public static final vpn_lockdown:I = 0x7f0b054a

.field public static final waiting_tone_button:I = 0x7f0b02cf

.field public static final waiting_tone_line:I = 0x7f0b02cd

.field public static final waiting_tone_volume_section:I = 0x7f0b02ce

.field public static final waiting_tone_volume_seekbar:I = 0x7f0b02d0

.field public static final wapi_as_cert:I = 0x7f0b0474

.field public static final wapi_cert:I = 0x7f0b0473

.field public static final wapi_psk:I = 0x7f0b046f

.field public static final wapi_psk_type:I = 0x7f0b0470

.field public static final wapi_user_cert:I = 0x7f0b0475

.field public static final warn_text_view:I = 0x7f0b0433

.field public static final warning_low_charge:I = 0x7f0b00b5

.field public static final warning_low_charge_dec:I = 0x7f0b00a2

.field public static final warning_low_password_quality:I = 0x7f0b00b7

.field public static final warning_low_password_quality_dec:I = 0x7f0b00a4

.field public static final warning_sdcard_ongoing_dec:I = 0x7f0b00a6

.field public static final warning_sdcard_ongoing_enc:I = 0x7f0b00a5

.field public static final warning_text:I = 0x7f0b0063

.field public static final warning_unplugged:I = 0x7f0b00b6

.field public static final warning_unplugged_dec:I = 0x7f0b00a3

.field public static final webView:I = 0x7f0b038f

.field public static final welcomedivider:I = 0x7f0b0492

.field public static final wfd_animation_view:I = 0x7f0b03e3

.field public static final wfd_connecting_text:I = 0x7f0b03e4

.field public static final wfd_device_list:I = 0x7f0b03fd

.field public static final wfd_picker_dialog_btn_cancel:I = 0x7f0b03f5

.field public static final wfd_picker_dialog_btn_ok:I = 0x7f0b03f7

.field public static final wfd_picker_dialog_btn_stop:I = 0x7f0b03f6

.field public static final wfd_picker_dialog_buttons_layout:I = 0x7f0b03f4

.field public static final wfd_picker_dialog_fragment:I = 0x7f0b03f0

.field public static final wfd_picker_dialog_fragment_layout:I = 0x7f0b03ee

.field public static final wfd_picker_dialog_layout:I = 0x7f0b03ef

.field public static final wfd_picker_dialog_welcome_layout:I = 0x7f0b03f1

.field public static final wfd_sel_device_row_icon:I = 0x7f0b03ff

.field public static final wfd_sel_device_row_text:I = 0x7f0b0400

.field public static final widget_button_divider:I = 0x7f0b037c

.field public static final widget_frame:I = 0x7f0b037a

.field public static final widget_line_one:I = 0x7f0b014c

.field public static final widget_line_three:I = 0x7f0b0160

.field public static final widget_line_two:I = 0x7f0b0156

.field public static final widget_list:I = 0x7f0b01cc

.field public static final widget_title:I = 0x7f0b014b

.field public static final wifi_advanced_fields:I = 0x7f0b0478

.field public static final wifi_advanced_toggle:I = 0x7f0b0476

.field public static final wifi_advanced_togglebox:I = 0x7f0b0477

.field public static final wifi_ap_5g_warning_dialog:I = 0x7f0b0405

.field public static final wifi_ap_add_or_delete_button:I = 0x7f0b0304

.field public static final wifi_ap_broadcast_network_name_msg:I = 0x7f0b0434

.field public static final wifi_ap_change_security:I = 0x7f0b0437

.field public static final wifi_ap_channel:I = 0x7f0b042a

.field public static final wifi_ap_channel_5g:I = 0x7f0b042b

.field public static final wifi_ap_connected_device_pref:I = 0x7f0b0303

.field public static final wifi_ap_connection_duration:I = 0x7f0b041e

.field public static final wifi_ap_connection_time:I = 0x7f0b041d

.field public static final wifi_ap_device_name:I = 0x7f0b043b

.field public static final wifi_ap_do_not_show_again:I = 0x7f0b0406

.field public static final wifi_ap_help_orient_layout_second:I = 0x7f0b0430

.field public static final wifi_ap_help_orient_layout_second_landscape:I = 0x7f0b0432

.field public static final wifi_ap_ip:I = 0x7f0b041b

.field public static final wifi_ap_mac:I = 0x7f0b041c

.field public static final wifi_ap_maxclient:I = 0x7f0b042d

.field public static final wifi_ap_maxclient_title:I = 0x7f0b042c

.field public static final wifi_ap_pref:I = 0x7f0b0300

.field public static final wifi_ap_remove_whitelist:I = 0x7f0b043a

.field public static final wifi_ap_settings:I = 0x7f0b0524

.field public static final wifi_ap_settings_allowed_toggle:I = 0x7f0b0307

.field public static final wifi_ap_timeout_settings:I = 0x7f0b042f

.field public static final wifi_ap_timeout_settings_title:I = 0x7f0b042e

.field public static final wifi_back_button:I = 0x7f0b0371

.field public static final wifi_cckm:I = 0x7f0b047d

.field public static final wifi_config_ui:I = 0x7f0b04b4

.field public static final wifi_ft:I = 0x7f0b047c

.field public static final wifi_internet_service_check_dialog:I = 0x7f0b0484

.field public static final wifi_keymgmt_layout:I = 0x7f0b047b

.field public static final wifi_manage_networks_fragment:I = 0x7f0b0487

.field public static final wifi_manage_networks_fragment_layout:I = 0x7f0b0486

.field public static final wifi_next_button:I = 0x7f0b0373

.field public static final wifi_p2p_device_picker_fragment:I = 0x7f0b049d

.field public static final wifi_password_layout:I = 0x7f0b0471

.field public static final wifi_picker_dialog_cancel:I = 0x7f0b04a2

.field public static final wifi_picker_dialog_fragment:I = 0x7f0b04a1

.field public static final wifi_picker_dialog_fragment_layout:I = 0x7f0b0489

.field public static final wifi_priority_fields:I = 0x7f0b047e

.field public static final wifi_priority_radiogroup:I = 0x7f0b047f

.field public static final wifi_secsetup_activity_fragment_layout:I = 0x7f0b0404

.field public static final wifi_settings:I = 0x7f0b04df

.field public static final wifi_settings_fragment_layout:I = 0x7f0b04b2

.field public static final wifi_setup_add_network:I = 0x7f0b04ba

.field public static final wifi_setup_cancel:I = 0x7f0b04bb

.field public static final wifi_setup_connect:I = 0x7f0b04bc

.field public static final wifi_setup_fragment:I = 0x7f0b04b3

.field public static final wifi_setup_refresh_list:I = 0x7f0b04be

.field public static final wifi_setup_skip_or_next:I = 0x7f0b04bd

.field public static final wifi_setup_title:I = 0x7f0b04ad

.field public static final wifi_skip_button:I = 0x7f0b0372

.field public static final wifi_sprinthotspot_settings:I = 0x7f0b04e0

.field public static final wifi_state:I = 0x7f0b04c0

.field public static final wifi_switch:I = 0x7f0b04a3

.field public static final wifi_wps:I = 0x7f0b054b

.field public static final wifiscreen_layout_id:I = 0x7f0b048e

.field public static final wifitimer_enable_end_time:I = 0x7f0b04ce

.field public static final wifitimer_enable_start_time:I = 0x7f0b04cb

.field public static final wifitimer_end_tp:I = 0x7f0b04d0

.field public static final wifitimer_endtime:I = 0x7f0b04cf

.field public static final wifitimer_start_tp:I = 0x7f0b04cd

.field public static final wifitimer_starttime:I = 0x7f0b04cc

.field public static final wireless_section:I = 0x7f0b04de

.field public static final wireless_settings:I = 0x7f0b04e8

.field public static final wps_dialog_btn:I = 0x7f0b04d6

.field public static final wps_dialog_txt:I = 0x7f0b04d3

.field public static final wps_pin:I = 0x7f0b049b

.field public static final wps_pin_entry:I = 0x7f0b049a

.field public static final wps_progress_bar:I = 0x7f0b04d5

.field public static final wps_setup:I = 0x7f0b0499

.field public static final wps_timeout_bar:I = 0x7f0b04d4

.field public static final yes:I = 0x7f0b00bd

.field public static final zero:I = 0x7f0b0125


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2586
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
