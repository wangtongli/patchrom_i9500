.class public final Lcom/android/settings_ex/R$xml;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings_ex/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "xml"
.end annotation


# static fields
.field public static final accessibility_settings:I = 0x7f070000

.field public static final accessibilitywidget_info:I = 0x7f070001

.field public static final accessibilitywidget_info_assistive_light:I = 0x7f070002

.field public static final accessibilitywidget_info_flashnotification:I = 0x7f070003

.field public static final accessibilitywidget_info_magnifier:I = 0x7f070004

.field public static final accessibilitywidget_info_negative_colour:I = 0x7f070005

.field public static final accessibilitywidget_info_turnoff_allsounds:I = 0x7f070006

.field public static final account_sync_settings:I = 0x7f070007

.field public static final accounts_headers:I = 0x7f070008

.field public static final add_account_settings:I = 0x7f070009

.field public static final air_motion_settings_2013:I = 0x7f07000a

.field public static final air_motion_tutorial_settings:I = 0x7f07000b

.field public static final air_scroll_advanced_settings_2013:I = 0x7f07000c

.field public static final air_turn_advanced_settings_2013:I = 0x7f07000d

.field public static final alert_message:I = 0x7f07000e

.field public static final allshare_setting:I = 0x7f07000f

.field public static final apn_editor:I = 0x7f070010

.field public static final apn_settings:I = 0x7f070011

.field public static final application_settings:I = 0x7f070012

.field public static final appwidget_info:I = 0x7f070013

.field public static final auto_haptic_settings:I = 0x7f070014

.field public static final backup_assistant_plus_settings:I = 0x7f070015

.field public static final block_settings:I = 0x7f070016

.field public static final bluetooth_device_advanced:I = 0x7f070017

.field public static final bluetooth_scan_dialog:I = 0x7f070018

.field public static final bluetooth_settings:I = 0x7f070019

.field public static final cloud_settings_document_sync:I = 0x7f07001a

.field public static final cloud_settings_main:I = 0x7f07001b

.field public static final cloud_settings_music_sync:I = 0x7f07001c

.field public static final cloud_settings_photos_sync:I = 0x7f07001d

.field public static final cloud_settings_videos_sync:I = 0x7f07001e

.field public static final connection_headers:I = 0x7f07001f

.field public static final connectivitylocationwidget_info:I = 0x7f070020

.field public static final contextualpage_settings:I = 0x7f070021

.field public static final customizable_key:I = 0x7f070022

.field public static final data_usage_metered_prefs:I = 0x7f070023

.field public static final date_time_prefs:I = 0x7f070024

.field public static final development_prefs:I = 0x7f070025

.field public static final device_headers:I = 0x7f070026

.field public static final device_info_memory:I = 0x7f070027

.field public static final device_info_settings:I = 0x7f070028

.field public static final device_info_status:I = 0x7f070029

.field public static final device_picker:I = 0x7f07002a

.field public static final device_picker_myplace:I = 0x7f07002b

.field public static final device_picker_phone:I = 0x7f07002c

.field public static final display_settings:I = 0x7f07002d

.field public static final dmr_setting:I = 0x7f07002e

.field public static final dock_settings:I = 0x7f07002f

.field public static final dormantmode:I = 0x7f070030

.field public static final driving_mode_settings:I = 0x7f070031

.field public static final drivingmodewidget_info:I = 0x7f070032

.field public static final dualclock_settings:I = 0x7f070033

.field public static final easy_display_settings:I = 0x7f070034

.field public static final empty_prefs:I = 0x7f070035

.field public static final ethernet_settings:I = 0x7f070036

.field public static final favoritesettingswidget_info:I = 0x7f070037

.field public static final feature_settings:I = 0x7f070038

.field public static final finger_air_view_settings:I = 0x7f070039

.field public static final glance_view_settings:I = 0x7f07003a

.field public static final hand_motion_settings_2013:I = 0x7f07003b

.field public static final home_settings_list_item:I = 0x7f07003c

.field public static final home_settings_list_item_third:I = 0x7f07003d

.field public static final hs20_settings:I = 0x7f07003e

.field public static final information_ticker:I = 0x7f07003f

.field public static final input_methods_prefs:I = 0x7f070040

.field public static final language_settings:I = 0x7f070041

.field public static final led_indicator_settings:I = 0x7f070042

.field public static final location_consent:I = 0x7f070043

.field public static final location_settings:I = 0x7f070044

.field public static final lockscreen_settings:I = 0x7f070045

.field public static final manage_accounts_settings:I = 0x7f070046

.field public static final management_headers:I = 0x7f070047

.field public static final menupowersavingmode_settings:I = 0x7f070048

.field public static final motion_double_tap_tutorial:I = 0x7f070049

.field public static final motion_sensitivity_settings:I = 0x7f07004a

.field public static final motion_settings:I = 0x7f07004b

.field public static final motion_settings_2013:I = 0x7f07004c

.field public static final motion_tutorial_settings:I = 0x7f07004d

.field public static final motion_view_settings:I = 0x7f07004e

.field public static final mouse_hovering_settings:I = 0x7f07004f

.field public static final my_clock_preferences:I = 0x7f070050

.field public static final my_place_profile_settings:I = 0x7f070051

.field public static final my_place_settings:I = 0x7f070052

.field public static final nfc_settings:I = 0x7f070053

.field public static final notification_panel_menu:I = 0x7f070054

.field public static final onehand_settings:I = 0x7f070055

.field public static final pen_help_menu:I = 0x7f070056

.field public static final pen_help_menu_tablet:I = 0x7f070057

.field public static final pen_hovering:I = 0x7f070058

.field public static final pen_settings_menu:I = 0x7f070059

.field public static final power_settings:I = 0x7f07005a

.field public static final power_usage_summary:I = 0x7f07005b

.field public static final privacy_settings:I = 0x7f07005c

.field public static final quick_glance_advanced_settings_2013:I = 0x7f07005d

.field public static final quick_launch_settings:I = 0x7f07005e

.field public static final remote_controls_settings:I = 0x7f07005f

.field public static final s_motion_guide_hub_prefs_2013:I = 0x7f070060

.field public static final s_motion_settings_2013:I = 0x7f070061

.field public static final safetyzone_notification_settings:I = 0x7f070062

.field public static final safetyzone_settings:I = 0x7f070063

.field public static final samba_configure:I = 0x7f070064

.field public static final samba_logs:I = 0x7f070065

.field public static final samba_settings:I = 0x7f070066

.field public static final seandroid_settings:I = 0x7f070067

.field public static final security_settings:I = 0x7f070068

.field public static final security_settings_biometric_weak:I = 0x7f070069

.field public static final security_settings_cac_pin:I = 0x7f07006a

.field public static final security_settings_chooser:I = 0x7f07006b

.field public static final security_settings_decrypted:I = 0x7f07006c

.field public static final security_settings_encrypted:I = 0x7f07006d

.field public static final security_settings_lockscreen:I = 0x7f07006e

.field public static final security_settings_misc:I = 0x7f07006f

.field public static final security_settings_password:I = 0x7f070070

.field public static final security_settings_pattern:I = 0x7f070071

.field public static final security_settings_picker:I = 0x7f070072

.field public static final security_settings_pin:I = 0x7f070073

.field public static final security_settings_sdcard_encryption:I = 0x7f070074

.field public static final security_settings_signature:I = 0x7f070075

.field public static final security_settings_unencrypted:I = 0x7f070076

.field public static final settings_headers:I = 0x7f070077

.field public static final sim_lock_settings:I = 0x7f070078

.field public static final smart_network_appwidget_info:I = 0x7f070079

.field public static final smartcard_config_settings:I = 0x7f07007a

.field public static final smartscreen_settings:I = 0x7f07007b

.field public static final smartscroll_advanced_settings:I = 0x7f07007c

.field public static final software_update_settings:I = 0x7f07007d

.field public static final sound_settings:I = 0x7f07007e

.field public static final spellchecker_prefs:I = 0x7f07007f

.field public static final sprint_4g_settings:I = 0x7f070080

.field public static final sprint_update_settings:I = 0x7f070081

.field public static final sprint_update_settings_sim_lock:I = 0x7f070082

.field public static final sync_settings:I = 0x7f070083

.field public static final testing_settings:I = 0x7f070084

.field public static final testing_wifi_settings:I = 0x7f070085

.field public static final tether_prefs:I = 0x7f070086

.field public static final timezones:I = 0x7f070087

.field public static final timezones_mea:I = 0x7f070088

.field public static final torchlight_settings:I = 0x7f070089

.field public static final tts_engine_settings:I = 0x7f07008a

.field public static final tts_settings:I = 0x7f07008b

.field public static final usb_settings:I = 0x7f07008c

.field public static final user_settings:I = 0x7f07008d

.field public static final usp_widget_info:I = 0x7f07008e

.field public static final vpn_settings2:I = 0x7f07008f

.field public static final vpn_settings_for_att:I = 0x7f070090

.field public static final wallpaper_settings:I = 0x7f070091

.field public static final wfd_help:I = 0x7f070092

.field public static final wfd_nfc_settings:I = 0x7f070093

.field public static final wfd_nfc_write:I = 0x7f070094

.field public static final wfd_settings:I = 0x7f070095

.field public static final wifi_access_points:I = 0x7f070096

.field public static final wifi_access_points_for_wifi_setup_xl:I = 0x7f070097

.field public static final wifi_access_points_verizon:I = 0x7f070098

.field public static final wifi_advanced_settings:I = 0x7f070099

.field public static final wifi_ap_allowedlist:I = 0x7f07009a

.field public static final wifi_ap_settings:I = 0x7f07009b

.field public static final wifi_calling_settings:I = 0x7f07009c

.field public static final wifi_connecting:I = 0x7f07009d

.field public static final wifi_connection_settings:I = 0x7f07009e

.field public static final wifi_display_settings:I = 0x7f07009f

.field public static final wifi_p2p_device_picker:I = 0x7f0700a0

.field public static final wifi_p2p_help:I = 0x7f0700a1

.field public static final wifi_p2p_settings:I = 0x7f0700a2

.field public static final wifi_picker_dialog:I = 0x7f0700a3

.field public static final wifi_settings:I = 0x7f0700a4

.field public static final wifiap_lan_settings:I = 0x7f0700a5

.field public static final wireless_settings:I = 0x7f0700a6


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
