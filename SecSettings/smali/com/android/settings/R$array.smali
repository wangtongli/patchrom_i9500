.class public final Lcom/android/settings_ex/R$array;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings_ex/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "array"
.end annotation


# static fields
.field public static final allow_list_entries:I = 0x7f0a0083

.field public static final allow_list_value:I = 0x7f0a0084

.field public static final allshare_contents_share:I = 0x7f0a006d

.field public static final allshare_download_from:I = 0x7f0a006b

.field public static final allshare_download_from_value:I = 0x7f0a006c

.field public static final allshare_download_to:I = 0x7f0a0069

.field public static final allshare_download_to_value:I = 0x7f0a006a

.field public static final animator_duration_scale_entries:I = 0x7f0a0055

.field public static final animator_duration_scale_values:I = 0x7f0a0056

.field public static final apn_auth_entries:I = 0x7f0a0044

.field public static final apn_auth_values:I = 0x7f0a0045

.field public static final apn_ently_key:I = 0x7f0a006e

.field public static final apn_ently_key_includeDUN:I = 0x7f0a0070

.field public static final apn_protocol_entries:I = 0x7f0a0046

.field public static final apn_protocol_values:I = 0x7f0a0047

.field public static final apn_type_key:I = 0x7f0a006f

.field public static final apn_type_key_includeDUN:I = 0x7f0a0071

.field public static final app_install_location_entries:I = 0x7f0a004a

.field public static final app_install_location_values:I = 0x7f0a004b

.field public static final app_process_limit_entries:I = 0x7f0a005b

.field public static final app_process_limit_values:I = 0x7f0a005c

.field public static final audio_output_entries:I = 0x7f0a00a1

.field public static final audio_output_values:I = 0x7f0a007f

.field public static final auto_power_off_entries:I = 0x7f0a0005

.field public static final auto_power_off_values:I = 0x7f0a0080

.field public static final auto_refresh_values:I = 0x7f0a0065

.field public static final bearer_entries:I = 0x7f0a0048

.field public static final bearer_values:I = 0x7f0a0049

.field public static final bluetooth_class_of_device_entries:I = 0x7f0a009f

.field public static final bluetooth_class_of_device_values:I = 0x7f0a00a0

.field public static final bluetooth_visibility_timeout_entries:I = 0x7f0a002e

.field public static final bua_plus_intents:I = 0x7f0a0085

.field public static final call_block_mode:I = 0x7f0a0073

.field public static final clock_position_entries:I = 0x7f0a009a

.field public static final clock_size_key:I = 0x7f0a0000

.field public static final clock_size_value:I = 0x7f0a0001

.field public static final contents_type_entries:I = 0x7f0a009b

.field public static final contents_type_entries_weibo:I = 0x7f0a009c

.field public static final contents_type_values:I = 0x7f0a0063

.field public static final customizedkey_popup:I = 0x7f0a0090

.field public static final data_service_entries:I = 0x7f0a0088

.field public static final data_service_values:I = 0x7f0a0089

.field public static final date_format_values:I = 0x7f0a000b

.field public static final device_lock_timeout_entries:I = 0x7f0a0012

.field public static final device_lock_timeout_values:I = 0x7f0a0013

.field public static final disable_block_apps:I = 0x7f0a0072

.field public static final dmr_access_mode_entries:I = 0x7f0a0095

.field public static final dmr_access_mode_values:I = 0x7f0a0096

.field public static final dream_timeout_entries:I = 0x7f0a0010

.field public static final dream_timeout_values:I = 0x7f0a0011

.field public static final emergency_tone_entries:I = 0x7f0a0040

.field public static final emergency_tone_values:I = 0x7f0a0041

.field public static final enable_opengl_traces_entries:I = 0x7f0a0059

.field public static final enable_opengl_traces_values:I = 0x7f0a005a

.field public static final entries_font_size:I = 0x7f0a0017

.field public static final entryvalues_font_size:I = 0x7f0a0018

.field public static final facebook_auto_refresh_entries:I = 0x7f0a009e

.field public static final font_size_range:I = 0x7f0a0003

.field public static final hdcp_checking_summaries:I = 0x7f0a0050

.field public static final hdcp_checking_titles:I = 0x7f0a004e

.field public static final hdcp_checking_values:I = 0x7f0a004f

.field public static final input_method_selector_titles:I = 0x7f0a0097

.field public static final input_method_selector_values:I = 0x7f0a0098

.field public static final key_backlight_entries:I = 0x7f0a0081

.field public static final key_backlight_values:I = 0x7f0a0082

.field public static final lock_after_timeout_entries:I = 0x7f0a0014

.field public static final lock_after_timeout_values:I = 0x7f0a0015

.field public static final lock_screen_unlock_effect_entries:I = 0x7f0a0002

.field public static final lock_screen_unlock_effect_values:I = 0x7f0a0004

.field public static final long_press_timeout_selector_titles:I = 0x7f0a004c

.field public static final long_press_timeout_selector_values:I = 0x7f0a004d

.field public static final message_block_mode:I = 0x7f0a0076

.field public static final myplace_select_method_entries:I = 0x7f0a0078

.field public static final myplace_select_method_no_map_entries:I = 0x7f0a0079

.field public static final overlay_display_devices_entries:I = 0x7f0a0057

.field public static final overlay_display_devices_values:I = 0x7f0a0058

.field public static final pen_hand_side_entries:I = 0x7f0a0067

.field public static final pen_hand_side_values:I = 0x7f0a0068

.field public static final pen_settings_entries:I = 0x7f0a007b

.field public static final pen_settings_values:I = 0x7f0a007d

.field public static final prefered_call_entries:I = 0x7f0a008a

.field public static final prefered_call_values:I = 0x7f0a008b

.field public static final quick_launch_entries:I = 0x7f0a0077

.field public static final quick_launch_values:I = 0x7f0a007a

.field public static final register_icon_entries:I = 0x7f0a008e

.field public static final register_icon_entries_v1:I = 0x7f0a008c

.field public static final register_icon_values:I = 0x7f0a008f

.field public static final register_icon_values_v1:I = 0x7f0a008d

.field public static final screen_timeout_entries:I = 0x7f0a000c

.field public static final screen_timeout_for_device_lock_entries:I = 0x7f0a000e

.field public static final screen_timeout_for_device_lock_values:I = 0x7f0a000f

.field public static final screen_timeout_values:I = 0x7f0a000d

.field public static final security_settings_premium_sms_values:I = 0x7f0a0062

.field public static final signature_settings_verification_level_entries:I = 0x7f0a0086

.field public static final signature_settings_verification_level_values:I = 0x7f0a0087

.field public static final silent_mode_entries:I = 0x7f0a0091

.field public static final silent_mode_values:I = 0x7f0a0092

.field public static final sliding_speed_entries:I = 0x7f0a009d

.field public static final sliding_speed_values:I = 0x7f0a0064

.field public static final sort_by_values:I = 0x7f0a0066

.field public static final special_locale_codes:I = 0x7f0a0042

.field public static final special_locale_names:I = 0x7f0a0043

.field public static final timezone_filters:I = 0x7f0a000a

.field public static final torchlight_timeout_entries:I = 0x7f0a0093

.field public static final torchlight_timeout_values:I = 0x7f0a0094

.field public static final touch_key_light_entries:I = 0x7f0a0099

.field public static final touch_key_light_values:I = 0x7f0a0016

.field public static final transition_animation_scale_entries:I = 0x7f0a0053

.field public static final transition_animation_scale_values:I = 0x7f0a0054

.field public static final tts_demo_string_langs:I = 0x7f0a001e

.field public static final tts_demo_strings:I = 0x7f0a001d

.field public static final tts_pitch_entries:I = 0x7f0a001b

.field public static final tts_pitch_values:I = 0x7f0a001c

.field public static final tts_rate_entries:I = 0x7f0a0019

.field public static final tts_rate_values:I = 0x7f0a001a

.field public static final usage_stats_display_order_types:I = 0x7f0a0037

.field public static final user_content_ratings_entries:I = 0x7f0a0060

.field public static final user_content_ratings_values:I = 0x7f0a0061

.field public static final video_call_block_mode:I = 0x7f0a0075

.field public static final voice_call_block_mode:I = 0x7f0a0074

.field public static final voice_input_alarm:I = 0x7f0a0007

.field public static final voice_input_camera:I = 0x7f0a0008

.field public static final voice_input_incoming_call:I = 0x7f0a0006

.field public static final voice_input_music:I = 0x7f0a0009

.field public static final vpn_states:I = 0x7f0a005f

.field public static final vpn_types:I = 0x7f0a005d

.field public static final vpn_types_long:I = 0x7f0a005e

.field public static final wifi_ap_channel:I = 0x7f0a002c

.field public static final wifi_ap_connection_mode:I = 0x7f0a002b

.field public static final wifi_ap_maxclient:I = 0x7f0a002d

.field public static final wifi_ap_security:I = 0x7f0a0026

.field public static final wifi_ap_timeout:I = 0x7f0a002a

.field public static final wifi_connect_entries:I = 0x7f0a007c

.field public static final wifi_connect_values:I = 0x7f0a007e

.field public static final wifi_connection_settings:I = 0x7f0a003e

.field public static final wifi_connection_settings_summary:I = 0x7f0a003f

.field public static final wifi_eap_entries:I = 0x7f0a0038

.field public static final wifi_eap_method:I = 0x7f0a0027

.field public static final wifi_frequency_band_entries:I = 0x7f0a0033

.field public static final wifi_frequency_band_values:I = 0x7f0a0034

.field public static final wifi_ip_settings:I = 0x7f0a003c

.field public static final wifi_network_setup:I = 0x7f0a003b

.field public static final wifi_p2p_status:I = 0x7f0a0029

.field public static final wifi_p2p_wps_setup:I = 0x7f0a0028

.field public static final wifi_phase1_entries:I = 0x7f0a0039

.field public static final wifi_phase2_entries:I = 0x7f0a003a

.field public static final wifi_preferred_band_entries:I = 0x7f0a0035

.field public static final wifi_preferred_band_values:I = 0x7f0a0036

.field public static final wifi_proxy_settings:I = 0x7f0a003d

.field public static final wifi_security:I = 0x7f0a0021

.field public static final wifi_security_ft:I = 0x7f0a0022

.field public static final wifi_security_no_eap:I = 0x7f0a0025

.field public static final wifi_signal:I = 0x7f0a002f

.field public static final wifi_sleep_policy_entries:I = 0x7f0a0030

.field public static final wifi_sleep_policy_entries_wifi_only:I = 0x7f0a0031

.field public static final wifi_sleep_policy_values:I = 0x7f0a0032

.field public static final wifi_status:I = 0x7f0a001f

.field public static final wifi_status_with_ssid:I = 0x7f0a0020

.field public static final wifi_wapi_psk_type:I = 0x7f0a0024

.field public static final wifi_wapi_security:I = 0x7f0a0023

.field public static final window_animation_scale_entries:I = 0x7f0a0051

.field public static final window_animation_scale_values:I = 0x7f0a0052


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
