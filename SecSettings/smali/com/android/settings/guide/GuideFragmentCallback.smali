.class public interface abstract Lcom/android/settings_ex/guide/GuideFragmentCallback;
.super Ljava/lang/Object;
.source "GuideFragmentCallback.java"


# virtual methods
.method public abstract dispatchKeyEvent(Landroid/view/KeyEvent;)Z
.end method

.method public abstract getOnDismissListener()Landroid/content/DialogInterface$OnDismissListener;
.end method

.method public abstract getOnKeyListener()Landroid/content/DialogInterface$OnKeyListener;
.end method

.method public abstract getOnShowListener()Landroid/content/DialogInterface$OnShowListener;
.end method

.method public abstract setEnablers(Lcom/android/settings_ex/bluetooth/BluetoothEnabler;Lcom/android/settings_ex/wifi/WifiEnabler;)V
.end method

.method public abstract setSelectedSectionId(J)Z
.end method
