.class public final Lcom/android/settings_ex/R$bool;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings_ex/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "bool"
.end annotation


# static fields
.field public static final accessibility_widget_assistive_light:I = 0x7f0c0009

.field public static final accessibility_widget_flashnotification:I = 0x7f0c000a

.field public static final accessibility_widget_led_notification:I = 0x7f0c000d

.field public static final accessibility_widget_magnifier:I = 0x7f0c000c

.field public static final accessibility_widget_mono_audio:I = 0x7f0c0008

.field public static final accessibility_widget_negative_colours:I = 0x7f0c000b

.field public static final accessibility_widget_turnoff_allsounds:I = 0x7f0c000e

.field public static final auto_confirm_bluetooth_activation_dialog:I = 0x7f0c0006

.field public static final config_additional_system_update_setting_enable:I = 0x7f0c0005

.field public static final config_msid_enable:I = 0x7f0c0023

.field public static final config_show_regulatory_info:I = 0x7f0c0007

.field public static final config_smart_network_widget_enabled:I = 0x7f0c001b

.field public static final driving_mode_widget:I = 0x7f0c0012

.field public static final enable_bua:I = 0x7f0c0018

.field public static final enable_dormant_mode:I = 0x7f0c0021

.field public static final enable_homesettings_icon:I = 0x7f0c001a

.field public static final enable_hotspot_launcher:I = 0x7f0c0014

.field public static final enable_tethersettings_launcher:I = 0x7f0c0015

.field public static final enable_tethersettings_shortcut:I = 0x7f0c0016

.field public static final enable_wifi_ip_settings:I = 0x7f0c001e

.field public static final enable_wifi_settings:I = 0x7f0c001d

.field public static final enable_wifiapsettings_activity:I = 0x7f0c0017

.field public static final favorite_settings_widget:I = 0x7f0c0011

.field public static final force_remove_alarm_volume:I = 0x7f0c0019

.field public static final hardwareAccelerated:I = 0x7f0c0010

.field public static final has_additional_pin:I = 0x7f0c000f

.field public static final has_connectivity_widget:I = 0x7f0c0003

.field public static final has_dock_settings:I = 0x7f0c0000

.field public static final has_powercontrol_widget:I = 0x7f0c0002

.field public static final has_settings_shortcut:I = 0x7f0c001c

.field public static final has_silent_mode:I = 0x7f0c0001

.field public static final has_usp_widget:I = 0x7f0c0013

.field public static final samsung_accessibility_settings:I = 0x7f0c0004

.field public static final settings_list:I = 0x7f0c001f

.field public static final settings_tab:I = 0x7f0c0020

.field public static final show_system_memory:I = 0x7f0c0022

.field public static final supports_advanced_settings:I = 0x7f0c0024


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 722
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
