.class public final Lcom/android/settings_ex/cloud/CloudSettingConstants$Protocol;
.super Ljava/lang/Object;
.source "CloudSettingConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings_ex/cloud/CloudSettingConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Protocol"
.end annotation


# static fields
.field public static final CLOUD_ACTION_REQUEST_CHANGED:Ljava/lang/String; = "com.sec.android.cloudagent.settings.ACTION_REQUEST_CHANGED"

.field public static final CLOUD_ACTION_REQUEST_CLOUD_STATUS:Ljava/lang/String; = "com.sec.android.cloudagent.settings.ACTION_REQUEST_CLOUD_STATUS"

.field public static final CLOUD_ACTION_REQUEST_OFF:Ljava/lang/String; = "com.sec.android.cloudagent.ACTION_REQUEST_CLOUD_OFF"

.field public static final CLOUD_ACTION_REQUEST_SIGNIN:Ljava/lang/String; = "com.sec.android.cloudagent.ACTION_REQUEST_SIGNIN"

.field public static final CLOUD_ACTION_REQUEST_STORAGE_USAGE:Ljava/lang/String; = "com.sec.android.cloudagent.settings.ACTION_REQUEST_STORAGE_USAGE"

.field public static final CLOUD_ACTION_RESPONSE_CLOUD_STATUS:Ljava/lang/String; = "com.sec.android.cloudagent.settings.ACTION_RESPONSE_CLOUD_STATUS"

.field public static final CLOUD_ACTION_RESPONSE_STORAGE_USAGE:Ljava/lang/String; = "com.sec.android.cloudagent.settings.ACTION_RESPONSE_STORAGE_USAGE"

.field public static final CLOUD_ACTION_SAMSUNG_ACCOUNT:Ljava/lang/String; = "com.osp.app.signin.action.SAMSUNG_ACCOUNT_SETUPWIZARD"

.field public static final CLOUD_AUTO_UPLOAD:I = 0x1000

.field public static final CLOUD_DOCUMENT_SYNC:I = 0x80

.field public static final CLOUD_EXTRA_ACCOUNT_NAME:Ljava/lang/String; = "accountName"

.field public static final CLOUD_EXTRA_ACCOUNT_NAME_LIST:Ljava/lang/String; = "accountNameList"

.field public static final CLOUD_EXTRA_CA_TO_SETTINGS_VALUES:Ljava/lang/String; = "caToSettingsValues"

.field public static final CLOUD_EXTRA_DOCUMENT_SYNC:Ljava/lang/String; = "docSync"

.field public static final CLOUD_EXTRA_DOCUMENT_SYNC_FOLDER_NAME:Ljava/lang/String; = "documentSyncFolderName"

.field public static final CLOUD_EXTRA_MUSIC_SYNC:Ljava/lang/String; = "musicSync"

.field public static final CLOUD_EXTRA_NUMBER_OF_DISPLAYING_PHOTOS:Ljava/lang/String; = "numberOfDisplayingPhotos"

.field public static final CLOUD_EXTRA_PHOTO_SYNC:Ljava/lang/String; = "photoSync"

.field public static final CLOUD_EXTRA_STORAGE_USAGE:Ljava/lang/String; = "storageUsage"

.field public static final CLOUD_EXTRA_VIDEO_SYNC:Ljava/lang/String; = "videoSync"

.field public static final CLOUD_EXTRA_WIFI_ONLY:Ljava/lang/String; = "wifiOnly"

.field public static final CLOUD_EXTRA_WIFI_ONLY_TYPE:Ljava/lang/String; = "wifiOnlyType"

.field public static final CLOUD_EXTRA_WIFI_ONLY_TYPE_DOCUMENT:Ljava/lang/String; = "doc"

.field public static final CLOUD_EXTRA_WIFI_ONLY_TYPE_MUSIC:Ljava/lang/String; = "music"

.field public static final CLOUD_EXTRA_WIFI_ONLY_TYPE_PHOTO:Ljava/lang/String; = "photo"

.field public static final CLOUD_EXTRA_WIFI_ONLY_TYPE_VIDEO:Ljava/lang/String; = "video"

.field public static final CLOUD_EXTRA_WIFI_ONLY_VALUE:Ljava/lang/String; = "wifiOnlyValue"

.field public static final CLOUD_MUSIC_SYNC:I = 0x40

.field public static final CLOUD_PHOTO_SYNC:I = 0x10

.field public static final CLOUD_SUPPORT_DOCUMENT_SYNC:I = 0x8

.field public static final CLOUD_SUPPORT_MUSIC_SYNC:I = 0x4

.field public static final CLOUD_SUPPORT_PHOTO_SYNC:I = 0x1

.field public static final CLOUD_SUPPORT_VIDEO_SYNC:I = 0x2

.field public static final CLOUD_USE_MOBILE_DATA_FOR_AUTO_UPLOAD:I = 0x2000

.field public static final CLOUD_USE_MOBILE_DATA_FOR_DOC_SYNC:I = 0x4000

.field public static final CLOUD_VIDEO_SYNC:I = 0x20

.field public static final CLOUD_WIFI_DOCUMENT:I = 0x800

.field public static final CLOUD_WIFI_MUSIC:I = 0x400

.field public static final CLOUD_WIFI_PHOTO:I = 0x100

.field public static final CLOUD_WIFI_VIDEO:I = 0x200

.field public static final EXTRA_CLOUD_SETTINGS:Ljava/lang/String; = "cloudSettings"

.field public static final EXTRA_CUR_ACCOUNT_NAME:Ljava/lang/String; = "accountName"

.field public static final EXTRA_STORAGE_VENDER:Ljava/lang/String; = "storageVender"

.field public static final EXTRA_VENDER_ID:Ljava/lang/String; = "storageVender"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
