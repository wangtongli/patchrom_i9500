.class public final Lcom/android/settings_ex/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings_ex/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final action_bar_button_text_color:I = 0x7f0d001d

.field public static final airplane_status_off:I = 0x7f0d0022

.field public static final airplane_status_on:I = 0x7f0d0021

.field public static final appwidget_smart_network_text_color:I = 0x7f0d0020

.field public static final black:I = 0x7f0d0007

.field public static final bottom_button_text:I = 0x7f0d0001

.field public static final bottom_button_text_selector:I = 0x7f0d0028

.field public static final button_background_color_normal:I = 0x7f0d0024

.field public static final button_background_color_pressed:I = 0x7f0d0023

.field public static final con_panel_off:I = 0x7f0d0027

.field public static final con_panel_on:I = 0x7f0d0026

.field public static final crypt_keeper_clock_am_pm:I = 0x7f0d0019

.field public static final crypt_keeper_clock_background:I = 0x7f0d0017

.field public static final crypt_keeper_clock_foreground:I = 0x7f0d0018

.field public static final device_vibration_strok_color:I = 0x7f0d0005

.field public static final divider_color:I = 0x7f0d001a

.field public static final guide_arrow_bubble_text_color:I = 0x7f0d001f

.field public static final memory_apps_usage:I = 0x7f0d000e

.field public static final memory_avail:I = 0x7f0d000d

.field public static final memory_cache:I = 0x7f0d0012

.field public static final memory_dcim:I = 0x7f0d0010

.field public static final memory_downloads:I = 0x7f0d000f

.field public static final memory_misc:I = 0x7f0d0013

.field public static final memory_music:I = 0x7f0d0011

.field public static final memory_system:I = 0x7f0d0014

.field public static final memory_used:I = 0x7f0d000c

.field public static final memory_user_dark:I = 0x7f0d0016

.field public static final memory_user_light:I = 0x7f0d0015

.field public static final notification_panel_text_color:I = 0x7f0d0025

.field public static final password_entry:I = 0x7f0d0000

.field public static final preview_background_color_normal:I = 0x7f0d0006

.field public static final red:I = 0x7f0d0008

.field public static final setup_divider_color:I = 0x7f0d001c

.field public static final setupwizard_divider_color:I = 0x7f0d001e

.field public static final title_color:I = 0x7f0d001b

.field public static final vib_center_text:I = 0x7f0d0002

.field public static final vib_rec_area:I = 0x7f0d0004

.field public static final vib_rec_line:I = 0x7f0d0003

.field public static final wifi_ap_connected:I = 0x7f0d000a

.field public static final wifi_ap_connecting:I = 0x7f0d000b

.field public static final wifi_connected_toast:I = 0x7f0d0009


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 800
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
