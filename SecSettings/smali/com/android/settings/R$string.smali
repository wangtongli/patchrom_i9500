.class public final Lcom/android/settings_ex/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings_ex/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final CACBTConnect:I = 0x7f0901c6

.field public static final Dialog_PowerkeyEndsCalls:I = 0x7f090de2

.field public static final FIPS_kernel_crypto_module:I = 0x7f090ae9

.field public static final FIPS_key_managemnet_module:I = 0x7f090aea

.field public static final FIPS_status_disabled:I = 0x7f090aec

.field public static final FIPS_status_enabled:I = 0x7f090aeb

.field public static final Recognition_preference_title:I = 0x7f090b52

.field public static final Sprint:I = 0x7f0911b6

.field public static final WSS_RSR_STR_FOTA_MENU_Update_Sub:I = 0x7f091130

.field public static final a_sensor_summary:I = 0x7f0909c4

.field public static final a_sensor_title:I = 0x7f0909c3

.field public static final abbrev_wday_abbrev_full_month_day_no_year:I = 0x7f091002

.field public static final abbrev_wday_day_abbrev_full_month_no_year:I = 0x7f091001

.field public static final about_phone:I = 0x7f090618

.field public static final about_settings:I = 0x7f090617

.field public static final about_settings_summary:I = 0x7f090619

.field public static final about_usb_settings_remove_the_cable_popup:I = 0x7f0909f2

.field public static final accelerometer_summary_off:I = 0x7f0904db

.field public static final accelerometer_summary_off_second:I = 0x7f09107a

.field public static final accelerometer_summary_on:I = 0x7f0904da

.field public static final accelerometer_summary_on_second:I = 0x7f091079

.field public static final accelerometer_summary_open_off:I = 0x7f091077

.field public static final accelerometer_summary_open_on:I = 0x7f091076

.field public static final accelerometer_title:I = 0x7f0904d9

.field public static final accelerometer_title_close:I = 0x7f091075

.field public static final accelerometer_title_second:I = 0x7f091078

.field public static final access_control:I = 0x7f090b53

.field public static final access_control_summary:I = 0x7f090b54

.field public static final accessdlg_action_default:I = 0x7f090d37

.field public static final accessdlg_message:I = 0x7f090d36

.field public static final accessdlg_title:I = 0x7f090d35

.field public static final accessibility_access_control_dialog_summary:I = 0x7f09077d

.field public static final accessibility_access_control_dialog_title:I = 0x7f09077c

.field public static final accessibility_access_control_disable_am_summary:I = 0x7f09077f

.field public static final accessibility_access_control_disable_am_title:I = 0x7f09077e

.field public static final accessibility_access_control_disable_tb_summary:I = 0x7f090780

.field public static final accessibility_access_control_summary:I = 0x7f090779

.field public static final accessibility_access_control_summary_1:I = 0x7f09077a

.field public static final accessibility_access_control_summary_2:I = 0x7f09077b

.field public static final accessibility_access_control_title:I = 0x7f090778

.field public static final accessibility_air_view_magnification_summary:I = 0x7f090782

.field public static final accessibility_air_view_magnification_title:I = 0x7f090781

.field public static final accessibility_am_dialog_summary:I = 0x7f090775

.field public static final accessibility_am_dialog_summary_ac:I = 0x7f090777

.field public static final accessibility_am_dialog_summary_tb:I = 0x7f090776

.field public static final accessibility_am_dialog_title:I = 0x7f090772

.field public static final accessibility_am_dialog_title_ac:I = 0x7f090774

.field public static final accessibility_am_dialog_title_tb:I = 0x7f090773

.field public static final accessibility_assistant_menu:I = 0x7f090b4f

.field public static final accessibility_assurance_menu_summary:I = 0x7f091265

.field public static final accessibility_feature_state_off:I = 0x7f090784

.field public static final accessibility_feature_state_on:I = 0x7f090783

.field public static final accessibility_global_gesture_preference_description:I = 0x7f090765

.field public static final accessibility_global_gesture_preference_summary_off:I = 0x7f090764

.field public static final accessibility_global_gesture_preference_summary_on:I = 0x7f090763

.field public static final accessibility_global_gesture_preference_title:I = 0x7f090762

.field public static final accessibility_logged_in:I = 0x7f090886

.field public static final accessibility_long_press_timeout_preference_title:I = 0x7f09076c

.field public static final accessibility_long_press_timeout_title:I = 0x7f0912d2

.field public static final accessibility_menu_item_settings:I = 0x7f09076e

.field public static final accessibility_menu_item_tutorial:I = 0x7f09076f

.field public static final accessibility_no_services_installed:I = 0x7f090793

.field public static final accessibility_not_logged_in:I = 0x7f090887

.field public static final accessibility_power_button_ends_call_prerefence_title:I = 0x7f09076a

.field public static final accessibility_power_button_ends_call_summary:I = 0x7f090de1

.field public static final accessibility_power_button_ends_call_title:I = 0x7f0912d0

.field public static final accessibility_screen_magnification_summary:I = 0x7f090761

.field public static final accessibility_screen_magnification_title:I = 0x7f090760

.field public static final accessibility_script_injection_allowed:I = 0x7f090785

.field public static final accessibility_script_injection_button_allow:I = 0x7f090787

.field public static final accessibility_script_injection_button_disallow:I = 0x7f090788

.field public static final accessibility_script_injection_disallowed:I = 0x7f090786

.field public static final accessibility_script_injection_security_warning_summary:I = 0x7f090797

.field public static final accessibility_script_injection_security_warning_title:I = 0x7f090796

.field public static final accessibility_script_injection_title:I = 0x7f0912d3

.field public static final accessibility_service_default_description:I = 0x7f090799

.field public static final accessibility_service_disable_warning_summary:I = 0x7f090792

.field public static final accessibility_service_disable_warning_title:I = 0x7f090791

.field public static final accessibility_service_no_apps_message:I = 0x7f090795

.field public static final accessibility_service_no_apps_title:I = 0x7f090794

.field public static final accessibility_service_security_warning_summary:I = 0x7f090790

.field public static final accessibility_service_security_warning_title:I = 0x7f09078f

.field public static final accessibility_service_state_off:I = 0x7f0912d5

.field public static final accessibility_service_state_on:I = 0x7f0912d4

.field public static final accessibility_services_title:I = 0x7f09075e

.field public static final accessibility_settings:I = 0x7f09075c

.field public static final accessibility_settings_title:I = 0x7f09075d

.field public static final accessibility_speak_password_summary:I = 0x7f091011

.field public static final accessibility_speak_password_title:I = 0x7f0912d1

.field public static final accessibility_sync_disabled:I = 0x7f090884

.field public static final accessibility_sync_enabled:I = 0x7f090883

.field public static final accessibility_sync_error:I = 0x7f090885

.field public static final accessibility_system_title:I = 0x7f09075f

.field public static final accessibility_talkback_noti_summary:I = 0x7f090771

.field public static final accessibility_talkback_noti_title:I = 0x7f090770

.field public static final accessibility_toggle_large_text_preference_title:I = 0x7f090766

.field public static final accessibility_toggle_large_text_title:I = 0x7f0912cf

.field public static final accessibility_toggle_screen_magnification_auto_update_preference_summary:I = 0x7f090769

.field public static final accessibility_toggle_screen_magnification_auto_update_preference_title:I = 0x7f090768

.field public static final accessibility_toggle_screen_magnification_preference_title:I = 0x7f090767

.field public static final accessibility_toggle_script_injection_preference_title:I = 0x7f09076d

.field public static final accessibility_toggle_speak_password_preference_title:I = 0x7f09076b

.field public static final accessibility_touch_exploration_warning:I = 0x7f090798

.field public static final accessibility_tutorial_back:I = 0x7f090971

.field public static final accessibility_tutorial_finish:I = 0x7f090972

.field public static final accessibility_tutorial_lesson_1_text_1:I = 0x7f090975

.field public static final accessibility_tutorial_lesson_1_text_2_more:I = 0x7f090976

.field public static final accessibility_tutorial_lesson_1_text_3:I = 0x7f090977

.field public static final accessibility_tutorial_lesson_1_text_4:I = 0x7f090978

.field public static final accessibility_tutorial_lesson_1_text_4_exited:I = 0x7f090979

.field public static final accessibility_tutorial_lesson_1_text_5:I = 0x7f09097a

.field public static final accessibility_tutorial_lesson_1_title:I = 0x7f090974

.field public static final accessibility_tutorial_lesson_2_text_1:I = 0x7f09097c

.field public static final accessibility_tutorial_lesson_2_text_2_more:I = 0x7f09097d

.field public static final accessibility_tutorial_lesson_2_text_3:I = 0x7f09097e

.field public static final accessibility_tutorial_lesson_2_text_3_more:I = 0x7f09097f

.field public static final accessibility_tutorial_lesson_2_text_4:I = 0x7f090980

.field public static final accessibility_tutorial_lesson_2_title:I = 0x7f09097b

.field public static final accessibility_tutorial_next:I = 0x7f090970

.field public static final accessibility_tutorial_skip:I = 0x7f090973

.field public static final accessibility_tutorial_title:I = 0x7f09096f

.field public static final accessibility_vision_category:I = 0x7f090b2e

.field public static final accessibility_widget_assistive_light:I = 0x7f090dda

.field public static final accessibility_widget_flashnotification:I = 0x7f090ddd

.field public static final accessibility_widget_led_notification:I = 0x7f090ddf

.field public static final accessibility_widget_magnifier:I = 0x7f090ddc

.field public static final accessibility_widget_mono_audio:I = 0x7f090dd9

.field public static final accessibility_widget_negative_color:I = 0x7f090ddb

.field public static final accessibility_widget_turnoff_allsounds:I = 0x7f090dde

.field public static final accessibility_widget_zoom:I = 0x7f090de0

.field public static final accessory:I = 0x7f091041

.field public static final account_label_for_social_hub:I = 0x7f090b58

.field public static final account_management:I = 0x7f091244

.field public static final account_registration:I = 0x7f090a45

.field public static final account_settings:I = 0x7f0904d4

.field public static final account_sync_settings_title:I = 0x7f09088c

.field public static final activate:I = 0x7f091116

.field public static final activate_summary:I = 0x7f091117

.field public static final activate_this_device:I = 0x7f090ee6

.field public static final active_device_admin_msg:I = 0x7f090856

.field public static final active_input_method_subtypes:I = 0x7f09072f

.field public static final activity_not_found:I = 0x7f090650

.field public static final activity_picker_label:I = 0x7f090137

.field public static final adb_warning_message:I = 0x7f090744

.field public static final adb_warning_title:I = 0x7f090743

.field public static final add_account_label:I = 0x7f09088e

.field public static final add_connection_titlebar:I = 0x7f090f63

.field public static final add_device_admin:I = 0x7f09085b

.field public static final add_device_admin_msg:I = 0x7f09085a

.field public static final add_google_account_summary:I = 0x7f090a47

.field public static final add_profile_name:I = 0x7f090f6a

.field public static final add_samsung_account_summary:I = 0x7f090a46

.field public static final additional_system_update:I = 0x7f090008

.field public static final additional_system_update_menu:I = 0x7f090009

.field public static final additional_system_update_settings_list_item_title:I = 0x7f0909ee

.field public static final adjust_brightness_summary:I = 0x7f090044

.field public static final adv_vpn:I = 0x7f09001e

.field public static final advanced_settings:I = 0x7f09065f

.field public static final advanced_settings_summary:I = 0x7f090660

.field public static final agps_using_consent_in_setting:I = 0x7f090603

.field public static final agree:I = 0x7f090edc

.field public static final air_call_accept:I = 0x7f090c5f

.field public static final air_call_accept_auto_start_speaker_summary:I = 0x7f090cc9

.field public static final air_call_accept_auto_start_speaker_title:I = 0x7f090cc8

.field public static final air_call_accept_call:I = 0x7f090cc7

.field public static final air_call_accept_descption:I = 0x7f090c60

.field public static final air_call_accept_setting_title:I = 0x7f090cc6

.field public static final air_clip_descption:I = 0x7f090c5d

.field public static final air_clip_descption_snote:I = 0x7f090c5e

.field public static final air_clip_title:I = 0x7f090c5c

.field public static final air_gesture_depth:I = 0x7f090010

.field public static final air_glance_view:I = 0x7f090c4f

.field public static final air_glance_view_descption:I = 0x7f090c50

.field public static final air_item_move_descption:I = 0x7f090c5a

.field public static final air_item_move_descption_calendar:I = 0x7f090c5b

.field public static final air_item_move_title:I = 0x7f090c59

.field public static final air_motion_disabled_title:I = 0x7f090c85

.field public static final air_motion_indicator_title:I = 0x7f090c4e

.field public static final air_motion_summary:I = 0x7f090c4a

.field public static final air_motion_title:I = 0x7f090c49

.field public static final air_motion_tutorial_msg:I = 0x7f090c63

.field public static final air_motion_tutorial_title:I = 0x7f090c62

.field public static final air_note_swap_descciption:I = 0x7f090c54

.field public static final air_note_swap_title:I = 0x7f090c53

.field public static final air_scroll_album_and_photo_list_summary:I = 0x7f090caf

.field public static final air_scroll_album_and_photo_list_title:I = 0x7f090cae

.field public static final air_scroll_all_list_summary:I = 0x7f090ca6

.field public static final air_scroll_all_list_title:I = 0x7f090ca5

.field public static final air_scroll_contact_list_summary:I = 0x7f090cab

.field public static final air_scroll_contact_list_title:I = 0x7f090caa

.field public static final air_scroll_descripption:I = 0x7f090c56

.field public static final air_scroll_email_app:I = 0x7f090cb0

.field public static final air_scroll_email_body_summary:I = 0x7f090cb2

.field public static final air_scroll_email_body_title:I = 0x7f090cb1

.field public static final air_scroll_email_list_summary:I = 0x7f090cad

.field public static final air_scroll_email_list_title:I = 0x7f090cac

.field public static final air_scroll_internet_app:I = 0x7f090ca7

.field public static final air_scroll_setting_guide_content:I = 0x7f090ca4

.field public static final air_scroll_setting_title:I = 0x7f090ca3

.field public static final air_scroll_title:I = 0x7f090c55

.field public static final air_scroll_web_page_summary:I = 0x7f090ca9

.field public static final air_scroll_web_page_title:I = 0x7f090ca8

.field public static final air_turn_bgm_on_lock_screen_summary:I = 0x7f090cc0

.field public static final air_turn_bgm_on_lock_screen_title:I = 0x7f090cbf

.field public static final air_turn_descption:I = 0x7f090c58

.field public static final air_turn_internet_window_summary:I = 0x7f090cbb

.field public static final air_turn_internet_window_title:I = 0x7f090cb9

.field public static final air_turn_internet_window_title_specific:I = 0x7f090cba

.field public static final air_turn_note_page_view_summary:I = 0x7f090cc5

.field public static final air_turn_note_page_view_title:I = 0x7f090cc1

.field public static final air_turn_note_page_view_title_snote:I = 0x7f090cc3

.field public static final air_turn_note_page_view_title_specific:I = 0x7f090cc2

.field public static final air_turn_note_page_view_title_specific_snote:I = 0x7f090cc4

.field public static final air_turn_now_playing_on_music_summary:I = 0x7f090cbe

.field public static final air_turn_now_playing_on_music_title:I = 0x7f090cbc

.field public static final air_turn_now_playing_on_music_title_specific:I = 0x7f090cbd

.field public static final air_turn_setting_guide_content:I = 0x7f090cb5

.field public static final air_turn_setting_title:I = 0x7f090cb3

.field public static final air_turn_single_photo_view_summary:I = 0x7f090cb8

.field public static final air_turn_single_photo_view_title:I = 0x7f090cb6

.field public static final air_turn_single_photo_view_title_specific:I = 0x7f090cb7

.field public static final air_turn_supporting_applications:I = 0x7f090cb4

.field public static final air_turn_title:I = 0x7f090c57

.field public static final air_view_menu_summary:I = 0x7f091267

.field public static final air_web_navigate:I = 0x7f090c51

.field public static final air_web_navigate_descption:I = 0x7f090c52

.field public static final airplane_error_msg:I = 0x7f090d56

.field public static final airplane_error_title:I = 0x7f090d55

.field public static final airplane_mode:I = 0x7f090147

.field public static final airplane_mode_desc:I = 0x7f0911c0

.field public static final airplane_mode_menu_summary:I = 0x7f091261

.field public static final airplane_mode_off:I = 0x7f0911c3

.field public static final airplane_mode_on:I = 0x7f0911c2

.field public static final airplane_mode_on_message:I = 0x7f0911f7

.field public static final airplane_mode_status:I = 0x7f0911c1

.field public static final alarm:I = 0x7f0909da

.field public static final alarm_notification_summary:I = 0x7f0909db

.field public static final alarm_volume_title:I = 0x7f0904a7

.field public static final alert_mesage_recipient:I = 0x7f090a4d

.field public static final alert_mesage_recipients:I = 0x7f090a3a

.field public static final alert_mesage_recipients_summary:I = 0x7f090a3b

.field public static final alert_message:I = 0x7f090a4e

.field public static final alert_message_connect_storage_for_backup:I = 0x7f0912c1

.field public static final alert_message_connect_storage_for_restore:I = 0x7f0912c2

.field public static final all:I = 0x7f091299

.field public static final all_sound_off:I = 0x7f090f1f

.field public static final all_sound_off_summary:I = 0x7f090f20

.field public static final all_volume_title:I = 0x7f090499

.field public static final allow:I = 0x7f09004a

.field public static final allow_bind_app_widget_activity_allow_bind:I = 0x7f090750

.field public static final allow_bind_app_widget_activity_allow_bind_title:I = 0x7f09074f

.field public static final allow_bind_app_widget_activity_always_allow_bind:I = 0x7f090751

.field public static final allow_mock_location:I = 0x7f090741

.field public static final allow_mock_location_summary:I = 0x7f090742

.field public static final allowed_contacts_title:I = 0x7f091094

.field public static final allowed_list:I = 0x7f091095

.field public static final allshare_accept_device:I = 0x7f090dc1

.field public static final allshare_accept_device_summary:I = 0x7f090dc2

.field public static final allshare_access_mode_changed:I = 0x7f090dc6

.field public static final allshare_category_advanced:I = 0x7f090dba

.field public static final allshare_category_setting:I = 0x7f090dac

.field public static final allshare_category_shared:I = 0x7f090dae

.field public static final allshare_check_music:I = 0x7f090db1

.field public static final allshare_check_photo:I = 0x7f090db0

.field public static final allshare_check_video:I = 0x7f090daf

.field public static final allshare_device_name:I = 0x7f090db4

.field public static final allshare_device_name_length:I = 0x7f090db8

.field public static final allshare_device_name_slash:I = 0x7f090dca

.field public static final allshare_do_not_show_again:I = 0x7f090db9

.field public static final allshare_download_option:I = 0x7f090db3

.field public static final allshare_download_to:I = 0x7f090db2

.field public static final allshare_nearby:I = 0x7f090dbf

.field public static final allshare_no_items:I = 0x7f090dc5

.field public static final allshare_not_allowed_device_list:I = 0x7f090dc7

.field public static final allshare_onoff_title:I = 0x7f090dad

.field public static final allshare_popup_cancel:I = 0x7f090db6

.field public static final allshare_popup_delete:I = 0x7f090dbd

.field public static final allshare_popup_ok:I = 0x7f090db5

.field public static final allshare_popup_save:I = 0x7f090dbc

.field public static final allshare_popup_start_warning:I = 0x7f090dbe

.field public static final allshare_popup_wifi_connect:I = 0x7f090db7

.field public static final allshare_progress_message:I = 0x7f090dc9

.field public static final allshare_reject_device:I = 0x7f090dc3

.field public static final allshare_reject_device_summary:I = 0x7f090dc4

.field public static final allshare_restart_file_sharing_q:I = 0x7f090dcb

.field public static final allshare_summary_shared_contents:I = 0x7f090dc0

.field public static final allshare_title:I = 0x7f090dab

.field public static final allshare_title_summary:I = 0x7f090dc8

.field public static final allshare_upload_from:I = 0x7f090dbb

.field public static final always_allow_bind_appwidgets_text:I = 0x7f09067b

.field public static final always_title:I = 0x7f091091

.field public static final am:I = 0x7f091003

.field public static final android_beam_disabled_summary:I = 0x7f090265

.field public static final android_beam_explained:I = 0x7f090267

.field public static final android_beam_explained_chn:I = 0x7f090268

.field public static final android_beam_label:I = 0x7f090266

.field public static final android_beam_off_summary:I = 0x7f090264

.field public static final android_beam_on_summary:I = 0x7f090263

.field public static final android_beam_settings_title:I = 0x7f090262

.field public static final androidbeam_off_popup_title:I = 0x7f09125e

.field public static final animator_duration_scale_title:I = 0x7f0908eb

.field public static final answering_end_call_summary:I = 0x7f090b42

.field public static final answering_end_call_title:I = 0x7f090b41

.field public static final apn_apn:I = 0x7f090597

.field public static final apn_auth_type:I = 0x7f0905a2

.field public static final apn_auth_type_chap:I = 0x7f0905a5

.field public static final apn_auth_type_none:I = 0x7f0905a3

.field public static final apn_auth_type_pap:I = 0x7f0905a4

.field public static final apn_auth_type_pap_chap:I = 0x7f0905a6

.field public static final apn_edit:I = 0x7f090594

.field public static final apn_exception_proxy:I = 0x7f090e5f

.field public static final apn_exception_proxy1:I = 0x7f090e61

.field public static final apn_exception_proxy2:I = 0x7f090e62

.field public static final apn_exception_proxy3:I = 0x7f090e63

.field public static final apn_exception_proxy_summary:I = 0x7f090e60

.field public static final apn_http_port:I = 0x7f090599

.field public static final apn_http_proxy:I = 0x7f090598

.field public static final apn_mcc:I = 0x7f0905a0

.field public static final apn_mms_port:I = 0x7f09059f

.field public static final apn_mms_proxy:I = 0x7f09059e

.field public static final apn_mmsc:I = 0x7f09059d

.field public static final apn_mnc:I = 0x7f0905a1

.field public static final apn_name:I = 0x7f090596

.field public static final apn_not_set:I = 0x7f090595

.field public static final apn_password:I = 0x7f09059b

.field public static final apn_protocol:I = 0x7f0905a8

.field public static final apn_roaming_protocol:I = 0x7f0905a9

.field public static final apn_server:I = 0x7f09059c

.field public static final apn_settings:I = 0x7f090593

.field public static final apn_type:I = 0x7f0905a7

.field public static final apn_user:I = 0x7f09059a

.field public static final app_disable_dlg_text:I = 0x7f0906be

.field public static final app_disable_dlg_title:I = 0x7f0906bd

.field public static final app_disable_notifications_dlg_text:I = 0x7f0906c2

.field public static final app_disable_notifications_dlg_title:I = 0x7f0906c1

.field public static final app_encryption_button_text:I = 0x7f090001

.field public static final app_encryption_desc:I = 0x7f090000

.field public static final app_factory_reset:I = 0x7f090679

.field public static final app_factory_reset_dlg_text:I = 0x7f09069f

.field public static final app_factory_reset_dlg_title:I = 0x7f09069e

.field public static final app_forward_locked:I = 0x7f0906b4

.field public static final app_install_location_summary:I = 0x7f0906bc

.field public static final app_install_location_title:I = 0x7f0906bb

.field public static final app_name_label:I = 0x7f090759

.field public static final app_not_found_dlg_text:I = 0x7f09069c

.field public static final app_not_found_dlg_title:I = 0x7f09069b

.field public static final app_notifications_switch_label:I = 0x7f090a10

.field public static final app_process_limit_title:I = 0x7f0908f0

.field public static final app_special_disable_dlg_text:I = 0x7f0906c0

.field public static final app_special_disable_dlg_title:I = 0x7f0906bf

.field public static final application_info_label:I = 0x7f090661

.field public static final application_size_label:I = 0x7f09066f

.field public static final applications_settings:I = 0x7f090657

.field public static final applications_settings_header:I = 0x7f090659

.field public static final applications_settings_summary:I = 0x7f090658

.field public static final asensor_button2:I = 0x7f0909c8

.field public static final asensor_calibrating:I = 0x7f0909c6

.field public static final asensor_end:I = 0x7f0909c7

.field public static final asensor_ready:I = 0x7f0909c5

.field public static final ask_compatibility:I = 0x7f09067f

.field public static final ask_on_plug:I = 0x7f090a33

.field public static final ask_on_plug_summary:I = 0x7f090a34

.field public static final assistant_menu_summary:I = 0x7f090b50

.field public static final assisted_gps:I = 0x7f090602

.field public static final assisted_gps_disabled:I = 0x7f090605

.field public static final assisted_gps_enabled:I = 0x7f090604

.field public static final audio_applications:I = 0x7f091047

.field public static final audio_applications_summary:I = 0x7f091048

.field public static final audio_balance:I = 0x7f0910a0

.field public static final audio_output:I = 0x7f091043

.field public static final audio_output_hdmi_stereo:I = 0x7f091278

.field public static final audio_output_hdmi_surround:I = 0x7f091279

.field public static final audio_output_optical_stereo:I = 0x7f09127a

.field public static final audio_output_optical_surround:I = 0x7f09127b

.field public static final audio_preference_title:I = 0x7f090b45

.field public static final audio_record_proc_title:I = 0x7f0904b7

.field public static final auth_error_title:I = 0x7f0911ab

.field public static final auth_fail:I = 0x7f0911aa

.field public static final authdlg_auto_reply:I = 0x7f090d25

.field public static final authdlg_message:I = 0x7f090d23

.field public static final authdlg_service_accept:I = 0x7f090d2e

.field public static final authdlg_service_decline:I = 0x7f090d2f

.field public static final authdlg_service_default:I = 0x7f090d2d

.field public static final authdlg_service_dun:I = 0x7f090d26

.field public static final authdlg_service_ftp:I = 0x7f090d27

.field public static final authdlg_service_map:I = 0x7f090d28

.field public static final authdlg_service_opp:I = 0x7f090d29

.field public static final authdlg_service_pbap:I = 0x7f090d2a

.field public static final authdlg_service_sap:I = 0x7f090d2c

.field public static final authdlg_service_spp:I = 0x7f090d2b

.field public static final authdlg_title:I = 0x7f090d22

.field public static final authorizaion_failed_error:I = 0x7f090d30

.field public static final auto_adjust_touch_summary:I = 0x7f0909b0

.field public static final auto_adjust_touch_title:I = 0x7f0909af

.field public static final auto_ap_forget:I = 0x7f090f6e

.field public static final auto_caps:I = 0x7f0906ee

.field public static final auto_caps_summary:I = 0x7f0906ef

.field public static final auto_detail_text:I = 0x7f09103f

.field public static final auto_detail_title:I = 0x7f09103e

.field public static final auto_haptic:I = 0x7f090e93

.field public static final auto_haptic_popup:I = 0x7f090e94

.field public static final auto_launch_disable_text:I = 0x7f09067c

.field public static final auto_launch_enable_text:I = 0x7f09067a

.field public static final auto_launch_label:I = 0x7f090663

.field public static final auto_launch_label_generic:I = 0x7f090664

.field public static final auto_power_deactivate_alarm:I = 0x7f090e9e

.field public static final auto_power_deactivate_alarm_summary:I = 0x7f090e9d

.field public static final auto_power_dialog_cancel:I = 0x7f090ea9

.field public static final auto_power_dialog_title:I = 0x7f090ea7

.field public static final auto_power_dialog_turn_off:I = 0x7f090ea8

.field public static final auto_power_down_dialog1:I = 0x7f090ea5

.field public static final auto_power_down_dialog2:I = 0x7f090ea6

.field public static final auto_power_end_time:I = 0x7f090ea0

.field public static final auto_power_off_entry:I = 0x7f090a6c

.field public static final auto_power_off_entry_one_hour:I = 0x7f090a6d

.field public static final auto_power_off_time:I = 0x7f090e98

.field public static final auto_power_on_off:I = 0x7f090e9a

.field public static final auto_power_on_off_summary_off:I = 0x7f090e9b

.field public static final auto_power_on_off_summary_on:I = 0x7f090e9c

.field public static final auto_power_on_off_time_same_summary:I = 0x7f090e99

.field public static final auto_power_on_time:I = 0x7f090e97

.field public static final auto_power_repeat:I = 0x7f090ea1

.field public static final auto_power_repeat_dayconcat:I = 0x7f090ea4

.field public static final auto_power_repeat_everyday:I = 0x7f090ea3

.field public static final auto_power_repeat_never:I = 0x7f090ea2

.field public static final auto_power_start_time:I = 0x7f090e9f

.field public static final auto_punctuate:I = 0x7f0906f0

.field public static final auto_punctuate_summary:I = 0x7f0906f2

.field public static final auto_recommendation:I = 0x7f090a89

.field public static final auto_recommendation_help:I = 0x7f090a8a

.field public static final auto_replace:I = 0x7f0906ec

.field public static final auto_replace_summary:I = 0x7f0906ed

.field public static final auto_restore_summary:I = 0x7f09084c

.field public static final auto_restore_title:I = 0x7f09084b

.field public static final auto_roaming_dial_impossible:I = 0x7f091277

.field public static final auto_sync:I = 0x7f090b1b

.field public static final auto_update:I = 0x7f0909b5

.field public static final auto_update_summary:I = 0x7f0909b6

.field public static final automatic_brightness:I = 0x7f0904ec

.field public static final automatic_city_summary:I = 0x7f090d04

.field public static final automatic_city_title:I = 0x7f090d03

.field public static final automatic_noti:I = 0x7f091073

.field public static final automatic_receive_call:I = 0x7f0904d2

.field public static final automatic_receive_call_summary:I = 0x7f0904d3

.field public static final automatic_unlock:I = 0x7f0904d0

.field public static final automatic_unlock_summary:I = 0x7f0904d1

.field public static final available_size_comment:I = 0x7f0912b6

.field public static final availablebutton_category:I = 0x7f0910a5

.field public static final awake:I = 0x7f0907b1

.field public static final background_data:I = 0x7f09088f

.field public static final background_data_dialog_message:I = 0x7f090892

.field public static final background_data_dialog_title:I = 0x7f090891

.field public static final background_data_summary:I = 0x7f090890

.field public static final background_process_stop_description:I = 0x7f0906de

.field public static final backup_comment:I = 0x7f0912c3

.field public static final backup_configure_account_default_summary:I = 0x7f09084a

.field public static final backup_configure_account_title:I = 0x7f090849

.field public static final backup_data_summary:I = 0x7f090848

.field public static final backup_data_title:I = 0x7f090847

.field public static final backup_description:I = 0x7f091284

.field public static final backup_erase_dialog_message:I = 0x7f090854

.field public static final backup_erase_dialog_title:I = 0x7f090853

.field public static final backup_file_path:I = 0x7f091296

.field public static final backup_finished:I = 0x7f0912a8

.field public static final backup_folders_of_external_device:I = 0x7f0912b7

.field public static final backup_lock_settings_picker_title:I = 0x7f090191

.field public static final backup_password_failure:I = 0x7f090dec

.field public static final backup_password_set:I = 0x7f090deb

.field public static final backup_password_unmatch:I = 0x7f090dea

.field public static final backup_path_documents:I = 0x7f0912b1

.field public static final backup_path_files:I = 0x7f0912b2

.field public static final backup_path_label:I = 0x7f091292

.field public static final backup_path_music:I = 0x7f0912ae

.field public static final backup_path_photo:I = 0x7f0912af

.field public static final backup_path_root:I = 0x7f0912ad

.field public static final backup_path_video:I = 0x7f0912b0

.field public static final backup_personal_data:I = 0x7f0912b4

.field public static final backup_private_data:I = 0x7f0912b5

.field public static final backup_progress:I = 0x7f091295

.field public static final backup_pw_cancel_button_text:I = 0x7f090986

.field public static final backup_pw_set_button_text:I = 0x7f090985

.field public static final backup_section_title:I = 0x7f090845

.field public static final backup_shared_data:I = 0x7f0912b3

.field public static final backup_title:I = 0x7f09129f

.field public static final backup_zone_description:I = 0x7f091294

.field public static final band_mode_failed:I = 0x7f090126

.field public static final band_mode_loading:I = 0x7f090124

.field public static final band_mode_set:I = 0x7f090125

.field public static final band_mode_succeeded:I = 0x7f090127

.field public static final band_mode_title:I = 0x7f090123

.field public static final baseband_version:I = 0x7f090540

.field public static final basic_vpn_settings:I = 0x7f09001f

.field public static final battery_action_app_details:I = 0x7f0907cd

.field public static final battery_action_app_settings:I = 0x7f0907ce

.field public static final battery_action_bluetooth:I = 0x7f0907d1

.field public static final battery_action_display:I = 0x7f0907cf

.field public static final battery_action_stop:I = 0x7f0907cc

.field public static final battery_action_wifi:I = 0x7f0907d0

.field public static final battery_desc_apps:I = 0x7f0907dd

.field public static final battery_desc_bluetooth:I = 0x7f0907da

.field public static final battery_desc_display:I = 0x7f0907d6

.field public static final battery_desc_radio:I = 0x7f0907d4

.field public static final battery_desc_standby:I = 0x7f0907d3

.field public static final battery_desc_users:I = 0x7f0907e1

.field public static final battery_desc_voice:I = 0x7f0907d2

.field public static final battery_desc_wifi:I = 0x7f0907d8

.field public static final battery_history_days:I = 0x7f090752

.field public static final battery_history_hours:I = 0x7f090753

.field public static final battery_history_minutes:I = 0x7f090754

.field public static final battery_history_seconds:I = 0x7f090755

.field public static final battery_info_awake_battery:I = 0x7f09008c

.field public static final battery_info_awake_plugged:I = 0x7f09008d

.field public static final battery_info_health_cold:I = 0x7f0900a3

.field public static final battery_info_health_dead:I = 0x7f0900a0

.field public static final battery_info_health_good:I = 0x7f09009e

.field public static final battery_info_health_label:I = 0x7f090085

.field public static final battery_info_health_over_voltage:I = 0x7f0900a1

.field public static final battery_info_health_overheat:I = 0x7f09009f

.field public static final battery_info_health_unknown:I = 0x7f09009d

.field public static final battery_info_health_unspecified_failure:I = 0x7f0900a2

.field public static final battery_info_label:I = 0x7f09013a

.field public static final battery_info_level_label:I = 0x7f090084

.field public static final battery_info_power_ac:I = 0x7f090098

.field public static final battery_info_power_ac_usb:I = 0x7f09009b

.field public static final battery_info_power_label:I = 0x7f090082

.field public static final battery_info_power_unknown:I = 0x7f09009c

.field public static final battery_info_power_unplugged:I = 0x7f090097

.field public static final battery_info_power_usb:I = 0x7f090099

.field public static final battery_info_power_wireless:I = 0x7f09009a

.field public static final battery_info_scale_label:I = 0x7f090083

.field public static final battery_info_screen_on:I = 0x7f09008e

.field public static final battery_info_status_charging:I = 0x7f090090

.field public static final battery_info_status_charging_ac:I = 0x7f090091

.field public static final battery_info_status_charging_usb:I = 0x7f090092

.field public static final battery_info_status_charging_wireless:I = 0x7f090093

.field public static final battery_info_status_discharging:I = 0x7f090094

.field public static final battery_info_status_full:I = 0x7f090096

.field public static final battery_info_status_label:I = 0x7f090081

.field public static final battery_info_status_not_charging:I = 0x7f090095

.field public static final battery_info_status_unknown:I = 0x7f09008f

.field public static final battery_info_technology_label:I = 0x7f090086

.field public static final battery_info_temperature_label:I = 0x7f090089

.field public static final battery_info_temperature_units:I = 0x7f09008a

.field public static final battery_info_uptime:I = 0x7f09008b

.field public static final battery_info_voltage_label:I = 0x7f090087

.field public static final battery_info_voltage_units:I = 0x7f090088

.field public static final battery_level_title:I = 0x7f090592

.field public static final battery_saving_msg:I = 0x7f090b83

.field public static final battery_saving_title:I = 0x7f090b82

.field public static final battery_since_reset:I = 0x7f0907a7

.field public static final battery_since_unplugged:I = 0x7f0907a6

.field public static final battery_stats_charging_label:I = 0x7f0907aa

.field public static final battery_stats_duration:I = 0x7f0907a9

.field public static final battery_stats_gps_on_label:I = 0x7f0907ac

.field public static final battery_stats_last_duration:I = 0x7f0907b0

.field public static final battery_stats_on_battery:I = 0x7f0907a8

.field public static final battery_stats_phone_signal_label:I = 0x7f0907af

.field public static final battery_stats_screen_on_label:I = 0x7f0907ab

.field public static final battery_stats_wake_lock_label:I = 0x7f0907ae

.field public static final battery_stats_wifi_running_label:I = 0x7f0907ad

.field public static final battery_status_title:I = 0x7f090591

.field public static final battery_sugg_apps_gps:I = 0x7f0907df

.field public static final battery_sugg_apps_info:I = 0x7f0907de

.field public static final battery_sugg_apps_settings:I = 0x7f0907e0

.field public static final battery_sugg_bluetooth_basic:I = 0x7f0907db

.field public static final battery_sugg_bluetooth_headset:I = 0x7f0907dc

.field public static final battery_sugg_display:I = 0x7f0907d7

.field public static final battery_sugg_radio:I = 0x7f0907d5

.field public static final battery_sugg_wifi:I = 0x7f0907d9

.field public static final bearer:I = 0x7f0905ad

.field public static final better_dont_mess_warning_message:I = 0x7f091273

.field public static final biometric_weak_improve_matching_title:I = 0x7f09016b

.field public static final biometric_weak_liveliness_summary:I = 0x7f09016d

.field public static final biometric_weak_liveliness_title:I = 0x7f09016c

.field public static final block_all_calls:I = 0x7f090e80

.field public static final block_all_messages:I = 0x7f090e84

.field public static final block_all_video_calls:I = 0x7f090e82

.field public static final block_all_voice_calls:I = 0x7f090e81

.field public static final block_number:I = 0x7f090e83

.field public static final block_number_and_phrases:I = 0x7f090e85

.field public static final block_settings_off:I = 0x7f090e86

.field public static final block_settings_title:I = 0x7f090e6f

.field public static final blocking_mode_menu_summary:I = 0x7f091263

.field public static final bluetooth:I = 0x7f0900a4

.field public static final bluetooth_a2dp_profile_summary_connected:I = 0x7f090234

.field public static final bluetooth_a2dp_profile_summary_use_for:I = 0x7f09023d

.field public static final bluetooth_abb:I = 0x7f0912e2

.field public static final bluetooth_accept:I = 0x7f0903bd

.field public static final bluetooth_advanced_settings:I = 0x7f0912e0

.field public static final bluetooth_advanced_titlebar:I = 0x7f09021f

.field public static final bluetooth_ask_discovery:I = 0x7f0900d0

.field public static final bluetooth_ask_enablement:I = 0x7f0900cf

.field public static final bluetooth_ask_enablement_and_discovery:I = 0x7f0900d2

.field public static final bluetooth_ask_enablement_and_lasting_discovery:I = 0x7f0900d3

.field public static final bluetooth_ask_lasting_discovery:I = 0x7f0900d1

.field public static final bluetooth_authorize_message:I = 0x7f090d33

.field public static final bluetooth_authorize_ticker:I = 0x7f090d31

.field public static final bluetooth_authorize_title:I = 0x7f090d32

.field public static final bluetooth_auto_connect:I = 0x7f0900d6

.field public static final bluetooth_available:I = 0x7f0900dd

.field public static final bluetooth_available_detailed:I = 0x7f0900de

.field public static final bluetooth_available_services_title:I = 0x7f090d6a

.field public static final bluetooth_button_retry:I = 0x7f090211

.field public static final bluetooth_class_of_device_default_value:I = 0x7f090d5e

.field public static final bluetooth_class_of_device_summary:I = 0x7f090d5d

.field public static final bluetooth_class_of_device_title:I = 0x7f090d5c

.field public static final bluetooth_confirm_just_works_passkey_msg:I = 0x7f090d34

.field public static final bluetooth_confirm_passkey_msg:I = 0x7f090203

.field public static final bluetooth_connect_specific_profiles_title:I = 0x7f090221

.field public static final bluetooth_connected:I = 0x7f0900bb

.field public static final bluetooth_connected_no_a2dp:I = 0x7f0900bd

.field public static final bluetooth_connected_no_headset:I = 0x7f0900bc

.field public static final bluetooth_connected_no_headset_no_a2dp:I = 0x7f0900be

.field public static final bluetooth_connecting:I = 0x7f0900c1

.field public static final bluetooth_connecting_error_message:I = 0x7f09020e

.field public static final bluetooth_connection_dialog_text:I = 0x7f0900d9

.field public static final bluetooth_connection_error_message:I = 0x7f090210

.field public static final bluetooth_connection_error_title:I = 0x7f09020f

.field public static final bluetooth_connection_notif_message:I = 0x7f0900d8

.field public static final bluetooth_connection_permission_request:I = 0x7f0900d7

.field public static final bluetooth_decline:I = 0x7f090d3e

.field public static final bluetooth_device:I = 0x7f0900c4

.field public static final bluetooth_device_advanced_enable_opp_title:I = 0x7f090233

.field public static final bluetooth_device_advanced_online_mode_summary:I = 0x7f090230

.field public static final bluetooth_device_advanced_online_mode_title:I = 0x7f09022f

.field public static final bluetooth_device_advanced_profile_header_title:I = 0x7f090231

.field public static final bluetooth_device_advanced_rename_device:I = 0x7f090232

.field public static final bluetooth_device_advanced_title:I = 0x7f09022e

.field public static final bluetooth_device_chooser:I = 0x7f090d1d

.field public static final bluetooth_device_context_connect:I = 0x7f090218

.field public static final bluetooth_device_context_connect_advanced:I = 0x7f09021d

.field public static final bluetooth_device_context_disconnect:I = 0x7f090219

.field public static final bluetooth_device_context_disconnect_unpair:I = 0x7f09021c

.field public static final bluetooth_device_context_pair_connect:I = 0x7f09021a

.field public static final bluetooth_device_context_unpair:I = 0x7f09021b

.field public static final bluetooth_device_details:I = 0x7f0900b1

.field public static final bluetooth_device_name:I = 0x7f0900b0

.field public static final bluetooth_device_paired:I = 0x7f090d7e

.field public static final bluetooth_devices:I = 0x7f0900af

.field public static final bluetooth_dialog_sap_deactivate:I = 0x7f090d7c

.field public static final bluetooth_disable_profile_message:I = 0x7f0900ba

.field public static final bluetooth_disable_profile_title:I = 0x7f0900b9

.field public static final bluetooth_disconnect_a2dp_profile:I = 0x7f090229

.field public static final bluetooth_disconnect_all_profiles:I = 0x7f0900b8

.field public static final bluetooth_disconnect_headset_profile:I = 0x7f09022a

.field public static final bluetooth_disconnect_hid_profile:I = 0x7f09022b

.field public static final bluetooth_disconnect_pan_nap_profile:I = 0x7f09022d

.field public static final bluetooth_disconnect_pan_user_profile:I = 0x7f09022c

.field public static final bluetooth_disconnect_title:I = 0x7f0900b7

.field public static final bluetooth_disconnected:I = 0x7f0900bf

.field public static final bluetooth_disconnecting:I = 0x7f0900c0

.field public static final bluetooth_discover_services:I = 0x7f090d6b

.field public static final bluetooth_display_move_to_my_place_msg:I = 0x7f090206

.field public static final bluetooth_display_passkey_pin_msg:I = 0x7f090205

.field public static final bluetooth_dock_settings:I = 0x7f090243

.field public static final bluetooth_dock_settings_a2dp:I = 0x7f090246

.field public static final bluetooth_dock_settings_headset:I = 0x7f090245

.field public static final bluetooth_dock_settings_remember:I = 0x7f090247

.field public static final bluetooth_dock_settings_title:I = 0x7f090244

.field public static final bluetooth_empty_list_bluetooth_off:I = 0x7f090220

.field public static final bluetooth_enable_alphanumeric_pin:I = 0x7f0901ff

.field public static final bluetooth_enter_keyboard_pin_msg:I = 0x7f090d6f

.field public static final bluetooth_enter_passkey_msg:I = 0x7f0901fe

.field public static final bluetooth_enter_passkey_other_device:I = 0x7f090202

.field public static final bluetooth_enter_pin_msg:I = 0x7f0901fd

.field public static final bluetooth_enter_pin_other_device:I = 0x7f090201

.field public static final bluetooth_error_title:I = 0x7f090209

.field public static final bluetooth_ftp_chg_dir:I = 0x7f090d45

.field public static final bluetooth_ftp_del_dir:I = 0x7f090d44

.field public static final bluetooth_ftp_del_file:I = 0x7f090d43

.field public static final bluetooth_ftp_get:I = 0x7f090d42

.field public static final bluetooth_ftp_mk_dir:I = 0x7f090d46

.field public static final bluetooth_ftp_put:I = 0x7f090d41

.field public static final bluetooth_ftp_server:I = 0x7f090d40

.field public static final bluetooth_headset_profile_summary_connected:I = 0x7f090235

.field public static final bluetooth_headset_profile_summary_use_for:I = 0x7f09023e

.field public static final bluetooth_help:I = 0x7f0900cc

.field public static final bluetooth_hid_advanced_online_mode_summary:I = 0x7f090d72

.field public static final bluetooth_hid_profile_summary_connected:I = 0x7f09023a

.field public static final bluetooth_hid_profile_summary_use_for:I = 0x7f090240

.field public static final bluetooth_incoming_pairing_msg:I = 0x7f090204

.field public static final bluetooth_is_discoverable:I = 0x7f0900a6

.field public static final bluetooth_is_discoverable_always:I = 0x7f0900a7

.field public static final bluetooth_lock_voice_dialing:I = 0x7f0900ad

.field public static final bluetooth_lock_voice_dialing_summary:I = 0x7f0900ae

.field public static final bluetooth_map_acceptance_dialog_text:I = 0x7f0900e0

.field public static final bluetooth_map_remember_choice:I = 0x7f0900e1

.field public static final bluetooth_map_request:I = 0x7f0900df

.field public static final bluetooth_menu_advanced:I = 0x7f09021e

.field public static final bluetooth_move_to_my_place:I = 0x7f0901fc

.field public static final bluetooth_name_max_error:I = 0x7f090ac7

.field public static final bluetooth_name_not_set:I = 0x7f0900b3

.field public static final bluetooth_no:I = 0x7f090d3d

.field public static final bluetooth_no_devices_found:I = 0x7f0900c7

.field public static final bluetooth_not_connected:I = 0x7f090d7f

.field public static final bluetooth_not_discoverable:I = 0x7f0900aa

.field public static final bluetooth_not_visible_to_other_devices:I = 0x7f0900a8

.field public static final bluetooth_notif_message:I = 0x7f0900ca

.field public static final bluetooth_notif_sap_message:I = 0x7f090d7b

.field public static final bluetooth_notif_sap_off:I = 0x7f090d79

.field public static final bluetooth_notif_sap_on:I = 0x7f090d78

.field public static final bluetooth_notif_sap_title:I = 0x7f090d7a

.field public static final bluetooth_notif_ticker:I = 0x7f0900c8

.field public static final bluetooth_notif_title:I = 0x7f0900c9

.field public static final bluetooth_off_error:I = 0x7f090d3f

.field public static final bluetooth_on_time:I = 0x7f0907b3

.field public static final bluetooth_only_visible_to_paired_devices:I = 0x7f0900a9

.field public static final bluetooth_operation_while_videocall:I = 0x7f090d77

.field public static final bluetooth_opp_profile_summary_connected:I = 0x7f090236

.field public static final bluetooth_opp_profile_summary_not_connected:I = 0x7f090237

.field public static final bluetooth_opp_profile_summary_use_for:I = 0x7f09023f

.field public static final bluetooth_ops_server:I = 0x7f090d4e

.field public static final bluetooth_pairing:I = 0x7f0900c3

.field public static final bluetooth_pairing_accept:I = 0x7f090207

.field public static final bluetooth_pairing_decline:I = 0x7f090208

.field public static final bluetooth_pairing_device_down_error_message:I = 0x7f09020c

.field public static final bluetooth_pairing_error_message:I = 0x7f09020a

.field public static final bluetooth_pairing_pin_error_message:I = 0x7f09020b

.field public static final bluetooth_pairing_rejected_error_message:I = 0x7f09020d

.field public static final bluetooth_pairing_request:I = 0x7f0901fb

.field public static final bluetooth_pan_nap_profile_summary_connected:I = 0x7f09023c

.field public static final bluetooth_pan_profile_summary_use_for:I = 0x7f090241

.field public static final bluetooth_pan_user_profile_summary_connected:I = 0x7f09023b

.field public static final bluetooth_pb_acceptance_dialog_text:I = 0x7f0900db

.field public static final bluetooth_pb_remember_choice:I = 0x7f0900dc

.field public static final bluetooth_pbs_pull_pb:I = 0x7f090d48

.field public static final bluetooth_pbs_pull_vcard_entry:I = 0x7f090d4b

.field public static final bluetooth_pbs_pull_vcard_list:I = 0x7f090d4a

.field public static final bluetooth_pbs_server:I = 0x7f090d47

.field public static final bluetooth_pbs_set_pb:I = 0x7f090d49

.field public static final bluetooth_permission_request:I = 0x7f0900ce

.field public static final bluetooth_phonebook_request:I = 0x7f0900da

.field public static final bluetooth_pin_values_hint:I = 0x7f090200

.field public static final bluetooth_pointing_device_connected_confirmation_text:I = 0x7f090d71

.field public static final bluetooth_pointing_device_connected_title:I = 0x7f090d70

.field public static final bluetooth_pr_profile_summary_connected:I = 0x7f090d51

.field public static final bluetooth_preference_device_settings:I = 0x7f090215

.field public static final bluetooth_preference_found_devices:I = 0x7f090217

.field public static final bluetooth_preference_paired_devices:I = 0x7f090216

.field public static final bluetooth_preference_scan_title:I = 0x7f090212

.field public static final bluetooth_profile_a2dp:I = 0x7f090222

.field public static final bluetooth_profile_details:I = 0x7f0900b2

.field public static final bluetooth_profile_headset:I = 0x7f090223

.field public static final bluetooth_profile_hid:I = 0x7f090225

.field public static final bluetooth_profile_opp:I = 0x7f090224

.field public static final bluetooth_profile_pan:I = 0x7f090227

.field public static final bluetooth_profile_pan_nap:I = 0x7f090228

.field public static final bluetooth_profile_pbap:I = 0x7f090d4c

.field public static final bluetooth_profile_printer:I = 0x7f090d4d

.field public static final bluetooth_profile_sap:I = 0x7f090226

.field public static final bluetooth_quick_toggle_summary:I = 0x7f0901f3

.field public static final bluetooth_quick_toggle_title:I = 0x7f0901f2

.field public static final bluetooth_remote_device:I = 0x7f090d24

.field public static final bluetooth_rename_button:I = 0x7f0900b6

.field public static final bluetooth_rename_device:I = 0x7f0900b5

.field public static final bluetooth_sap_connect_message:I = 0x7f0900e4

.field public static final bluetooth_sap_profile_summary_connected:I = 0x7f090238

.field public static final bluetooth_sap_profile_summary_not_connected:I = 0x7f090239

.field public static final bluetooth_sap_profile_summary_use_for:I = 0x7f090242

.field public static final bluetooth_sap_remember_choice:I = 0x7f0900e2

.field public static final bluetooth_sap_request:I = 0x7f0900e3

.field public static final bluetooth_sap_weak_linkkey:I = 0x7f090d76

.field public static final bluetooth_scan_device:I = 0x7f090d1e

.field public static final bluetooth_scan_for_devices:I = 0x7f0900b4

.field public static final bluetooth_search_for_devices:I = 0x7f090213

.field public static final bluetooth_searching_for_devices:I = 0x7f090214

.field public static final bluetooth_security_mode_disable:I = 0x7f090d38

.field public static final bluetooth_security_mode_handsfree:I = 0x7f090d39

.field public static final bluetooth_service_access_req_message:I = 0x7f090d20

.field public static final bluetooth_service_notif_title:I = 0x7f090d21

.field public static final bluetooth_services:I = 0x7f090d6c

.field public static final bluetooth_settings:I = 0x7f0901f4

.field public static final bluetooth_settings_my_device:I = 0x7f0901f7

.field public static final bluetooth_settings_summary:I = 0x7f0901f6

.field public static final bluetooth_settings_title:I = 0x7f0901f5

.field public static final bluetooth_show_received_files:I = 0x7f0900cb

.field public static final bluetooth_stop_scan:I = 0x7f090d1f

.field public static final bluetooth_summary_connected_to_a2dp_headset:I = 0x7f090d7d

.field public static final bluetooth_summary_connected_to_hid:I = 0x7f090d6e

.field public static final bluetooth_tether_checkbox_text:I = 0x7f0905ee

.field public static final bluetooth_tethering_available_subtext:I = 0x7f0905ef

.field public static final bluetooth_tethering_bluetooth_visibilityoff:I = 0x7f0905f6

.field public static final bluetooth_tethering_cannot_connect_while_connected_to_wifi:I = 0x7f0905f7

.field public static final bluetooth_tethering_device_connected_subtext:I = 0x7f0905f0

.field public static final bluetooth_tethering_devices_connected_subtext:I = 0x7f0905f1

.field public static final bluetooth_tethering_errored_subtext:I = 0x7f0905f3

.field public static final bluetooth_tethering_off_subtext:I = 0x7f0905f2

.field public static final bluetooth_tethering_overflow_error:I = 0x7f0905f4

.field public static final bluetooth_turning_off:I = 0x7f0900d5

.field public static final bluetooth_turning_on:I = 0x7f0900d4

.field public static final bluetooth_unknown:I = 0x7f0900c2

.field public static final bluetooth_untether_blank:I = 0x7f0905f5

.field public static final bluetooth_visibility:I = 0x7f0900a5

.field public static final bluetooth_visibility_timeout:I = 0x7f0900ab

.field public static final bluetooth_visibility_timeout_summary:I = 0x7f0900ac

.field public static final bluetooth_yes:I = 0x7f090d3c

.field public static final both_wallpaper:I = 0x7f0909ce

.field public static final bright_adjustment_panel_summary:I = 0x7f0910a7

.field public static final bright_adjustment_panel_title:I = 0x7f0910a6

.field public static final brightness:I = 0x7f0904dc

.field public static final brightness_level:I = 0x7f091040

.field public static final brightness_summary:I = 0x7f0904dd

.field public static final bssid_label:I = 0x7f090485

.field public static final bt_enable_cancel:I = 0x7f090d5a

.field public static final bt_enable_line1:I = 0x7f090d58

.field public static final bt_enable_line2:I = 0x7f090d59

.field public static final bt_enable_ok:I = 0x7f090d5b

.field public static final bt_enable_title:I = 0x7f090d57

.field public static final bt_error_btn_ok:I = 0x7f090d52

.field public static final bt_pref_bpp_svc:I = 0x7f090d67

.field public static final bt_pref_fm_svc:I = 0x7f090d64

.field public static final bt_pref_fmtx_svc:I = 0x7f090d65

.field public static final bt_pref_opp_svc:I = 0x7f090d66

.field public static final bt_pref_pbap_svc:I = 0x7f090d62

.field public static final bt_pref_sap_svc:I = 0x7f090d63

.field public static final bt_pref_svccfg:I = 0x7f090d61

.field public static final bt_pref_testmode_svc:I = 0x7f090d68

.field public static final bt_scan_on_active_call_disallowed:I = 0x7f090d6d

.field public static final bt_summary_connected:I = 0x7f0901fa

.field public static final bt_summary_disable:I = 0x7f0901f8

.field public static final bt_summary_enable:I = 0x7f0901f9

.field public static final btn_sync:I = 0x7f090edf

.field public static final btn_tmo:I = 0x7f090903

.field public static final bua_plus_contacts:I = 0x7f090ec0

.field public static final bua_plus_media:I = 0x7f090ec1

.field public static final bua_plus_media_only:I = 0x7f090ec2

.field public static final bua_plus_storage_available:I = 0x7f090ec7

.field public static final bua_plus_storage_calculating:I = 0x7f090ec3

.field public static final bua_plus_storage_error:I = 0x7f090ec9

.field public static final bua_plus_storage_error_detail:I = 0x7f090eca

.field public static final bua_plus_storage_subscribe:I = 0x7f090ec4

.field public static final bua_plus_storage_subscribe_detail:I = 0x7f090ec5

.field public static final bua_plus_storage_total:I = 0x7f090ec6

.field public static final bua_plus_storage_upgrade:I = 0x7f090ec8

.field public static final bua_plus_title:I = 0x7f090ebf

.field public static final bua_plus_upgrade_failed:I = 0x7f090ecb

.field public static final bugreport_in_power:I = 0x7f09073b

.field public static final bugreport_in_power_summary:I = 0x7f09073c

.field public static final build_number:I = 0x7f090544

.field public static final builtin_keyboard_settings_summary:I = 0x7f090735

.field public static final builtin_keyboard_settings_title:I = 0x7f090734

.field public static final cacbluetooth_title:I = 0x7f0901c5

.field public static final cache_header_label:I = 0x7f090669

.field public static final cache_size_label:I = 0x7f09066b

.field public static final cached:I = 0x7f0906c8

.field public static final call_block_list:I = 0x7f090e72

.field public static final call_block_list_summary:I = 0x7f090e73

.field public static final call_block_mode:I = 0x7f090e71

.field public static final call_block_title:I = 0x7f090e70

.field public static final call_settings:I = 0x7f090012

.field public static final call_settings_summary:I = 0x7f0905da

.field public static final call_settings_title:I = 0x7f0905d9

.field public static final cancel:I = 0x7f090143

.field public static final cancel_addnetwork:I = 0x7f0902c9

.field public static final cancel_backup:I = 0x7f091286

.field public static final cancel_button:I = 0x7f0903df

.field public static final cancel_sync_label:I = 0x7f090b57

.field public static final cannot_backup_due_to_not_enough_space:I = 0x7f0912a5

.field public static final cannot_use_card_without_NFC:I = 0x7f09125f

.field public static final cantDeactivate3LM:I = 0x7f090003

.field public static final cant_sync_dialog_message:I = 0x7f0908b1

.field public static final cant_sync_dialog_title:I = 0x7f0908b0

.field public static final carrier_enabled:I = 0x7f0905aa

.field public static final carrier_enabled_summaryOff:I = 0x7f0905ac

.field public static final carrier_enabled_summaryOn:I = 0x7f0905ab

.field public static final cdma_pay_as_you_go:I = 0x7f090f00

.field public static final cdma_security_settings_summary:I = 0x7f09017a

.field public static final cdrom_device:I = 0x7f090a35

.field public static final cdrom_device_summary:I = 0x7f090a36

.field public static final cell_broadcast_settings:I = 0x7f090a23

.field public static final cell_broadcast_settings_summary:I = 0x7f090a24

.field public static final change_admin:I = 0x7f091245

.field public static final change_sim_alert:I = 0x7f090a38

.field public static final change_sim_alert_summary:I = 0x7f090a39

.field public static final changing_mode_pe:I = 0x7f0911e0

.field public static final checkbox_notification_same_as_incoming_call:I = 0x7f0904a3

.field public static final choose_timezone:I = 0x7f0900e6

.field public static final chooser_wallpaper:I = 0x7f0909cf

.field public static final clear_activities:I = 0x7f09067d

.field public static final clear_cache_btn_text:I = 0x7f09066a

.field public static final clear_data_dlg_text:I = 0x7f090698

.field public static final clear_data_dlg_title:I = 0x7f090697

.field public static final clear_data_failed:I = 0x7f09069d

.field public static final clear_failed_dlg_text:I = 0x7f0906a1

.field public static final clear_failed_dlg_title:I = 0x7f0906a0

.field public static final clear_font_summary:I = 0x7f090077

.field public static final clear_font_title:I = 0x7f090076

.field public static final clear_user_data_text:I = 0x7f090678

.field public static final clock:I = 0x7f090a5a

.field public static final clock_options:I = 0x7f091007

.field public static final clock_position:I = 0x7f090a60

.field public static final clock_size:I = 0x7f091006

.field public static final clock_summary:I = 0x7f090a5b

.field public static final clock_weather_position:I = 0x7f090a5f

.field public static final clock_widget:I = 0x7f091005

.field public static final collect_diagnostics_summary:I = 0x7f090042

.field public static final collect_diagnostics_title:I = 0x7f090041

.field public static final color_blind_acyanopsia:I = 0x7f0907a1

.field public static final color_blind_deuteranopia:I = 0x7f0907a0

.field public static final color_blind_protanopia:I = 0x7f09079f

.field public static final color_blind_title:I = 0x7f090b51

.field public static final colorblind_use_popup_msg:I = 0x7f09079e

.field public static final colorblind_use_popup_title:I = 0x7f09079d

.field public static final common_zone_title:I = 0x7f09128a

.field public static final complete_info:I = 0x7f090f6f

.field public static final computing_size:I = 0x7f0906aa

.field public static final config_list_label:I = 0x7f090493

.field public static final config_wfc_dialog_intent:I = 0x7f09000a

.field public static final configure_input_method:I = 0x7f09072c

.field public static final confirm_new_backup_pw_prompt:I = 0x7f090984

.field public static final confirm_old_password:I = 0x7f090034

.field public static final confirm_password:I = 0x7f090035

.field public static final connect_error:I = 0x7f091234

.field public static final connect_starting:I = 0x7f091232

.field public static final connect_stopping:I = 0x7f091233

.field public static final connected_dialog_message:I = 0x7f090a31

.field public static final connection_name_title:I = 0x7f090f64

.field public static final connectivity_and_location:I = 0x7f090ede

.field public static final contact_vibration_title:I = 0x7f090daa

.field public static final content_description_hand:I = 0x7f091016

.field public static final content_text_1:I = 0x7f091129

.field public static final content_text_2:I = 0x7f09112a

.field public static final content_text_3:I = 0x7f09112b

.field public static final contents_type:I = 0x7f090b61

.field public static final contextual_page:I = 0x7f091056

.field public static final contextual_page_audio_dock_page:I = 0x7f091061

.field public static final contextual_page_car_cradle:I = 0x7f091059

.field public static final contextual_page_car_cradle_page:I = 0x7f09105e

.field public static final contextual_page_desk_cradle:I = 0x7f09105a

.field public static final contextual_page_desk_cradle_page:I = 0x7f09105f

.field public static final contextual_page_dock_page:I = 0x7f091062

.field public static final contextual_page_earphone:I = 0x7f091058

.field public static final contextual_page_earphone_page:I = 0x7f09105d

.field public static final contextual_page_roaming:I = 0x7f09105b

.field public static final contextual_page_roaming_page:I = 0x7f091060

.field public static final contextual_page_s_pen:I = 0x7f091057

.field public static final contextual_page_s_pen_page:I = 0x7f09105c

.field public static final contextual_page_summary:I = 0x7f091063

.field public static final contributors_title:I = 0x7f09061b

.field public static final control_bar:I = 0x7f090b4c

.field public static final controls_label:I = 0x7f09066c

.field public static final controls_subtitle:I = 0x7f0907b8

.field public static final copying_files:I = 0x7f091297

.field public static final copyright_title:I = 0x7f09061d

.field public static final country_code_toast:I = 0x7f090a4f

.field public static final cover:I = 0x7f0904cf

.field public static final create:I = 0x7f090049

.field public static final create_pattern:I = 0x7f090d9c

.field public static final credentials_configure_lock_screen_hint:I = 0x7f090840

.field public static final credentials_enabled:I = 0x7f09083f

.field public static final credentials_erased:I = 0x7f09083d

.field public static final credentials_install:I = 0x7f09082d

.field public static final credentials_install_gesture_explanation:I = 0x7f090834

.field public static final credentials_install_gesture_prompt:I = 0x7f090833

.field public static final credentials_install_summary:I = 0x7f09082e

.field public static final credentials_not_erased:I = 0x7f09083e

.field public static final credentials_old_password:I = 0x7f090837

.field public static final credentials_password_too_short:I = 0x7f090839

.field public static final credentials_reset:I = 0x7f09082f

.field public static final credentials_reset_hint:I = 0x7f090838

.field public static final credentials_reset_summary:I = 0x7f090830

.field public static final credentials_reset_warning:I = 0x7f09083b

.field public static final credentials_reset_warning_plural:I = 0x7f09083c

.field public static final credentials_title:I = 0x7f09082c

.field public static final credentials_unlock:I = 0x7f090835

.field public static final credentials_unlock_hint:I = 0x7f090836

.field public static final credentials_wrong_password:I = 0x7f09083a

.field public static final crypt_keeper_button_text:I = 0x7f090181

.field public static final crypt_keeper_button_text_dec:I = 0x7f090a93

.field public static final crypt_keeper_confirm_title:I = 0x7f090186

.field public static final crypt_keeper_confirm_title_dec:I = 0x7f090a92

.field public static final crypt_keeper_cooldown:I = 0x7f09018a

.field public static final crypt_keeper_decrypt_settings_title:I = 0x7f090a8d

.field public static final crypt_keeper_decrypt_summary:I = 0x7f090a8f

.field public static final crypt_keeper_decrypt_title:I = 0x7f090a8e

.field public static final crypt_keeper_desc:I = 0x7f090180

.field public static final crypt_keeper_desc_alarm:I = 0x7f091189

.field public static final crypt_keeper_desc_dec:I = 0x7f090a97

.field public static final crypt_keeper_dialog_need_password_message:I = 0x7f090185

.field public static final crypt_keeper_dialog_need_password_title:I = 0x7f090184

.field public static final crypt_keeper_encrypt_summary:I = 0x7f09017e

.field public static final crypt_keeper_encrypt_title:I = 0x7f09017d

.field public static final crypt_keeper_encrypted_summary:I = 0x7f09017f

.field public static final crypt_keeper_enter_password:I = 0x7f09018b

.field public static final crypt_keeper_failed_summary:I = 0x7f09018d

.field public static final crypt_keeper_failed_summary_dec:I = 0x7f09018f

.field public static final crypt_keeper_failed_title:I = 0x7f09018c

.field public static final crypt_keeper_failed_title_dec:I = 0x7f09018e

.field public static final crypt_keeper_final_desc:I = 0x7f090187

.field public static final crypt_keeper_final_desc_dec:I = 0x7f090a96

.field public static final crypt_keeper_low_charge_text:I = 0x7f090182

.field public static final crypt_keeper_preparing_description:I = 0x7f090a8c

.field public static final crypt_keeper_set_screen_lock_type:I = 0x7f090a8b

.field public static final crypt_keeper_settings_title:I = 0x7f09017c

.field public static final crypt_keeper_setup_description:I = 0x7f090189

.field public static final crypt_keeper_setup_description_dec:I = 0x7f090a95

.field public static final crypt_keeper_setup_title:I = 0x7f090188

.field public static final crypt_keeper_setup_title_dec:I = 0x7f090a94

.field public static final crypt_keeper_switch_input_method:I = 0x7f090192

.field public static final crypt_keeper_unplugged_text:I = 0x7f090183

.field public static final crypt_keeper_warning_reset_message:I = 0x7f090a98

.field public static final cryptkeeper_emergency_call:I = 0x7f090941

.field public static final cryptkeeper_return_to_call:I = 0x7f090942

.field public static final current_backup_pw_prompt:I = 0x7f090982

.field public static final current_input_method:I = 0x7f090727

.field public static final custom_vibrations:I = 0x7f090da4

.field public static final customizable_key_title:I = 0x7f090fc2

.field public static final customizedkey_assign_btn_text:I = 0x7f091150

.field public static final customizedkey_assign_shortcut:I = 0x7f09114a

.field public static final customizedkey_assign_summary:I = 0x7f09114e

.field public static final customizedkey_assign_summary2:I = 0x7f09114f

.field public static final customizedkey_assign_toast:I = 0x7f09114d

.field public static final customizedkey_reset_toast:I = 0x7f09114c

.field public static final customizedkey_settings_title:I = 0x7f09114b

.field public static final data_connection_message:I = 0x7f090f93

.field public static final data_connection_title:I = 0x7f090f94

.field public static final data_roaming_setting:I = 0x7f091187

.field public static final data_roaming_setting_disable_toast:I = 0x7f091186

.field public static final data_roaming_setting_enable_toast:I = 0x7f091185

.field public static final data_roaming_setting_text:I = 0x7f091188

.field public static final data_service_network:I = 0x7f091114

.field public static final data_service_network_changed_to_ps:I = 0x7f091147

.field public static final data_service_network_summary:I = 0x7f091115

.field public static final data_size_label:I = 0x7f090671

.field public static final data_transfer_mode_comfirm_text:I = 0x7f090eea

.field public static final data_transfer_mode_comfirm_title:I = 0x7f090ee9

.field public static final data_transfer_mode_off_failure_text:I = 0x7f090eee

.field public static final data_transfer_mode_off_failure_title:I = 0x7f090eed

.field public static final data_transfer_mode_on_failure_text:I = 0x7f090eec

.field public static final data_transfer_mode_on_failure_title:I = 0x7f090eeb

.field public static final data_transfer_mode_summary:I = 0x7f090ee8

.field public static final data_transfer_mode_title:I = 0x7f090ee7

.field public static final data_usage_alert_at_warning_title:I = 0x7f091050

.field public static final data_usage_app_restrict_background:I = 0x7f090920

.field public static final data_usage_app_restrict_background_summary:I = 0x7f090921

.field public static final data_usage_app_restrict_background_summary_disabled:I = 0x7f090922

.field public static final data_usage_app_restrict_dialog:I = 0x7f090924

.field public static final data_usage_app_restrict_dialog_title:I = 0x7f090923

.field public static final data_usage_app_restricted:I = 0x7f090901

.field public static final data_usage_app_settings:I = 0x7f09091f

.field public static final data_usage_auto_sync_off_dialog:I = 0x7f090929

.field public static final data_usage_auto_sync_off_dialog_title:I = 0x7f090928

.field public static final data_usage_auto_sync_on_dialog:I = 0x7f090927

.field public static final data_usage_auto_sync_on_dialog_title:I = 0x7f090926

.field public static final data_usage_change_cycle:I = 0x7f0908fc

.field public static final data_usage_cycle:I = 0x7f0908f4

.field public static final data_usage_cycle_editor_positive:I = 0x7f09092c

.field public static final data_usage_cycle_editor_subtitle:I = 0x7f09092b

.field public static final data_usage_cycle_editor_title:I = 0x7f09092a

.field public static final data_usage_disable_3g_limit:I = 0x7f09090b

.field public static final data_usage_disable_4g_limit:I = 0x7f09090a

.field public static final data_usage_disable_mobile:I = 0x7f090905

.field public static final data_usage_disable_mobile_limit:I = 0x7f090909

.field public static final data_usage_disable_mobile_limit_vzw:I = 0x7f091053

.field public static final data_usage_disable_mobile_vzw_1:I = 0x7f090906

.field public static final data_usage_disable_mobile_vzw_2:I = 0x7f090907

.field public static final data_usage_disable_mobile_warning_ktt:I = 0x7f09090e

.field public static final data_usage_disable_mobile_warning_lgt:I = 0x7f09090f

.field public static final data_usage_disable_mobile_warning_skt:I = 0x7f09090d

.field public static final data_usage_disable_wifi_limit:I = 0x7f09090c

.field public static final data_usage_disclaimer:I = 0x7f090902

.field public static final data_usage_disclaimer_tmo:I = 0x7f090904

.field public static final data_usage_empty:I = 0x7f0908fe

.field public static final data_usage_enable_3g:I = 0x7f09091d

.field public static final data_usage_enable_4g:I = 0x7f09091e

.field public static final data_usage_enable_confirmation:I = 0x7f090910

.field public static final data_usage_enable_mobile:I = 0x7f09091c

.field public static final data_usage_enable_mobile_warning:I = 0x7f090914

.field public static final data_usage_enable_mobile_warning_ktt:I = 0x7f090912

.field public static final data_usage_enable_mobile_warning_lgt:I = 0x7f090913

.field public static final data_usage_enable_mobile_warning_skt:I = 0x7f090911

.field public static final data_usage_label_background:I = 0x7f090900

.field public static final data_usage_label_foreground:I = 0x7f0908ff

.field public static final data_usage_limit_dialog:I = 0x7f090930

.field public static final data_usage_limit_dialog_mobile:I = 0x7f090931

.field public static final data_usage_limit_dialog_title:I = 0x7f09092f

.field public static final data_usage_limit_dialog_title_vzw:I = 0x7f091054

.field public static final data_usage_limit_dialog_vzw:I = 0x7f091055

.field public static final data_usage_limit_editor_title:I = 0x7f09092e

.field public static final data_usage_list_mobile:I = 0x7f09091a

.field public static final data_usage_list_none:I = 0x7f09091b

.field public static final data_usage_menu_auto_sync:I = 0x7f0908fb

.field public static final data_usage_menu_metered:I = 0x7f0908fa

.field public static final data_usage_menu_restrict_background:I = 0x7f0908f6

.field public static final data_usage_menu_roaming:I = 0x7f0908f5

.field public static final data_usage_menu_show_ethernet:I = 0x7f0908f9

.field public static final data_usage_menu_show_wifi:I = 0x7f0908f8

.field public static final data_usage_menu_split_4g:I = 0x7f0908f7

.field public static final data_usage_metered_body:I = 0x7f09093d

.field public static final data_usage_metered_mobile:I = 0x7f09093e

.field public static final data_usage_metered_title:I = 0x7f09093c

.field public static final data_usage_metered_wifi:I = 0x7f09093f

.field public static final data_usage_metered_wifi_disabled:I = 0x7f090940

.field public static final data_usage_pick_cycle_day:I = 0x7f0908fd

.field public static final data_usage_received_sent:I = 0x7f090939

.field public static final data_usage_restrict_background:I = 0x7f090933

.field public static final data_usage_restrict_background_multiuser:I = 0x7f090934

.field public static final data_usage_restrict_background_title:I = 0x7f090932

.field public static final data_usage_restrict_denied_dialog:I = 0x7f090925

.field public static final data_usage_summary_title:I = 0x7f0908f3

.field public static final data_usage_sweep_limit:I = 0x7f090936

.field public static final data_usage_sweep_warning:I = 0x7f090935

.field public static final data_usage_tab_3g:I = 0x7f090919

.field public static final data_usage_tab_4g:I = 0x7f090918

.field public static final data_usage_tab_ethernet:I = 0x7f090916

.field public static final data_usage_tab_mobile:I = 0x7f090917

.field public static final data_usage_tab_wifi:I = 0x7f090915

.field public static final data_usage_total_during_range:I = 0x7f09093a

.field public static final data_usage_total_during_range_mobile:I = 0x7f09093b

.field public static final data_usage_uninstalled_apps:I = 0x7f090937

.field public static final data_usage_uninstalled_apps_users:I = 0x7f090938

.field public static final data_usage_warning_editor_title:I = 0x7f09092d

.field public static final data_usage_warning_popup_content:I = 0x7f091052

.field public static final data_usage_warning_popup_title:I = 0x7f091051

.field public static final date_and_time:I = 0x7f0900e5

.field public static final date_and_time_settings_automode_reboot:I = 0x7f09015e

.field public static final date_and_time_settings_confirm_reboot:I = 0x7f09015d

.field public static final date_and_time_settings_summary:I = 0x7f090158

.field public static final date_and_time_settings_title:I = 0x7f090156

.field public static final date_and_time_settings_title_setup_wizard:I = 0x7f090157

.field public static final date_picker_title:I = 0x7f090169

.field public static final date_time_12_hour_sample:I = 0x7f0912c4

.field public static final date_time_24_hour_sample:I = 0x7f0912c5

.field public static final date_time_24hour:I = 0x7f090162

.field public static final date_time_auto:I = 0x7f090159

.field public static final date_time_auto_summaryOff:I = 0x7f09015b

.field public static final date_time_auto_summaryOn:I = 0x7f09015a

.field public static final date_time_date_format:I = 0x7f090166

.field public static final date_time_set_date:I = 0x7f090165

.field public static final date_time_set_time:I = 0x7f090163

.field public static final date_time_set_timezone:I = 0x7f090164

.field public static final daydream_warning_msg:I = 0x7f090a85

.field public static final debug_app:I = 0x7f0908bf

.field public static final debug_app_not_set:I = 0x7f0908c0

.field public static final debug_app_set:I = 0x7f0908c1

.field public static final debug_applications_category:I = 0x7f0908ed

.field public static final debug_debugging_category:I = 0x7f0908be

.field public static final debug_drawing_category:I = 0x7f0908c7

.field public static final debug_input_category:I = 0x7f0908c6

.field public static final debug_intent_sender_label:I = 0x7f090138

.field public static final debug_layout:I = 0x7f0908df

.field public static final debug_layout_summary:I = 0x7f0908e0

.field public static final debug_monitoring_category:I = 0x7f0908c8

.field public static final debugging_dialog_message:I = 0x7f090a30

.field public static final debugging_dialog_title:I = 0x7f090a2f

.field public static final decrypt_title:I = 0x7f090a90

.field public static final default_date_format:I = 0x7f0900e7

.field public static final default_passwd:I = 0x7f09121b

.field public static final default_pattern:I = 0x7f090da8

.field public static final default_sms_message:I = 0x7f090a51

.field public static final del_auto_ap:I = 0x7f090f71

.field public static final del_items:I = 0x7f090f73

.field public static final delete:I = 0x7f0908b7

.field public static final delete_admin:I = 0x7f091243

.field public static final delete_pattern:I = 0x7f090da7

.field public static final deny:I = 0x7f09004b

.field public static final description_play_button:I = 0x7f091103

.field public static final desk_cradle_enable_popup_summary:I = 0x7f0909e4

.field public static final desk_cradle_enable_popup_title:I = 0x7f0909e3

.field public static final desk_cradle_summary:I = 0x7f0909e2

.field public static final desk_cradle_title:I = 0x7f0909e1

.field public static final desk_home_display_warning_msg:I = 0x7f090a87

.field public static final desk_home_display_warning_title:I = 0x7f090a86

.field public static final desk_home_screen_display:I = 0x7f090e95

.field public static final desk_home_screen_display_summary:I = 0x7f090e96

.field public static final dest_path_label:I = 0x7f09128f

.field public static final dest_size_label:I = 0x7f091290

.field public static final details_subtitle:I = 0x7f0907b7

.field public static final details_title:I = 0x7f0907b6

.field public static final dev_settings_warning_message:I = 0x7f090746

.field public static final dev_settings_warning_title:I = 0x7f090745

.field public static final development_settings_summary:I = 0x7f090738

.field public static final development_settings_title:I = 0x7f090736

.field public static final development_settings_title_orange:I = 0x7f090737

.field public static final development_warning_msg:I = 0x7f090a70

.field public static final device_admin_add_title:I = 0x7f09085c

.field public static final device_admin_settings_title:I = 0x7f090855

.field public static final device_admin_status:I = 0x7f09085e

.field public static final device_admin_title:I = 0x7f0901ef

.field public static final device_admin_warning:I = 0x7f09085d

.field public static final device_help_motion_title:I = 0x7f090c3a

.field public static final device_info_default:I = 0x7f09004c

.field public static final device_info_label:I = 0x7f090139

.field public static final device_info_not_available:I = 0x7f090546

.field public static final device_info_settings:I = 0x7f09053a

.field public static final device_lock_time:I = 0x7f090027

.field public static final device_lock_time_noti:I = 0x7f09002a

.field public static final device_lock_time_powerkey_summary:I = 0x7f090029

.field public static final device_lock_time_summary:I = 0x7f090028

.field public static final device_manager:I = 0x7f09125c

.field public static final device_name:I = 0x7f09002c

.field public static final device_picker:I = 0x7f0900cd

.field public static final device_picker_send:I = 0x7f090425

.field public static final device_picker_title:I = 0x7f090423

.field public static final device_status:I = 0x7f090548

.field public static final device_status_activity_title:I = 0x7f090547

.field public static final device_status_summary:I = 0x7f090549

.field public static final diagnostics_and_usage_title:I = 0x7f090de7

.field public static final dialogMessage:I = 0x7f09002d

.field public static final dialog_battery_data_warning:I = 0x7f0903bc

.field public static final dialog_hotspot_button_accept:I = 0x7f09104f

.field public static final dialog_hotspot_button_continue:I = 0x7f090380

.field public static final dialog_max_client_change_warning:I = 0x7f0903b0

.field public static final dialog_tethering_warning:I = 0x7f09104e

.field public static final dialog_title_font_size:I = 0x7f0904ef

.field public static final direct_share_settings:I = 0x7f090faa

.field public static final direct_share_settings_summary:I = 0x7f090fab

.field public static final disableNetwork:I = 0x7f090479

.field public static final disable_alarm_and_timer_title:I = 0x7f09108d

.field public static final disable_debug_summary:I = 0x7f091013

.field public static final disable_debug_title:I = 0x7f091012

.field public static final disable_hovering_summary:I = 0x7f09078a

.field public static final disable_hovering_title:I = 0x7f090789

.field public static final disable_incoming_calls_title:I = 0x7f09108b

.field public static final disable_led_indicator_title:I = 0x7f09108e

.field public static final disable_notifications_title:I = 0x7f09108c

.field public static final disable_or_not:I = 0x7f090f74

.field public static final disable_overlays:I = 0x7f0908d7

.field public static final disable_overlays_summary:I = 0x7f0908d8

.field public static final disable_talkback_to_enable_air_motion_message:I = 0x7f090c4d

.field public static final disable_talkback_to_enable_air_motion_title:I = 0x7f090c4c

.field public static final disable_text:I = 0x7f090676

.field public static final disabled:I = 0x7f090691

.field public static final disagree:I = 0x7f090edd

.field public static final discard_changes:I = 0x7f090f68

.field public static final disconnect:I = 0x7f09047b

.field public static final disconnect_network_message:I = 0x7f09112f

.field public static final disconnect_network_title:I = 0x7f09112e

.field public static final display_battery_percentage:I = 0x7f090d96

.field public static final display_battery_percentage_summary:I = 0x7f090d97

.field public static final display_font_size_label:I = 0x7f0900ea

.field public static final display_label:I = 0x7f09013b

.field public static final display_order_text:I = 0x7f090758

.field public static final display_preview_label:I = 0x7f0900e9

.field public static final display_saving:I = 0x7f0909ad

.field public static final display_saving_mode_summary:I = 0x7f0909ae

.field public static final display_settings:I = 0x7f0904d8

.field public static final display_settings_title:I = 0x7f090494

.field public static final dlg_cancel:I = 0x7f09069a

.field public static final dlg_confirm_unmount_text:I = 0x7f090580

.field public static final dlg_confirm_unmount_title:I = 0x7f09057f

.field public static final dlg_error_unmount_text:I = 0x7f090582

.field public static final dlg_error_unmount_title:I = 0x7f090581

.field public static final dlg_ok:I = 0x7f090699

.field public static final dmr_access_control_always_accept:I = 0x7f0911ef

.field public static final dmr_access_control_always_ask:I = 0x7f0911f0

.field public static final dmr_access_control_always_reject:I = 0x7f0911f1

.field public static final dmr_access_control_list:I = 0x7f0911ee

.field public static final dmr_allowed_device_list:I = 0x7f0911e7

.field public static final dmr_allowed_device_summary:I = 0x7f0911e8

.field public static final dmr_denied_device_list:I = 0x7f0911e9

.field public static final dmr_denied_device_summary:I = 0x7f0911ea

.field public static final dmr_device_name:I = 0x7f0911e6

.field public static final dmr_file_data_transfer:I = 0x7f0911e5

.field public static final dmr_onoff_title:I = 0x7f0911e4

.field public static final dmr_popup_no_deivce_msg:I = 0x7f0911ec

.field public static final dmr_popup_save:I = 0x7f0911eb

.field public static final dmr_popup_wifi_connect:I = 0x7f0911ed

.field public static final dmr_title:I = 0x7f0911e3

.field public static final do_not_disturb:I = 0x7f090f12

.field public static final do_not_show:I = 0x7f090a41

.field public static final do_not_show_again:I = 0x7f0909ec

.field public static final do_not_show_me_this_again:I = 0x7f090ed7

.field public static final dock_audio_settings_title:I = 0x7f0904c5

.field public static final dock_audio_summary_car:I = 0x7f0904c7

.field public static final dock_audio_summary_desk:I = 0x7f0904c6

.field public static final dock_audio_summary_none:I = 0x7f0904c8

.field public static final dock_audio_summary_unknown:I = 0x7f0904c9

.field public static final dock_not_found_text:I = 0x7f0904cb

.field public static final dock_not_found_title:I = 0x7f0904ca

.field public static final dock_settings:I = 0x7f0904c3

.field public static final dock_settings_summary:I = 0x7f0904a8

.field public static final dock_settings_title:I = 0x7f0904c4

.field public static final dock_sounds_enable_summary_off:I = 0x7f0904ce

.field public static final dock_sounds_enable_summary_on:I = 0x7f0904cd

.field public static final dock_sounds_enable_title:I = 0x7f0904cc

.field public static final docomocloud_settings:I = 0x7f090f24

.field public static final docomoservice_settings:I = 0x7f090f23

.field public static final does_not_exist:I = 0x7f0906b3

.field public static final done:I = 0x7f091121

.field public static final dormant_mode:I = 0x7f091087

.field public static final dormant_mode_add:I = 0x7f091098

.field public static final dormant_mode_am:I = 0x7f091096

.field public static final dormant_mode_enabled:I = 0x7f09109a

.field public static final dormant_mode_help:I = 0x7f091088

.field public static final dormant_mode_help_nocall:I = 0x7f091089

.field public static final dormant_mode_off_message:I = 0x7f09109b

.field public static final dormant_mode_on:I = 0x7f091099

.field public static final dormant_mode_pm:I = 0x7f091097

.field public static final double_tap_summary:I = 0x7f090bef

.field public static final double_tap_title:I = 0x7f090be6

.field public static final double_tap_tutorial_content:I = 0x7f090c09

.field public static final double_tap_tutorial_title:I = 0x7f090c13

.field public static final download:I = 0x7f09117e

.field public static final download_cancel:I = 0x7f09117f

.field public static final download_fail:I = 0x7f091181

.field public static final download_latest:I = 0x7f091183

.field public static final download_ringtones:I = 0x7f090f7a

.field public static final download_success:I = 0x7f091180

.field public static final download_unsuccess:I = 0x7f091182

.field public static final download_wallpapers:I = 0x7f090f79

.field public static final driving:I = 0x7f090dee

.field public static final driving_mode_category_title:I = 0x7f0909a6

.field public static final driving_mode_settings_summary:I = 0x7f0909d1

.field public static final driving_mode_settings_title:I = 0x7f0909d0

.field public static final driving_mode_summary:I = 0x7f0909a7

.field public static final driving_mode_summary_without_call:I = 0x7f0909a8

.field public static final driving_mode_title:I = 0x7f0909a5

.field public static final driving_mode_to_enable_driving_mode_message:I = 0x7f0909a9

.field public static final dsa_do_not_show_again:I = 0x7f09113b

.field public static final dsa_dual_sim_always_on:I = 0x7f091136

.field public static final dsa_dual_sim_always_on_summary:I = 0x7f091138

.field public static final dsa_dual_sim_always_on_swa:I = 0x7f091137

.field public static final dsa_enable_dual_sim_always_on:I = 0x7f09113c

.field public static final dsa_enable_dual_sim_always_on_swa:I = 0x7f09113d

.field public static final dsa_help_msg_1:I = 0x7f091142

.field public static final dsa_help_msg_2:I = 0x7f091143

.field public static final dsa_help_msg_3:I = 0x7f091144

.field public static final dsa_help_msg_4:I = 0x7f091145

.field public static final dsa_help_msg_5:I = 0x7f091146

.field public static final dsa_init_inform:I = 0x7f091139

.field public static final dsa_init_inform_not_initial:I = 0x7f09113e

.field public static final dsa_init_inform_swa:I = 0x7f09113a

.field public static final dtmf_tone_enable_summary_off:I = 0x7f0904ab

.field public static final dtmf_tone_enable_summary_on:I = 0x7f0904aa

.field public static final dtmf_tone_enable_title:I = 0x7f0904a9

.field public static final dual_standby_management:I = 0x7f090ee4

.field public static final dualclock_settings_summary:I = 0x7f090c47

.field public static final dualclock_settings_title:I = 0x7f090d02

.field public static final earset:I = 0x7f091046

.field public static final easy_access_category:I = 0x7f090b40

.field public static final easy_mode:I = 0x7f090fc9

.field public static final easy_mode_alert_dialog_body:I = 0x7f090fcb

.field public static final easy_mode_feature:I = 0x7f090011

.field public static final easy_mode_off_alert_dialog_body:I = 0x7f090fcd

.field public static final easy_mode_off_alert_dialog_title:I = 0x7f090fcc

.field public static final easy_mode_starting:I = 0x7f090fca

.field public static final easy_mode_summary:I = 0x7f090fc8

.field public static final easy_mode_title:I = 0x7f090fde

.field public static final easymode_help_1:I = 0x7f090fce

.field public static final easymode_help_2:I = 0x7f090fd3

.field public static final easymode_help_apps:I = 0x7f090fcf

.field public static final easymode_help_clock_alarm:I = 0x7f090fd1

.field public static final easymode_help_contacts:I = 0x7f090fd0

.field public static final easymode_help_dialer:I = 0x7f090fd2

.field public static final edit_screen_capture_summary:I = 0x7f0911c5

.field public static final edit_screen_capture_title:I = 0x7f0911c4

.field public static final edm_policy_not_allow_open_wifiap:I = 0x7f090476

.field public static final eight:I = 0x7f09003d

.field public static final emergency_tone_summary:I = 0x7f090842

.field public static final emergency_tone_title:I = 0x7f090841

.field public static final empty:I = 0x7f09129e

.field public static final emptyMessage:I = 0x7f09002e

.field public static final empty_list_msg:I = 0x7f0906ac

.field public static final empty_message_toast:I = 0x7f090a50

.field public static final enableNetwork:I = 0x7f09047a

.field public static final enable_adb:I = 0x7f090739

.field public static final enable_adb_summary:I = 0x7f09073a

.field public static final enable_compatibility:I = 0x7f090680

.field public static final enable_motion_to_try_air_call_accept:I = 0x7f090c8b

.field public static final enable_motion_to_try_air_clip:I = 0x7f090c8a

.field public static final enable_motion_to_try_air_move:I = 0x7f090c89

.field public static final enable_motion_to_try_air_scroll:I = 0x7f090c87

.field public static final enable_motion_to_try_air_turn:I = 0x7f090c88

.field public static final enable_motion_to_try_quick_glance:I = 0x7f090c86

.field public static final enable_opengl_traces_title:I = 0x7f0908de

.field public static final enable_text:I = 0x7f090677

.field public static final enable_traces_dialog_title:I = 0x7f0908da

.field public static final enable_traces_summary_all:I = 0x7f0908dd

.field public static final enable_traces_summary_none:I = 0x7f0908db

.field public static final enable_traces_summary_num:I = 0x7f0908dc

.field public static final enable_traces_title:I = 0x7f0908d9

.field public static final enabling_progress_content:I = 0x7f090d60

.field public static final enabling_progress_title:I = 0x7f090d5f

.field public static final encrypt_option_memory_space:I = 0x7f090a9b

.field public static final encrypt_tablet_fast_encryption:I = 0x7f090a9a

.field public static final encrypt_tablet_require_password:I = 0x7f090a99

.field public static final enforce_read_external_confirm_message:I = 0x7f09074c

.field public static final enforce_read_external_confirm_title:I = 0x7f09074b

.field public static final enforce_read_external_summary:I = 0x7f09074a

.field public static final enforce_read_external_title:I = 0x7f090749

.field public static final enter_password:I = 0x7f0908b4

.field public static final epush_settings:I = 0x7f09118b

.field public static final erase_external_storage:I = 0x7f0905c1

.field public static final erase_external_storage_description:I = 0x7f0905c3

.field public static final erase_external_storage_description_sd:I = 0x7f0905c4

.field public static final erase_external_storage_sd:I = 0x7f0905c2

.field public static final error_apn_empty:I = 0x7f0905b5

.field public static final error_mcc_not3:I = 0x7f0905b6

.field public static final error_mnc_not23:I = 0x7f0905b7

.field public static final error_name_empty:I = 0x7f0905b4

.field public static final error_saving:I = 0x7f0902cb

.field public static final error_starting_wimax:I = 0x7f0911a8

.field public static final error_stopping_wimax:I = 0x7f0911a9

.field public static final error_title:I = 0x7f0905b3

.field public static final error_unavailable_apn:I = 0x7f091070

.field public static final eth_con_type:I = 0x7f091224

.field public static final eth_con_type_dhcp:I = 0x7f091225

.field public static final eth_con_type_manual:I = 0x7f091226

.field public static final eth_conf_perf_title:I = 0x7f09122d

.field public static final eth_conf_summary:I = 0x7f09122e

.field public static final eth_config_title:I = 0x7f091222

.field public static final eth_connected_ipaddr:I = 0x7f091235

.field public static final eth_dev_list:I = 0x7f091223

.field public static final eth_dns:I = 0x7f091227

.field public static final eth_gw:I = 0x7f091228

.field public static final eth_ipaddr:I = 0x7f091229

.field public static final eth_mask:I = 0x7f09122f

.field public static final eth_quick_toggle_summary:I = 0x7f09122b

.field public static final eth_quick_toggle_title:I = 0x7f09122a

.field public static final eth_radio_ctrl_title:I = 0x7f09122c

.field public static final eth_setting:I = 0x7f091220

.field public static final eth_settings_summary:I = 0x7f091221

.field public static final eth_toggle_summary_off:I = 0x7f091230

.field public static final eth_toggle_summary_on:I = 0x7f091231

.field public static final exit_setup:I = 0x7f090f9e

.field public static final external_code_size_label:I = 0x7f090670

.field public static final external_data_size_label:I = 0x7f090672

.field public static final external_sd_memory_unmounted:I = 0x7f090b2d

.field public static final external_storage_label:I = 0x7f091288

.field public static final external_storage_title:I = 0x7f0912be

.field public static final face_lock_warning:I = 0x7f090a6e

.field public static final face_voice_lock_warning:I = 0x7f090a6f

.field public static final facebook_auto_refresh_title:I = 0x7f090b69

.field public static final facebook_cancel:I = 0x7f090b6a

.field public static final facebook_do_not_show_agagin:I = 0x7f090b6d

.field public static final facebook_install_popup_msg:I = 0x7f090b68

.field public static final facebook_install_popup_title:I = 0x7f090b67

.field public static final facebook_noti_of_charges:I = 0x7f090b6b

.field public static final facebook_noti_of_charges_msg:I = 0x7f090b6c

.field public static final fail:I = 0x7f091149

.field public static final failed_to_connect_to_server:I = 0x7f0908a4

.field public static final failed_to_open_app_settings_toast:I = 0x7f0906f8

.field public static final fast_power_on_summary:I = 0x7f090eab

.field public static final fast_power_on_title:I = 0x7f090eaa

.field public static final favorite_notification_title:I = 0x7f09116f

.field public static final favorite_settings:I = 0x7f090fa2

.field public static final fcc_equipment_id:I = 0x7f09053f

.field public static final feature_settings:I = 0x7f090015

.field public static final feature_settings_category:I = 0x7f090a76

.field public static final feature_settings_summary:I = 0x7f090016

.field public static final features_category_title:I = 0x7f09108a

.field public static final file:I = 0x7f091298

.field public static final file_share_not_available_message:I = 0x7f090424

.field public static final filter:I = 0x7f09068b

.field public static final filter_apps_all:I = 0x7f09068d

.field public static final filter_apps_onsdcard:I = 0x7f090690

.field public static final filter_apps_running:I = 0x7f09068f

.field public static final filter_apps_third_party:I = 0x7f09068e

.field public static final filter_dlg_title:I = 0x7f09068c

.field public static final find_my_mobile:I = 0x7f090a37

.field public static final finger_air_view_additional_feedback_title:I = 0x7f090cd7

.field public static final finger_air_view_disabled_message:I = 0x7f090cee

.field public static final finger_air_view_disabled_title:I = 0x7f090ced

.field public static final finger_air_view_exclusive_option_talkback:I = 0x7f090cf8

.field public static final finger_air_view_exclusive_options_popup_content:I = 0x7f090cf7

.field public static final finger_air_view_full_text_summary:I = 0x7f090cd6

.field public static final finger_air_view_full_text_title:I = 0x7f090cd5

.field public static final finger_air_view_help_information_priview_content:I = 0x7f090ce1

.field public static final finger_air_view_help_information_priview_gallery_content:I = 0x7f090ce7

.field public static final finger_air_view_help_information_priview_gallery_title:I = 0x7f090ce6

.field public static final finger_air_view_help_information_priview_message_content:I = 0x7f090ce9

.field public static final finger_air_view_help_information_priview_message_title:I = 0x7f090ce8

.field public static final finger_air_view_help_information_priview_splanner_content:I = 0x7f090ce4

.field public static final finger_air_view_help_information_priview_splanner_content_calendar:I = 0x7f090ce5

.field public static final finger_air_view_help_information_priview_splanner_title:I = 0x7f090ce2

.field public static final finger_air_view_help_information_priview_splanner_title_calendar:I = 0x7f090ce3

.field public static final finger_air_view_help_magnifier_content:I = 0x7f090cec

.field public static final finger_air_view_help_progress_bar_preview_content:I = 0x7f090cea

.field public static final finger_air_view_help_speed_dial_tip_content:I = 0x7f090ceb

.field public static final finger_air_view_help_title:I = 0x7f090ce0

.field public static final finger_air_view_highlight_summary:I = 0x7f090cce

.field public static final finger_air_view_highlight_title:I = 0x7f090ccd

.field public static final finger_air_view_icon_labels_title:I = 0x7f090cfb

.field public static final finger_air_view_information_preview_summary:I = 0x7f090cd4

.field public static final finger_air_view_information_preview_title:I = 0x7f090cd3

.field public static final finger_air_view_magnifier_summary:I = 0x7f090cd0

.field public static final finger_air_view_magnifier_title:I = 0x7f090ccf

.field public static final finger_air_view_pointer_summary:I = 0x7f090cdb

.field public static final finger_air_view_pointer_title:I = 0x7f090cda

.field public static final finger_air_view_progress_bar_preview_summary:I = 0x7f090cdd

.field public static final finger_air_view_progress_bar_preview_title:I = 0x7f090cdc

.field public static final finger_air_view_setting_guide:I = 0x7f090ccc

.field public static final finger_air_view_show_up_indicator_summary:I = 0x7f090cd2

.field public static final finger_air_view_show_up_indicator_title:I = 0x7f090cd1

.field public static final finger_air_view_sound_and_haptic_feedback_summary:I = 0x7f090cd9

.field public static final finger_air_view_sound_and_haptic_feedback_title:I = 0x7f090cd8

.field public static final finger_air_view_speed_dial_tip_summary:I = 0x7f090cdf

.field public static final finger_air_view_speed_dial_tip_title:I = 0x7f090cde

.field public static final finger_air_view_title:I = 0x7f090cca

.field public static final finger_air_view_try_information_preview:I = 0x7f090cef

.field public static final finger_air_view_try_magnifier:I = 0x7f090cf2

.field public static final finger_air_view_try_progress_bar_preview:I = 0x7f090cf0

.field public static final finger_air_view_try_speed_dial_tip:I = 0x7f090cf1

.field public static final finger_air_view_turn_on_information_preview_message:I = 0x7f090cf3

.field public static final finger_air_view_turn_on_magnifier_message:I = 0x7f090cf6

.field public static final finger_air_view_turn_on_progress_bar_preview_message:I = 0x7f090cf4

.field public static final finger_air_view_turn_on_speed_dial_tip_message:I = 0x7f090cf5

.field public static final finish_button_label:I = 0x7f0908aa

.field public static final firewall:I = 0x7f0911f3

.field public static final firmware_version:I = 0x7f09053d

.field public static final five:I = 0x7f09003a

.field public static final flash_battery_low_msg:I = 0x7f090024

.field public static final flash_need_motion_popup:I = 0x7f090022

.field public static final flash_need_motion_popup_title:I = 0x7f090023

.field public static final flash_notification:I = 0x7f09109d

.field public static final flash_notification_summary:I = 0x7f09109e

.field public static final flight_error_msg:I = 0x7f090d54

.field public static final flight_error_title:I = 0x7f090d53

.field public static final flight_mode_disable_popup:I = 0x7f090a67

.field public static final flight_mode_enable_popup:I = 0x7f090a68

.field public static final flight_mode_enable_popup_more_specific:I = 0x7f090a69

.field public static final flight_mode_enable_popup_title:I = 0x7f090a66

.field public static final flight_mode_enabled_toast:I = 0x7f09118a

.field public static final font:I = 0x7f090fbe

.field public static final font_change_popup:I = 0x7f090020

.field public static final font_change_popup_title:I = 0x7f090021

.field public static final font_size_preview_text:I = 0x7f09007d

.field public static final font_size_save:I = 0x7f09007e

.field public static final font_size_summary:I = 0x7f090b32

.field public static final font_size_title:I = 0x7f090b31

.field public static final for_syncing:I = 0x7f091082

.field public static final force_hw_ui:I = 0x7f0908e3

.field public static final force_hw_ui_summary:I = 0x7f0908e4

.field public static final force_msaa:I = 0x7f0908e5

.field public static final force_msaa_summary:I = 0x7f0908e6

.field public static final force_stop:I = 0x7f09066d

.field public static final force_stop_dlg_text:I = 0x7f0906b8

.field public static final force_stop_dlg_title:I = 0x7f0906b7

.field public static final four:I = 0x7f090039

.field public static final from_the_beginning:I = 0x7f090a88

.field public static final from_title:I = 0x7f091092

.field public static final fs_meta_data:I = 0x7f09000f

.field public static final full_locale_test:I = 0x7f0911c6

.field public static final g_sensor_summary:I = 0x7f0909bc

.field public static final g_sensor_title:I = 0x7f0909bb

.field public static final gadget_bluetooth:I = 0x7f090822

.field public static final gadget_brightness_state_auto:I = 0x7f090826

.field public static final gadget_brightness_state_full:I = 0x7f090827

.field public static final gadget_brightness_state_half:I = 0x7f090828

.field public static final gadget_brightness_state_off:I = 0x7f090829

.field public static final gadget_brightness_template:I = 0x7f090825

.field public static final gadget_gps:I = 0x7f090823

.field public static final gadget_picker_title:I = 0x7f09074d

.field public static final gadget_state_off:I = 0x7f09081e

.field public static final gadget_state_on:I = 0x7f09081d

.field public static final gadget_state_template:I = 0x7f09081c

.field public static final gadget_state_turning_off:I = 0x7f090820

.field public static final gadget_state_turning_on:I = 0x7f09081f

.field public static final gadget_sync:I = 0x7f090824

.field public static final gadget_title:I = 0x7f090819

.field public static final gadget_toggle_bluetooth:I = 0x7f09081b

.field public static final gadget_toggle_wifi:I = 0x7f09081a

.field public static final gadget_wifi:I = 0x7f090821

.field public static final game_controller_settings_category:I = 0x7f0906fb

.field public static final getConfiguredNetworks:I = 0x7f09047c

.field public static final getConnectionInfo:I = 0x7f09047d

.field public static final glance_view_advanced_setting_and_tutorial_title:I = 0x7f090c9b

.field public static final glance_view_advanced_setting_title:I = 0x7f090c9c

.field public static final glance_view_battery_charging_info:I = 0x7f090c97

.field public static final glance_view_battery_power:I = 0x7f090c98

.field public static final glance_view_current_music_info:I = 0x7f090c96

.field public static final glance_view_display_setting:I = 0x7f090c8e

.field public static final glance_view_missed_call:I = 0x7f090c91

.field public static final glance_view_missed_calls:I = 0x7f090c92

.field public static final glance_view_more_information:I = 0x7f090c9d

.field public static final glance_view_nearest_alarm:I = 0x7f090c9a

.field public static final glance_view_new_message:I = 0x7f090c93

.field public static final glance_view_notification:I = 0x7f090c90

.field public static final glance_view_now_playing_music:I = 0x7f090c95

.field public static final glance_view_setting_guide_content:I = 0x7f090c8d

.field public static final glance_view_setting_title:I = 0x7f090c8c

.field public static final glance_view_status_bar:I = 0x7f090c99

.field public static final glance_view_time_date:I = 0x7f090c8f

.field public static final glance_view_unread_messages:I = 0x7f090c94

.field public static final global_change_warning:I = 0x7f090a0d

.field public static final global_font_change_title:I = 0x7f090a0f

.field public static final global_locale_change_title:I = 0x7f090a0e

.field public static final global_roaming_country_update_not_available:I = 0x7f091184

.field public static final go_to_samsungdive:I = 0x7f090a53

.field public static final google_account_add:I = 0x7f091086

.field public static final google_account_add_cancel:I = 0x7f091085

.field public static final google_account_add_ok:I = 0x7f091084

.field public static final google_account_add_title_attention:I = 0x7f091083

.field public static final gpsAlertDetail:I = 0x7f090616

.field public static final gpsAlertTitle:I = 0x7f090615

.field public static final gps_body:I = 0x7f09104b

.field public static final gps_body_bottom:I = 0x7f09104c

.field public static final gps_body_title:I = 0x7f09104a

.field public static final gps_donotshow:I = 0x7f09104d

.field public static final gps_location_title:I = 0x7f090ed2

.field public static final gps_notification_sounds_summary_off:I = 0x7f090614

.field public static final gps_notification_sounds_summary_on:I = 0x7f090613

.field public static final gps_notification_sounds_title:I = 0x7f090612

.field public static final gps_security_mode_disable:I = 0x7f090d3b

.field public static final gps_title:I = 0x7f091049

.field public static final gsensor_button2:I = 0x7f0909c2

.field public static final gsensor_calibrating:I = 0x7f0909be

.field public static final gsensor_end:I = 0x7f0909c1

.field public static final gsensor_fail:I = 0x7f0909c0

.field public static final gsensor_nofile:I = 0x7f0909bf

.field public static final gsensor_ready:I = 0x7f0909bd

.field public static final guide_bt_ap_connected:I = 0x7f091022

.field public static final guide_bt_no_ap_summary:I = 0x7f091024

.field public static final guide_bt_select_ap_summary:I = 0x7f091023

.field public static final guide_close_popup_icon:I = 0x7f091027

.field public static final guide_dialog_ap_enabled:I = 0x7f091020

.field public static final guide_dialog_attention_title:I = 0x7f091021

.field public static final guide_dialog_comlete_summary:I = 0x7f09101f

.field public static final guide_dialog_comlete_title:I = 0x7f09101e

.field public static final guide_searching:I = 0x7f091025

.field public static final guide_smart_block_help_text:I = 0x7f090e07

.field public static final guide_tap_area:I = 0x7f091017

.field public static final guide_wifi_connected_automatically:I = 0x7f09101b

.field public static final guide_wifi_connected_successfully:I = 0x7f09101c

.field public static final guide_wifi_no_ap_summary:I = 0x7f09101a

.field public static final guide_wifi_security_credential:I = 0x7f091018

.field public static final guide_wifi_select_ap_summary:I = 0x7f091019

.field public static final guide_wifi_tap_scan:I = 0x7f09101d

.field public static final haptic_feedback_enable_summary_off:I = 0x7f0904b6

.field public static final haptic_feedback_enable_summary_on:I = 0x7f0904b5

.field public static final haptic_feedback_enable_title:I = 0x7f0904b4

.field public static final haptic_feedback_intensity_title:I = 0x7f0904b2

.field public static final haptic_feedback_intensity_title_for_camera:I = 0x7f0904b3

.field public static final hardkeyboard_category:I = 0x7f0906f1

.field public static final hardware_version:I = 0x7f090de8

.field public static final hdcp_checking_dialog_title:I = 0x7f0908bd

.field public static final hdcp_checking_title:I = 0x7f0908bc

.field public static final hdmi:I = 0x7f091042

.field public static final header_account_settings:I = 0x7f0908a7

.field public static final header_add_an_account:I = 0x7f0908a9

.field public static final header_application_sync_settings:I = 0x7f0908a2

.field public static final header_category_backup_options:I = 0x7f090a79

.field public static final header_category_connect_share:I = 0x7f090a7a

.field public static final header_category_connectivity:I = 0x7f090a73

.field public static final header_category_device:I = 0x7f090050

.field public static final header_category_my_accounts:I = 0x7f090a78

.field public static final header_category_network_connections:I = 0x7f090a77

.field public static final header_category_personal:I = 0x7f090051

.field public static final header_category_personalization:I = 0x7f090a74

.field public static final header_category_system:I = 0x7f090052

.field public static final header_category_use_management:I = 0x7f090a75

.field public static final header_category_wireless_networks:I = 0x7f09004f

.field public static final header_data_and_synchronization:I = 0x7f0908a5

.field public static final header_manage_accounts:I = 0x7f09088b

.field public static final heavy_weight_stop_description:I = 0x7f0906dd

.field public static final help_label:I = 0x7f090a11

.field public static final help_smart_pause_0:I = 0x7f090e1b

.field public static final help_smart_pause_1:I = 0x7f090e1c

.field public static final help_smart_pause_2:I = 0x7f090e1d

.field public static final help_smart_pause_3:I = 0x7f090e1e

.field public static final help_smart_pause_4:I = 0x7f090e1f

.field public static final help_smart_pause_5:I = 0x7f090e20

.field public static final help_smart_pause_5_without_hdmi:I = 0x7f090e21

.field public static final help_smart_pause_6:I = 0x7f090e22

.field public static final help_smart_scroll_0:I = 0x7f090e23

.field public static final help_smart_scroll_00:I = 0x7f090e24

.field public static final help_smart_scroll_01:I = 0x7f090e27

.field public static final help_smart_scroll_0_1:I = 0x7f090e28

.field public static final help_smart_scroll_0_2:I = 0x7f090e29

.field public static final help_smart_scroll_0_3:I = 0x7f090e2a

.field public static final help_smart_scroll_1:I = 0x7f090e2b

.field public static final help_smart_scroll_2:I = 0x7f090e2c

.field public static final help_smart_scroll_3:I = 0x7f090e2d

.field public static final help_smart_scroll_4:I = 0x7f090e2e

.field public static final help_smart_scroll_5:I = 0x7f090e2f

.field public static final help_smart_scroll_email:I = 0x7f090e26

.field public static final help_smart_scroll_internet:I = 0x7f090e25

.field public static final help_text_summary:I = 0x7f090a6b

.field public static final help_text_title:I = 0x7f090a6a

.field public static final help_title:I = 0x7f090f77

.field public static final help_title_summary:I = 0x7f090f78

.field public static final help_url_accounts:I = 0x7f090a19

.field public static final help_url_backup_reset:I = 0x7f090a1b

.field public static final help_url_battery:I = 0x7f090a18

.field public static final help_url_bluetooth:I = 0x7f090a13

.field public static final help_url_choose_lockscreen:I = 0x7f090a1a

.field public static final help_url_data_usage:I = 0x7f090a14

.field public static final help_url_dreams:I = 0x7f090a1d

.field public static final help_url_location_access:I = 0x7f090a1f

.field public static final help_url_more_networks:I = 0x7f090a15

.field public static final help_url_security:I = 0x7f090a20

.field public static final help_url_sound:I = 0x7f090a17

.field public static final help_url_tether:I = 0x7f090a1c

.field public static final help_url_users:I = 0x7f090a1e

.field public static final help_url_vpn:I = 0x7f090a16

.field public static final help_url_wifi:I = 0x7f090a12

.field public static final hid_keyboard_connect_hint:I = 0x7f090d75

.field public static final hid_mouse_connect_hint:I = 0x7f090d74

.field public static final hid_not_supported:I = 0x7f090d73

.field public static final hidden_ssid_label:I = 0x7f090487

.field public static final high_contrast_summary:I = 0x7f090b30

.field public static final high_contrast_title:I = 0x7f090b2f

.field public static final hint_recipient:I = 0x7f090a49

.field public static final history_details_title:I = 0x7f0907b5

.field public static final home:I = 0x7f090a72

.field public static final home_screen:I = 0x7f0909e6

.field public static final home_screen_wallpaper:I = 0x7f090d95

.field public static final home_settings:I = 0x7f090f25

.field public static final home_settings_basic_mode:I = 0x7f090f2b

.field public static final home_settings_basic_mode_summary:I = 0x7f090f2c

.field public static final home_settings_category:I = 0x7f090f26

.field public static final home_settings_customer_mode:I = 0x7f090f29

.field public static final home_settings_customer_mode_summary:I = 0x7f090f2a

.field public static final home_settings_easy_mode:I = 0x7f090f2d

.field public static final home_settings_easy_mode_summary:I = 0x7f090f2e

.field public static final home_settings_header:I = 0x7f090f27

.field public static final home_settings_message:I = 0x7f090f28

.field public static final home_settings_setup_help:I = 0x7f090f30

.field public static final home_settings_setup_help2:I = 0x7f090f31

.field public static final home_settings_setup_title:I = 0x7f090f2f

.field public static final homesync_account_mgmt_delete:I = 0x7f091242

.field public static final homesync_account_mgmt_description:I = 0x7f09123c

.field public static final homesync_account_mgmt_description_admin:I = 0x7f091239

.field public static final homesync_account_mgmt_description_admin_title:I = 0x7f091238

.field public static final homesync_account_mgmt_description_guest:I = 0x7f09123b

.field public static final homesync_account_mgmt_description_guest_title:I = 0x7f09123a

.field public static final homesync_account_mgmt_description_title:I = 0x7f091237

.field public static final homesync_account_mgmt_list_admin_title:I = 0x7f09123d

.field public static final homesync_account_mgmt_list_guest_title:I = 0x7f09123e

.field public static final homesync_account_mgmt_menu_add:I = 0x7f091241

.field public static final homesync_account_mgmt_not_register:I = 0x7f091240

.field public static final homesync_account_mgmt_register:I = 0x7f09123f

.field public static final homesync_account_mgmt_title:I = 0x7f091236

.field public static final homesync_backup:I = 0x7f091282

.field public static final homesync_backup_and_restore_title:I = 0x7f09127e

.field public static final homesync_backup_restore_start:I = 0x7f091281

.field public static final homesync_backup_summary:I = 0x7f09127f

.field public static final homesync_backup_title:I = 0x7f0912a6

.field public static final homesync_restore:I = 0x7f091283

.field public static final homesync_restore_summary:I = 0x7f091280

.field public static final homesync_restore_title:I = 0x7f0912a7

.field public static final hovering_icon_label:I = 0x7f090b91

.field public static final hovering_icon_label_msg:I = 0x7f090b92

.field public static final hovering_information_preview:I = 0x7f090b8f

.field public static final hovering_intormation_preview_msg:I = 0x7f090b90

.field public static final hovering_list_scroll:I = 0x7f090b93

.field public static final hovering_list_scroll_msg:I = 0x7f090b94

.field public static final hovering_msg:I = 0x7f090b8c

.field public static final hovering_pointer:I = 0x7f090b8d

.field public static final hovering_pointer_msg:I = 0x7f090b8e

.field public static final hovering_ripple_effect:I = 0x7f090b95

.field public static final hovering_ripple_effect_msg:I = 0x7f090b96

.field public static final hovering_summary:I = 0x7f090b8b

.field public static final hovering_title:I = 0x7f090b8a

.field public static final hovering_use_popup_msg:I = 0x7f090b89

.field public static final hovering_use_popup_title:I = 0x7f090b88

.field public static final how_to_capture_the_screen_summary:I = 0x7f090b79

.field public static final how_to_capture_the_screen_title:I = 0x7f090b78

.field public static final how_to_use_pen_gesture_summary_first:I = 0x7f090b7b

.field public static final how_to_use_pen_gesture_summary_fourth:I = 0x7f090b7e

.field public static final how_to_use_pen_gesture_summary_second:I = 0x7f090b7c

.field public static final how_to_use_pen_gesture_summary_third:I = 0x7f090b7d

.field public static final how_to_use_pen_gesture_title:I = 0x7f090b7a

.field public static final hs20_available_hs_pref:I = 0x7f090289

.field public static final hs20_connectable_hs_pref:I = 0x7f090282

.field public static final hs20_picker_dialog_scan:I = 0x7f09028c

.field public static final hs20_picker_dialog_title:I = 0x7f090284

.field public static final hs20_settings_menu_scan:I = 0x7f09028a

.field public static final hs20_warning_not_proper_cred:I = 0x7f090285

.field public static final hs20settings_title:I = 0x7f09028b

.field public static final hs20settings_wifi_disabled:I = 0x7f090283

.field public static final huge:I = 0x7f090b33

.field public static final icon_glossary:I = 0x7f090013

.field public static final icon_number_1:I = 0x7f091124

.field public static final icon_number_2:I = 0x7f091125

.field public static final ime_security_warning:I = 0x7f0906f4

.field public static final immediately_destroy_activities:I = 0x7f0908ee

.field public static final immediately_destroy_activities_summary:I = 0x7f0908ef

.field public static final ims_reg_required_off:I = 0x7f090058

.field public static final ims_reg_required_on:I = 0x7f090057

.field public static final incoming_call:I = 0x7f0909d2

.field public static final incoming_call_notification_summary:I = 0x7f0909d3

.field public static final incoming_call_volume_title:I = 0x7f0904a1

.field public static final incomplete_profile:I = 0x7f090f69

.field public static final increase_legibility_popup:I = 0x7f090078

.field public static final increase_legibility_popup_title:I = 0x7f090079

.field public static final information_ticker:I = 0x7f090b5c

.field public static final information_ticker_facebook_summary:I = 0x7f090b5e

.field public static final information_ticker_only_weibo_summary:I = 0x7f090b60

.field public static final information_ticker_summary:I = 0x7f090b5d

.field public static final information_ticker_weibo_summary:I = 0x7f090b5f

.field public static final initiate_decrypt:I = 0x7f090a91

.field public static final ink_effect:I = 0x7f090fb1

.field public static final ink_effect_color_black:I = 0x7f090fbb

.field public static final ink_effect_color_blue:I = 0x7f090fb8

.field public static final ink_effect_color_brown:I = 0x7f090fb7

.field public static final ink_effect_color_green:I = 0x7f090fb6

.field public static final ink_effect_color_lightblue:I = 0x7f090fbc

.field public static final ink_effect_color_navy:I = 0x7f090fb9

.field public static final ink_effect_color_none:I = 0x7f090fb2

.field public static final ink_effect_color_orange:I = 0x7f090fb5

.field public static final ink_effect_color_pink:I = 0x7f090fb3

.field public static final ink_effect_color_purple:I = 0x7f090fba

.field public static final ink_effect_color_red:I = 0x7f090fb4

.field public static final inkeffect_help_body:I = 0x7f090fbd

.field public static final input_and_control:I = 0x7f09125a

.field public static final input_method:I = 0x7f090726

.field public static final input_method_selector:I = 0x7f090728

.field public static final input_method_selector_always_hide_title:I = 0x7f09072b

.field public static final input_method_selector_always_hide_value:I = 0x7f09000d

.field public static final input_method_selector_always_show_title:I = 0x7f09072a

.field public static final input_method_selector_always_show_value:I = 0x7f09000c

.field public static final input_method_selector_show_automatically_title:I = 0x7f090729

.field public static final input_method_selector_show_automatically_value:I = 0x7f09000b

.field public static final input_method_selector_visibility_default_value:I = 0x7f09000e

.field public static final input_method_settings:I = 0x7f09072d

.field public static final input_method_settings_button:I = 0x7f09072e

.field public static final input_methods_and_subtype_enabler_title:I = 0x7f090732

.field public static final input_methods_settings_label_format:I = 0x7f090731

.field public static final input_methods_settings_title:I = 0x7f090725

.field public static final insert_sim_card:I = 0x7f0909aa

.field public static final insert_sim_card_message:I = 0x7f0909ab

.field public static final insert_simuim_card:I = 0x7f09110a

.field public static final insert_simuim_card_message:I = 0x7f09110b

.field public static final insert_uim_card:I = 0x7f091106

.field public static final insert_uim_card_message:I = 0x7f091107

.field public static final install_all_warning:I = 0x7f09065c

.field public static final install_applications:I = 0x7f09065a

.field public static final install_text:I = 0x7f090675

.field public static final install_unknown_applications:I = 0x7f09065b

.field public static final installed_variant_default:I = 0x7f090542

.field public static final installed_variant_version:I = 0x7f090541

.field public static final insufficient_storage:I = 0x7f0906b2

.field public static final intelligent_help_0:I = 0x7f090df8

.field public static final intelligent_help_1:I = 0x7f090df9

.field public static final intelligent_help_2:I = 0x7f090dfd

.field public static final intelligent_help_3:I = 0x7f090dfe

.field public static final intelligent_help_summary_0:I = 0x7f090dfa

.field public static final intelligent_help_summary_1:I = 0x7f090dfb

.field public static final intelligent_help_summary_2:I = 0x7f090dfc

.field public static final intelligent_help_summary_3:I = 0x7f090fa5

.field public static final intelligent_rotation_help_1:I = 0x7f090e00

.field public static final intelligent_rotation_summary:I = 0x7f090df7

.field public static final intelligent_rotation_title:I = 0x7f090df6

.field public static final intelligent_screen_summary:I = 0x7f090df3

.field public static final intelligent_screen_title:I = 0x7f090df2

.field public static final intelligent_sleep_folder_summary:I = 0x7f090dff

.field public static final intelligent_sleep_summary:I = 0x7f090df5

.field public static final intelligent_sleep_title:I = 0x7f090df4

.field public static final intent_sender_account_label:I = 0x7f0900f0

.field public static final intent_sender_action_label:I = 0x7f0900ed

.field public static final intent_sender_data_label:I = 0x7f0900eb

.field public static final intent_sender_resource_label:I = 0x7f0900ef

.field public static final intent_sender_sendbroadcast_text:I = 0x7f0900ec

.field public static final intent_sender_startactivity_text:I = 0x7f0900ee

.field public static final internal_memory:I = 0x7f090566

.field public static final internal_storage:I = 0x7f090694

.field public static final international_roaming_setting:I = 0x7f091028

.field public static final introduction_message:I = 0x7f090a43

.field public static final introduction_remote_controls:I = 0x7f090a42

.field public static final invalid_location:I = 0x7f0906b5

.field public static final invalid_size_value:I = 0x7f0906ab

.field public static final ipaddr_label:I = 0x7f090488

.field public static final it_policy_not_allow_wifi:I = 0x7f090474

.field public static final it_policy_not_allow_wifiap:I = 0x7f090475

.field public static final iwlan:I = 0x7f090f33

.field public static final iwlan_add_network:I = 0x7f090f38

.field public static final iwlan_add_network_pdg_domain_name_invalid:I = 0x7f090f44

.field public static final iwlan_add_network_pdg_domain_name_is_alread_exist:I = 0x7f090f42

.field public static final iwlan_add_network_pdg_domain_name_is_empty:I = 0x7f090f43

.field public static final iwlan_add_network_pdg_ip_address_invalid:I = 0x7f090f46

.field public static final iwlan_add_network_pdg_ip_address_is_empty:I = 0x7f090f45

.field public static final iwlan_cancel:I = 0x7f090f4b

.field public static final iwlan_connect:I = 0x7f090f47

.field public static final iwlan_delete:I = 0x7f090f49

.field public static final iwlan_disconnect:I = 0x7f090f48

.field public static final iwlan_network:I = 0x7f090f37

.field public static final iwlan_network_skt:I = 0x7f090f36

.field public static final iwlan_network_skt_default:I = 0x7f090f39

.field public static final iwlan_network_skt_pdg_domain_name:I = 0x7f090f34

.field public static final iwlan_network_skt_pdg_ip_address:I = 0x7f090f35

.field public static final iwlan_ok:I = 0x7f090f4c

.field public static final iwlan_pdg_address:I = 0x7f090f41

.field public static final iwlan_pdg_name:I = 0x7f090f40

.field public static final iwlan_ready:I = 0x7f090f3f

.field public static final iwlan_save:I = 0x7f090f4a

.field public static final iwland_state_connected:I = 0x7f090f3a

.field public static final iwland_state_connecting:I = 0x7f090f3b

.field public static final iwland_state_deamon_processing:I = 0x7f090f3e

.field public static final iwland_state_disconnecting:I = 0x7f090f3c

.field public static final iwland_state_idle:I = 0x7f090f3d

.field public static final join_many_items_first:I = 0x7f0906a6

.field public static final join_many_items_last:I = 0x7f0906a5

.field public static final join_many_items_middle:I = 0x7f0906a7

.field public static final join_two_items:I = 0x7f0906a4

.field public static final keep_screen_on:I = 0x7f09073f

.field public static final keep_screen_on_summary:I = 0x7f090740

.field public static final kernel_version:I = 0x7f090543

.field public static final key_backlight:I = 0x7f09107d

.field public static final key_backlight_duration:I = 0x7f0909ea

.field public static final key_backlight_duration_summary:I = 0x7f0909eb

.field public static final key_backlight_summary:I = 0x7f09107e

.field public static final key_night_mode:I = 0x7f09107f

.field public static final key_night_mode_summary:I = 0x7f091080

.field public static final keyboard_layout_default_label:I = 0x7f090701

.field public static final keyboard_layout_dialog_setup_button:I = 0x7f0906ff

.field public static final keyboard_layout_dialog_switch_hint:I = 0x7f090700

.field public static final keyboard_layout_dialog_title:I = 0x7f0906fe

.field public static final keyboard_layout_picker_title:I = 0x7f090702

.field public static final keyboard_settings_category:I = 0x7f0906e9

.field public static final keyboard_settings_title:I = 0x7f0907ed

.field public static final keyguard_password_attempt_count_pin_code:I = 0x7f090e67

.field public static final keyguard_password_attempts_count_pin_code:I = 0x7f090e68

.field public static final kids_mode:I = 0x7f090fd4

.field public static final kies_title:I = 0x7f090a7e

.field public static final kies_via_wifi:I = 0x7f090a26

.field public static final kies_via_wifi_summary:I = 0x7f090a27

.field public static final kt_sim_change_failed:I = 0x7f090527

.field public static final kt_sim_disable_sim_lock:I = 0x7f090530

.field public static final kt_sim_disable_sim_perso:I = 0x7f090536

.field public static final kt_sim_enable_sim_lock:I = 0x7f09052f

.field public static final kt_sim_enable_sim_perso:I = 0x7f090535

.field public static final kt_sim_enter_new:I = 0x7f09052d

.field public static final kt_sim_enter_new_again:I = 0x7f09052e

.field public static final kt_sim_enter_old:I = 0x7f09052c

.field public static final kt_sim_lock_failed:I = 0x7f090526

.field public static final kt_sim_no_pin_entered:I = 0x7f090529

.field public static final kt_sim_perso_bad_passwd:I = 0x7f090537

.field public static final kt_sim_perso_enter_new:I = 0x7f090531

.field public static final kt_sim_perso_enter_new_again:I = 0x7f090532

.field public static final kt_sim_perso_enter_old:I = 0x7f090538

.field public static final kt_sim_perso_exceed_max_retry_count:I = 0x7f090539

.field public static final kt_sim_perso_passwd_dont_match:I = 0x7f090534

.field public static final kt_sim_perso_wrong_passwd_length:I = 0x7f090533

.field public static final kt_sim_pins_dont_match:I = 0x7f09052b

.field public static final kt_sim_wrong_pin_chances:I = 0x7f090528

.field public static final kt_sim_wrong_pin_length:I = 0x7f09052a

.field public static final label_settings:I = 0x7f09118c

.field public static final language_keyboard_settings_title:I = 0x7f0906e6

.field public static final language_picker_title:I = 0x7f090136

.field public static final language_settings:I = 0x7f0906e5

.field public static final language_settings_category:I = 0x7f0906e8

.field public static final lanunch_page_buddy:I = 0x7f091067

.field public static final lanunch_page_buddy_help_1:I = 0x7f091068

.field public static final lanunch_page_buddy_help_2:I = 0x7f091069

.field public static final lanunch_page_buddy_help_3:I = 0x7f09106a

.field public static final large:I = 0x7f090b34

.field public static final large_font:I = 0x7f09007c

.field public static final last_synced:I = 0x7f090897

.field public static final launch_count_label:I = 0x7f09075a

.field public static final learn_about_air_clip:I = 0x7f090c7e

.field public static final learn_about_air_clip_content:I = 0x7f090c7f

.field public static final learn_about_air_clip_content_snote:I = 0x7f090c80

.field public static final learn_about_air_glance_view:I = 0x7f090c6a

.field public static final learn_about_air_glance_view_content:I = 0x7f090c6b

.field public static final learn_about_air_item_move:I = 0x7f090c73

.field public static final learn_about_air_item_move_content:I = 0x7f090c74

.field public static final learn_about_air_item_move_content_calendar:I = 0x7f090c76

.field public static final learn_about_air_item_move_content_calendar_with_lockscreen:I = 0x7f090c77

.field public static final learn_about_air_item_move_content_part_1:I = 0x7f090c78

.field public static final learn_about_air_item_move_content_part_2:I = 0x7f090c79

.field public static final learn_about_air_item_move_content_range_part_1:I = 0x7f090c7a

.field public static final learn_about_air_item_move_content_range_part_2:I = 0x7f090c7b

.field public static final learn_about_air_item_move_content_range_part_3:I = 0x7f090c7c

.field public static final learn_about_air_item_move_content_range_part_3_calendar:I = 0x7f090c7d

.field public static final learn_about_air_item_move_content_with_lockscreen:I = 0x7f090c75

.field public static final learn_about_air_motion_indicator:I = 0x7f090c64

.field public static final learn_about_air_motion_indicator_content:I = 0x7f090c65

.field public static final learn_about_air_motion_indicator_content_part_1:I = 0x7f090c66

.field public static final learn_about_air_motion_indicator_content_part_2:I = 0x7f090c67

.field public static final learn_about_air_motion_indicator_content_part_3:I = 0x7f090c68

.field public static final learn_about_air_motion_indicator_content_part_3_us:I = 0x7f090c69

.field public static final learn_about_air_note_swap:I = 0x7f090c6d

.field public static final learn_about_air_scroll:I = 0x7f090c6e

.field public static final learn_about_air_scroll_content:I = 0x7f090c6f

.field public static final learn_about_air_turn:I = 0x7f090c70

.field public static final learn_about_air_turn_content:I = 0x7f090c71

.field public static final learn_about_air_turn_content_snote:I = 0x7f090c72

.field public static final learn_about_air_web_navigate:I = 0x7f090c6c

.field public static final learn_about_call_accept:I = 0x7f090c81

.field public static final learn_about_call_accept_content:I = 0x7f090c82

.field public static final learn_about_double_tap_title:I = 0x7f090bfd

.field public static final learn_about_glance_view_description:I = 0x7f090c9f

.field public static final learn_about_glance_view_description_part_1:I = 0x7f090ca0

.field public static final learn_about_glance_view_description_part_2:I = 0x7f090ca1

.field public static final learn_about_glance_view_description_part_3:I = 0x7f090ca2

.field public static final learn_about_motions_title:I = 0x7f090be0

.field public static final learn_about_palm_swipe_content:I = 0x7f090c29

.field public static final learn_about_palm_swipe_title:I = 0x7f090c28

.field public static final learn_about_palm_touch_content:I = 0x7f090c2b

.field public static final learn_about_palm_touch_title:I = 0x7f090c2a

.field public static final learn_about_pan_title:I = 0x7f090bfb

.field public static final learn_about_pick_up_title:I = 0x7f090bfe

.field public static final learn_about_pick_up_to_call_out_title:I = 0x7f090bff

.field public static final learn_about_quick_glance_title:I = 0x7f090c9e

.field public static final learn_about_rotate_content:I = 0x7f090c39

.field public static final learn_about_rotate_title:I = 0x7f090c38

.field public static final learn_about_scroll_acceleration_content:I = 0x7f090e1a

.field public static final learn_about_scroll_sensitivity_content:I = 0x7f090e18

.field public static final learn_about_scroll_type_content:I = 0x7f090e17

.field public static final learn_about_scroll_unit_content:I = 0x7f090e19

.field public static final learn_about_shake_title:I = 0x7f090bfc

.field public static final learn_about_surface_title:I = 0x7f090c21

.field public static final learn_about_tap_and_twist_content:I = 0x7f090c2d

.field public static final learn_about_tap_and_twist_title:I = 0x7f090c2c

.field public static final learn_about_tilt_title:I = 0x7f090bfa

.field public static final learn_about_turn_over_title:I = 0x7f090c00

.field public static final learn_power_saving_tips:I = 0x7f090b11

.field public static final led_indicator_charging:I = 0x7f090dce

.field public static final led_indicator_charging_summary:I = 0x7f090dcf

.field public static final led_indicator_incoming_notification:I = 0x7f090dd7

.field public static final led_indicator_incoming_notification_summary:I = 0x7f090dd8

.field public static final led_indicator_low_battery:I = 0x7f090fa0

.field public static final led_indicator_low_battery_summary:I = 0x7f090fa1

.field public static final led_indicator_missed_event:I = 0x7f090dd1

.field public static final led_indicator_missed_event_summary:I = 0x7f090dd2

.field public static final led_indicator_notification:I = 0x7f090dd0

.field public static final led_indicator_notifications:I = 0x7f090dd3

.field public static final led_indicator_notifications_summary:I = 0x7f090dd4

.field public static final led_indicator_settings:I = 0x7f090dcd

.field public static final led_indicator_voice_recording:I = 0x7f090dd5

.field public static final led_indicator_voice_recording_summary:I = 0x7f090dd6

.field public static final left:I = 0x7f0910a2

.field public static final legal_information:I = 0x7f09061a

.field public static final lgt_service_global_auto_roaming:I = 0x7f090f22

.field public static final lgt_service_global_auto_roaming_auto_dialing:I = 0x7f091174

.field public static final lgt_service_global_auto_roaming_auto_dialing_summary:I = 0x7f091175

.field public static final lgt_service_global_auto_roaming_background_data_disabling_popup:I = 0x7f09117a

.field public static final lgt_service_global_auto_roaming_background_data_disabling_toast:I = 0x7f09117c

.field public static final lgt_service_global_auto_roaming_background_data_enabling_popup:I = 0x7f09117b

.field public static final lgt_service_global_auto_roaming_background_data_enabling_toast:I = 0x7f09117d

.field public static final lgt_service_global_auto_roaming_background_data_summary:I = 0x7f091179

.field public static final lgt_service_global_auto_roaming_call_customcenter:I = 0x7f091176

.field public static final lgt_service_global_auto_roaming_call_customcenter_summary:I = 0x7f091177

.field public static final lgt_service_global_auto_roaming_call_customcenter_summary_nocall:I = 0x7f091178

.field public static final lgt_service_global_auto_roaming_country_update:I = 0x7f091172

.field public static final lgt_service_global_auto_roaming_country_update_summary:I = 0x7f091173

.field public static final lgt_service_global_auto_roaming_network_setting:I = 0x7f091170

.field public static final lgt_service_global_auto_roaming_network_setting_summary:I = 0x7f091171

.field public static final license_settings:I = 0x7f09016e

.field public static final license_title:I = 0x7f09061e

.field public static final light_effect:I = 0x7f090faf

.field public static final light_effect_of_ripple:I = 0x7f090fb0

.field public static final limit_app_contents_transfer:I = 0x7f091260

.field public static final link_samsung_dive:I = 0x7f090a3f

.field public static final link_speed_label:I = 0x7f09048b

.field public static final list_to_tab:I = 0x7f090a84

.field public static final live_wallpapers:I = 0x7f090b1c

.field public static final local_backup_password_summary_change:I = 0x7f09084f

.field public static final local_backup_password_summary_none:I = 0x7f09084e

.field public static final local_backup_password_title:I = 0x7f09084d

.field public static final local_backup_password_toast_confirmation_mismatch:I = 0x7f090851

.field public static final local_backup_password_toast_success:I = 0x7f090850

.field public static final local_backup_password_toast_validation_failure:I = 0x7f090852

.field public static final location_3rdparty_gps:I = 0x7f090ed3

.field public static final location_3rdparty_gps_description:I = 0x7f090ed4

.field public static final location_3rdparty_gps_dialogtext:I = 0x7f090ed6

.field public static final location_3rdparty_gps_vzw_description:I = 0x7f090ed5

.field public static final location_3rdparty_network:I = 0x7f090ed8

.field public static final location_3rdparty_network_description:I = 0x7f090ed9

.field public static final location_3rdparty_network_dialogtext:I = 0x7f090eda

.field public static final location_access_summary:I = 0x7f090609

.field public static final location_access_summary_new:I = 0x7f09060a

.field public static final location_access_title:I = 0x7f090608

.field public static final location_consent:I = 0x7f090a48

.field public static final location_e911:I = 0x7f090ecc

.field public static final location_e911_description:I = 0x7f090ecd

.field public static final location_gps:I = 0x7f090600

.field public static final location_gps_disabled:I = 0x7f090de4

.field public static final location_logs:I = 0x7f090610

.field public static final location_neighborhood_level:I = 0x7f0905fd

.field public static final location_neighborhood_level_wifi:I = 0x7f0905fe

.field public static final location_network_based:I = 0x7f0905fb

.field public static final location_networks_disabled:I = 0x7f090de3

.field public static final location_pdr:I = 0x7f09060c

.field public static final location_pdr_diag_desc:I = 0x7f09060e

.field public static final location_pdr_diag_dont_ask:I = 0x7f09060f

.field public static final location_settings_title:I = 0x7f090177

.field public static final location_sources_heading:I = 0x7f09060b

.field public static final location_street_level:I = 0x7f090601

.field public static final location_title:I = 0x7f0905fa

.field public static final location_use_pdr:I = 0x7f09060d

.field public static final location_vzw:I = 0x7f090ecf

.field public static final location_vzw_description:I = 0x7f090ed0

.field public static final location_vzw_dialogtext:I = 0x7f090ed1

.field public static final location_vzw_neighborhood_level:I = 0x7f0905ff

.field public static final location_vzw_network_based:I = 0x7f0905fc

.field public static final location_vzwgps_dialogtext:I = 0x7f090edb

.field public static final lock_after_timeout:I = 0x7f09016f

.field public static final lock_after_timeout_immedi_summary:I = 0x7f090e4c

.field public static final lock_after_timeout_summary:I = 0x7f090170

.field public static final lock_intro_message:I = 0x7f090654

.field public static final lock_screen:I = 0x7f0909ca

.field public static final lock_screen_camera_widget_body:I = 0x7f090fec

.field public static final lock_screen_camera_widget_title:I = 0x7f090feb

.field public static final lock_screen_clock_myprofile_widget:I = 0x7f090ff4

.field public static final lock_screen_clock_preview:I = 0x7f091000

.field public static final lock_screen_clock_widget_body:I = 0x7f090ffe

.field public static final lock_screen_clock_widget_options:I = 0x7f090ff5

.field public static final lock_screen_date_and_year_summary:I = 0x7f090ffa

.field public static final lock_screen_date_and_year_title:I = 0x7f090ff9

.field public static final lock_screen_edit_clock_summary:I = 0x7f090ff8

.field public static final lock_screen_edit_clock_title:I = 0x7f090ff7

.field public static final lock_screen_edit_shortcut_widget_summary:I = 0x7f090ff3

.field public static final lock_screen_edit_shortcut_widget_title:I = 0x7f090ff2

.field public static final lock_screen_face_unlock_options:I = 0x7f090fe5

.field public static final lock_screen_multiple_lock:I = 0x7f090fe0

.field public static final lock_screen_multiple_lock_summary:I = 0x7f090fe1

.field public static final lock_screen_myprofile_widget_body:I = 0x7f090fff

.field public static final lock_screen_myprofile_widget_options:I = 0x7f090ff6

.field public static final lock_screen_options:I = 0x7f091008

.field public static final lock_screen_options_summary:I = 0x7f091009

.field public static final lock_screen_options_summary_kor:I = 0x7f09100b

.field public static final lock_screen_options_summary_no_ticker:I = 0x7f09100a

.field public static final lock_screen_owner_info_summary:I = 0x7f090ffd

.field public static final lock_screen_owner_info_title:I = 0x7f090ffc

.field public static final lock_screen_password_options:I = 0x7f090fe6

.field public static final lock_screen_pattern_options:I = 0x7f090fe3

.field public static final lock_screen_pin_options:I = 0x7f090fe4

.field public static final lock_screen_security:I = 0x7f090fdf

.field public static final lock_screen_shortcut_camera_screen:I = 0x7f090ff0

.field public static final lock_screen_shortcut_camera_widget:I = 0x7f090ff1

.field public static final lock_screen_shortcut_guide_content:I = 0x7f090ef8

.field public static final lock_screen_shortcut_guide_content_2:I = 0x7f090ef9

.field public static final lock_screen_shortcut_guide_title:I = 0x7f090ef7

.field public static final lock_screen_shortcut_header_summary:I = 0x7f090ef2

.field public static final lock_screen_shortcut_header_title:I = 0x7f090ef1

.field public static final lock_screen_shortcut_options_summary:I = 0x7f090fef

.field public static final lock_screen_shortcut_options_title:I = 0x7f090fee

.field public static final lock_screen_shortcut_summary:I = 0x7f090ef0

.field public static final lock_screen_shortcut_title:I = 0x7f090eef

.field public static final lock_screen_shortcut_widget_body:I = 0x7f090fed

.field public static final lock_screen_shortcut_widget_title:I = 0x7f090fea

.field public static final lock_screen_show_date_title:I = 0x7f090ffb

.field public static final lock_screen_swipe_options:I = 0x7f090fe2

.field public static final lock_screen_widget_options:I = 0x7f090fe7

.field public static final lock_screen_widget_options_summary:I = 0x7f090fe8

.field public static final lock_screen_widget_options_summary_no_camera:I = 0x7f090fe9

.field public static final lock_settings_picker_title:I = 0x7f090190

.field public static final lock_settings_title:I = 0x7f090633

.field public static final lock_setup:I = 0x7f0901ee

.field public static final lock_sounds_enable_summary_off:I = 0x7f0904b1

.field public static final lock_sounds_enable_summary_on:I = 0x7f0904b0

.field public static final lock_sounds_enable_title:I = 0x7f0904af

.field public static final lock_timeout_max:I = 0x7f090e4d

.field public static final lock_title:I = 0x7f090653

.field public static final lockfinger_change_finger_unlock_title:I = 0x7f091151

.field public static final lockfinger_dbadmin_failure_default_toast:I = 0x7f091152

.field public static final lockfinger_dbadmin_title:I = 0x7f091153

.field public static final lockfinger_enrollment_canceled_toast:I = 0x7f091154

.field public static final lockfinger_enrollment_database_full:I = 0x7f091155

.field public static final lockfinger_enrollment_failure_default_toast:I = 0x7f091156

.field public static final lockfinger_enrollment_succeeded_toast:I = 0x7f091157

.field public static final lockfinger_enrollment_timeout_toast:I = 0x7f091158

.field public static final lockfinger_enrollment_unknown_error_toast:I = 0x7f091159

.field public static final lockfinger_finger_settings_title:I = 0x7f09115a

.field public static final lockfinger_lockout_countdown_footer:I = 0x7f09115b

.field public static final lockfinger_semaphore_failure_toast:I = 0x7f09115c

.field public static final lockfinger_set_finger_unlock_title:I = 0x7f09115d

.field public static final lockfinger_toggle_unlock_finger_summary:I = 0x7f09115e

.field public static final lockfinger_toggle_unlock_finger_title:I = 0x7f09115f

.field public static final lockfinger_too_many_bad_swipes_header:I = 0x7f091160

.field public static final lockfinger_tsm_library_not_available_toast:I = 0x7f091161

.field public static final lockfinger_ui_timeout_header:I = 0x7f091162

.field public static final lockfinger_unlock_disable_lock_finger_summary:I = 0x7f091163

.field public static final lockfinger_unlock_fingerprint_shortcut_summary:I = 0x7f09116d

.field public static final lockfinger_unlock_set_unlock_finger_summary:I = 0x7f091164

.field public static final lockfinger_unlock_set_unlock_finger_title:I = 0x7f091165

.field public static final lockfinger_unlock_set_unlock_launch_picker_change_summary:I = 0x7f091166

.field public static final lockfinger_unlock_set_unlock_launch_picker_summary:I = 0x7f091167

.field public static final lockpassword_cac_contains_non_digits:I = 0x7f0901b1

.field public static final lockpassword_cac_pin_set_toast:I = 0x7f0901c8

.field public static final lockpassword_cancel_label:I = 0x7f0901eb

.field public static final lockpassword_choose_lock_generic_header:I = 0x7f09062f

.field public static final lockpassword_choose_your_cac_pin_header:I = 0x7f0901c2

.field public static final lockpassword_choose_your_password_header:I = 0x7f090627

.field public static final lockpassword_choose_your_pattern_header:I = 0x7f090628

.field public static final lockpassword_choose_your_pin_header:I = 0x7f090629

.field public static final lockpassword_confirm_cac_pins_dont_match:I = 0x7f0901c7

.field public static final lockpassword_confirm_passwords_dont_match:I = 0x7f09062d

.field public static final lockpassword_confirm_pins_dont_match:I = 0x7f09062e

.field public static final lockpassword_confirm_your_cac_pin_header:I = 0x7f0901c3

.field public static final lockpassword_confirm_your_password_header:I = 0x7f09062a

.field public static final lockpassword_confirm_your_pattern_header:I = 0x7f09062b

.field public static final lockpassword_confirm_your_pin_header:I = 0x7f09062c

.field public static final lockpassword_connect_cac_bt:I = 0x7f0901c4

.field public static final lockpassword_continue_label:I = 0x7f0901df

.field public static final lockpassword_finger_set_toast:I = 0x7f091168

.field public static final lockpassword_illegal_character:I = 0x7f0901e4

.field public static final lockpassword_ok_label:I = 0x7f0901ea

.field public static final lockpassword_password_exists_history:I = 0x7f090d17

.field public static final lockpassword_password_failed_match_pattern:I = 0x7f0901e8

.field public static final lockpassword_password_recently_used:I = 0x7f0901e9

.field public static final lockpassword_password_repeating_chars:I = 0x7f090d18

.field public static final lockpassword_password_requires_alpha:I = 0x7f0901e5

.field public static final lockpassword_password_requires_digit:I = 0x7f0901e6

.field public static final lockpassword_password_requires_symbol:I = 0x7f0901e7

.field public static final lockpassword_password_sequential_chars:I = 0x7f090d19

.field public static final lockpassword_password_set_toast:I = 0x7f090630

.field public static final lockpassword_password_too_long:I = 0x7f0901e0

.field public static final lockpassword_password_too_short:I = 0x7f0901dc

.field public static final lockpassword_pattern_set_toast:I = 0x7f090632

.field public static final lockpassword_pin_contains_non_digits:I = 0x7f0901e2

.field public static final lockpassword_pin_recently_used:I = 0x7f0901e3

.field public static final lockpassword_pin_repeating_nums:I = 0x7f090d1a

.field public static final lockpassword_pin_sequential_nums:I = 0x7f090d1b

.field public static final lockpassword_pin_set_toast:I = 0x7f090631

.field public static final lockpassword_pin_too_long:I = 0x7f0901e1

.field public static final lockpassword_pin_too_short:I = 0x7f0901dd

.field public static final lockpassword_press_continue:I = 0x7f0901de

.field public static final lockpassword_requires_more_complex_chars:I = 0x7f090d16

.field public static final lockpattern_change_lock_finger_label:I = 0x7f091169

.field public static final lockpattern_change_lock_pattern_label:I = 0x7f090634

.field public static final lockpattern_change_lock_pin_label:I = 0x7f090635

.field public static final lockpattern_confirm_button_text:I = 0x7f090641

.field public static final lockpattern_continue_button_text:I = 0x7f090644

.field public static final lockpattern_need_to_confirm:I = 0x7f09063f

.field public static final lockpattern_need_to_unlock:I = 0x7f090636

.field public static final lockpattern_need_to_unlock_footer:I = 0x7f090637

.field public static final lockpattern_need_to_unlock_wrong:I = 0x7f090638

.field public static final lockpattern_need_to_unlock_wrong_footer:I = 0x7f090639

.field public static final lockpattern_pattern_confirmed_header:I = 0x7f090640

.field public static final lockpattern_pattern_entered_header:I = 0x7f09063e

.field public static final lockpattern_recording_incorrect_too_short:I = 0x7f09063d

.field public static final lockpattern_recording_inprogress:I = 0x7f09063c

.field public static final lockpattern_recording_intro_footer:I = 0x7f09063b

.field public static final lockpattern_recording_intro_header:I = 0x7f09063a

.field public static final lockpattern_restart_button_text:I = 0x7f090642

.field public static final lockpattern_retry_button_text:I = 0x7f090643

.field public static final lockpattern_settings_change_lock_pattern:I = 0x7f09064c

.field public static final lockpattern_settings_choose_lock_pattern:I = 0x7f09064b

.field public static final lockpattern_settings_enable_hold_button_instantly_locks:I = 0x7f091108

.field public static final lockpattern_settings_enable_power_button_instantly_locks:I = 0x7f09064a

.field public static final lockpattern_settings_enable_summary:I = 0x7f090647

.field public static final lockpattern_settings_enable_tactile_feedback_title:I = 0x7f090649

.field public static final lockpattern_settings_enable_title:I = 0x7f090646

.field public static final lockpattern_settings_enable_visible_pattern_title:I = 0x7f090648

.field public static final lockpattern_settings_help_how_to_record:I = 0x7f09064d

.field public static final lockpattern_settings_title:I = 0x7f090645

.field public static final lockpattern_too_many_failed_confirmation_attempts_footer:I = 0x7f09064f

.field public static final lockpattern_too_many_failed_confirmation_attempts_header:I = 0x7f09064e

.field public static final lockpattern_tutorial_cancel_label:I = 0x7f0901ec

.field public static final lockpattern_tutorial_continue_label:I = 0x7f0901ed

.field public static final lockscreen_emergency_call:I = 0x7f090063

.field public static final lockscreen_wallpaper:I = 0x7f0909cd

.field public static final long_press:I = 0x7f090fc4

.field public static final loss_prevention_alert_Popup_msg:I = 0x7f090b9e

.field public static final loss_prevention_alert_Popup_title:I = 0x7f090b9d

.field public static final loss_prevention_alert_msg:I = 0x7f090b9c

.field public static final loss_prevention_alert_title:I = 0x7f090b9b

.field public static final lte_ram_dump_off:I = 0x7f09005a

.field public static final lte_ram_dump_on:I = 0x7f090059

.field public static final lte_roaming_set_warning_message:I = 0x7f091276

.field public static final macaddr_label:I = 0x7f090489

.field public static final main_running_process_description:I = 0x7f0906e0

.field public static final manage_custom_vibration:I = 0x7f090da2

.field public static final manage_device_admin:I = 0x7f0901f0

.field public static final manage_device_admin_summary:I = 0x7f0901f1

.field public static final manage_space_text:I = 0x7f09068a

.field public static final manageapplications_settings_summary:I = 0x7f090656

.field public static final manageapplications_settings_title:I = 0x7f090655

.field public static final manual_setup:I = 0x7f090f99

.field public static final masked_star_chars:I = 0x7f090040

.field public static final master_clear_accounts:I = 0x7f0905be

.field public static final master_clear_button_text:I = 0x7f0905c5

.field public static final master_clear_confirm_title:I = 0x7f0905cb

.field public static final master_clear_desc:I = 0x7f0905bd

.field public static final master_clear_desc_also_erases_external:I = 0x7f0905bf

.field public static final master_clear_desc_erase_external_storage:I = 0x7f0905c0

.field public static final master_clear_failed:I = 0x7f0905ca

.field public static final master_clear_final_button_text:I = 0x7f0905c7

.field public static final master_clear_final_desc:I = 0x7f0905c6

.field public static final master_clear_gesture_explanation:I = 0x7f0905c9

.field public static final master_clear_gesture_prompt:I = 0x7f0905c8

.field public static final master_clear_summary:I = 0x7f0905bc

.field public static final master_clear_title:I = 0x7f0905bb

.field public static final matched_ap_title:I = 0x7f090f65

.field public static final max_recipients:I = 0x7f090a4c

.field public static final max_sta_reached_title:I = 0x7f0912df

.field public static final media_format_button_text:I = 0x7f0905d1

.field public static final media_format_button_text_sd:I = 0x7f0905d2

.field public static final media_format_desc:I = 0x7f0905cf

.field public static final media_format_desc_sd:I = 0x7f0905d0

.field public static final media_format_final_button_text:I = 0x7f0905d5

.field public static final media_format_final_desc:I = 0x7f0905d3

.field public static final media_format_final_desc_sd:I = 0x7f0905d4

.field public static final media_format_gesture_explanation:I = 0x7f0905d7

.field public static final media_format_gesture_explanation_sd:I = 0x7f0905d8

.field public static final media_format_gesture_prompt:I = 0x7f0905d6

.field public static final media_format_summary:I = 0x7f0905ce

.field public static final media_format_title:I = 0x7f0905cc

.field public static final media_format_title_sd:I = 0x7f0905cd

.field public static final media_share_title:I = 0x7f090a7d

.field public static final media_volume_summary:I = 0x7f0904a6

.field public static final media_volume_title:I = 0x7f0904a5

.field public static final medium_font:I = 0x7f09007b

.field public static final meeting:I = 0x7f090def

.field public static final memory:I = 0x7f0906ce

.field public static final memory_apps_usage:I = 0x7f09056c

.field public static final memory_available:I = 0x7f090568

.field public static final memory_available_read_only:I = 0x7f090569

.field public static final memory_calculating_size:I = 0x7f09056b

.field public static final memory_clear_cache_message:I = 0x7f09057d

.field public static final memory_clear_cache_title:I = 0x7f09057c

.field public static final memory_dcim_usage:I = 0x7f09056f

.field public static final memory_downloads_usage:I = 0x7f09056e

.field public static final memory_media_cache_usage:I = 0x7f090572

.field public static final memory_media_misc_usage:I = 0x7f090571

.field public static final memory_media_usage:I = 0x7f09056d

.field public static final memory_music_usage:I = 0x7f090570

.field public static final memory_size:I = 0x7f09056a

.field public static final menu_cancel:I = 0x7f0905b2

.field public static final menu_delete:I = 0x7f0905ae

.field public static final menu_new:I = 0x7f0905af

.field public static final menu_new_vzw:I = 0x7f0905b0

.field public static final menu_restore:I = 0x7f0905b9

.field public static final menu_save:I = 0x7f0905b1

.field public static final menu_stats_last_unplugged:I = 0x7f0907e3

.field public static final menu_stats_refresh:I = 0x7f0907e5

.field public static final menu_stats_total:I = 0x7f0907e4

.field public static final menu_stats_unplugged:I = 0x7f0907e2

.field public static final message:I = 0x7f0909d4

.field public static final message_Ok:I = 0x7f0909ed

.field public static final message_block_list:I = 0x7f090e7c

.field public static final message_block_list_summary:I = 0x7f090e7d

.field public static final message_block_mode:I = 0x7f090e7b

.field public static final message_block_phrase_list:I = 0x7f090e7e

.field public static final message_block_phrase_list_summary:I = 0x7f090e7f

.field public static final message_block_title:I = 0x7f090e7a

.field public static final message_network_unavailable:I = 0x7f090a5e

.field public static final message_notification_summary:I = 0x7f0909d5

.field public static final misc_files:I = 0x7f0908b8

.field public static final misc_files_selected_count:I = 0x7f0908b9

.field public static final misc_files_selected_count_bytes:I = 0x7f0908ba

.field public static final misc_system_memory:I = 0x7f091010

.field public static final mobile_ap_disable:I = 0x7f0903dc

.field public static final mobile_ap_not_disable:I = 0x7f0903dd

.field public static final mobile_data_dialog_text:I = 0x7f0912e1

.field public static final mobile_data_off_dialog_text_common:I = 0x7f090908

.field public static final mobileap:I = 0x7f09037d

.field public static final mobileap_dig_ok:I = 0x7f090ac5

.field public static final mobileap_notification_dialog_title:I = 0x7f090ac4

.field public static final mobility_preference_title:I = 0x7f090dcc

.field public static final mode_change:I = 0x7f090fd5

.field public static final mode_change_title:I = 0x7f090fd6

.field public static final mode_preview_help:I = 0x7f091074

.field public static final model_number:I = 0x7f09053e

.field public static final mono_audio_summary:I = 0x7f090b47

.field public static final mono_audio_title:I = 0x7f090b46

.field public static final monotype_android_market_uri:I = 0x7f090990

.field public static final monotype_default_font:I = 0x7f09099b

.field public static final monotype_dialog_button:I = 0x7f09098b

.field public static final monotype_dialog_button_more_fonts:I = 0x7f09098c

.field public static final monotype_dialog_font_applemint:I = 0x7f090996

.field public static final monotype_dialog_font_choco:I = 0x7f090995

.field public static final monotype_dialog_font_cool:I = 0x7f090993

.field public static final monotype_dialog_font_girl:I = 0x7f090efa

.field public static final monotype_dialog_font_kaiti:I = 0x7f090efb

.field public static final monotype_dialog_font_loading:I = 0x7f090efd

.field public static final monotype_dialog_font_maruberi:I = 0x7f090998

.field public static final monotype_dialog_font_miao:I = 0x7f090efc

.field public static final monotype_dialog_font_mincho:I = 0x7f090999

.field public static final monotype_dialog_font_pop:I = 0x7f09099a

.field public static final monotype_dialog_font_rose:I = 0x7f090994

.field public static final monotype_dialog_font_tinkerbell:I = 0x7f090997

.field public static final monotype_dialog_no_fonts_installed:I = 0x7f090992

.field public static final monotype_dialog_revert_msg:I = 0x7f09098f

.field public static final monotype_dialog_revert_question:I = 0x7f09098e

.field public static final monotype_dialog_set_font_question:I = 0x7f09098d

.field public static final monotype_dialog_title:I = 0x7f09098a

.field public static final monotype_invalid_font:I = 0x7f0909a1

.field public static final monotype_loading_list:I = 0x7f0909a0

.field public static final monotype_not_supported_font:I = 0x7f0909a4

.field public static final monotype_preference_summary:I = 0x7f090989

.field public static final monotype_preference_title:I = 0x7f090988

.field public static final monotype_restart_dialog_button_1:I = 0x7f09099e

.field public static final monotype_restart_dialog_button_2:I = 0x7f09099f

.field public static final monotype_restart_dialog_text_1:I = 0x7f09099c

.field public static final monotype_restart_dialog_text_2:I = 0x7f09099d

.field public static final monotype_samsung_apps_uri:I = 0x7f090991

.field public static final monotype_select_font:I = 0x7f0909a3

.field public static final monotype_uninstall_font:I = 0x7f0909a2

.field public static final more_accounts:I = 0x7f090b5b

.field public static final more_networks:I = 0x7f090fc1

.field public static final more_settings:I = 0x7f090fc0

.field public static final motion2013_browse_image_title:I = 0x7f090c40

.field public static final motion2013_capture_screen_title:I = 0x7f090c46

.field public static final motion2013_move_icon_title:I = 0x7f090c3f

.field public static final motion2013_mute_pause_title:I = 0x7f090c44

.field public static final motion2013_palm_motion_title:I = 0x7f090c45

.field public static final motion2013_pan_title:I = 0x7f090c3e

.field public static final motion2013_pick_up_title:I = 0x7f090c3b

.field public static final motion2013_shake_title:I = 0x7f090c41

.field public static final motion2013_tilt_title:I = 0x7f090c3c

.field public static final motion2013_turn_over_title:I = 0x7f090c43

.field public static final motion2013_update_refresh_title:I = 0x7f090c42

.field public static final motion2013_zoom_title:I = 0x7f090c3d

.field public static final motion_activation:I = 0x7f090bde

.field public static final motion_advanced_settings:I = 0x7f090c84

.field public static final motion_camera_short_cut_summary:I = 0x7f090c36

.field public static final motion_camera_short_cut_title:I = 0x7f090c35

.field public static final motion_camera_short_cut_turn_on_motion:I = 0x7f090c37

.field public static final motion_camera_short_cut_will_turn_off:I = 0x7f090c33

.field public static final motion_disabled_dialog_use_motion_btn:I = 0x7f090c17

.field public static final motion_disabled_message:I = 0x7f090c19

.field public static final motion_disabled_title:I = 0x7f090c18

.field public static final motion_settings_title:I = 0x7f090bdd

.field public static final motion_summary:I = 0x7f090bdb

.field public static final motion_title:I = 0x7f090bda

.field public static final motion_tutorial_attention:I = 0x7f090c14

.field public static final motion_tutorial_done_btn:I = 0x7f090c11

.field public static final motion_tutorial_good_job:I = 0x7f090c0f

.field public static final motion_tutorial_settings_title:I = 0x7f090bdf

.field public static final motion_tutorial_title:I = 0x7f090bf8

.field public static final motion_tutorial_try_again_btn:I = 0x7f090c10

.field public static final motion_tutorial_try_btn:I = 0x7f090c0e

.field public static final motion_unlock_and_camera_short_cut_will_turn_off:I = 0x7f090c34

.field public static final motion_unlock_title:I = 0x7f090c2e

.field public static final motion_unlock_tutorial_content:I = 0x7f090c30

.field public static final motion_unlock_tutorial_content_2:I = 0x7f090c31

.field public static final motion_unlock_tutorial_title:I = 0x7f090c2f

.field public static final motion_unlock_will_turn_off:I = 0x7f090c32

.field public static final motions_and_gestures_menu_summary:I = 0x7f091266

.field public static final mouse_over_icon_labels_summary:I = 0x7f090cfe

.field public static final mouse_over_information_preview_summary:I = 0x7f090cfd

.field public static final mouse_over_list_scrolling_summary:I = 0x7f090cff

.field public static final mouse_over_magnetic_pointer_summary:I = 0x7f090d00

.field public static final mouse_over_magnetic_pointer_title:I = 0x7f090cfa

.field public static final mouse_over_progress_bar_preview_summary:I = 0x7f090d01

.field public static final mouse_over_setting_guide:I = 0x7f090cfc

.field public static final mouse_over_text_title:I = 0x7f090cf9

.field public static final move_app:I = 0x7f0906ae

.field public static final move_app_failed_dlg_text:I = 0x7f0906ba

.field public static final move_app_failed_dlg_title:I = 0x7f0906b9

.field public static final move_app_to_internal:I = 0x7f0906af

.field public static final move_app_to_sdcard:I = 0x7f0906b0

.field public static final moving:I = 0x7f0906b1

.field public static final mtp_ptp_mode_summary:I = 0x7f09057e

.field public static final multi_window_contents:I = 0x7f0904f2

.field public static final multi_window_summary:I = 0x7f0904f1

.field public static final multi_window_title:I = 0x7f0904f0

.field public static final multi_window_use_popup_msg:I = 0x7f090b87

.field public static final multi_window_use_popup_title:I = 0x7f090b86

.field public static final musicfx_title:I = 0x7f09049a

.field public static final mute:I = 0x7f090df1

.field public static final mwlan:I = 0x7f090f8e

.field public static final mwlan_allowed_2:I = 0x7f090f91

.field public static final mwlan_denied_2:I = 0x7f090f92

.field public static final mwlan_permission_summary_2:I = 0x7f090f90

.field public static final mwlan_permission_title_2:I = 0x7f090f8f

.field public static final my_profile_align:I = 0x7f090d09

.field public static final my_profile_center_align:I = 0x7f090d0f

.field public static final my_profile_date_text:I = 0x7f090d14

.field public static final my_profile_dialog_hint:I = 0x7f090d0c

.field public static final my_profile_edit:I = 0x7f090d08

.field public static final my_profile_edit_default:I = 0x7f090d10

.field public static final my_profile_edit_default2:I = 0x7f090d12

.field public static final my_profile_edit_default_vzw:I = 0x7f090d11

.field public static final my_profile_fontsize:I = 0x7f090d0a

.field public static final my_profile_left_align:I = 0x7f090d0d

.field public static final my_profile_preview:I = 0x7f090d0b

.field public static final my_profile_right_align:I = 0x7f090d0e

.field public static final my_profile_summary:I = 0x7f090d07

.field public static final my_profile_time_text:I = 0x7f090d13

.field public static final my_profile_title:I = 0x7f090d06

.field public static final my_sound_title:I = 0x7f0904c2

.field public static final myplace_add_summary:I = 0x7f0911d0

.field public static final myplace_already_exists:I = 0x7f0911d2

.field public static final myplace_car:I = 0x7f0911ce

.field public static final myplace_current_location:I = 0x7f0911dd

.field public static final myplace_default_name:I = 0x7f0911c8

.field public static final myplace_delete_dialog_text:I = 0x7f0911cf

.field public static final myplace_dialog_text_default:I = 0x7f0911d6

.field public static final myplace_heading:I = 0x7f0911c9

.field public static final myplace_home:I = 0x7f0911cc

.field public static final myplace_location_disabled_dialog_content:I = 0x7f0911dc

.field public static final myplace_location_title:I = 0x7f0911db

.field public static final myplace_map:I = 0x7f0911d5

.field public static final myplace_map_no_result:I = 0x7f0911d9

.field public static final myplace_menu_summary:I = 0x7f0911ca

.field public static final myplace_network_error:I = 0x7f0911d1

.field public static final myplace_office:I = 0x7f0911cd

.field public static final myplace_search:I = 0x7f0911de

.field public static final myplace_search_location_title:I = 0x7f0911da

.field public static final myplace_select_method:I = 0x7f0911d3

.field public static final myplace_select_method_text:I = 0x7f0911d4

.field public static final myplace_summary:I = 0x7f0911cb

.field public static final myplace_title:I = 0x7f0911c7

.field public static final myplace_wifi_auto_connect:I = 0x7f0911d7

.field public static final myplace_wifi_auto_connect_summary:I = 0x7f0911d8

.field public static final name_ex:I = 0x7f090da3

.field public static final negativecolors_use_popup_msg:I = 0x7f09079c

.field public static final negativecolors_use_popup_title:I = 0x7f09079b

.field public static final network_key_length_error:I = 0x7f090ac8

.field public static final network_security:I = 0x7f0911f2

.field public static final network_settings_title:I = 0x7f0905f9

.field public static final network_state_label:I = 0x7f090482

.field public static final networkid_label:I = 0x7f09048a

.field public static final networks:I = 0x7f090154

.field public static final never_turn_on_wifi:I = 0x7f090f14

.field public static final new_backup_pw_prompt:I = 0x7f090983

.field public static final new_email:I = 0x7f0909d6

.field public static final new_email_notification_summary:I = 0x7f0909d7

.field public static final new_password:I = 0x7f090032

.field public static final new_voicemail:I = 0x7f0909d8

.field public static final new_voicemail_notification_summary:I = 0x7f0909d9

.field public static final next:I = 0x7f091293

.field public static final next_button_label:I = 0x7f090652

.field public static final next_label:I = 0x7f090135

.field public static final nfc_explained:I = 0x7f09025f

.field public static final nfc_off_popup_title:I = 0x7f09125d

.field public static final nfc_quick_toggle_summary:I = 0x7f09025e

.field public static final nfc_quick_toggle_title:I = 0x7f09025d

.field public static final nfc_settings_help_summary:I = 0x7f09026b

.field public static final nfc_settings_rw_p2p_summary:I = 0x7f090261

.field public static final nfc_settings_rw_p2p_title:I = 0x7f090260

.field public static final nfc_toggle_disabled:I = 0x7f090002

.field public static final nfc_wallet_disabled_summary:I = 0x7f090278

.field public static final nfc_wallet_last_used:I = 0x7f09027a

.field public static final nfc_wallet_no_method:I = 0x7f090277

.field public static final nfc_wallet_title:I = 0x7f090276

.field public static final nfc_wallet_usim_wallet:I = 0x7f090279

.field public static final nine:I = 0x7f09003e

.field public static final no:I = 0x7f090048

.field public static final noContacts:I = 0x7f09109c

.field public static final no_application:I = 0x7f0908c3

.field public static final no_applications:I = 0x7f090693

.field public static final no_custom_vibrations:I = 0x7f090da5

.field public static final no_device_admins:I = 0x7f090859

.field public static final no_devices_found:I = 0x7f090fc7

.field public static final no_location_info:I = 0x7f090f76

.field public static final no_network_card:I = 0x7f091123

.field public static final no_running_services:I = 0x7f0906c9

.field public static final no_services:I = 0x7f0906d7

.field public static final normal:I = 0x7f090b35

.field public static final normal_date_format:I = 0x7f0900e8

.field public static final not_available_in_airplane_mode:I = 0x7f091274

.field public static final not_installed:I = 0x7f090692

.field public static final note:I = 0x7f090f15

.field public static final nothing_to_del:I = 0x7f090f72

.field public static final notification_airplane_mode:I = 0x7f0910bb

.field public static final notification_panel_air_gesture:I = 0x7f0910bf

.field public static final notification_panel_air_view:I = 0x7f0910bc

.field public static final notification_panel_apn:I = 0x7f0910b0

.field public static final notification_panel_blocking_mode:I = 0x7f0910b1

.field public static final notification_panel_bluetooth:I = 0x7f0910ae

.field public static final notification_panel_driving:I = 0x7f0910b8

.field public static final notification_panel_empty:I = 0x7f0910c0

.field public static final notification_panel_gps:I = 0x7f0910ab

.field public static final notification_panel_mobile_allshare_cast:I = 0x7f0910b3

.field public static final notification_panel_mobile_data:I = 0x7f0910af

.field public static final notification_panel_multiwindows:I = 0x7f0910b4

.field public static final notification_panel_nfc:I = 0x7f0910b7

.field public static final notification_panel_powersaving:I = 0x7f0910b2

.field public static final notification_panel_sbeam:I = 0x7f0910b6

.field public static final notification_panel_screen_rotation:I = 0x7f0910ad

.field public static final notification_panel_smartpause:I = 0x7f0910be

.field public static final notification_panel_smartscroll:I = 0x7f0910bd

.field public static final notification_panel_smartstay:I = 0x7f0910b9

.field public static final notification_panel_sound:I = 0x7f0910ac

.field public static final notification_panel_sync:I = 0x7f0910ba

.field public static final notification_panel_title:I = 0x7f0910a4

.field public static final notification_panel_unable_add_maximum:I = 0x7f0910c2

.field public static final notification_panel_unable_remove_minimum:I = 0x7f0910c1

.field public static final notification_panel_wifi:I = 0x7f0910aa

.field public static final notification_panel_wifihotspot:I = 0x7f0910b5

.field public static final notification_pulse_title:I = 0x7f0904a0

.field public static final notification_sound_dialog_title:I = 0x7f0904a4

.field public static final notification_sound_summary:I = 0x7f091140

.field public static final notification_sound_title:I = 0x7f09049f

.field public static final notification_volume_title:I = 0x7f0904a2

.field public static final notify_me_available_network:I = 0x7f090f16

.field public static final notify_me_available_network_summary:I = 0x7f090f17

.field public static final nousbdriver_attention_message:I = 0x7f09100e

.field public static final nousbdriver_attention_message_nokies:I = 0x7f09100f

.field public static final nsd_quick_toggle_summary:I = 0x7f09026a

.field public static final nsd_quick_toggle_title:I = 0x7f090269

.field public static final oem_radio_info_label:I = 0x7f090122

.field public static final ok_button:I = 0x7f0903de

.field public static final on_reading_usim:I = 0x7f090525

.field public static final one:I = 0x7f090036

.field public static final onehand_activate_calculator_summary:I = 0x7f090f55

.field public static final onehand_activate_calculator_title:I = 0x7f090f54

.field public static final onehand_activate_category:I = 0x7f090f4f

.field public static final onehand_activate_dialer_summary:I = 0x7f090f51

.field public static final onehand_activate_dialer_title:I = 0x7f090f50

.field public static final onehand_activate_pattern_summary:I = 0x7f090f57

.field public static final onehand_activate_pattern_title:I = 0x7f090f56

.field public static final onehand_activate_samsung_keypad_summary:I = 0x7f090f53

.field public static final onehand_activate_samsung_keypad_title:I = 0x7f090f52

.field public static final onehand_settings_title:I = 0x7f090f4e

.field public static final onehand_tutorial_category:I = 0x7f090f58

.field public static final onehand_tutorial_help_text:I = 0x7f090f5a

.field public static final onehand_tutorial_help_title:I = 0x7f090f59

.field public static final ongoing_backup_files:I = 0x7f0912bf

.field public static final ongoing_restore_files:I = 0x7f0912c0

.field public static final onscreen_keyboard_settings_summary:I = 0x7f090733

.field public static final ops_download:I = 0x7f090d4f

.field public static final ops_upload:I = 0x7f090d50

.field public static final optical_trackpad_sensivity:I = 0x7f09116b

.field public static final options_for_same_files:I = 0x7f0912b9

.field public static final other_accounts:I = 0x7f090ebe

.field public static final other_settings:I = 0x7f090fbf

.field public static final outdoor:I = 0x7f090df0

.field public static final overlay_display_devices_title:I = 0x7f0908ec

.field public static final overwrite_all:I = 0x7f0912ba

.field public static final owner_info_settings_edit_text_hint:I = 0x7f090174

.field public static final owner_info_settings_summary:I = 0x7f090173

.field public static final owner_info_settings_title:I = 0x7f090172

.field public static final packages_subtitle:I = 0x7f0907b9

.field public static final page_buddy:I = 0x7f091064

.field public static final page_buddy_help:I = 0x7f091066

.field public static final page_buddy_summary:I = 0x7f091065

.field public static final paging_mode_popup_title:I = 0x7f091135

.field public static final paging_mode_summary:I = 0x7f091113

.field public static final paging_mode_title:I = 0x7f091112

.field public static final palm_swipe_summary:I = 0x7f090c23

.field public static final palm_swipe_title:I = 0x7f090c22

.field public static final palm_touch_summary:I = 0x7f090c25

.field public static final palm_touch_title:I = 0x7f090c24

.field public static final pan_summary:I = 0x7f090bec

.field public static final pan_title:I = 0x7f090be3

.field public static final pan_to_browse_image_summary:I = 0x7f090bed

.field public static final pan_to_browse_image_title:I = 0x7f090be4

.field public static final pan_to_browse_image_tutorial_content:I = 0x7f090c04

.field public static final pan_tutorial_content:I = 0x7f090c03

.field public static final password:I = 0x7f090033

.field public static final password_expired:I = 0x7f090d15

.field public static final password_expired_cac:I = 0x7f0901b0

.field public static final pattern_name:I = 0x7f090da0

.field public static final pen:I = 0x7f090eb9

.field public static final pen_application_contextualpage:I = 0x7f090eb7

.field public static final pen_application_none:I = 0x7f090eb5

.field public static final pen_application_shortcutpanel:I = 0x7f090eb6

.field public static final pen_application_summary:I = 0x7f090eb4

.field public static final pen_application_title:I = 0x7f090eb3

.field public static final pen_detach_noti_off:I = 0x7f090ead

.field public static final pen_detach_noti_sound1:I = 0x7f090eae

.field public static final pen_detach_noti_sound2:I = 0x7f090eaf

.field public static final pen_detach_noti_sound3:I = 0x7f090eb0

.field public static final pen_detach_noti_title:I = 0x7f090eac

.field public static final pen_detachment_sound_title:I = 0x7f090b81

.field public static final pen_detect_mode_summary:I = 0x7f090eb2

.field public static final pen_detect_mode_title:I = 0x7f090eb1

.field public static final pen_gesture_guide_msg:I = 0x7f090b98

.field public static final pen_gesture_guide_title:I = 0x7f090b97

.field public static final pen_hand_side_title:I = 0x7f090b75

.field public static final pen_help_gesture:I = 0x7f090ba0

.field public static final pen_help_gesture_back_msg:I = 0x7f090bb4

.field public static final pen_help_gesture_back_title:I = 0x7f090bb3

.field public static final pen_help_gesture_capture_msg:I = 0x7f090bbc

.field public static final pen_help_gesture_capture_title:I = 0x7f090bbb

.field public static final pen_help_gesture_command_msg:I = 0x7f090bba

.field public static final pen_help_gesture_command_title:I = 0x7f090bb9

.field public static final pen_help_gesture_crop_msg:I = 0x7f090bbe

.field public static final pen_help_gesture_crop_title:I = 0x7f090bbd

.field public static final pen_help_gesture_menu_msg:I = 0x7f090bb6

.field public static final pen_help_gesture_menu_title:I = 0x7f090bb5

.field public static final pen_help_gesture_msg:I = 0x7f090bb2

.field public static final pen_help_gesture_note_msg:I = 0x7f090bb8

.field public static final pen_help_gesture_note_title:I = 0x7f090bb7

.field public static final pen_help_gesture_selection_msg:I = 0x7f090bc0

.field public static final pen_help_gesture_selection_title:I = 0x7f090bbf

.field public static final pen_help_gesture_title:I = 0x7f090bb1

.field public static final pen_help_hovering:I = 0x7f090b9f

.field public static final pen_help_hovering_button:I = 0x7f090ba5

.field public static final pen_help_hovering_icon_label_title:I = 0x7f090bad

.field public static final pen_help_hovering_icon_lable_msg:I = 0x7f090bae

.field public static final pen_help_hovering_info_tip_msg:I = 0x7f090baa

.field public static final pen_help_hovering_info_tip_title:I = 0x7f090ba9

.field public static final pen_help_hovering_list_scroll_msg:I = 0x7f090bb0

.field public static final pen_help_hovering_list_scroll_title:I = 0x7f090baf

.field public static final pen_help_hovering_msg:I = 0x7f090ba3

.field public static final pen_help_hovering_msg_hover:I = 0x7f090ba4

.field public static final pen_help_hovering_pointer_msg1:I = 0x7f090ba7

.field public static final pen_help_hovering_pointer_msg2:I = 0x7f090ba8

.field public static final pen_help_hovering_pointer_title:I = 0x7f090ba6

.field public static final pen_help_hovering_zoom_in_msg:I = 0x7f090bac

.field public static final pen_help_hovering_zoom_in_title:I = 0x7f090bab

.field public static final pen_help_msg:I = 0x7f090b77

.field public static final pen_help_quick_command:I = 0x7f090ba1

.field public static final pen_help_quickcmd_add_msg1:I = 0x7f090bd0

.field public static final pen_help_quickcmd_add_msg2:I = 0x7f090bd1

.field public static final pen_help_quickcmd_add_msg3:I = 0x7f090bd2

.field public static final pen_help_quickcmd_add_msg4:I = 0x7f090bd3

.field public static final pen_help_quickcmd_add_title:I = 0x7f090bcf

.field public static final pen_help_quickcmd_call_msg:I = 0x7f090bc3

.field public static final pen_help_quickcmd_call_title:I = 0x7f090bc2

.field public static final pen_help_quickcmd_delete_msg1:I = 0x7f090bd5

.field public static final pen_help_quickcmd_delete_msg2:I = 0x7f090bd6

.field public static final pen_help_quickcmd_delete_msg3:I = 0x7f090bd7

.field public static final pen_help_quickcmd_delete_msg4:I = 0x7f090bd8

.field public static final pen_help_quickcmd_delete_title:I = 0x7f090bd4

.field public static final pen_help_quickcmd_execute_inapp_msg1:I = 0x7f090bca

.field public static final pen_help_quickcmd_execute_inapp_msg2:I = 0x7f090bcb

.field public static final pen_help_quickcmd_execute_inapp_msg2_new:I = 0x7f090bcc

.field public static final pen_help_quickcmd_execute_inapp_title:I = 0x7f090bc9

.field public static final pen_help_quickcmd_execute_predefined_msg_a:I = 0x7f090bc7

.field public static final pen_help_quickcmd_execute_predefined_msg_b:I = 0x7f090bc8

.field public static final pen_help_quickcmd_execute_predefined_title:I = 0x7f090bc6

.field public static final pen_help_quickcmd_msg:I = 0x7f090bc1

.field public static final pen_help_quickcmd_use_msg:I = 0x7f090bce

.field public static final pen_help_quickcmd_use_title:I = 0x7f090bcd

.field public static final pen_help_quickcmd_write_msg:I = 0x7f090bc5

.field public static final pen_help_quickcmd_write_title:I = 0x7f090bc4

.field public static final pen_help_title:I = 0x7f090b76

.field public static final pen_hovering:I = 0x7f090eba

.field public static final pen_hovering_sound_title:I = 0x7f090ba2

.field public static final pen_hovering_summary:I = 0x7f090ebb

.field public static final pen_left:I = 0x7f090b80

.field public static final pen_open_quick_note:I = 0x7f090b84

.field public static final pen_open_quick_note_msg:I = 0x7f090b85

.field public static final pen_quick_command_msg:I = 0x7f090b9a

.field public static final pen_quick_command_title:I = 0x7f090b99

.field public static final pen_right:I = 0x7f090b7f

.field public static final pen_settings:I = 0x7f090eb8

.field public static final pen_settings_general_section_title:I = 0x7f090b70

.field public static final pen_settings_gesture_section_title:I = 0x7f090b72

.field public static final pen_settings_hovering_section_title:I = 0x7f090b71

.field public static final pen_settings_loss_prevention_section_title:I = 0x7f090b73

.field public static final pen_settings_s_pen_help_section_title:I = 0x7f090b74

.field public static final pen_settings_title:I = 0x7f090b6f

.field public static final percentage:I = 0x7f090987

.field public static final permissions_label:I = 0x7f090666

.field public static final personal_data:I = 0x7f0912ab

.field public static final personal_data_section_title:I = 0x7f090846

.field public static final personal_zone:I = 0x7f0912a3

.field public static final personal_zone_title:I = 0x7f09128b

.field public static final personalization:I = 0x7f091259

.field public static final phone_info_label:I = 0x7f09013c

.field public static final phone_language:I = 0x7f0906ea

.field public static final phone_language_summary:I = 0x7f0906eb

.field public static final phone_password_settings_category:I = 0x7f090030

.field public static final phone_password_settings_title:I = 0x7f090031

.field public static final phone_profile:I = 0x7f090ded

.field public static final phone_vibration_summary:I = 0x7f090d9a

.field public static final phone_vibration_summary_sound:I = 0x7f090d99

.field public static final phone_vibration_title:I = 0x7f090d98

.field public static final pick_up_summary:I = 0x7f090bf0

.field public static final pick_up_title:I = 0x7f090be7

.field public static final pick_up_to_call_out_content:I = 0x7f090c1c

.field public static final pick_up_to_call_out_summary:I = 0x7f090c1b

.field public static final pick_up_to_call_out_title:I = 0x7f090c1a

.field public static final pick_up_tutorial_content:I = 0x7f090c0a

.field public static final pick_up_tutorial_content_1:I = 0x7f090c0b

.field public static final pick_up_tutorial_content_2:I = 0x7f090c0c

.field public static final pico_installed:I = 0x7f090810

.field public static final pico_languages_and_voices:I = 0x7f09080f

.field public static final pico_not_installed:I = 0x7f090811

.field public static final pico_voice_summary_female:I = 0x7f090812

.field public static final pico_voice_summary_male:I = 0x7f090813

.field public static final ping_test_label:I = 0x7f09011d

.field public static final please_select_phase1:I = 0x7f0902e1

.field public static final please_select_phase2:I = 0x7f0902e2

.field public static final please_wait:I = 0x7f091148

.field public static final pm:I = 0x7f091004

.field public static final pointer_location:I = 0x7f0908cb

.field public static final pointer_location_summary:I = 0x7f0908cc

.field public static final pointer_settings_category:I = 0x7f0906f9

.field public static final pointer_speed:I = 0x7f0906fa

.field public static final power_bluetooth:I = 0x7f0907bc

.field public static final power_cell:I = 0x7f0907bd

.field public static final power_idle:I = 0x7f0907bf

.field public static final power_key_hold_summary:I = 0x7f090b44

.field public static final power_key_hold_title:I = 0x7f090b43

.field public static final power_on:I = 0x7f090026

.field public static final power_phone:I = 0x7f0907be

.field public static final power_saving_background_colour:I = 0x7f090b07

.field public static final power_saving_background_colour_summary:I = 0x7f090b08

.field public static final power_saving_background_colour_summary_without_browser:I = 0x7f090b09

.field public static final power_saving_cpu:I = 0x7f090b03

.field public static final power_saving_cpu_summary:I = 0x7f090b04

.field public static final power_saving_display:I = 0x7f090b05

.field public static final power_saving_display_summary:I = 0x7f090b06

.field public static final power_saving_haptic:I = 0x7f090b0b

.field public static final power_saving_haptic_summary:I = 0x7f090b0c

.field public static final power_saving_help_edit_bubble_summary:I = 0x7f0911f5

.field public static final power_saving_mode_on_summary:I = 0x7f090043

.field public static final power_saving_mode_setting:I = 0x7f090b02

.field public static final power_saving_mode_setting_category:I = 0x7f090b12

.field public static final power_saving_mode_title:I = 0x7f090b0f

.field public static final power_saving_off_message:I = 0x7f090f9a

.field public static final power_saving_screen_timeout:I = 0x7f090b0a

.field public static final power_saving_tips:I = 0x7f090b0e

.field public static final power_saving_tips_auto_sync_1:I = 0x7f090b28

.field public static final power_saving_tips_background_colour:I = 0x7f090b15

.field public static final power_saving_tips_background_colour_without_browser:I = 0x7f090b16

.field public static final power_saving_tips_bluetooth_1:I = 0x7f090b22

.field public static final power_saving_tips_bluetooth_2:I = 0x7f090b23

.field public static final power_saving_tips_bluetooth_3:I = 0x7f090b24

.field public static final power_saving_tips_brightness_1:I = 0x7f090b20

.field public static final power_saving_tips_brightness_2:I = 0x7f090b21

.field public static final power_saving_tips_cpu:I = 0x7f090b13

.field public static final power_saving_tips_display:I = 0x7f090b14

.field public static final power_saving_tips_gps_1:I = 0x7f090b29

.field public static final power_saving_tips_gps_2:I = 0x7f090b2a

.field public static final power_saving_tips_gps_3:I = 0x7f090b2b

.field public static final power_saving_tips_haptic:I = 0x7f090b18

.field public static final power_saving_tips_live_wallpaper_1:I = 0x7f090b2c

.field public static final power_saving_tips_mobile_data_1:I = 0x7f090b25

.field public static final power_saving_tips_mobile_data_2:I = 0x7f090b26

.field public static final power_saving_tips_mobile_data_3:I = 0x7f090b27

.field public static final power_saving_tips_screen_1:I = 0x7f090b1d

.field public static final power_saving_tips_screen_2:I = 0x7f090b1e

.field public static final power_saving_tips_screen_3:I = 0x7f090b1f

.field public static final power_saving_tips_timeout:I = 0x7f090b17

.field public static final power_saving_widget_tip:I = 0x7f090b19

.field public static final power_saving_widget_tip_2:I = 0x7f090b1a

.field public static final power_screen:I = 0x7f0907ba

.field public static final power_title:I = 0x7f090025

.field public static final power_usage_level_and_status:I = 0x7f0907a5

.field public static final power_usage_not_available:I = 0x7f0907a4

.field public static final power_usage_summary:I = 0x7f0907a3

.field public static final power_usage_summary_title:I = 0x7f0907a2

.field public static final power_wifi:I = 0x7f0907bb

.field public static final powersavingmode_start:I = 0x7f090b0d

.field public static final preference_change_password_title:I = 0x7f0908a6

.field public static final preset_pattern:I = 0x7f090da9

.field public static final prev_button_label:I = 0x7f090f32

.field public static final privacy_alert:I = 0x7f09102a

.field public static final privacy_alert_booster_msg1:I = 0x7f091036

.field public static final privacy_alert_booster_msg2:I = 0x7f091037

.field public static final privacy_alert_booster_msg4:I = 0x7f091038

.field public static final privacy_alert_booster_title:I = 0x7f091035

.field public static final privacy_alert_contents:I = 0x7f09102b

.field public static final privacy_alert_generic_msg1:I = 0x7f091032

.field public static final privacy_alert_generic_msg2:I = 0x7f091033

.field public static final privacy_alert_generic_msg4:I = 0x7f091034

.field public static final privacy_alert_proceed_button:I = 0x7f09102c

.field public static final privacy_alert_sprint_msg1:I = 0x7f09102e

.field public static final privacy_alert_sprint_msg2:I = 0x7f09102f

.field public static final privacy_alert_sprint_msg3:I = 0x7f091030

.field public static final privacy_alert_sprint_msg4:I = 0x7f091031

.field public static final privacy_alert_sprint_title:I = 0x7f09102d

.field public static final privacy_alert_virgin_msg1:I = 0x7f09103a

.field public static final privacy_alert_virgin_msg2:I = 0x7f09103b

.field public static final privacy_alert_virgin_msg3:I = 0x7f09103c

.field public static final privacy_alert_virgin_msg4:I = 0x7f09103d

.field public static final privacy_alert_virgin_title:I = 0x7f091039

.field public static final privacy_settings:I = 0x7f090843

.field public static final privacy_settings_title:I = 0x7f090844

.field public static final private_data:I = 0x7f0912ac

.field public static final private_zone:I = 0x7f0912a4

.field public static final private_zone_title:I = 0x7f09128c

.field public static final process_kernel_label:I = 0x7f0907e6

.field public static final process_mediaserver_label:I = 0x7f0907e7

.field public static final process_provider_in_use_description:I = 0x7f0906e2

.field public static final process_service_in_use_description:I = 0x7f0906e1

.field public static final profile_add:I = 0x7f090e4e

.field public static final profile_exit:I = 0x7f090e4f

.field public static final profile_name_limit:I = 0x7f090f70

.field public static final progress_scanning:I = 0x7f0900c5

.field public static final progress_status:I = 0x7f090d69

.field public static final progress_tap_to_pair:I = 0x7f0900c6

.field public static final provider_label:I = 0x7f0908ae

.field public static final proxy_action_text:I = 0x7f0900f8

.field public static final proxy_clear_text:I = 0x7f0900f2

.field public static final proxy_defaultView_text:I = 0x7f0900f7

.field public static final proxy_error:I = 0x7f0900fb

.field public static final proxy_error_dismiss:I = 0x7f0900fc

.field public static final proxy_error_empty_host_set_port:I = 0x7f090100

.field public static final proxy_error_empty_port:I = 0x7f0900ff

.field public static final proxy_error_invalid_exclusion_list:I = 0x7f0900fe

.field public static final proxy_error_invalid_host:I = 0x7f0900fd

.field public static final proxy_error_invalid_port:I = 0x7f090101

.field public static final proxy_exclusionlist_hint:I = 0x7f0900f6

.field public static final proxy_exclusionlist_label:I = 0x7f0900f5

.field public static final proxy_hostname_hint:I = 0x7f0900fa

.field public static final proxy_hostname_label:I = 0x7f0900f9

.field public static final proxy_port_hint:I = 0x7f0900f4

.field public static final proxy_port_label:I = 0x7f0900f3

.field public static final proxy_settings_label:I = 0x7f09013e

.field public static final proxy_settings_title:I = 0x7f0900f1

.field public static final proxy_warning_limited_support:I = 0x7f090102

.field public static final push_message:I = 0x7f0909b9

.field public static final push_message_summary:I = 0x7f0909ba

.field public static final pwdMessage:I = 0x7f09002f

.field public static final question_for_cancel:I = 0x7f0912a9

.field public static final question_for_restoration:I = 0x7f09129a

.field public static final quick_applications:I = 0x7f090f98

.field public static final quick_camera:I = 0x7f090f97

.field public static final quick_launch_assign_application:I = 0x7f09071c

.field public static final quick_launch_clear_cancel_button:I = 0x7f090722

.field public static final quick_launch_clear_dialog_message:I = 0x7f090720

.field public static final quick_launch_clear_dialog_title:I = 0x7f09071f

.field public static final quick_launch_clear_ok_button:I = 0x7f090721

.field public static final quick_launch_display_mode_applications:I = 0x7f090723

.field public static final quick_launch_display_mode_shortcuts:I = 0x7f090724

.field public static final quick_launch_no_shortcut:I = 0x7f09071d

.field public static final quick_launch_shortcut:I = 0x7f09071e

.field public static final quick_launch_summary:I = 0x7f09071b

.field public static final quick_launch_title:I = 0x7f09071a

.field public static final quick_note_summary:I = 0x7f09100d

.field public static final quick_note_title:I = 0x7f09100c

.field public static final quick_screen_capture:I = 0x7f090f96

.field public static final radioInfo_cid:I = 0x7f090073

.field public static final radioInfo_data_connected:I = 0x7f09006b

.field public static final radioInfo_data_connecting:I = 0x7f09006a

.field public static final radioInfo_data_disconnected:I = 0x7f090069

.field public static final radioInfo_data_suspended:I = 0x7f09006c

.field public static final radioInfo_display_asu:I = 0x7f090071

.field public static final radioInfo_display_bytes:I = 0x7f09006f

.field public static final radioInfo_display_dbm:I = 0x7f090070

.field public static final radioInfo_display_packets:I = 0x7f09006e

.field public static final radioInfo_lac:I = 0x7f090072

.field public static final radioInfo_menu_getPDP:I = 0x7f09005e

.field public static final radioInfo_menu_viewADN:I = 0x7f09005b

.field public static final radioInfo_menu_viewFDN:I = 0x7f09005c

.field public static final radioInfo_menu_viewSDN:I = 0x7f09005d

.field public static final radioInfo_phone_idle:I = 0x7f090066

.field public static final radioInfo_phone_offhook:I = 0x7f090068

.field public static final radioInfo_phone_ringing:I = 0x7f090067

.field public static final radioInfo_roaming_in:I = 0x7f090064

.field public static final radioInfo_roaming_not:I = 0x7f090065

.field public static final radioInfo_service_emergency:I = 0x7f090061

.field public static final radioInfo_service_in:I = 0x7f09005f

.field public static final radioInfo_service_off:I = 0x7f090062

.field public static final radioInfo_service_out:I = 0x7f090060

.field public static final radioInfo_unknown:I = 0x7f09006d

.field public static final radio_controls_summary:I = 0x7f09014a

.field public static final radio_controls_title:I = 0x7f090148

.field public static final radio_info_band_mode_label:I = 0x7f090117

.field public static final radio_info_call_redirect_label:I = 0x7f09010a

.field public static final radio_info_call_status_label:I = 0x7f090112

.field public static final radio_info_cellinfo_label:I = 0x7f090105

.field public static final radio_info_current_network_label:I = 0x7f09010d

.field public static final radio_info_data_attempts_label:I = 0x7f090106

.field public static final radio_info_data_successes_label:I = 0x7f09010e

.field public static final radio_info_gprs_service_label:I = 0x7f090107

.field public static final radio_info_gsm_disconnects_label:I = 0x7f09010c

.field public static final radio_info_gsm_service_label:I = 0x7f090110

.field public static final radio_info_http_client_test:I = 0x7f09011c

.field public static final radio_info_imei_label:I = 0x7f090109

.field public static final radio_info_message_waiting_label:I = 0x7f090115

.field public static final radio_info_neighboring_location_label:I = 0x7f090104

.field public static final radio_info_network_type_label:I = 0x7f090118

.field public static final radio_info_phone_number_label:I = 0x7f090116

.field public static final radio_info_ping_hostname:I = 0x7f09011b

.field public static final radio_info_ping_ipaddr:I = 0x7f09011a

.field public static final radio_info_ppp_received_label:I = 0x7f09010f

.field public static final radio_info_ppp_resets_label:I = 0x7f09010b

.field public static final radio_info_ppp_sent_label:I = 0x7f090113

.field public static final radio_info_radio_resets_label:I = 0x7f090114

.field public static final radio_info_roaming_label:I = 0x7f090108

.field public static final radio_info_set_perferred_label:I = 0x7f090119

.field public static final radio_info_signal_location_label:I = 0x7f090103

.field public static final radio_info_signal_strength_label:I = 0x7f090111

.field public static final radio_info_smsc_label:I = 0x7f09011e

.field public static final radio_info_smsc_refresh_label:I = 0x7f090120

.field public static final radio_info_smsc_update_label:I = 0x7f09011f

.field public static final radio_info_toggle_dns_check_label:I = 0x7f090121

.field public static final random_cid_title:I = 0x7f090f60

.field public static final rcs_settings_title:I = 0x7f090f1e

.field public static final read_only:I = 0x7f0912cd

.field public static final real_cid_summary:I = 0x7f090f61

.field public static final real_cid_title:I = 0x7f090f5e

.field public static final real_cid_title1:I = 0x7f090f5f

.field public static final really_remove_account_message:I = 0x7f0908ac

.field public static final really_remove_account_title:I = 0x7f0908ab

.field public static final recognizer_settings_summary:I = 0x7f0907f1

.field public static final recognizer_settings_title:I = 0x7f0907f0

.field public static final recognizer_title:I = 0x7f0907ef

.field public static final recommend_applications:I = 0x7f09106d

.field public static final recommend_applications_help_1:I = 0x7f09106e

.field public static final recommend_applications_help_2:I = 0x7f09106f

.field public static final recompute_size:I = 0x7f090696

.field public static final register_icon:I = 0x7f09111e

.field public static final register_icon_summary:I = 0x7f091120

.field public static final register_icon_title:I = 0x7f091118

.field public static final register_name:I = 0x7f09111c

.field public static final register_name_summary:I = 0x7f09111f

.field public static final register_name_title:I = 0x7f09111d

.field public static final register_network_card_help_title:I = 0x7f091122

.field public static final register_network_card_popup_content:I = 0x7f09111b

.field public static final register_network_card_popup_title:I = 0x7f09111a

.field public static final register_network_card_title:I = 0x7f091119

.field public static final regulatory_information:I = 0x7f09061c

.field public static final remote_controls:I = 0x7f090a3c

.field public static final remote_controls_mbody:I = 0x7f090a3e

.field public static final remote_controls_message:I = 0x7f090a44

.field public static final remote_controls_summary:I = 0x7f090a3d

.field public static final remove_account_failed:I = 0x7f0908ad

.field public static final remove_account_label:I = 0x7f0908a8

.field public static final remove_device_admin:I = 0x7f090857

.field public static final remove_network:I = 0x7f0902c7

.field public static final remove_samsung_account:I = 0x7f090a40

.field public static final rename_pattern:I = 0x7f090da6

.field public static final reset_app_preferences:I = 0x7f090686

.field public static final reset_app_preferences_button:I = 0x7f090689

.field public static final reset_app_preferences_desc:I = 0x7f090688

.field public static final reset_app_preferences_title:I = 0x7f090687

.field public static final reset_popup:I = 0x7f090fc6

.field public static final reset_settings:I = 0x7f090fc5

.field public static final restart_single_sign_on:I = 0x7f091081

.field public static final restore_default_apn:I = 0x7f0905b8

.field public static final restore_default_apn_completed:I = 0x7f0905ba

.field public static final restore_folders:I = 0x7f0912b8

.field public static final restore_title:I = 0x7f0912a0

.field public static final retrieving_data_from_samsung_account:I = 0x7f0908a3

.field public static final right:I = 0x7f0910a3

.field public static final ring_volume_title:I = 0x7f09049b

.field public static final ringtone_add:I = 0x7f091109

.field public static final ringtone_after_vibration:I = 0x7f090d1c

.field public static final ringtone_summary:I = 0x7f090498

.field public static final ringtone_summary_ds:I = 0x7f091141

.field public static final ringtone_title:I = 0x7f090497

.field public static final ripple_effect:I = 0x7f090fac

.field public static final ripple_effect_second_summary:I = 0x7f091029

.field public static final ripple_effect_summary:I = 0x7f090fad

.field public static final roaming:I = 0x7f09014b

.field public static final roaming_auto_dial:I = 0x7f09126f

.field public static final roaming_current:I = 0x7f09126b

.field public static final roaming_current_title:I = 0x7f09126a

.field public static final roaming_data_warning_msg_3G:I = 0x7f091275

.field public static final roaming_disable:I = 0x7f09014d

.field public static final roaming_enable:I = 0x7f09014c

.field public static final roaming_guide:I = 0x7f091270

.field public static final roaming_join_cancel:I = 0x7f091271

.field public static final roaming_reenable_message:I = 0x7f09014e

.field public static final roaming_reenable_title:I = 0x7f090153

.field public static final roaming_selection:I = 0x7f09126c

.field public static final roaming_settings_summary:I = 0x7f090ebd

.field public static final roaming_settings_title:I = 0x7f090ebc

.field public static final roaming_turn_it_on_button:I = 0x7f09014f

.field public static final roaming_use_data_roaming:I = 0x7f09126d

.field public static final roaming_use_lte_roaming:I = 0x7f09126e

.field public static final roaming_warning:I = 0x7f090150

.field public static final roaming_warning_att:I = 0x7f090151

.field public static final roaming_warning_multiuser:I = 0x7f090152

.field public static final rssi_label:I = 0x7f090484

.field public static final running_process_item_removed_user_label:I = 0x7f0906d1

.field public static final running_process_item_user_label:I = 0x7f0906d0

.field public static final running_processes_item_description_p_p:I = 0x7f0906d5

.field public static final running_processes_item_description_p_s:I = 0x7f0906d4

.field public static final running_processes_item_description_s_p:I = 0x7f0906d3

.field public static final running_processes_item_description_s_s:I = 0x7f0906d2

.field public static final runningservicedetails_processes_title:I = 0x7f0906d9

.field public static final runningservicedetails_services_title:I = 0x7f0906d8

.field public static final runningservicedetails_settings_title:I = 0x7f0906d6

.field public static final runningservicedetails_stop_dlg_text:I = 0x7f0906e4

.field public static final runningservicedetails_stop_dlg_title:I = 0x7f0906e3

.field public static final runningservices_settings_summary:I = 0x7f0906c6

.field public static final runningservices_settings_title:I = 0x7f0906c5

.field public static final s_beam_dependency:I = 0x7f090273

.field public static final s_beam_disabled_summary:I = 0x7f09026f

.field public static final s_beam_explained:I = 0x7f090271

.field public static final s_beam_explained_wo_snote:I = 0x7f090272

.field public static final s_beam_it_policy_not_allow_wifi:I = 0x7f090275

.field public static final s_beam_label:I = 0x7f090270

.field public static final s_beam_nfc_n_abeam_turnon:I = 0x7f090274

.field public static final s_beam_off_summary:I = 0x7f09026e

.field public static final s_beam_on_summary:I = 0x7f09026d

.field public static final s_beam_settings_title:I = 0x7f09026c

.field public static final s_motion_title:I = 0x7f090bd9

.field public static final s_voice_settings_title:I = 0x7f0907eb

.field public static final safety_assurance_menu_summary:I = 0x7f091264

.field public static final safety_assurance_settings:I = 0x7f090b01

.field public static final safety_zone_add_current_location:I = 0x7f090af8

.field public static final safety_zone_add_current_location_summary:I = 0x7f090af9

.field public static final safety_zone_already_used:I = 0x7f090af0

.field public static final safety_zone_ask_confirmation:I = 0x7f090aee

.field public static final safety_zone_ask_confirmation_summary:I = 0x7f090aef

.field public static final safety_zone_change_ap:I = 0x7f090b00

.field public static final safety_zone_delete_location:I = 0x7f090afb

.field public static final safety_zone_location:I = 0x7f090af7

.field public static final safety_zone_notification_summary:I = 0x7f090afd

.field public static final safety_zone_notification_vibrate:I = 0x7f090afe

.field public static final safety_zone_notification_vibrate_summary:I = 0x7f090aff

.field public static final safety_zone_off_notice:I = 0x7f090af6

.field public static final safety_zone_on_notice:I = 0x7f090af3

.field public static final safety_zone_rename_location:I = 0x7f090afa

.field public static final safety_zone_rename_location_content:I = 0x7f090afc

.field public static final safety_zone_summary:I = 0x7f090af1

.field public static final safety_zone_summary_att:I = 0x7f090af2

.field public static final safety_zone_title:I = 0x7f090aed

.field public static final safety_zone_turn_wireless_network01:I = 0x7f090af4

.field public static final safety_zone_turn_wireless_network02:I = 0x7f090af5

.field public static final samba_access_from_pc:I = 0x7f0911fd

.field public static final samba_access_log:I = 0x7f0911fb

.field public static final samba_config_cancle:I = 0x7f09121a

.field public static final samba_config_ok:I = 0x7f091219

.field public static final samba_configname:I = 0x7f09120a

.field public static final samba_configure:I = 0x7f0911fa

.field public static final samba_configure_text:I = 0x7f091207

.field public static final samba_current_access_count:I = 0x7f091212

.field public static final samba_current_list:I = 0x7f09120f

.field public static final samba_delete:I = 0x7f091210

.field public static final samba_delete_log:I = 0x7f09120e

.field public static final samba_done:I = 0x7f09120d

.field public static final samba_info_title:I = 0x7f091211

.field public static final samba_init_noti_summary:I = 0x7f091218

.field public static final samba_max_passwd:I = 0x7f09121f

.field public static final samba_msg:I = 0x7f0911fe

.field public static final samba_netbios_ip:I = 0x7f091205

.field public static final samba_netbios_name:I = 0x7f0911ff

.field public static final samba_netbiosname_dialog:I = 0x7f091217

.field public static final samba_netbiosname_dialog_Title:I = 0x7f091216

.field public static final samba_network:I = 0x7f091200

.field public static final samba_network_lan:I = 0x7f091201

.field public static final samba_network_not:I = 0x7f091203

.field public static final samba_network_wifi:I = 0x7f091202

.field public static final samba_no_access_client:I = 0x7f09121c

.field public static final samba_no_access_log:I = 0x7f09121d

.field public static final samba_passwd:I = 0x7f091208

.field public static final samba_popup_network_connect:I = 0x7f09120b

.field public static final samba_popup_ok:I = 0x7f09120c

.field public static final samba_server_info:I = 0x7f0911fc

.field public static final samba_settings_title:I = 0x7f0911f9

.field public static final samba_show_passwd:I = 0x7f091209

.field public static final samba_support_windowos:I = 0x7f091204

.field public static final samba_username_dialog:I = 0x7f091215

.field public static final samba_username_dialog_Title:I = 0x7f091214

.field public static final samba_windowos_plan:I = 0x7f091206

.field public static final samba_wrong_passwd:I = 0x7f09121e

.field public static final sambatabhost:I = 0x7f091213

.field public static final samsung_Sbackup:I = 0x7f090fa8

.field public static final samsung_backup:I = 0x7f090fa7

.field public static final samsung_restore:I = 0x7f090fa9

.field public static final samsung_storage_usage:I = 0x7f090fa6

.field public static final samsung_unlock:I = 0x7f0909cb

.field public static final samsung_unlock_option_say_your_wakeup_command_to_unlock_screen:I = 0x7f090fa3

.field public static final samsung_unlock_option_set_wakeup_command:I = 0x7f090fa4

.field public static final samsung_update:I = 0x7f0909b3

.field public static final save_error:I = 0x7f090d9e

.field public static final save_pattern:I = 0x7f090d9f

.field public static final save_pattern_decription:I = 0x7f090da1

.field public static final sbeam_menu_summary:I = 0x7f091262

.field public static final sc_pin_dialog_associationerror:I = 0x7f0901a7

.field public static final sc_pin_dialog_carderror:I = 0x7f0901ab

.field public static final sc_pin_dialog_cardexpired:I = 0x7f0901ac

.field public static final sc_pin_dialog_cardlocked:I = 0x7f0901aa

.field public static final sc_pin_dialog_connectionerror:I = 0x7f0901a8

.field public static final sc_pin_dialog_invalidpin:I = 0x7f0901a9

.field public static final sc_pin_dialog_register_message:I = 0x7f0901ad

.field public static final sc_pin_dialog_registererror:I = 0x7f0901a6

.field public static final sc_pin_dialog_verify_message:I = 0x7f0901ae

.field public static final sc_pin_dialog_wait_message:I = 0x7f0901af

.field public static final scan_ap:I = 0x7f090f6c

.field public static final scan_list_label:I = 0x7f09048c

.field public static final scan_wimax:I = 0x7f0911ba

.field public static final schedule:I = 0x7f0909dc

.field public static final schedule_notification_summary:I = 0x7f0909dd

.field public static final scloud_account_signin:I = 0x7f0910df

.field public static final scloud_account_signout:I = 0x7f0910e0

.field public static final scloud_add_samsung_account:I = 0x7f0910c7

.field public static final scloud_apply:I = 0x7f0910d4

.field public static final scloud_auth_fail:I = 0x7f0910e2

.field public static final scloud_back:I = 0x7f0910fe

.field public static final scloud_cloud_service_with_dropbox:I = 0x7f0910c6

.field public static final scloud_current_wallpaper:I = 0x7f091102

.field public static final scloud_data_sync:I = 0x7f0910c5

.field public static final scloud_disable_wifi_only_body:I = 0x7f0910eb

.field public static final scloud_disable_wifi_only_title:I = 0x7f0910ea

.field public static final scloud_doc_sync_summary:I = 0x7f0910f0

.field public static final scloud_document:I = 0x7f0910cf

.field public static final scloud_document_empty_text:I = 0x7f0910dc

.field public static final scloud_document_empty_text_v2:I = 0x7f0910de

.field public static final scloud_documents:I = 0x7f0910ee

.field public static final scloud_dropbox:I = 0x7f0910ca

.field public static final scloud_enable:I = 0x7f0910d2

.field public static final scloud_gallery_app_name:I = 0x7f0910fa

.field public static final scloud_internet:I = 0x7f0910fd

.field public static final scloud_link_account:I = 0x7f0910f1

.field public static final scloud_link_account_summary_documents:I = 0x7f0910f5

.field public static final scloud_link_account_summary_music:I = 0x7f0910f6

.field public static final scloud_link_account_summary_pictures:I = 0x7f0910f3

.field public static final scloud_link_account_summary_prefix:I = 0x7f0910f2

.field public static final scloud_link_account_summary_videos:I = 0x7f0910f4

.field public static final scloud_logs:I = 0x7f0910ff

.field public static final scloud_memo:I = 0x7f0910fc

.field public static final scloud_mms:I = 0x7f091101

.field public static final scloud_music:I = 0x7f0910ed

.field public static final scloud_music_app_name:I = 0x7f0910f8

.field public static final scloud_music_empty_text_v2:I = 0x7f0910dd

.field public static final scloud_musics:I = 0x7f0910ce

.field public static final scloud_musics_empty_text:I = 0x7f0910db

.field public static final scloud_network_fail:I = 0x7f0910e3

.field public static final scloud_number_of_displaying_photos:I = 0x7f0910d0

.field public static final scloud_off:I = 0x7f0910d3

.field public static final scloud_photos:I = 0x7f0910cc

.field public static final scloud_photos_empty_text:I = 0x7f0910d5

.field public static final scloud_photos_empty_text_v2:I = 0x7f0910d6

.field public static final scloud_photos_empty_text_v3:I = 0x7f0910d7

.field public static final scloud_progress:I = 0x7f0910e1

.field public static final scloud_recent_photos:I = 0x7f0910d1

.field public static final scloud_remove_cache_button:I = 0x7f0910ec

.field public static final scloud_remove_cache_warning:I = 0x7f0910e4

.field public static final scloud_remove_cache_warning_title:I = 0x7f0910e5

.field public static final scloud_remove_warning_option:I = 0x7f0910e6

.field public static final scloud_samsung:I = 0x7f0910c4

.field public static final scloud_samsung_cloud_service:I = 0x7f0910c9

.field public static final scloud_set_sync_account:I = 0x7f0910c8

.field public static final scloud_settings:I = 0x7f0910c3

.field public static final scloud_sms:I = 0x7f091100

.field public static final scloud_storage_usage:I = 0x7f0910cb

.field public static final scloud_sync_summary:I = 0x7f0910ef

.field public static final scloud_verify_account:I = 0x7f0910fb

.field public static final scloud_video_app_name:I = 0x7f0910f9

.field public static final scloud_videos:I = 0x7f0910cd

.field public static final scloud_videos_empty_text:I = 0x7f0910d8

.field public static final scloud_videos_empty_text_v2:I = 0x7f0910d9

.field public static final scloud_videos_empty_text_v3:I = 0x7f0910da

.field public static final scloud_wifi_only:I = 0x7f0910e7

.field public static final scloud_wifi_only_summary:I = 0x7f0910e8

.field public static final scloud_wifi_only_summary_doc:I = 0x7f0910f7

.field public static final scloud_wifi_only_sync_summary:I = 0x7f0910e9

.field public static final screen_compatibility_label:I = 0x7f090665

.field public static final screen_compatibility_text:I = 0x7f09067e

.field public static final screen_display:I = 0x7f0909e5

.field public static final screen_mode_auto:I = 0x7f090a63

.field public static final screen_mode_auto_contents:I = 0x7f090a65

.field public static final screen_mode_auto_summary:I = 0x7f090a64

.field public static final screen_mode_dynamic:I = 0x7f090a56

.field public static final screen_mode_movie:I = 0x7f090a58

.field public static final screen_mode_natural:I = 0x7f090a59

.field public static final screen_mode_setting:I = 0x7f090a54

.field public static final screen_mode_standard:I = 0x7f090a57

.field public static final screen_mode_summary:I = 0x7f090a55

.field public static final screen_quick_launch_summary:I = 0x7f090f95

.field public static final screen_timeout:I = 0x7f0904de

.field public static final screen_timeout_noti:I = 0x7f09002b

.field public static final screen_timeout_summary:I = 0x7f0904df

.field public static final screensaver_activate_on_dock_title:I = 0x7f0912cc

.field public static final screensaver_component_title:I = 0x7f0912c7

.field public static final screensaver_settings_button:I = 0x7f0904eb

.field public static final screensaver_settings_disabled_prompt:I = 0x7f0904e8

.field public static final screensaver_settings_dream_start:I = 0x7f0904ea

.field public static final screensaver_settings_summary_dock:I = 0x7f0904e6

.field public static final screensaver_settings_summary_either_long:I = 0x7f0904e3

.field public static final screensaver_settings_summary_either_short:I = 0x7f0904e4

.field public static final screensaver_settings_summary_off:I = 0x7f0904e7

.field public static final screensaver_settings_summary_on:I = 0x7f0912c6

.field public static final screensaver_settings_summary_sleep:I = 0x7f0904e5

.field public static final screensaver_settings_title:I = 0x7f0904e2

.field public static final screensaver_settings_when_to_dream:I = 0x7f0904e9

.field public static final screensaver_test:I = 0x7f0912cb

.field public static final screensaver_timeout_summary:I = 0x7f0912c9

.field public static final screensaver_timeout_title:I = 0x7f0912c8

.field public static final screensaver_timeout_zero_summary:I = 0x7f0912ca

.field public static final screentimeout_summary:I = 0x7f090b10

.field public static final scroll_acceleration_off:I = 0x7f090e38

.field public static final scroll_acceleration_on:I = 0x7f090e37

.field public static final scroll_mode_face_tracking:I = 0x7f090e32

.field public static final scroll_mode_title:I = 0x7f090e30

.field public static final scroll_mode_wrist_angle:I = 0x7f090e31

.field public static final scroll_speed_fast:I = 0x7f090e3a

.field public static final scroll_speed_slow:I = 0x7f090e39

.field public static final scroll_type_continuous_scroll:I = 0x7f090e34

.field public static final scroll_type_stop_scroll:I = 0x7f090e33

.field public static final scroll_unit_block:I = 0x7f090e35

.field public static final scroll_unit_line:I = 0x7f090e36

.field public static final sd_card_settings_label:I = 0x7f09013d

.field public static final sd_card_storage:I = 0x7f090695

.field public static final sd_eject:I = 0x7f090573

.field public static final sd_eject_summary:I = 0x7f090574

.field public static final sd_ejecting_summary:I = 0x7f090585

.field public static final sd_ejecting_title:I = 0x7f090584

.field public static final sd_encrypt_dialog:I = 0x7f090004

.field public static final sd_encrypt_title:I = 0x7f090006

.field public static final sd_format:I = 0x7f090578

.field public static final sd_format_sd:I = 0x7f090579

.field public static final sd_format_summary:I = 0x7f09057a

.field public static final sd_format_summary_sd:I = 0x7f09057b

.field public static final sd_insert_summary:I = 0x7f090575

.field public static final sd_memory:I = 0x7f090567

.field public static final sd_mount:I = 0x7f090576

.field public static final sd_mount_summary:I = 0x7f090577

.field public static final sd_waiting_encryption_decryption:I = 0x7f090ae7

.field public static final sdcard_changes_instructions:I = 0x7f090128

.field public static final sdcard_confirm_action:I = 0x7f090aad

.field public static final sdcard_decrypt_err_notification:I = 0x7f090ad7

.field public static final sdcard_decrypt_error:I = 0x7f090ae5

.field public static final sdcard_decrypt_full_error_gb:I = 0x7f090ae1

.field public static final sdcard_decrypt_full_error_mb:I = 0x7f090ae3

.field public static final sdcard_decrypt_needed:I = 0x7f090ac0

.field public static final sdcard_decrypt_notification:I = 0x7f090add

.field public static final sdcard_decrypt_policy_set:I = 0x7f090acc

.field public static final sdcard_decrypt_policy_set_confirm:I = 0x7f090aca

.field public static final sdcard_decrypt_policy_set_decryption_configured:I = 0x7f090ace

.field public static final sdcard_decrypt_policy_set_decryption_configured_confirm:I = 0x7f090ad4

.field public static final sdcard_decrypt_title_notification:I = 0x7f090adb

.field public static final sdcard_decrypt_title_ongoing_dec_second_storage:I = 0x7f090ad9

.field public static final sdcard_encdec_unavail_warning:I = 0x7f090ad1

.field public static final sdcard_encdec_usability_warning:I = 0x7f090ad2

.field public static final sdcard_encrypt_applied_admin:I = 0x7f090ab6

.field public static final sdcard_encrypt_apply:I = 0x7f090aab

.field public static final sdcard_encrypt_apply_desc:I = 0x7f090abe

.field public static final sdcard_encrypt_continue:I = 0x7f090aac

.field public static final sdcard_encrypt_disabled:I = 0x7f090ab4

.field public static final sdcard_encrypt_disabled_by_admin:I = 0x7f090aa8

.field public static final sdcard_encrypt_disabled_ongoing_dec:I = 0x7f090aaa

.field public static final sdcard_encrypt_disabled_ongoing_enc:I = 0x7f090aa9

.field public static final sdcard_encrypt_enable_desc:I = 0x7f090ab8

.field public static final sdcard_encrypt_enc_error:I = 0x7f090ad5

.field public static final sdcard_encrypt_err_notification:I = 0x7f090ad6

.field public static final sdcard_encrypt_error:I = 0x7f090ae4

.field public static final sdcard_encrypt_exclude_mm_off:I = 0x7f090aa7

.field public static final sdcard_encrypt_exclude_mm_on:I = 0x7f090aa6

.field public static final sdcard_encrypt_full_encryption_ask:I = 0x7f090ab9

.field public static final sdcard_encrypt_full_encryption_desc:I = 0x7f090aba

.field public static final sdcard_encrypt_full_error_gb:I = 0x7f090ae0

.field public static final sdcard_encrypt_full_error_mb:I = 0x7f090ae2

.field public static final sdcard_encrypt_full_off:I = 0x7f090aa3

.field public static final sdcard_encrypt_full_on:I = 0x7f090aa4

.field public static final sdcard_encrypt_has_been_disabled:I = 0x7f090ab5

.field public static final sdcard_encrypt_has_been_enabled:I = 0x7f090ab7

.field public static final sdcard_encrypt_multimedia_ask:I = 0x7f090abb

.field public static final sdcard_encrypt_multimedia_desc:I = 0x7f090abc

.field public static final sdcard_encrypt_needed:I = 0x7f090ac1

.field public static final sdcard_encrypt_notification:I = 0x7f090adc

.field public static final sdcard_encrypt_password_needed:I = 0x7f090a9d

.field public static final sdcard_encrypt_policy_mm_on:I = 0x7f090acf

.field public static final sdcard_encrypt_policy_set:I = 0x7f090acb

.field public static final sdcard_encrypt_policy_set_confirm:I = 0x7f090ac9

.field public static final sdcard_encrypt_policy_set_full_on:I = 0x7f090acd

.field public static final sdcard_encrypt_policy_set_full_on_confirm:I = 0x7f090ad3

.field public static final sdcard_encrypt_selected_options:I = 0x7f090abd

.field public static final sdcard_encrypt_sett_off:I = 0x7f090aa0

.field public static final sdcard_encrypt_sett_on:I = 0x7f090aa1

.field public static final sdcard_encrypt_settings:I = 0x7f090a9e

.field public static final sdcard_encrypt_title:I = 0x7f090a9c

.field public static final sdcard_encrypt_title_exclude_mm_files:I = 0x7f090aa5

.field public static final sdcard_encrypt_title_full_encryption:I = 0x7f090aa2

.field public static final sdcard_encrypt_title_notification:I = 0x7f090ada

.field public static final sdcard_encrypt_title_ongoing_second_storage:I = 0x7f090ad8

.field public static final sdcard_encrypt_title_setting:I = 0x7f090a9f

.field public static final sdcard_encrypt_unmount_warning:I = 0x7f090ad0

.field public static final sdcard_encrypt_will_enable:I = 0x7f090abf

.field public static final sdcard_format:I = 0x7f090075

.field public static final sdcard_mount_msg:I = 0x7f090ae6

.field public static final sdcard_need_decrypt:I = 0x7f090ab2

.field public static final sdcard_need_encrypt:I = 0x7f090ab3

.field public static final sdcard_no:I = 0x7f090ac3

.field public static final sdcard_not_inserted:I = 0x7f090ab0

.field public static final sdcard_policy_has_been_applied:I = 0x7f090ab1

.field public static final sdcard_pwd_prompt:I = 0x7f090aae

.field public static final sdcard_pwd_prompt_explanation:I = 0x7f090aaf

.field public static final sdcard_setting:I = 0x7f09007f

.field public static final sdcard_setting_sd:I = 0x7f090080

.field public static final sdcard_settings_available_bytes_label:I = 0x7f09012c

.field public static final sdcard_settings_bad_removal_status:I = 0x7f09012f

.field public static final sdcard_settings_factory_reset_warning:I = 0x7f090133

.field public static final sdcard_settings_mass_storage_status:I = 0x7f09012d

.field public static final sdcard_settings_not_present_status:I = 0x7f09012b

.field public static final sdcard_settings_read_only_status:I = 0x7f090132

.field public static final sdcard_settings_scanning_status:I = 0x7f090131

.field public static final sdcard_settings_screen_mass_storage_text:I = 0x7f090129

.field public static final sdcard_settings_total_bytes_label:I = 0x7f09012a

.field public static final sdcard_settings_unmounted_status:I = 0x7f09012e

.field public static final sdcard_settings_used_bytes_label:I = 0x7f090130

.field public static final sdcard_unmount:I = 0x7f090074

.field public static final sdcard_yes:I = 0x7f090ac2

.field public static final seandroid_control:I = 0x7f0911df

.field public static final seandroid_set_enforcing:I = 0x7f0911e1

.field public static final seandroid_set_enforcing_summary:I = 0x7f0911e2

.field public static final search_settings:I = 0x7f0904d6

.field public static final search_settings_summary:I = 0x7f0904d7

.field public static final second_lock_text:I = 0x7f090ee2

.field public static final second_lock_text_finger:I = 0x7f09116e

.field public static final security_passwords_title:I = 0x7f09017b

.field public static final security_policy_summary:I = 0x7f090ade

.field public static final security_policy_title:I = 0x7f090adf

.field public static final security_settings_billing_desc:I = 0x7f0906a8

.field public static final security_settings_desc:I = 0x7f0906a2

.field public static final security_settings_desc_multi:I = 0x7f0906a3

.field public static final security_settings_premium_sms_desc:I = 0x7f0906a9

.field public static final security_settings_summary:I = 0x7f090179

.field public static final security_settings_title:I = 0x7f090178

.field public static final security_unlock:I = 0x7f0909cc

.field public static final security_warning_content:I = 0x7f091249

.field public static final security_warning_dialog_title:I = 0x7f091248

.field public static final security_warning_later_btn:I = 0x7f09124a

.field public static final security_warning_set_btn:I = 0x7f09124b

.field public static final security_warning_ticker:I = 0x7f091247

.field public static final security_warning_title:I = 0x7f091246

.field public static final select_all:I = 0x7f0908bb

.field public static final select_application:I = 0x7f0908c2

.field public static final select_application_title:I = 0x7f090ef6

.field public static final select_backup_zone:I = 0x7f091289

.field public static final select_dest:I = 0x7f09129c

.field public static final select_device_admin_msg:I = 0x7f090858

.field public static final select_input_language_title:I = 0x7f0906e7

.field public static final select_location:I = 0x7f090f6b

.field public static final select_newer:I = 0x7f09129d

.field public static final select_newer_files:I = 0x7f0912bc

.field public static final select_pattern:I = 0x7f090d9b

.field public static final select_source:I = 0x7f09129b

.field public static final select_wifi_ap_title:I = 0x7f090f67

.field public static final selinux_status:I = 0x7f090545

.field public static final selinux_status_disabled:I = 0x7f0909ef

.field public static final selinux_status_enforcing:I = 0x7f0909f1

.field public static final selinux_status_permissive:I = 0x7f0909f0

.field public static final sensitivity:I = 0x7f09116c

.field public static final sensitivity_fast:I = 0x7f090bf5

.field public static final sensitivity_settings_and_motion_tutorial_title:I = 0x7f090bf7

.field public static final sensitivity_settings_title:I = 0x7f090bf2

.field public static final sensitivity_slow:I = 0x7f090bf4

.field public static final sensitivity_test_btn:I = 0x7f090bf6

.field public static final sensitivity_title:I = 0x7f090bf3

.field public static final service_background_processes:I = 0x7f0906cc

.field public static final service_busy:I = 0x7f0908b6

.field public static final service_client_name:I = 0x7f0906cb

.field public static final service_foreground_processes:I = 0x7f0906cd

.field public static final service_manage:I = 0x7f0906db

.field public static final service_manage_description:I = 0x7f0906df

.field public static final service_process_name:I = 0x7f0906cf

.field public static final service_restarting:I = 0x7f0906c7

.field public static final service_started_by_app:I = 0x7f0906ca

.field public static final service_stop:I = 0x7f0906da

.field public static final service_stop_description:I = 0x7f0906dc

.field public static final set_backup_pin:I = 0x7f090ee1

.field public static final set_home_city:I = 0x7f090c48

.field public static final set_quick_setting_button_summary:I = 0x7f0910a9

.field public static final set_quick_setting_button_title:I = 0x7f0910a8

.field public static final set_roaming_city:I = 0x7f090d05

.field public static final set_shortcut_title:I = 0x7f090ef5

.field public static final set_time_cannot_same:I = 0x7f091090

.field public static final set_time_category_title:I = 0x7f09108f

.field public static final set_up_simuim:I = 0x7f091104

.field public static final settings_backup:I = 0x7f090899

.field public static final settings_backup_summary:I = 0x7f09089a

.field public static final settings_button:I = 0x7f09079a

.field public static final settings_label:I = 0x7f090144

.field public static final settings_label_launcher:I = 0x7f090145

.field public static final settings_license_activity_loading:I = 0x7f090622

.field public static final settings_license_activity_title:I = 0x7f090620

.field public static final settings_license_activity_unavailable:I = 0x7f090621

.field public static final settings_menu_doNotSave:I = 0x7f09034f

.field public static final settings_menu_done:I = 0x7f09034e

.field public static final settings_safetylegal_activity_loading:I = 0x7f090626

.field public static final settings_safetylegal_activity_title:I = 0x7f090624

.field public static final settings_safetylegal_activity_unreachable:I = 0x7f090625

.field public static final settings_safetylegal_title:I = 0x7f090623

.field public static final settings_shortcut:I = 0x7f090146

.field public static final setup_wifi_title:I = 0x7f090e4b

.field public static final setup_wizard:I = 0x7f090f9f

.field public static final seven:I = 0x7f09003c

.field public static final shake_summary:I = 0x7f090bee

.field public static final shake_title:I = 0x7f090be5

.field public static final shake_tutorial_content:I = 0x7f090c05

.field public static final shake_tutorial_content_basicapp:I = 0x7f090c08

.field public static final shake_tutorial_content_removekies:I = 0x7f090c06

.field public static final shake_tutorial_content_removekiesonly:I = 0x7f090c07

.field public static final shake_tutorial_title:I = 0x7f090c12

.field public static final share_accessibility_settings:I = 0x7f090de5

.field public static final share_accessibility_settings_summary:I = 0x7f090de6

.field public static final share_and_transfer_settings_title:I = 0x7f090a7b

.field public static final shared_data:I = 0x7f0912aa

.field public static final shared_zone:I = 0x7f0912a2

.field public static final short_press:I = 0x7f090fc3

.field public static final shortcut_category_frequent:I = 0x7f090ef3

.field public static final shortcut_category_normal:I = 0x7f090ef4

.field public static final show_all_anrs:I = 0x7f0908f1

.field public static final show_all_anrs_summary:I = 0x7f0908f2

.field public static final show_background_processes:I = 0x7f090685

.field public static final show_cpu_usage:I = 0x7f0908e1

.field public static final show_cpu_usage_summary:I = 0x7f0908e2

.field public static final show_dev_already:I = 0x7f09004e

.field public static final show_dev_on:I = 0x7f09004d

.field public static final show_hw_layers_updates:I = 0x7f0908d3

.field public static final show_hw_layers_updates_summary:I = 0x7f0908d4

.field public static final show_hw_overdraw:I = 0x7f0908d5

.field public static final show_hw_overdraw_summary:I = 0x7f0908d6

.field public static final show_hw_screen_updates:I = 0x7f0908d1

.field public static final show_hw_screen_updates_summary:I = 0x7f0908d2

.field public static final show_message_ap:I = 0x7f0902c8

.field public static final show_message_ap_none:I = 0x7f0903e9

.field public static final show_owner_info_on_lockscreen_label:I = 0x7f090171

.field public static final show_password:I = 0x7f0906f3

.field public static final show_password_summary:I = 0x7f0912ce

.field public static final show_running_services:I = 0x7f090684

.field public static final show_screen_updates:I = 0x7f0908cf

.field public static final show_screen_updates_summary:I = 0x7f0908d0

.field public static final show_touches:I = 0x7f0908cd

.field public static final show_touches_summary:I = 0x7f0908ce

.field public static final show_user_info_on_lockscreen_label:I = 0x7f090175

.field public static final signature_identity_desc:I = 0x7f090667

.field public static final signature_label:I = 0x7f090668

.field public static final signature_settings_enable_visible_title:I = 0x7f090e6c

.field public static final signature_settings_verification_level_summary:I = 0x7f090e6e

.field public static final signature_settings_verification_level_title:I = 0x7f090e6d

.field public static final silent_mode_title:I = 0x7f090496

.field public static final sim_absent:I = 0x7f090522

.field public static final sim_alert_message:I = 0x7f090a4a

.field public static final sim_alert_message_without_country_code:I = 0x7f090a4b

.field public static final sim_bad_pin:I = 0x7f090501

.field public static final sim_bad_pin_4to8:I = 0x7f090e6b

.field public static final sim_card_management:I = 0x7f090ee3

.field public static final sim_card_management_ds:I = 0x7f091111

.field public static final sim_card_manager_help_switchkey:I = 0x7f091131

.field public static final sim_card_manager_prefered_ask_always:I = 0x7f091251

.field public static final sim_card_manager_prefered_change_network_mode:I = 0x7f091253

.field public static final sim_card_manager_prefered_current_network:I = 0x7f091250

.field public static final sim_card_manager_prefered_data_service:I = 0x7f09124e

.field public static final sim_card_manager_prefered_mobile_data_off:I = 0x7f091252

.field public static final sim_card_manager_prefered_title:I = 0x7f09124f

.field public static final sim_card_manager_prefered_video_call:I = 0x7f09124d

.field public static final sim_card_manager_prefered_voice_call:I = 0x7f09124c

.field public static final sim_change_failed:I = 0x7f090503

.field public static final sim_change_pin:I = 0x7f090500

.field public static final sim_change_succeeded:I = 0x7f090504

.field public static final sim_disable_sim_lock:I = 0x7f0904fc

.field public static final sim_disable_sim_perso:I = 0x7f090515

.field public static final sim_enable_sim_lock:I = 0x7f0904fb

.field public static final sim_enable_sim_perso:I = 0x7f090514

.field public static final sim_enter_cancel:I = 0x7f090507

.field public static final sim_enter_new:I = 0x7f0904fe

.field public static final sim_enter_ok:I = 0x7f090506

.field public static final sim_enter_old:I = 0x7f0904fd

.field public static final sim_enter_pin:I = 0x7f0904fa

.field public static final sim_lock_failed:I = 0x7f090505

.field public static final sim_lock_is_released:I = 0x7f090521

.field public static final sim_lock_is_set:I = 0x7f090520

.field public static final sim_lock_off:I = 0x7f0904f8

.field public static final sim_lock_on:I = 0x7f0904f7

.field public static final sim_lock_settings:I = 0x7f0904f3

.field public static final sim_lock_settings_category:I = 0x7f0904f4

.field public static final sim_lock_settings_title:I = 0x7f0904f5

.field public static final sim_name_1:I = 0x7f091133

.field public static final sim_name_2:I = 0x7f091134

.field public static final sim_no_pin_entered:I = 0x7f09051a

.field public static final sim_not_registered:I = 0x7f090523

.field public static final sim_perso_bad_passwd:I = 0x7f090516

.field public static final sim_perso_change:I = 0x7f09050b

.field public static final sim_perso_change_summary:I = 0x7f09050c

.field public static final sim_perso_checking:I = 0x7f090519

.field public static final sim_perso_enter_new:I = 0x7f09050d

.field public static final sim_perso_enter_new_again:I = 0x7f09050e

.field public static final sim_perso_enter_old:I = 0x7f090517

.field public static final sim_perso_no_passwd_entered:I = 0x7f09050f

.field public static final sim_perso_off:I = 0x7f09050a

.field public static final sim_perso_on:I = 0x7f090509

.field public static final sim_perso_passwd_dont_match:I = 0x7f090511

.field public static final sim_perso_pw_change_succeeded:I = 0x7f090518

.field public static final sim_perso_set_succeeded:I = 0x7f090512

.field public static final sim_perso_toggle:I = 0x7f090508

.field public static final sim_perso_unset_succeeded:I = 0x7f090513

.field public static final sim_perso_wrong_passwd_length:I = 0x7f090510

.field public static final sim_pin_change:I = 0x7f0904f9

.field public static final sim_pin_change_summary:I = 0x7f090524

.field public static final sim_pin_disabled:I = 0x7f090046

.field public static final sim_pin_enabled:I = 0x7f090045

.field public static final sim_pin_toggle:I = 0x7f0904f6

.field public static final sim_pins_dont_match:I = 0x7f090502

.field public static final sim_puk_locked_msg:I = 0x7f09051d

.field public static final sim_puk_locked_msg1:I = 0x7f09051e

.field public static final sim_puk_locked_msg2:I = 0x7f09051f

.field public static final sim_reenter_new:I = 0x7f0904ff

.field public static final sim_unlock_not_allowed:I = 0x7f090e6a

.field public static final sim_wrong_pin_chances:I = 0x7f09051c

.field public static final sim_wrong_pin_length:I = 0x7f09051b

.field public static final simuim_card_lock:I = 0x7f091105

.field public static final six:I = 0x7f09003b

.field public static final skip:I = 0x7f090005

.field public static final skip_all:I = 0x7f0912bb

.field public static final skip_button_label:I = 0x7f090651

.field public static final skip_label:I = 0x7f090134

.field public static final sleeping_mode_am:I = 0x7f090e91

.field public static final sleeping_mode_disabled:I = 0x7f090e90

.field public static final sleeping_mode_enabled:I = 0x7f090e8f

.field public static final sleeping_mode_end_time:I = 0x7f090e8a

.field public static final sleeping_mode_every_week:I = 0x7f090e8c

.field public static final sleeping_mode_off:I = 0x7f090e8e

.field public static final sleeping_mode_on:I = 0x7f090e8d

.field public static final sleeping_mode_pm:I = 0x7f090e92

.field public static final sleeping_mode_repeat:I = 0x7f090e8b

.field public static final sleeping_mode_start_time:I = 0x7f090e89

.field public static final sleeping_mode_summary:I = 0x7f090e88

.field public static final sleeping_mode_title:I = 0x7f090e87

.field public static final sliding_speed:I = 0x7f090b62

.field public static final small:I = 0x7f090b36

.field public static final small_font:I = 0x7f09007a

.field public static final smart_block_summary:I = 0x7f090e06

.field public static final smart_block_title:I = 0x7f090e05

.field public static final smart_network:I = 0x7f090017

.field public static final smart_network_guide_check:I = 0x7f09001b

.field public static final smart_network_guide_msg:I = 0x7f09001a

.field public static final smart_network_summary:I = 0x7f090018

.field public static final smart_network_widget_title:I = 0x7f090019

.field public static final smart_pause_summary:I = 0x7f090e02

.field public static final smart_pause_title:I = 0x7f090e01

.field public static final smart_screen_summary:I = 0x7f090e09

.field public static final smart_screen_title:I = 0x7f090e08

.field public static final smart_scroll_acceleration_summary:I = 0x7f090e11

.field public static final smart_scroll_acceleration_title:I = 0x7f090e10

.field public static final smart_scroll_app_chrome:I = 0x7f090e3d

.field public static final smart_scroll_app_chrome_summary:I = 0x7f090e3e

.field public static final smart_scroll_app_email_body:I = 0x7f090e41

.field public static final smart_scroll_app_email_body_summary:I = 0x7f090e42

.field public static final smart_scroll_app_email_list:I = 0x7f090e3f

.field public static final smart_scroll_app_email_list_summary:I = 0x7f090e40

.field public static final smart_scroll_app_gmail_body:I = 0x7f090e43

.field public static final smart_scroll_app_gmail_body_summary:I = 0x7f090e44

.field public static final smart_scroll_app_readers_hub:I = 0x7f090e45

.field public static final smart_scroll_app_readers_hub_summary:I = 0x7f090e46

.field public static final smart_scroll_app_web:I = 0x7f090e3b

.field public static final smart_scroll_app_web_summary:I = 0x7f090e3c

.field public static final smart_scroll_enable_msg:I = 0x7f090e14

.field public static final smart_scroll_sensitivity_summary:I = 0x7f090e0d

.field public static final smart_scroll_sensitivity_title:I = 0x7f090e0c

.field public static final smart_scroll_summary:I = 0x7f090e04

.field public static final smart_scroll_title:I = 0x7f090e03

.field public static final smart_scroll_type_summary:I = 0x7f090e0b

.field public static final smart_scroll_type_title:I = 0x7f090e0a

.field public static final smart_scroll_unit_summary:I = 0x7f090e0f

.field public static final smart_scroll_unit_title:I = 0x7f090e0e

.field public static final smart_scroll_visual_feedback_icon_summary:I = 0x7f090e13

.field public static final smart_scroll_visual_feedback_icon_title:I = 0x7f090e12

.field public static final smart_switch_widget_title:I = 0x7f09109f

.field public static final smartcard_config:I = 0x7f0901b5

.field public static final smartcard_configuration:I = 0x7f0901b4

.field public static final smartcard_configuration_summary:I = 0x7f0901b6

.field public static final smartcard_credential:I = 0x7f0901b2

.field public static final smartcard_credential_settings:I = 0x7f0901b3

.field public static final smartcard_help:I = 0x7f0901bd

.field public static final smartcard_notregisterd:I = 0x7f0901b9

.field public static final smartcard_opt_warning:I = 0x7f0901be

.field public static final smartcard_register:I = 0x7f0901b7

.field public static final smartcard_register_summary:I = 0x7f0901ba

.field public static final smartcard_registerd:I = 0x7f0901b8

.field public static final smartcard_unregister:I = 0x7f0901bb

.field public static final smartcard_unregister_summary:I = 0x7f0901bc

.field public static final smartsceen_tutorial_try_btn:I = 0x7f090e15

.field public static final smartscreen_advanced_setting_title:I = 0x7f090e16

.field public static final smartscreen_use_popup_msg:I = 0x7f090e47

.field public static final smcc_mode_summary:I = 0x7f090f1d

.field public static final smcc_mode_title:I = 0x7f090f1c

.field public static final sms_over_ims_off:I = 0x7f090056

.field public static final sms_over_ims_on:I = 0x7f090055

.field public static final social_hub_accounts:I = 0x7f090b59

.field public static final software_update_settings_list_item_summary:I = 0x7f0909b2

.field public static final software_update_settings_list_item_title:I = 0x7f0909b1

.field public static final software_version:I = 0x7f090de9

.field public static final sort_order_alpha:I = 0x7f090682

.field public static final sort_order_size:I = 0x7f090683

.field public static final sound_balance_cancel:I = 0x7f090b4e

.field public static final sound_balance_init_message:I = 0x7f090b48

.field public static final sound_balance_left_message:I = 0x7f090b4a

.field public static final sound_balance_right_message:I = 0x7f090b4b

.field public static final sound_balance_set:I = 0x7f090b4d

.field public static final sound_balance_summary:I = 0x7f0910a1

.field public static final sound_balance_swipe_message:I = 0x7f090b49

.field public static final sound_category_call_ringtone_vibrate_title:I = 0x7f090864

.field public static final sound_category_calls_and_notification_title:I = 0x7f090861

.field public static final sound_category_calls_title:I = 0x7f09113f

.field public static final sound_category_feedback_title:I = 0x7f090863

.field public static final sound_category_notification_title:I = 0x7f090862

.field public static final sound_category_sound_title:I = 0x7f090860

.field public static final sound_category_system_title:I = 0x7f090865

.field public static final sound_effects_enable_summary_off:I = 0x7f0904ae

.field public static final sound_effects_enable_summary_on:I = 0x7f0904ad

.field public static final sound_effects_enable_title:I = 0x7f0904ac

.field public static final sound_settings:I = 0x7f090495

.field public static final source_path_label:I = 0x7f09128d

.field public static final source_size_label:I = 0x7f09128e

.field public static final spellchecker_language:I = 0x7f0906f7

.field public static final spellchecker_quick_settings:I = 0x7f0906f6

.field public static final spellchecker_security_warning:I = 0x7f0906f5

.field public static final spellcheckers_settings_title:I = 0x7f090981

.field public static final sprint_4g_network_provider:I = 0x7f0908b3

.field public static final sprint_4g_settings:I = 0x7f0908b2

.field public static final sprint_connected_summary:I = 0x7f0911b8

.field public static final sprint_touch_manager_summary:I = 0x7f09127d

.field public static final sprint_touch_manager_title:I = 0x7f09127c

.field public static final ssid_label:I = 0x7f090486

.field public static final ssid_max_byte_error:I = 0x7f090ac6

.field public static final standard_mode:I = 0x7f090fd7

.field public static final standard_mode_body:I = 0x7f090fd8

.field public static final standard_mode_popup_body:I = 0x7f090fda

.field public static final standard_mode_popup_title:I = 0x7f090fd9

.field public static final start_backup:I = 0x7f091285

.field public static final start_restoration:I = 0x7f091287

.field public static final start_restore:I = 0x7f0912bd

.field public static final start_up_title:I = 0x7f090a71

.field public static final starter_mode_body:I = 0x7f090fdb

.field public static final starter_mode_popup_body:I = 0x7f090fdd

.field public static final starter_mode_popup_title:I = 0x7f090fdc

.field public static final status_awake_time:I = 0x7f090565

.field public static final status_bar_latest_events_title:I = 0x7f091272

.field public static final status_bt_address:I = 0x7f09055f

.field public static final status_bt_mac_address:I = 0x7f090560

.field public static final status_data_state:I = 0x7f090558

.field public static final status_eri_version:I = 0x7f090553

.field public static final status_ethernet_mac_address:I = 0x7f09055e

.field public static final status_fips:I = 0x7f090ae8

.field public static final status_icc_id:I = 0x7f090555

.field public static final status_imei:I = 0x7f09054d

.field public static final status_imei_sv:I = 0x7f09054e

.field public static final status_latest_area_info:I = 0x7f090557

.field public static final status_meid_number:I = 0x7f090554

.field public static final status_min_number:I = 0x7f090550

.field public static final status_msid_number:I = 0x7f090551

.field public static final status_network_type:I = 0x7f090556

.field public static final status_nid_number:I = 0x7f090eff

.field public static final status_number:I = 0x7f09054f

.field public static final status_operator:I = 0x7f09055c

.field public static final status_prl_version:I = 0x7f090552

.field public static final status_roaming:I = 0x7f09055b

.field public static final status_serial_number:I = 0x7f090561

.field public static final status_service_state:I = 0x7f090559

.field public static final status_sid_number:I = 0x7f090efe

.field public static final status_signal_strength:I = 0x7f09055a

.field public static final status_unavailable:I = 0x7f090562

.field public static final status_up_time:I = 0x7f090563

.field public static final status_user_name:I = 0x7f090564

.field public static final status_wifi_mac_address:I = 0x7f09055d

.field public static final status_wimax_mac_address:I = 0x7f0911bb

.field public static final stereo:I = 0x7f091044

.field public static final stms_version:I = 0x7f0911f6

.field public static final storage_label:I = 0x7f090662

.field public static final storage_low_summary:I = 0x7f090587

.field public static final storage_low_title:I = 0x7f090586

.field public static final storage_menu_usb:I = 0x7f090588

.field public static final storage_other_users:I = 0x7f090590

.field public static final storage_removed:I = 0x7f0912a1

.field public static final storage_settings:I = 0x7f09054a

.field public static final storage_settings_summary:I = 0x7f09054c

.field public static final storage_settings_title:I = 0x7f09054b

.field public static final storage_state_label:I = 0x7f091291

.field public static final storage_title_usb:I = 0x7f090589

.field public static final storageuse_settings_summary:I = 0x7f0906c4

.field public static final storageuse_settings_title:I = 0x7f0906c3

.field public static final strict_mode:I = 0x7f0908c9

.field public static final strict_mode_summary:I = 0x7f0908ca

.field public static final sub_lcd_auto_lock:I = 0x7f09107b

.field public static final sub_lcd_auto_lock_summary:I = 0x7f09107c

.field public static final sum_carrier_select:I = 0x7f090155

.field public static final summary_font_size:I = 0x7f0904ee

.field public static final summary_location_logs:I = 0x7f090611

.field public static final supplicant_state_label:I = 0x7f090483

.field public static final surface_summary:I = 0x7f090c1e

.field public static final surface_title:I = 0x7f090c1d

.field public static final surface_tutorial_settings_title:I = 0x7f090c20

.field public static final surround:I = 0x7f091045

.field public static final switching:I = 0x7f091132

.field public static final sync_active:I = 0x7f090889

.field public static final sync_all_label:I = 0x7f090b55

.field public static final sync_automatically:I = 0x7f090893

.field public static final sync_calendar:I = 0x7f09089f

.field public static final sync_contacts:I = 0x7f0908a0

.field public static final sync_disabled:I = 0x7f090895

.field public static final sync_enabled:I = 0x7f090894

.field public static final sync_error:I = 0x7f090896

.field public static final sync_failed:I = 0x7f090888

.field public static final sync_gmail:I = 0x7f09089e

.field public static final sync_in_progress:I = 0x7f090898

.field public static final sync_is_failing:I = 0x7f09088d

.field public static final sync_item_title:I = 0x7f0908af

.field public static final sync_menu_sync_cancel:I = 0x7f09089c

.field public static final sync_menu_sync_now:I = 0x7f09089b

.field public static final sync_now_label:I = 0x7f090b56

.field public static final sync_one_time_sync:I = 0x7f09089d

.field public static final sync_plug:I = 0x7f0908a1

.field public static final sync_settings:I = 0x7f0904d5

.field public static final sync_switch:I = 0x7f09088a

.field public static final synchronize_account:I = 0x7f090b5a

.field public static final sysscope_normal:I = 0x7f090140

.field public static final sysscope_scanning:I = 0x7f090142

.field public static final sysscope_status:I = 0x7f09013f

.field public static final sysscope_tampered:I = 0x7f090141

.field public static final system_language_summary:I = 0x7f09078d

.field public static final system_package:I = 0x7f0906b6

.field public static final system_permissions:I = 0x7f09125b

.field public static final system_update:I = 0x7f090ee5

.field public static final system_update_settings_list_item_summary:I = 0x7f09053c

.field public static final system_update_settings_list_item_title:I = 0x7f09053b

.field public static final tab_category_accounts:I = 0x7f090a81

.field public static final tab_category_connections:I = 0x7f090a7f

.field public static final tab_category_more:I = 0x7f090a82

.field public static final tab_category_mydevice:I = 0x7f090a80

.field public static final tab_to_list:I = 0x7f090a83

.field public static final tap_and_twist_summary:I = 0x7f090c27

.field public static final tap_and_twist_title:I = 0x7f090c26

.field public static final tap_to_configure:I = 0x7f09001d

.field public static final tap_to_start:I = 0x7f090d9d

.field public static final temperature_warning_msg:I = 0x7f090a25

.field public static final terminate_page_buddy:I = 0x7f09106b

.field public static final terminate_page_buddy_help:I = 0x7f09106c

.field public static final terms_and_condition:I = 0x7f090014

.field public static final terms_title:I = 0x7f09061f

.field public static final testing:I = 0x7f090716

.field public static final testing_battery_info:I = 0x7f090718

.field public static final testing_phone_info:I = 0x7f090717

.field public static final testing_sim_toolkit:I = 0x7f090719

.field public static final testing_usage_stats:I = 0x7f090757

.field public static final testing_wifi_info:I = 0x7f090478

.field public static final tether_settings_summary_all:I = 0x7f0905e0

.field public static final tether_settings_summary_usb:I = 0x7f0905e4

.field public static final tether_settings_summary_usb_bt:I = 0x7f0905e1

.field public static final tether_settings_summary_usb_wifi:I = 0x7f0905e2

.field public static final tether_settings_summary_wifi:I = 0x7f0905e3

.field public static final tether_settings_title_all:I = 0x7f0905df

.field public static final tether_settings_title_all_tmo:I = 0x7f0905e5

.field public static final tether_settings_title_bluetooth:I = 0x7f0905dd

.field public static final tether_settings_title_usb:I = 0x7f0905db

.field public static final tether_settings_title_usb_bluetooth:I = 0x7f0905de

.field public static final tether_settings_title_wifi:I = 0x7f0905dc

.field public static final tethering_attention_details:I = 0x7f0903bb

.field public static final tethering_attention_message:I = 0x7f0903ba

.field public static final tethering_attention_title:I = 0x7f0903b9

.field public static final tethering_blocked:I = 0x7f090007

.field public static final tethering_help_button_text:I = 0x7f0905f8

.field public static final text_empty:I = 0x7f09001c

.field public static final text_limit:I = 0x7f090a52

.field public static final this_feature_is_supported_by:I = 0x7f090c61

.field public static final three:I = 0x7f090038

.field public static final ticker_facebook_settings:I = 0x7f090b66

.field public static final ticker_news_settings:I = 0x7f090b64

.field public static final ticker_settings:I = 0x7f090b63

.field public static final ticker_stock_settings:I = 0x7f090b65

.field public static final ticker_weibo_settings:I = 0x7f090b6e

.field public static final tilt_summary:I = 0x7f090be9

.field public static final tilt_summary_without_browser:I = 0x7f090bea

.field public static final tilt_title:I = 0x7f090be1

.field public static final tilt_to_scroll_list_summary:I = 0x7f090beb

.field public static final tilt_to_scroll_list_title:I = 0x7f090be2

.field public static final tilt_to_scroll_list_tutorial_content:I = 0x7f090c02

.field public static final tilt_tutorial_content:I = 0x7f090c01

.field public static final time_picker_title:I = 0x7f09016a

.field public static final time_zone_auto_stub:I = 0x7f090882

.field public static final tiny:I = 0x7f090b37

.field public static final title_font_size:I = 0x7f0904ed

.field public static final title_text_1:I = 0x7f091126

.field public static final title_text_2:I = 0x7f091127

.field public static final title_text_3:I = 0x7f091128

.field public static final to_enable_air_motion_message:I = 0x7f090c4b

.field public static final to_enable_feature_common_message:I = 0x7f0911f8

.field public static final to_enable_finger_air_view_message:I = 0x7f090ccb

.field public static final to_enable_motion_message:I = 0x7f090bdc

.field public static final to_enable_surface_message:I = 0x7f090c1f

.field public static final to_title:I = 0x7f091093

.field public static final toggle_airplane_mode_off:I = 0x7f09112d

.field public static final toggle_airplane_mode_on:I = 0x7f09112c

.field public static final torch_light:I = 0x7f090b38

.field public static final torch_light_notification_message:I = 0x7f090b3d

.field public static final torch_light_notification_title:I = 0x7f090b3c

.field public static final torch_light_popup_message:I = 0x7f090b3b

.field public static final torch_light_popup_title:I = 0x7f090b3a

.field public static final torch_light_summary:I = 0x7f090b39

.field public static final torch_light_talkback_turned_off:I = 0x7f090b3f

.field public static final torch_light_talkback_turned_on:I = 0x7f090b3e

.field public static final torchlight_settings:I = 0x7f091254

.field public static final torchlight_settings_title:I = 0x7f091255

.field public static final torchlight_summary:I = 0x7f091256

.field public static final torchlight_timeout_summary:I = 0x7f091258

.field public static final torchlight_timeout_title:I = 0x7f091257

.field public static final total_size_label:I = 0x7f09066e

.field public static final touch_key_light:I = 0x7f0909e8

.field public static final touch_key_light_summary:I = 0x7f0909e9

.field public static final track_frame_time:I = 0x7f0908e7

.field public static final track_frame_time_summary:I = 0x7f0908e8

.field public static final transfer_title:I = 0x7f090a7c

.field public static final transition_animation_scale_title:I = 0x7f0908ea

.field public static final troaming_activity_name:I = 0x7f091269

.field public static final trusted_credentials:I = 0x7f090831

.field public static final trusted_credentials_disable_confirmation:I = 0x7f09096d

.field public static final trusted_credentials_disable_label:I = 0x7f090969

.field public static final trusted_credentials_enable_confirmation:I = 0x7f09096c

.field public static final trusted_credentials_enable_label:I = 0x7f09096a

.field public static final trusted_credentials_remove_confirmation:I = 0x7f09096e

.field public static final trusted_credentials_remove_label:I = 0x7f09096b

.field public static final trusted_credentials_summary:I = 0x7f090832

.field public static final trusted_credentials_system_tab:I = 0x7f090967

.field public static final trusted_credentials_user_tab:I = 0x7f090968

.field public static final try_again:I = 0x7f0908b5

.field public static final try_air_scroll_title:I = 0x7f090c83

.field public static final try_and_sensitivity:I = 0x7f090bf9

.field public static final tts_data_installed_summary:I = 0x7f090803

.field public static final tts_default_lang_summary:I = 0x7f0907fe

.field public static final tts_default_lang_title:I = 0x7f0907fd

.field public static final tts_default_pitch_summary:I = 0x7f0907fc

.field public static final tts_default_pitch_title:I = 0x7f0907fb

.field public static final tts_default_rate_summary:I = 0x7f0907fa

.field public static final tts_default_rate_title:I = 0x7f0907f9

.field public static final tts_default_settings_section:I = 0x7f0907f6

.field public static final tts_default_synth_summary:I = 0x7f0907f8

.field public static final tts_default_synth_title:I = 0x7f0907f7

.field public static final tts_engine_error:I = 0x7f090805

.field public static final tts_engine_error_config:I = 0x7f090806

.field public static final tts_engine_error_reselect:I = 0x7f090807

.field public static final tts_engine_name_is_disabled_summary:I = 0x7f09080c

.field public static final tts_engine_name_is_enabled_summary:I = 0x7f09080b

.field public static final tts_engine_name_settings:I = 0x7f09080a

.field public static final tts_engine_preference_section_title:I = 0x7f090817

.field public static final tts_engine_security_warning:I = 0x7f090808

.field public static final tts_engine_settings_button:I = 0x7f090816

.field public static final tts_engine_settings_section:I = 0x7f09080d

.field public static final tts_engine_settings_title:I = 0x7f09080e

.field public static final tts_engines_section:I = 0x7f090809

.field public static final tts_general_section_title:I = 0x7f090818

.field public static final tts_install_data_summary:I = 0x7f090802

.field public static final tts_install_data_title:I = 0x7f090801

.field public static final tts_language_summary:I = 0x7f09078c

.field public static final tts_language_title:I = 0x7f09078b

.field public static final tts_notif_engine_install_message:I = 0x7f090815

.field public static final tts_notif_engine_install_title:I = 0x7f090814

.field public static final tts_play_example_summary:I = 0x7f090800

.field public static final tts_play_example_title:I = 0x7f0907ff

.field public static final tts_settings:I = 0x7f0907f2

.field public static final tts_settings_changed_demo:I = 0x7f090804

.field public static final tts_settings_title:I = 0x7f0907f3

.field public static final turn_off_all:I = 0x7f090ee0

.field public static final turn_off_radio:I = 0x7f090054

.field public static final turn_off_talkback_exclusive_options_content:I = 0x7f09078e

.field public static final turn_on_radio:I = 0x7f090053

.field public static final turn_over_attention_content:I = 0x7f090c15

.field public static final turn_over_attention_vibrate_btn:I = 0x7f090c16

.field public static final turn_over_summary:I = 0x7f090bf1

.field public static final turn_over_title:I = 0x7f090be8

.field public static final turn_over_tutorial_content:I = 0x7f090c0d

.field public static final two:I = 0x7f090037

.field public static final uicc_unlock:I = 0x7f091110

.field public static final unable_to_load_sim:I = 0x7f090e69

.field public static final unable_to_set_as_ringtone:I = 0x7f0904c1

.field public static final uninstall_all_users_text:I = 0x7f090674

.field public static final uninstall_text:I = 0x7f090673

.field public static final unknown:I = 0x7f090681

.field public static final unknown_carrier:I = 0x7f0911b7

.field public static final unknown_carrier_connected_summary:I = 0x7f0911b9

.field public static final unlock_backup_info_summary:I = 0x7f090197

.field public static final unlock_backup_signature_info_summary:I = 0x7f090198

.field public static final unlock_change_lock_cac_pin_title:I = 0x7f0901c1

.field public static final unlock_change_lock_finger_title:I = 0x7f09116a

.field public static final unlock_change_lock_password_title:I = 0x7f0901db

.field public static final unlock_change_lock_pattern_title:I = 0x7f0901d9

.field public static final unlock_change_lock_pin_title:I = 0x7f0901da

.field public static final unlock_disable_lock_cac_pin_summary:I = 0x7f0901c0

.field public static final unlock_disable_lock_password_summary:I = 0x7f0901d8

.field public static final unlock_disable_lock_pattern_summary:I = 0x7f0901d6

.field public static final unlock_disable_lock_pin_summary:I = 0x7f0901d7

.field public static final unlock_disable_lock_title:I = 0x7f0901d5

.field public static final unlock_effect:I = 0x7f090fae

.field public static final unlock_screen:I = 0x7f0909de

.field public static final unlock_screen_contents_summary:I = 0x7f0909df

.field public static final unlock_screen_contents_summary_wifi:I = 0x7f0909e0

.field public static final unlock_set_unlock_biometric_weak_summary:I = 0x7f09019e

.field public static final unlock_set_unlock_biometric_weak_title:I = 0x7f09019d

.field public static final unlock_set_unlock_biometric_weak_with_voice_title:I = 0x7f09019f

.field public static final unlock_set_unlock_cac_pin_summary:I = 0x7f0901a5

.field public static final unlock_set_unlock_cac_pin_title:I = 0x7f0901a4

.field public static final unlock_set_unlock_disabled_summary:I = 0x7f0901cd

.field public static final unlock_set_unlock_launch_picker_change_summary:I = 0x7f090195

.field public static final unlock_set_unlock_launch_picker_change_title:I = 0x7f090194

.field public static final unlock_set_unlock_launch_picker_enable_summary:I = 0x7f090196

.field public static final unlock_set_unlock_launch_picker_title:I = 0x7f090193

.field public static final unlock_set_unlock_mode_biometric_weak:I = 0x7f0901d0

.field public static final unlock_set_unlock_mode_cac_pin:I = 0x7f0901bf

.field public static final unlock_set_unlock_mode_none:I = 0x7f0901cf

.field public static final unlock_set_unlock_mode_off:I = 0x7f0901ce

.field public static final unlock_set_unlock_mode_password:I = 0x7f0901d4

.field public static final unlock_set_unlock_mode_pattern:I = 0x7f0901d2

.field public static final unlock_set_unlock_mode_pin:I = 0x7f0901d3

.field public static final unlock_set_unlock_mode_signature:I = 0x7f0901d1

.field public static final unlock_set_unlock_none_summary:I = 0x7f09019c

.field public static final unlock_set_unlock_none_title:I = 0x7f09019b

.field public static final unlock_set_unlock_off_summary:I = 0x7f09019a

.field public static final unlock_set_unlock_off_title:I = 0x7f090199

.field public static final unlock_set_unlock_password_summary:I = 0x7f0901cc

.field public static final unlock_set_unlock_password_title:I = 0x7f0901cb

.field public static final unlock_set_unlock_pattern_summary:I = 0x7f0901a3

.field public static final unlock_set_unlock_pattern_title:I = 0x7f0901a2

.field public static final unlock_set_unlock_pin_summary:I = 0x7f0901ca

.field public static final unlock_set_unlock_pin_title:I = 0x7f0901c9

.field public static final unlock_set_unlock_signature_summary:I = 0x7f0901a1

.field public static final unlock_set_unlock_signature_title:I = 0x7f0901a0

.field public static final unmount_inform_text:I = 0x7f090583

.field public static final untitled_apn:I = 0x7f09085f

.field public static final update_firmware:I = 0x7f09110f

.field public static final update_prl:I = 0x7f09110d

.field public static final update_profile:I = 0x7f09110c

.field public static final update_samsung_software:I = 0x7f09110e

.field public static final update_summary:I = 0x7f0909b4

.field public static final usage_name_percent:I = 0x7f0907b4

.field public static final usage_stats_label:I = 0x7f090756

.field public static final usage_time_label:I = 0x7f09075b

.field public static final usage_type_audio:I = 0x7f0907c8

.field public static final usage_type_cpu:I = 0x7f0907c0

.field public static final usage_type_cpu_foreground:I = 0x7f0907c1

.field public static final usage_type_data_recv:I = 0x7f0907c7

.field public static final usage_type_data_send:I = 0x7f0907c6

.field public static final usage_type_gps:I = 0x7f0907c3

.field public static final usage_type_no_coverage:I = 0x7f0907cb

.field public static final usage_type_on_time:I = 0x7f0907ca

.field public static final usage_type_phone:I = 0x7f0907c5

.field public static final usage_type_video:I = 0x7f0907c9

.field public static final usage_type_wake_lock:I = 0x7f0907c2

.field public static final usage_type_wifi_running:I = 0x7f0907c4

.field public static final usb_connecting_message:I = 0x7f090a2e

.field public static final usb_connection_category:I = 0x7f09058a

.field public static final usb_label_installer_cd:I = 0x7f09058f

.field public static final usb_mtp_summary:I = 0x7f09058c

.field public static final usb_mtp_title:I = 0x7f09058b

.field public static final usb_otg_dlg_confirm_unmount_text:I = 0x7f090d8b

.field public static final usb_otg_dlg_confirm_unmount_title:I = 0x7f090d8a

.field public static final usb_otg_dlg_error_unmount_text:I = 0x7f090d8d

.field public static final usb_otg_dlg_error_unmount_title:I = 0x7f090d8c

.field public static final usb_otg_eject:I = 0x7f090d83

.field public static final usb_otg_eject_summary:I = 0x7f090d84

.field public static final usb_otg_ejecting_title:I = 0x7f090d8f

.field public static final usb_otg_format:I = 0x7f090d88

.field public static final usb_otg_format_summary:I = 0x7f090d89

.field public static final usb_otg_insert_summary:I = 0x7f090d85

.field public static final usb_otg_media_format_button_text:I = 0x7f090d92

.field public static final usb_otg_media_format_desc:I = 0x7f090d91

.field public static final usb_otg_media_format_final_desc:I = 0x7f090d93

.field public static final usb_otg_media_format_gesture_explanation:I = 0x7f090d94

.field public static final usb_otg_media_format_title:I = 0x7f090d90

.field public static final usb_otg_mount:I = 0x7f090d86

.field public static final usb_otg_mount_summary:I = 0x7f090d87

.field public static final usb_otg_storage:I = 0x7f090d82

.field public static final usb_otg_unmount_inform_text:I = 0x7f090d8e

.field public static final usb_ptp_summary:I = 0x7f09058e

.field public static final usb_ptp_title:I = 0x7f09058d

.field public static final usb_settings_title:I = 0x7f090a32

.field public static final usb_storage_button_mount:I = 0x7f090a2c

.field public static final usb_storage_message:I = 0x7f090a2b

.field public static final usb_storage_stop_button_mount:I = 0x7f090a2d

.field public static final usb_storage_title:I = 0x7f090a2a

.field public static final usb_tethering_active_subtext:I = 0x7f0905ea

.field public static final usb_tethering_available_subtext:I = 0x7f0905e9

.field public static final usb_tethering_button_text:I = 0x7f0905e7

.field public static final usb_tethering_button_text_vzw:I = 0x7f0905e8

.field public static final usb_tethering_errored_subtext:I = 0x7f0905ed

.field public static final usb_tethering_storage_active_subtext:I = 0x7f0905eb

.field public static final usb_tethering_unavailable_subtext:I = 0x7f0905ec

.field public static final usb_title:I = 0x7f0905e6

.field public static final usb_utility_summary:I = 0x7f090a29

.field public static final usb_utility_title:I = 0x7f090a28

.field public static final use_default_tts_settings_summary:I = 0x7f0907f5

.field public static final use_default_tts_settings_title:I = 0x7f0907f4

.field public static final use_ip_firewall_for_application:I = 0x7f0911f4

.field public static final use_location_summary:I = 0x7f090607

.field public static final use_location_title:I = 0x7f090606

.field public static final use_system_language_to_select_input_method_subtypes:I = 0x7f090730

.field public static final user_account_title:I = 0x7f090a21

.field public static final user_add_user_menu:I = 0x7f0909f6

.field public static final user_add_user_message_long:I = 0x7f0909fd

.field public static final user_add_user_message_short:I = 0x7f0909fe

.field public static final user_add_user_title:I = 0x7f0909fc

.field public static final user_adding_new_user:I = 0x7f090a0a

.field public static final user_cannot_manage_message:I = 0x7f090a03

.field public static final user_confirm_remove_message:I = 0x7f090a09

.field public static final user_confirm_remove_self_message:I = 0x7f090a08

.field public static final user_confirm_remove_self_title:I = 0x7f090a06

.field public static final user_confirm_remove_title:I = 0x7f090a07

.field public static final user_delete_button:I = 0x7f090a0c

.field public static final user_delete_user_description:I = 0x7f090a0b

.field public static final user_details_title:I = 0x7f0912d6

.field public static final user_dict_multiple_settings_title:I = 0x7f090705

.field public static final user_dict_settings_add_dialog_confirm:I = 0x7f09070c

.field public static final user_dict_settings_add_dialog_less_options:I = 0x7f09070b

.field public static final user_dict_settings_add_dialog_more_options:I = 0x7f09070a

.field public static final user_dict_settings_add_dialog_title:I = 0x7f090708

.field public static final user_dict_settings_add_locale_option_name:I = 0x7f09070f

.field public static final user_dict_settings_add_menu_title:I = 0x7f090707

.field public static final user_dict_settings_add_screen_title:I = 0x7f090709

.field public static final user_dict_settings_add_shortcut_option_name:I = 0x7f09070e

.field public static final user_dict_settings_add_word_option_name:I = 0x7f09070d

.field public static final user_dict_settings_all_languages:I = 0x7f090714

.field public static final user_dict_settings_context_menu_delete_title:I = 0x7f090712

.field public static final user_dict_settings_context_menu_edit_title:I = 0x7f090711

.field public static final user_dict_settings_edit_dialog_title:I = 0x7f090710

.field public static final user_dict_settings_empty_text:I = 0x7f090713

.field public static final user_dict_settings_more_languages:I = 0x7f090715

.field public static final user_dict_settings_summary:I = 0x7f090706

.field public static final user_dict_settings_titlebar:I = 0x7f090703

.field public static final user_dict_single_settings_title:I = 0x7f090704

.field public static final user_discard_user_menu:I = 0x7f0912de

.field public static final user_info_settings_title:I = 0x7f090176

.field public static final user_information_heading:I = 0x7f0912d7

.field public static final user_list_title:I = 0x7f0909f5

.field public static final user_market_apps_heading:I = 0x7f0912dd

.field public static final user_market_requires_pin:I = 0x7f0912da

.field public static final user_max_content_rating:I = 0x7f0912db

.field public static final user_name_title:I = 0x7f0912d8

.field public static final user_new_user_name:I = 0x7f090a05

.field public static final user_nickname:I = 0x7f0909fb

.field public static final user_owner:I = 0x7f0909fa

.field public static final user_picture_title:I = 0x7f090a22

.field public static final user_remove_user_menu:I = 0x7f090a04

.field public static final user_restrictions_heading:I = 0x7f0912d9

.field public static final user_settings_title:I = 0x7f0909f3

.field public static final user_setup_button_setup_later:I = 0x7f090a02

.field public static final user_setup_button_setup_now:I = 0x7f090a01

.field public static final user_setup_dialog_message:I = 0x7f090a00

.field public static final user_setup_dialog_title:I = 0x7f0909ff

.field public static final user_summary_active:I = 0x7f0909f7

.field public static final user_summary_inactive:I = 0x7f0909f8

.field public static final user_summary_not_set_up:I = 0x7f0909f9

.field public static final user_system_apps_heading:I = 0x7f0912dc

.field public static final user_you_section:I = 0x7f0909f4

.field public static final verifier_device_identifier:I = 0x7f09073d

.field public static final verifier_device_identifier_not_available:I = 0x7f09073e

.field public static final verify_applications:I = 0x7f09065d

.field public static final verify_applications_summary:I = 0x7f09065e

.field public static final verify_apps_over_usb_summary:I = 0x7f090748

.field public static final verify_apps_over_usb_title:I = 0x7f090747

.field public static final version_text:I = 0x7f0906ad

.field public static final vibrate_in_silent_title:I = 0x7f09049c

.field public static final vibrate_input_devices:I = 0x7f0906fc

.field public static final vibrate_input_devices_summary:I = 0x7f0906fd

.field public static final vibrate_on_ring_summary:I = 0x7f09049e

.field public static final vibrate_on_ring_title:I = 0x7f09049d

.field public static final vibrate_when_ringing_title:I = 0x7f0904c0

.field public static final vibration_intensity:I = 0x7f0909ac

.field public static final video_call_block_list:I = 0x7f090e79

.field public static final video_call_block_mode:I = 0x7f090e78

.field public static final video_call_block_title:I = 0x7f090e77

.field public static final view_Button:I = 0x7f0902c5

.field public static final voice_call_block_list:I = 0x7f090e76

.field public static final voice_call_block_mode:I = 0x7f090e75

.field public static final voice_call_block_title:I = 0x7f090e74

.field public static final voice_call_ringtone:I = 0x7f091071

.field public static final voice_call_ringtone2:I = 0x7f091072

.field public static final voice_category:I = 0x7f0907ee

.field public static final voice_control_menu_summary:I = 0x7f091268

.field public static final voice_input_control_alarm:I = 0x7f090f0a

.field public static final voice_input_control_alarm_summary:I = 0x7f090f0b

.field public static final voice_input_control_camera:I = 0x7f090f0c

.field public static final voice_input_control_camera_summary:I = 0x7f090f0d

.field public static final voice_input_control_cancel:I = 0x7f090f04

.field public static final voice_input_control_chaton_v:I = 0x7f090f08

.field public static final voice_input_control_incomming_calls:I = 0x7f090f07

.field public static final voice_input_control_incomming_calls_summary:I = 0x7f090f09

.field public static final voice_input_control_message:I = 0x7f090f02

.field public static final voice_input_control_music:I = 0x7f090f0e

.field public static final voice_input_control_music_summary:I = 0x7f090f0f

.field public static final voice_input_control_ok:I = 0x7f090f03

.field public static final voice_input_control_radio:I = 0x7f090f10

.field public static final voice_input_control_radio_summary:I = 0x7f090f11

.field public static final voice_input_control_summry:I = 0x7f090f05

.field public static final voice_input_control_title:I = 0x7f090f01

.field public static final voice_input_control_warrning:I = 0x7f090f06

.field public static final voice_input_control_widget_title:I = 0x7f090f1b

.field public static final voice_input_output_settings:I = 0x7f0907e9

.field public static final voice_input_output_settings_title:I = 0x7f0907ea

.field public static final voice_search_settings_title:I = 0x7f0907ec

.field public static final voice_title:I = 0x7f0907e8

.field public static final volume_alarm_description:I = 0x7f0904bb

.field public static final volume_alarm_mute:I = 0x7f0904bf

.field public static final volume_media_description:I = 0x7f0904b8

.field public static final volume_media_mute:I = 0x7f0904bd

.field public static final volume_notification_description:I = 0x7f0904ba

.field public static final volume_notification_mute:I = 0x7f0904be

.field public static final volume_ring_description:I = 0x7f0904b9

.field public static final volume_ring_mute:I = 0x7f0904bc

.field public static final vpn_add:I = 0x7f09095b

.field public static final vpn_cancel:I = 0x7f090957

.field public static final vpn_connect:I = 0x7f090959

.field public static final vpn_connect_to:I = 0x7f09095c

.field public static final vpn_create:I = 0x7f09095e

.field public static final vpn_dns_servers:I = 0x7f09094f

.field public static final vpn_edit:I = 0x7f09095a

.field public static final vpn_ipsec_ca_cert:I = 0x7f09094b

.field public static final vpn_ipsec_identifier:I = 0x7f090948

.field public static final vpn_ipsec_secret:I = 0x7f090949

.field public static final vpn_ipsec_server_cert:I = 0x7f09094c

.field public static final vpn_ipsec_user_cert:I = 0x7f09094a

.field public static final vpn_iwlan_running:I = 0x7f090f4d

.field public static final vpn_l2tp_secret:I = 0x7f090947

.field public static final vpn_lockdown_config_error:I = 0x7f090964

.field public static final vpn_lockdown_none:I = 0x7f090963

.field public static final vpn_lockdown_summary:I = 0x7f090962

.field public static final vpn_menu_delete:I = 0x7f090960

.field public static final vpn_menu_edit:I = 0x7f09095f

.field public static final vpn_menu_lockdown:I = 0x7f090961

.field public static final vpn_missing_cert:I = 0x7f090966

.field public static final vpn_mppe:I = 0x7f090946

.field public static final vpn_name:I = 0x7f090943

.field public static final vpn_no_ca_cert:I = 0x7f090955

.field public static final vpn_no_network:I = 0x7f090965

.field public static final vpn_no_server_cert:I = 0x7f090956

.field public static final vpn_not_used:I = 0x7f090954

.field public static final vpn_password:I = 0x7f090952

.field public static final vpn_routes:I = 0x7f090950

.field public static final vpn_save:I = 0x7f090958

.field public static final vpn_save_login:I = 0x7f090953

.field public static final vpn_search_domains:I = 0x7f09094e

.field public static final vpn_server:I = 0x7f090945

.field public static final vpn_settings_summary:I = 0x7f09082b

.field public static final vpn_settings_title:I = 0x7f09082a

.field public static final vpn_show_options:I = 0x7f09094d

.field public static final vpn_title:I = 0x7f09095d

.field public static final vpn_type:I = 0x7f090944

.field public static final vpn_username:I = 0x7f090951

.field public static final vzw_location_title:I = 0x7f090ece

.field public static final wag_fqdn:I = 0x7f0903f5

.field public static final wag_fqdn_def:I = 0x7f0903f8

.field public static final wag_ip:I = 0x7f0903f4

.field public static final wag_ip_def:I = 0x7f0903f6

.field public static final wag_port:I = 0x7f0903f7

.field public static final wait_for_debugger:I = 0x7f0908c4

.field public static final wait_for_debugger_summary:I = 0x7f0908c5

.field public static final waiting_tone_volume_title:I = 0x7f090f21

.field public static final wakeup_in_lockscreen_summary:I = 0x7f091014

.field public static final wakeup_in_lockscreen_summary_nomotion:I = 0x7f091015

.field public static final wallpaper:I = 0x7f0909e7

.field public static final wallpaper_settings_fragment_title:I = 0x7f0904e1

.field public static final wallpaper_settings_title:I = 0x7f0904e0

.field public static final wallpapers:I = 0x7f0909c9

.field public static final warning:I = 0x7f09015c

.field public static final weather:I = 0x7f090a61

.field public static final weather_settings:I = 0x7f090a5c

.field public static final weather_settings_summary:I = 0x7f090a5d

.field public static final weather_summary:I = 0x7f090a62

.field public static final wfd_help_message_1:I = 0x7f090464

.field public static final wfd_help_message_2:I = 0x7f090465

.field public static final wfd_help_message_3:I = 0x7f090466

.field public static final wfd_help_message_4:I = 0x7f090467

.field public static final wfd_learn_nfc:I = 0x7f09045a

.field public static final wfd_linking_nfc:I = 0x7f09045b

.field public static final wfd_nfc_help_message_1:I = 0x7f090461

.field public static final wfd_nfc_help_message_2:I = 0x7f090462

.field public static final wfd_nfc_help_message_3:I = 0x7f090463

.field public static final wfd_nfc_off_message:I = 0x7f090460

.field public static final wfd_nfc_readonly:I = 0x7f09045d

.field public static final wfd_nfc_write_failed:I = 0x7f09045f

.field public static final wfd_nfc_write_success:I = 0x7f09045e

.field public static final wfd_pin_connection:I = 0x7f09046b

.field public static final wfd_settings_cannot_proceed_cause_drm:I = 0x7f090452

.field public static final wfd_settings_cannot_proceed_cause_hdmi_busy:I = 0x7f090450

.field public static final wfd_settings_cannot_proceed_cause_hotspot_busy:I = 0x7f090457

.field public static final wfd_settings_cannot_proceed_cause_max_res:I = 0x7f090453

.field public static final wfd_settings_cannot_proceed_cause_p2p_busy:I = 0x7f09044e

.field public static final wfd_settings_cannot_proceed_cause_sbeam_busy:I = 0x7f090456

.field public static final wfd_settings_connecting:I = 0x7f090449

.field public static final wfd_settings_connection_fail:I = 0x7f090446

.field public static final wfd_settings_disable_alarm:I = 0x7f09044c

.field public static final wfd_settings_guide:I = 0x7f09044a

.field public static final wfd_settings_my_device:I = 0x7f090448

.field public static final wfd_settings_network_fail:I = 0x7f090447

.field public static final wfd_settings_ongoing_restart:I = 0x7f090451

.field public static final wfd_settings_restart:I = 0x7f09044f

.field public static final wfd_settings_start_alarm:I = 0x7f09044b

.field public static final wfd_settings_summary:I = 0x7f090444

.field public static final wfd_settings_terminates:I = 0x7f090445

.field public static final wfd_settings_terminates_scan:I = 0x7f09044d

.field public static final wfd_settings_title:I = 0x7f090443

.field public static final wfd_settings_update_dongle:I = 0x7f090454

.field public static final wfd_settings_update_process:I = 0x7f090455

.field public static final wfd_start_allshare:I = 0x7f09045c

.field public static final wfd_tag_write_message_1:I = 0x7f090468

.field public static final wfd_tag_write_message_2:I = 0x7f090469

.field public static final wfd_welcome_message:I = 0x7f09046a

.field public static final widget_picker_title:I = 0x7f09074e

.field public static final wifi_access_points:I = 0x7f0902ab

.field public static final wifi_access_points_manage:I = 0x7f0902aa

.field public static final wifi_add_network:I = 0x7f0902a7

.field public static final wifi_add_wifi_network:I = 0x7f0902a8

.field public static final wifi_advanced_ip_address_title:I = 0x7f0903f1

.field public static final wifi_advanced_mac_address_title:I = 0x7f0903f0

.field public static final wifi_advanced_settings_label:I = 0x7f090338

.field public static final wifi_advanced_titlebar:I = 0x7f0903ec

.field public static final wifi_always_ask_connect:I = 0x7f090e54

.field public static final wifi_and_mobile_skipped_message:I = 0x7f090336

.field public static final wifi_ap_5g_warning_msg:I = 0x7f09037b

.field public static final wifi_ap_add_allowed_device:I = 0x7f090355

.field public static final wifi_ap_advanced:I = 0x7f09039c

.field public static final wifi_ap_advanced_title:I = 0x7f09039b

.field public static final wifi_ap_allow_all_device:I = 0x7f09035d

.field public static final wifi_ap_allow_device_added:I = 0x7f090361

.field public static final wifi_ap_allow_device_already_added:I = 0x7f090363

.field public static final wifi_ap_allow_device_removed:I = 0x7f090362

.field public static final wifi_ap_allow_only_allowlist:I = 0x7f090360

.field public static final wifi_ap_allowed_device_add_button:I = 0x7f090354

.field public static final wifi_ap_allowed_device_remove:I = 0x7f09035a

.field public static final wifi_ap_allowed_list_empty:I = 0x7f09035e

.field public static final wifi_ap_batterymgr_summary1:I = 0x7f0903ae

.field public static final wifi_ap_batterymgr_summary2:I = 0x7f0903af

.field public static final wifi_ap_broadcast_network_name_msg:I = 0x7f0903c9

.field public static final wifi_ap_broadcast_network_name_title:I = 0x7f0903c8

.field public static final wifi_ap_cannot_be_enabled:I = 0x7f09037a

.field public static final wifi_ap_change_policy_warn:I = 0x7f09039a

.field public static final wifi_ap_change_security:I = 0x7f090396

.field public static final wifi_ap_channel:I = 0x7f090377

.field public static final wifi_ap_channel_2g:I = 0x7f090378

.field public static final wifi_ap_channel_5g:I = 0x7f090379

.field public static final wifi_ap_close:I = 0x7f0903ca

.field public static final wifi_ap_colon:I = 0x7f090358

.field public static final wifi_ap_connected_device:I = 0x7f090370

.field public static final wifi_ap_connected_devices:I = 0x7f090371

.field public static final wifi_ap_connection_duration:I = 0x7f090385

.field public static final wifi_ap_connection_mode:I = 0x7f090373

.field public static final wifi_ap_connection_time:I = 0x7f090384

.field public static final wifi_ap_devicename:I = 0x7f09035b

.field public static final wifi_ap_dialog_attention_check:I = 0x7f0903cd

.field public static final wifi_ap_dialog_disclaimer:I = 0x7f0903cf

.field public static final wifi_ap_dialog_disclaimer_spi:I = 0x7f0903d1

.field public static final wifi_ap_disconnect:I = 0x7f09036e

.field public static final wifi_ap_disconnect_client:I = 0x7f09036c

.field public static final wifi_ap_disconnect_confirm:I = 0x7f09036d

.field public static final wifi_ap_do_not_show_again:I = 0x7f0903cb

.field public static final wifi_ap_done:I = 0x7f090357

.field public static final wifi_ap_edit_allowed_device:I = 0x7f090356

.field public static final wifi_ap_enable_warning:I = 0x7f09032d

.field public static final wifi_ap_first_time_configuration:I = 0x7f0903cc

.field public static final wifi_ap_hide_ssid:I = 0x7f090351

.field public static final wifi_ap_hidessid_warning:I = 0x7f0903c0

.field public static final wifi_ap_howto:I = 0x7f090372

.field public static final wifi_ap_input_mac_address:I = 0x7f090353

.field public static final wifi_ap_instruction:I = 0x7f09038a

.field public static final wifi_ap_instruction_activate:I = 0x7f090389

.field public static final wifi_ap_instruction_activate_color:I = 0x7f09038c

.field public static final wifi_ap_instruction_activate_color_hidden:I = 0x7f09038e

.field public static final wifi_ap_instruction_battery_warning:I = 0x7f090393

.field public static final wifi_ap_instruction_changeable_maxclient:I = 0x7f090391

.field public static final wifi_ap_instruction_changeable_maxclient_mid:I = 0x7f090395

.field public static final wifi_ap_instruction_changeable_maxclient_note:I = 0x7f090392

.field public static final wifi_ap_instruction_help:I = 0x7f0903b8

.field public static final wifi_ap_instruction_mtr:I = 0x7f090390

.field public static final wifi_ap_instruction_open_activate:I = 0x7f090388

.field public static final wifi_ap_instruction_open_activate_color:I = 0x7f09038b

.field public static final wifi_ap_instruction_open_activate_color_hidden:I = 0x7f09038d

.field public static final wifi_ap_instruction_tmo:I = 0x7f09038f

.field public static final wifi_ap_ip:I = 0x7f090382

.field public static final wifi_ap_mac:I = 0x7f090383

.field public static final wifi_ap_mac_address:I = 0x7f090e57

.field public static final wifi_ap_maxclient:I = 0x7f09039d

.field public static final wifi_ap_menu_addlist:I = 0x7f090374

.field public static final wifi_ap_menu_allow_list:I = 0x7f090366

.field public static final wifi_ap_menu_configure:I = 0x7f090364

.field public static final wifi_ap_menu_delete:I = 0x7f090375

.field public static final wifi_ap_menu_help:I = 0x7f090367

.field public static final wifi_ap_menu_timeout:I = 0x7f090365

.field public static final wifi_ap_mobile_hotspot_settings:I = 0x7f0903c6

.field public static final wifi_ap_mpcs_info:I = 0x7f0903c4

.field public static final wifi_ap_no_devices:I = 0x7f090386

.field public static final wifi_ap_no_hotspot_mode_warning:I = 0x7f0903bf

.field public static final wifi_ap_no_reminder_mpcs_warning:I = 0x7f0903c5

.field public static final wifi_ap_none_security_warning:I = 0x7f090397

.field public static final wifi_ap_not_mac_address_warn:I = 0x7f090381

.field public static final wifi_ap_not_supported:I = 0x7f0903ce

.field public static final wifi_ap_notification_dialog_ok:I = 0x7f09037f

.field public static final wifi_ap_notification_dialog_title:I = 0x7f09037e

.field public static final wifi_ap_notification_turnoff_timeout:I = 0x7f090352

.field public static final wifi_ap_ok:I = 0x7f090368

.field public static final wifi_ap_remove_whitelist:I = 0x7f090387

.field public static final wifi_ap_set_up_and_manage_mobile_hotspot:I = 0x7f0903c7

.field public static final wifi_ap_settings_category:I = 0x7f09036f

.field public static final wifi_ap_show_advanced:I = 0x7f090376

.field public static final wifi_ap_starting:I = 0x7f0903c1

.field public static final wifi_ap_stopping:I = 0x7f0903c2

.field public static final wifi_ap_timeout:I = 0x7f090369

.field public static final wifi_ap_timeout_message:I = 0x7f09036b

.field public static final wifi_ap_timeout_title:I = 0x7f09036a

.field public static final wifi_ap_titlebar:I = 0x7f090f66

.field public static final wifi_ap_turning_off:I = 0x7f09032c

.field public static final wifi_ap_turning_on:I = 0x7f09032b

.field public static final wifi_ap_warn_max_whitelist:I = 0x7f090394

.field public static final wifi_ap_warn_roaming_msg:I = 0x7f090399

.field public static final wifi_ap_warn_roaming_title:I = 0x7f090398

.field public static final wifi_ap_wifi_off_warn:I = 0x7f090359

.field public static final wifi_ap_wifi_p2p_off_warn:I = 0x7f09035f

.field public static final wifi_api_test:I = 0x7f09047e

.field public static final wifi_att_auto_connect:I = 0x7f0902a3

.field public static final wifi_att_auto_connect_summary:I = 0x7f0902a4

.field public static final wifi_authentication_failed:I = 0x7f090305

.field public static final wifi_authentication_information:I = 0x7f090f8d

.field public static final wifi_authentication_invalid_certificate:I = 0x7f090309

.field public static final wifi_authentication_invalid_password:I = 0x7f090308

.field public static final wifi_authentication_invalid_value:I = 0x7f090307

.field public static final wifi_authentication_no_response:I = 0x7f090306

.field public static final wifi_authentication_succeed:I = 0x7f090304

.field public static final wifi_automatic_connect:I = 0x7f090e52

.field public static final wifi_calling_mode_title:I = 0x7f090e48

.field public static final wifi_calling_settings_summary:I = 0x7f090e4a

.field public static final wifi_calling_settings_title:I = 0x7f090e49

.field public static final wifi_cancel:I = 0x7f0903e4

.field public static final wifi_choose_a_network:I = 0x7f0903fa

.field public static final wifi_config_info:I = 0x7f090492

.field public static final wifi_connect:I = 0x7f0903e1

.field public static final wifi_connect_failed_message:I = 0x7f090337

.field public static final wifi_connect_seamlessly:I = 0x7f090f18

.field public static final wifi_connect_seamlessly_summary:I = 0x7f090f19

.field public static final wifi_connect_type_title:I = 0x7f090e50

.field public static final wifi_connection_charges_notification:I = 0x7f090f8c

.field public static final wifi_connection_priority_summary:I = 0x7f090f8a

.field public static final wifi_connection_priority_title:I = 0x7f090f89

.field public static final wifi_connection_settings:I = 0x7f090f8b

.field public static final wifi_database_empty:I = 0x7f090f5c

.field public static final wifi_disable:I = 0x7f090e5a

.field public static final wifi_disable_alert:I = 0x7f090f75

.field public static final wifi_disabled_generic:I = 0x7f0902f0

.field public static final wifi_disabled_heading:I = 0x7f0902f3

.field public static final wifi_disabled_help:I = 0x7f0902f4

.field public static final wifi_disabled_network_failure:I = 0x7f0902f2

.field public static final wifi_disabled_password_failure:I = 0x7f0902f5

.field public static final wifi_display_available_devices:I = 0x7f09024f

.field public static final wifi_display_details:I = 0x7f090253

.field public static final wifi_display_disconnect_text:I = 0x7f090255

.field public static final wifi_display_disconnect_title:I = 0x7f090254

.field public static final wifi_display_no_devices_found:I = 0x7f09024d

.field public static final wifi_display_options_done:I = 0x7f090258

.field public static final wifi_display_options_forget:I = 0x7f090257

.field public static final wifi_display_options_name:I = 0x7f090259

.field public static final wifi_display_options_title:I = 0x7f090256

.field public static final wifi_display_paired_devices:I = 0x7f09024e

.field public static final wifi_display_search_for_devices:I = 0x7f09024b

.field public static final wifi_display_searching_for_devices:I = 0x7f09024c

.field public static final wifi_display_settings_empty_list_wifi_display_disabled:I = 0x7f09024a

.field public static final wifi_display_settings_empty_list_wifi_display_off:I = 0x7f090249

.field public static final wifi_display_settings_title:I = 0x7f090248

.field public static final wifi_display_status_available:I = 0x7f090252

.field public static final wifi_display_status_connected:I = 0x7f090251

.field public static final wifi_display_status_connecting:I = 0x7f090250

.field public static final wifi_display_summary_disabled:I = 0x7f09025c

.field public static final wifi_display_summary_off:I = 0x7f09025b

.field public static final wifi_display_summary_on:I = 0x7f09025a

.field public static final wifi_dns1:I = 0x7f09033f

.field public static final wifi_dns1_hint:I = 0x7f090340

.field public static final wifi_dns2:I = 0x7f090341

.field public static final wifi_dns2_hint:I = 0x7f090342

.field public static final wifi_dont_skip:I = 0x7f090334

.field public static final wifi_during_usb_tethering:I = 0x7f0903d0

.field public static final wifi_eap_anonymous:I = 0x7f0902e6

.field public static final wifi_eap_ca_cert:I = 0x7f0902e3

.field public static final wifi_eap_identity:I = 0x7f0902e5

.field public static final wifi_eap_method:I = 0x7f0902e0

.field public static final wifi_eap_user_cert:I = 0x7f0902e4

.field public static final wifi_edit:I = 0x7f0902c6

.field public static final wifi_empty_list_wifi_off:I = 0x7f0902b6

.field public static final wifi_empty_list_wifi_on:I = 0x7f0902b7

.field public static final wifi_enable_location_title:I = 0x7f090f62

.field public static final wifi_enable_manual_connect_summary:I = 0x7f090e55

.field public static final wifi_enable_manual_connect_title:I = 0x7f090e51

.field public static final wifi_enable_warning:I = 0x7f09032a

.field public static final wifi_enable_warning_wifi_direct:I = 0x7f090329

.field public static final wifi_enable_watchdog_service:I = 0x7f09029d

.field public static final wifi_enable_watchdog_service_summary:I = 0x7f0902a0

.field public static final wifi_error:I = 0x7f090296

.field public static final wifi_factory_message:I = 0x7f090442

.field public static final wifi_fail_to_scan:I = 0x7f090298

.field public static final wifi_failed_connect_message:I = 0x7f090325

.field public static final wifi_failed_connect_wifi_message:I = 0x7f090326

.field public static final wifi_failed_forget_message:I = 0x7f090327

.field public static final wifi_failed_save_message:I = 0x7f090328

.field public static final wifi_failed_to_obtain_IP:I = 0x7f0902f1

.field public static final wifi_forget:I = 0x7f0903e2

.field public static final wifi_gateway:I = 0x7f090343

.field public static final wifi_gateway_hint:I = 0x7f090344

.field public static final wifi_hidden_ap_add:I = 0x7f090f81

.field public static final wifi_hidden_ap_already_registered:I = 0x7f090f82

.field public static final wifi_hidden_ap_delete:I = 0x7f090f83

.field public static final wifi_hidden_ap_no_aps:I = 0x7f090f7f

.field public static final wifi_hidden_ap_summary:I = 0x7f090f7e

.field public static final wifi_hidden_ap_title:I = 0x7f090f7d

.field public static final wifi_hidden_ap_tooltip:I = 0x7f090f80

.field public static final wifi_hidden_ap_will_be_deleted:I = 0x7f090f84

.field public static final wifi_hotspot20_enable:I = 0x7f09027c

.field public static final wifi_hotspot20_enable_summary_off:I = 0x7f09027d

.field public static final wifi_hotspot20_enable_summary_on:I = 0x7f09027e

.field public static final wifi_hotspot20_roam_enable:I = 0x7f090286

.field public static final wifi_hotspot20_roam_enable_summary:I = 0x7f090287

.field public static final wifi_hs20:I = 0x7f09027b

.field public static final wifi_hs20_available:I = 0x7f090288

.field public static final wifi_hs20_op_name:I = 0x7f09027f

.field public static final wifi_hs20_venue:I = 0x7f090280

.field public static final wifi_hs20_viaPassPoint:I = 0x7f090281

.field public static final wifi_in_airplane_mode:I = 0x7f090297

.field public static final wifi_invalid_usim_warning:I = 0x7f090302

.field public static final wifi_ip_address:I = 0x7f0902dd

.field public static final wifi_ip_address_hint:I = 0x7f0902de

.field public static final wifi_ip_settings:I = 0x7f0902ec

.field public static final wifi_ip_settings_invalid_dns:I = 0x7f09033d

.field public static final wifi_ip_settings_invalid_gateway:I = 0x7f09033c

.field public static final wifi_ip_settings_invalid_ip_address:I = 0x7f09033b

.field public static final wifi_ip_settings_invalid_network_prefix_length:I = 0x7f09033e

.field public static final wifi_ip_settings_menu_cancel:I = 0x7f09033a

.field public static final wifi_ip_settings_menu_save:I = 0x7f090339

.field public static final wifi_key_management:I = 0x7f0902df

.field public static final wifi_manage_network:I = 0x7f0902a9

.field public static final wifi_manually_connect:I = 0x7f090e53

.field public static final wifi_menu_advanced:I = 0x7f0902b1

.field public static final wifi_menu_autojoin:I = 0x7f0902b3

.field public static final wifi_menu_connect:I = 0x7f0902b2

.field public static final wifi_menu_forget:I = 0x7f0902b4

.field public static final wifi_menu_modify:I = 0x7f0902b5

.field public static final wifi_menu_more_options:I = 0x7f0902ad

.field public static final wifi_menu_p2p:I = 0x7f0902af

.field public static final wifi_menu_priority:I = 0x7f090e58

.field public static final wifi_menu_priority_summary:I = 0x7f090e59

.field public static final wifi_menu_scan:I = 0x7f0902b0

.field public static final wifi_menu_view:I = 0x7f090f6d

.field public static final wifi_menu_wps_pbc:I = 0x7f0902ac

.field public static final wifi_menu_wps_pin:I = 0x7f0902ae

.field public static final wifi_mobileAP_modemnai_missmath_message:I = 0x7f09032f

.field public static final wifi_mobileAP_modemnai_missmath_title:I = 0x7f09032e

.field public static final wifi_mobileAP_no_data_message:I = 0x7f090330

.field public static final wifi_more:I = 0x7f0902b9

.field public static final wifi_net_saved:I = 0x7f0902ca

.field public static final wifi_netmask:I = 0x7f090e66

.field public static final wifi_network_connection_ap:I = 0x7f0903ea

.field public static final wifi_network_prefix_length:I = 0x7f090345

.field public static final wifi_network_prefix_length_hint:I = 0x7f090346

.field public static final wifi_network_setup:I = 0x7f0902bd

.field public static final wifi_no_usim_warning:I = 0x7f090301

.field public static final wifi_not_in_range:I = 0x7f0902f6

.field public static final wifi_not_support_usim_warning:I = 0x7f090303

.field public static final wifi_notify_mobileap_is_on:I = 0x7f0903e0

.field public static final wifi_notify_open_networks:I = 0x7f09029b

.field public static final wifi_notify_open_networks_summary:I = 0x7f09029c

.field public static final wifi_ok:I = 0x7f0903e8

.field public static final wifi_on_dialog_summary:I = 0x7f090f13

.field public static final wifi_on_time:I = 0x7f0907b2

.field public static final wifi_only:I = 0x7f0909b7

.field public static final wifi_only_summary:I = 0x7f0909b8

.field public static final wifi_open:I = 0x7f0902fc

.field public static final wifi_open_security_warning:I = 0x7f090477

.field public static final wifi_other_network:I = 0x7f0902b8

.field public static final wifi_p2p_auto_disabling_confirmation:I = 0x7f09042a

.field public static final wifi_p2p_available_devices:I = 0x7f090408

.field public static final wifi_p2p_blank_toast:I = 0x7f090435

.field public static final wifi_p2p_busy_devices:I = 0x7f09040a

.field public static final wifi_p2p_busy_devices_summary:I = 0x7f090413

.field public static final wifi_p2p_busy_toast_message:I = 0x7f090414

.field public static final wifi_p2p_cancel_connect:I = 0x7f090422

.field public static final wifi_p2p_cancel_connect_message:I = 0x7f090458

.field public static final wifi_p2p_cancel_connect_title:I = 0x7f090411

.field public static final wifi_p2p_connect:I = 0x7f09043d

.field public static final wifi_p2p_connect_button_text:I = 0x7f090415

.field public static final wifi_p2p_connected_devices:I = 0x7f090409

.field public static final wifi_p2p_connected_with_another_device:I = 0x7f090431

.field public static final wifi_p2p_connected_with_n_devices:I = 0x7f09042c

.field public static final wifi_p2p_connecting:I = 0x7f090441

.field public static final wifi_p2p_connection_failed_message:I = 0x7f09043a

.field public static final wifi_p2p_connection_failed_title:I = 0x7f090439

.field public static final wifi_p2p_connection_limit:I = 0x7f09042e

.field public static final wifi_p2p_create_group:I = 0x7f09041f

.field public static final wifi_p2p_delete_group_message:I = 0x7f090459

.field public static final wifi_p2p_device_info:I = 0x7f0903fd

.field public static final wifi_p2p_device_name:I = 0x7f09041a

.field public static final wifi_p2p_disconnect_message:I = 0x7f09040f

.field public static final wifi_p2p_disconnect_multiple_message:I = 0x7f090410

.field public static final wifi_p2p_disconnect_title:I = 0x7f09040e

.field public static final wifi_p2p_disconnecting_confirmation:I = 0x7f090429

.field public static final wifi_p2p_disconnecting_confirmation_for_scanning:I = 0x7f090438

.field public static final wifi_p2p_enabling_confirmation:I = 0x7f09042b

.field public static final wifi_p2p_end_connection:I = 0x7f090418

.field public static final wifi_p2p_failed_connect_message:I = 0x7f09040c

.field public static final wifi_p2p_failed_rename_message:I = 0x7f09040d

.field public static final wifi_p2p_help:I = 0x7f09041c

.field public static final wifi_p2p_help_message:I = 0x7f090420

.field public static final wifi_p2p_incoming_request_confirmation:I = 0x7f09042d

.field public static final wifi_p2p_info_mac:I = 0x7f090426

.field public static final wifi_p2p_info_time:I = 0x7f090427

.field public static final wifi_p2p_info_title:I = 0x7f090428

.field public static final wifi_p2p_invited:I = 0x7f090440

.field public static final wifi_p2p_menu_advanced:I = 0x7f090404

.field public static final wifi_p2p_menu_cancel_connect:I = 0x7f090437

.field public static final wifi_p2p_menu_create_group:I = 0x7f090402

.field public static final wifi_p2p_menu_more:I = 0x7f090416

.field public static final wifi_p2p_menu_multi_connect:I = 0x7f090436

.field public static final wifi_p2p_menu_remove_group:I = 0x7f090403

.field public static final wifi_p2p_menu_rename:I = 0x7f090406

.field public static final wifi_p2p_menu_search:I = 0x7f090401

.field public static final wifi_p2p_menu_searching:I = 0x7f090405

.field public static final wifi_p2p_multi_connect_footer:I = 0x7f09043c

.field public static final wifi_p2p_multi_connect_title:I = 0x7f09043b

.field public static final wifi_p2p_multi_selectAll:I = 0x7f09043e

.field public static final wifi_p2p_no_device:I = 0x7f090421

.field public static final wifi_p2p_no_device_found:I = 0x7f090434

.field public static final wifi_p2p_no_selected_devices:I = 0x7f09043f

.field public static final wifi_p2p_peer_devices:I = 0x7f090407

.field public static final wifi_p2p_persist_network:I = 0x7f090400

.field public static final wifi_p2p_remembered_groups:I = 0x7f09040b

.field public static final wifi_p2p_scan_summary:I = 0x7f090419

.field public static final wifi_p2p_scan_title:I = 0x7f090417

.field public static final wifi_p2p_scanning_summary:I = 0x7f09041e

.field public static final wifi_p2p_scanning_title:I = 0x7f09041d

.field public static final wifi_p2p_select_device:I = 0x7f090433

.field public static final wifi_p2p_settings_summary:I = 0x7f0903fc

.field public static final wifi_p2p_settings_title:I = 0x7f0903fb

.field public static final wifi_p2p_ssid_max_byte_error:I = 0x7f09041b

.field public static final wifi_p2p_tap_to_change_settings:I = 0x7f09042f

.field public static final wifi_p2p_tap_to_connect:I = 0x7f090432

.field public static final wifi_p2p_turnon_message:I = 0x7f090412

.field public static final wifi_p2p_wifi_direct_is_on:I = 0x7f090430

.field public static final wifi_p2p_wps_pin:I = 0x7f0903ff

.field public static final wifi_p2p_wps_setup:I = 0x7f0903fe

.field public static final wifi_password:I = 0x7f0902e8

.field public static final wifi_password_max_byte_error:I = 0x7f090300

.field public static final wifi_poor_network_detection:I = 0x7f09029e

.field public static final wifi_poor_network_detection_summary:I = 0x7f09029f

.field public static final wifi_power_save_database_title:I = 0x7f090f5d

.field public static final wifi_priority_down:I = 0x7f090e5c

.field public static final wifi_priority_save:I = 0x7f090e5d

.field public static final wifi_priority_shift:I = 0x7f090e5e

.field public static final wifi_priority_up:I = 0x7f090e5b

.field public static final wifi_quick_toggle_summary:I = 0x7f09028e

.field public static final wifi_quick_toggle_title:I = 0x7f09028d

.field public static final wifi_remembered:I = 0x7f0902ef

.field public static final wifi_required_info_text:I = 0x7f0902bb

.field public static final wifi_reset_dialog_alert_message:I = 0x7f090f87

.field public static final wifi_reset_dialog_progress_message:I = 0x7f090f88

.field public static final wifi_reset_summary:I = 0x7f090f86

.field public static final wifi_reset_title:I = 0x7f090f85

.field public static final wifi_save:I = 0x7f0903e3

.field public static final wifi_scan_ap:I = 0x7f0903e5

.field public static final wifi_scan_finish:I = 0x7f0903e6

.field public static final wifi_scc_diff_notification_dialog_msg:I = 0x7f0903d3

.field public static final wifi_scc_diff_notification_title:I = 0x7f0903d2

.field public static final wifi_search_networks:I = 0x7f090f1a

.field public static final wifi_secured:I = 0x7f0902fb

.field public static final wifi_secured_first_item:I = 0x7f0902f9

.field public static final wifi_secured_second_item:I = 0x7f0902fa

.field public static final wifi_security:I = 0x7f0902d9

.field public static final wifi_security_cckm:I = 0x7f09031c

.field public static final wifi_security_eap:I = 0x7f09031b

.field public static final wifi_security_ft:I = 0x7f090320

.field public static final wifi_security_ft_psk:I = 0x7f09031d

.field public static final wifi_security_ft_wpa2_psk:I = 0x7f09031e

.field public static final wifi_security_mode_disable:I = 0x7f090d3a

.field public static final wifi_security_none:I = 0x7f090315

.field public static final wifi_security_open_msg:I = 0x7f09030a

.field public static final wifi_security_psk_generic:I = 0x7f09031a

.field public static final wifi_security_short_cckm:I = 0x7f090311

.field public static final wifi_security_short_eap:I = 0x7f090310

.field public static final wifi_security_short_ft:I = 0x7f090314

.field public static final wifi_security_short_ft_psk:I = 0x7f090312

.field public static final wifi_security_short_ft_wpa2_psk:I = 0x7f090313

.field public static final wifi_security_short_psk_generic:I = 0x7f09030f

.field public static final wifi_security_short_wep:I = 0x7f09030b

.field public static final wifi_security_short_wpa:I = 0x7f09030c

.field public static final wifi_security_short_wpa2:I = 0x7f09030d

.field public static final wifi_security_short_wpa_wpa2:I = 0x7f09030e

.field public static final wifi_security_wapi_cert:I = 0x7f090322

.field public static final wifi_security_wapi_psk:I = 0x7f090321

.field public static final wifi_security_wapi_psk_ascii:I = 0x7f090323

.field public static final wifi_security_wapi_psk_hexadecimal:I = 0x7f090324

.field public static final wifi_security_wep:I = 0x7f090316

.field public static final wifi_security_wpa:I = 0x7f090317

.field public static final wifi_security_wpa2:I = 0x7f090318

.field public static final wifi_security_wpa_wpa2:I = 0x7f090319

.field public static final wifi_security_wpa_wpa2_ft:I = 0x7f09031f

.field public static final wifi_setting_frequency_band_error:I = 0x7f0903ef

.field public static final wifi_setting_frequency_band_summary:I = 0x7f0903ee

.field public static final wifi_setting_frequency_band_title:I = 0x7f0903ed

.field public static final wifi_setting_preferred_band_summary:I = 0x7f090f7c

.field public static final wifi_setting_preferred_band_title:I = 0x7f090f7b

.field public static final wifi_setting_sleep_policy_error:I = 0x7f0902a2

.field public static final wifi_setting_sleep_policy_title:I = 0x7f0902a1

.field public static final wifi_settings:I = 0x7f09028f

.field public static final wifi_settings_category:I = 0x7f090290

.field public static final wifi_settings_summary:I = 0x7f090292

.field public static final wifi_settings_title:I = 0x7f090291

.field public static final wifi_setup_add_network:I = 0x7f09086c

.field public static final wifi_setup_back:I = 0x7f090870

.field public static final wifi_setup_cancel:I = 0x7f090875

.field public static final wifi_setup_connect:I = 0x7f090872

.field public static final wifi_setup_description_connected:I = 0x7f090881

.field public static final wifi_setup_description_connecting:I = 0x7f090880

.field public static final wifi_setup_detail:I = 0x7f090871

.field public static final wifi_setup_eap_not_supported:I = 0x7f09087f

.field public static final wifi_setup_forget:I = 0x7f090873

.field public static final wifi_setup_next:I = 0x7f09086f

.field public static final wifi_setup_not_connected:I = 0x7f09086b

.field public static final wifi_setup_refresh_list:I = 0x7f09086d

.field public static final wifi_setup_save:I = 0x7f090874

.field public static final wifi_setup_screentext1:I = 0x7f090f9c

.field public static final wifi_setup_screentext1_scamera:I = 0x7f090f9d

.field public static final wifi_setup_screentitle:I = 0x7f090f9b

.field public static final wifi_setup_skip:I = 0x7f09086e

.field public static final wifi_setup_status_connecting:I = 0x7f09087c

.field public static final wifi_setup_status_eap_not_supported:I = 0x7f09087e

.field public static final wifi_setup_status_edit_network:I = 0x7f09087a

.field public static final wifi_setup_status_existing_network:I = 0x7f090878

.field public static final wifi_setup_status_new_network:I = 0x7f09087b

.field public static final wifi_setup_status_proceed_to_next:I = 0x7f09087d

.field public static final wifi_setup_status_scanning:I = 0x7f090876

.field public static final wifi_setup_status_select_network:I = 0x7f090877

.field public static final wifi_setup_status_unsecured_network:I = 0x7f090879

.field public static final wifi_setup_title:I = 0x7f090866

.field public static final wifi_setup_title_add_network:I = 0x7f09086a

.field public static final wifi_setup_title_connected_network:I = 0x7f090869

.field public static final wifi_setup_title_connecting_network:I = 0x7f090868

.field public static final wifi_setup_title_editing_network:I = 0x7f090867

.field public static final wifi_setup_wizard_title:I = 0x7f090293

.field public static final wifi_setup_wps:I = 0x7f0902ba

.field public static final wifi_show_advanced:I = 0x7f0902bc

.field public static final wifi_show_password:I = 0x7f0902e9

.field public static final wifi_signal:I = 0x7f0902da

.field public static final wifi_skip_anyway:I = 0x7f090333

.field public static final wifi_skipped_message:I = 0x7f090335

.field public static final wifi_speed:I = 0x7f0902dc

.field public static final wifi_ssid:I = 0x7f0902d8

.field public static final wifi_ssid_max_byte_error:I = 0x7f0902ff

.field public static final wifi_starting:I = 0x7f090294

.field public static final wifi_state_disabled:I = 0x7f09048e

.field public static final wifi_state_disabling:I = 0x7f09048d

.field public static final wifi_state_enabled:I = 0x7f090490

.field public static final wifi_state_enabling:I = 0x7f09048f

.field public static final wifi_state_label:I = 0x7f090481

.field public static final wifi_state_unknown:I = 0x7f090491

.field public static final wifi_status:I = 0x7f0902db

.field public static final wifi_status_test:I = 0x7f09047f

.field public static final wifi_stopping:I = 0x7f090295

.field public static final wifi_suspend_optimizations:I = 0x7f0902a5

.field public static final wifi_suspend_optimizations_summary:I = 0x7f0902a6

.field public static final wifi_terminal_mac_address:I = 0x7f090e56

.field public static final wifi_tether_checkbox_text:I = 0x7f09046c

.field public static final wifi_tether_checkbox_text_chameleon:I = 0x7f0903be

.field public static final wifi_tether_configure_ap_text:I = 0x7f090471

.field public static final wifi_tether_configure_ssid_default:I = 0x7f090473

.field public static final wifi_tether_configure_subtext:I = 0x7f090472

.field public static final wifi_tether_dialog_nolte_warning:I = 0x7f0903c3

.field public static final wifi_tether_dialog_nosim_warning:I = 0x7f09037c

.field public static final wifi_tether_enabled_subtext:I = 0x7f09046f

.field public static final wifi_tether_failed_subtext:I = 0x7f090470

.field public static final wifi_tether_otherdevices_name:I = 0x7f09035c

.field public static final wifi_tether_starting:I = 0x7f09046d

.field public static final wifi_tether_stopping:I = 0x7f09046e

.field public static final wifi_unchanged:I = 0x7f0902ed

.field public static final wifi_unknown_secured_ap:I = 0x7f0903e7

.field public static final wifi_unknown_security_type:I = 0x7f0902fd

.field public static final wifi_unspecified:I = 0x7f0902ee

.field public static final wifi_update:I = 0x7f090480

.field public static final wifi_use_wag_ip:I = 0x7f0903f3

.field public static final wifi_use_wag_ip_summary:I = 0x7f0903f9

.field public static final wifi_use_wifi_power_save:I = 0x7f090f5b

.field public static final wifi_wag_ip_settings_titlebar:I = 0x7f0903f2

.field public static final wifi_wapi_as_certificate:I = 0x7f0902ea

.field public static final wifi_wapi_fail_to_auth:I = 0x7f09029a

.field public static final wifi_wapi_fail_to_init:I = 0x7f090299

.field public static final wifi_wapi_psk_type:I = 0x7f0902e7

.field public static final wifi_wapi_user_certificate:I = 0x7f0902eb

.field public static final wifi_watchdog_connectivity_check:I = 0x7f0903d6

.field public static final wifi_watchdog_connectivity_check_enable_dialog:I = 0x7f0903d9

.field public static final wifi_watchdog_connectivity_check_enable_dialog_title:I = 0x7f0903d8

.field public static final wifi_watchdog_connectivity_check_summary:I = 0x7f0903d7

.field public static final wifi_watchdog_network_disabled_detailed_sec:I = 0x7f0903d5

.field public static final wifi_watchdog_network_disabled_sec:I = 0x7f0903d4

.field public static final wifi_watchdog_not_available_service:I = 0x7f0903da

.field public static final wifi_watchdog_recheck:I = 0x7f0903db

.field public static final wifi_wps_available:I = 0x7f0902fe

.field public static final wifi_wps_available_first_item:I = 0x7f0902f7

.field public static final wifi_wps_available_second_item:I = 0x7f0902f8

.field public static final wifi_wps_button_message:I = 0x7f0902c1

.field public static final wifi_wps_complete:I = 0x7f0902d0

.field public static final wifi_wps_connected:I = 0x7f0902d1

.field public static final wifi_wps_fail_error:I = 0x7f0902c4

.field public static final wifi_wps_failed:I = 0x7f0902c0

.field public static final wifi_wps_failed_auth:I = 0x7f0902d6

.field public static final wifi_wps_failed_generic:I = 0x7f0902d3

.field public static final wifi_wps_failed_overlap:I = 0x7f0902d7

.field public static final wifi_wps_failed_tkip:I = 0x7f0902d5

.field public static final wifi_wps_failed_wep:I = 0x7f0902d4

.field public static final wifi_wps_in_progress:I = 0x7f0902d2

.field public static final wifi_wps_onstart_pbc:I = 0x7f0902ce

.field public static final wifi_wps_onstart_pin:I = 0x7f0902cf

.field public static final wifi_wps_overlap_error:I = 0x7f0903eb

.field public static final wifi_wps_pin:I = 0x7f0902be

.field public static final wifi_wps_pin_message:I = 0x7f0902c2

.field public static final wifi_wps_pin_output:I = 0x7f0902bf

.field public static final wifi_wps_setup_msg:I = 0x7f0902cd

.field public static final wifi_wps_setup_title:I = 0x7f0902cc

.field public static final wifi_wps_timer_error:I = 0x7f0902c3

.field public static final wifiap_dhcp_enable:I = 0x7f0903a4

.field public static final wifiap_dhcp_enable_summary:I = 0x7f0903a8

.field public static final wifiap_dhcp_end_ip:I = 0x7f0903a6

.field public static final wifiap_dhcp_lease_time:I = 0x7f0903a7

.field public static final wifiap_dhcp_lease_time_summery:I = 0x7f0903aa

.field public static final wifiap_dhcp_max_user:I = 0x7f0903ab

.field public static final wifiap_dhcp_settings:I = 0x7f0903a3

.field public static final wifiap_dhcp_start_ip:I = 0x7f0903a5

.field public static final wifiap_ip_instruction:I = 0x7f0903a1

.field public static final wifiap_lan_settings:I = 0x7f09039e

.field public static final wifiap_leasetime_instruction:I = 0x7f0903a9

.field public static final wifiap_local_ip_addrs:I = 0x7f0903a0

.field public static final wifiap_local_settings:I = 0x7f09039f

.field public static final wifiap_local_subnetmask:I = 0x7f0903a2

.field public static final wifiap_maxuser_instruction:I = 0x7f0903ad

.field public static final wifiaplan_dlg_convert_otherips:I = 0x7f0903b2

.field public static final wifiaplan_dlg_ip_not_compatible:I = 0x7f0903b3

.field public static final wifiaplan_dlg_ip_not_compatible_message:I = 0x7f0903b4

.field public static final wifiaplan_dlg_restart_ap:I = 0x7f0903b5

.field public static final wifiaplan_dlg_restart_required:I = 0x7f0903b6

.field public static final wifiaplan_dlg_restart_required_message:I = 0x7f0903b7

.field public static final wifiaplan_dlg_revert_changes:I = 0x7f0903b1

.field public static final wifiaplan_ip_change_alert:I = 0x7f0903ac

.field public static final wificonnected_notificaton_detail:I = 0x7f090332

.field public static final wificonnected_notificaton_title:I = 0x7f090331

.field public static final wifitimer_endtime_title:I = 0x7f090348

.field public static final wifitimer_notification_summery:I = 0x7f09034d

.field public static final wifitimer_off_notification_title:I = 0x7f09034c

.field public static final wifitimer_on_notification_title:I = 0x7f09034b

.field public static final wifitimer_startend_warning_message:I = 0x7f090350

.field public static final wifitimer_starttime_title:I = 0x7f090347

.field public static final wifitimer_summery_off:I = 0x7f09034a

.field public static final wifitimer_title:I = 0x7f090349

.field public static final wimax:I = 0x7f09118f

.field public static final wimax_bt_warning:I = 0x7f09119a

.field public static final wimax_cancel_button:I = 0x7f091197

.field public static final wimax_connecting_summary:I = 0x7f0911af

.field public static final wimax_dns1:I = 0x7f0911be

.field public static final wimax_dns2:I = 0x7f0911bf

.field public static final wimax_error:I = 0x7f0911a7

.field public static final wimax_gateway:I = 0x7f0911bd

.field public static final wimax_ip:I = 0x7f0911bc

.field public static final wimax_mobileap_warning:I = 0x7f09119b

.field public static final wimax_networks:I = 0x7f091195

.field public static final wimax_notify_open_networks:I = 0x7f091191

.field public static final wimax_notify_open_networks_summary:I = 0x7f091192

.field public static final wimax_obtaining_ip:I = 0x7f0911b0

.field public static final wimax_ok_button:I = 0x7f091196

.field public static final wimax_quick_toggle_summary:I = 0x7f091190

.field public static final wimax_settings:I = 0x7f09118d

.field public static final wimax_settings_summary:I = 0x7f09118e

.field public static final wimax_starting:I = 0x7f0911a5

.field public static final wimax_state_connected:I = 0x7f0911b5

.field public static final wimax_status_active:I = 0x7f0911b1

.field public static final wimax_status_disconnected:I = 0x7f0911b4

.field public static final wimax_status_disconnecting:I = 0x7f0911b3

.field public static final wimax_status_idle:I = 0x7f0911b2

.field public static final wimax_status_initializing:I = 0x7f0911ac

.field public static final wimax_status_ready:I = 0x7f0911ae

.field public static final wimax_status_scanning:I = 0x7f0911ad

.field public static final wimax_stopping:I = 0x7f0911a6

.field public static final wimax_tethered_checkusb:I = 0x7f09119d

.field public static final wimax_tethered_checkusbpath:I = 0x7f0911a0

.field public static final wimax_tethered_mode:I = 0x7f091193

.field public static final wimax_tethered_mode_summary:I = 0x7f091194

.field public static final wimax_tethered_off:I = 0x7f0911a3

.field public static final wimax_tethered_on:I = 0x7f0911a2

.field public static final wimax_tethered_on_warning:I = 0x7f09119f

.field public static final wimax_tethered_quick_toggle_summary:I = 0x7f0911a1

.field public static final wimax_tethered_usbunplug:I = 0x7f09119e

.field public static final wimax_tethered_warning:I = 0x7f0911a4

.field public static final wimax_tethered_warning_popup:I = 0x7f09119c

.field public static final wimax_wifi_warning:I = 0x7f091198

.field public static final wimax_wifi_warning2:I = 0x7f091199

.field public static final window_animation_scale_title:I = 0x7f0908e9

.field public static final wireless_networks_settings_title:I = 0x7f090149

.field public static final with_cicle_summary:I = 0x7f090d81

.field public static final with_cicle_title:I = 0x7f090d80

.field public static final wlan_reselection:I = 0x7f090e64

.field public static final wlan_reselection_summary:I = 0x7f090e65

.field public static final wrong_input_toast:I = 0x7f091026

.field public static final yes:I = 0x7f090047

.field public static final zero:I = 0x7f09003f

.field public static final zone_auto:I = 0x7f09015f

.field public static final zone_auto_summaryOff:I = 0x7f090161

.field public static final zone_auto_summaryOn:I = 0x7f090160

.field public static final zone_list_menu_sort_alphabetically:I = 0x7f090167

.field public static final zone_list_menu_sort_by_timezone:I = 0x7f090168


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4414
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
