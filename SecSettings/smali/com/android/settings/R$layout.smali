.class public final Lcom/android/settings_ex/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings_ex/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final access_point_category_for_setup_wizard_xl:I = 0x7f040000

.field public static final accessibility_settings_setupwizard:I = 0x7f040001

.field public static final accessibility_widget:I = 0x7f040002

.field public static final accessibility_widget_portrait:I = 0x7f040003

.field public static final account_preference:I = 0x7f040004

.field public static final account_provider_preference:I = 0x7f040005

.field public static final account_sync_screen:I = 0x7f040006

.field public static final account_view:I = 0x7f040007

.field public static final actionbar_custom_tab:I = 0x7f040008

.field public static final add_account_screen:I = 0x7f040009

.field public static final airplane_mode_settings:I = 0x7f04000a

.field public static final allshare_welcome:I = 0x7f04000b

.field public static final android_beam:I = 0x7f04000c

.field public static final apn_preference_layout:I = 0x7f04000d

.field public static final app_percentage_item:I = 0x7f04000e

.field public static final apps_spinner_dropdown_item:I = 0x7f04000f

.field public static final apps_spinner_item:I = 0x7f040010

.field public static final asensor:I = 0x7f040011

.field public static final auto_unlock_zone_icon:I = 0x7f040012

.field public static final backup_introduction:I = 0x7f040013

.field public static final backup_list_main:I = 0x7f040014

.field public static final backup_listview:I = 0x7f040015

.field public static final band_mode:I = 0x7f040016

.field public static final battery_info:I = 0x7f040017

.field public static final bluetooth_authorize_service:I = 0x7f040018

.field public static final bluetooth_connection_access:I = 0x7f040019

.field public static final bluetooth_device_picker:I = 0x7f04001a

.field public static final bluetooth_discoverable:I = 0x7f04001b

.field public static final bluetooth_map_access:I = 0x7f04001c

.field public static final bluetooth_no_nearby_devices:I = 0x7f04001d

.field public static final bluetooth_pairing_with_cac:I = 0x7f04001e

.field public static final bluetooth_pairing_with_cac_emergencycall:I = 0x7f04001f

.field public static final bluetooth_pb_access:I = 0x7f040020

.field public static final bluetooth_pin_confirm:I = 0x7f040021

.field public static final bluetooth_pin_entry:I = 0x7f040022

.field public static final bluetooth_sap_access:I = 0x7f040023

.field public static final bluetooth_scan_dialog:I = 0x7f040024

.field public static final bookmark_picker_item:I = 0x7f040025

.field public static final bt_enabling_progress:I = 0x7f040026

.field public static final choose_lock_additional_pin:I = 0x7f040027

.field public static final choose_lock_face_warning:I = 0x7f040028

.field public static final choose_lock_motion:I = 0x7f040029

.field public static final choose_lock_password:I = 0x7f04002a

.field public static final choose_lock_password_emergency:I = 0x7f04002b

.field public static final choose_lock_pattern:I = 0x7f04002c

.field public static final choose_lock_pattern_tutorial:I = 0x7f04002d

.field public static final choose_lock_signature:I = 0x7f04002e

.field public static final common_help_item:I = 0x7f04002f

.field public static final confirm_dialog:I = 0x7f040030

.field public static final confirm_lock_password:I = 0x7f040031

.field public static final confirm_lock_pattern:I = 0x7f040032

.field public static final connectivitylocation_widget:I = 0x7f040033

.field public static final contextualpage_help:I = 0x7f040034

.field public static final credentials_dialog:I = 0x7f040035

.field public static final crypt_decrypt_blank:I = 0x7f040036

.field public static final crypt_decrypt_confirm:I = 0x7f040037

.field public static final crypt_decrypt_password_entry:I = 0x7f040038

.field public static final crypt_decrypt_progress:I = 0x7f040039

.field public static final crypt_decrypt_settings:I = 0x7f04003a

.field public static final crypt_decrypt_status:I = 0x7f04003b

.field public static final crypt_keeper_blank:I = 0x7f04003c

.field public static final crypt_keeper_confirm:I = 0x7f04003d

.field public static final crypt_keeper_password_entry:I = 0x7f04003e

.field public static final crypt_keeper_password_field:I = 0x7f04003f

.field public static final crypt_keeper_progress:I = 0x7f040040

.field public static final crypt_keeper_sdcard_confirm:I = 0x7f040041

.field public static final crypt_keeper_settings:I = 0x7f040042

.field public static final crypt_keeper_settings_sd:I = 0x7f040043

.field public static final crypt_keeper_settings_sd_option:I = 0x7f040044

.field public static final crypt_keeper_status:I = 0x7f040045

.field public static final custom_alert_dialog:I = 0x7f040046

.field public static final custom_offload_preference:I = 0x7f040047

.field public static final custom_preference:I = 0x7f040048

.field public static final custom_title_heading:I = 0x7f040049

.field public static final customer_alert_dialog:I = 0x7f04004a

.field public static final data_usage_app_title:I = 0x7f04004b

.field public static final data_usage_bytes_editor:I = 0x7f04004c

.field public static final data_usage_chart:I = 0x7f04004d

.field public static final data_usage_cycle_editor:I = 0x7f04004e

.field public static final data_usage_cycles:I = 0x7f04004f

.field public static final data_usage_detail:I = 0x7f040050

.field public static final data_usage_header:I = 0x7f040051

.field public static final data_usage_item:I = 0x7f040052

.field public static final data_usage_summary:I = 0x7f040053

.field public static final date_time_dialog:I = 0x7f040054

.field public static final date_time_settings_setupwizard:I = 0x7f040055

.field public static final date_time_setup_custom_list_item_2:I = 0x7f040056

.field public static final device_admin_add:I = 0x7f040057

.field public static final device_admin_item:I = 0x7f040058

.field public static final device_admin_settings:I = 0x7f040059

.field public static final device_name_popup:I = 0x7f04005a

.field public static final dialog_audio_output_mode:I = 0x7f04005b

.field public static final dialog_auto_haptic:I = 0x7f04005c

.field public static final dialog_edittext:I = 0x7f04005d

.field public static final dialog_edittext_phone:I = 0x7f04005e

.field public static final dialog_motion_guide:I = 0x7f04005f

.field public static final dialog_motion_guide_2013:I = 0x7f040060

.field public static final dialog_noti_charges:I = 0x7f040061

.field public static final dialog_pen_keeper_noti:I = 0x7f040062

.field public static final dialog_security_warning:I = 0x7f040063

.field public static final dialog_select_font_style:I = 0x7f040064

.field public static final dialog_select_pattern:I = 0x7f040065

.field public static final dialog_select_pen_detach_noti:I = 0x7f040066

.field public static final dialpad:I = 0x7f040067

.field public static final display:I = 0x7f040068

.field public static final dock_audio_media_enable_dialog:I = 0x7f040069

.field public static final dormant_help:I = 0x7f04006a

.field public static final dormant_help_nocall:I = 0x7f04006b

.field public static final dormantmode_customlist:I = 0x7f04006c

.field public static final dormantmode_customlist_del:I = 0x7f04006d

.field public static final dormantmode_time:I = 0x7f04006e

.field public static final dream_info_row:I = 0x7f04006f

.field public static final drivingmode_widget:I = 0x7f040070

.field public static final easy_mode_help:I = 0x7f040071

.field public static final eth_configure:I = 0x7f040072

.field public static final favoritesettings_widget:I = 0x7f040073

.field public static final feature_settings_setupwizard:I = 0x7f040074

.field public static final file_operation_progress:I = 0x7f040075

.field public static final finger_air_view_help:I = 0x7f040076

.field public static final full_locale_picker_item:I = 0x7f040077

.field public static final gsensor:I = 0x7f040078

.field public static final guide_bt_enable_help:I = 0x7f040079

.field public static final guide_bt_scan_help:I = 0x7f04007a

.field public static final guide_bt_step_1_off:I = 0x7f04007b

.field public static final guide_bt_step_1_off_hotspot:I = 0x7f04007c

.field public static final guide_bt_step_1_off_vzw:I = 0x7f04007d

.field public static final guide_bt_step_1_on:I = 0x7f04007e

.field public static final guide_bt_step_1_on_hotspot:I = 0x7f04007f

.field public static final guide_bt_step_1_on_vzw:I = 0x7f040080

.field public static final guide_bt_step_2:I = 0x7f040081

.field public static final guide_dialog_bubble:I = 0x7f040082

.field public static final guide_nfc:I = 0x7f040083

.field public static final guide_nfc_step_2:I = 0x7f040084

.field public static final guide_power_saving_step_1:I = 0x7f040085

.field public static final guide_power_saving_step_2:I = 0x7f040086

.field public static final guide_smart_block_help:I = 0x7f040087

.field public static final guide_vzw_wifi_step_1_off:I = 0x7f040088

.field public static final guide_vzw_wifi_step_1_on:I = 0x7f040089

.field public static final guide_wifi_enable_help:I = 0x7f04008a

.field public static final guide_wifi_password_dialog:I = 0x7f04008b

.field public static final guide_wifi_scan_help:I = 0x7f04008c

.field public static final guide_wifi_step_1_off:I = 0x7f04008d

.field public static final guide_wifi_step_1_on:I = 0x7f04008e

.field public static final guide_wifi_step_2:I = 0x7f04008f

.field public static final home_settings:I = 0x7f040090

.field public static final home_settings_setupwizard:I = 0x7f040091

.field public static final homesync_account_management:I = 0x7f040092

.field public static final homesync_account_management_row:I = 0x7f040093

.field public static final homesync_backup_and_restore_settings:I = 0x7f040094

.field public static final hs20_picker_dialog:I = 0x7f040095

.field public static final inkeffect_preview:I = 0x7f040096

.field public static final inkeffect_preview_list_item:I = 0x7f040097

.field public static final installed_app_details:I = 0x7f040098

.field public static final intent_sender:I = 0x7f040099

.field public static final introduction:I = 0x7f04009a

.field public static final iwlan_dialog:I = 0x7f04009b

.field public static final iwlan_dialog_row:I = 0x7f04009c

.field public static final keyboard_layout_dialog_switch_hint:I = 0x7f04009d

.field public static final keyguard_appwidget_item:I = 0x7f04009e

.field public static final keyguard_appwidget_picker_layout:I = 0x7f04009f

.field public static final lbslicense:I = 0x7f0400a0

.field public static final linked_contacts_list_row:I = 0x7f0400a1

.field public static final locale_picker:I = 0x7f0400a2

.field public static final lock_screen_clock_myprofile_layout:I = 0x7f0400a3

.field public static final lock_screen_edit_clock:I = 0x7f0400a4

.field public static final lock_screen_shorcut_icon:I = 0x7f0400a5

.field public static final lock_screen_short_camera_layout:I = 0x7f0400a6

.field public static final lock_screen_shortcut_applist_item:I = 0x7f0400a7

.field public static final lock_screen_shortcut_cursor:I = 0x7f0400a8

.field public static final lock_screen_shortcut_cursor_boxedition:I = 0x7f0400a9

.field public static final lock_screen_shortscut_checkbox_edition:I = 0x7f0400aa

.field public static final lock_screen_shortscut_setting:I = 0x7f0400ab

.field public static final main:I = 0x7f0400ac

.field public static final manage_accounts_screen:I = 0x7f0400ad

.field public static final manage_applications_apps:I = 0x7f0400ae

.field public static final manage_applications_content:I = 0x7f0400af

.field public static final manage_applications_item:I = 0x7f0400b0

.field public static final manage_applications_running:I = 0x7f0400b1

.field public static final master_clear:I = 0x7f0400b2

.field public static final master_clear_account:I = 0x7f0400b3

.field public static final master_clear_confirm:I = 0x7f0400b4

.field public static final media_format_final:I = 0x7f0400b5

.field public static final media_format_final_sd:I = 0x7f0400b6

.field public static final media_format_primary:I = 0x7f0400b7

.field public static final media_format_primary_sd:I = 0x7f0400b8

.field public static final mobile_data_alertdialog:I = 0x7f0400b9

.field public static final mode_preview:I = 0x7f0400ba

.field public static final mode_preview_list_item:I = 0x7f0400bb

.field public static final my_place_icon_bt:I = 0x7f0400bc

.field public static final my_place_icon_gps:I = 0x7f0400bd

.field public static final my_place_icon_wifi:I = 0x7f0400be

.field public static final my_place_profile:I = 0x7f0400bf

.field public static final my_profile:I = 0x7f0400c0

.field public static final nfc_explain:I = 0x7f0400c1

.field public static final notification_charge_dialog:I = 0x7f0400c2

.field public static final notification_panel:I = 0x7f0400c3

.field public static final notification_panel_cursor:I = 0x7f0400c4

.field public static final notification_panel_icon:I = 0x7f0400c5

.field public static final null_image_widget_layout:I = 0x7f0400c6

.field public static final number_picker_dialog:I = 0x7f0400c7

.field public static final onehand_help:I = 0x7f0400c8

.field public static final ownerinfo:I = 0x7f0400c9

.field public static final ownerinfo_popup:I = 0x7f0400ca

.field public static final pattern_save_dialog:I = 0x7f0400cb

.field public static final pen_help:I = 0x7f0400cc

.field public static final pen_help_gesture:I = 0x7f0400cd

.field public static final pen_help_hovering:I = 0x7f0400ce

.field public static final pen_help_item:I = 0x7f0400cf

.field public static final pen_help_popup:I = 0x7f0400d0

.field public static final pen_help_quick_command:I = 0x7f0400d1

.field public static final personalvibration_layout:I = 0x7f0400d2

.field public static final pick_item:I = 0x7f0400d3

.field public static final power_saving_mode_tips:I = 0x7f0400d4

.field public static final power_usage_action_item:I = 0x7f0400d5

.field public static final power_usage_detail_item_text:I = 0x7f0400d6

.field public static final power_usage_details:I = 0x7f0400d7

.field public static final power_usage_package_item:I = 0x7f0400d8

.field public static final pref_list_item:I = 0x7f0400d9

.field public static final preference:I = 0x7f0400da

.field public static final preference_batteryhistory:I = 0x7f0400db

.field public static final preference_bluetooth:I = 0x7f0400dc

.field public static final preference_bluetooth_profile:I = 0x7f0400dd

.field public static final preference_dialog:I = 0x7f0400de

.field public static final preference_dialog_audio_balance:I = 0x7f0400df

.field public static final preference_dialog_brightness:I = 0x7f0400e0

.field public static final preference_dialog_flash:I = 0x7f0400e1

.field public static final preference_dialog_font:I = 0x7f0400e2

.field public static final preference_dialog_nousb:I = 0x7f0400e3

.field public static final preference_dialog_power_saving:I = 0x7f0400e4

.field public static final preference_dialog_reset:I = 0x7f0400e5

.field public static final preference_dialog_ringervolume:I = 0x7f0400e6

.field public static final preference_dialog_smart_alert:I = 0x7f0400e7

.field public static final preference_dialog_smart_rotation:I = 0x7f0400e8

.field public static final preference_dialog_smart_stay:I = 0x7f0400e9

.field public static final preference_dialog_vibrationfeedback:I = 0x7f0400ea

.field public static final preference_empty_list:I = 0x7f0400eb

.field public static final preference_header_item:I = 0x7f0400ec

.field public static final preference_header_switch_item:I = 0x7f0400ed

.field public static final preference_icon:I = 0x7f0400ee

.field public static final preference_inputmethod:I = 0x7f0400ef

.field public static final preference_inputmethod_widget:I = 0x7f0400f0

.field public static final preference_memoryusage:I = 0x7f0400f1

.field public static final preference_motion_sensitivity:I = 0x7f0400f2

.field public static final preference_myplace_gps:I = 0x7f0400f3

.field public static final preference_offload_progress_category:I = 0x7f0400f4

.field public static final preference_powergauge:I = 0x7f0400f5

.field public static final preference_progress_category:I = 0x7f0400f6

.field public static final preference_radio:I = 0x7f0400f7

.field public static final preference_settings_checkbox_widget:I = 0x7f0400f8

.field public static final preference_spellchecker:I = 0x7f0400f9

.field public static final preference_tts_engine:I = 0x7f0400fa

.field public static final preference_unclickable_layout:I = 0x7f0400fb

.field public static final preference_user_delete_widget:I = 0x7f0400fc

.field public static final preference_widget_shortcut:I = 0x7f0400fd

.field public static final preference_widget_sync_toggle:I = 0x7f0400fe

.field public static final preference_widget_wifi_signal:I = 0x7f0400ff

.field public static final preference_wifi_ap_allowed_list:I = 0x7f040100

.field public static final preference_wifi_ap_connected_device:I = 0x7f040101

.field public static final preference_wifi_ap_how_to_connect:I = 0x7f040102

.field public static final preference_wifi_ap_settings:I = 0x7f040103

.field public static final profile_icon_small:I = 0x7f040104

.field public static final provider_preference:I = 0x7f040105

.field public static final proxy:I = 0x7f040106

.field public static final quick_setting_icons_container:I = 0x7f040107

.field public static final quick_settings_icon:I = 0x7f040108

.field public static final quick_settings_icon_overlayed:I = 0x7f040109

.field public static final quick_settings_layout:I = 0x7f04010a

.field public static final radio_info:I = 0x7f04010b

.field public static final recommendation_ringtone:I = 0x7f04010c

.field public static final regulatory_info:I = 0x7f04010d

.field public static final remember_dock_setting:I = 0x7f04010e

.field public static final remote_controls:I = 0x7f04010f

.field public static final restore_folder_list:I = 0x7f040110

.field public static final restore_introduction:I = 0x7f040111

.field public static final restore_listview:I = 0x7f040112

.field public static final round_add_icon:I = 0x7f040113

.field public static final round_more_icon:I = 0x7f040114

.field public static final running_processes_item:I = 0x7f040115

.field public static final running_processes_view:I = 0x7f040116

.field public static final running_service_details:I = 0x7f040117

.field public static final running_service_details_process:I = 0x7f040118

.field public static final running_service_details_service:I = 0x7f040119

.field public static final s_beam:I = 0x7f04011a

.field public static final s_motion_guide_layout_2013:I = 0x7f04011b

.field public static final safety_zone_delete_list:I = 0x7f04011c

.field public static final safety_zone_save_dialog:I = 0x7f04011d

.field public static final scan_dialog_button:I = 0x7f04011e

.field public static final scloud_account_preference:I = 0x7f04011f

.field public static final screendialog_editmyprofile:I = 0x7f040120

.field public static final select_map_activity:I = 0x7f040121

.field public static final selectall:I = 0x7f040122

.field public static final separator_label:I = 0x7f040123

.field public static final set_backup_pw:I = 0x7f040124

.field public static final settings_storage_miscfiles:I = 0x7f040125

.field public static final settings_storage_miscfiles_list:I = 0x7f040126

.field public static final settings_top_level:I = 0x7f040127

.field public static final setup_preference:I = 0x7f040128

.field public static final shortcut_item_category:I = 0x7f040129

.field public static final sim_change_alert:I = 0x7f04012a

.field public static final simple_list_item_1:I = 0x7f04012b

.field public static final simple_list_item_multiple_choice:I = 0x7f04012c

.field public static final smart_network_appwidget:I = 0x7f04012d

.field public static final smart_network_guide_dialog:I = 0x7f04012e

.field public static final smartscreen_guide_layout:I = 0x7f04012f

.field public static final smartscreen_guide_layout_frag:I = 0x7f040130

.field public static final smartscreen_help_popup:I = 0x7f040131

.field public static final smartscreen_help_popup_for_scroll:I = 0x7f040132

.field public static final spinner_listitemview:I = 0x7f040133

.field public static final sync_description:I = 0x7f040134

.field public static final t_roaming_settings:I = 0x7f040135

.field public static final tab_widget:I = 0x7f040136

.field public static final tether_help:I = 0x7f040137

.field public static final text_description_preference:I = 0x7f040138

.field public static final title:I = 0x7f040139

.field public static final trusted_credential:I = 0x7f04013a

.field public static final trusted_credential_details:I = 0x7f04013b

.field public static final trusted_credentials:I = 0x7f04013c

.field public static final tw_preference_holo:I = 0x7f04013d

.field public static final tw_select_dialog_singlechoice_holo:I = 0x7f04013e

.field public static final twelve_key_entry:I = 0x7f04013f

.field public static final two_buttons_panel:I = 0x7f040140

.field public static final usage_stats:I = 0x7f040141

.field public static final usage_stats_item:I = 0x7f040142

.field public static final usb_utility:I = 0x7f040143

.field public static final user_dictionary_add_word:I = 0x7f040144

.field public static final user_dictionary_add_word_fullscreen:I = 0x7f040145

.field public static final user_dictionary_item:I = 0x7f040146

.field public static final usp_widget:I = 0x7f040147

.field public static final usp_widget_alternative:I = 0x7f040148

.field public static final usp_widget_indicator:I = 0x7f040149

.field public static final vpn_dialog:I = 0x7f04014a

.field public static final vpn_lockdown_editor:I = 0x7f04014b

.field public static final vzwlbsdialog_checkbox:I = 0x7f04014c

.field public static final wallpaper_list_item:I = 0x7f04014d

.field public static final weak_biometric_fallback_header:I = 0x7f04014e

.field public static final wfd_auto_connect_dialog:I = 0x7f04014f

.field public static final wfd_connect_end_button:I = 0x7f040150

.field public static final wfd_help:I = 0x7f040151

.field public static final wfd_picker:I = 0x7f040152

.field public static final wfd_picker_dialog:I = 0x7f040153

.field public static final wfd_sel_device_body:I = 0x7f040154

.field public static final wfd_sel_device_list_row:I = 0x7f040155

.field public static final wfd_sel_device_title:I = 0x7f040156

.field public static final wfd_write_nfc:I = 0x7f040157

.field public static final wfd_write_nfc2:I = 0x7f040158

.field public static final widget:I = 0x7f040159

.field public static final wifi_advanced_sec_setupwizard:I = 0x7f04015a

.field public static final wifi_ap_5g_warning_dialog:I = 0x7f04015b

.field public static final wifi_ap_allowed_device_add:I = 0x7f04015c

.field public static final wifi_ap_allowed_list:I = 0x7f04015d

.field public static final wifi_ap_custom_action_bar:I = 0x7f04015e

.field public static final wifi_ap_custom_action_bar_2:I = 0x7f04015f

.field public static final wifi_ap_deviceinfo_dialog:I = 0x7f040160

.field public static final wifi_ap_dialog:I = 0x7f040161

.field public static final wifi_ap_disabled_help:I = 0x7f040162

.field public static final wifi_ap_disabled_help_scroll:I = 0x7f040163

.field public static final wifi_ap_feature_warn_dialog:I = 0x7f040164

.field public static final wifi_ap_hide_ssid_warning_dialog:I = 0x7f040165

.field public static final wifi_ap_mpcs_warning_dialog:I = 0x7f040166

.field public static final wifi_ap_open_security_warning_dialog:I = 0x7f040167

.field public static final wifi_ap_timeout:I = 0x7f040168

.field public static final wifi_ap_whitelist_dialog:I = 0x7f040169

.field public static final wifi_api_test:I = 0x7f04016a

.field public static final wifi_config_info:I = 0x7f04016b

.field public static final wifi_config_ui_for_setup_wizard:I = 0x7f04016c

.field public static final wifi_connection_preference_layout:I = 0x7f04016d

.field public static final wifi_dialog:I = 0x7f04016e

.field public static final wifi_dialog_priority:I = 0x7f04016f

.field public static final wifi_dialog_row:I = 0x7f040170

.field public static final wifi_display_options:I = 0x7f040171

.field public static final wifi_display_preference:I = 0x7f040172

.field public static final wifi_error_connection_dialog:I = 0x7f040173

.field public static final wifi_hidden_ap_add_dialog:I = 0x7f040174

.field public static final wifi_hidden_ap_delete_layout:I = 0x7f040175

.field public static final wifi_hidden_ap_list_layout:I = 0x7f040176

.field public static final wifi_hs20_nodevice_layout:I = 0x7f040177

.field public static final wifi_internet_service_check_dialog:I = 0x7f040178

.field public static final wifi_manage_networks:I = 0x7f040179

.field public static final wifi_offload:I = 0x7f04017a

.field public static final wifi_offload_setupwizard:I = 0x7f04017b

.field public static final wifi_p2p_connect_end_button:I = 0x7f04017c

.field public static final wifi_p2p_custom_action_bar:I = 0x7f04017d

.field public static final wifi_p2p_device_picker_layout:I = 0x7f04017e

.field public static final wifi_p2p_deviceinfo_dialog:I = 0x7f04017f

.field public static final wifi_p2p_devicename_dialog:I = 0x7f040180

.field public static final wifi_p2p_dialog:I = 0x7f040181

.field public static final wifi_p2p_dummy_picker:I = 0x7f040182

.field public static final wifi_p2p_help_layout:I = 0x7f040183

.field public static final wifi_p2p_nodevice_layout:I = 0x7f040184

.field public static final wifi_p2p_picker_nodevice_layout:I = 0x7f040185

.field public static final wifi_picker_dialog:I = 0x7f040186

.field public static final wifi_picker_dialog_with_cancel:I = 0x7f040187

.field public static final wifi_sec_setupwizard:I = 0x7f040188

.field public static final wifi_settings_for_setup_wizard_xl:I = 0x7f040189

.field public static final wifi_setup_custom_list_item_1:I = 0x7f04018a

.field public static final wifi_status_test:I = 0x7f04018b

.field public static final wifi_timer:I = 0x7f04018c

.field public static final wifi_vzw_alert_dialog:I = 0x7f04018d

.field public static final wifi_wps_dialog:I = 0x7f04018e

.field public static final zone_picker_list_item:I = 0x7f04018f


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3967
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
