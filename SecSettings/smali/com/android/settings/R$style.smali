.class public final Lcom/android/settings_ex/R$style;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings_ex/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "style"
.end annotation


# static fields
.field public static final ActionBarTabStyle:I = 0x7f10004e

.field public static final ActionBarTabTextStyle:I = 0x7f10004f

.field public static final CommonListItemBackground:I = 0x7f100041

.field public static final CryptKeeperBlankTheme:I = 0x7f100021

.field public static final Custom_Button_Bg:I = 0x7f100042

.field public static final Custom_Button_CheckBox:I = 0x7f100045

.field public static final Custom_Button_Text:I = 0x7f100044

.field public static final Custom_Select_Button_Bg:I = 0x7f100043

.field public static final DialtactsActionBarStyle:I = 0x7f10004d

.field public static final DormantNoContactsTextAppearance:I = 0x7f100046

.field public static final GuideArrowBubbleText:I = 0x7f10003c

.field public static final GuideArrowBubbleText_Down:I = 0x7f10003e

.field public static final GuideArrowBubbleText_DownCenter:I = 0x7f10003f

.field public static final GuideArrowBubbleText_NoArrow:I = 0x7f100040

.field public static final GuideArrowBubbleText_Up:I = 0x7f10003d

.field public static final GuideBubbleText:I = 0x7f10003b

.field public static final InputMethodPreferenceStyle:I = 0x7f100027

.field public static final KeyguardAppWidgetItem:I = 0x7f10002a

.field public static final NoAnimationDialog:I = 0x7f10003a

.field public static final SecurityPreferenceButton:I = 0x7f100023

.field public static final SecurityPreferenceButtonContainer:I = 0x7f100022

.field public static final SettingsContentsListDivider:I = 0x7f100048

.field public static final SettingsDeviceDefaultListDivider:I = 0x7f10004b

.field public static final SettingsTheme:I = 0x7f10002b

.field public static final SettingsTheme_Dialog:I = 0x7f10002c

.field public static final SettingsTheme_DialogWhenLarge:I = 0x7f10002f

.field public static final SettingsTheme_Dialog_Alert:I = 0x7f100031

.field public static final SettingsTheme_Dialog_NoWindowDisplay:I = 0x7f10002d

.field public static final SettingsTheme_NoActionBar:I = 0x7f10002e

.field public static final SettingsTheme_Panel:I = 0x7f100030

.field public static final SetupTitle:I = 0x7f100015

.field public static final SetupWizardBackground:I = 0x7f100000

.field public static final SetupWizardTheme:I = 0x7f100001

.field public static final SetupWizardTheme1:I = 0x7f100004

.field public static final SetupWizardTheme_fHD:I = 0x7f100002

.field public static final SetupWizardTheme_fHD_Wifi:I = 0x7f100033

.field public static final SetupWizardTheme_phone:I = 0x7f100003

.field public static final TabTheme:I = 0x7f10004c

.field public static final TallTitleBarTheme:I = 0x7f100011

.field public static final TextAppearance:I = 0x7f10000d

.field public static final TextAppearance_PagerTabs:I = 0x7f100029

.field public static final TextAppearance_Switch:I = 0x7f100028

.field public static final TextAppearance_info_label:I = 0x7f10000e

.field public static final TextAppearance_info_small:I = 0x7f10000f

.field public static final TextAppearance_info_value:I = 0x7f100010

.field public static final TextView_only_focus:I = 0x7f100047

.field public static final Theme_AccessibilityTutorialActivity:I = 0x7f100051

.field public static final Theme_CreateShortCut:I = 0x7f100012

.field public static final Theme_OffloadDialog:I = 0x7f100034

.field public static final Theme_WifiDialog:I = 0x7f100032

.field public static final Theme_WifiP2PDialog:I = 0x7f100050

.field public static final TopDivider:I = 0x7f100014

.field public static final Transparent:I = 0x7f100020

.field public static final TwHelpDialogTheme:I = 0x7f100039

.field public static final description_shadow:I = 0x7f100006

.field public static final entry_layout:I = 0x7f10000b

.field public static final form_value:I = 0x7f10000c

.field public static final help_bubble:I = 0x7f100049

.field public static final help_bubble_with_button:I = 0x7f10004a

.field public static final info_label:I = 0x7f100007

.field public static final info_layout:I = 0x7f10000a

.field public static final info_small:I = 0x7f100009

.field public static final info_value:I = 0x7f100008

.field public static final page_buddy_help:I = 0x7f100038

.field public static final page_buddy_help_category:I = 0x7f100037

.field public static final pen_help_msg:I = 0x7f100036

.field public static final pen_help_title:I = 0x7f100035

.field public static final setup_wizard_button:I = 0x7f10001f

.field public static final setup_wizard_theme:I = 0x7f100013

.field public static final title_area:I = 0x7f100005

.field public static final vpn_checkbox_value:I = 0x7f100026

.field public static final vpn_label:I = 0x7f100024

.field public static final vpn_value:I = 0x7f100025

.field public static final wifi_item:I = 0x7f100016

.field public static final wifi_item_checkbox:I = 0x7f100019

.field public static final wifi_item_content:I = 0x7f10001b

.field public static final wifi_item_info_content:I = 0x7f10001e

.field public static final wifi_item_info_label:I = 0x7f10001d

.field public static final wifi_item_label:I = 0x7f10001a

.field public static final wifi_section:I = 0x7f10001c

.field public static final wifi_title_match:I = 0x7f100018

.field public static final wifi_wps_dlg:I = 0x7f100017


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
