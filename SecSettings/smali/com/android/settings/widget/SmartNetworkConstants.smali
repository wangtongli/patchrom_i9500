.class public Lcom/android/settings_ex/widget/SmartNetworkConstants;
.super Ljava/lang/Object;
.source "SmartNetworkConstants.java"


# static fields
.field public static final SMART_NETWORK:Ljava/lang/String; = "smart_network"

.field public static final SMART_NETWORK_DEFAULT:I = 0x0

.field public static final SMART_NETWORK_GUIDE_DEFAULT:Z = true

.field public static final SMART_NETWORK_GUIDE_SHAREDPREF:Ljava/lang/String; = "smart_network_guide_sharedpref"

.field public static final SMART_NETWORK_OFF:I = 0x0

.field public static final SMART_NETWORK_ON:I = 0x1


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
