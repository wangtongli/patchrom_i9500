.class public final Lcom/android/settings_ex/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings_ex/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final account_and_sync_account_textsize:I = 0x7f0f00b2

.field public static final account_and_sync_list_height:I = 0x7f0f00b4

.field public static final account_and_sync_summary_textsize:I = 0x7f0f00b3

.field public static final action_bar_switch_padding:I = 0x7f0f0008

.field public static final alert_dialog_padding_left_right:I = 0x7f0f00b5

.field public static final align_btn_layoutHeight:I = 0x7f0f00c5

.field public static final align_btn_layoutWidht:I = 0x7f0f00c4

.field public static final align_btn_margin:I = 0x7f0f00c3

.field public static final align_layoutHeight:I = 0x7f0f00c2

.field public static final app_icon_size:I = 0x7f0f0009

.field public static final appwidget_height:I = 0x7f0f0106

.field public static final appwidget_min_height:I = 0x7f0f0021

.field public static final appwidget_min_width:I = 0x7f0f0020

.field public static final appwidget_preview_height:I = 0x7f0f0024

.field public static final appwidget_preview_width:I = 0x7f0f0023

.field public static final appwidget_smart_network_min_height:I = 0x7f0f00b8

.field public static final appwidget_smart_network_min_width:I = 0x7f0f00b7

.field public static final appwidget_smart_network_text_size:I = 0x7f0f00b6

.field public static final appwidget_width:I = 0x7f0f0105

.field public static final bind_app_widget_dialog_checkbox_bottom_padding:I = 0x7f0f0010

.field public static final bottom_button_text_size:I = 0x7f0f0002

.field public static final bottom_buttons_height:I = 0x7f0f0001

.field public static final bottom_next_btn_paddingRight:I = 0x7f0f0003

.field public static final bt_settings_background_height:I = 0x7f0f0042

.field public static final bt_settings_background_width:I = 0x7f0f0041

.field public static final bt_settings_divider_height:I = 0x7f0f0040

.field public static final bt_settings_divider_width:I = 0x7f0f003f

.field public static final con_bg_height:I = 0x7f0f0110

.field public static final con_btn_bluetooth_width:I = 0x7f0f011e

.field public static final con_btn_data_width:I = 0x7f0f011f

.field public static final con_btn_gps_width:I = 0x7f0f0120

.field public static final con_btn_sync_width:I = 0x7f0f0121

.field public static final con_btn_text_size:I = 0x7f0f0114

.field public static final con_btn_turn_off_margin:I = 0x7f0f0111

.field public static final con_btn_turn_off_text_height:I = 0x7f0f0115

.field public static final con_btn_turn_off_text_marginSide:I = 0x7f0f0116

.field public static final con_btn_turn_off_width:I = 0x7f0f0112

.field public static final con_btn_wifi_width:I = 0x7f0f011d

.field public static final con_img_icon_height:I = 0x7f0f0118

.field public static final con_img_icon_marginTop:I = 0x7f0f011b

.field public static final con_img_icon_width:I = 0x7f0f0117

.field public static final con_img_turn_off_marginTop:I = 0x7f0f0113

.field public static final con_ind_bluetooth_width:I = 0x7f0f0123

.field public static final con_ind_data_width:I = 0x7f0f0124

.field public static final con_ind_gps_width:I = 0x7f0f0125

.field public static final con_ind_height:I = 0x7f0f0127

.field public static final con_ind_marginLeft:I = 0x7f0f0128

.field public static final con_ind_marginTop:I = 0x7f0f011c

.field public static final con_ind_sync_width:I = 0x7f0f0126

.field public static final con_ind_wifi_width:I = 0x7f0f0122

.field public static final con_text_height:I = 0x7f0f0119

.field public static final con_text_marginTop:I = 0x7f0f011a

.field public static final content_margin_left:I = 0x7f0f000d

.field public static final crypt_clock_size:I = 0x7f0f0015

.field public static final custom_colorview_parentlayout_marginLeft:I = 0x7f0f00d5

.field public static final custom_colorview_parentlayout_marginRight:I = 0x7f0f00d6

.field public static final custom_colorview_parentlayout_marginTop:I = 0x7f0f00d7

.field public static final custom_gradientcolorview_layoutHeight:I = 0x7f0f00da

.field public static final custom_splitcolorview_layoutHeight:I = 0x7f0f00d9

.field public static final data_usage_chart_height:I = 0x7f0f0012

.field public static final data_usage_chart_optimalWidth:I = 0x7f0f0013

.field public static final datetime_margin_bottom:I = 0x7f0f0108

.field public static final datetime_margin_top:I = 0x7f0f0107

.field public static final description_margin_sides:I = 0x7f0f000f

.field public static final description_margin_top:I = 0x7f0f000e

.field public static final device_memory_usage_button_height:I = 0x7f0f0007

.field public static final device_memory_usage_button_width:I = 0x7f0f0006

.field public static final dialog_editprofile_edittext_margin:I = 0x7f0f00d3

.field public static final dialog_editprofile_label_layoutHeight:I = 0x7f0f00d0

.field public static final dialog_editprofile_label_paddingLeft:I = 0x7f0f00d2

.field public static final dialog_editprofile_label_textSize:I = 0x7f0f00d1

.field public static final divider_height:I = 0x7f0f001c

.field public static final divider_margin_bottom:I = 0x7f0f001e

.field public static final divider_margin_top:I = 0x7f0f001d

.field public static final edit_preview_font_maximum_size:I = 0x7f0f00bf

.field public static final edit_preview_font_minimum_size:I = 0x7f0f00c0

.field public static final editprofile_layoutHeight:I = 0x7f0f00c1

.field public static final favorite_settings_widget_image_height:I = 0x7f0f010c

.field public static final favorite_settings_widget_image_width:I = 0x7f0f010b

.field public static final favorite_settings_widget_text_height:I = 0x7f0f010d

.field public static final favorite_settings_widget_text_margin_top:I = 0x7f0f010e

.field public static final favorite_settings_widget_text_size:I = 0x7f0f010f

.field public static final font_size_land:I = 0x7f0f0034

.field public static final fontsize_layoutHeight:I = 0x7f0f00c7

.field public static final fontsize_spinner_layoutHeight:I = 0x7f0f00ca

.field public static final fontsize_spinner_layoutWidht:I = 0x7f0f00c9

.field public static final fontsize_spinner_marginLeft:I = 0x7f0f00c8

.field public static final guide_actionbar_bubble_margin_top:I = 0x7f0f00a0

.field public static final guide_actionbar_height:I = 0x7f0f009e

.field public static final guide_actionbar_land_buttons_margin_left:I = 0x7f0f00a5

.field public static final guide_actionbar_land_height:I = 0x7f0f009f

.field public static final guide_actionbar_switcher_bubble_left_margin:I = 0x7f0f0045

.field public static final guide_arrow_bubble_padding_left:I = 0x7f0f008a

.field public static final guide_arrow_bubble_padding_right:I = 0x7f0f0089

.field public static final guide_arrow_center_down_bubble_padding_bottom:I = 0x7f0f0091

.field public static final guide_arrow_center_down_bubble_padding_top:I = 0x7f0f0090

.field public static final guide_arrow_down_bubble_margin_bottom:I = 0x7f0f0095

.field public static final guide_arrow_down_bubble_padding_bottom:I = 0x7f0f008e

.field public static final guide_arrow_down_bubble_padding_top:I = 0x7f0f008d

.field public static final guide_arrow_down_center_bubble_margin_top:I = 0x7f0f008f

.field public static final guide_arrow_height:I = 0x7f0f00b1

.field public static final guide_arrow_up_bubble_margin_pointer_right:I = 0x7f0f0097

.field public static final guide_arrow_up_bubble_margin_top:I = 0x7f0f0092

.field public static final guide_arrow_up_bubble_padding_bottom:I = 0x7f0f008c

.field public static final guide_arrow_up_bubble_padding_top:I = 0x7f0f008b

.field public static final guide_bottom_button_height_punch:I = 0x7f0f0098

.field public static final guide_bottom_buttons_height:I = 0x7f0f009d

.field public static final guide_bottom_pointer_margin_bottom:I = 0x7f0f0099

.field public static final guide_bt_bottom_pointer_margin_left:I = 0x7f0f009a

.field public static final guide_bt_land_scan_button_pointer_margin_top:I = 0x7f0f00a4

.field public static final guide_bt_land_scan_button_width:I = 0x7f0f00a2

.field public static final guide_bt_pointer_step_1_land_margin_bottom:I = 0x7f0f0109

.field public static final guide_bt_pointer_step_1_land_margin_right:I = 0x7f0f006a

.field public static final guide_bt_pointer_step_1_land_margin_top:I = 0x7f0f0069

.field public static final guide_bt_pointer_step_1_port_margin_right:I = 0x7f0f0068

.field public static final guide_bt_pointer_step_1_port_margin_top:I = 0x7f0f0067

.field public static final guide_bt_switcher_step_1_land_margin_right:I = 0x7f0f005c

.field public static final guide_bt_switcher_step_1_land_margin_top:I = 0x7f0f005d

.field public static final guide_bt_switcher_step_1_land_margin_top_hotspot:I = 0x7f0f005e

.field public static final guide_bt_switcher_step_1_port_margin_right:I = 0x7f0f0058

.field public static final guide_bt_switcher_step_1_port_margin_top:I = 0x7f0f0059

.field public static final guide_bt_switcher_step_1_port_margin_top_hotspot:I = 0x7f0f005a

.field public static final guide_bt_switcher_step_1_port_margin_top_vzw:I = 0x7f0f005b

.field public static final guide_btsettings_scan_buttons_up:I = 0x7f0f00b0

.field public static final guide_bubble_text_size:I = 0x7f0f0046

.field public static final guide_closed_bubble_close_img_margin_right:I = 0x7f0f0047

.field public static final guide_closed_bubble_close_img_margin_top:I = 0x7f0f0048

.field public static final guide_closed_bubble_divider_margin_right:I = 0x7f0f0049

.field public static final guide_enable_pointer_port_margin_right:I = 0x7f0f0064

.field public static final guide_enable_pointer_port_margin_top:I = 0x7f0f0063

.field public static final guide_hand_left_margin:I = 0x7f0f0044

.field public static final guide_hand_top_margin:I = 0x7f0f0043

.field public static final guide_land_closed_bubble_margin_right:I = 0x7f0f0050

.field public static final guide_land_closed_bubble_margin_top:I = 0x7f0f004e

.field public static final guide_land_closed_bubble_padding_bottom:I = 0x7f0f0051

.field public static final guide_land_closed_bubble_padding_left:I = 0x7f0f004f

.field public static final guide_land_scan_button_height:I = 0x7f0f00a3

.field public static final guide_no_arrow_bubble_margin_bottom:I = 0x7f0f0093

.field public static final guide_no_arrow_bubble_margin_right:I = 0x7f0f0096

.field public static final guide_no_arrow_bubble_margin_top:I = 0x7f0f0094

.field public static final guide_password_dlg_margin_bottom:I = 0x7f0f00a1

.field public static final guide_port_closed_bubble_margin_right:I = 0x7f0f004c

.field public static final guide_port_closed_bubble_margin_top:I = 0x7f0f004a

.field public static final guide_port_closed_bubble_padding_bottom:I = 0x7f0f004d

.field public static final guide_port_closed_bubble_padding_left:I = 0x7f0f004b

.field public static final guide_settings_bluetooth_land_left:I = 0x7f0f00af

.field public static final guide_settings_bluetooth_land_right:I = 0x7f0f00ad

.field public static final guide_settings_bluetooth_land_top:I = 0x7f0f00aa

.field public static final guide_settings_bluetooth_land_top_hotspot:I = 0x7f0f00ab

.field public static final guide_settings_bluetooth_port_left:I = 0x7f0f00ae

.field public static final guide_settings_bluetooth_port_right:I = 0x7f0f00ac

.field public static final guide_settings_bluetooth_port_top:I = 0x7f0f00a7

.field public static final guide_settings_bluetooth_port_top_hotspot:I = 0x7f0f00a8

.field public static final guide_settings_bluetooth_port_top_vzw:I = 0x7f0f00a9

.field public static final guide_step_1_land_bt_list_item_margin_top:I = 0x7f0f0078

.field public static final guide_step_1_land_bt_list_item_margin_top_hotspot:I = 0x7f0f0079

.field public static final guide_step_1_land_bt_pointer_margin_bottom:I = 0x7f0f010a

.field public static final guide_step_1_land_bt_pointer_margin_top:I = 0x7f0f007b

.field public static final guide_step_1_land_list_item_height:I = 0x7f0f0073

.field public static final guide_step_1_land_list_item_width:I = 0x7f0f0072

.field public static final guide_step_1_land_vzw_wifi_list_item_margin_top:I = 0x7f0f0086

.field public static final guide_step_1_land_vzw_wifi_pointer_margin_top:I = 0x7f0f0088

.field public static final guide_step_1_land_wifi_list_item_margin_top:I = 0x7f0f007f

.field public static final guide_step_1_land_wifi_pointer_margin_top:I = 0x7f0f0080

.field public static final guide_step_1_list_item_margin_left:I = 0x7f0f0074

.field public static final guide_step_1_pointer_margin_left:I = 0x7f0f007c

.field public static final guide_step_1_port_bt_list_item_margin_top:I = 0x7f0f0075

.field public static final guide_step_1_port_bt_list_item_margin_top_hotspot:I = 0x7f0f0076

.field public static final guide_step_1_port_bt_list_item_margin_top_vzw:I = 0x7f0f0077

.field public static final guide_step_1_port_bt_pointer_margin_top:I = 0x7f0f007a

.field public static final guide_step_1_port_list_item_height:I = 0x7f0f0071

.field public static final guide_step_1_port_list_item_width:I = 0x7f0f0070

.field public static final guide_step_1_port_vzw_wifi_list_item_margin_top:I = 0x7f0f0085

.field public static final guide_step_1_port_vzw_wifi_pointer_margin_top:I = 0x7f0f0087

.field public static final guide_step_1_port_wifi_list_item_margin_top:I = 0x7f0f007d

.field public static final guide_step_1_port_wifi_pointer_margin_top:I = 0x7f0f007e

.field public static final guide_switcher_height:I = 0x7f0f0053

.field public static final guide_switcher_land_margin_right:I = 0x7f0f0055

.field public static final guide_switcher_land_margin_top:I = 0x7f0f0057

.field public static final guide_switcher_port_margin_right:I = 0x7f0f0054

.field public static final guide_switcher_port_margin_top:I = 0x7f0f0056

.field public static final guide_switcher_width:I = 0x7f0f0052

.field public static final guide_vzw_wifi_pointer_step_1_land_margin_top:I = 0x7f0f0084

.field public static final guide_vzw_wifi_pointer_step_1_port_margin_top:I = 0x7f0f0083

.field public static final guide_vzw_wifi_switcher_step_1_land_margin_top:I = 0x7f0f0082

.field public static final guide_vzw_wifi_switcher_step_1_port_margin_top:I = 0x7f0f0081

.field public static final guide_wifi_bottom_button_width:I = 0x7f0f009b

.field public static final guide_wifi_bottom_pointer_margin_left:I = 0x7f0f009c

.field public static final guide_wifi_enable_pointer_land_margin_right:I = 0x7f0f0066

.field public static final guide_wifi_enable_pointer_land_margin_top:I = 0x7f0f0065

.field public static final guide_wifi_pointer_enable_land_margin_right:I = 0x7f0f006f

.field public static final guide_wifi_pointer_step_1_land_margin_right:I = 0x7f0f006e

.field public static final guide_wifi_pointer_step_1_land_margin_top:I = 0x7f0f006d

.field public static final guide_wifi_pointer_step_1_port_margin_right:I = 0x7f0f006c

.field public static final guide_wifi_pointer_step_1_port_margin_top:I = 0x7f0f006b

.field public static final guide_wifi_security_margin_bottom:I = 0x7f0f00a6

.field public static final guide_wifi_switcher_step_1_land_margin_right:I = 0x7f0f0061

.field public static final guide_wifi_switcher_step_1_land_margin_top:I = 0x7f0f0062

.field public static final guide_wifi_switcher_step_1_port_margin_right:I = 0x7f0f005f

.field public static final guide_wifi_switcher_step_1_port_margin_top:I = 0x7f0f0060

.field public static final header_icon_width:I = 0x7f0f001f

.field public static final image_and_text_padding:I = 0x7f0f002e

.field public static final image_and_text_padding_land:I = 0x7f0f0031

.field public static final image_upper_padding:I = 0x7f0f0030

.field public static final image_upper_padding_first:I = 0x7f0f002f

.field public static final image_upper_padding_first_land:I = 0x7f0f0032

.field public static final image_upper_padding_land:I = 0x7f0f0033

.field public static final installed_app_details_bullet_offset:I = 0x7f0f0011

.field public static final keyguard_appwidget_picker_margin_left:I = 0x7f0f0026

.field public static final keyguard_appwidget_picker_margin_right:I = 0x7f0f0027

.field public static final keyguard_appwidget_picker_max_width:I = 0x7f0f0025

.field public static final layout_height_land:I = 0x7f0f0036

.field public static final line_bar_size_land:I = 0x7f0f0035

.field public static final lock_screen_shortcut_app_icon_size:I = 0x7f0f0028

.field public static final lock_screen_shortcut_cursor_height:I = 0x7f0f0029

.field public static final lock_screen_shortcut_cursor_width:I = 0x7f0f002a

.field public static final lock_screen_shortcut_cursor_width_5icon:I = 0x7f0f002b

.field public static final lock_screen_shortcut_delete_size:I = 0x7f0f002d

.field public static final lock_screen_shortcut_dragging_shadow_position:I = 0x7f0f002c

.field public static final margin_from_right:I = 0x7f0f00c6

.field public static final myplace_location_edit_area_btn_height:I = 0x7f0f00e3

.field public static final myplace_location_edit_area_btn_width:I = 0x7f0f00e2

.field public static final myplace_location_edit_area_margin_between_view_height:I = 0x7f0f00e0

.field public static final myplace_location_edit_area_margin_between_view_width:I = 0x7f0f00e1

.field public static final myplace_location_edit_area_margin_bottom:I = 0x7f0f00df

.field public static final myplace_location_edit_area_margin_left_right:I = 0x7f0f00dd

.field public static final myplace_location_edit_area_margin_top:I = 0x7f0f00de

.field public static final myplace_map_capture_height:I = 0x7f0f00e5

.field public static final myplace_map_capture_width:I = 0x7f0f00e4

.field public static final myprofile_time_large_size:I = 0x7f0f00db

.field public static final myprofile_time_small_size:I = 0x7f0f00dc

.field public static final next_arrow_paddingLeft:I = 0x7f0f0004

.field public static final notification_bar_margin_left:I = 0x7f0f0103

.field public static final notification_bar_width:I = 0x7f0f0102

.field public static final notification_image_height:I = 0x7f0f00fc

.field public static final notification_image_margin_top:I = 0x7f0f00fd

.field public static final notification_image_width:I = 0x7f0f00fb

.field public static final notification_panel_height:I = 0x7f0f00fa

.field public static final notification_panel_width:I = 0x7f0f00f9

.field public static final notification_text_height:I = 0x7f0f00fe

.field public static final notification_text_line_spacing:I = 0x7f0f0101

.field public static final notification_text_margin_top:I = 0x7f0f00ff

.field public static final notification_text_size:I = 0x7f0f0100

.field public static final pager_tabs_padding:I = 0x7f0f0022

.field public static final password_lock_headerText_side_padding:I = 0x7f0f0000

.field public static final powersavingmode_help_dialog_tap_bubble_body_margin_top:I = 0x7f0f00ef

.field public static final powersavingmode_help_dialog_tap_bubble_margin_top:I = 0x7f0f00ea

.field public static final powersavingmode_help_dialog_tap_bubble_max_width:I = 0x7f0f00e9

.field public static final powersavingmode_help_dialog_tap_bubble_padding_bottom:I = 0x7f0f00ec

.field public static final powersavingmode_help_dialog_tap_bubble_padding_right:I = 0x7f0f00ed

.field public static final powersavingmode_help_dialog_tap_bubble_padding_top:I = 0x7f0f00eb

.field public static final powersavingmode_help_dialog_tap_bubble_picker_margin_top:I = 0x7f0f00f0

.field public static final powersavingmode_help_dialog_tap_bubble_text_size:I = 0x7f0f00ee

.field public static final powersavingmode_help_dialog_tap_img_height:I = 0x7f0f00e7

.field public static final powersavingmode_help_dialog_tap_img_margin_right:I = 0x7f0f00e8

.field public static final powersavingmode_help_dialog_tap_img_width:I = 0x7f0f00e6

.field public static final powersavingmode_help_edit_bubble_close_btn_margin_right:I = 0x7f0f00f4

.field public static final powersavingmode_help_edit_bubble_close_btn_margin_top:I = 0x7f0f00f3

.field public static final powersavingmode_help_edit_bubble_margin_bottom:I = 0x7f0f00f1

.field public static final powersavingmode_help_edit_bubble_padding_right:I = 0x7f0f00f2

.field public static final powersavingmode_help_touchable_switch_height:I = 0x7f0f00f6

.field public static final powersavingmode_help_touchable_switch_margin_right:I = 0x7f0f00f8

.field public static final powersavingmode_help_touchable_switch_margin_top:I = 0x7f0f00f7

.field public static final powersavingmode_help_touchable_switch_width:I = 0x7f0f00f5

.field public static final preference_padding_left:I = 0x7f0f0104

.field public static final preview_text_layout_padding:I = 0x7f0f00d8

.field public static final previewarea_layoutHeight:I = 0x7f0f00cb

.field public static final previewarea_layout_marginBottom:I = 0x7f0f00cf

.field public static final previewarea_layout_marginLeft:I = 0x7f0f00cd

.field public static final previewarea_layout_marginRight:I = 0x7f0f00cc

.field public static final previewarea_layout_marginTop:I = 0x7f0f00ce

.field public static final screen_layout_paddingLeft:I = 0x7f0f00bd

.field public static final screen_layout_paddingRight:I = 0x7f0f00be

.field public static final screen_margin_bottom:I = 0x7f0f000c

.field public static final screen_margin_sides:I = 0x7f0f000a

.field public static final screen_margin_top:I = 0x7f0f000b

.field public static final setup_border_width:I = 0x7f0f0017

.field public static final setup_button_size:I = 0x7f0f001a

.field public static final setup_item_margin:I = 0x7f0f001b

.field public static final setup_margin_bottom:I = 0x7f0f0018

.field public static final setup_title_height:I = 0x7f0f0016

.field public static final setup_title_size:I = 0x7f0f0019

.field public static final setupwizard_bottom_button_area_paddingTop:I = 0x7f0f0005

.field public static final setupwizard_wifi_screen_margin_bottom:I = 0x7f0f00bc

.field public static final setupwizard_wifi_screen_margin_left:I = 0x7f0f00b9

.field public static final setupwizard_wifi_screen_margin_right:I = 0x7f0f00ba

.field public static final setupwizard_wifi_screen_margin_top:I = 0x7f0f00bb

.field public static final spinner_textview_layoutHeight:I = 0x7f0f00d4

.field public static final tw_preference_fragment_padding_bottom:I = 0x7f0f003d

.field public static final tw_preference_fragment_padding_side:I = 0x7f0f003c

.field public static final tw_tab_padding_side:I = 0x7f0f003e

.field public static final uspwidget_btn_img_height:I = 0x7f0f0038

.field public static final uspwidget_btn_img_width:I = 0x7f0f0037

.field public static final uspwidget_img_padding_top:I = 0x7f0f0039

.field public static final uspwidget_text_height:I = 0x7f0f003a

.field public static final uspwidget_text_size:I = 0x7f0f003b

.field public static final volume_seekbar_side_margin:I = 0x7f0f0014


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 851
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
