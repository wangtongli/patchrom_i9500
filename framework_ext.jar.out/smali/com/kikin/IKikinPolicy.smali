.class public interface abstract Lcom/kikin/IKikinPolicy;
.super Ljava/lang/Object;
.source "IKikinPolicy.java"


# virtual methods
.method public abstract analyzeModifiedSelection(Landroid/os/Bundle;Ljava/lang/String;)V
.end method

.method public abstract doKikinSearch(Landroid/os/Bundle;)V
.end method

.method public abstract hideKikinResultsView(Z)V
.end method

.method public abstract isKikinResultsViewVisible()Z
.end method

.method public abstract resetSelection()V
.end method

.method public abstract setKikinResultsViewCallbackHandler(Lcom/kikin/IKikinResultsViewCallbackHandler;)V
.end method

.method public abstract setSelectionTopPosition(I)V
.end method

.method public abstract showKikinResultsView(Landroid/view/ActionMode;Z)V
.end method
