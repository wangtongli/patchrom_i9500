.class public interface abstract Lcom/kikin/IKikinSelectionCallbackHandler;
.super Ljava/lang/Object;
.source "IKikinSelectionCallbackHandler.java"


# virtual methods
.method public abstract getHandlerView()Landroid/view/View;
.end method

.method public abstract isSelectingText()Z
.end method

.method public abstract updateSelection(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract updateSelectionInCAB(Ljava/lang/String;)V
.end method
