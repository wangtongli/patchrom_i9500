.class public Lcom/sec/android/app/camera/CommonFeature;
.super Ljava/lang/Object;
.source "CommonFeature.java"


# static fields
.field public static final ACTION_SHOT_RESOLUTION:Ljava/lang/String; = "640x480"

.field public static final ACTIVITY_CLASS_NAME_GALLERY3D:Ljava/lang/String; = "com.sec.android.gallery3d.app.Gallery"

.field public static final ACTIVITY_CLASS_NAME_VIDEOPLAYER:Ljava/lang/String; = "com.sec.android.app.videoplayer.activity.MoviePlayer"

.field public static final AMOLED_DISPLAY:Z = true

.field public static final AUTOFOCUS_EXCUTE_FLASH:Z = false

.field public static final BACK_AUTO_NIGHT_DETECTION_PREVIEW_FPS_MAX:I = 0x7530

.field public static final BACK_AUTO_NIGHT_DETECTION_PREVIEW_FPS_MIN:I = 0x1b58

.field public static final BACK_CAMCORDER_RESOLUTION_1280X720:Z = true

.field public static final BACK_CAMCORDER_RESOLUTION_1280X720_PICTURE_HEIGHT:I = 0x912

.field public static final BACK_CAMCORDER_RESOLUTION_1280X720_PICTURE_WIDTH:I = 0x1020

.field public static final BACK_CAMCORDER_RESOLUTION_1440X1080:Z = true

.field public static final BACK_CAMCORDER_RESOLUTION_1440X1080_PICTURE_HEIGHT:I = 0xc18

.field public static final BACK_CAMCORDER_RESOLUTION_1440X1080_PICTURE_WIDTH:I = 0x1020

.field public static final BACK_CAMCORDER_RESOLUTION_176X144:Z = false

.field public static final BACK_CAMCORDER_RESOLUTION_1920X1080:Z = true

.field public static final BACK_CAMCORDER_RESOLUTION_1920X1080_PICTURE_HEIGHT:I = 0x912

.field public static final BACK_CAMCORDER_RESOLUTION_1920X1080_PICTURE_WIDTH:I = 0x1020

.field public static final BACK_CAMCORDER_RESOLUTION_320X240:Z = true

.field public static final BACK_CAMCORDER_RESOLUTION_320X240_PICTURE_HEIGHT:I = 0xc18

.field public static final BACK_CAMCORDER_RESOLUTION_320X240_PICTURE_WIDTH:I = 0x1020

.field public static final BACK_CAMCORDER_RESOLUTION_640X480:Z = false

.field public static final BACK_CAMCORDER_RESOLUTION_640X480_PICTURE_HEIGHT:I = 0xc18

.field public static final BACK_CAMCORDER_RESOLUTION_640X480_PICTURE_WIDTH:I = 0x1020

.field public static final BACK_CAMCORDER_RESOLUTION_720X480:Z = false

.field public static final BACK_CAMCORDER_RESOLUTION_720X480_PICTURE_HEIGHT:I = 0xac0

.field public static final BACK_CAMCORDER_RESOLUTION_720X480_PICTURE_WIDTH:I = 0x1020

.field public static final BACK_CAMCORDER_RESOLUTION_960X720:Z = false

.field public static final BACK_CAMCORDER_RESOLUTION_960X720_PICTURE_HEIGHT:I = 0xc18

.field public static final BACK_CAMCORDER_RESOLUTION_960X720_PICTURE_WIDTH:I = 0x1020

.field public static final BACK_CAMERA_PICTURE_DEFAULT_RESOLUTION:Ljava/lang/String; = "4128x2322"

.field public static final BACK_CAMERA_PICTURE_MAX_RESOLUTION:Ljava/lang/String; = "4128x2322"

.field public static final BACK_CAMERA_RECORDING_DEFAULT_RESOLUTION:Ljava/lang/String; = "1920x1080"

.field public static final BACK_CAMERA_RECORDING_MAX_RESOLUTION:Ljava/lang/String; = "1920x1080"

.field public static final BACK_CAMERA_RESOLUTION_1248X672:Z = false

.field public static final BACK_CAMERA_RESOLUTION_1280X720:Z = false

.field public static final BACK_CAMERA_RESOLUTION_1280X960:Z = false

.field public static final BACK_CAMERA_RESOLUTION_1392X1392:Z = false

.field public static final BACK_CAMERA_RESOLUTION_1440X1080:Z = false

.field public static final BACK_CAMERA_RESOLUTION_1536X864:Z = false

.field public static final BACK_CAMERA_RESOLUTION_1600X1200:Z = false

.field public static final BACK_CAMERA_RESOLUTION_1600X960:Z = false

.field public static final BACK_CAMERA_RESOLUTION_1632X880:Z = false

.field public static final BACK_CAMERA_RESOLUTION_176X144:Z = false

.field public static final BACK_CAMERA_RESOLUTION_1920X1080:Z = false

.field public static final BACK_CAMERA_RESOLUTION_2048X1104:Z = false

.field public static final BACK_CAMERA_RESOLUTION_2048X1152:Z = true

.field public static final BACK_CAMERA_RESOLUTION_2048X1232:Z = false

.field public static final BACK_CAMERA_RESOLUTION_2048X1536:Z = false

.field public static final BACK_CAMERA_RESOLUTION_2560X1440:Z = false

.field public static final BACK_CAMERA_RESOLUTION_2560X1536:Z = false

.field public static final BACK_CAMERA_RESOLUTION_2560X1920:Z = false

.field public static final BACK_CAMERA_RESOLUTION_2592X1944:Z = false

.field public static final BACK_CAMERA_RESOLUTION_3072X1856:Z = false

.field public static final BACK_CAMERA_RESOLUTION_3072X2304:Z = false

.field public static final BACK_CAMERA_RESOLUTION_320X240:Z = false

.field public static final BACK_CAMERA_RESOLUTION_3264X1836:Z = true

.field public static final BACK_CAMERA_RESOLUTION_3264X1968:Z = false

.field public static final BACK_CAMERA_RESOLUTION_3264X2448:Z = true

.field public static final BACK_CAMERA_RESOLUTION_400X240:Z = false

.field public static final BACK_CAMERA_RESOLUTION_4096X2304:Z = false

.field public static final BACK_CAMERA_RESOLUTION_4096X3072:Z = false

.field public static final BACK_CAMERA_RESOLUTION_4128X2322:Z = true

.field public static final BACK_CAMERA_RESOLUTION_4128X3096:Z = true

.field public static final BACK_CAMERA_RESOLUTION_640X480:Z = false

.field public static final BACK_CAMERA_RESOLUTION_720X480:Z = false

.field public static final BACK_CAMERA_RESOLUTION_800X480:Z = false

.field public static final BACK_CAMERA_SHOOTINGMODE_3DPANORAMA:Z = false

.field public static final BACK_CAMERA_SHOOTINGMODE_ACTION_SHOT:Z = false

.field public static final BACK_CAMERA_SHOOTINGMODE_ADD_ME:Z = false

.field public static final BACK_CAMERA_SHOOTINGMODE_AQUA:Z = false

.field public static final BACK_CAMERA_SHOOTINGMODE_AUTO:Z = true

.field public static final BACK_CAMERA_SHOOTINGMODE_AUTO_PORTRAIT:Z = false

.field public static final BACK_CAMERA_SHOOTINGMODE_BEAUTY_SHOT:Z = true

.field public static final BACK_CAMERA_SHOOTINGMODE_BEST_FACE_SHOT:Z = true

.field public static final BACK_CAMERA_SHOOTINGMODE_BEST_PHOTO_SHOT:Z = true

.field public static final BACK_CAMERA_SHOOTINGMODE_BURST_SHOT:Z = false

.field public static final BACK_CAMERA_SHOOTINGMODE_CARTOON:Z = false

.field public static final BACK_CAMERA_SHOOTINGMODE_CINEPIC:Z = true

.field public static final BACK_CAMERA_SHOOTINGMODE_CONTINUOUS_SHOT:Z = false

.field public static final BACK_CAMERA_SHOOTINGMODE_DRAMA:Z = true

.field public static final BACK_CAMERA_SHOOTINGMODE_ERASER_SHOT:Z = true

.field public static final BACK_CAMERA_SHOOTINGMODE_FRAME_SHOT:Z = false

.field public static final BACK_CAMERA_SHOOTINGMODE_GOLF:Z = false

.field public static final BACK_CAMERA_SHOOTINGMODE_MOSAIC_SHOT:Z = false

.field public static final BACK_CAMERA_SHOOTINGMODE_NIGHT_SHOT:Z = true

.field public static final BACK_CAMERA_SHOOTINGMODE_PANORAMA_SHOT:Z = true

.field public static final BACK_CAMERA_SHOOTINGMODE_PETDET_SHOT:Z = false

.field public static final BACK_CAMERA_SHOOTINGMODE_RICH_TONE:Z = true

.field public static final BACK_CAMERA_SHOOTINGMODE_SMILE_SHOT:Z = false

.field public static final BACK_CAMERA_SHOOTINGMODE_SNS:Z = false

.field public static final BACK_CAMERA_SHOOTINGMODE_SOUNDSHOT:Z = true

.field public static final BACK_CAMERA_SHOOTINGMODE_SPORTS_SCENE:Z = true

.field public static final BACK_CAMERA_SHOOTINGMODE_STOP_MOTION:Z = false

.field public static final BACK_CAMERA_SHOOTINGMODE_VINTAGE_SHOT:Z = false

.field public static final BEAUTYSHOT_NOT_SUPPORT_ZOOM:Z = false

.field public static final BEST_RESOLUTION:Ljava/lang/String; = "4128x2322"

.field public static final BURSTSHOT_EFFECT_RESET_NONE:Z = true

.field public static final BURSTSHOT_NOT_SUPPORT_ZOOM:Z = false

.field public static final BURST_PANORAMA_RESOLUTION:Ljava/lang/String; = "3264x1836"

.field public static final BURST_RESOLUTION:Ljava/lang/String; = "4128x2322"

.field public static final BURST_SHOT_IN_SETTINGS:Z = true

.field public static final CAMCORDER_720P_PREVIEW_FPS_MAX:I = 0x7530

.field public static final CAMCORDER_720P_PREVIEW_FPS_MIN:I = 0x3a98

.field public static final CAMCORDER_ANTISHAKE:Z = true

.field public static final CAMCORDER_CONTINUOUS_AF:Z = true

.field public static final CAMCORDER_PREVIEW_FPS_MAX:I = 0x7530

.field public static final CAMCORDER_PREVIEW_FPS_MAX_MMS:I = 0x3a98

.field public static final CAMCORDER_PREVIEW_FPS_MIN:I = 0x7530

.field public static final CAMCORDER_PREVIEW_FPS_MIN_MMS:I = 0x3a98

.field public static final CAMCORDER_RESOLUTION_1280X720_PREVIEW_HEIGHT:I = 0x438

.field public static final CAMCORDER_RESOLUTION_1280X720_PREVIEW_WIDTH:I = 0x780

.field public static final CAMCORDER_RESOLUTION_1440X1080_PREVIEW_HEIGHT:I = 0x438

.field public static final CAMCORDER_RESOLUTION_1440X1080_PREVIEW_WIDTH:I = 0x5a0

.field public static final CAMCORDER_RESOLUTION_176X144_PREVIEW_HEIGHT:I = 0x360

.field public static final CAMCORDER_RESOLUTION_176X144_PREVIEW_WIDTH:I = 0x420

.field public static final CAMCORDER_RESOLUTION_1920X1080_PREVIEW_HEIGHT:I = 0x438

.field public static final CAMCORDER_RESOLUTION_1920X1080_PREVIEW_WIDTH:I = 0x780

.field public static final CAMCORDER_RESOLUTION_320X240_PREVIEW_HEIGHT:I = 0x438

.field public static final CAMCORDER_RESOLUTION_320X240_PREVIEW_WIDTH:I = 0x5a0

.field public static final CAMCORDER_RESOLUTION_640X480_PREVIEW_HEIGHT:I = 0x438

.field public static final CAMCORDER_RESOLUTION_640X480_PREVIEW_WIDTH:I = 0x5a0

.field public static final CAMCORDER_RESOLUTION_720X480_PREVIEW_HEIGHT:I = 0x3f0

.field public static final CAMCORDER_RESOLUTION_720X480_PREVIEW_WIDTH:I = 0x654

.field public static final CAMCORDER_RESOLUTION_960X720_PREVIEW_HEIGHT:I = 0x438

.field public static final CAMCORDER_RESOLUTION_960X720_PREVIEW_WIDTH:I = 0x5a0

.field public static final CAMCORDER_TOUCH_AF_MIN_RESOLUTION:Ljava/lang/String; = "176x144"

.field public static final CAMCORDER_USE_INNER_PROFILE:Z = true

.field public static final CAMCORDER_USE_VIDEO_BITRATE_FROM_INNER_PROFILE:Z = true

.field public static final CAMERA_ANIMATED_SCENE_MAX_VIDEO_TIME:I = 0x5

.field public static final CAMERA_ANTISHAKE:Z = true

.field public static final CAMERA_AUTO_CONTRAST:Z = false

.field public static final CAMERA_AUTO_UPLOAD:Z = false

.field public static final CAMERA_BOUNCE_ANIMATION:Z = false

.field public static final CAMERA_BURSTSHOT_100MS:I = 0x18

.field public static final CAMERA_BURSTSHOT_133MS:I = 0x19

.field public static final CAMERA_BURSTSHOT_SOUND:I = 0x18

.field public static final CAMERA_CINEPIC_MODE_RECORDING_VIDEO_FPS:I = 0xf

.field public static final CAMERA_CONTINUOUS_AF:Z = true

.field public static final CAMERA_CONTINUOUS_SHOT_175MS:I = 0x12

.field public static final CAMERA_CONTINUOUS_SHOT_200MS:I = 0x13

.field public static final CAMERA_CONTINUOUS_SHOT_320MS:I = 0x11

.field public static final CAMERA_CPU_EFFECT:Z = true

.field public static final CAMERA_DEFAULT_LAYOUT:Z = false

.field public static final CAMERA_DOWNLOAD_EFFECT:Z = false

.field public static final CAMERA_DUALCAMERA:Z = true

.field public static final CAMERA_DUAL_RECORDING_VIDEO_FPS:I = 0x18

.field public static final CAMERA_EASY_SNAP:Z = true

.field public static final CAMERA_EFFECT_COMIC:Z = true

.field public static final CAMERA_EFFECT_FADED_COLOURS:Z = true

.field public static final CAMERA_EFFECT_FISHEYE:Z = true

.field public static final CAMERA_EFFECT_FOR_REAL:Z = false

.field public static final CAMERA_EFFECT_GOTHIC_NOIR:Z = true

.field public static final CAMERA_EFFECT_GREY_SCALE:Z = true

.field public static final CAMERA_EFFECT_IMPRESSIONIST:Z = false

.field public static final CAMERA_EFFECT_NEGATIVE:Z = false

.field public static final CAMERA_EFFECT_NOIR_NOTE:Z = false

.field public static final CAMERA_EFFECT_NOSTALGIA:Z = false

.field public static final CAMERA_EFFECT_OLDPHOTO:Z = false

.field public static final CAMERA_EFFECT_PASTER_SKETCH:Z = true

.field public static final CAMERA_EFFECT_POINT_BLUE:Z = false

.field public static final CAMERA_EFFECT_POINT_GREEN:Z = false

.field public static final CAMERA_EFFECT_POINT_RED_YELLOW:Z = false

.field public static final CAMERA_EFFECT_POSTERIZE:Z = false

.field public static final CAMERA_EFFECT_RAINBOW:Z = true

.field public static final CAMERA_EFFECT_RETRO:Z = false

.field public static final CAMERA_EFFECT_SANDSTONE:Z = true

.field public static final CAMERA_EFFECT_SEPIA:Z = true

.field public static final CAMERA_EFFECT_SOLARIZE:Z = false

.field public static final CAMERA_EFFECT_STUCCHEVOLE:Z = false

.field public static final CAMERA_EFFECT_SUNSHINE:Z = false

.field public static final CAMERA_EFFECT_TURQUOISE:Z = true

.field public static final CAMERA_EFFECT_VIGNETTE_OUTFOCUS:Z = true

.field public static final CAMERA_EFFECT_VINCENT:Z = false

.field public static final CAMERA_EFFECT_VINTAGE:Z = true

.field public static final CAMERA_EFFECT_VINTAGE_COLD:Z = false

.field public static final CAMERA_EFFECT_VINTAGE_WARM:Z = false

.field public static final CAMERA_ENABLE_AUDIO_RECORDING:Z = true

.field public static final CAMERA_ENABLE_AUDIO_RECORDING_MENU:Z = false

.field public static final CAMERA_ENABLE_RECORDING_SPEED_CONTROL:Z = true

.field public static final CAMERA_ENABLE_SELF_TOGGLE_ANIMAION:Z = false

.field public static final CAMERA_ENABLE_STATUS_BAR:Z = false

.field public static final CAMERA_ENABLE_THERMISTOR_TEMPERATURE:Z = false

.field public static final CAMERA_FIRMWARE_UPDATE_CHECK_POPUP:Z = false

.field public static final CAMERA_FIRMWARE_UPDATE_ENABLE_FOR_SAMSUNG_CAMERA:Z = false

.field public static final CAMERA_FIXED_BEST_RESOLUTION:Z = false

.field public static final CAMERA_FIXED_BURST_RESOLUTION:Z = false

.field public static final CAMERA_FIXED_ERASER_RESOLUTION:Z = false

.field public static final CAMERA_FIXED_FRONT_ORIENTATION:Z = true

.field public static final CAMERA_FIXED_HDR_RESOLUTION:Z = false

.field public static final CAMERA_FIXED_QUALTY_FOR_BURST:Z = false

.field public static final CAMERA_FIX_PREVIEWFPSRANGE:Z = true

.field public static final CAMERA_FLASH:Z = true

.field public static final CAMERA_FLASH_LIMITATION_IN_LOW_TEMP:Z = true

.field public static final CAMERA_FOCUS:Z = true

.field public static final CAMERA_FOCUS_FACE_DETECTION:Z = true

.field public static final CAMERA_FOCUS_INFINITY:Z = true

.field public static final CAMERA_FRONTCAMERA:Z = true

.field public static final CAMERA_GPU_EFFECT:Z = true

.field public static final CAMERA_HALF_SHUTTER:Z = false

.field public static final CAMERA_HARD_KEY:Z = false

.field public static final CAMERA_HAS_KEYPAD:Z = false

.field public static final CAMERA_HDR_STOP_PREVIEW_CALL:Z = false

.field public static final CAMERA_ISO_1600:Z = false

.field public static final CAMERA_ISO_3200:Z = false

.field public static final CAMERA_ISO_800:Z = true

.field public static final CAMERA_ISO_SUPPORT:Z = true

.field public static final CAMERA_IS_DUAL_LCD:Z = false

.field public static final CAMERA_IS_FOLDER_TYPE:Z = false

.field public static final CAMERA_LCD_ORIENTATION:I = 0x0

.field public static final CAMERA_LCD_ORIENTATION_LANDSCAPE:I = 0x1

.field public static final CAMERA_LCD_ORIENTATION_PORTRAIT:I = 0x0

.field public static final CAMERA_LIMITATION_IN_LOW_TEMP:Z = false

.field public static final CAMERA_LIVE_BEAUTY:Z = true

.field public static final CAMERA_LOW_PERFORMANCE_BEST_FACE:Z = false

.field public static final CAMERA_LOW_PERFORMANCE_BEST_PHOTO:Z = false

.field public static final CAMERA_LOW_PERFORMANCE_BEST_PHOTO_SHOT_400MS:Z = false

.field public static final CAMERA_LOW_PERFORMANCE_BURST_SHOT_320MS:Z = false

.field public static final CAMERA_LOW_PERFORMANCE_CONTINOUS:Z = false

.field public static final CAMERA_MAX_EMAIL_ATTACH_VIDEO_SIZE:I = 0x32

.field public static final CAMERA_MAX_ISO:Ljava/lang/String; = "800"

.field public static final CAMERA_MENU_FOR_SELF_FLIP:Z = true

.field public static final CAMERA_MICRO_SD_SLOT:Z = true

.field public static final CAMERA_MMS_ATTACH_MAX_VIDEO_SIZE:I = 0x127

.field public static final CAMERA_NO_SIDEMENU_BG:Z = false

.field public static final CAMERA_NO_STOPPREVIEW_IN_TAKEPICTURE:Z = true

.field public static final CAMERA_OVERHEAT_AVAILABLE_TEMP:I = 0x1e0

.field public static final CAMERA_OVERHEAT_LIMITATION_TEMP:I = 0x1fe

.field public static final CAMERA_POSTANIMATION_BLINK:I = 0x3

.field public static final CAMERA_POSTANIMATION_NONE:I = 0x0

.field public static final CAMERA_POSTANIMATION_SHRINK:I = 0x1

.field public static final CAMERA_POSTANIMATION_SHUTTER:I = 0x2

.field public static final CAMERA_POSTANIMATION_SUPPORT:I = 0x3

.field public static final CAMERA_PREVENT_GLCONTEXT_LOSS:Z = true

.field public static final CAMERA_PREVIEW_FPS_MAX:I = 0x7530

.field public static final CAMERA_PREVIEW_FPS_MIN:I = 0x3a98

.field public static final CAMERA_QUALITY_BURST:I = 0x5a

.field public static final CAMERA_QUALITY_FINE:I = 0x5c

.field public static final CAMERA_QUALITY_NORMAL:I = 0x28

.field public static final CAMERA_QUALITY_SUPERFINE:I = 0x60

.field public static final CAMERA_QUALITY_SUPPORT:Z = false

.field public static final CAMERA_REALIGN_720X480_PREVIEW_LAYOUT_FOR_TABLET:Z = false

.field public static final CAMERA_RECORDING_BROADCASTING:Z = false

.field public static final CAMERA_RECORDING_EMAIL:Z = false

.field public static final CAMERA_RECORDING_SHARING:Z = true

.field public static final CAMERA_RECORDING_SNAPSHOT:Z = true

.field public static final CAMERA_RECORDING_VIDEO_FPS_NORMAL:I = 0x1e

.field public static final CAMERA_REVIEW_2_SEC:Z = false

.field public static final CAMERA_REVIEW_5_SEC:Z = false

.field public static final CAMERA_REVIEW_MENU:Z = true

.field public static final CAMERA_REVIEW_TRANSPARENT_GALLERY:Z = true

.field public static final CAMERA_SAVE_RICHTONE:Z = true

.field public static final CAMERA_SCENE_BACKLIGHT:Z = true

.field public static final CAMERA_SCENE_BEACHSNOW:Z = true

.field public static final CAMERA_SCENE_CANDLE_LIGHT:Z = true

.field public static final CAMERA_SCENE_DAWNDUST:Z = true

.field public static final CAMERA_SCENE_FALLCOLOR:Z = true

.field public static final CAMERA_SCENE_FIREWORK:Z = true

.field public static final CAMERA_SCENE_INDOOR:Z = true

.field public static final CAMERA_SCENE_PORTRAIT:Z = true

.field public static final CAMERA_SCENE_SPORT:Z = true

.field public static final CAMERA_SCENE_TEXT:Z = true

.field public static final CAMERA_SHOW_POST_PROGRESS_POPUP_DURING_PANORAMA:Z = true

.field public static final CAMERA_SHUTTER_SOUND_ON_OFF:Z = false

.field public static final CAMERA_SMALL_CAPTURE_BITMAP:Z = false

.field public static final CAMERA_SUPPORT_FD_CAF:Z = true

.field public static final CAMERA_SUPPORT_GPS_TAG:Z = true

.field public static final CAMERA_SUPPORT_RECORDING_PAUSE_RESUME:Z = true

.field public static final CAMERA_SUPPORT_TOUCH_FOCUSMODE:Z = false

.field public static final CAMERA_TOUCH_AF:Z = true

.field public static final CAMERA_UI_FOCUS:Z = true

.field public static final CAMERA_USE_BRIGHTNESS:Z = false

.field public static final CAMERA_USE_THE_VOLUME_KEY_AS_CAMERA:Z = true

.field public static final CAMERA_ZOOM_SUPPORT:Z = true

.field public static final CAMERA_ZOOM_SUPPORT_IN_FULLHD:Z = true

.field public static final CAMERA_ZOOM_SUPPORT_MAX_RESOLUTION:Z = true

.field public static final CAM_AVAILABLE_LOW_TEMP:I = -0x64

.field public static final CAM_FLASH_AVAILABLE_TEMP:I = -0x32

.field public static final CHATON_PHOTO_SHARE_IN_SETTING:Z = true

.field public static final CINEPIC_MODE_RESOLUTION:Ljava/lang/String; = "800x450"

.field public static final CINEPIC_MODE_RESOLUTION_PREVIEW_HEIGHT:I = 0x438

.field public static final CINEPIC_MODE_RESOLUTION_PREVIEW_WIDTH:I = 0x780

.field public static final DRAMA_SHOT_RESOLUTION:Ljava/lang/String; = "1920x1080"

.field public static final DUAL_CAMERA_RECORDING_DEFAULT_RESOLUTION:Ljava/lang/String; = "1280x720"

.field public static final DUAL_PREVIEW_FPS:I = 0x5dc0

.field public static final DUMP_CAMCORDER_PARAMETERS:Z = false

.field public static final DUMP_CAMCORDER_PROFILE:Z = false

.field public static final DUMP_PARAMETERS:Z = false

.field public static final EFFECT_BACK_CAMERA_PICTURE_DEFAULT_RESOLUTION:Ljava/lang/String; = "4096x2304"

.field public static final EFFECT_BACK_CAMERA_RESOLUTION_4096X2304:Z = true

.field public static final EFFECT_BACK_CAMERA_RESOLUTION_4096X3072:Z = true

.field public static final ENABLE_BURST_PANORAMA_SHOT:Z = true

.field public static final ENABLE_SETTING_MENU_CUT_OFF:Z = true

.field public static final ERASER_RESOLUTION:Ljava/lang/String; = "4128x2322"

.field public static final FRONT_AUTO_NIGHT_DETECTION_PREVIEW_FPS_MAX:I = 0x7530

.field public static final FRONT_AUTO_NIGHT_DETECTION_PREVIEW_FPS_MIN:I = 0xfa0

.field public static final FRONT_CAMCORDER_RESOLUTION_1280X720:Z = true

.field public static final FRONT_CAMCORDER_RESOLUTION_1280X720_PICTURE_HEIGHT:I = 0x438

.field public static final FRONT_CAMCORDER_RESOLUTION_1280X720_PICTURE_WIDTH:I = 0x780

.field public static final FRONT_CAMCORDER_RESOLUTION_1280X720_PREVIEW_HEIGHT:I = 0x438

.field public static final FRONT_CAMCORDER_RESOLUTION_1280X720_PREVIEW_WIDTH:I = 0x780

.field public static final FRONT_CAMCORDER_RESOLUTION_1440X1080:Z = true

.field public static final FRONT_CAMCORDER_RESOLUTION_1440X1080_PICTURE_HEIGHT:I = 0x438

.field public static final FRONT_CAMCORDER_RESOLUTION_1440X1080_PICTURE_WIDTH:I = 0x5a0

.field public static final FRONT_CAMCORDER_RESOLUTION_1440X1080_PREVIEW_HEIGHT:I = 0x438

.field public static final FRONT_CAMCORDER_RESOLUTION_1440X1080_PREVIEW_WIDTH:I = 0x5a0

.field public static final FRONT_CAMCORDER_RESOLUTION_176X144:Z = false

.field public static final FRONT_CAMCORDER_RESOLUTION_176X144_PREVIEW_HEIGHT:I = 0x360

.field public static final FRONT_CAMCORDER_RESOLUTION_176X144_PREVIEW_WIDTH:I = 0x420

.field public static final FRONT_CAMCORDER_RESOLUTION_1920X1080:Z = true

.field public static final FRONT_CAMCORDER_RESOLUTION_1920X1080_PICTURE_HEIGHT:I = 0x438

.field public static final FRONT_CAMCORDER_RESOLUTION_1920X1080_PICTURE_WIDTH:I = 0x780

.field public static final FRONT_CAMCORDER_RESOLUTION_1920X1080_PREVIEW_HEIGHT:I = 0x438

.field public static final FRONT_CAMCORDER_RESOLUTION_1920X1080_PREVIEW_WIDTH:I = 0x780

.field public static final FRONT_CAMCORDER_RESOLUTION_320X240:Z = true

.field public static final FRONT_CAMCORDER_RESOLUTION_320X240_PICTURE_HEIGHT:I = 0x438

.field public static final FRONT_CAMCORDER_RESOLUTION_320X240_PICTURE_WIDTH:I = 0x5a0

.field public static final FRONT_CAMCORDER_RESOLUTION_320X240_PREVIEW_HEIGHT:I = 0x438

.field public static final FRONT_CAMCORDER_RESOLUTION_320X240_PREVIEW_WIDTH:I = 0x5a0

.field public static final FRONT_CAMCORDER_RESOLUTION_640X480:Z = false

.field public static final FRONT_CAMCORDER_RESOLUTION_640X480_PICTURE_HEIGHT:I = 0x438

.field public static final FRONT_CAMCORDER_RESOLUTION_640X480_PICTURE_WIDTH:I = 0x5a0

.field public static final FRONT_CAMCORDER_RESOLUTION_640X480_PREVIEW_HEIGHT:I = 0x438

.field public static final FRONT_CAMCORDER_RESOLUTION_640X480_PREVIEW_WIDTH:I = 0x5a0

.field public static final FRONT_CAMCORDER_RESOLUTION_720X480:Z = false

.field public static final FRONT_CAMCORDER_RESOLUTION_720X480_PICTURE_HEIGHT:I = 0x438

.field public static final FRONT_CAMCORDER_RESOLUTION_720X480_PICTURE_WIDTH:I = 0x654

.field public static final FRONT_CAMCORDER_RESOLUTION_720X480_PREVIEW_HEIGHT:I = 0x3f0

.field public static final FRONT_CAMCORDER_RESOLUTION_720X480_PREVIEW_WIDTH:I = 0x654

.field public static final FRONT_CAMCORDER_RESOLUTION_960X720:Z = false

.field public static final FRONT_CAMCORDER_RESOLUTION_960X720_PICTURE_HEIGHT:I = 0x438

.field public static final FRONT_CAMCORDER_RESOLUTION_960X720_PICTURE_WIDTH:I = 0x5a0

.field public static final FRONT_CAMERA_FACE_DETECTION:Z = false

.field public static final FRONT_CAMERA_PICTURE_DEFAULT_RESOLUTION:Ljava/lang/String; = "1920x1080"

.field public static final FRONT_CAMERA_PICTURE_MAX_RESOLUTION:Ljava/lang/String; = "1920x1080"

.field public static final FRONT_CAMERA_RECORDING_DEFAULT_RESOLUTION:Ljava/lang/String; = "1920x1080"

.field public static final FRONT_CAMERA_RECORDING_MAX_RESOLUTION:Ljava/lang/String; = "1920x1080"

.field public static final FRONT_CAMERA_RESOLUTION_1248X672:Z = false

.field public static final FRONT_CAMERA_RESOLUTION_1280X720:Z = false

.field public static final FRONT_CAMERA_RESOLUTION_1280X960:Z = false

.field public static final FRONT_CAMERA_RESOLUTION_1392X1392:Z = false

.field public static final FRONT_CAMERA_RESOLUTION_1440X1080:Z = true

.field public static final FRONT_CAMERA_RESOLUTION_1536X864:Z = false

.field public static final FRONT_CAMERA_RESOLUTION_1600X1200:Z = false

.field public static final FRONT_CAMERA_RESOLUTION_1600X960:Z = false

.field public static final FRONT_CAMERA_RESOLUTION_1632X880:Z = false

.field public static final FRONT_CAMERA_RESOLUTION_176X144:Z = false

.field public static final FRONT_CAMERA_RESOLUTION_1920X1080:Z = true

.field public static final FRONT_CAMERA_RESOLUTION_2048X1104:Z = false

.field public static final FRONT_CAMERA_RESOLUTION_2048X1152:Z = false

.field public static final FRONT_CAMERA_RESOLUTION_2048X1232:Z = false

.field public static final FRONT_CAMERA_RESOLUTION_2048X1536:Z = false

.field public static final FRONT_CAMERA_RESOLUTION_2560X1440:Z = false

.field public static final FRONT_CAMERA_RESOLUTION_2560X1536:Z = false

.field public static final FRONT_CAMERA_RESOLUTION_2560X1920:Z = false

.field public static final FRONT_CAMERA_RESOLUTION_2592X1944:Z = false

.field public static final FRONT_CAMERA_RESOLUTION_3072X1856:Z = false

.field public static final FRONT_CAMERA_RESOLUTION_3072X2304:Z = false

.field public static final FRONT_CAMERA_RESOLUTION_320X240:Z = false

.field public static final FRONT_CAMERA_RESOLUTION_3264X1836:Z = false

.field public static final FRONT_CAMERA_RESOLUTION_3264X1968:Z = false

.field public static final FRONT_CAMERA_RESOLUTION_3264X2448:Z = false

.field public static final FRONT_CAMERA_RESOLUTION_400X240:Z = false

.field public static final FRONT_CAMERA_RESOLUTION_4096X3072:Z = false

.field public static final FRONT_CAMERA_RESOLUTION_4128X2322:Z = false

.field public static final FRONT_CAMERA_RESOLUTION_4128X3096:Z = false

.field public static final FRONT_CAMERA_RESOLUTION_640X480:Z = true

.field public static final FRONT_CAMERA_RESOLUTION_720X480:Z = false

.field public static final FRONT_CAMERA_RESOLUTION_800X480:Z = false

.field public static final FRONT_CAMERA_SHOOTINGMODE_3DPANORAMA:Z = false

.field public static final FRONT_CAMERA_SHOOTINGMODE_ACTION_SHOT:Z = false

.field public static final FRONT_CAMERA_SHOOTINGMODE_ADD_ME:Z = false

.field public static final FRONT_CAMERA_SHOOTINGMODE_AUTO:Z = true

.field public static final FRONT_CAMERA_SHOOTINGMODE_AUTO_PORTRAIT:Z = false

.field public static final FRONT_CAMERA_SHOOTINGMODE_BEAUTY_SHOT:Z = true

.field public static final FRONT_CAMERA_SHOOTINGMODE_BEST_FACE_SHOT:Z = true

.field public static final FRONT_CAMERA_SHOOTINGMODE_BEST_PHOTO_SHOT:Z = true

.field public static final FRONT_CAMERA_SHOOTINGMODE_BURST_SHOT:Z = false

.field public static final FRONT_CAMERA_SHOOTINGMODE_CARTOON:Z = false

.field public static final FRONT_CAMERA_SHOOTINGMODE_CINEPIC:Z = false

.field public static final FRONT_CAMERA_SHOOTINGMODE_CONTINUOUS_SHOT:Z = false

.field public static final FRONT_CAMERA_SHOOTINGMODE_DRAMA:Z = false

.field public static final FRONT_CAMERA_SHOOTINGMODE_ERASER_SHOT:Z = false

.field public static final FRONT_CAMERA_SHOOTINGMODE_FRAME_SHOT:Z = false

.field public static final FRONT_CAMERA_SHOOTINGMODE_GOLF:Z = false

.field public static final FRONT_CAMERA_SHOOTINGMODE_MOSAIC_SHOT:Z = false

.field public static final FRONT_CAMERA_SHOOTINGMODE_NIGHT_SHOT:Z = true

.field public static final FRONT_CAMERA_SHOOTINGMODE_PANORAMA_SHOT:Z = false

.field public static final FRONT_CAMERA_SHOOTINGMODE_RICH_TONE:Z = false

.field public static final FRONT_CAMERA_SHOOTINGMODE_SMILE_SHOT:Z = false

.field public static final FRONT_CAMERA_SHOOTINGMODE_SNS:Z = false

.field public static final FRONT_CAMERA_SHOOTINGMODE_SOUNDSHOT:Z = true

.field public static final FRONT_CAMERA_SHOOTINGMODE_STOP_MOTION:Z = false

.field public static final FRONT_CAMERA_SHOOTINGMODE_VINTAGE_SHOT:Z = false

.field public static final GOLF_PREVIEW_FPS:I = 0x1d4c0

.field public static final GOLF_SHOT_RESOLUTION:Ljava/lang/String; = "800x450"

.field public static final IMPROVEMENT_SCREEN_QUALITY:Z = false

.field public static final INTERNAL_SD:Z = true

.field public static final LOW_BATTERY_THRESHOLD_VALUE:I = 0x5

.field public static final LOW_TEMP_FLASH_THRESHOLD_VALUE:I = 0x64

.field public static final MID_SIDEMENU:Z = false

.field public static final MODE_MENU_5_COLUMNS:Z = false

.field public static final NORMAL_RATIO:D = 1.3333333333333333

.field public static final PACKAGE_NAME_GALLERY3D:Ljava/lang/String; = "com.sec.android.gallery3d"

.field public static final PACKAGE_NAME_VIDEOPLAYER:Ljava/lang/String; = "com.sec.android.app.videoplayer"

.field public static final PANORAMA_RESOLUTION:Ljava/lang/String; = "640x480"

.field public static final REMOTE_VIEWFINDER_IN_SETTING:Z = true

.field public static final SEND_ORIENTATION_INFO_TO_HAL:Z = true

.field public static final SET_JPEG_THUMBNAILSIZE_IN_SET_PICTURE_SIZE:Z = false

.field public static final SLEEP_BEFORE_FRONT_STARTPREVIEW_MS:I = 0xc8

.field public static final SLEEP_BEFORE_REAR_STARTPREVIEW_MS:I = 0x64

.field public static final SLEEP_BEFORE_STARTPREVIEW:Z = false

.field public static final SLOWMOTION_CAMCORDER_RESOLUTION:Ljava/lang/String; = "1280x720"

.field public static final SLOWMOTION_CAMCORDER_RESOLUTION_PREVIEW_HEIGHT:I = 0x2d0

.field public static final SLOWMOTION_CAMCORDER_RESOLUTION_PREVIEW_WIDTH:I = 0x500

.field public static final SUPPORT_AUTO_NIGHT_DETECTION:Z = true

.field public static final SUPPORT_AUTO_RECORD_SIZE:Z = true

.field public static final SUPPORT_CAMERA_FIRMWARE:Z = true

.field public static final SUPPORT_CONTEXTUAL_FILENAME:Z = true

.field public static final SUPPORT_OUTDOOR_VISIBILITY:Z = false

.field public static final SUPPORT_SCROLLBAR_AUTO_HIDE:Z = true

.field public static final SUPPORT_VOICECOMMAND:Z = true

.field public static final TABLET_SIDEMENU:Z = false

.field public static final TEMP_BLOCK_EFFECT:Z = false

.field public static final WIDE_SCREEN_RATIO:D = 1.7777777777777777


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
