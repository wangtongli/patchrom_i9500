.class public Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;
.super Lcom/sec/android/app/camera/MenuBase;
.source "TwGLEasyCameraRecordingMenu.java"


# static fields
#the value of this static final field might be set in the static constructor
.field private static final BASEMENU_LEFT_MARGIN:I = 0x0

#the value of this static final field might be set in the static constructor
.field private static final BASEMENU_TOP_MARGIN:I = 0x0

#the value of this static final field might be set in the static constructor
.field private static final FULL_SCREEN_HEIGHT:I = 0x0

#the value of this static final field might be set in the static constructor
.field private static final FULL_SCREEN_WIDTH:I = 0x0

#the value of this static final field might be set in the static constructor
.field private static final INDICATOR_BOTTON_MARGIN:I = 0x0

#the value of this static final field might be set in the static constructor
.field private static final INDICATOR_GROUP_HEIGHT:I = 0x0

#the value of this static final field might be set in the static constructor
.field private static final INDICATOR_GROUP_POS_X:I = 0x0

#the value of this static final field might be set in the static constructor
.field private static final INDICATOR_GROUP_POS_Y:I = 0x0

#the value of this static final field might be set in the static constructor
.field private static final INDICATOR_GROUP_WIDTH:I = 0x0

#the value of this static final field might be set in the static constructor
.field private static final INDICATOR_REC_ICON_LEFT_MARGIN:I = 0x0

#the value of this static final field might be set in the static constructor
.field private static final INDICATOR_REC_ICON_SIDE_MARGIN:I = 0x0

#the value of this static final field might be set in the static constructor
.field private static final INDICATOR_REC_ICON_TOP_MARGIN:I = 0x0

#the value of this static final field might be set in the static constructor
.field private static final INDICATOR_REC_ICON_WIDTH:I = 0x0

#the value of this static final field might be set in the static constructor
.field private static final INDICATOR_REC_TEXT_MARGIN:I = 0x0

#the value of this static final field might be set in the static constructor
.field private static final INDICATOR_REC_TEXT_WIDTH:I = 0x0

#the value of this static final field might be set in the static constructor
.field private static final INDICATOR_REC_TIME_WIDTH:I = 0x0

#the value of this static final field might be set in the static constructor
.field private static final INDICATOR_REC_WIDTH:I = 0x0

#the value of this static final field might be set in the static constructor
.field private static final INDICATOR_SIDE_MARGIN:I = 0x0

.field public static final MAX_EMAIL_SIZE:J = 0x3200000L

.field private static final MAX_RECORDING_TIME:I = 0x1517f

.field private static final ONE_SECOND:I = 0x1

#the value of this static final field might be set in the static constructor
.field private static final PROGRESSBAR_HEIGHT:I = 0x0

#the value of this static final field might be set in the static constructor
.field private static final PROGRESSBAR_POS_X:I = 0x0

#the value of this static final field might be set in the static constructor
.field private static final PROGRESSBAR_SIDE_MARGIN:I = 0x0

#the value of this static final field might be set in the static constructor
.field private static final PROGRESSBAR_WIDTH:I = 0x0

#the value of this static final field might be set in the static constructor
.field private static final PROGRESS_GROUP_BOTTOM_MARGIN:I = 0x0

#the value of this static final field might be set in the static constructor
.field private static final PROGRESS_GROUP_BOTTOM_MARGIN_180:I = 0x0

#the value of this static final field might be set in the static constructor
.field private static final PROGRESS_GROUP_HEIGHT:I = 0x0

#the value of this static final field might be set in the static constructor
.field private static final PROGRESS_GROUP_SIDE_MARGIN:I = 0x0

#the value of this static final field might be set in the static constructor
.field private static final PROGRESS_GROUP_WIDTH:I = 0x0

#the value of this static final field might be set in the static constructor
.field private static final PROGRESS_GROUP_X:I = 0x0

#the value of this static final field might be set in the static constructor
.field private static final PROGRESS_GROUP_Y:I = 0x0

#the value of this static final field might be set in the static constructor
.field private static final PROGRESS_MAX_SIZE_POS_X:I = 0x0

#the value of this static final field might be set in the static constructor
.field private static final PROGRESS_SIZE_TEXT_WIDTH:I = 0x0

#the value of this static final field might be set in the static constructor
.field private static final RECORDING_BUTTON_POS_Y:I = 0x0

#the value of this static final field might be set in the static constructor
.field private static final RECORDING_BUTTON_STOP_ICON_LEFT_MARGIN:I = 0x0

#the value of this static final field might be set in the static constructor
.field private static final RECORDING_BUTTON_STOP_ICON_TOP_MARGIN:I = 0x0

.field public static final RECORDING_MENU_CANCEL:I = 0x4

.field public static final RECORDING_MENU_PAUSE:I = 0x1

.field public static final RECORDING_MENU_RESUME:I = 0x3

.field public static final RECORDING_MENU_STOP:I = 0x2

.field public static final RECORDING_MENU_TAKEPICTURE:I = 0x5

.field public static final RECORDING_TIMER_STEP_MSG:I = 0x1

#the value of this static final field might be set in the static constructor
.field private static final REC_SIZE_TEXT_SIZE:I = 0x0

#the value of this static final field might be set in the static constructor
.field private static final REC_TIME_TEXT_SIZE:I = 0x0

#the value of this static final field might be set in the static constructor
.field private static final REMAIN_TIME_GROUP_HEIGHT:I = 0x0

#the value of this static final field might be set in the static constructor
.field private static final REMAIN_TIME_GROUP_POS_X:I = 0x0

#the value of this static final field might be set in the static constructor
.field private static final REMAIN_TIME_GROUP_POS_Y:I = 0x0

#the value of this static final field might be set in the static constructor
.field private static final REMAIN_TIME_GROUP_WIDTH:I = 0x0

#the value of this static final field might be set in the static constructor
.field private static final RIGHT_SIDE_MENU_POS_X:I = 0x0

#the value of this static final field might be set in the static constructor
.field private static final RIGHT_SIDE_MENU_WIDTH:I = 0x0

.field private static final SECONDS_IN_AN_HOUR:I = 0xe10

.field private static final SECONDS_IN_A_DAY:I = 0x15180

.field private static final SECONDS_IN_A_MINUTE:I = 0x3c

#the value of this static final field might be set in the static constructor
.field private static final SIDE_MENU_WIDTH:I = 0x0

.field protected static final TAG:Ljava/lang/String; = "TwGLEasyCameraRecordingMenu"


# instance fields
.field private isSharingMode:Z

.field private mCurRecSize:Lcom/sec/android/glview/TwGLText;

.field private mEmailMaxRecSize:Lcom/sec/android/glview/TwGLText;

.field private mHMS:Ljava/lang/String;

.field private mIndicatorRecGroup:Lcom/sec/android/glview/TwGLViewGroup;

.field private mIndicatorRecIcon:Lcom/sec/android/glview/TwGLImage;

.field private mIndicatorRecIconText:Lcom/sec/android/glview/TwGLText;

.field private mIndicatorRecTime:Lcom/sec/android/glview/TwGLText;

.field private mMenuGroup:Lcom/sec/android/glview/TwGLViewGroup;

.field private mProgress:I

.field private mProgressBar:Lcom/sec/android/glview/TwGLProgressBar;

.field private mProgressBarGroup:Lcom/sec/android/glview/TwGLViewGroup;

.field private mRecRemainTimeGroup:Lcom/sec/android/glview/TwGLViewGroup;

.field private mRecRemainTimeText:Lcom/sec/android/glview/TwGLText;

.field private mRecordingLimitTime:I

.field private mRecordingProgressHandler:Landroid/os/Handler;

.field private mRecordingSideMenu:Lcom/sec/android/glview/TwGLViewGroup;

.field private mRecordingState:I

.field private mRecordingTime:I

.field private mSharingMaxRecSize:Lcom/sec/android/glview/TwGLText;

.field private mStopButton:Lcom/sec/android/glview/TwGLButton;

.field private mStopButtonImage:Lcom/sec/android/glview/TwGLImage;

.field private mStopping:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 29
    const v0, 0x7f080314

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->FULL_SCREEN_WIDTH:I

    .line 30
    const v0, 0x7f080315

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->FULL_SCREEN_HEIGHT:I

    .line 31
    const v0, 0x7f080005

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->SIDE_MENU_WIDTH:I

    .line 32
    const v0, 0x7f090043

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getInteger(I)I

    move-result v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->REC_SIZE_TEXT_SIZE:I

    .line 33
    const v0, 0x7f090044

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getInteger(I)I

    move-result v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->REC_TIME_TEXT_SIZE:I

    .line 35
    const v0, 0x7f0801c9

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    const v1, 0x7f0801c5

    invoke-static {v1}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    add-int/2addr v0, v1

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->RIGHT_SIDE_MENU_WIDTH:I

    .line 37
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->FULL_SCREEN_WIDTH:I

    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->RIGHT_SIDE_MENU_WIDTH:I

    sub-int/2addr v0, v1

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->RIGHT_SIDE_MENU_POS_X:I

    .line 40
    const v0, 0x7f0801c4

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->RECORDING_BUTTON_POS_Y:I

    .line 41
    const v0, 0x7f0801c7

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->RECORDING_BUTTON_STOP_ICON_TOP_MARGIN:I

    .line 42
    const v0, 0x7f0801c8

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->RECORDING_BUTTON_STOP_ICON_LEFT_MARGIN:I

    .line 55
    const v0, 0x7f0801b9

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->BASEMENU_LEFT_MARGIN:I

    .line 56
    const v0, 0x7f0801b8

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->BASEMENU_TOP_MARGIN:I

    .line 58
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->BASEMENU_LEFT_MARGIN:I

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->INDICATOR_GROUP_POS_X:I

    .line 59
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->BASEMENU_TOP_MARGIN:I

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->INDICATOR_GROUP_POS_Y:I

    .line 61
    const v0, 0x7f0801b6

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->INDICATOR_GROUP_WIDTH:I

    .line 62
    const v0, 0x7f0801b7

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->INDICATOR_GROUP_HEIGHT:I

    .line 63
    const v0, 0x7f0801bd

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->INDICATOR_REC_ICON_TOP_MARGIN:I

    .line 64
    const v0, 0x7f0801bc

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->INDICATOR_REC_ICON_LEFT_MARGIN:I

    .line 65
    const v0, 0x7f0801be

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->INDICATOR_REC_ICON_SIDE_MARGIN:I

    .line 66
    const v0, 0x7f0801bb

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->INDICATOR_REC_WIDTH:I

    .line 67
    const v0, 0x7f0801bf

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->INDICATOR_REC_ICON_WIDTH:I

    .line 68
    const v0, 0x7f0801c0

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->INDICATOR_REC_TEXT_MARGIN:I

    .line 69
    const v0, 0x7f0801c1

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->INDICATOR_REC_TEXT_WIDTH:I

    .line 70
    const v0, 0x7f0801c2

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->INDICATOR_REC_TIME_WIDTH:I

    .line 71
    const v0, 0x7f0801c3

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->INDICATOR_SIDE_MARGIN:I

    .line 72
    const v0, 0x7f0801ba

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->INDICATOR_BOTTON_MARGIN:I

    .line 75
    const v0, 0x7f0800e7

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->PROGRESSBAR_WIDTH:I

    .line 76
    const v0, 0x7f0800e8

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->PROGRESSBAR_HEIGHT:I

    .line 77
    const v0, 0x7f0800e9

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->PROGRESSBAR_SIDE_MARGIN:I

    .line 78
    const v0, 0x7f0800ea

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->PROGRESS_SIZE_TEXT_WIDTH:I

    .line 79
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->PROGRESS_SIZE_TEXT_WIDTH:I

    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->PROGRESSBAR_WIDTH:I

    add-int/2addr v0, v1

    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->PROGRESSBAR_SIDE_MARGIN:I

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->PROGRESS_MAX_SIZE_POS_X:I

    .line 80
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->PROGRESS_SIZE_TEXT_WIDTH:I

    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->PROGRESSBAR_SIDE_MARGIN:I

    add-int/2addr v0, v1

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->PROGRESSBAR_POS_X:I

    .line 81
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->PROGRESSBAR_WIDTH:I

    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->PROGRESS_SIZE_TEXT_WIDTH:I

    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->PROGRESSBAR_SIDE_MARGIN:I

    add-int/2addr v1, v2

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->PROGRESS_GROUP_WIDTH:I

    .line 82
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->INDICATOR_GROUP_HEIGHT:I

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->PROGRESS_GROUP_HEIGHT:I

    .line 83
    const v0, 0x7f0800ec

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->PROGRESS_GROUP_SIDE_MARGIN:I

    .line 84
    const v0, 0x7f0800ed

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->PROGRESS_GROUP_BOTTOM_MARGIN:I

    .line 85
    const v0, 0x7f0800ee

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->PROGRESS_GROUP_BOTTOM_MARGIN_180:I

    .line 86
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->FULL_SCREEN_WIDTH:I

    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->PROGRESS_GROUP_WIDTH:I

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->PROGRESS_GROUP_X:I

    .line 87
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->FULL_SCREEN_HEIGHT:I

    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->PROGRESS_GROUP_HEIGHT:I

    sub-int/2addr v0, v1

    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->PROGRESS_GROUP_BOTTOM_MARGIN:I

    sub-int/2addr v0, v1

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->PROGRESS_GROUP_Y:I

    .line 96
    const v0, 0x7f0800b6

    invoke-static {v0}, Lcom/sec/android/glview/TwGLContext;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->REMAIN_TIME_GROUP_WIDTH:I

    .line 97
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->INDICATOR_GROUP_HEIGHT:I

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->REMAIN_TIME_GROUP_HEIGHT:I

    .line 98
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->FULL_SCREEN_WIDTH:I

    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->SIDE_MENU_WIDTH:I

    sub-int/2addr v0, v1

    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->INDICATOR_SIDE_MARGIN:I

    sub-int/2addr v0, v1

    sget v1, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->REMAIN_TIME_GROUP_WIDTH:I

    sub-int/2addr v0, v1

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->REMAIN_TIME_GROUP_POS_X:I

    .line 99
    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->BASEMENU_TOP_MARGIN:I

    sput v0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->REMAIN_TIME_GROUP_POS_Y:I

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/camera/Camera;ILcom/sec/android/glview/TwGLViewGroup;Lcom/sec/android/app/camera/MenuResourceDepot;I)V
    .locals 7
    .parameter "activityContext"
    .parameter "viewId"
    .parameter "glParentView"
    .parameter "menuResourceDepot"
    .parameter "zOrder"

    .prologue
    const/4 v6, 0x0

    .line 162
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/camera/MenuBase;-><init>(Lcom/sec/android/app/camera/Camera;ILcom/sec/android/glview/TwGLViewGroup;Lcom/sec/android/app/camera/MenuResourceDepot;IZ)V

    .line 117
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mRecordingState:I

    .line 144
    const-string v0, "00:00"

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mHMS:Ljava/lang/String;

    .line 146
    iput v6, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mRecordingTime:I

    .line 147
    iput v6, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mRecordingLimitTime:I

    .line 148
    iput v6, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mProgress:I

    .line 149
    iput-boolean v6, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->isSharingMode:Z

    .line 150
    iput-boolean v6, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mStopping:Z

    .line 152
    new-instance v0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu$1;-><init>(Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;)V

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mRecordingProgressHandler:Landroid/os/Handler;

    .line 164
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/MenuBase;->setTouchHandled(Z)V

    .line 165
    new-instance v0, Lcom/sec/android/glview/TwGLViewGroup;

    iget-object v1, p0, Lcom/sec/android/app/camera/MenuBase;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/Camera;->getGLContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/glview/TwGLViewGroup;-><init>(Lcom/sec/android/glview/TwGLContext;)V

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mMenuGroup:Lcom/sec/android/glview/TwGLViewGroup;

    .line 166
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mMenuGroup:Lcom/sec/android/glview/TwGLViewGroup;

    invoke-virtual {v0, p2}, Lcom/sec/android/glview/TwGLView;->setTag(I)V

    .line 167
    invoke-direct {p0}, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->init()V

    .line 168
    return-void
.end method

.method private hmsConvert(I)Ljava/lang/String;
    .locals 6
    .parameter "seconds"

    .prologue
    const/16 v5, 0xa

    const/4 v4, 0x0

    .line 375
    const v3, 0x1517f

    if-le p1, v3, :cond_0

    .line 376
    const p1, 0x1517f

    .line 379
    :cond_0
    div-int/lit8 v0, p1, 0x3c

    .line 380
    .local v0, min:I
    rem-int/lit8 v1, p1, 0x3c

    .line 382
    .local v1, sec:I
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 384
    .local v2, str:Ljava/lang/StringBuilder;
    if-ge v0, v5, :cond_1

    .line 385
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 386
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 391
    :goto_0
    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 393
    if-ge v1, v5, :cond_2

    .line 394
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 395
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 400
    :goto_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 388
    :cond_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 397
    :cond_2
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method private init()V
    .locals 15

    .prologue
    const/4 v14, 0x4

    const/4 v13, 0x2

    const/4 v12, 0x0

    const/16 v11, 0xff

    const/4 v9, 0x1

    .line 171
    iget-object v0, p0, Lcom/sec/android/app/camera/MenuBase;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getGLContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v1

    .line 172
    .local v1, glContext:Lcom/sec/android/glview/TwGLContext;
    new-instance v0, Lcom/sec/android/glview/TwGLViewGroup;

    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->INDICATOR_GROUP_POS_X:I

    int-to-float v2, v2

    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->INDICATOR_GROUP_POS_Y:I

    int-to-float v3, v3

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->INDICATOR_GROUP_WIDTH:I

    int-to-float v4, v4

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->INDICATOR_GROUP_HEIGHT:I

    int-to-float v5, v5

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/glview/TwGLViewGroup;-><init>(Lcom/sec/android/glview/TwGLContext;FFFF)V

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mIndicatorRecGroup:Lcom/sec/android/glview/TwGLViewGroup;

    .line 173
    new-instance v0, Lcom/sec/android/glview/TwGLImage;

    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->INDICATOR_REC_ICON_LEFT_MARGIN:I

    int-to-float v2, v2

    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->INDICATOR_REC_ICON_TOP_MARGIN:I

    int-to-float v3, v3

    const v4, 0x7f020006

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/glview/TwGLImage;-><init>(Lcom/sec/android/glview/TwGLContext;FFI)V

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mIndicatorRecIcon:Lcom/sec/android/glview/TwGLImage;

    .line 174
    new-instance v0, Lcom/sec/android/glview/TwGLText;

    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->INDICATOR_REC_ICON_LEFT_MARGIN:I

    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->INDICATOR_REC_ICON_WIDTH:I

    add-int/2addr v2, v3

    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->INDICATOR_REC_ICON_SIDE_MARGIN:I

    add-int/2addr v2, v3

    int-to-float v2, v2

    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->INDICATOR_REC_TEXT_WIDTH:I

    int-to-float v4, v3

    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->INDICATOR_GROUP_HEIGHT:I

    int-to-float v5, v3

    const-string v6, "REC"

    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->REC_TIME_TEXT_SIZE:I

    int-to-float v7, v3

    const/16 v3, 0xdc

    const/16 v8, 0x28

    const/16 v10, 0x28

    invoke-static {v11, v3, v8, v10}, Landroid/graphics/Color;->argb(IIII)I

    move-result v8

    move v3, v12

    invoke-direct/range {v0 .. v9}, Lcom/sec/android/glview/TwGLText;-><init>(Lcom/sec/android/glview/TwGLContext;FFFFLjava/lang/String;FIZ)V

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mIndicatorRecIconText:Lcom/sec/android/glview/TwGLText;

    .line 175
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mIndicatorRecIconText:Lcom/sec/android/glview/TwGLText;

    invoke-virtual {v0, v9, v13}, Lcom/sec/android/glview/TwGLText;->setAlign(II)V

    .line 176
    new-instance v0, Lcom/sec/android/glview/TwGLText;

    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->INDICATOR_REC_WIDTH:I

    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->INDICATOR_REC_TEXT_MARGIN:I

    add-int/2addr v2, v3

    int-to-float v2, v2

    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->INDICATOR_REC_TIME_WIDTH:I

    int-to-float v4, v3

    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->INDICATOR_GROUP_HEIGHT:I

    int-to-float v5, v3

    iget-object v6, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mHMS:Ljava/lang/String;

    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->REC_TIME_TEXT_SIZE:I

    int-to-float v7, v3

    move v3, v12

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/glview/TwGLText;-><init>(Lcom/sec/android/glview/TwGLContext;FFFFLjava/lang/String;F)V

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mIndicatorRecTime:Lcom/sec/android/glview/TwGLText;

    .line 178
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mIndicatorRecTime:Lcom/sec/android/glview/TwGLText;

    invoke-virtual {v0, v9, v13}, Lcom/sec/android/glview/TwGLText;->setAlign(II)V

    .line 180
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mIndicatorRecGroup:Lcom/sec/android/glview/TwGLViewGroup;

    invoke-virtual {v0, v9}, Lcom/sec/android/glview/TwGLView;->setRotatable(Z)V

    .line 181
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mIndicatorRecGroup:Lcom/sec/android/glview/TwGLViewGroup;

    invoke-static {}, Lcom/sec/android/glview/TwGLUtil;->getAlphaOnAnimation()Landroid/view/animation/Animation;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/android/glview/TwGLView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 182
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mIndicatorRecGroup:Lcom/sec/android/glview/TwGLViewGroup;

    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->INDICATOR_GROUP_POS_X:I

    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->INDICATOR_GROUP_HEIGHT:I

    add-int/2addr v2, v3

    int-to-float v2, v2

    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->INDICATOR_GROUP_POS_Y:I

    int-to-float v3, v3

    invoke-virtual {v0, v9, v2, v3}, Lcom/sec/android/glview/TwGLView;->setLeftTop(IFF)V

    .line 183
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mIndicatorRecGroup:Lcom/sec/android/glview/TwGLViewGroup;

    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->INDICATOR_GROUP_POS_X:I

    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->INDICATOR_GROUP_WIDTH:I

    add-int/2addr v2, v3

    int-to-float v2, v2

    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->INDICATOR_GROUP_POS_Y:I

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->INDICATOR_GROUP_HEIGHT:I

    add-int/2addr v3, v4

    int-to-float v3, v3

    invoke-virtual {v0, v13, v2, v3}, Lcom/sec/android/glview/TwGLView;->setLeftTop(IFF)V

    .line 184
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mIndicatorRecGroup:Lcom/sec/android/glview/TwGLViewGroup;

    const/4 v2, 0x3

    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->INDICATOR_GROUP_POS_X:I

    int-to-float v3, v3

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->INDICATOR_GROUP_POS_Y:I

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->INDICATOR_GROUP_WIDTH:I

    add-int/2addr v4, v5

    int-to-float v4, v4

    invoke-virtual {v0, v2, v3, v4}, Lcom/sec/android/glview/TwGLView;->setLeftTop(IFF)V

    .line 186
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mIndicatorRecIcon:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v0, v14}, Lcom/sec/android/glview/TwGLView;->setVisibility(I)V

    .line 187
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mIndicatorRecIconText:Lcom/sec/android/glview/TwGLText;

    invoke-virtual {v0, v14}, Lcom/sec/android/glview/TwGLView;->setVisibility(I)V

    .line 188
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mIndicatorRecTime:Lcom/sec/android/glview/TwGLText;

    invoke-virtual {v0, v14}, Lcom/sec/android/glview/TwGLView;->setVisibility(I)V

    .line 190
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mIndicatorRecGroup:Lcom/sec/android/glview/TwGLViewGroup;

    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mIndicatorRecIcon:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v0, v2}, Lcom/sec/android/glview/TwGLViewGroup;->addView(Lcom/sec/android/glview/TwGLView;)V

    .line 191
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mIndicatorRecGroup:Lcom/sec/android/glview/TwGLViewGroup;

    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mIndicatorRecIconText:Lcom/sec/android/glview/TwGLText;

    invoke-virtual {v0, v2}, Lcom/sec/android/glview/TwGLViewGroup;->addView(Lcom/sec/android/glview/TwGLView;)V

    .line 192
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mIndicatorRecGroup:Lcom/sec/android/glview/TwGLViewGroup;

    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mIndicatorRecTime:Lcom/sec/android/glview/TwGLText;

    invoke-virtual {v0, v2}, Lcom/sec/android/glview/TwGLViewGroup;->addView(Lcom/sec/android/glview/TwGLView;)V

    .line 193
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mMenuGroup:Lcom/sec/android/glview/TwGLViewGroup;

    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mIndicatorRecGroup:Lcom/sec/android/glview/TwGLViewGroup;

    invoke-virtual {v0, v2}, Lcom/sec/android/glview/TwGLViewGroup;->addView(Lcom/sec/android/glview/TwGLView;)V

    .line 207
    new-instance v0, Lcom/sec/android/glview/TwGLViewGroup;

    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->PROGRESS_GROUP_X:I

    int-to-float v2, v2

    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->PROGRESS_GROUP_Y:I

    int-to-float v3, v3

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->PROGRESS_GROUP_WIDTH:I

    int-to-float v4, v4

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->PROGRESS_GROUP_HEIGHT:I

    int-to-float v5, v5

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/glview/TwGLViewGroup;-><init>(Lcom/sec/android/glview/TwGLContext;FFFF)V

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mProgressBarGroup:Lcom/sec/android/glview/TwGLViewGroup;

    .line 208
    new-instance v0, Lcom/sec/android/glview/TwGLText;

    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->PROGRESS_SIZE_TEXT_WIDTH:I

    int-to-float v4, v2

    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->PROGRESS_GROUP_HEIGHT:I

    int-to-float v5, v2

    const-string v6, "0K"

    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->REC_SIZE_TEXT_SIZE:I

    int-to-float v7, v2

    invoke-static {v11, v11, v11, v11}, Landroid/graphics/Color;->argb(IIII)I

    move-result v8

    move v2, v12

    move v3, v12

    invoke-direct/range {v0 .. v9}, Lcom/sec/android/glview/TwGLText;-><init>(Lcom/sec/android/glview/TwGLContext;FFFFLjava/lang/String;FIZ)V

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mCurRecSize:Lcom/sec/android/glview/TwGLText;

    .line 209
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mCurRecSize:Lcom/sec/android/glview/TwGLText;

    const/4 v2, 0x3

    invoke-virtual {v0, v2, v13}, Lcom/sec/android/glview/TwGLText;->setAlign(II)V

    .line 211
    new-instance v0, Lcom/sec/android/glview/TwGLProgressBar;

    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->PROGRESSBAR_POS_X:I

    int-to-float v2, v2

    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->PROGRESS_GROUP_HEIGHT:I

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->PROGRESSBAR_HEIGHT:I

    sub-int/2addr v3, v4

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->PROGRESSBAR_WIDTH:I

    int-to-float v4, v4

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->PROGRESSBAR_HEIGHT:I

    int-to-float v5, v5

    const v6, 0x7f0202e9

    const v7, 0x7f0202e8

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/glview/TwGLProgressBar;-><init>(Lcom/sec/android/glview/TwGLContext;FFFFII)V

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mProgressBar:Lcom/sec/android/glview/TwGLProgressBar;

    .line 212
    new-instance v0, Lcom/sec/android/glview/TwGLText;

    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->PROGRESS_MAX_SIZE_POS_X:I

    int-to-float v2, v2

    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->PROGRESS_SIZE_TEXT_WIDTH:I

    int-to-float v4, v3

    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->PROGRESS_GROUP_HEIGHT:I

    int-to-float v5, v3

    const-string v6, ""

    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->REC_SIZE_TEXT_SIZE:I

    int-to-float v7, v3

    invoke-static {v11, v11, v11, v11}, Landroid/graphics/Color;->argb(IIII)I

    move-result v8

    move v3, v12

    invoke-direct/range {v0 .. v9}, Lcom/sec/android/glview/TwGLText;-><init>(Lcom/sec/android/glview/TwGLContext;FFFFLjava/lang/String;FIZ)V

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mSharingMaxRecSize:Lcom/sec/android/glview/TwGLText;

    .line 213
    new-instance v0, Lcom/sec/android/glview/TwGLText;

    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->PROGRESS_MAX_SIZE_POS_X:I

    int-to-float v2, v2

    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->PROGRESS_SIZE_TEXT_WIDTH:I

    int-to-float v4, v3

    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->PROGRESS_GROUP_HEIGHT:I

    int-to-float v5, v3

    const-string v6, "50M"

    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->REC_SIZE_TEXT_SIZE:I

    int-to-float v7, v3

    invoke-static {v11, v11, v11, v11}, Landroid/graphics/Color;->argb(IIII)I

    move-result v8

    move v3, v12

    invoke-direct/range {v0 .. v9}, Lcom/sec/android/glview/TwGLText;-><init>(Lcom/sec/android/glview/TwGLContext;FFFFLjava/lang/String;FIZ)V

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mEmailMaxRecSize:Lcom/sec/android/glview/TwGLText;

    .line 215
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mProgressBarGroup:Lcom/sec/android/glview/TwGLViewGroup;

    invoke-virtual {v0, v9}, Lcom/sec/android/glview/TwGLView;->setRotatable(Z)V

    .line 216
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mProgressBarGroup:Lcom/sec/android/glview/TwGLViewGroup;

    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->PROGRESS_GROUP_SIDE_MARGIN:I

    int-to-float v2, v2

    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->FULL_SCREEN_HEIGHT:I

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->PROGRESS_GROUP_WIDTH:I

    sub-int/2addr v3, v4

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    invoke-virtual {v0, v9, v2, v3}, Lcom/sec/android/glview/TwGLView;->setLeftTop(IFF)V

    .line 217
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mProgressBarGroup:Lcom/sec/android/glview/TwGLViewGroup;

    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->FULL_SCREEN_WIDTH:I

    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->PROGRESS_GROUP_X:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->PROGRESS_GROUP_BOTTOM_MARGIN_180:I

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->PROGRESS_GROUP_HEIGHT:I

    add-int/2addr v3, v4

    int-to-float v3, v3

    invoke-virtual {v0, v13, v2, v3}, Lcom/sec/android/glview/TwGLView;->setLeftTop(IFF)V

    .line 218
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mProgressBarGroup:Lcom/sec/android/glview/TwGLViewGroup;

    const/4 v2, 0x3

    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->FULL_SCREEN_WIDTH:I

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->PROGRESS_GROUP_SIDE_MARGIN:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->FULL_SCREEN_HEIGHT:I

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->PROGRESS_GROUP_WIDTH:I

    sub-int/2addr v4, v5

    div-int/lit8 v4, v4, 0x2

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->PROGRESS_GROUP_WIDTH:I

    add-int/2addr v4, v5

    int-to-float v4, v4

    invoke-virtual {v0, v2, v3, v4}, Lcom/sec/android/glview/TwGLView;->setLeftTop(IFF)V

    .line 219
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mProgressBarGroup:Lcom/sec/android/glview/TwGLViewGroup;

    invoke-virtual {v0, v14}, Lcom/sec/android/glview/TwGLView;->setVisibility(I)V

    .line 221
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mProgressBar:Lcom/sec/android/glview/TwGLProgressBar;

    const/16 v2, 0x64

    invoke-virtual {v0, v2}, Lcom/sec/android/glview/TwGLProgressBar;->setMax(I)V

    .line 222
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mSharingMaxRecSize:Lcom/sec/android/glview/TwGLText;

    invoke-virtual {v0, v9, v13}, Lcom/sec/android/glview/TwGLText;->setAlign(II)V

    .line 223
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mEmailMaxRecSize:Lcom/sec/android/glview/TwGLText;

    invoke-virtual {v0, v9, v13}, Lcom/sec/android/glview/TwGLText;->setAlign(II)V

    .line 224
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mProgressBarGroup:Lcom/sec/android/glview/TwGLViewGroup;

    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mCurRecSize:Lcom/sec/android/glview/TwGLText;

    invoke-virtual {v0, v2}, Lcom/sec/android/glview/TwGLViewGroup;->addView(Lcom/sec/android/glview/TwGLView;)V

    .line 225
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mProgressBarGroup:Lcom/sec/android/glview/TwGLViewGroup;

    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mProgressBar:Lcom/sec/android/glview/TwGLProgressBar;

    invoke-virtual {v0, v2}, Lcom/sec/android/glview/TwGLViewGroup;->addView(Lcom/sec/android/glview/TwGLView;)V

    .line 226
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mProgressBarGroup:Lcom/sec/android/glview/TwGLViewGroup;

    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mSharingMaxRecSize:Lcom/sec/android/glview/TwGLText;

    invoke-virtual {v0, v2}, Lcom/sec/android/glview/TwGLViewGroup;->addView(Lcom/sec/android/glview/TwGLView;)V

    .line 227
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mProgressBarGroup:Lcom/sec/android/glview/TwGLViewGroup;

    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mEmailMaxRecSize:Lcom/sec/android/glview/TwGLText;

    invoke-virtual {v0, v2}, Lcom/sec/android/glview/TwGLViewGroup;->addView(Lcom/sec/android/glview/TwGLView;)V

    .line 228
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mMenuGroup:Lcom/sec/android/glview/TwGLViewGroup;

    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mProgressBarGroup:Lcom/sec/android/glview/TwGLViewGroup;

    invoke-virtual {v0, v2}, Lcom/sec/android/glview/TwGLViewGroup;->addView(Lcom/sec/android/glview/TwGLView;)V

    .line 230
    new-instance v0, Lcom/sec/android/glview/TwGLViewGroup;

    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->REMAIN_TIME_GROUP_POS_X:I

    int-to-float v2, v2

    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->REMAIN_TIME_GROUP_POS_Y:I

    int-to-float v3, v3

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->REMAIN_TIME_GROUP_WIDTH:I

    int-to-float v4, v4

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->REMAIN_TIME_GROUP_HEIGHT:I

    int-to-float v5, v5

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/glview/TwGLViewGroup;-><init>(Lcom/sec/android/glview/TwGLContext;FFFF)V

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mRecRemainTimeGroup:Lcom/sec/android/glview/TwGLViewGroup;

    .line 231
    new-instance v0, Lcom/sec/android/glview/TwGLText;

    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->REMAIN_TIME_GROUP_WIDTH:I

    int-to-float v4, v2

    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->REMAIN_TIME_GROUP_HEIGHT:I

    int-to-float v5, v2

    const-string v6, "0K"

    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->REC_SIZE_TEXT_SIZE:I

    int-to-float v7, v2

    invoke-static {v11, v11, v11, v11}, Landroid/graphics/Color;->argb(IIII)I

    move-result v8

    move v2, v12

    move v3, v12

    invoke-direct/range {v0 .. v9}, Lcom/sec/android/glview/TwGLText;-><init>(Lcom/sec/android/glview/TwGLContext;FFFFLjava/lang/String;FIZ)V

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mRecRemainTimeText:Lcom/sec/android/glview/TwGLText;

    .line 232
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mRecRemainTimeGroup:Lcom/sec/android/glview/TwGLViewGroup;

    invoke-virtual {v0, v9}, Lcom/sec/android/glview/TwGLView;->setRotatable(Z)V

    .line 234
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mRecRemainTimeGroup:Lcom/sec/android/glview/TwGLViewGroup;

    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->FULL_SCREEN_WIDTH:I

    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->SIDE_MENU_WIDTH:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->FULL_SCREEN_HEIGHT:I

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->REMAIN_TIME_GROUP_WIDTH:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    invoke-virtual {v0, v9, v2, v3}, Lcom/sec/android/glview/TwGLView;->setLeftTop(IFF)V

    .line 235
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mRecRemainTimeGroup:Lcom/sec/android/glview/TwGLViewGroup;

    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->SIDE_MENU_WIDTH:I

    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->REMAIN_TIME_GROUP_WIDTH:I

    add-int/2addr v2, v3

    int-to-float v2, v2

    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->FULL_SCREEN_HEIGHT:I

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->INDICATOR_SIDE_MARGIN:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    invoke-virtual {v0, v13, v2, v3}, Lcom/sec/android/glview/TwGLView;->setLeftTop(IFF)V

    .line 236
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mRecRemainTimeGroup:Lcom/sec/android/glview/TwGLViewGroup;

    const/4 v2, 0x3

    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->SIDE_MENU_WIDTH:I

    int-to-float v3, v3

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->REMAIN_TIME_GROUP_WIDTH:I

    int-to-float v4, v4

    invoke-virtual {v0, v2, v3, v4}, Lcom/sec/android/glview/TwGLView;->setLeftTop(IFF)V

    .line 238
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mRecRemainTimeGroup:Lcom/sec/android/glview/TwGLViewGroup;

    invoke-virtual {v0, v14}, Lcom/sec/android/glview/TwGLView;->setVisibility(I)V

    .line 239
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mRecRemainTimeText:Lcom/sec/android/glview/TwGLText;

    const/4 v2, 0x3

    invoke-virtual {v0, v2, v13}, Lcom/sec/android/glview/TwGLText;->setAlign(II)V

    .line 240
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mRecRemainTimeGroup:Lcom/sec/android/glview/TwGLViewGroup;

    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mRecRemainTimeText:Lcom/sec/android/glview/TwGLText;

    invoke-virtual {v0, v2}, Lcom/sec/android/glview/TwGLViewGroup;->addView(Lcom/sec/android/glview/TwGLView;)V

    .line 241
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mMenuGroup:Lcom/sec/android/glview/TwGLViewGroup;

    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mRecRemainTimeGroup:Lcom/sec/android/glview/TwGLViewGroup;

    invoke-virtual {v0, v2}, Lcom/sec/android/glview/TwGLViewGroup;->addView(Lcom/sec/android/glview/TwGLView;)V

    .line 243
    new-instance v2, Lcom/sec/android/glview/TwGLViewGroup;

    iget-object v0, p0, Lcom/sec/android/app/camera/MenuBase;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getGLContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v3

    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->RIGHT_SIDE_MENU_POS_X:I

    int-to-float v4, v0

    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->RIGHT_SIDE_MENU_WIDTH:I

    int-to-float v6, v0

    sget v0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->FULL_SCREEN_HEIGHT:I

    int-to-float v7, v0

    move v5, v12

    invoke-direct/range {v2 .. v7}, Lcom/sec/android/glview/TwGLViewGroup;-><init>(Lcom/sec/android/glview/TwGLContext;FFFF)V

    iput-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mRecordingSideMenu:Lcom/sec/android/glview/TwGLViewGroup;

    .line 244
    new-instance v0, Lcom/sec/android/glview/TwGLButton;

    sget v2, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->RECORDING_BUTTON_POS_Y:I

    int-to-float v3, v2

    const v4, 0x7f0202fc

    const v5, 0x7f0202fe

    const v6, 0x7f0202fd

    const/4 v7, 0x0

    move v2, v12

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/glview/TwGLButton;-><init>(Lcom/sec/android/glview/TwGLContext;FFIIII)V

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mStopButton:Lcom/sec/android/glview/TwGLButton;

    .line 245
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mStopButton:Lcom/sec/android/glview/TwGLButton;

    invoke-virtual {v0, v9}, Lcom/sec/android/glview/TwGLButton;->setMute(Z)V

    .line 246
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mStopButton:Lcom/sec/android/glview/TwGLButton;

    invoke-virtual {v0, p0}, Lcom/sec/android/glview/TwGLView;->setOnClickListener(Lcom/sec/android/glview/TwGLView$OnClickListener;)V

    .line 247
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mStopButton:Lcom/sec/android/glview/TwGLButton;

    iget-object v2, p0, Lcom/sec/android/app/camera/MenuBase;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v2}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a01af

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/android/glview/TwGLView;->setTitle(Ljava/lang/String;)V

    .line 249
    new-instance v0, Lcom/sec/android/glview/TwGLImage;

    iget-object v2, p0, Lcom/sec/android/app/camera/MenuBase;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v2}, Lcom/sec/android/app/camera/Camera;->getGLContext()Lcom/sec/android/glview/TwGLContext;

    move-result-object v2

    sget v3, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->RECORDING_BUTTON_STOP_ICON_LEFT_MARGIN:I

    int-to-float v3, v3

    sget v4, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->RECORDING_BUTTON_POS_Y:I

    sget v5, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->RECORDING_BUTTON_STOP_ICON_TOP_MARGIN:I

    add-int/2addr v4, v5

    int-to-float v4, v4

    const v5, 0x7f020308

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/sec/android/glview/TwGLImage;-><init>(Lcom/sec/android/glview/TwGLContext;FFI)V

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mStopButtonImage:Lcom/sec/android/glview/TwGLImage;

    .line 250
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mStopButtonImage:Lcom/sec/android/glview/TwGLImage;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/sec/android/glview/TwGLView;->setRotatable(Z)V

    .line 251
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mStopButtonImage:Lcom/sec/android/glview/TwGLImage;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/sec/android/glview/TwGLView;->setRotateAnimation(Z)V

    .line 252
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mStopButtonImage:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v0, v9}, Lcom/sec/android/glview/TwGLView;->setBypassTouch(Z)V

    .line 254
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mRecordingSideMenu:Lcom/sec/android/glview/TwGLViewGroup;

    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mStopButton:Lcom/sec/android/glview/TwGLButton;

    invoke-virtual {v0, v2}, Lcom/sec/android/glview/TwGLViewGroup;->addView(Lcom/sec/android/glview/TwGLView;)V

    .line 255
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mRecordingSideMenu:Lcom/sec/android/glview/TwGLViewGroup;

    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mStopButtonImage:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v0, v2}, Lcom/sec/android/glview/TwGLViewGroup;->addView(Lcom/sec/android/glview/TwGLView;)V

    .line 276
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mRecordingSideMenu:Lcom/sec/android/glview/TwGLViewGroup;

    invoke-virtual {v0, v14}, Lcom/sec/android/glview/TwGLView;->setVisibility(I)V

    .line 277
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mMenuGroup:Lcom/sec/android/glview/TwGLViewGroup;

    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mRecordingSideMenu:Lcom/sec/android/glview/TwGLViewGroup;

    invoke-virtual {v0, v2}, Lcom/sec/android/glview/TwGLViewGroup;->addView(Lcom/sec/android/glview/TwGLView;)V

    .line 278
    iget-object v0, p0, Lcom/sec/android/app/camera/MenuBase;->mGLParentView:Lcom/sec/android/glview/TwGLViewGroup;

    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mMenuGroup:Lcom/sec/android/glview/TwGLViewGroup;

    invoke-virtual {v0, v2}, Lcom/sec/android/glview/TwGLViewGroup;->addView(Lcom/sec/android/glview/TwGLView;)V

    .line 279
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 283
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mStopButton:Lcom/sec/android/glview/TwGLButton;

    if-eqz v0, :cond_0

    .line 284
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mStopButton:Lcom/sec/android/glview/TwGLButton;

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLView;->setOnClickListener(Lcom/sec/android/glview/TwGLView$OnClickListener;)V

    .line 285
    iput-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mStopButton:Lcom/sec/android/glview/TwGLButton;

    .line 288
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mMenuGroup:Lcom/sec/android/glview/TwGLViewGroup;

    if-eqz v0, :cond_1

    .line 289
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mMenuGroup:Lcom/sec/android/glview/TwGLViewGroup;

    invoke-virtual {v0}, Lcom/sec/android/glview/TwGLViewGroup;->clear()V

    .line 290
    iput-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mMenuGroup:Lcom/sec/android/glview/TwGLViewGroup;

    .line 293
    :cond_1
    iput-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mIndicatorRecTime:Lcom/sec/android/glview/TwGLText;

    .line 294
    iput-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mIndicatorRecIcon:Lcom/sec/android/glview/TwGLImage;

    .line 296
    iput-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mRecRemainTimeText:Lcom/sec/android/glview/TwGLText;

    .line 297
    iput-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mProgressBar:Lcom/sec/android/glview/TwGLProgressBar;

    .line 298
    iput-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mCurRecSize:Lcom/sec/android/glview/TwGLText;

    .line 299
    iput-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mSharingMaxRecSize:Lcom/sec/android/glview/TwGLText;

    .line 300
    iput-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mEmailMaxRecSize:Lcom/sec/android/glview/TwGLText;

    .line 302
    iput-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mIndicatorRecGroup:Lcom/sec/android/glview/TwGLViewGroup;

    .line 304
    iput-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mProgressBarGroup:Lcom/sec/android/glview/TwGLViewGroup;

    .line 305
    iput-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mRecRemainTimeGroup:Lcom/sec/android/glview/TwGLViewGroup;

    .line 306
    iput-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mRecordingProgressHandler:Landroid/os/Handler;

    .line 308
    invoke-super {p0}, Lcom/sec/android/app/camera/MenuBase;->clear()V

    .line 309
    return-void
.end method

.method public doStop()V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 449
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mRecordingTime:I

    if-lt v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mStopping:Z

    if-eqz v0, :cond_1

    .line 458
    :cond_0
    :goto_0
    return-void

    .line 453
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->stopTimer()V

    .line 454
    iget-object v0, p0, Lcom/sec/android/app/camera/MenuBase;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/camera/Camera;->handleRecordingCommand(I)V

    .line 455
    iput v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mRecordingState:I

    .line 457
    iput-boolean v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mStopping:Z

    goto :goto_0
.end method

.method public getRecordingState()I
    .locals 1

    .prologue
    .line 492
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mRecordingState:I

    return v0
.end method

.method public getRecordingTime()I
    .locals 1

    .prologue
    .line 421
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mRecordingTime:I

    return v0
.end method

.method public onClick(Lcom/sec/android/glview/TwGLView;)Z
    .locals 3
    .parameter "view"

    .prologue
    const/4 v0, 0x1

    .line 501
    invoke-virtual {p1}, Lcom/sec/android/glview/TwGLView;->getTag()I

    move-result v1

    const/16 v2, 0x20

    if-ne v1, v2, :cond_0

    .line 502
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->doStop()V

    .line 517
    :goto_0
    return v0

    .line 504
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mStopButton:Lcom/sec/android/glview/TwGLButton;

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 505
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->doStop()V

    goto :goto_0

    .line 517
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onHide()V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 357
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mMenuGroup:Lcom/sec/android/glview/TwGLViewGroup;

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLView;->setVisibility(I)V

    .line 358
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mRecordingSideMenu:Lcom/sec/android/glview/TwGLViewGroup;

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLView;->setVisibility(I)V

    .line 360
    iget-object v0, p0, Lcom/sec/android/app/camera/MenuBase;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->showBaseMenu()V

    .line 361
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->stopTimer()V

    .line 363
    invoke-super {p0}, Lcom/sec/android/app/camera/MenuBase;->onHide()V

    .line 364
    return-void
.end method

.method public onHideMenu()V
    .locals 0

    .prologue
    .line 367
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->onHide()V

    .line 368
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .parameter "keyCode"
    .parameter "event"

    .prologue
    .line 521
    sparse-switch p1, :sswitch_data_0

    .line 529
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 527
    :sswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 521
    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x17 -> :sswitch_0
        0x1b -> :sswitch_0
        0x42 -> :sswitch_0
        0x82 -> :sswitch_0
    .end sparse-switch
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 3
    .parameter "keyCode"
    .parameter "event"

    .prologue
    const/4 v2, 0x5

    const/4 v0, 0x1

    .line 533
    sparse-switch p1, :sswitch_data_0

    .line 552
    const/4 v0, 0x0

    :cond_0
    :goto_0
    return v0

    .line 535
    :sswitch_0
    iget-boolean v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mStopping:Z

    if-nez v1, :cond_0

    .line 538
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->doStop()V

    goto :goto_0

    .line 543
    :sswitch_1
    iget-object v1, p0, Lcom/sec/android/app/camera/MenuBase;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/Camera;->getRemainTime()I

    move-result v1

    if-lt v1, v2, :cond_0

    .line 546
    iget-object v1, p0, Lcom/sec/android/app/camera/MenuBase;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/camera/Camera;->handleRecordingCommand(I)V

    goto :goto_0

    .line 549
    :sswitch_2
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->doStop()V

    goto :goto_0

    .line 533
    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x17 -> :sswitch_1
        0x1b -> :sswitch_1
        0x42 -> :sswitch_1
        0x82 -> :sswitch_2
    .end sparse-switch
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 371
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->onHide()V

    .line 372
    return-void
.end method

.method protected onShow()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x0

    .line 312
    iget-object v0, p0, Lcom/sec/android/app/camera/MenuBase;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->hideBaseMenu()V

    .line 314
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mMenuGroup:Lcom/sec/android/glview/TwGLViewGroup;

    invoke-virtual {v0, v6}, Lcom/sec/android/glview/TwGLView;->setVisibility(I)V

    .line 315
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mRecordingSideMenu:Lcom/sec/android/glview/TwGLViewGroup;

    invoke-virtual {v0, v6}, Lcom/sec/android/glview/TwGLView;->setVisibility(I)V

    .line 317
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mIndicatorRecGroup:Lcom/sec/android/glview/TwGLViewGroup;

    invoke-virtual {v0, v6}, Lcom/sec/android/glview/TwGLView;->setVisibility(I)V

    .line 318
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mIndicatorRecIcon:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v0, v6}, Lcom/sec/android/glview/TwGLView;->setVisibility(I)V

    .line 319
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mIndicatorRecIconText:Lcom/sec/android/glview/TwGLText;

    invoke-virtual {v0, v6}, Lcom/sec/android/glview/TwGLView;->setVisibility(I)V

    .line 320
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mIndicatorRecTime:Lcom/sec/android/glview/TwGLText;

    invoke-virtual {v0, v6}, Lcom/sec/android/glview/TwGLView;->setVisibility(I)V

    .line 322
    iget-object v0, p0, Lcom/sec/android/app/camera/MenuBase;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/CameraSettings;->getAttachMMSMode()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 323
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->isSharingMode:Z

    .line 324
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mSharingMaxRecSize:Lcom/sec/android/glview/TwGLText;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/camera/MenuBase;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v2}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/camera/CameraSettings;->getRequestedRecordingSize()J

    move-result-wide v2

    const-wide/16 v4, 0x400

    div-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "K"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLText;->setText(Ljava/lang/String;)V

    .line 325
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mProgressBar:Lcom/sec/android/glview/TwGLProgressBar;

    invoke-virtual {v0, v6}, Lcom/sec/android/glview/TwGLView;->setVisibility(I)V

    .line 326
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mProgressBarGroup:Lcom/sec/android/glview/TwGLViewGroup;

    invoke-virtual {v0, v6}, Lcom/sec/android/glview/TwGLView;->setVisibility(I)V

    .line 327
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mCurRecSize:Lcom/sec/android/glview/TwGLText;

    invoke-virtual {v0, v6}, Lcom/sec/android/glview/TwGLView;->setVisibility(I)V

    .line 328
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mSharingMaxRecSize:Lcom/sec/android/glview/TwGLText;

    invoke-virtual {v0, v6}, Lcom/sec/android/glview/TwGLView;->setVisibility(I)V

    .line 329
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mEmailMaxRecSize:Lcom/sec/android/glview/TwGLText;

    invoke-virtual {v0, v7}, Lcom/sec/android/glview/TwGLView;->setVisibility(I)V

    .line 343
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mRecRemainTimeGroup:Lcom/sec/android/glview/TwGLViewGroup;

    invoke-virtual {v0, v7}, Lcom/sec/android/glview/TwGLView;->setVisibility(I)V

    .line 344
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mRecRemainTimeText:Lcom/sec/android/glview/TwGLText;

    invoke-virtual {v0, v7}, Lcom/sec/android/glview/TwGLView;->setVisibility(I)V

    .line 346
    iput-boolean v6, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mStopping:Z

    .line 347
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mProgress:I

    if-lez v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->isSharingMode:Z

    if-nez v0, :cond_1

    .line 348
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mProgressBar:Lcom/sec/android/glview/TwGLProgressBar;

    invoke-virtual {v0, v6}, Lcom/sec/android/glview/TwGLProgressBar;->setProgress(I)V

    .line 349
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mCurRecSize:Lcom/sec/android/glview/TwGLText;

    const-string v1, "0K"

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLText;->setText(Ljava/lang/String;)V

    .line 351
    :cond_1
    iput v6, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mRecordingLimitTime:I

    .line 353
    invoke-super {p0}, Lcom/sec/android/app/camera/MenuBase;->onShow()V

    .line 354
    return-void

    .line 333
    :cond_2
    iput-boolean v6, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->isSharingMode:Z

    .line 334
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mProgressBar:Lcom/sec/android/glview/TwGLProgressBar;

    invoke-virtual {v0, v7}, Lcom/sec/android/glview/TwGLView;->setVisibility(I)V

    .line 335
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mProgressBarGroup:Lcom/sec/android/glview/TwGLViewGroup;

    invoke-virtual {v0, v7}, Lcom/sec/android/glview/TwGLView;->setVisibility(I)V

    .line 336
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mCurRecSize:Lcom/sec/android/glview/TwGLText;

    invoke-virtual {v0, v7}, Lcom/sec/android/glview/TwGLView;->setVisibility(I)V

    .line 337
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mSharingMaxRecSize:Lcom/sec/android/glview/TwGLText;

    invoke-virtual {v0, v7}, Lcom/sec/android/glview/TwGLView;->setVisibility(I)V

    .line 338
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mEmailMaxRecSize:Lcom/sec/android/glview/TwGLText;

    invoke-virtual {v0, v7}, Lcom/sec/android/glview/TwGLView;->setVisibility(I)V

    goto :goto_0
.end method

.method public pauseTimer()V
    .locals 2

    .prologue
    .line 487
    iget-object v0, p0, Lcom/sec/android/app/camera/MenuBase;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->restartInactivityTimer()V

    .line 488
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mRecordingProgressHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 489
    return-void
.end method

.method public setRecordingState(I)V
    .locals 0
    .parameter "state"

    .prologue
    .line 496
    iput p1, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mRecordingState:I

    .line 497
    return-void
.end method

.method public startTimer()V
    .locals 3

    .prologue
    .line 461
    iget-object v0, p0, Lcom/sec/android/app/camera/MenuBase;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->stopInactivityTimer()V

    .line 462
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mRecordingState:I

    .line 464
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mIndicatorRecTime:Lcom/sec/android/glview/TwGLText;

    const-string v1, "00:00"

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLText;->setText(Ljava/lang/String;)V

    .line 465
    iget-object v0, p0, Lcom/sec/android/app/camera/MenuBase;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->getRequestedDurationLimit()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mRecordingLimitTime:I

    .line 466
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mRecordingLimitTime:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mRecordingLimitTime:I

    const/16 v1, 0x3c

    if-gt v0, v1, :cond_0

    .line 467
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->hmsConvert(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mHMS:Ljava/lang/String;

    .line 468
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mIndicatorRecTime:Lcom/sec/android/glview/TwGLText;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mHMS:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " / "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mRecordingLimitTime:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " sec"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLText;->setText(Ljava/lang/String;)V

    .line 471
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->stepSecond()V

    .line 472
    return-void
.end method

.method public stepSecond()V
    .locals 5

    .prologue
    .line 475
    iget-object v1, p0, Lcom/sec/android/app/camera/MenuBase;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/Camera;->handleRecordingTimerElapsed()I

    move-result v0

    .line 477
    .local v0, drift:I
    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mRecordingProgressHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    rsub-int/lit8 v3, v0, 0x64

    int-to-long v3, v3

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 478
    return-void
.end method

.method public stopTimer()V
    .locals 2

    .prologue
    .line 481
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mRecordingProgressHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 482
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mRecordingTime:I

    .line 483
    iget-object v0, p0, Lcom/sec/android/app/camera/MenuBase;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->restartInactivityTimer()V

    .line 484
    return-void
.end method

.method public updateProgressBarText(J)V
    .locals 6
    .parameter "bytes"

    .prologue
    .line 425
    iget-boolean v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->isSharingMode:Z

    if-eqz v2, :cond_2

    .line 426
    iget-object v2, p0, Lcom/sec/android/app/camera/MenuBase;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v2}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/CameraSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/camera/CameraSettings;->getRequestedRecordingSize()J

    move-result-wide v0

    .line 427
    .local v0, mMaxSize:J
    const-wide/16 v2, 0x64

    mul-long/2addr v2, p1

    div-long/2addr v2, v0

    long-to-int v2, v2

    iput v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mProgress:I

    .line 428
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mProgressBar:Lcom/sec/android/glview/TwGLProgressBar;

    iget v3, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mProgress:I

    invoke-virtual {v2, v3}, Lcom/sec/android/glview/TwGLProgressBar;->setProgress(I)V

    .line 430
    cmp-long v2, p1, v0

    if-ltz v2, :cond_0

    .line 431
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->doStop()V

    .line 433
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mCurRecSize:Lcom/sec/android/glview/TwGLText;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-wide/16 v4, 0x400

    div-long v4, p1, v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "K"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/glview/TwGLText;->setText(Ljava/lang/String;)V

    .line 446
    .end local v0           #mMaxSize:J
    :cond_1
    :goto_0
    return-void

    .line 435
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/camera/MenuBase;->mActivityContext:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v2}, Lcom/sec/android/app/camera/Camera;->getRemainTime()I

    move-result v2

    const/4 v3, 0x1

    if-ge v2, v3, :cond_1

    .line 436
    invoke-virtual {p0}, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->doStop()V

    goto :goto_0
.end method

.method public updateRecordingTime(I)V
    .locals 3
    .parameter "second"

    .prologue
    .line 404
    iput p1, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mRecordingTime:I

    .line 405
    invoke-direct {p0, p1}, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->hmsConvert(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mHMS:Ljava/lang/String;

    .line 407
    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mRecordingLimitTime:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mRecordingLimitTime:I

    const/16 v1, 0x3c

    if-gt v0, v1, :cond_0

    .line 408
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mIndicatorRecTime:Lcom/sec/android/glview/TwGLText;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mHMS:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " / "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mRecordingLimitTime:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " sec"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLText;->setText(Ljava/lang/String;)V

    .line 413
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mIndicatorRecIcon:Lcom/sec/android/glview/TwGLImage;

    invoke-virtual {v0}, Lcom/sec/android/glview/TwGLView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 414
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mIndicatorRecIcon:Lcom/sec/android/glview/TwGLImage;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLView;->setVisibility(I)V

    .line 418
    :goto_1
    return-void

    .line 410
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mIndicatorRecTime:Lcom/sec/android/glview/TwGLText;

    iget-object v1, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mHMS:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLText;->setText(Ljava/lang/String;)V

    goto :goto_0

    .line 416
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/camera/glwidget/TwGLEasyCameraRecordingMenu;->mIndicatorRecIcon:Lcom/sec/android/glview/TwGLImage;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/glview/TwGLView;->setVisibility(I)V

    goto :goto_1
.end method
