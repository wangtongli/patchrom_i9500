.class public final Lcom/sec/android/app/camera/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/camera/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final ADJUST:I = 0x7f0a00fd

.field public static final AS_RSC:I = 0x7f0a00e2

.field public static final AUTO:I = 0x7f0a0060

.field public static final Action_Shot_Helptext:I = 0x7f0a013f

.field public static final Add_Me_Helptext:I = 0x7f0a013e

.field public static final BestPhoto_Helptext:I = 0x7f0a0145

.field public static final CONTRAST:I = 0x7f0a00fe

.field public static final CS_PROGRESS_NUMBER:I = 0x7f0a0023

.field public static final CS_PROGRESS_TEXT:I = 0x7f0a00eb

.field public static final Cartoon_Helptext:I = 0x7f0a0143

.field public static final Cinepic_Helptext:I = 0x7f0a0149

.field public static final Dialog_No:I = 0x7f0a013b

.field public static final Dialog_Yes:I = 0x7f0a013a

.field public static final Drama_Helptext:I = 0x7f0a0147

.field public static final EFFECT_CONSTRAINT_MESSAGE:I = 0x7f0a0094

.field public static final ER_EFFECT_BIG_EYES:I = 0x7f0a01db

.field public static final ER_EFFECT_BIG_MOUTH:I = 0x7f0a01dd

.field public static final ER_EFFECT_BIG_NOSE:I = 0x7f0a01dc

.field public static final ER_EFFECT_NOEFFECT:I = 0x7f0a01d9

.field public static final ER_EFFECT_NONE:I = 0x7f0a01d8

.field public static final ER_EFFECT_SMALL_EYES:I = 0x7f0a01de

.field public static final ER_EFFECT_SMALL_MOUTH:I = 0x7f0a01df

.field public static final ER_EFFECT_SQUEEZE:I = 0x7f0a01da

.field public static final EXPOSURE_METER:I = 0x7f0a00fc

.field public static final EXT_DOWNLOAD:I = 0x7f0a0091

.field public static final EXT_DOWNLOADS:I = 0x7f0a0092

.field public static final EXT_PRELOAD:I = 0x7f0a0093

.field public static final E_BW:I = 0x7f0a006e

.field public static final E_CIRCLELENS:I = 0x7f0a009b

.field public static final E_COMIC:I = 0x7f0a0084

.field public static final E_CUBISM:I = 0x7f0a0096

.field public static final E_EMBOSS:I = 0x7f0a0071

.field public static final E_EXPOSURE_OVERLAY:I = 0x7f0a009e

.field public static final E_FADED_COLOURS:I = 0x7f0a0080

.field public static final E_FISHEYE:I = 0x7f0a008c

.field public static final E_FOR_REAL:I = 0x7f0a008d

.field public static final E_GOTHIC_NOIR:I = 0x7f0a0086

.field public static final E_HEART:I = 0x7f0a009d

.field public static final E_IMPRESSIONIST:I = 0x7f0a0087

.field public static final E_INSTAGRAM_NASHVILLE:I = 0x7f0a008b

.field public static final E_MONOCHROME:I = 0x7f0a0074

.field public static final E_NEGATIVE:I = 0x7f0a0070

.field public static final E_NOIR_NOTE:I = 0x7f0a008f

.field public static final E_NOSTALGIA:I = 0x7f0a007f

.field public static final E_OLDPHOTO:I = 0x7f0a0083

.field public static final E_OUTLINE:I = 0x7f0a0072

.field public static final E_OVAL_BLUR:I = 0x7f0a009a

.field public static final E_PASTEL_SKETCH:I = 0x7f0a0085

.field public static final E_POINT_BLUE:I = 0x7f0a007c

.field public static final E_POINT_GREEN:I = 0x7f0a007d

.field public static final E_POINT_RED_YELLOW:I = 0x7f0a007e

.field public static final E_POLAROID:I = 0x7f0a0097

.field public static final E_POSTCARD:I = 0x7f0a0098

.field public static final E_POSTERIZE:I = 0x7f0a0077

.field public static final E_RAINBOW:I = 0x7f0a008a

.field public static final E_RETRO:I = 0x7f0a0081

.field public static final E_SANDSTONE:I = 0x7f0a0089

.field public static final E_SEPIA:I = 0x7f0a006f

.field public static final E_SHINY:I = 0x7f0a009c

.field public static final E_SIGNATURE:I = 0x7f0a0099

.field public static final E_SKETCH:I = 0x7f0a0075

.field public static final E_SOLARIZE:I = 0x7f0a0078

.field public static final E_SPLIT_VIEW:I = 0x7f0a009f

.field public static final E_STUCCHEVOLE:I = 0x7f0a008e

.field public static final E_SUNSHINE:I = 0x7f0a0082

.field public static final E_VIGNETTE_OUTFOCUS:I = 0x7f0a0088

.field public static final E_VINCENT:I = 0x7f0a0090

.field public static final E_VINTAGE:I = 0x7f0a0079

.field public static final E_VINTAGE_COLD:I = 0x7f0a007a

.field public static final E_VINTAGE_WARM:I = 0x7f0a007b

.field public static final E_WASHED:I = 0x7f0a0076

.field public static final E_WATERCOLOR:I = 0x7f0a0073

.field public static final E_WINDOW:I = 0x7f0a0095

.field public static final Eraser_Helptext:I = 0x7f0a0146

.field public static final F_AUTOFOCUS:I = 0x7f0a00ce

.field public static final F_AUTO_FLASH:I = 0x7f0a00cd

.field public static final F_FACE:I = 0x7f0a00d0

.field public static final F_MACRO:I = 0x7f0a00cf

.field public static final F_PANFOCUS:I = 0x7f0a00d1

.field public static final GPS_msg:I = 0x7f0a014e

.field public static final Golf_help_text:I = 0x7f0a0142

.field public static final HDR_Helptext:I = 0x7f0a0144

.field public static final ISO_100:I = 0x7f0a0068

.field public static final ISO_1600:I = 0x7f0a006c

.field public static final ISO_200:I = 0x7f0a0069

.field public static final ISO_3200:I = 0x7f0a006d

.field public static final ISO_400:I = 0x7f0a006a

.field public static final ISO_50:I = 0x7f0a0067

.field public static final ISO_800:I = 0x7f0a006b

.field public static final MODE:I = 0x7f0a0048

.field public static final M_CENTER:I = 0x7f0a00db

.field public static final M_CENTER_WEIGHTED:I = 0x7f0a00dc

.field public static final M_MATRIX:I = 0x7f0a00de

.field public static final M_SPOT:I = 0x7f0a00dd

.field public static final MaxRecordingSizeMMS:I = 0x7f0a0022

.field public static final MultiFrame_Helptext:I = 0x7f0a0148

.field public static final NORMAL:I = 0x7f0a005c

.field public static final OK_STRING:I = 0x7f0a0101

.field public static final OPTION_OFF:I = 0x7f0a00fb

.field public static final OPTION_ON:I = 0x7f0a00fa

.field public static final OnScreen_HelpTextTouchAF:I = 0x7f0a0156

.field public static final Q_FINE:I = 0x7f0a00d3

.field public static final Q_SUPER_FINE:I = 0x7f0a00d2

.field public static final RM_BROADCASTING:I = 0x7f0a00ea

.field public static final RM_EMAIL:I = 0x7f0a00e6

.field public static final RM_FAST:I = 0x7f0a00e8

.field public static final RM_MMS:I = 0x7f0a00e5

.field public static final RM_NORMAL:I = 0x7f0a00e4

.field public static final RM_SHARING:I = 0x7f0a00e9

.field public static final RM_SLOW:I = 0x7f0a00e7

.field public static final R_1024x768:I = 0x7f0a00c3

.field public static final R_1248x672:I = 0x7f0a00ba

.field public static final R_1280x720:I = 0x7f0a00b9

.field public static final R_1280x960:I = 0x7f0a00b8

.field public static final R_1392x1392:I = 0x7f0a00b7

.field public static final R_1440x1080:I = 0x7f0a00b2

.field public static final R_1536x864:I = 0x7f0a00b6

.field public static final R_1600x1200:I = 0x7f0a00b4

.field public static final R_1600x960:I = 0x7f0a00b5

.field public static final R_1632x880:I = 0x7f0a00b3

.field public static final R_176x144:I = 0x7f0a00c2

.field public static final R_1920x1080:I = 0x7f0a00b1

.field public static final R_2048x1104:I = 0x7f0a00b0

.field public static final R_2048x1152:I = 0x7f0a00af

.field public static final R_2048x1232:I = 0x7f0a00ae

.field public static final R_2048x1536:I = 0x7f0a00ad

.field public static final R_2560x1440:I = 0x7f0a00ac

.field public static final R_2560x1536:I = 0x7f0a00ab

.field public static final R_2560x1920:I = 0x7f0a00aa

.field public static final R_2592x1944:I = 0x7f0a00a9

.field public static final R_3072x1856:I = 0x7f0a00a8

.field public static final R_3072x2304:I = 0x7f0a00a7

.field public static final R_320x240:I = 0x7f0a00c1

.field public static final R_3264x1836:I = 0x7f0a00a6

.field public static final R_3264x1968:I = 0x7f0a00a5

.field public static final R_3264x2448:I = 0x7f0a00a4

.field public static final R_400x240:I = 0x7f0a00c0

.field public static final R_4096x2304:I = 0x7f0a00a3

.field public static final R_4096x3072:I = 0x7f0a00a2

.field public static final R_4128x2322:I = 0x7f0a00a1

.field public static final R_4128x3096:I = 0x7f0a00a0

.field public static final R_640x480:I = 0x7f0a00bf

.field public static final R_720x480:I = 0x7f0a00be

.field public static final R_800x450:I = 0x7f0a00bd

.field public static final R_800x480:I = 0x7f0a00bc

.field public static final R_960x720:I = 0x7f0a00bb

.field public static final R_MOVIE_1280x720:I = 0x7f0a00c6

.field public static final R_MOVIE_1440x1080:I = 0x7f0a00c5

.field public static final R_MOVIE_176x144:I = 0x7f0a00cc

.field public static final R_MOVIE_1920x1080:I = 0x7f0a00c4

.field public static final R_MOVIE_320x240:I = 0x7f0a00cb

.field public static final R_MOVIE_640x480:I = 0x7f0a00ca

.field public static final R_MOVIE_720x480:I = 0x7f0a00c9

.field public static final R_MOVIE_800x450:I = 0x7f0a00c8

.field public static final R_MOVIE_960x720:I = 0x7f0a00c7

.field public static final SATURATION:I = 0x7f0a00ff

.field public static final SCN_BACK_LIGHT:I = 0x7f0a00f9

.field public static final SCN_BEACH_SNOW:I = 0x7f0a00f1

.field public static final SCN_CANDLE_LIGHT:I = 0x7f0a00f8

.field public static final SCN_DAWN_DUSK:I = 0x7f0a00f3

.field public static final SCN_FALL_COLOR:I = 0x7f0a00f4

.field public static final SCN_FIREWORK:I = 0x7f0a00f6

.field public static final SCN_LANDSCAPE:I = 0x7f0a00ee

.field public static final SCN_NIGHT_SHOT:I = 0x7f0a00f5

.field public static final SCN_NONE:I = 0x7f0a00ec

.field public static final SCN_PARTY_INDOOR:I = 0x7f0a00f0

.field public static final SCN_PORTRAIT:I = 0x7f0a00ed

.field public static final SCN_SPORTS:I = 0x7f0a00ef

.field public static final SCN_SUNSET:I = 0x7f0a00f2

.field public static final SCN_TEXT:I = 0x7f0a00f7

.field public static final SHARPNESS:I = 0x7f0a0100

.field public static final SM_3DPANORAMA:I = 0x7f0a0045

.field public static final SM_3DPANORAMA_description:I = 0x7f0a0052

.field public static final SM_ACTION:I = 0x7f0a0030

.field public static final SM_ADDME:I = 0x7f0a002f

.field public static final SM_AQUA:I = 0x7f0a0047

.field public static final SM_AQUA_description:I = 0x7f0a005b

.field public static final SM_AUTO:I = 0x7f0a003d

.field public static final SM_AUTO_PORTRAIT:I = 0x7f0a003f

.field public static final SM_AUTO_PORTRAIT_description:I = 0x7f0a0058

.field public static final SM_AUTO_burst_off_description:I = 0x7f0a004a

.field public static final SM_AUTO_description:I = 0x7f0a0049

.field public static final SM_BEAUTY:I = 0x7f0a002d

.field public static final SM_BEAUTY_description:I = 0x7f0a004b

.field public static final SM_BEST_FACE:I = 0x7f0a0037

.field public static final SM_BEST_FACE_description:I = 0x7f0a004e

.field public static final SM_BEST_PHOTO:I = 0x7f0a0036

.field public static final SM_BEST_PHOTO_description:I = 0x7f0a004c

.field public static final SM_BUDDY_PHOTOSHARING:I = 0x7f0a0038

.field public static final SM_BURST:I = 0x7f0a0035

.field public static final SM_CARTOON:I = 0x7f0a0032

.field public static final SM_CINEPIC:I = 0x7f0a0046

.field public static final SM_CINEPIC_description:I = 0x7f0a0055

.field public static final SM_CONTINUOUS:I = 0x7f0a0028

.field public static final SM_CONTINUOUS_description:I = 0x7f0a004d

.field public static final SM_COUPLE:I = 0x7f0a0044

.field public static final SM_DRAMA:I = 0x7f0a0042

.field public static final SM_DRAMA_description:I = 0x7f0a0050

.field public static final SM_ERASER:I = 0x7f0a003b

.field public static final SM_ERASER_description:I = 0x7f0a004f

.field public static final SM_FRAME:I = 0x7f0a002c

.field public static final SM_GOLF:I = 0x7f0a0041

.field public static final SM_GOLF_description:I = 0x7f0a0059

.field public static final SM_HDR:I = 0x7f0a0034

.field public static final SM_MANUAL:I = 0x7f0a0040

.field public static final SM_MOSAIC:I = 0x7f0a0029

.field public static final SM_MULTI_FRAME:I = 0x7f0a0039

.field public static final SM_NIGHT:I = 0x7f0a003a

.field public static final SM_NIGHT_description:I = 0x7f0a0056

.field public static final SM_PANORAMA:I = 0x7f0a002a

.field public static final SM_PANORAMA_description:I = 0x7f0a0051

.field public static final SM_PET:I = 0x7f0a003c

.field public static final SM_PET_description:I = 0x7f0a005a

.field public static final SM_RICH_TONE:I = 0x7f0a003e

.field public static final SM_RICH_TONE_description:I = 0x7f0a0053

.field public static final SM_SHARE_MODE:I = 0x7f0a0033

.field public static final SM_SHOOTANDSHARE:I = 0x7f0a0275

.field public static final SM_SINGLE:I = 0x7f0a0027

.field public static final SM_SMILE:I = 0x7f0a002b

.field public static final SM_SPORTS_SCENE_description:I = 0x7f0a0057

.field public static final SM_STOPMOTION:I = 0x7f0a0031

.field public static final SM_STORY:I = 0x7f0a0043

.field public static final SM_STORY_description:I = 0x7f0a0054

.field public static final SM_VINTAGE:I = 0x7f0a002e

.field public static final SPEED_2X_FASTER:I = 0x7f0a01cb

.field public static final SPEED_2X_SLOWER:I = 0x7f0a01ce

.field public static final SPEED_4X_FASTER:I = 0x7f0a01cc

.field public static final SPEED_4X_SLOWER:I = 0x7f0a01cf

.field public static final SPEED_8X_FASTER:I = 0x7f0a01cd

.field public static final SPEED_8X_SLOWER:I = 0x7f0a01d0

.field public static final SPEED_OFF:I = 0x7f0a01ca

.field public static final SS_SOUND1:I = 0x7f0a00da

.field public static final SS_SOUND2:I = 0x7f0a00d9

.field public static final SS_SOUND3:I = 0x7f0a00d8

.field public static final S_DEVICE:I = 0x7f0a00e0

.field public static final S_MMC:I = 0x7f0a00e1

.field public static final S_PHONE:I = 0x7f0a00df

.field public static final SmileShot_message:I = 0x7f0a0152

.field public static final Stop_Motion_Helptext:I = 0x7f0a0140

.field public static final Story_Shot_Helptext:I = 0x7f0a0141

.field public static final TTS_Face_bottom:I = 0x7f0a01ef

.field public static final TTS_Face_close:I = 0x7f0a01ed

.field public static final TTS_Face_detected:I = 0x7f0a01ea

.field public static final TTS_Face_hold:I = 0x7f0a01ec

.field public static final TTS_Face_left:I = 0x7f0a01f0

.field public static final TTS_Face_not_found:I = 0x7f0a01eb

.field public static final TTS_Face_right:I = 0x7f0a01f1

.field public static final TTS_Face_top:I = 0x7f0a01ee

.field public static final T_FIVE:I = 0x7f0a00d6

.field public static final T_TEN:I = 0x7f0a00d7

.field public static final T_THREE:I = 0x7f0a00d5

.field public static final T_TWO:I = 0x7f0a00d4

.field public static final Title_AntiShake:I = 0x7f0a011a

.field public static final Title_AudioRecording:I = 0x7f0a0121

.field public static final Title_AutoContrast:I = 0x7f0a011d

.field public static final Title_Auto_night_detection:I = 0x7f0a0139

.field public static final Title_Back:I = 0x7f0a0126

.field public static final Title_BlinkDetection:I = 0x7f0a011c

.field public static final Title_Brightness:I = 0x7f0a0113

.field public static final Title_CamcorderResolution:I = 0x7f0a0106

.field public static final Title_CameraResolution:I = 0x7f0a0105

.field public static final Title_ChangeMemoryInUseDailog:I = 0x7f0a0154

.field public static final Title_DefaultLayout:I = 0x7f0a0123

.field public static final Title_DeviceList:I = 0x7f0a0107

.field public static final Title_Dual_Camera:I = 0x7f0a0133

.field public static final Title_EffectRecorder:I = 0x7f0a01d7

.field public static final Title_Effects:I = 0x7f0a0109

.field public static final Title_ExposureValue:I = 0x7f0a0114

.field public static final Title_Face_detection:I = 0x7f0a0138

.field public static final Title_Flash:I = 0x7f0a0112

.field public static final Title_Flip:I = 0x7f0a012a

.field public static final Title_Focus:I = 0x7f0a010c

.field public static final Title_FocusMode:I = 0x7f0a0116

.field public static final Title_Frame:I = 0x7f0a0110

.field public static final Title_Front_Camera:I = 0x7f0a0132

.field public static final Title_GPS:I = 0x7f0a011e

.field public static final Title_GroupPlay:I = 0x7f0a0137

.field public static final Title_Guideline:I = 0x7f0a0135

.field public static final Title_HDR:I = 0x7f0a0129

.field public static final Title_HideDualStyles:I = 0x7f0a0215

.field public static final Title_HideEffects:I = 0x7f0a010b

.field public static final Title_ISO:I = 0x7f0a010d

.field public static final Title_ImageQuality:I = 0x7f0a0117

.field public static final Title_ImageViewer:I = 0x7f0a0127

.field public static final Title_InsertMemoryCardDailog:I = 0x7f0a0155

.field public static final Title_Metering:I = 0x7f0a0118

.field public static final Title_Mode:I = 0x7f0a0125

.field public static final Title_Mosaic:I = 0x7f0a010f

.field public static final Title_ObjectTracking:I = 0x7f0a0119

.field public static final Title_Outdoor_Visibility:I = 0x7f0a0128

.field public static final Title_PetSound:I = 0x7f0a012f

.field public static final Title_Rear_Camera:I = 0x7f0a0131

.field public static final Title_RecordingMode:I = 0x7f0a0115

.field public static final Title_Reset:I = 0x7f0a0124

.field public static final Title_ResetDailog:I = 0x7f0a0153

.field public static final Title_Resolution:I = 0x7f0a0276

.field public static final Title_Review:I = 0x7f0a011f

.field public static final Title_Save_Richtone:I = 0x7f0a0136

.field public static final Title_SceneMode:I = 0x7f0a0102

.field public static final Title_Self_Portrait:I = 0x7f0a012b

.field public static final Title_Self_Recording:I = 0x7f0a012c

.field public static final Title_SetUp:I = 0x7f0a0103

.field public static final Title_Share:I = 0x7f0a012e

.field public static final Title_ShootAndShare:I = 0x7f0a0277

.field public static final Title_ShootingMode:I = 0x7f0a0111

.field public static final Title_ShowDualStyles:I = 0x7f0a0214

.field public static final Title_ShowEffects:I = 0x7f0a010a

.field public static final Title_ShutterSound:I = 0x7f0a010e

.field public static final Title_SoundShotMode:I = 0x7f0a0130

.field public static final Title_Storage:I = 0x7f0a0122

.field public static final Title_Timer:I = 0x7f0a0104

.field public static final Title_VideoQuality:I = 0x7f0a0120

.field public static final Title_VideoSpeed:I = 0x7f0a01c9

.field public static final Title_VideoStabilisation:I = 0x7f0a011b

.field public static final Title_Voice_Capture:I = 0x7f0a012d

.field public static final Title_Volume_Key_As:I = 0x7f0a0134

.field public static final Title_WhiteBalance:I = 0x7f0a0108

.field public static final Title_soundshot_effect:I = 0x7f0a0222

.field public static final VM_COOL:I = 0x7f0a005e

.field public static final VM_MONO:I = 0x7f0a005f

.field public static final VM_WARM:I = 0x7f0a005d

.field public static final Video_Stabilisation_Dialog:I = 0x7f0a00e3

.field public static final WB_CLOUDY:I = 0x7f0a0061

.field public static final WB_DAYLIGHT:I = 0x7f0a0062

.field public static final WB_FLUORESCENT:I = 0x7f0a0063

.field public static final WB_HORIZON:I = 0x7f0a0064

.field public static final WB_INCANDESCENT:I = 0x7f0a0066

.field public static final WB_SHADE:I = 0x7f0a0065

.field public static final ZOOM_X_1:I = 0x7f0a0024

.field public static final ZOOM_X_3:I = 0x7f0a0025

.field public static final ZOOM_X_4:I = 0x7f0a0026

.field public static final activate_agree:I = 0x7f0a0197

.field public static final activate_reject:I = 0x7f0a0198

.field public static final add_photo_title:I = 0x7f0a0186

.field public static final animatedphoto_recording_time_unit:I = 0x7f0a0270

.field public static final app_name:I = 0x7f0a0151

.field public static final aquashot_helptext:I = 0x7f0a01c6

.field public static final aquashot_key_select_text:I = 0x7f0a01c5

.field public static final auto_night_detection_not_apply_to_recording:I = 0x7f0a0168

.field public static final battery_low:I = 0x7f0a01b3

.field public static final battery_overheat:I = 0x7f0a000f

.field public static final beautyface_auto:I = 0x7f0a0251

.field public static final beautyface_live_off:I = 0x7f0a0255

.field public static final beautyface_live_on:I = 0x7f0a0254

.field public static final beautyface_manual:I = 0x7f0a0252

.field public static final beautyface_popup:I = 0x7f0a0253

.field public static final best_photo_guidetext:I = 0x7f0a01b7

.field public static final bestgroup_title:I = 0x7f0a01b2

.field public static final bestpic_off:I = 0x7f0a01b1

.field public static final bestpic_on:I = 0x7f0a01b0

.field public static final buddy_photo_share_helptext:I = 0x7f0a01b4

.field public static final buffer_overflow_error_msg:I = 0x7f0a0018

.field public static final burst_shot_guidetext:I = 0x7f0a01b6

.field public static final burst_shot_storage_guidetext:I = 0x7f0a01bb

.field public static final burstshot_turn_on_helptext:I = 0x7f0a024f

.field public static final camcorder_label:I = 0x7f0a0002

.field public static final camera_key:I = 0x7f0a022d

.field public static final camera_label:I = 0x7f0a0001

.field public static final camera_security_enter:I = 0x7f0a0162

.field public static final camera_security_exit:I = 0x7f0a0163

.field public static final camera_set:I = 0x7f0a019d

.field public static final camera_share:I = 0x7f0a019a

.field public static final camera_toss:I = 0x7f0a019c

.field public static final cancel:I = 0x7f0a0150

.field public static final cannot_record:I = 0x7f0a0167

.field public static final cannot_start_camcorder_msg:I = 0x7f0a0020

.field public static final cannot_start_camera_msg:I = 0x7f0a001f

.field public static final cannot_write_file:I = 0x7f0a0164

.field public static final capture_failed_msg:I = 0x7f0a0016

.field public static final change_to_card_memory:I = 0x7f0a0008

.field public static final change_to_device_memory:I = 0x7f0a000a

.field public static final change_to_phone_memory:I = 0x7f0a0009

.field public static final chaton_install:I = 0x7f0a01c8

.field public static final chaton_logout_state:I = 0x7f0a01c7

.field public static final cinemaphoto_shot_guidetext:I = 0x7f0a01ba

.field public static final close:I = 0x7f0a001d

.field public static final close_camera_for_unmounted_memory:I = 0x7f0a015a

.field public static final close_camera_when_media_mounted:I = 0x7f0a0159

.field public static final close_camera_when_mmc_inserted:I = 0x7f0a0158

.field public static final confirm_delete_title:I = 0x7f0a01a2

.field public static final context_menu_download:I = 0x7f0a0170

.field public static final context_menu_edit:I = 0x7f0a016b

.field public static final context_menu_effect:I = 0x7f0a016f

.field public static final context_menu_help:I = 0x7f0a016c

.field public static final context_menu_richton:I = 0x7f0a016d

.field public static final context_menu_setting:I = 0x7f0a016e

.field public static final contextual_filename:I = 0x7f0a01e2

.field public static final crop_discard_text:I = 0x7f0a015d

.field public static final crop_gallery_text:I = 0x7f0a015e

.field public static final crop_label:I = 0x7f0a0160

.field public static final crop_save_text:I = 0x7f0a015c

.field public static final default_storage_changed_to_memory_card:I = 0x7f0a016a

.field public static final default_storage_changed_to_phone:I = 0x7f0a0169

.field public static final deviceType:I = 0x7f0a01a3

.field public static final directshare_content_photo_send_completed:I = 0x7f0a0206

.field public static final directshare_content_photo_send_failed:I = 0x7f0a0208

.field public static final directshare_content_video_send_completed:I = 0x7f0a0207

.field public static final directshare_content_video_send_failed:I = 0x7f0a0209

.field public static final directshare_noti_bar_title:I = 0x7f0a0205

.field public static final directshare_photo_upload:I = 0x7f0a0203

.field public static final directshare_toast_photo_send_completed:I = 0x7f0a020b

.field public static final directshare_toast_photo_send_failed:I = 0x7f0a020c

.field public static final directshare_toast_photo_send_image:I = 0x7f0a020a

.field public static final directshare_toast_video_send_completed:I = 0x7f0a020e

.field public static final directshare_toast_video_send_failed:I = 0x7f0a020f

.field public static final directshare_toast_video_send_image:I = 0x7f0a020d

.field public static final directshare_video_upload:I = 0x7f0a0204

.field public static final disable:I = 0x7f0a001e

.field public static final do_not_ask_again:I = 0x7f0a01ac

.field public static final done:I = 0x7f0a0185

.field public static final drama_shot_duidetext:I = 0x7f0a01b8

.field public static final dramashot_error_large_object:I = 0x7f0a026d

.field public static final dramashot_error_multiple_objects:I = 0x7f0a026f

.field public static final dramashot_error_no_moving_object:I = 0x7f0a026e

.field public static final dual:I = 0x7f0a0211

.field public static final dual_off:I = 0x7f0a0212

.field public static final dual_on:I = 0x7f0a0213

.field public static final dual_resolution_info_dialog:I = 0x7f0a01bd

.field public static final dual_video_reach_size_limit:I = 0x7f0a000b

.field public static final editableshortcut_helptext:I = 0x7f0a0266

.field public static final editableshortcut_reset_layout_dialog_msg:I = 0x7f0a0267

.field public static final enable_gps_dialog:I = 0x7f0a01e6

.field public static final eraser_shot_guidetext:I = 0x7f0a01b9

.field public static final ev_0:I = 0x7f0a0175

.field public static final ev_minus_0_5:I = 0x7f0a0176

.field public static final ev_minus_1_0:I = 0x7f0a0177

.field public static final ev_minus_1_5:I = 0x7f0a0178

.field public static final ev_minus_2_0:I = 0x7f0a0179

.field public static final ev_plus_0_5:I = 0x7f0a0174

.field public static final ev_plus_1_0:I = 0x7f0a0173

.field public static final ev_plus_1_5:I = 0x7f0a0172

.field public static final ev_plus_2_0:I = 0x7f0a0171

.field public static final face_detected:I = 0x7f0a01e3

.field public static final face_touched:I = 0x7f0a01e5

.field public static final face_zoom_helptext:I = 0x7f0a01b5

.field public static final faces_detected:I = 0x7f0a01e4

.field public static final fastmotion_speed_x2:I = 0x7f0a01d1

.field public static final fastmotion_speed_x4:I = 0x7f0a01d2

.field public static final fastmotion_speed_x8:I = 0x7f0a01d3

.field public static final gallery:I = 0x7f0a0195

.field public static final general_label:I = 0x7f0a0003

.field public static final golf_text_detecting:I = 0x7f0a01f8

.field public static final golf_text_unable_detect_golfswing:I = 0x7f0a01fa

.field public static final golf_text_unable_portrait:I = 0x7f0a01f9

.field public static final grid:I = 0x7f0a0216

.field public static final help_Anti_shake_detection_item:I = 0x7f0a024b

.field public static final help_Auto_night_detection_item:I = 0x7f0a0248

.field public static final help_Burst_shot_detection_item:I = 0x7f0a0249

.field public static final help_Face_detection_detection_item:I = 0x7f0a024a

.field public static final help_GPS_detection_item:I = 0x7f0a024e

.field public static final help_Guidelines_detection_item:I = 0x7f0a024d

.field public static final help_Review_item:I = 0x7f0a0250

.field public static final help_Video_stabilisation_item:I = 0x7f0a024c

.field public static final help_info:I = 0x7f0a0233

.field public static final help_rec_fast_item:I = 0x7f0a023c

.field public static final help_rec_fast_title:I = 0x7f0a023b

.field public static final help_rec_normal_item:I = 0x7f0a0236

.field public static final help_rec_normal_title:I = 0x7f0a0235

.field public static final help_rec_sharing_item:I = 0x7f0a0238

.field public static final help_rec_sharing_title:I = 0x7f0a0237

.field public static final help_rec_slow_item:I = 0x7f0a023a

.field public static final help_rec_slow_title:I = 0x7f0a0239

.field public static final help_rec_title:I = 0x7f0a0234

.field public static final help_share_buddy_item:I = 0x7f0a0242

.field public static final help_share_buddy_title:I = 0x7f0a0241

.field public static final help_share_chaton_item:I = 0x7f0a0244

.field public static final help_share_chaton_title:I = 0x7f0a0243

.field public static final help_share_direct_item:I = 0x7f0a0246

.field public static final help_share_direct_title:I = 0x7f0a0245

.field public static final help_share_remoteviewfinder_item:I = 0x7f0a0247

.field public static final help_share_shareshot_item:I = 0x7f0a023f

.field public static final help_share_shareshot_item_chn:I = 0x7f0a0240

.field public static final help_share_shareshot_title:I = 0x7f0a023e

.field public static final help_share_title:I = 0x7f0a023d

.field public static final help_text_panorama:I = 0x7f0a014a

.field public static final help_text_panorama_device:I = 0x7f0a014b

.field public static final help_tutorial_camera_mode_1:I = 0x7f0a025f

.field public static final help_tutorial_camera_mode_2:I = 0x7f0a0260

.field public static final help_tutorial_capture_1:I = 0x7f0a0259

.field public static final help_tutorial_capture_2:I = 0x7f0a025a

.field public static final help_tutorial_capture_3:I = 0x7f0a025b

.field public static final help_tutorial_dual_camera_1:I = 0x7f0a0261

.field public static final help_tutorial_dual_camera_2:I = 0x7f0a0262

.field public static final help_tutorial_dual_camera_3:I = 0x7f0a0263

.field public static final help_tutorial_dual_camera_6:I = 0x7f0a0264

.field public static final help_tutorial_dual_camera_8:I = 0x7f0a0265

.field public static final help_tutorial_recording_1:I = 0x7f0a025c

.field public static final help_tutorial_recording_2:I = 0x7f0a025d

.field public static final help_tutorial_recording_3:I = 0x7f0a025e

.field public static final help_voice_rec_item:I = 0x7f0a0232

.field public static final help_voice_rec_title:I = 0x7f0a0231

.field public static final help_voice_take_picture_title:I = 0x7f0a0230

.field public static final help_voice_title:I = 0x7f0a022f

.field public static final history:I = 0x7f0a01a1

.field public static final image_stitching_fail:I = 0x7f0a0021

.field public static final insert_mmc_msg:I = 0x7f0a0006

.field public static final loading_gallery_images:I = 0x7f0a0166

.field public static final low_batt_msg:I = 0x7f0a001c

.field public static final low_database_storage_view_text:I = 0x7f0a0161

.field public static final low_temp_msg:I = 0x7f0a0180

.field public static final media_scanner_running:I = 0x7f0a0157

.field public static final media_server_died_msg:I = 0x7f0a0019

.field public static final message_DefaultLayout:I = 0x7f0a014c

.field public static final message_EULA_GPS:I = 0x7f0a014d

.field public static final message_enable_GPS:I = 0x7f0a014f

.field public static final message_size_limited:I = 0x7f0a015b

.field public static final minus:I = 0x7f0a017b

.field public static final mobileType:I = 0x7f0a01a4

.field public static final name_cannot_empty:I = 0x7f0a0199

.field public static final network_title:I = 0x7f0a0183

.field public static final network_title_chn:I = 0x7f0a0184

.field public static final nfc_connect_view_text:I = 0x7f0a0194

.field public static final no_images_in_gallery:I = 0x7f0a0165

.field public static final not_enough_space:I = 0x7f0a0007

.field public static final not_supported_zoom:I = 0x7f0a017c

.field public static final not_supported_zoom_toast_popup:I = 0x7f0a017d

.field public static final null_string:I = 0x7f0a0000

.field public static final on_device_help_camcorder:I = 0x7f0a01c2

.field public static final on_device_help_editable:I = 0x7f0a01c4

.field public static final on_device_help_focus:I = 0x7f0a01c1

.field public static final on_device_help_self:I = 0x7f0a01c3

.field public static final open:I = 0x7f0a013d

.field public static final open_hw_failed_msg:I = 0x7f0a0015

.field public static final panorama_no_direction:I = 0x7f0a01f7

.field public static final panorama_processing_msg:I = 0x7f0a000e

.field public static final panorama_warning_text_down:I = 0x7f0a01f3

.field public static final panorama_warning_text_left:I = 0x7f0a01f4

.field public static final panorama_warning_text_right:I = 0x7f0a01f5

.field public static final panorama_warning_text_too_fast:I = 0x7f0a01f6

.field public static final panorama_warning_text_up:I = 0x7f0a01f2

.field public static final pause:I = 0x7f0a01e7

.field public static final pcType:I = 0x7f0a01a6

.field public static final petdet_sound_1:I = 0x7f0a0218

.field public static final petdet_sound_2:I = 0x7f0a0219

.field public static final petdet_sound_3:I = 0x7f0a021a

.field public static final petdet_sound_4:I = 0x7f0a021b

.field public static final plugged_low_batt_msg:I = 0x7f0a017f

.field public static final plus:I = 0x7f0a017a

.field public static final preview_file_received_title:I = 0x7f0a0182

.field public static final processing_msg:I = 0x7f0a000d

.field public static final profile_user_default_name:I = 0x7f0a01a7

.field public static final quick_settings:I = 0x7f0a013c

.field public static final record:I = 0x7f0a01ae

.field public static final record_key:I = 0x7f0a022e

.field public static final recording_failed_msg:I = 0x7f0a0017

.field public static final remote_view_finder_launch_gallery_guidetext:I = 0x7f0a01be

.field public static final remote_view_finder_wifi_direct_guidetext:I = 0x7f0a01bf

.field public static final remote_view_finder_wifi_direct_guidetext_chn:I = 0x7f0a01c0

.field public static final reset_dialog_msg:I = 0x7f0a0268

.field public static final reset_dialog_option_msg:I = 0x7f0a0269

.field public static final resume:I = 0x7f0a01e8

.field public static final richtone_and_original:I = 0x7f0a0256

.field public static final richtone_only:I = 0x7f0a0257

.field public static final rvf_not_supported_dialog_msg:I = 0x7f0a026c

.field public static final savingImage:I = 0x7f0a0014

.field public static final self_recording_open_warning_popup:I = 0x7f0a01e1

.field public static final self_shot_open_warning_popup:I = 0x7f0a01e0

.field public static final sendImage:I = 0x7f0a019f

.field public static final sendVideo:I = 0x7f0a019e

.field public static final setImage:I = 0x7f0a01a0

.field public static final settings:I = 0x7f0a001b

.field public static final share_add_member:I = 0x7f0a0202

.field public static final share_as_image_files:I = 0x7f0a0273

.field public static final share_as_video_files:I = 0x7f0a0272

.field public static final share_buddy:I = 0x7f0a01fc

.field public static final share_chaton:I = 0x7f0a01fd

.field public static final share_chaton_edit_button:I = 0x7f0a0210

.field public static final share_direct_share:I = 0x7f0a01fe

.field public static final share_disabled_msg:I = 0x7f0a0274

.field public static final share_remote_viewfinder:I = 0x7f0a01ff

.field public static final share_remote_viewfinder_dialog_text:I = 0x7f0a0200

.field public static final share_remote_viewfinder_dialog_text_chn:I = 0x7f0a0201

.field public static final share_shot:I = 0x7f0a01fb

.field public static final share_soundshot:I = 0x7f0a0271

.field public static final share_with:I = 0x7f0a0193

.field public static final shareshot_no_user:I = 0x7f0a01a8

.field public static final sharing_devices:I = 0x7f0a0192

.field public static final shutter:I = 0x7f0a01ad

.field public static final slowmotion_speed_x1_2:I = 0x7f0a01d4

.field public static final slowmotion_speed_x1_4:I = 0x7f0a01d5

.field public static final slowmotion_speed_x1_8:I = 0x7f0a01d6

.field public static final snap_shot:I = 0x7f0a01e9

.field public static final snap_shot_limitation_dialog:I = 0x7f0a01bc

.field public static final snapshot_saved:I = 0x7f0a01a9

.field public static final soundshot_add_sound_guidetext:I = 0x7f0a022b

.field public static final soundshot_add_voice_guidetext:I = 0x7f0a022a

.field public static final soundshot_auto_guidetext:I = 0x7f0a0229

.field public static final soundshot_mode_add_sound:I = 0x7f0a021f

.field public static final soundshot_mode_add_voice:I = 0x7f0a021e

.field public static final soundshot_mode_auto:I = 0x7f0a021c

.field public static final soundshot_mode_manual:I = 0x7f0a021d

.field public static final soundshot_recording:I = 0x7f0a0220

.field public static final soundshot_recording_time_unit:I = 0x7f0a0221

.field public static final soundshot_sound_effect_1:I = 0x7f0a0223

.field public static final soundshot_sound_effect_2:I = 0x7f0a0224

.field public static final soundshot_sound_effect_3:I = 0x7f0a0225

.field public static final soundshot_sound_effect_4:I = 0x7f0a0226

.field public static final soundshot_sound_effect_5:I = 0x7f0a0227

.field public static final soundshot_sound_effect_OK:I = 0x7f0a0228

.field public static final stms_version:I = 0x7f0a0258

.field public static final stop:I = 0x7f0a01af

.field public static final storage_setting_dialog_message:I = 0x7f0a01ab

.field public static final storage_setting_dialog_title:I = 0x7f0a01aa

.field public static final tabletType:I = 0x7f0a01a5

.field public static final take_photo:I = 0x7f0a0196

.field public static final temperature_too_high_dual_record:I = 0x7f0a0013

.field public static final temperature_too_high_flash_off:I = 0x7f0a0010

.field public static final temperature_too_high_recording_disable:I = 0x7f0a0012

.field public static final temperature_too_high_recording_flash_off:I = 0x7f0a0011

.field public static final unknown_error_callback_msg:I = 0x7f0a001a

.field public static final user_profile_title:I = 0x7f0a0181

.field public static final video_play:I = 0x7f0a019b

.field public static final video_reach_size_limit:I = 0x7f0a000c

.field public static final wait:I = 0x7f0a0004

.field public static final wait_message:I = 0x7f0a026a

.field public static final wallpaper:I = 0x7f0a015f

.field public static final warning_msg:I = 0x7f0a0005

.field public static final wheel:I = 0x7f0a0217

.field public static final wifi_dialog_easy_connect:I = 0x7f0a018f

.field public static final wifi_dialog_manual:I = 0x7f0a0190

.field public static final wifi_dialog_manual_chn:I = 0x7f0a0191

.field public static final wifi_dialog_no:I = 0x7f0a018e

.field public static final wifi_dialog_text:I = 0x7f0a0188

.field public static final wifi_dialog_text_chn:I = 0x7f0a0189

.field public static final wifi_dialog_title:I = 0x7f0a0187

.field public static final wifi_dialog_yes:I = 0x7f0a018d

.field public static final wifi_direct_dialog_text:I = 0x7f0a018b

.field public static final wifi_direct_dialog_text_chn:I = 0x7f0a018c

.field public static final wifi_direct_dialog_title:I = 0x7f0a018a

.field public static final wifi_display_disconnect_dialog_msg:I = 0x7f0a026b

.field public static final zoom_key:I = 0x7f0a022c

.field public static final zoom_not_available:I = 0x7f0a017e


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3482
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
