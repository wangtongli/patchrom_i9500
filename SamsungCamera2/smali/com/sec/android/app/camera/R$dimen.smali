.class public final Lcom/sec/android/app/camera/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/camera/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final HDR_button_icon_height:I = 0x7f080440

.field public static final HDR_button_icon_pos_x_0:I = 0x7f080441

.field public static final HDR_button_icon_pos_x_180:I = 0x7f080443

.field public static final HDR_button_icon_pos_x_270:I = 0x7f080444

.field public static final HDR_button_icon_pos_x_90:I = 0x7f080442

.field public static final HDR_button_icon_pos_y_0:I = 0x7f080445

.field public static final HDR_button_icon_pos_y_180:I = 0x7f080447

.field public static final HDR_button_icon_pos_y_270:I = 0x7f080448

.field public static final HDR_button_icon_pos_y_90:I = 0x7f080446

.field public static final HDR_button_icon_width:I = 0x7f08043f

.field public static final HDR_button_text_size:I = 0x7f080449

.field public static final abstract_reset_popup_background_height:I = 0x7f080321

.field public static final abstract_reset_popup_background_width:I = 0x7f080320

.field public static final actionshot_capture_progressbar_alpha:I = 0x7f08037b

.field public static final actionshot_capture_progressbar_height:I = 0x7f08037a

.field public static final actionshot_capture_progressbar_setsize_width_0:I = 0x7f08037c

.field public static final actionshot_capture_progressbar_setsize_width_90:I = 0x7f08037d

.field public static final actionshot_capture_progressbar_width:I = 0x7f080379

.field public static final actionshot_capture_progressbar_x_0:I = 0x7f080371

.field public static final actionshot_capture_progressbar_x_180:I = 0x7f080373

.field public static final actionshot_capture_progressbar_x_270:I = 0x7f080374

.field public static final actionshot_capture_progressbar_x_90:I = 0x7f080372

.field public static final actionshot_capture_progressbar_y_0:I = 0x7f080375

.field public static final actionshot_capture_progressbar_y_180:I = 0x7f080377

.field public static final actionshot_capture_progressbar_y_270:I = 0x7f080378

.field public static final actionshot_capture_progressbar_y_90:I = 0x7f080376

.field public static final actionshot_help_text_height:I = 0x7f080387

.field public static final actionshot_help_text_width:I = 0x7f080386

.field public static final actionshot_help_text_x_0:I = 0x7f08037e

.field public static final actionshot_help_text_x_180:I = 0x7f080380

.field public static final actionshot_help_text_x_270:I = 0x7f080381

.field public static final actionshot_help_text_x_90:I = 0x7f08037f

.field public static final actionshot_help_text_y_0:I = 0x7f080382

.field public static final actionshot_help_text_y_180:I = 0x7f080384

.field public static final actionshot_help_text_y_270:I = 0x7f080385

.field public static final actionshot_help_text_y_90:I = 0x7f080383

.field public static final actionshot_post_capture_progressbar_height:I = 0x7f080370

.field public static final actionshot_post_capture_progressbar_width:I = 0x7f08036f

.field public static final actionshot_progress_text_x:I = 0x7f08036d

.field public static final actionshot_progress_text_y:I = 0x7f08036e

.field public static final actionshot_rect_height:I = 0x7f08038b

.field public static final actionshot_rect_pos_x:I = 0x7f080388

.field public static final actionshot_rect_pos_y:I = 0x7f080389

.field public static final actionshot_rect_width:I = 0x7f08038a

.field public static final actionshot_sidemenu_width:I = 0x7f08038c

.field public static final af_focus_solid_rect_height:I = 0x7f080079

.field public static final af_focus_solid_rect_width:I = 0x7f080078

.field public static final af_touch_rect_width:I = 0x7f08036a

.field public static final autoportrait_detect_area_base_pos_x:I = 0x7f080182

.field public static final autoportrait_detect_area_base_pos_y:I = 0x7f080183

.field public static final autoportrait_detect_area_maximum_height:I = 0x7f08017f

.field public static final autoportrait_detect_area_maximum_width:I = 0x7f08017e

.field public static final autoportrait_detect_area_minimum_height:I = 0x7f080181

.field public static final autoportrait_detect_area_minimum_width:I = 0x7f080180

.field public static final autoportrait_handler_width:I = 0x7f080184

.field public static final baseindicator_battery_icon_pos_x:I = 0x7f080080

.field public static final baseindicator_battery_pos_x:I = 0x7f08007f

.field public static final baseindicator_drama_help_text_add_height:I = 0x7f0800b2

.field public static final baseindicator_drama_help_text_top_margin:I = 0x7f0800b3

.field public static final baseindicator_eqbar_band_0_pos_x:I = 0x7f0800a4

.field public static final baseindicator_eqbar_band_10_pos_x:I = 0x7f080326

.field public static final baseindicator_eqbar_band_1_pos_x:I = 0x7f0800a5

.field public static final baseindicator_eqbar_band_2_pos_x:I = 0x7f0800a6

.field public static final baseindicator_eqbar_band_3_pos_x:I = 0x7f0800a7

.field public static final baseindicator_eqbar_band_4_pos_x:I = 0x7f0800a8

.field public static final baseindicator_eqbar_band_5_pos_x:I = 0x7f0800a9

.field public static final baseindicator_eqbar_band_6_pos_x:I = 0x7f0800aa

.field public static final baseindicator_eqbar_band_7_pos_x:I = 0x7f0800ab

.field public static final baseindicator_eqbar_band_8_pos_x:I = 0x7f080324

.field public static final baseindicator_eqbar_band_9_pos_x:I = 0x7f080325

.field public static final baseindicator_eqbar_band_height:I = 0x7f0800a2

.field public static final baseindicator_eqbar_band_padding:I = 0x7f0800a3

.field public static final baseindicator_eqbar_band_width:I = 0x7f0800a1

.field public static final baseindicator_eqbar_group_pos_x_0:I = 0x7f080096

.field public static final baseindicator_eqbar_group_pos_x_180:I = 0x7f08009a

.field public static final baseindicator_eqbar_group_pos_x_270:I = 0x7f08009c

.field public static final baseindicator_eqbar_group_pos_x_90:I = 0x7f080098

.field public static final baseindicator_eqbar_group_pos_y_0:I = 0x7f080097

.field public static final baseindicator_eqbar_group_pos_y_180:I = 0x7f08009b

.field public static final baseindicator_eqbar_group_pos_y_270:I = 0x7f08009d

.field public static final baseindicator_eqbar_group_pos_y_90:I = 0x7f080099

.field public static final baseindicator_eqbar_height:I = 0x7f0800a0

.field public static final baseindicator_eqbar_mic_x:I = 0x7f0800ac

.field public static final baseindicator_eqbar_mic_y:I = 0x7f0800ad

.field public static final baseindicator_eqbar_pos_x:I = 0x7f080323

.field public static final baseindicator_eqbar_pos_y:I = 0x7f08009e

.field public static final baseindicator_eqbar_text_height:I = 0x7f0800b0

.field public static final baseindicator_eqbar_text_pos_x:I = 0x7f0800ae

.field public static final baseindicator_eqbar_text_size:I = 0x7f0800b1

.field public static final baseindicator_eqbar_text_width:I = 0x7f0800af

.field public static final baseindicator_eqbar_width:I = 0x7f08009f

.field public static final baseindicator_group_height:I = 0x7f08007d

.field public static final baseindicator_group_landscape_pos_x:I = 0x7f08007a

.field public static final baseindicator_group_landscape_pos_y:I = 0x7f08007b

.field public static final baseindicator_group_portrait_x_offset_from_right:I = 0x7f080488

.field public static final baseindicator_group_width:I = 0x7f08007c

.field public static final baseindicator_help_text_0_top_margin:I = 0x7f0800b4

.field public static final baseindicator_help_text_180_top_margin:I = 0x7f0800b5

.field public static final baseindicator_percentage_width:I = 0x7f08048c

.field public static final baseindicator_recording_amp_height:I = 0x7f08008c

.field public static final baseindicator_recording_amp_pos_x:I = 0x7f080089

.field public static final baseindicator_recording_amp_pos_y:I = 0x7f08008a

.field public static final baseindicator_recording_amp_width:I = 0x7f08008b

.field public static final baseindicator_remain_counter_width:I = 0x7f08007e

.field public static final baseindicator_shootingmode_pos_x:I = 0x7f080081

.field public static final baseindicator_shootingmode_width:I = 0x7f080082

.field public static final baseindicator_shootingmode_x_180:I = 0x7f080085

.field public static final baseindicator_shootingmode_x_270:I = 0x7f080087

.field public static final baseindicator_shootingmode_x_90:I = 0x7f080083

.field public static final baseindicator_shootingmode_y_180:I = 0x7f080086

.field public static final baseindicator_shootingmode_y_270:I = 0x7f080088

.field public static final baseindicator_shootingmode_y_90:I = 0x7f080084

.field public static final basemenu_live_beauty_face_button_pos_x:I = 0x7f08002b

.field public static final basemenu_live_beauty_face_button_pos_y:I = 0x7f08002c

.field public static final basemenu_live_beauty_face_button_size:I = 0x7f08002d

.field public static final basemenu_thumbnail_button_pos_x:I = 0x7f080023

.field public static final basemenu_thumbnail_button_pos_y:I = 0x7f080024

.field public static final batteryindicator_battery_height:I = 0x7f0802f1

.field public static final batteryindicator_battery_text_height:I = 0x7f0802f2

.field public static final batteryindicator_battery_text_width:I = 0x7f0802f3

.field public static final best_pic_icon_height:I = 0x7f08042e

.field public static final best_pic_icon_pos_x_0:I = 0x7f08042f

.field public static final best_pic_icon_pos_x_180:I = 0x7f080431

.field public static final best_pic_icon_pos_x_270:I = 0x7f080432

.field public static final best_pic_icon_pos_x_90:I = 0x7f080430

.field public static final best_pic_icon_pos_y_0:I = 0x7f080433

.field public static final best_pic_icon_pos_y_180:I = 0x7f080435

.field public static final best_pic_icon_pos_y_270:I = 0x7f080436

.field public static final best_pic_icon_pos_y_90:I = 0x7f080434

.field public static final best_pic_icon_text:I = 0x7f08042c

.field public static final best_pic_icon_width:I = 0x7f08042d

.field public static final best_pic_text_pos_x_0:I = 0x7f080437

.field public static final best_pic_text_pos_x_180:I = 0x7f080439

.field public static final best_pic_text_pos_x_270:I = 0x7f08043a

.field public static final best_pic_text_pos_x_90:I = 0x7f080438

.field public static final best_pic_text_pos_y_0:I = 0x7f08043b

.field public static final best_pic_text_pos_y_180:I = 0x7f08043d

.field public static final best_pic_text_pos_y_270:I = 0x7f08043e

.field public static final best_pic_text_pos_y_90:I = 0x7f08043c

.field public static final burst_panoramashot_arrow_start_offset:I = 0x7f0804cf

.field public static final burst_panoramashot_capturetri_height:I = 0x7f0804b6

.field public static final burst_panoramashot_capturetri_width:I = 0x7f0804b5

.field public static final burst_panoramashot_guide_text_height:I = 0x7f08049e

.field public static final burst_panoramashot_guide_text_margin:I = 0x7f08049f

.field public static final burst_panoramashot_guide_text_width:I = 0x7f08049d

.field public static final burst_panoramashot_help_text_height:I = 0x7f0804a4

.field public static final burst_panoramashot_help_text_width:I = 0x7f0804a3

.field public static final burst_panoramashot_init_background_landscape_rect_height:I = 0x7f0804ba

.field public static final burst_panoramashot_init_background_landscape_rect_width:I = 0x7f0804b9

.field public static final burst_panoramashot_init_background_landscape_rect_x:I = 0x7f0804b7

.field public static final burst_panoramashot_init_background_landscape_rect_y:I = 0x7f0804b8

.field public static final burst_panoramashot_init_background_portrait_rect_height:I = 0x7f0804be

.field public static final burst_panoramashot_init_background_portrait_rect_width:I = 0x7f0804bd

.field public static final burst_panoramashot_init_background_portrait_rect_x:I = 0x7f0804bb

.field public static final burst_panoramashot_init_background_portrait_rect_y:I = 0x7f0804bc

.field public static final burst_panoramashot_init_preview_landscape_rect_height:I = 0x7f0804c2

.field public static final burst_panoramashot_init_preview_landscape_rect_width:I = 0x7f0804c1

.field public static final burst_panoramashot_init_preview_landscape_rect_x:I = 0x7f0804bf

.field public static final burst_panoramashot_init_preview_landscape_rect_y:I = 0x7f0804c0

.field public static final burst_panoramashot_init_preview_portrait_rect_height:I = 0x7f0804c6

.field public static final burst_panoramashot_init_preview_portrait_rect_width:I = 0x7f0804c5

.field public static final burst_panoramashot_init_preview_portrait_rect_x:I = 0x7f0804c3

.field public static final burst_panoramashot_init_preview_portrait_rect_y:I = 0x7f0804c4

.field public static final burst_panoramashot_livepreview_landscape_height:I = 0x7f0804a6

.field public static final burst_panoramashot_livepreview_landscape_width:I = 0x7f0804a5

.field public static final burst_panoramashot_livepreview_landscape_x:I = 0x7f0804a7

.field public static final burst_panoramashot_livepreview_landscape_y:I = 0x7f0804a8

.field public static final burst_panoramashot_livepreview_portrait_height:I = 0x7f0804aa

.field public static final burst_panoramashot_livepreview_portrait_single_height:I = 0x7f08052d

.field public static final burst_panoramashot_livepreview_portrait_single_width:I = 0x7f08052c

.field public static final burst_panoramashot_livepreview_portrait_width:I = 0x7f0804a9

.field public static final burst_panoramashot_livepreview_portrait_x:I = 0x7f0804ab

.field public static final burst_panoramashot_livepreview_portrait_y:I = 0x7f0804ac

.field public static final burst_panoramashot_livepreview_rect_thickness:I = 0x7f0804b4

.field public static final burst_panoramashot_livepreview_single_height:I = 0x7f0804af

.field public static final burst_panoramashot_livepreview_single_portrait_height:I = 0x7f0804b3

.field public static final burst_panoramashot_livepreview_single_portrait_width:I = 0x7f0804b2

.field public static final burst_panoramashot_livepreview_single_portrait_x:I = 0x7f0804b0

.field public static final burst_panoramashot_livepreview_single_portrait_y:I = 0x7f0804b1

.field public static final burst_panoramashot_livepreview_single_width:I = 0x7f0804ae

.field public static final burst_panoramashot_livepreview_single_x:I = 0x7f0804ad

.field public static final burst_panoramashot_livepreview_tri_offset:I = 0x7f0804d0

.field public static final burst_panoramashot_progress_text_height:I = 0x7f0804d1

.field public static final burst_panoramashot_progressbar_text_height:I = 0x7f0804ca

.field public static final burst_panoramashot_progressbar_text_width:I = 0x7f0804c9

.field public static final burst_panoramashot_progressbar_text_x:I = 0x7f0804c7

.field public static final burst_panoramashot_progressbar_text_y:I = 0x7f0804c8

.field public static final burst_panoramashot_rect_height:I = 0x7f0804a1

.field public static final burst_panoramashot_rect_margin:I = 0x7f0804a2

.field public static final burst_panoramashot_rect_width:I = 0x7f0804a0

.field public static final burst_panoramashot_root_shift:I = 0x7f080492

.field public static final burst_panoramashot_stopbutton_x:I = 0x7f0804cb

.field public static final burst_panoramashot_stopbutton_y:I = 0x7f0804cc

.field public static final burst_panoramashot_tri_height:I = 0x7f080226

.field public static final burst_panoramashot_tri_height_width:I = 0x7f08049c

.field public static final burst_panoramashot_tri_pos_x_0:I = 0x7f080493

.field public static final burst_panoramashot_tri_pos_x_180:I = 0x7f080495

.field public static final burst_panoramashot_tri_pos_x_270:I = 0x7f080496

.field public static final burst_panoramashot_tri_pos_x_90:I = 0x7f080494

.field public static final burst_panoramashot_tri_pos_y_0:I = 0x7f080497

.field public static final burst_panoramashot_tri_pos_y_180:I = 0x7f080499

.field public static final burst_panoramashot_tri_pos_y_270:I = 0x7f08049a

.field public static final burst_panoramashot_tri_pos_y_90:I = 0x7f080498

.field public static final burst_panoramashot_tri_width:I = 0x7f080225

.field public static final burst_panoramashot_tri_width_height:I = 0x7f08049b

.field public static final burst_panoramashot_viewfinder_destination_height:I = 0x7f080532

.field public static final burst_panoramashot_viewfinder_destination_width:I = 0x7f080531

.field public static final burst_panoramashot_viewfinder_destination_x:I = 0x7f08052f

.field public static final burst_panoramashot_viewfinder_destination_y:I = 0x7f080530

.field public static final burst_panoramashot_viewfinder_thickness:I = 0x7f08052e

.field public static final burst_panoramashot_warning_arrow_offset:I = 0x7f0804ce

.field public static final burst_panoramashot_warning_arrow_swing_distance:I = 0x7f0804cd

.field public static final burstshot_help_text_x_0:I = 0x7f080408

.field public static final burstshot_help_text_x_180:I = 0x7f08040a

.field public static final burstshot_help_text_x_270:I = 0x7f08040b

.field public static final burstshot_help_text_x_90:I = 0x7f080409

.field public static final burstshot_help_text_y_0:I = 0x7f08040c

.field public static final burstshot_help_text_y_180:I = 0x7f08040e

.field public static final burstshot_help_text_y_270:I = 0x7f08040f

.field public static final burstshot_help_text_y_90:I = 0x7f08040d

.field public static final burstshot_post_progressbar_height:I = 0x7f080419

.field public static final burstshot_post_progressbar_pos_x_0:I = 0x7f080410

.field public static final burstshot_post_progressbar_pos_x_180:I = 0x7f080412

.field public static final burstshot_post_progressbar_pos_x_270:I = 0x7f080413

.field public static final burstshot_post_progressbar_pos_x_90:I = 0x7f080411

.field public static final burstshot_post_progressbar_pos_y_0:I = 0x7f080414

.field public static final burstshot_post_progressbar_pos_y_180:I = 0x7f080416

.field public static final burstshot_post_progressbar_pos_y_270:I = 0x7f080417

.field public static final burstshot_post_progressbar_pos_y_90:I = 0x7f080415

.field public static final burstshot_post_progressbar_width:I = 0x7f080418

.field public static final burstshot_postcapture_progressbar_pos_x_0:I = 0x7f080428

.field public static final burstshot_postcapture_progressbar_pos_x_90:I = 0x7f080429

.field public static final burstshot_postcapture_progressbar_pos_y_0:I = 0x7f08042a

.field public static final burstshot_postcapture_progressbar_pos_y_90:I = 0x7f08042b

.field public static final burstshot_progressbar_pos_x:I = 0x7f080426

.field public static final burstshot_progressbar_pos_y:I = 0x7f080427

.field public static final burstshot_progressbar_text_x:I = 0x7f080424

.field public static final burstshot_progressbar_text_y:I = 0x7f080425

.field public static final burstshot_result_text_height:I = 0x7f080423

.field public static final burstshot_result_text_pos_x_0:I = 0x7f08041a

.field public static final burstshot_result_text_pos_x_180:I = 0x7f08041c

.field public static final burstshot_result_text_pos_x_270:I = 0x7f08041d

.field public static final burstshot_result_text_pos_x_90:I = 0x7f08041b

.field public static final burstshot_result_text_pos_y_0:I = 0x7f08041e

.field public static final burstshot_result_text_pos_y_180:I = 0x7f080420

.field public static final burstshot_result_text_pos_y_270:I = 0x7f080421

.field public static final burstshot_result_text_pos_y_90:I = 0x7f08041f

.field public static final burstshot_result_text_width:I = 0x7f080422

.field public static final camcorder_baseindicator_indicator_group_padding:I = 0x7f08048b

.field public static final camcorder_baseindicator_remain_counter_padding:I = 0x7f08048a

.field public static final camcorder_baseindicator_remain_counter_width:I = 0x7f080489

.field public static final camcorder_recording_bottom_menu_bar:I = 0x7f080355

.field public static final camcorder_recording_menu_margin:I = 0x7f080354

.field public static final camcorder_screen_height_1280x720_record:I = 0x7f080331

.field public static final camcorder_screen_height_176x144_record:I = 0x7f08033b

.field public static final camcorder_screen_height_1920x1080_record:I = 0x7f08032f

.field public static final camcorder_screen_height_320x240_record:I = 0x7f080339

.field public static final camcorder_screen_height_640x480_record:I = 0x7f080337

.field public static final camcorder_screen_height_720x480_record:I = 0x7f080335

.field public static final camcorder_screen_height_800x480_record:I = 0x7f080333

.field public static final camcorder_screen_width_1280x720_record:I = 0x7f080330

.field public static final camcorder_screen_width_176x144_record:I = 0x7f08033a

.field public static final camcorder_screen_width_1920x1080_record:I = 0x7f08032e

.field public static final camcorder_screen_width_320x240_record:I = 0x7f080338

.field public static final camcorder_screen_width_640x480_record:I = 0x7f080336

.field public static final camcorder_screen_width_720x480_record:I = 0x7f080334

.field public static final camcorder_screen_width_800x480_record:I = 0x7f080332

.field public static final camcorder_video_thumbnail_height:I = 0x7f08033d

.field public static final camcorder_video_thumbnail_width:I = 0x7f08033c

.field public static final camera_crop_outline_width:I = 0x7f08022d

.field public static final camera_nfc_connecting_cancel_btn_height:I = 0x7f0804e0

.field public static final camera_nfc_connecting_cancel_btn_width:I = 0x7f0804df

.field public static final camera_nfc_connecting_cancel_btn_x:I = 0x7f0804dd

.field public static final camera_nfc_connecting_cancel_btn_y:I = 0x7f0804de

.field public static final camera_nfc_connecting_image_height:I = 0x7f0804d8

.field public static final camera_nfc_connecting_image_width:I = 0x7f0804d7

.field public static final camera_nfc_connecting_image_x:I = 0x7f0804d5

.field public static final camera_nfc_connecting_image_y:I = 0x7f0804d6

.field public static final camera_nfc_connecting_meesage_height:I = 0x7f0804dc

.field public static final camera_nfc_connecting_meesage_width:I = 0x7f0804db

.field public static final camera_nfc_connecting_meesage_x:I = 0x7f0804d9

.field public static final camera_nfc_connecting_meesage_y:I = 0x7f0804da

.field public static final cameraindicator_indicator_width:I = 0x7f080491

.field public static final cameraindicator_landscape_pos_x:I = 0x7f08048f

.field public static final cameraindicator_landscape_pos_y:I = 0x7f080490

.field public static final cameraindicator_portrait_pos_x:I = 0x7f08048d

.field public static final cameraindicator_portrait_pos_y:I = 0x7f08048e

.field public static final capture_help_text_alignment:I = 0x7f0803eb

.field public static final capture_help_text_height:I = 0x7f0803e2

.field public static final capture_help_text_width:I = 0x7f0803e1

.field public static final capture_help_text_x_0:I = 0x7f0803e3

.field public static final capture_help_text_x_180:I = 0x7f0803e5

.field public static final capture_help_text_x_270:I = 0x7f0803e6

.field public static final capture_help_text_x_90:I = 0x7f0803e4

.field public static final capture_help_text_y_0:I = 0x7f0803e7

.field public static final capture_help_text_y_180:I = 0x7f0803e9

.field public static final capture_help_text_y_270:I = 0x7f0803ea

.field public static final capture_help_text_y_90:I = 0x7f0803e8

.field public static final capture_popup_group_height:I = 0x7f0803c8

.field public static final capture_popup_group_width:I = 0x7f0803c7

.field public static final capture_popup_group_x:I = 0x7f0803c5

.field public static final capture_popup_group_y:I = 0x7f0803c6

.field public static final capture_progress_height:I = 0x7f0803cd

.field public static final capture_progressbar_bottom_padding_landscape:I = 0x7f0802cb

.field public static final capture_progressbar_bottom_padding_portrait:I = 0x7f0802cc

.field public static final capture_progressbar_height:I = 0x7f0803cc

.field public static final capture_progressbar_pos_x:I = 0x7f0803d6

.field public static final capture_progressbar_pos_y:I = 0x7f0803d7

.field public static final capture_progressbar_step_gap:I = 0x7f0802ce

.field public static final capture_progressbar_step_width:I = 0x7f0802cd

.field public static final capture_progressbar_text_bottom_padding:I = 0x7f0802d0

.field public static final capture_progressbar_text_height:I = 0x7f0802cf

.field public static final capture_progressbar_text_interval:I = 0x7f0803d8

.field public static final capture_progressbar_text_width:I = 0x7f0803dc

.field public static final capture_progressbar_text_x:I = 0x7f0803d9

.field public static final capture_progressbar_text_y:I = 0x7f0803db

.field public static final capture_progressbar_width:I = 0x7f0803cb

.field public static final capture_progressbar_width_l:I = 0x7f0803c9

.field public static final capture_progressbar_width_p:I = 0x7f0803ca

.field public static final capture_progressbar_x_0:I = 0x7f0803ce

.field public static final capture_progressbar_x_180:I = 0x7f0803d0

.field public static final capture_progressbar_x_270:I = 0x7f0803d1

.field public static final capture_progressbar_x_90:I = 0x7f0803cf

.field public static final capture_progressbar_y_0:I = 0x7f0803d2

.field public static final capture_progressbar_y_180:I = 0x7f0803d4

.field public static final capture_progressbar_y_270:I = 0x7f0803d5

.field public static final capture_progressbar_y_90:I = 0x7f0803d3

.field public static final capture_stop_button_height:I = 0x7f0802df

.field public static final capture_stop_button_width:I = 0x7f0802de

.field public static final capture_stop_button_x:I = 0x7f0802dc

.field public static final capture_stop_button_y:I = 0x7f0802dd

.field public static final checkbox_pos_y:I = 0x7f080486

.field public static final checkbox_x_offset_from_right:I = 0x7f080487

.field public static final contextmenu_edit_button_pos_x:I = 0x7f0803a5

.field public static final contextmenu_edit_button_pos_y:I = 0x7f0803a6

.field public static final contextmenu_edit_button_resource_width:I = 0x7f0803a7

.field public static final contextmenu_edit_landscape_button_resource_pos_x:I = 0x7f0803aa

.field public static final contextmenu_edit_landscape_button_resource_pos_y:I = 0x7f0803ab

.field public static final contextmenu_edit_landscape_text_pos_x:I = 0x7f0803ac

.field public static final contextmenu_edit_landscape_text_pos_y:I = 0x7f0803ad

.field public static final contextmenu_edit_portrait_button_resource_pos_x:I = 0x7f0803ae

.field public static final contextmenu_edit_portrait_button_resource_pos_y:I = 0x7f0803af

.field public static final contextmenu_edit_portrait_text_pos_x:I = 0x7f0803b0

.field public static final contextmenu_edit_portrait_text_pos_y:I = 0x7f0803b1

.field public static final contextmenu_edit_text_height:I = 0x7f0803a9

.field public static final contextmenu_edit_text_width:I = 0x7f0803a8

.field public static final contextmenu_icon_pos_x:I = 0x7f0802ec

.field public static final contextmenu_icon_pos_y:I = 0x7f0802ed

.field public static final contextmenu_item_height:I = 0x7f0802e9

.field public static final contextmenu_item_text_height:I = 0x7f0802eb

.field public static final contextmenu_item_text_pos_x:I = 0x7f0802ee

.field public static final contextmenu_item_text_pos_y:I = 0x7f0802ef

.field public static final contextmenu_item_text_width:I = 0x7f0802ea

.field public static final contextmenu_item_width:I = 0x7f0802e8

.field public static final contextmenu_landscape_height:I = 0x7f0803a0

.field public static final contextmenu_landscape_pos_x:I = 0x7f08039d

.field public static final contextmenu_landscape_pos_y:I = 0x7f08039e

.field public static final contextmenu_landscape_width:I = 0x7f08039f

.field public static final contextmenu_portrait_height:I = 0x7f0803a4

.field public static final contextmenu_portrait_pos_x:I = 0x7f0803a1

.field public static final contextmenu_portrait_pos_y:I = 0x7f0803a2

.field public static final contextmenu_portrait_width:I = 0x7f0803a3

.field public static final contextmenu_separator_height:I = 0x7f0802f0

.field public static final cropimage_land_button_height:I = 0x7f08036c

.field public static final cropimage_port_button_height:I = 0x7f08036b

.field public static final devicelist_menu_height:I = 0x7f0804fe

.field public static final devicelist_menu_list_height:I = 0x7f0804ff

.field public static final devicelist_menu_width:I = 0x7f0804fd

.field public static final dialogpopup_button_height:I = 0x7f0803b3

.field public static final dialogpopup_button_padding_x:I = 0x7f0803b4

.field public static final dialogpopup_button_padding_y:I = 0x7f0803b5

.field public static final dialogpopup_button_width:I = 0x7f0803b2

.field public static final dialogpopup_float_25:I = 0x7f0803ba

.field public static final dialogpopup_menu_portrait_pos_x:I = 0x7f0803b8

.field public static final dialogpopup_message_area_height:I = 0x7f0803b9

.field public static final dialogpopup_message_area_pos_x:I = 0x7f0803b6

.field public static final dialogpopup_message_area_pos_y:I = 0x7f0803b7

.field public static final distance_swipe_to_go_gallery:I = 0x7f0804d3

.field public static final dramashot_capture_progressbar_x_0:I = 0x7f0804e1

.field public static final dramashot_capture_progressbar_x_180:I = 0x7f0804e3

.field public static final dramashot_capture_progressbar_x_270:I = 0x7f0804e4

.field public static final dramashot_capture_progressbar_x_90:I = 0x7f0804e2

.field public static final dramashot_capture_progressbar_y_0:I = 0x7f0804e5

.field public static final dramashot_capture_progressbar_y_180:I = 0x7f0804e7

.field public static final dramashot_capture_progressbar_y_270:I = 0x7f0804e8

.field public static final dramashot_capture_progressbar_y_90:I = 0x7f0804e6

.field public static final dramashot_help_text_alignment:I = 0x7f08053d

.field public static final dramashot_help_text_height:I = 0x7f080534

.field public static final dramashot_help_text_width:I = 0x7f080533

.field public static final dramashot_help_text_x_0:I = 0x7f080535

.field public static final dramashot_help_text_x_180:I = 0x7f080537

.field public static final dramashot_help_text_x_270:I = 0x7f080538

.field public static final dramashot_help_text_x_90:I = 0x7f080536

.field public static final dramashot_help_text_y_0:I = 0x7f080539

.field public static final dramashot_help_text_y_180:I = 0x7f08053b

.field public static final dramashot_help_text_y_270:I = 0x7f08053c

.field public static final dramashot_help_text_y_90:I = 0x7f08053a

.field public static final dramashot_progressbar_width_l:I = 0x7f0804e9

.field public static final dramashot_progressbar_width_p:I = 0x7f0804ea

.field public static final dualrecording_time_text_width:I = 0x7f0800d5

.field public static final dualshot_circlelens_default:I = 0x7f08024a

.field public static final dualshot_circlelens_height:I = 0x7f080249

.field public static final dualshot_circlelens_width:I = 0x7f080248

.field public static final dualshot_cubisim_default:I = 0x7f080235

.field public static final dualshot_cubisim_height:I = 0x7f080234

.field public static final dualshot_cubisim_width:I = 0x7f080233

.field public static final dualshot_exposure_height:I = 0x7f080254

.field public static final dualshot_exposure_start_posx:I = 0x7f080251

.field public static final dualshot_exposure_start_posy:I = 0x7f080252

.field public static final dualshot_exposure_width:I = 0x7f080253

.field public static final dualshot_heart_default:I = 0x7f080244

.field public static final dualshot_heart_height:I = 0x7f080243

.field public static final dualshot_heart_width:I = 0x7f080242

.field public static final dualshot_normal_default:I = 0x7f080232

.field public static final dualshot_normal_height:I = 0x7f080231

.field public static final dualshot_normal_width:I = 0x7f080230

.field public static final dualshot_ovalblur_default:I = 0x7f08023e

.field public static final dualshot_ovalblur_height:I = 0x7f08023d

.field public static final dualshot_ovalblur_width:I = 0x7f08023c

.field public static final dualshot_polaroid_default:I = 0x7f080247

.field public static final dualshot_polaroid_height:I = 0x7f080246

.field public static final dualshot_polaroid_width:I = 0x7f080245

.field public static final dualshot_postcard_default:I = 0x7f080238

.field public static final dualshot_postcard_height:I = 0x7f080237

.field public static final dualshot_postcard_width:I = 0x7f080236

.field public static final dualshot_shiny_default:I = 0x7f080241

.field public static final dualshot_shiny_height:I = 0x7f080240

.field public static final dualshot_shiny_width:I = 0x7f08023f

.field public static final dualshot_signature_default:I = 0x7f08023b

.field public static final dualshot_signature_height:I = 0x7f08023a

.field public static final dualshot_signature_width:I = 0x7f080239

.field public static final dualshot_splitview_default:I = 0x7f080250

.field public static final dualshot_splitview_height:I = 0x7f08024e

.field public static final dualshot_splitview_normal_ratio_width:I = 0x7f08024f

.field public static final dualshot_splitview_start_posx:I = 0x7f08024b

.field public static final dualshot_splitview_start_posy:I = 0x7f08024c

.field public static final dualshot_splitview_width:I = 0x7f08024d

.field public static final dualshot_start_posx:I = 0x7f08022e

.field public static final dualshot_start_posy:I = 0x7f08022f

.field public static final duration:I = 0x7f080509

.field public static final easycameraRecording_recording_button_height:I = 0x7f0801ca

.field public static final easycameraRecording_recording_button_width:I = 0x7f0801c9

.field public static final easycameraRecording_right_button_margin_middle:I = 0x7f0801c6

.field public static final easycameraRecording_right_button_margin_right:I = 0x7f0801c5

.field public static final easycameraRecording_right_button_margin_top:I = 0x7f0801c4

.field public static final easycameraRecording_snapshot_icon_height:I = 0x7f0801cc

.field public static final easycameraRecording_snapshot_icon_width:I = 0x7f0801cb

.field public static final easycameraRecording_stop_icon_margin_left:I = 0x7f0801c8

.field public static final easycameraRecording_stop_icon_margin_top:I = 0x7f0801c7

.field public static final easycamera_basemenu_button_left_margin:I = 0x7f0801b2

.field public static final easycamera_basemenu_button_top_margin:I = 0x7f0801b1

.field public static final easycamera_flash_button_pos_x:I = 0x7f0801b3

.field public static final easycamera_indicator_group_bottom_margin:I = 0x7f0801ba

.field public static final easycamera_indicator_group_height:I = 0x7f0801b7

.field public static final easycamera_indicator_group_margin_left:I = 0x7f0801b9

.field public static final easycamera_indicator_group_margin_top:I = 0x7f0801b8

.field public static final easycamera_indicator_group_width:I = 0x7f0801b6

.field public static final easycamera_indicator_rec_icon_left_margin:I = 0x7f0801bc

.field public static final easycamera_indicator_rec_icon_side_margin:I = 0x7f0801be

.field public static final easycamera_indicator_rec_icon_top_margin:I = 0x7f0801bd

.field public static final easycamera_indicator_rec_icon_width:I = 0x7f0801bf

.field public static final easycamera_indicator_rec_side_margin:I = 0x7f0801c3

.field public static final easycamera_indicator_rec_text_margin:I = 0x7f0801c0

.field public static final easycamera_indicator_rec_text_width:I = 0x7f0801c1

.field public static final easycamera_indicator_rec_time_width:I = 0x7f0801c2

.field public static final easycamera_indicator_rec_width:I = 0x7f0801bb

.field public static final easycamera_item_height:I = 0x7f08019f

.field public static final easycamera_item_icon_pos_x:I = 0x7f0801e0

.field public static final easycamera_item_icon_pos_y:I = 0x7f0801e1

.field public static final easycamera_item_icon_size:I = 0x7f0801df

.field public static final easycamera_item_title_bg_height:I = 0x7f0801dc

.field public static final easycamera_item_title_bg_width:I = 0x7f0801db

.field public static final easycamera_item_title_bottom_bg_pos_y:I = 0x7f0801de

.field public static final easycamera_item_title_top_bg_pos_y:I = 0x7f0801dd

.field public static final easycamera_item_width:I = 0x7f08019e

.field public static final easycamera_menu_grid_column_1_pos_x:I = 0x7f0801d7

.field public static final easycamera_menu_grid_column_offset:I = 0x7f0801d8

.field public static final easycamera_menu_grid_row_1_pos_y:I = 0x7f0801d9

.field public static final easycamera_menu_grid_row_offset:I = 0x7f0801da

.field public static final easycamera_menu_item_height:I = 0x7f0801d6

.field public static final easycamera_menu_item_width:I = 0x7f0801d5

.field public static final easycamera_mode_button_pos_x:I = 0x7f0801aa

.field public static final easycamera_mode_button_pos_y:I = 0x7f0801ab

.field public static final easycamera_mode_button_size:I = 0x7f0801a9

.field public static final easycamera_mode_text_height:I = 0x7f0801af

.field public static final easycamera_mode_text_margin_right:I = 0x7f0801b0

.field public static final easycamera_mode_text_pos_x:I = 0x7f0801ac

.field public static final easycamera_mode_text_pos_y:I = 0x7f0801ad

.field public static final easycamera_mode_text_width:I = 0x7f0801ae

.field public static final easycamera_right_button_height:I = 0x7f0801a1

.field public static final easycamera_right_button_margin_bottom:I = 0x7f0801a8

.field public static final easycamera_right_button_margin_middle:I = 0x7f0801a6

.field public static final easycamera_right_button_margin_right:I = 0x7f0801a7

.field public static final easycamera_right_button_margin_top:I = 0x7f0801a5

.field public static final easycamera_right_button_width:I = 0x7f0801a0

.field public static final easycamera_right_shutter_button_height:I = 0x7f0801a4

.field public static final easycamera_right_shutter_button_icon_height:I = 0x7f0801a3

.field public static final easycamera_right_shutter_button_icon_width:I = 0x7f0801a2

.field public static final easycamera_thumbnail_button_margin_bottom:I = 0x7f0801b4

.field public static final easycamera_thumbnail_button_margin_left:I = 0x7f0801b5

.field public static final easycamera_thumbnail_height:I = 0x7f0801d0

.field public static final easycamera_thumbnail_image_height:I = 0x7f0801d4

.field public static final easycamera_thumbnail_image_pos_x:I = 0x7f0801d1

.field public static final easycamera_thumbnail_image_pos_y:I = 0x7f0801d2

.field public static final easycamera_thumbnail_image_width:I = 0x7f0801d3

.field public static final easycamera_thumbnail_pos_x:I = 0x7f0801cd

.field public static final easycamera_thumbnail_pos_y:I = 0x7f0801ce

.field public static final easycamera_thumbnail_width:I = 0x7f0801cf

.field public static final editableshortcut_helptext_height:I = 0x7f080307

.field public static final editableshortcut_helptext_height_vertical:I = 0x7f080308

.field public static final editableshortcut_helptext_pos_x:I = 0x7f080303

.field public static final editableshortcut_helptext_pos_y:I = 0x7f080304

.field public static final editableshortcut_helptext_side_margin:I = 0x7f080309

.field public static final editableshortcut_helptext_size:I = 0x7f0803c0

.field public static final editableshortcut_helptext_width:I = 0x7f080305

.field public static final editableshortcut_helptext_width_vertical:I = 0x7f080306

.field public static final editableshortcut_horizontal_line_offset:I = 0x7f0802f9

.field public static final editableshortcut_horizontal_line_pos_x:I = 0x7f0802f7

.field public static final editableshortcut_horizontal_line_pos_y:I = 0x7f0802f8

.field public static final editableshortcut_item_h_padding:I = 0x7f0803bf

.field public static final editableshortcut_item_height:I = 0x7f0803be

.field public static final editableshortcut_item_pos_y_offset:I = 0x7f080302

.field public static final editableshortcut_item_width:I = 0x7f0803bd

.field public static final editableshortcut_list_height:I = 0x7f0802ff

.field public static final editableshortcut_list_landscape_pos_x:I = 0x7f080300

.field public static final editableshortcut_list_landscape_pos_y:I = 0x7f080301

.field public static final editableshortcut_list_line_offset:I = 0x7f0802fd

.field public static final editableshortcut_list_width:I = 0x7f0802fe

.field public static final editableshortcut_menu_background_pos_y:I = 0x7f0802f4

.field public static final editableshortcut_menu_height:I = 0x7f0802f6

.field public static final editableshortcut_menu_pos_x:I = 0x7f0803bb

.field public static final editableshortcut_menu_pos_y:I = 0x7f0803bc

.field public static final editableshortcut_menu_width:I = 0x7f0802f5

.field public static final editableshortcut_reset_group_height:I = 0x7f08030d

.field public static final editableshortcut_reset_group_pos_x:I = 0x7f08030a

.field public static final editableshortcut_reset_group_pos_y:I = 0x7f08030b

.field public static final editableshortcut_reset_group_width:I = 0x7f08030c

.field public static final editableshortcut_reset_margin_left:I = 0x7f0803c1

.field public static final editableshortcut_reset_margin_top:I = 0x7f0803c2

.field public static final editableshortcut_reset_pos_x:I = 0x7f08030e

.field public static final editableshortcut_reset_pos_y:I = 0x7f08030f

.field public static final editableshortcut_reset_text_height:I = 0x7f080313

.field public static final editableshortcut_reset_text_pos_x:I = 0x7f080310

.field public static final editableshortcut_reset_text_pos_y:I = 0x7f080311

.field public static final editableshortcut_reset_text_width:I = 0x7f080312

.field public static final editableshortcut_vertical_line_offset:I = 0x7f0802fc

.field public static final editableshortcut_vertical_line_pos_x:I = 0x7f0802fa

.field public static final editableshortcut_vertical_line_pos_y:I = 0x7f0802fb

.field public static final effect_menu_width:I = 0x7f080228

.field public static final effectmenu_button_margin:I = 0x7f080165

.field public static final effectmenu_height:I = 0x7f08015c

.field public static final effectmenu_highlight_margin:I = 0x7f080166

.field public static final effectmenu_highlight_pos_x:I = 0x7f080167

.field public static final effectmenu_item_height:I = 0x7f08015e

.field public static final effectmenu_item_margin:I = 0x7f08015f

.field public static final effectmenu_item_preview_padding:I = 0x7f080160

.field public static final effectmenu_item_width:I = 0x7f08015d

.field public static final effectmenu_pos_x:I = 0x7f08015a

.field public static final effectmenu_pos_y:I = 0x7f08015b

.field public static final effectmenu_title_height:I = 0x7f080162

.field public static final effectmenu_title_padding:I = 0x7f080163

.field public static final effectmenu_title_pos_y:I = 0x7f080164

.field public static final effectmenu_title_width:I = 0x7f080161

.field public static final ext_button_height:I = 0x7f080031

.field public static final ext_button_icon_pos_x:I = 0x7f080035

.field public static final ext_button_icon_pos_y:I = 0x7f080036

.field public static final ext_button_pos_x:I = 0x7f08002e

.field public static final ext_button_pos_y:I = 0x7f08002f

.field public static final ext_button_text_pos_x:I = 0x7f080322

.field public static final ext_button_text_size:I = 0x7f080032

.field public static final ext_button_width:I = 0x7f080030

.field public static final ext_download_button_text_pos_x:I = 0x7f080033

.field public static final ext_preload_button_text_pos_x:I = 0x7f080034

.field public static final ext_text_height:I = 0x7f08034d

.field public static final ext_text_pos_x:I = 0x7f08034b

.field public static final ext_text_size:I = 0x7f08034e

.field public static final ext_text_width:I = 0x7f08034c

.field public static final fastcontrol_list_item_height:I = 0x7f080369

.field public static final golf_capture_help_text_height:I = 0x7f0802e5

.field public static final golf_capture_help_text_left:I = 0x7f0802e6

.field public static final golf_capture_help_text_top:I = 0x7f0802e7

.field public static final golf_capture_help_text_width:I = 0x7f0802e4

.field public static final golf_rect_horizontal_height:I = 0x7f0802e3

.field public static final golf_rect_horizontal_left:I = 0x7f0802e0

.field public static final golf_rect_horizontal_top:I = 0x7f0802e1

.field public static final golf_rect_horizontal_width:I = 0x7f0802e2

.field public static final help_item_bottom_padding:I = 0x7f0802c4

.field public static final help_item_default_height:I = 0x7f0802c0

.field public static final help_item_description_icon_right_padding:I = 0x7f0802c9

.field public static final help_item_description_icon_width:I = 0x7f0802c8

.field public static final help_item_left_padding:I = 0x7f0802c1

.field public static final help_item_point_pos_x:I = 0x7f0802c6

.field public static final help_item_point_pos_y:I = 0x7f0802c7

.field public static final help_item_right_padding:I = 0x7f0802c2

.field public static final help_item_text_height:I = 0x7f0802ca

.field public static final help_item_top_bottom_padding:I = 0x7f0802c5

.field public static final help_item_top_padding:I = 0x7f0802c3

.field public static final help_popup_close_left_padding:I = 0x7f0802bb

.field public static final help_popup_close_pos_y:I = 0x7f0802ba

.field public static final help_popup_close_width:I = 0x7f0802bc

.field public static final help_popup_height:I = 0x7f0802b2

.field public static final help_popup_portrait_height:I = 0x7f0802bf

.field public static final help_popup_portrait_pos_x:I = 0x7f0802bd

.field public static final help_popup_portrait_width:I = 0x7f0802be

.field public static final help_popup_pos_x:I = 0x7f0802af

.field public static final help_popup_pos_y:I = 0x7f0802b0

.field public static final help_popup_title_divider_line_height:I = 0x7f0802b9

.field public static final help_popup_title_divider_line_pos_y:I = 0x7f0802b7

.field public static final help_popup_title_divider_line_width:I = 0x7f0802b8

.field public static final help_popup_title_height:I = 0x7f0802b3

.field public static final help_popup_title_left_padding:I = 0x7f0802b5

.field public static final help_popup_title_text_height:I = 0x7f0802b4

.field public static final help_popup_title_top_padding:I = 0x7f0802b6

.field public static final help_popup_width:I = 0x7f0802b1

.field public static final help_toturial_touch_focus_padding:I = 0x7f0801ff

.field public static final help_tutorial_dual_pip_focus_x:I = 0x7f0801fd

.field public static final help_tutorial_dual_pip_focus_y:I = 0x7f0801fe

.field public static final help_tutorial_mode_popup_landscape_x:I = 0x7f080206

.field public static final help_tutorial_mode_popup_landscape_y:I = 0x7f080207

.field public static final help_tutorial_mode_popup_portrait_x:I = 0x7f080208

.field public static final help_tutorial_mode_popup_portrait_y:I = 0x7f080209

.field public static final help_tutorial_mode_popup_x:I = 0x7f0804fa

.field public static final help_tutorial_mode_popup_y:I = 0x7f0804fb

.field public static final help_tutorial_popup_down_padding:I = 0x7f080203

.field public static final help_tutorial_popup_picker_padding:I = 0x7f080200

.field public static final help_tutorial_popup_r_l_padding:I = 0x7f080201

.field public static final help_tutorial_popup_up_padding:I = 0x7f080202

.field public static final help_tutorial_popup_width:I = 0x7f0804f9

.field public static final help_tutorial_shutter_button_focus_y:I = 0x7f0801fa

.field public static final help_tutorial_sidemenu_focus_x:I = 0x7f0801f9

.field public static final help_tutorial_text_height:I = 0x7f08020e

.field public static final help_tutorial_text_height_margin:I = 0x7f08020b

.field public static final help_tutorial_text_line_margin:I = 0x7f08020f

.field public static final help_tutorial_text_width:I = 0x7f08020c

.field public static final help_tutorial_text_width_margin:I = 0x7f08020a

.field public static final help_tutorial_text_width_port:I = 0x7f08020d

.field public static final help_tutorial_touch_focus_popup_picker_x:I = 0x7f080204

.field public static final help_tutorial_touch_focus_popup_picker_y:I = 0x7f080205

.field public static final help_tutorial_touch_focus_x:I = 0x7f0801fb

.field public static final help_tutorial_touch_focus_y:I = 0x7f0801fc

.field public static final icon_image_height:I = 0x7f080485

.field public static final icon_image_pos_x:I = 0x7f080482

.field public static final icon_image_pos_y:I = 0x7f080483

.field public static final icon_image_width:I = 0x7f080484

.field public static final item_checkbox_pos_y:I = 0x7f080116

.field public static final item_checkbox_width:I = 0x7f080114

.field public static final item_checkbox_x_offset_from_right:I = 0x7f080115

.field public static final item_editableshortcut_type_button_height:I = 0x7f080121

.field public static final item_editableshortcut_type_button_image_pos_x:I = 0x7f080122

.field public static final item_editableshortcut_type_button_image_pos_y:I = 0x7f080123

.field public static final item_editableshortcut_type_button_width:I = 0x7f080120

.field public static final item_editableshortcut_type_text_height:I = 0x7f080127

.field public static final item_editableshortcut_type_text_pos_x:I = 0x7f080124

.field public static final item_editableshortcut_type_text_pos_y:I = 0x7f080125

.field public static final item_editableshortcut_type_text_width:I = 0x7f080126

.field public static final item_effect_width:I = 0x7f080008

.field public static final item_height:I = 0x7f080009

.field public static final item_list_type_button_height:I = 0x7f08010f

.field public static final item_list_type_button_image_pos_x:I = 0x7f08010c

.field public static final item_list_type_button_image_pos_y:I = 0x7f08010d

.field public static final item_list_type_button_width:I = 0x7f08010e

.field public static final item_list_type_text_height:I = 0x7f080113

.field public static final item_list_type_text_pos_x:I = 0x7f080110

.field public static final item_list_type_text_pos_y:I = 0x7f080111

.field public static final item_list_type_text_width:I = 0x7f080112

.field public static final item_separator_height:I = 0x7f080128

.field public static final item_separator_pos_x:I = 0x7f080129

.field public static final item_setting_type_button_height:I = 0x7f080118

.field public static final item_setting_type_button_width:I = 0x7f080117

.field public static final item_setting_type_data_height:I = 0x7f08011f

.field public static final item_setting_type_data_pos_x:I = 0x7f08011d

.field public static final item_setting_type_data_pos_y:I = 0x7f08011e

.field public static final item_setting_type_data_width:I = 0x7f08011c

.field public static final item_setting_type_text_pos_x:I = 0x7f080119

.field public static final item_setting_type_text_pos_y:I = 0x7f08011a

.field public static final item_setting_type_text_width:I = 0x7f08011b

.field public static final item_sidebutton_type_button_height:I = 0x7f08038e

.field public static final item_sidebutton_type_button_image_pos_x:I = 0x7f08038f

.field public static final item_sidebutton_type_button_image_pos_y:I = 0x7f080390

.field public static final item_sidebutton_type_button_width:I = 0x7f08038d

.field public static final item_width:I = 0x7f080007

.field public static final item_x:I = 0x7f08000a

.field public static final item_y:I = 0x7f08000b

.field public static final list_pos_x:I = 0x7f0804fc

.field public static final list_type_text_pos_x:I = 0x7f080504

.field public static final list_type_text_pos_y:I = 0x7f080505

.field public static final list_type_text_width:I = 0x7f080506

.field public static final list_with_ok_type_text_width:I = 0x7f080229

.field public static final listtypemenu_anchor_height:I = 0x7f080028

.field public static final listtypemenu_anchor_pos_x:I = 0x7f080029

.field public static final listtypemenu_anchor_pos_y:I = 0x7f08002a

.field public static final listtypemenu_anchor_width:I = 0x7f080027

.field public static final listtypemenu_info_left_padding:I = 0x7f080107

.field public static final listtypemenu_info_pos_y:I = 0x7f080106

.field public static final listtypemenu_info_width:I = 0x7f080108

.field public static final listtypemenu_item_height:I = 0x7f0800fc

.field public static final listtypemenu_item_width:I = 0x7f0800fb

.field public static final listtypemenu_max_offset:I = 0x7f0800f3

.field public static final listtypemenu_menu_area_height:I = 0x7f0800fe

.field public static final listtypemenu_menu_area_width:I = 0x7f0800fd

.field public static final listtypemenu_menu_depth_portrait_offset:I = 0x7f080100

.field public static final listtypemenu_menu_depth_portrait_pos_x_270:I = 0x7f0800f7

.field public static final listtypemenu_menu_depth_portrait_pos_x_90:I = 0x7f0800f6

.field public static final listtypemenu_menu_depth_pos_x:I = 0x7f0800ff

.field public static final listtypemenu_menu_depth_width:I = 0x7f0800fa

.field public static final listtypemenu_menu_height:I = 0x7f0800f9

.field public static final listtypemenu_menu_landscape_bottom_padding:I = 0x7f08010b

.field public static final listtypemenu_menu_portrait_side_padding:I = 0x7f08010a

.field public static final listtypemenu_menu_pos_x:I = 0x7f0800f0

.field public static final listtypemenu_menu_pos_y:I = 0x7f0800f1

.field public static final listtypemenu_menu_width:I = 0x7f0800f8

.field public static final listtypemenu_offset_from_anchor:I = 0x7f0800f2

.field public static final listtypemenu_portrait_top:I = 0x7f0800f4

.field public static final listtypemenu_portrait_top_margin:I = 0x7f0800f5

.field public static final listtypemenu_right_boundary:I = 0x7f080109

.field public static final listtypemenu_title_divider_line_height:I = 0x7f080105

.field public static final listtypemenu_title_divider_line_pos_y:I = 0x7f080103

.field public static final listtypemenu_title_divider_line_width:I = 0x7f080104

.field public static final listtypemenu_title_height:I = 0x7f080101

.field public static final listtypemenu_title_left_padding:I = 0x7f080102

.field public static final listtypemenu_title_top_padding:I = 0x7f0803c3

.field public static final max_zoom_level:I = 0x7f080470

.field public static final max_zoom_ratio:I = 0x7f08046e

.field public static final menu_edit_helptext_height:I = 0x7f080348

.field public static final menu_edit_helptext_margin:I = 0x7f080346

.field public static final menu_edit_helptext_width:I = 0x7f080347

.field public static final min_zoom_ratio:I = 0x7f08046f

.field public static final mode_description_content_landscape_pos_x:I = 0x7f08006f

.field public static final mode_description_content_landscape_width:I = 0x7f080071

.field public static final mode_description_content_portrait_pos_x:I = 0x7f080070

.field public static final mode_description_content_portrait_width:I = 0x7f080072

.field public static final mode_description_description_line_height:I = 0x7f080075

.field public static final mode_description_group_landscape_width:I = 0x7f08006d

.field public static final mode_description_group_portrait_width:I = 0x7f08006e

.field public static final mode_description_group_pos_x_0:I = 0x7f080061

.field public static final mode_description_group_pos_x_180:I = 0x7f080063

.field public static final mode_description_group_pos_x_270:I = 0x7f080064

.field public static final mode_description_group_pos_x_90:I = 0x7f080062

.field public static final mode_description_group_pos_y_0:I = 0x7f080065

.field public static final mode_description_group_pos_y_180:I = 0x7f080067

.field public static final mode_description_group_pos_y_270:I = 0x7f080068

.field public static final mode_description_group_pos_y_90:I = 0x7f080066

.field public static final mode_description_padding_landscape_height:I = 0x7f080076

.field public static final mode_description_padding_portrait_height:I = 0x7f080077

.field public static final mode_description_title_content_padding:I = 0x7f080074

.field public static final mode_description_title_height:I = 0x7f080073

.field public static final mode_description_title_pos_y_0:I = 0x7f080069

.field public static final mode_description_title_pos_y_180:I = 0x7f08006b

.field public static final mode_description_title_pos_y_270:I = 0x7f08006c

.field public static final mode_description_title_pos_y_90:I = 0x7f08006a

.field public static final mode_item_title_bg_height:I = 0x7f08005e

.field public static final mode_item_title_bg_width:I = 0x7f08005d

.field public static final mode_item_title_bottom_bg_pos_y:I = 0x7f080060

.field public static final mode_item_title_top_bg_pos_y:I = 0x7f08005f

.field public static final mode_menu_auto_mode_button_pos_x:I = 0x7f080037

.field public static final mode_menu_auto_mode_button_pos_y:I = 0x7f080038

.field public static final mode_menu_auto_mode_text_pos_y:I = 0x7f080327

.field public static final mode_menu_grid_column_1_of_4_pos_x:I = 0x7f08003d

.field public static final mode_menu_grid_column_1_of_5_pos_x:I = 0x7f08003e

.field public static final mode_menu_grid_column_offset:I = 0x7f08003f

.field public static final mode_menu_grid_column_offset_5columns:I = 0x7f080040

.field public static final mode_menu_grid_row_1_pos_y:I = 0x7f080041

.field public static final mode_menu_grid_row_offset:I = 0x7f080042

.field public static final mode_menu_help_group_width:I = 0x7f08005c

.field public static final mode_menu_item_height:I = 0x7f08003c

.field public static final mode_menu_item_width:I = 0x7f08003b

.field public static final mode_menu_landscape_left_margin:I = 0x7f080328

.field public static final mode_menu_list_type_button_pos_x:I = 0x7f080039

.field public static final mode_menu_list_type_button_pos_y:I = 0x7f08003a

.field public static final mode_menu_list_type_text_pos_y:I = 0x7f08032b

.field public static final mode_menu_portrait_left_margin:I = 0x7f080329

.field public static final mode_menu_portrait_right_maring:I = 0x7f08032a

.field public static final mode_menu_text_height:I = 0x7f08032d

.field public static final mode_menu_text_width:I = 0x7f08032c

.field public static final mode_menu_wheel_item1_pos_z:I = 0x7f080047

.field public static final mode_menu_wheel_item2_pos_z:I = 0x7f08004c

.field public static final mode_menu_wheel_item3_pos_z:I = 0x7f080051

.field public static final mode_menu_wheel_item4_pos_z:I = 0x7f080056

.field public static final mode_menu_wheel_item5_pos_z:I = 0x7f08005b

.field public static final mode_menu_wheel_landscape_item1_pos_x:I = 0x7f080043

.field public static final mode_menu_wheel_landscape_item1_pos_y:I = 0x7f080044

.field public static final mode_menu_wheel_landscape_item2_pos_x:I = 0x7f080048

.field public static final mode_menu_wheel_landscape_item2_pos_y:I = 0x7f080049

.field public static final mode_menu_wheel_landscape_item3_pos_x:I = 0x7f08004d

.field public static final mode_menu_wheel_landscape_item3_pos_y:I = 0x7f08004e

.field public static final mode_menu_wheel_landscape_item4_pos_x:I = 0x7f080052

.field public static final mode_menu_wheel_landscape_item4_pos_y:I = 0x7f080053

.field public static final mode_menu_wheel_landscape_item5_pos_x:I = 0x7f080057

.field public static final mode_menu_wheel_landscape_item5_pos_y:I = 0x7f080058

.field public static final mode_menu_wheel_portrait_item1_pos_x:I = 0x7f080045

.field public static final mode_menu_wheel_portrait_item1_pos_y:I = 0x7f080046

.field public static final mode_menu_wheel_portrait_item2_pos_x:I = 0x7f08004a

.field public static final mode_menu_wheel_portrait_item2_pos_y:I = 0x7f08004b

.field public static final mode_menu_wheel_portrait_item3_pos_x:I = 0x7f08004f

.field public static final mode_menu_wheel_portrait_item3_pos_y:I = 0x7f080050

.field public static final mode_menu_wheel_portrait_item4_pos_x:I = 0x7f080054

.field public static final mode_menu_wheel_portrait_item4_pos_y:I = 0x7f080055

.field public static final mode_menu_wheel_portrait_item5_pos_x:I = 0x7f080059

.field public static final mode_menu_wheel_portrait_item5_pos_y:I = 0x7f08005a

.field public static final modeswitchbutton_cell_padding_height:I = 0x7f080394

.field public static final modeswitchbutton_cell_padding_width:I = 0x7f080393

.field public static final modeswitchbutton_selected_image_pos_x:I = 0x7f080392

.field public static final modeswitchbutton_switch_ball_height:I = 0x7f080397

.field public static final modeswitchbutton_switch_ball_max_movement:I = 0x7f080395

.field public static final modeswitchbutton_switch_ball_pos_x:I = 0x7f08039b

.field public static final modeswitchbutton_switch_ball_pos_y:I = 0x7f08039c

.field public static final modeswitchbutton_switch_ball_select_area_height:I = 0x7f08039a

.field public static final modeswitchbutton_switch_ball_select_area_width:I = 0x7f080399

.field public static final modeswitchbutton_switch_ball_width:I = 0x7f080396

.field public static final modeswitchbutton_switch_bg_image_pos_x:I = 0x7f080391

.field public static final modeswitchbutton_touch_padding:I = 0x7f080398

.field public static final okbutton_height:I = 0x7f08022c

.field public static final okbutton_offset_from_right:I = 0x7f08022a

.field public static final okbutton_width:I = 0x7f08022b

.field public static final panorama_capture_progressbar_text_x:I = 0x7f0803da

.field public static final panoramashot_livepreview_landscape_height:I = 0x7f0803fa

.field public static final panoramashot_livepreview_landscape_width:I = 0x7f0803f9

.field public static final panoramashot_livepreview_landscape_x:I = 0x7f0803fb

.field public static final panoramashot_livepreview_landscape_y:I = 0x7f0803fc

.field public static final panoramashot_livepreview_portrait_height:I = 0x7f0803fe

.field public static final panoramashot_livepreview_portrait_width:I = 0x7f0803fd

.field public static final panoramashot_livepreview_portrait_x:I = 0x7f0803ff

.field public static final panoramashot_livepreview_portrait_y:I = 0x7f080400

.field public static final panoramashot_livepreview_rect_thickness:I = 0x7f080403

.field public static final panoramashot_livepreview_single_height:I = 0x7f080402

.field public static final panoramashot_livepreview_single_width:I = 0x7f080401

.field public static final panoramashot_rect_height:I = 0x7f0803f7

.field public static final panoramashot_rect_margin:I = 0x7f0803f8

.field public static final panoramashot_rect_width:I = 0x7f0803f6

.field public static final panoramashot_root_shift:I = 0x7f080224

.field public static final panoramashot_tri_height:I = 0x7f0803f5

.field public static final panoramashot_tri_pos_x_0:I = 0x7f0803ec

.field public static final panoramashot_tri_pos_x_180:I = 0x7f0803ee

.field public static final panoramashot_tri_pos_x_270:I = 0x7f0803ef

.field public static final panoramashot_tri_pos_x_90:I = 0x7f0803ed

.field public static final panoramashot_tri_pos_y_0:I = 0x7f0803f0

.field public static final panoramashot_tri_pos_y_180:I = 0x7f0803f2

.field public static final panoramashot_tri_pos_y_270:I = 0x7f0803f3

.field public static final panoramashot_tri_pos_y_90:I = 0x7f0803f1

.field public static final panoramashot_tri_width:I = 0x7f0803f4

.field public static final panoramashot_viewfinder_destination_height:I = 0x7f080407

.field public static final panoramashot_viewfinder_destination_width:I = 0x7f080406

.field public static final panoramashot_viewfinder_destination_x:I = 0x7f080404

.field public static final panoramashot_viewfinder_destination_y:I = 0x7f080405

.field public static final panoramashot_viewfinder_thickness:I = 0x7f080223

.field public static final petdet_sound_button_height:I = 0x7f08012d

.field public static final petdet_sound_button_width:I = 0x7f08012c

.field public static final petdet_sound_button_x:I = 0x7f08012a

.field public static final petdet_sound_button_y:I = 0x7f08012b

.field public static final postcapture_progressbar_pos_x_0:I = 0x7f0803dd

.field public static final postcapture_progressbar_pos_x_90:I = 0x7f0803de

.field public static final postcapture_progressbar_pos_y_0:I = 0x7f0803df

.field public static final postcapture_progressbar_pos_y_90:I = 0x7f0803e0

.field public static final preview_normal_height:I = 0x7f080319

.field public static final preview_normal_width:I = 0x7f080318

.field public static final preview_shot_mode_height:I = 0x7f08031c

.field public static final preview_shot_mode_width:I = 0x7f08031b

.field public static final preview_wide_height:I = 0x7f08031a

.field public static final preview_wide_width:I = 0x7f080317

.field public static final progress_group_portrait_width:I = 0x7f080351

.field public static final progress_group_x:I = 0x7f08034f

.field public static final progress_group_y:I = 0x7f080350

.field public static final progressbar_text_size_width:I = 0x7f080353

.field public static final progressbar_width:I = 0x7f080352

.field public static final progressing_bar_height:I = 0x7f0802db

.field public static final progressing_bar_top_padding:I = 0x7f0802da

.field public static final progressing_item_left_padding:I = 0x7f0802d7

.field public static final progressing_popup_height:I = 0x7f0802d6

.field public static final progressing_popup_portrait_pos_x:I = 0x7f0802d3

.field public static final progressing_popup_portrait_pos_y:I = 0x7f0802d4

.field public static final progressing_popup_pos_x:I = 0x7f0802d1

.field public static final progressing_popup_pos_y:I = 0x7f0802d2

.field public static final progressing_popup_width:I = 0x7f0802d5

.field public static final progressing_title_height:I = 0x7f0802d9

.field public static final progressing_title_top_padding:I = 0x7f0802d8

.field public static final quicksetting_menu_box_height:I = 0x7f080174

.field public static final quicksetting_menu_box_item_pos_x:I = 0x7f080172

.field public static final quicksetting_menu_box_line_pos_x:I = 0x7f080171

.field public static final quicksetting_menu_box_width:I = 0x7f080173

.field public static final quicksetting_menu_group_height:I = 0x7f08016b

.field public static final quicksetting_menu_group_pos_x:I = 0x7f080168

.field public static final quicksetting_menu_group_pos_y:I = 0x7f080169

.field public static final quicksetting_menu_group_side_margin:I = 0x7f08016c

.field public static final quicksetting_menu_group_width:I = 0x7f08016a

.field public static final quicksetting_menu_item_height:I = 0x7f08016e

.field public static final quicksetting_menu_item_interval:I = 0x7f080170

.field public static final quicksetting_menu_item_interval_test:I = 0x7f08016f

.field public static final quicksetting_menu_item_width:I = 0x7f08016d

.field public static final quicksetting_submenu_item_interval:I = 0x7f080177

.field public static final quicksetting_submenu_item_pos_x:I = 0x7f080175

.field public static final quicksetting_submenu_pos_x:I = 0x7f080176

.field public static final recording_cafbutton_pos_x:I = 0x7f0800d7

.field public static final recording_cafbutton_pos_y:I = 0x7f0800d8

.field public static final recording_dualbutton_pos_x:I = 0x7f0800da

.field public static final recording_dualbutton_pos_y:I = 0x7f0800db

.field public static final recording_dualbutton_width:I = 0x7f0800d9

.field public static final recording_dualswitchbutton_pos_x:I = 0x7f0800dc

.field public static final recording_dualswitchbutton_pos_y:I = 0x7f0800dd

.field public static final recording_fast_motion_indicator_height:I = 0x7f0800c8

.field public static final recording_fast_motion_indicator_icon_left_margin:I = 0x7f0800cb

.field public static final recording_fast_motion_indicator_icon_top_margin:I = 0x7f0800cc

.field public static final recording_fast_motion_indicator_micoff_pos_x:I = 0x7f0800d0

.field public static final recording_fast_motion_indicator_pos_x:I = 0x7f0800c9

.field public static final recording_fast_motion_indicator_pos_y:I = 0x7f0800ca

.field public static final recording_fast_motion_indicator_rec_top_margin:I = 0x7f0800cd

.field public static final recording_fast_motion_indicator_time_img_left_margin:I = 0x7f0800cf

.field public static final recording_fast_motion_indicator_time_text_left_margin:I = 0x7f0800ce

.field public static final recording_fast_motion_indicator_width:I = 0x7f0800c6

.field public static final recording_icon_pos_x:I = 0x7f0800b8

.field public static final recording_icon_pos_y:I = 0x7f0800b9

.field public static final recording_icon_text_width:I = 0x7f0800d3

.field public static final recording_indicator_bottom_margin:I = 0x7f08035a

.field public static final recording_indicator_height:I = 0x7f0800c0

.field public static final recording_indicator_icon_left_margin:I = 0x7f0800c5

.field public static final recording_indicator_icon_width:I = 0x7f0800c4

.field public static final recording_indicator_left_margin:I = 0x7f0800c2

.field public static final recording_indicator_left_offset:I = 0x7f080359

.field public static final recording_indicator_side_margin:I = 0x7f0800c1

.field public static final recording_indicator_top_margin:I = 0x7f0800c3

.field public static final recording_indicator_width:I = 0x7f0800bf

.field public static final recording_max_time_text_margin:I = 0x7f0804ec

.field public static final recording_max_time_text_width:I = 0x7f0804eb

.field public static final recording_mic_off_icon_pos_x:I = 0x7f0800ef

.field public static final recording_pause_button_pos_x:I = 0x7f0800bb

.field public static final recording_pause_button_pos_y:I = 0x7f0800bc

.field public static final recording_pause_icon_pos_x:I = 0x7f0800d2

.field public static final recording_progressbar_group_bottom_margin:I = 0x7f0800ed

.field public static final recording_progressbar_group_bottom_margin_180:I = 0x7f0800ee

.field public static final recording_progressbar_group_height:I = 0x7f0800eb

.field public static final recording_progressbar_group_side_margin:I = 0x7f0800ec

.field public static final recording_progressbar_height:I = 0x7f0800e8

.field public static final recording_progressbar_side_margin:I = 0x7f0800e9

.field public static final recording_progressbar_text_width:I = 0x7f0800ea

.field public static final recording_progressbar_width:I = 0x7f0800e7

.field public static final recording_settinsline1_pos_x:I = 0x7f0800e1

.field public static final recording_settinsline1_pos_x_margin:I = 0x7f0800e0

.field public static final recording_settinsline1_pos_y:I = 0x7f0800e2

.field public static final recording_settinsline2_pos_x:I = 0x7f0800e3

.field public static final recording_settinsline2_pos_y:I = 0x7f0800e4

.field public static final recording_settinsline3_pos_x:I = 0x7f0800e5

.field public static final recording_settinsline3_pos_y:I = 0x7f0800e6

.field public static final recording_settinsline_left_margin:I = 0x7f0800df

.field public static final recording_settinsline_width:I = 0x7f0800de

.field public static final recording_shutter_button_pos_y:I = 0x7f0800ba

.field public static final recording_size_text_width:I = 0x7f0800d6

.field public static final recording_slow_motion_indicator_micoff_pos_x:I = 0x7f0800d1

.field public static final recording_slow_motion_indicator_width:I = 0x7f0800c7

.field public static final recording_stop_button_pos_x:I = 0x7f0800bd

.field public static final recording_stop_button_pos_y:I = 0x7f0800be

.field public static final recording_time_text_width:I = 0x7f0800d4

.field public static final repeat:I = 0x7f08050a

.field public static final review_play_video_button_width:I = 0x7f0804d2

.field public static final screen_full_height:I = 0x7f080001

.field public static final screen_full_width:I = 0x7f080000

.field public static final screen_height:I = 0x7f080315

.field public static final screen_width:I = 0x7f080314

.field public static final separator_height:I = 0x7f080507

.field public static final separator_pos_x:I = 0x7f080508

.field public static final setting_menu_height:I = 0x7f080188

.field public static final setting_menu_list_height:I = 0x7f080192

.field public static final setting_menu_list_width:I = 0x7f080191

.field public static final setting_menu_margin:I = 0x7f080189

.field public static final setting_menu_portrait_height:I = 0x7f08045f

.field public static final setting_menu_pos_x:I = 0x7f080185

.field public static final setting_menu_pos_y:I = 0x7f080186

.field public static final setting_menu_width:I = 0x7f080187

.field public static final setting_title_camcorder_width:I = 0x7f080195

.field public static final setting_title_camera_width:I = 0x7f080194

.field public static final setting_title_common_width:I = 0x7f080196

.field public static final setting_title_divider_line_height:I = 0x7f08019d

.field public static final setting_title_divider_line_pos_y:I = 0x7f08019b

.field public static final setting_title_divider_line_width:I = 0x7f08019c

.field public static final setting_title_height:I = 0x7f080190

.field public static final setting_title_img_center_sidepadding:I = 0x7f08018f

.field public static final setting_title_img_sidepadding:I = 0x7f08018e

.field public static final setting_title_img_top_padding:I = 0x7f08018c

.field public static final setting_title_img_width:I = 0x7f08018d

.field public static final setting_title_left_padding:I = 0x7f08018a

.field public static final setting_title_tab_focus_line_height:I = 0x7f080198

.field public static final setting_title_tab_focus_line_pos_y:I = 0x7f080197

.field public static final setting_title_tab_unfocus_line_height:I = 0x7f08019a

.field public static final setting_title_tab_unfocus_line_pos_y:I = 0x7f080199

.field public static final setting_title_top_padding:I = 0x7f08018b

.field public static final setting_title_width:I = 0x7f080193

.field public static final shareshot_button_icon_height:I = 0x7f08044b

.field public static final shareshot_button_icon_pos_x_0:I = 0x7f08044c

.field public static final shareshot_button_icon_pos_x_180:I = 0x7f08044e

.field public static final shareshot_button_icon_pos_x_270:I = 0x7f08044f

.field public static final shareshot_button_icon_pos_x_90:I = 0x7f08044d

.field public static final shareshot_button_icon_pos_y_0:I = 0x7f080450

.field public static final shareshot_button_icon_pos_y_180:I = 0x7f080452

.field public static final shareshot_button_icon_pos_y_270:I = 0x7f080453

.field public static final shareshot_button_icon_pos_y_90:I = 0x7f080451

.field public static final shareshot_button_icon_width:I = 0x7f08044a

.field public static final shareshot_count_text_height:I = 0x7f080455

.field public static final shareshot_count_text_pos_x_0:I = 0x7f080457

.field public static final shareshot_count_text_pos_x_180:I = 0x7f080459

.field public static final shareshot_count_text_pos_x_270:I = 0x7f08045a

.field public static final shareshot_count_text_pos_x_90:I = 0x7f080458

.field public static final shareshot_count_text_pos_y_0:I = 0x7f08045b

.field public static final shareshot_count_text_pos_y_180:I = 0x7f08045d

.field public static final shareshot_count_text_pos_y_270:I = 0x7f08045e

.field public static final shareshot_count_text_pos_y_90:I = 0x7f08045c

.field public static final shareshot_count_text_size:I = 0x7f080456

.field public static final shareshot_count_text_width:I = 0x7f080454

.field public static final sharetargetlist_button_cancel_pos_x:I = 0x7f0804f5

.field public static final sharetargetlist_button_cancel_pos_y:I = 0x7f0804f6

.field public static final sharetargetlist_button_done_pos_x:I = 0x7f0804f7

.field public static final sharetargetlist_button_done_pos_y:I = 0x7f0804f8

.field public static final sharetargetlist_button_edit_pos_y:I = 0x7f0801eb

.field public static final sharetargetlist_button_gap:I = 0x7f0801ea

.field public static final sharetargetlist_button_group_height:I = 0x7f0801e7

.field public static final sharetargetlist_button_group_pos_x:I = 0x7f0804f4

.field public static final sharetargetlist_button_group_pos_y:I = 0x7f0801e5

.field public static final sharetargetlist_button_group_width:I = 0x7f0801e6

.field public static final sharetargetlist_button_height:I = 0x7f0801ed

.field public static final sharetargetlist_button_ok_pos_x:I = 0x7f0801e8

.field public static final sharetargetlist_button_ok_pos_y:I = 0x7f0801e9

.field public static final sharetargetlist_button_width:I = 0x7f0801ec

.field public static final sharetargetlist_checkbox_pos_y:I = 0x7f0801f6

.field public static final sharetargetlist_checkbox_x_offset_from_right:I = 0x7f0801f5

.field public static final sharetargetlist_icon_image_height:I = 0x7f0801f1

.field public static final sharetargetlist_icon_image_pos_x:I = 0x7f0801ee

.field public static final sharetargetlist_icon_image_pos_y:I = 0x7f0801ef

.field public static final sharetargetlist_icon_image_width:I = 0x7f0801f0

.field public static final sharetargetlist_list_height:I = 0x7f0801e4

.field public static final sharetargetlist_list_item_height:I = 0x7f0804f3

.field public static final sharetargetlist_list_item_width:I = 0x7f0804f2

.field public static final sharetargetlist_list_pos_x:I = 0x7f0804f1

.field public static final sharetargetlist_list_pos_y:I = 0x7f0801e2

.field public static final sharetargetlist_list_width:I = 0x7f0801e3

.field public static final sharetargetlist_menu_height:I = 0x7f0804ee

.field public static final sharetargetlist_menu_left_bottom_pading:I = 0x7f0804ef

.field public static final sharetargetlist_menu_top_right_pading:I = 0x7f0804f0

.field public static final sharetargetlist_menu_width:I = 0x7f0804ed

.field public static final sharetargetlist_separator_height:I = 0x7f0801f7

.field public static final sharetargetlist_separator_pos_x:I = 0x7f0801f8

.field public static final sharetargetlist_text_pos_x:I = 0x7f0801f2

.field public static final sharetargetlist_text_pos_y:I = 0x7f0801f3

.field public static final sharetargetlist_text_width:I = 0x7f0801f4

.field public static final shootingmode_helptext_bottom_margin:I = 0x7f080092

.field public static final shootingmode_helptext_bottom_margin_for_burstpanorama:I = 0x7f080094

.field public static final shootingmode_helptext_height:I = 0x7f08008f

.field public static final shootingmode_helptext_height_vertical:I = 0x7f080090

.field public static final shootingmode_helptext_progressbar_margin_for_burstpanorama:I = 0x7f080095

.field public static final shootingmode_helptext_side_margin:I = 0x7f080093

.field public static final shootingmode_helptext_top_margin:I = 0x7f080091

.field public static final shootingmode_helptext_width:I = 0x7f08008d

.field public static final shootingmode_helptext_width_vertical:I = 0x7f08008e

.field public static final shrink_bottom_offset:I = 0x7f080464

.field public static final shrink_bottom_offset_camcorder_mini:I = 0x7f080466

.field public static final shrink_bottom_offset_camcorder_wide:I = 0x7f080465

.field public static final shrink_ratio:I = 0x7f080469

.field public static final shrink_ratio_wide:I = 0x7f08046a

.field public static final shrink_right_offset:I = 0x7f080467

.field public static final shrink_right_offset_wide:I = 0x7f080468

.field public static final sidemenu_anchor_height:I = 0x7f080345

.field public static final sidemenu_dualcamera_button_pos_x:I = 0x7f08001c

.field public static final sidemenu_effect_button_pos_x:I = 0x7f080025

.field public static final sidemenu_effect_button_pos_y:I = 0x7f080026

.field public static final sidemenu_height:I = 0x7f080006

.field public static final sidemenu_left_item_interval:I = 0x7f08000d

.field public static final sidemenu_left_item_start_pos_y:I = 0x7f08000c

.field public static final sidemenu_mode_button_pos_x:I = 0x7f080342

.field public static final sidemenu_mode_button_pos_y:I = 0x7f080343

.field public static final sidemenu_quicksetting_button_open_close_gap:I = 0x7f08001f

.field public static final sidemenu_quicksetting_button_pos_x:I = 0x7f08001d

.field public static final sidemenu_quicksetting_button_pos_y:I = 0x7f08001e

.field public static final sidemenu_quicksetting_button_submenu_gap:I = 0x7f080020

.field public static final sidemenu_recording_button_pos_x:I = 0x7f080018

.field public static final sidemenu_recording_button_pos_y:I = 0x7f080019

.field public static final sidemenu_separator_offset_x:I = 0x7f080022

.field public static final sidemenu_separator_pos_x:I = 0x7f080021

.field public static final sidemenu_setting_button_pos_x:I = 0x7f080349

.field public static final sidemenu_setting_button_pos_y:I = 0x7f08034a

.field public static final sidemenu_shootingmode_button_pos_x:I = 0x7f080012

.field public static final sidemenu_shootingmode_button_pos_y:I = 0x7f080013

.field public static final sidemenu_shootingmode_text_height:I = 0x7f080017

.field public static final sidemenu_shootingmode_text_pos_x:I = 0x7f080014

.field public static final sidemenu_shootingmode_text_pos_y:I = 0x7f080015

.field public static final sidemenu_shootingmode_text_width:I = 0x7f080016

.field public static final sidemenu_shutter_button_icon_pos_x:I = 0x7f080010

.field public static final sidemenu_shutter_button_icon_pos_y:I = 0x7f080011

.field public static final sidemenu_shutter_button_image_height:I = 0x7f080341

.field public static final sidemenu_shutter_button_image_pos_x:I = 0x7f08033e

.field public static final sidemenu_shutter_button_image_pos_y:I = 0x7f08033f

.field public static final sidemenu_shutter_button_image_width:I = 0x7f080340

.field public static final sidemenu_shutter_button_pos_x:I = 0x7f08000e

.field public static final sidemenu_shutter_button_pos_y:I = 0x7f08000f

.field public static final sidemenu_switchcamera_button_pos_x:I = 0x7f08001a

.field public static final sidemenu_switchcamera_button_pos_y:I = 0x7f08001b

.field public static final sidemenu_thumnail_button_pos_y:I = 0x7f080344

.field public static final sidemenu_width:I = 0x7f080005

.field public static final size_text_height:I = 0x7f0800b7

.field public static final size_text_width:I = 0x7f0800b6

.field public static final slidermenu_exposure_button_height:I = 0x7f080159

.field public static final slidermenu_exposure_button_width:I = 0x7f080158

.field public static final slidermenu_exposure_minus_button_pos_x:I = 0x7f080156

.field public static final slidermenu_exposure_minus_button_pos_y:I = 0x7f080157

.field public static final slidermenu_exposure_plus_button_pos_x:I = 0x7f080154

.field public static final slidermenu_exposure_plus_button_pos_y:I = 0x7f080155

.field public static final slidermenu_exposure_slider_height:I = 0x7f080149

.field public static final slidermenu_exposure_slider_pos_x:I = 0x7f080146

.field public static final slidermenu_exposure_slider_pos_y:I = 0x7f080147

.field public static final slidermenu_exposure_slider_width:I = 0x7f080148

.field public static final slidermenu_gauge_ball_select_area_height:I = 0x7f08014d

.field public static final slidermenu_gauge_ball_select_area_width:I = 0x7f08014c

.field public static final slidermenu_indicator_group_height:I = 0x7f08013d

.field public static final slidermenu_indicator_group_pos_x:I = 0x7f08013a

.field public static final slidermenu_indicator_group_pos_y:I = 0x7f08013b

.field public static final slidermenu_indicator_group_width:I = 0x7f08013c

.field public static final slidermenu_indicator_symbol_height:I = 0x7f080141

.field public static final slidermenu_indicator_symbol_width:I = 0x7f080140

.field public static final slidermenu_indicator_value_text_height:I = 0x7f08013f

.field public static final slidermenu_indicator_value_text_width:I = 0x7f08013e

.field public static final slidermenu_slide_group_height:I = 0x7f080139

.field public static final slidermenu_slide_group_pos_x:I = 0x7f080136

.field public static final slidermenu_slide_group_pos_y:I = 0x7f080137

.field public static final slidermenu_slide_group_width:I = 0x7f080138

.field public static final slidermenu_slider_touch_area_height:I = 0x7f08014b

.field public static final slidermenu_slider_touch_area_width:I = 0x7f08014a

.field public static final slidermenu_zoom_button_height:I = 0x7f080153

.field public static final slidermenu_zoom_button_width:I = 0x7f080152

.field public static final slidermenu_zoom_minus_button_pos_x:I = 0x7f080150

.field public static final slidermenu_zoom_minus_button_pos_y:I = 0x7f080151

.field public static final slidermenu_zoom_plus_button_pos_x:I = 0x7f08014e

.field public static final slidermenu_zoom_plus_button_pos_y:I = 0x7f08014f

.field public static final slidermenu_zoom_slider_height:I = 0x7f080145

.field public static final slidermenu_zoom_slider_pos_x:I = 0x7f080142

.field public static final slidermenu_zoom_slider_pos_y:I = 0x7f080143

.field public static final slidermenu_zoom_slider_width:I = 0x7f080144

.field public static final smileshot_blink_image_height:I = 0x7f080221

.field public static final smileshot_blink_image_width:I = 0x7f080220

.field public static final smileshot_blink_image_x:I = 0x7f080460

.field public static final smileshot_blink_image_y:I = 0x7f080461

.field public static final smileshot_help_text_height:I = 0x7f08021f

.field public static final smileshot_help_text_width:I = 0x7f08021e

.field public static final smileshot_rect_pos_x:I = 0x7f080463

.field public static final smileshot_rect_thickness:I = 0x7f080222

.field public static final smileshot_sidemenu_width:I = 0x7f080462

.field public static final snapbutton_pos_y:I = 0x7f080357

.field public static final snapbutton_width:I = 0x7f080356

.field public static final sns_image_icon_pos_x:I = 0x7f08047e

.field public static final sns_image_icon_pos_y:I = 0x7f08047f

.field public static final sns_set_list_type_text_pos_x:I = 0x7f080480

.field public static final sns_set_list_type_text_width:I = 0x7f080481

.field public static final sns_setting_menu_height:I = 0x7f080227

.field public static final soundeffect_item_checkbox_pos_y:I = 0x7f080529

.field public static final soundeffect_item_checkbox_width:I = 0x7f080527

.field public static final soundeffect_item_checkbox_x_offset_from_right:I = 0x7f080528

.field public static final soundeffect_item_list_type_button_height:I = 0x7f080522

.field public static final soundeffect_item_list_type_button_image_pos_x:I = 0x7f08051f

.field public static final soundeffect_item_list_type_button_image_pos_y:I = 0x7f080520

.field public static final soundeffect_item_list_type_button_width:I = 0x7f080521

.field public static final soundeffect_item_list_type_text_height:I = 0x7f080526

.field public static final soundeffect_item_list_type_text_pos_x:I = 0x7f080523

.field public static final soundeffect_item_list_type_text_pos_y:I = 0x7f080524

.field public static final soundeffect_item_list_type_text_width:I = 0x7f080525

.field public static final soundeffect_item_separator_height:I = 0x7f08052a

.field public static final soundeffect_item_separator_pos_x:I = 0x7f08052b

.field public static final soundshot_button_height:I = 0x7f080258

.field public static final soundshot_button_width:I = 0x7f080257

.field public static final soundshot_button_x:I = 0x7f080255

.field public static final soundshot_button_y:I = 0x7f080256

.field public static final soundshot_help_text_font_size:I = 0x7f080259

.field public static final soundshot_help_text_height:I = 0x7f08025b

.field public static final soundshot_help_text_width:I = 0x7f08025a

.field public static final soundshot_help_text_x_0:I = 0x7f08025c

.field public static final soundshot_help_text_x_180:I = 0x7f08025e

.field public static final soundshot_help_text_x_270:I = 0x7f08025f

.field public static final soundshot_help_text_x_90:I = 0x7f08025d

.field public static final soundshot_help_text_y_0:I = 0x7f080260

.field public static final soundshot_help_text_y_180:I = 0x7f080262

.field public static final soundshot_help_text_y_270:I = 0x7f080263

.field public static final soundshot_help_text_y_90:I = 0x7f080261

.field public static final soundshot_progress_bar_height:I = 0x7f080265

.field public static final soundshot_progress_bar_pos_x_0:I = 0x7f080266

.field public static final soundshot_progress_bar_pos_x_180:I = 0x7f08026a

.field public static final soundshot_progress_bar_pos_x_270:I = 0x7f08026c

.field public static final soundshot_progress_bar_pos_x_90:I = 0x7f080268

.field public static final soundshot_progress_bar_pos_y_0:I = 0x7f080267

.field public static final soundshot_progress_bar_pos_y_180:I = 0x7f08026b

.field public static final soundshot_progress_bar_pos_y_270:I = 0x7f08026d

.field public static final soundshot_progress_bar_pos_y_90:I = 0x7f080269

.field public static final soundshot_progress_bar_seq_0_pos_x:I = 0x7f08026f

.field public static final soundshot_progress_bar_seq_1_pos_x:I = 0x7f080270

.field public static final soundshot_progress_bar_seq_2_pos_x:I = 0x7f080271

.field public static final soundshot_progress_bar_seq_3_pos_x:I = 0x7f080272

.field public static final soundshot_progress_bar_seq_4_pos_x:I = 0x7f080273

.field public static final soundshot_progress_bar_seq_5_pos_x:I = 0x7f080274

.field public static final soundshot_progress_bar_seq_6_pos_x:I = 0x7f080275

.field public static final soundshot_progress_bar_seq_7_pos_x:I = 0x7f080276

.field public static final soundshot_progress_bar_seq_8_pos_x:I = 0x7f080277

.field public static final soundshot_progress_bar_seq_pos_y:I = 0x7f08026e

.field public static final soundshot_progress_bar_timer_pos_x:I = 0x7f080278

.field public static final soundshot_progress_bar_timer_pos_y:I = 0x7f080279

.field public static final soundshot_progress_bar_width:I = 0x7f080264

.field public static final speed_indicator_height:I = 0x7f08021d

.field public static final speed_indicator_width:I = 0x7f08021c

.field public static final speedcontrol_button_height:I = 0x7f08035c

.field public static final speedcontrol_button_pos_x_0:I = 0x7f08035e

.field public static final speedcontrol_button_pos_x_180:I = 0x7f080360

.field public static final speedcontrol_button_pos_x_270:I = 0x7f080361

.field public static final speedcontrol_button_pos_x_90:I = 0x7f08035f

.field public static final speedcontrol_button_pos_y_0:I = 0x7f080362

.field public static final speedcontrol_button_pos_y_180:I = 0x7f080364

.field public static final speedcontrol_button_pos_y_270:I = 0x7f080365

.field public static final speedcontrol_button_pos_y_90:I = 0x7f080363

.field public static final speedcontrol_button_text_size:I = 0x7f08035d

.field public static final speedcontrol_button_width:I = 0x7f08035b

.field public static final speedcontrol_list_item_height:I = 0x7f080368

.field public static final speedcontrol_list_item_text_size:I = 0x7f080366

.field public static final speedcontrol_list_item_width:I = 0x7f080367

.field public static final statusbar_height:I = 0x7f080316

.field public static final stopbutton_pos_y:I = 0x7f080358

.field public static final story_effect_button_height:I = 0x7f08051c

.field public static final story_effect_button_width:I = 0x7f08051b

.field public static final story_effect_menu_height:I = 0x7f080514

.field public static final story_effect_menu_item_height:I = 0x7f080518

.field public static final story_effect_menu_item_width:I = 0x7f080517

.field public static final story_effect_menu_list_height:I = 0x7f08051a

.field public static final story_effect_menu_list_width:I = 0x7f080519

.field public static final story_effect_menu_pos_x_0:I = 0x7f08050b

.field public static final story_effect_menu_pos_x_180:I = 0x7f08050f

.field public static final story_effect_menu_pos_x_270:I = 0x7f080511

.field public static final story_effect_menu_pos_x_90:I = 0x7f08050d

.field public static final story_effect_menu_pos_y_0:I = 0x7f08050c

.field public static final story_effect_menu_pos_y_180:I = 0x7f080510

.field public static final story_effect_menu_pos_y_270:I = 0x7f080512

.field public static final story_effect_menu_pos_y_90:I = 0x7f08050e

.field public static final story_effect_menu_title_height:I = 0x7f080515

.field public static final story_effect_menu_title_top_padding:I = 0x7f080516

.field public static final story_effect_menu_width:I = 0x7f080513

.field public static final story_effect_ok_button_pos_x_0:I = 0x7f08051d

.field public static final story_effect_ok_button_pos_y_0:I = 0x7f08051e

.field public static final story_effect_progress_EQ_BG_pos_x:I = 0x7f08029b

.field public static final story_effect_progress_EQ_BG_pos_y:I = 0x7f08029c

.field public static final story_effect_progress_bar_height:I = 0x7f0802a0

.field public static final story_effect_progress_bar_pos_x:I = 0x7f08029d

.field public static final story_effect_progress_bar_pos_y:I = 0x7f08029e

.field public static final story_effect_progress_bar_seq_0_pos_x:I = 0x7f0802a6

.field public static final story_effect_progress_bar_seq_1_pos_x:I = 0x7f0802a7

.field public static final story_effect_progress_bar_seq_2_pos_x:I = 0x7f0802a8

.field public static final story_effect_progress_bar_seq_3_pos_x:I = 0x7f0802a9

.field public static final story_effect_progress_bar_seq_4_pos_x:I = 0x7f0802aa

.field public static final story_effect_progress_bar_seq_5_pos_x:I = 0x7f0802ab

.field public static final story_effect_progress_bar_seq_6_pos_x:I = 0x7f0802ac

.field public static final story_effect_progress_bar_seq_7_pos_x:I = 0x7f0802ad

.field public static final story_effect_progress_bar_seq_8_pos_x:I = 0x7f0802ae

.field public static final story_effect_progress_bar_width:I = 0x7f08029f

.field public static final story_effect_progress_eqbar_0_pos_x:I = 0x7f080285

.field public static final story_effect_progress_eqbar_10_pos_x:I = 0x7f08028f

.field public static final story_effect_progress_eqbar_11_pos_x:I = 0x7f080290

.field public static final story_effect_progress_eqbar_12_pos_x:I = 0x7f080291

.field public static final story_effect_progress_eqbar_13_pos_x:I = 0x7f080292

.field public static final story_effect_progress_eqbar_14_pos_x:I = 0x7f080293

.field public static final story_effect_progress_eqbar_15_pos_x:I = 0x7f080294

.field public static final story_effect_progress_eqbar_16_pos_x:I = 0x7f080295

.field public static final story_effect_progress_eqbar_17_pos_x:I = 0x7f080296

.field public static final story_effect_progress_eqbar_18_pos_x:I = 0x7f080297

.field public static final story_effect_progress_eqbar_19_pos_x:I = 0x7f080298

.field public static final story_effect_progress_eqbar_1_pos_x:I = 0x7f080286

.field public static final story_effect_progress_eqbar_2_pos_x:I = 0x7f080287

.field public static final story_effect_progress_eqbar_3_pos_x:I = 0x7f080288

.field public static final story_effect_progress_eqbar_4_pos_x:I = 0x7f080289

.field public static final story_effect_progress_eqbar_5_pos_x:I = 0x7f08028a

.field public static final story_effect_progress_eqbar_6_pos_x:I = 0x7f08028b

.field public static final story_effect_progress_eqbar_7_pos_x:I = 0x7f08028c

.field public static final story_effect_progress_eqbar_8_pos_x:I = 0x7f08028d

.field public static final story_effect_progress_eqbar_9_pos_x:I = 0x7f08028e

.field public static final story_effect_progress_eqbar_pos_y:I = 0x7f080284

.field public static final story_effect_progress_height:I = 0x7f08027b

.field public static final story_effect_progress_mic_pos_x:I = 0x7f080299

.field public static final story_effect_progress_mic_pos_y:I = 0x7f08029a

.field public static final story_effect_progress_pos_x_0:I = 0x7f08027c

.field public static final story_effect_progress_pos_x_180:I = 0x7f080280

.field public static final story_effect_progress_pos_x_270:I = 0x7f080282

.field public static final story_effect_progress_pos_x_90:I = 0x7f08027e

.field public static final story_effect_progress_pos_y_0:I = 0x7f08027d

.field public static final story_effect_progress_pos_y_180:I = 0x7f080281

.field public static final story_effect_progress_pos_y_270:I = 0x7f080283

.field public static final story_effect_progress_pos_y_90:I = 0x7f08027f

.field public static final story_effect_progress_text_font_size:I = 0x7f0802a5

.field public static final story_effect_progress_text_height:I = 0x7f0802a4

.field public static final story_effect_progress_text_pos_x:I = 0x7f0802a1

.field public static final story_effect_progress_text_pos_y:I = 0x7f0802a2

.field public static final story_effect_progress_text_width:I = 0x7f0802a3

.field public static final story_effect_progress_width:I = 0x7f08027a

.field public static final thumbnail_height:I = 0x7f080131

.field public static final thumbnail_image_height:I = 0x7f080135

.field public static final thumbnail_image_pos_x:I = 0x7f080132

.field public static final thumbnail_image_pos_y:I = 0x7f080133

.field public static final thumbnail_image_width:I = 0x7f080134

.field public static final thumbnail_left:I = 0x7f08031d

.field public static final thumbnail_list_item_height:I = 0x7f080478

.field public static final thumbnail_list_item_width:I = 0x7f080477

.field public static final thumbnail_list_menu_width:I = 0x7f080476

.field public static final thumbnail_pos_x:I = 0x7f08012e

.field public static final thumbnail_pos_y:I = 0x7f08012f

.field public static final thumbnail_size:I = 0x7f08031f

.field public static final thumbnail_top:I = 0x7f08031e

.field public static final thumbnail_type_button_height:I = 0x7f08047a

.field public static final thumbnail_type_button_margin_y:I = 0x7f08047b

.field public static final thumbnail_type_button_width:I = 0x7f080479

.field public static final thumbnail_type_text_height:I = 0x7f08047c

.field public static final thumbnail_type_text_margin_y:I = 0x7f08047d

.field public static final thumbnail_width:I = 0x7f080130

.field public static final thumnail_listmenu_menu_height:I = 0x7f0803c4

.field public static final timer_counting_view_height:I = 0x7f080472

.field public static final timer_counting_view_width:I = 0x7f080471

.field public static final timer_group_height:I = 0x7f08017b

.field public static final timer_group_pos_x:I = 0x7f080178

.field public static final timer_group_pos_y:I = 0x7f080179

.field public static final timer_group_width:I = 0x7f08017a

.field public static final timer_number_pos_x:I = 0x7f08017c

.field public static final timer_number_pos_y:I = 0x7f08017d

.field public static final timer_pos_left:I = 0x7f080474

.field public static final timer_pos_top:I = 0x7f080475

.field public static final timer_text_size:I = 0x7f080473

.field public static final touch_af_area_margin_bottom:I = 0x7f080004

.field public static final touch_af_area_margin_right:I = 0x7f080003

.field public static final touch_af_area_margin_top:I = 0x7f080002

.field public static final tutorial_mode_description_content_landscape_pos_x:I = 0x7f080210

.field public static final tutorial_mode_description_content_landscape_width:I = 0x7f080211

.field public static final tutorial_mode_description_content_portrait_width:I = 0x7f080213

.field public static final tutorial_mode_description_group_landscape_width:I = 0x7f080212

.field public static final tutorial_mode_description_group_portrait_pos_x_270:I = 0x7f080217

.field public static final tutorial_mode_description_group_portrait_pos_x_90:I = 0x7f080216

.field public static final tutorial_mode_description_group_portrait_width:I = 0x7f080214

.field public static final tutorial_mode_image_x:I = 0x7f080218

.field public static final tutorial_mode_menu_help_group_width:I = 0x7f080215

.field public static final velocity_swipe_to_go_gallery:I = 0x7f0804d4

.field public static final waiting_icon_image_height:I = 0x7f080503

.field public static final waiting_icon_image_pos_x:I = 0x7f080500

.field public static final waiting_icon_image_pos_y:I = 0x7f080501

.field public static final waiting_icon_image_width:I = 0x7f080502

.field public static final zoom_rect_line_width:I = 0x7f08046b

.field public static final zoom_rect_width:I = 0x7f08021b

.field public static final zoom_text_height:I = 0x7f08021a

.field public static final zoom_text_pos_x:I = 0x7f08046d

.field public static final zoom_text_size:I = 0x7f08046c

.field public static final zoom_text_upper_margin:I = 0x7f080219


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
