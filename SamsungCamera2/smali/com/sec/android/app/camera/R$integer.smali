.class public final Lcom/sec/android/app/camera/R$integer;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/camera/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "integer"
.end annotation


# static fields
.field public static final baseindicator_battery_indicator_display_under:I = 0x7f090004

.field public static final baseindicator_remain_counter_display_under:I = 0x7f090005

.field public static final baseindicator_text_size:I = 0x7f09003a

.field public static final batteryindicator_font_size:I = 0x7f090022

.field public static final capture_help_text_size:I = 0x7f09003c

.field public static final capture_progressbar_text_size:I = 0x7f09003b

.field public static final contextmenu_edit_font_size:I = 0x7f09002f

.field public static final datatext_font_size:I = 0x7f09002e

.field public static final dialogpopup_button_font_size:I = 0x7f090031

.field public static final dialogpopup_message_font_size:I = 0x7f090030

.field public static final easycamera_mode_item_title_font_size:I = 0x7f090029

.field public static final easycamera_modemenu_columns:I = 0x7f090002

.field public static final easycamera_modemenu_rows:I = 0x7f090003

.field public static final easycamera_quicksettingmenu_num_of_item:I = 0x7f09001b

.field public static final easycamera_rec_text_font_size:I = 0x7f090043

.field public static final easycamera_rec_time_text_font_size:I = 0x7f090044

.field public static final easycamera_shootingmode_text_size:I = 0x7f090045

.field public static final editableshortcut_columns:I = 0x7f090011

.field public static final editableshortcut_helptext_size:I = 0x7f090023

.field public static final editableshortcut_rows:I = 0x7f090012

.field public static final guideline_bottom_interval:I = 0x7f090019

.field public static final guideline_width:I = 0x7f09000f

.field public static final help_item_font_size:I = 0x7f09003e

.field public static final help_popup_title_text_size:I = 0x7f09003d

.field public static final iimage_mini_thumb_max_num_pixels:I = 0x7f09000e

.field public static final iimage_mini_thumb_target_size:I = 0x7f09000c

.field public static final iimage_thumbnail_max_num_pixels:I = 0x7f09000d

.field public static final iimage_thumbnail_target_size:I = 0x7f09000b

.field public static final item_editableshortcut_type_font_size:I = 0x7f09002d

.field public static final item_list_type_text_font_size:I = 0x7f09002c

.field public static final item_thumbnail_type_font_size:I = 0x7f090025

.field public static final list_type_text_font_size:I = 0x7f090035

.field public static final listtypemenu_menu_depth_title_text_size:I = 0x7f090041

.field public static final listtypemenu_title_font_size:I = 0x7f090032

.field public static final listtypemenu_title_text_size:I = 0x7f090028

.field public static final max_zoom_level:I = 0x7f090013

.field public static final max_zoom_ratio:I = 0x7f090014

.field public static final min_zoom_ratio:I = 0x7f090015

.field public static final mode_button_font_size:I = 0x7f09001d

.field public static final mode_description_content_font_size:I = 0x7f090020

.field public static final mode_description_title_font_size:I = 0x7f09001f

.field public static final mode_item_title_font_size:I = 0x7f09001e

.field public static final mode_menu_text_size:I = 0x7f090021

.field public static final modemenu_columns:I = 0x7f090000

.field public static final modemenu_rows:I = 0x7f090001

.field public static final nfc_connect_view_button_font_size:I = 0x7f090017

.field public static final nfc_connect_view_font_size:I = 0x7f090016

.field public static final panorama_guide_text_size:I = 0x7f090042

.field public static final postview_blink_line_thick:I = 0x7f090010

.field public static final profile_menu_title_font_size:I = 0x7f090036

.field public static final profile_name_font_size:I = 0x7f090037

.field public static final progressing_title_size:I = 0x7f09003f

.field public static final quicksettingmenu_num_of_item:I = 0x7f09001a

.field public static final quicksettingmenu_shortcut_num_of_item:I = 0x7f09001c

.field public static final rec_indicator_text_font_size:I = 0x7f090026

.field public static final rec_size_text_font_size:I = 0x7f090027

.field public static final sel_pic_item_text_font_size:I = 0x7f090039

.field public static final sel_pic_title_font_size:I = 0x7f090038

.field public static final setting_title_font_size:I = 0x7f090033

.field public static final sharing_device_dialog_max_height:I = 0x7f090040

.field public static final sidemenu_left_num_of_item:I = 0x7f090018

.field public static final slidermenu_exposure_value_num_of_step:I = 0x7f090007

.field public static final slidermenu_exposure_value_offset:I = 0x7f090008

.field public static final slidermenu_zoom_max_ratio:I = 0x7f09000a

.field public static final slidermenu_zoom_num_of_step:I = 0x7f090009

.field public static final slidermenu_zoom_value_text_font_size:I = 0x7f090034

.field public static final zoom_indicator_text_font_size:I = 0x7f09002b

.field public static final zoom_text_size:I = 0x7f090024

.field public static final zoom_value_text_font_size:I = 0x7f09002a

.field public static final zoom_velocity:I = 0x7f090006


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3314
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
