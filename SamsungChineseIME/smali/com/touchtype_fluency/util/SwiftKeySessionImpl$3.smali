.class Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;
.super Ljava/util/HashMap;
.source "SwiftKeySessionImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/touchtype_fluency/util/SwiftKeySessionImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashMap",
        "<",
        "Ljava/lang/String;",
        "Ljava/util/List",
        "<",
        "Ljava/lang/String;",
        ">;>;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 750
    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    .line 751
    const-string v0, "#"

    new-array v1, v8, [Ljava/lang/String;

    const-string v2, "\u02c7"

    aput-object v2, v1, v4

    const-string v2, "\u02cb"

    aput-object v2, v1, v5

    const-string v2, "\u02ca"

    aput-object v2, v1, v6

    const-string v2, "\u02d9"

    aput-object v2, v1, v7

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 752
    const-string v0, "1"

    new-array v1, v7, [Ljava/lang/String;

    const-string v2, "\u3105"

    aput-object v2, v1, v4

    const-string v2, "\u3109"

    aput-object v2, v1, v5

    const-string v2, "\u311a"

    aput-object v2, v1, v6

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 753
    const-string v0, "2"

    new-array v1, v8, [Ljava/lang/String;

    const-string v2, "\u310d"

    aput-object v2, v1, v4

    const-string v2, "\u3110"

    aput-object v2, v1, v5

    const-string v2, "\u311e"

    aput-object v2, v1, v6

    const-string v2, "\u3127"

    aput-object v2, v1, v7

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 754
    const-string v0, "3"

    new-array v1, v8, [Ljava/lang/String;

    const-string v2, "\u3113"

    aput-object v2, v1, v4

    const-string v2, "\u3117"

    aput-object v2, v1, v5

    const-string v2, "\u3122"

    aput-object v2, v1, v6

    const-string v2, "\u3126"

    aput-object v2, v1, v7

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 755
    const-string v0, "4"

    new-array v1, v7, [Ljava/lang/String;

    const-string v2, "\u3106"

    aput-object v2, v1, v4

    const-string v2, "\u310a"

    aput-object v2, v1, v5

    const-string v2, "\u311b"

    aput-object v2, v1, v6

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 756
    const-string v0, "5"

    new-array v1, v8, [Ljava/lang/String;

    const-string v2, "\u310e"

    aput-object v2, v1, v4

    const-string v2, "\u3111"

    aput-object v2, v1, v5

    const-string v2, "\u311f"

    aput-object v2, v1, v6

    const-string v2, "\u3128"

    aput-object v2, v1, v7

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 757
    const-string v0, "6"

    new-array v1, v7, [Ljava/lang/String;

    const-string v2, "\u3114"

    aput-object v2, v1, v4

    const-string v2, "\u3118"

    aput-object v2, v1, v5

    const-string v2, "\u3123"

    aput-object v2, v1, v6

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 758
    const-string v0, "7"

    new-array v1, v8, [Ljava/lang/String;

    const-string v2, "\u3107"

    aput-object v2, v1, v4

    const-string v2, "\u310b"

    aput-object v2, v1, v5

    const-string v2, "\u311c"

    aput-object v2, v1, v6

    const-string v2, "\u311d"

    aput-object v2, v1, v7

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 759
    const-string v0, "8"

    new-array v1, v8, [Ljava/lang/String;

    const-string v2, "\u310f"

    aput-object v2, v1, v4

    const-string v2, "\u3112"

    aput-object v2, v1, v5

    const-string v2, "\u3120"

    aput-object v2, v1, v6

    const-string v2, "\u3129"

    aput-object v2, v1, v7

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 760
    const-string v0, "9"

    new-array v1, v8, [Ljava/lang/String;

    const-string v2, "\u3115"

    aput-object v2, v1, v4

    const-string v2, "\u3119"

    aput-object v2, v1, v5

    const-string v2, "\u3124"

    aput-object v2, v1, v6

    const-string v2, "\u3125"

    aput-object v2, v1, v7

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 761
    const-string v0, "0"

    new-array v1, v8, [Ljava/lang/String;

    const-string v2, "\u3108"

    aput-object v2, v1, v4

    const-string v2, "\u310c"

    aput-object v2, v1, v5

    const-string v2, "\u3121"

    aput-object v2, v1, v6

    const-string v2, "\u3116"

    aput-object v2, v1, v7

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 762
    const-string v0, "00"

    new-array v1, v7, [Ljava/lang/String;

    const-string v2, "\u3108\u3121"

    aput-object v2, v1, v4

    const-string v2, "\u310c\u3121"

    aput-object v2, v1, v5

    const-string v2, "\u3116\u3121"

    aput-object v2, v1, v6

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 763
    const-string v0, "01"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u3108\u311a"

    aput-object v2, v1, v4

    const-string v2, "\u310c\u311a"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 764
    const-string v0, "02"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u310c\u311e"

    aput-object v2, v1, v4

    const-string v2, "\u310c\u3127"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 765
    const-string v0, "020"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u310c\u3127\u3121"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 766
    const-string v0, "021"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u310c\u3127\u311a"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 767
    const-string v0, "023"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u310c\u3127\u3122"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 768
    const-string v0, "026"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u310c\u3127\u3123"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 769
    const-string v0, "027"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u310c\u3127\u311d"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 770
    const-string v0, "028"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u310c\u3127\u3120"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 771
    const-string v0, "029"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u310c\u3127\u3124"

    aput-object v2, v1, v4

    const-string v2, "\u310c\u3127\u3125"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 772
    const-string v0, "03"

    new-array v1, v7, [Ljava/lang/String;

    const-string v2, "\u3108\u3122"

    aput-object v2, v1, v4

    const-string v2, "\u310c\u3122"

    aput-object v2, v1, v5

    const-string v2, "\u3116\u3122"

    aput-object v2, v1, v6

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 773
    const-string v0, "04"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u3108\u311b"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 774
    const-string v0, "05"

    new-array v1, v8, [Ljava/lang/String;

    const-string v2, "\u3108\u311f"

    aput-object v2, v1, v4

    const-string v2, "\u3108\u3128"

    aput-object v2, v1, v5

    const-string v2, "\u310c\u311f"

    aput-object v2, v1, v6

    const-string v2, "\u310c\u3128"

    aput-object v2, v1, v7

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 775
    const-string v0, "053"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u310c\u3128\u3122"

    aput-object v2, v1, v4

    const-string v2, "\u3116\u3128\u3122"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 776
    const-string v0, "054"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u310c\u3128\u311b"

    aput-object v2, v1, v4

    const-string v2, "\u3116\u3128\u311b"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 777
    const-string v0, "055"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u3116\u3128\u311f"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 778
    const-string v0, "056"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u310c\u3128\u3123"

    aput-object v2, v1, v4

    const-string v2, "\u3116\u3128\u3123"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 779
    const-string v0, "059"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u310c\u3128\u3125"

    aput-object v2, v1, v4

    const-string v2, "\u3116\u3128\u3125"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 780
    const-string v0, "06"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u3108\u3123"

    aput-object v2, v1, v4

    const-string v2, "\u3116\u3123"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 781
    const-string v0, "07"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u310c\u311c"

    aput-object v2, v1, v4

    const-string v2, "\u3116\u311c"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 782
    const-string v0, "08"

    new-array v1, v7, [Ljava/lang/String;

    const-string v2, "\u310c\u3120"

    aput-object v2, v1, v4

    const-string v2, "\u310c\u3129"

    aput-object v2, v1, v5

    const-string v2, "\u3116\u3120"

    aput-object v2, v1, v6

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 783
    const-string v0, "087"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u310c\u3129\u311d"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 784
    const-string v0, "09"

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "\u3108\u3124"

    aput-object v2, v1, v4

    const-string v2, "\u3108\u3125"

    aput-object v2, v1, v5

    const-string v2, "\u310c\u3124"

    aput-object v2, v1, v6

    const-string v2, "\u310c\u3125"

    aput-object v2, v1, v7

    const-string v2, "\u3116\u3124"

    aput-object v2, v1, v8

    const/4 v2, 0x5

    const-string v3, "\u3116\u3125"

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 785
    const-string v0, "10"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u3109\u3121"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 786
    const-string v0, "11"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u3105\u311a"

    aput-object v2, v1, v4

    const-string v2, "\u3109\u311a"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 787
    const-string v0, "12"

    new-array v1, v8, [Ljava/lang/String;

    const-string v2, "\u3105\u311e"

    aput-object v2, v1, v4

    const-string v2, "\u3105\u3127"

    aput-object v2, v1, v5

    const-string v2, "\u3109\u311e"

    aput-object v2, v1, v6

    const-string v2, "\u3109\u3127"

    aput-object v2, v1, v7

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 788
    const-string v0, "120"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u3109\u3127\u3121"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 789
    const-string v0, "121"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u3109\u3127\u311a"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 790
    const-string v0, "123"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u3105\u3127\u3122"

    aput-object v2, v1, v4

    const-string v2, "\u3109\u3127\u3122"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 791
    const-string v0, "126"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u3105\u3127\u3123"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 792
    const-string v0, "127"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u3105\u3127\u311d"

    aput-object v2, v1, v4

    const-string v2, "\u3109\u3127\u311d"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 793
    const-string v0, "128"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u3105\u3127\u3120"

    aput-object v2, v1, v4

    const-string v2, "\u3109\u3127\u3120"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 794
    const-string v0, "129"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u3105\u3127\u3125"

    aput-object v2, v1, v4

    const-string v2, "\u3109\u3127\u3125"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 795
    const-string v0, "13"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u3105\u3122"

    aput-object v2, v1, v4

    const-string v2, "\u3109\u3122"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 796
    const-string v0, "14"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u3105\u311b"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 797
    const-string v0, "15"

    new-array v1, v8, [Ljava/lang/String;

    const-string v2, "\u3105\u311f"

    aput-object v2, v1, v4

    const-string v2, "\u3105\u3128"

    aput-object v2, v1, v5

    const-string v2, "\u3109\u311f"

    aput-object v2, v1, v6

    const-string v2, "\u3109\u3128"

    aput-object v2, v1, v7

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 798
    const-string v0, "153"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u3109\u3128\u3122"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 799
    const-string v0, "154"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u3109\u3128\u311b"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 800
    const-string v0, "155"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u3109\u3128\u311f"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 801
    const-string v0, "156"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u3109\u3128\u3123"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 802
    const-string v0, "159"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u3109\u3128\u3125"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 803
    const-string v0, "16"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u3105\u3123"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 804
    const-string v0, "17"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u3109\u311c"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 805
    const-string v0, "18"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u3105\u3120"

    aput-object v2, v1, v4

    const-string v2, "\u3109\u3120"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 806
    const-string v0, "19"

    new-array v1, v8, [Ljava/lang/String;

    const-string v2, "\u3105\u3124"

    aput-object v2, v1, v4

    const-string v2, "\u3105\u3125"

    aput-object v2, v1, v5

    const-string v2, "\u3109\u3124"

    aput-object v2, v1, v6

    const-string v2, "\u3109\u3125"

    aput-object v2, v1, v7

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 807
    const-string v0, "20"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u310d\u3121"

    aput-object v2, v1, v4

    const-string v2, "\u3127\u3121"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 808
    const-string v0, "21"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u310d\u311a"

    aput-object v2, v1, v4

    const-string v2, "\u3127\u311a"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 809
    const-string v0, "22"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u310d\u311e"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 810
    const-string v0, "220"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u3110\u3127\u3121"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 811
    const-string v0, "221"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u3110\u3127\u311a"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 812
    const-string v0, "223"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u3110\u3127\u3122"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 813
    const-string v0, "226"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u3110\u3127\u3123"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 814
    const-string v0, "227"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u3110\u3127\u311d"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 815
    const-string v0, "228"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u3110\u3127\u3120"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 816
    const-string v0, "229"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u3110\u3127\u3124"

    aput-object v2, v1, v4

    const-string v2, "\u3110\u3127\u3125"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 817
    const-string v0, "23"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u310d\u3122"

    aput-object v2, v1, v4

    const-string v2, "\u3127\u3122"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 818
    const-string v0, "24"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u3127\u311b"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 819
    const-string v0, "25"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u310d\u311f"

    aput-object v2, v1, v4

    const-string v2, "\u310d\u3128"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 820
    const-string v0, "251"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u310d\u3128\u311a"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 821
    const-string v0, "252"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u310d\u3128\u311e"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 822
    const-string v0, "253"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u310d\u3128\u3122"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 823
    const-string v0, "254"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u310d\u3128\u311b"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 824
    const-string v0, "255"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u310d\u3128\u311f"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 825
    const-string v0, "256"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u310d\u3128\u3123"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 826
    const-string v0, "259"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u310d\u3128\u3125"

    aput-object v2, v1, v4

    const-string v2, "\u310d\u3128\u3124"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 827
    const-string v0, "26"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u310d\u3123"

    aput-object v2, v1, v4

    const-string v2, "\u3127\u3123"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 828
    const-string v0, "27"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u310d\u311c"

    aput-object v2, v1, v4

    const-string v2, "\u3127\u311d"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 829
    const-string v0, "28"

    new-array v1, v7, [Ljava/lang/String;

    const-string v2, "\u310d\u3120"

    aput-object v2, v1, v4

    const-string v2, "\u3110\u3129"

    aput-object v2, v1, v5

    const-string v2, "\u3127\u3120"

    aput-object v2, v1, v6

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 830
    const-string v0, "283"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u3110\u3129\u3122"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 831
    const-string v0, "286"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u3110\u3129\u3123"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 832
    const-string v0, "287"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u3110\u3129\u311d"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 833
    const-string v0, "289"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u3110\u3129\u3125"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 834
    const-string v0, "29"

    new-array v1, v8, [Ljava/lang/String;

    const-string v2, "\u310d\u3124"

    aput-object v2, v1, v4

    const-string v2, "\u310d\u3125"

    aput-object v2, v1, v5

    const-string v2, "\u3127\u3124"

    aput-object v2, v1, v6

    const-string v2, "\u3127\u3125"

    aput-object v2, v1, v7

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 835
    const-string v0, "30"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u3113\u3121"

    aput-object v2, v1, v4

    const-string v2, "\u3117\u3121"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 836
    const-string v0, "31"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u3117\u311a"

    aput-object v2, v1, v4

    const-string v2, "\u3113\u311a"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 837
    const-string v0, "32"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u3117\u311e"

    aput-object v2, v1, v4

    const-string v2, "\u3113\u311e"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 838
    const-string v0, "33"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u3117\u3122"

    aput-object v2, v1, v4

    const-string v2, "\u3113\u3122"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 839
    const-string v0, "35"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u3117\u311f"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 840
    const-string v0, "351"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u3113\u3128\u311a"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 841
    const-string v0, "352"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u3113\u3128\u311e"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 842
    const-string v0, "353"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u3113\u3128\u3122"

    aput-object v2, v1, v4

    const-string v2, "\u3117\u3128\u3122"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 843
    const-string v0, "354"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u3113\u3128\u311b"

    aput-object v2, v1, v4

    const-string v2, "\u3117\u3128\u311b"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 844
    const-string v0, "355"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u3113\u3128\u311f"

    aput-object v2, v1, v4

    const-string v2, "\u3117\u3128\u311f"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 845
    const-string v0, "356"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u3113\u3128\u3123"

    aput-object v2, v1, v4

    const-string v2, "\u3117\u3128\u3123"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 846
    const-string v0, "359"

    new-array v1, v7, [Ljava/lang/String;

    const-string v2, "\u3113\u3128\u3125"

    aput-object v2, v1, v4

    const-string v2, "\u3113\u3128\u3124"

    aput-object v2, v1, v5

    const-string v2, "\u3117\u3128\u3125"

    aput-object v2, v1, v6

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 847
    const-string v0, "36"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u3117\u3123"

    aput-object v2, v1, v4

    const-string v2, "\u3113\u3123"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 848
    const-string v0, "37"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u3117\u311c"

    aput-object v2, v1, v4

    const-string v2, "\u3113\u311c"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 849
    const-string v0, "38"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u3117\u3120"

    aput-object v2, v1, v4

    const-string v2, "\u3113\u3120"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 850
    const-string v0, "39"

    new-array v1, v8, [Ljava/lang/String;

    const-string v2, "\u3117\u3124"

    aput-object v2, v1, v4

    const-string v2, "\u3117\u3125"

    aput-object v2, v1, v5

    const-string v2, "\u3113\u3124"

    aput-object v2, v1, v6

    const-string v2, "\u3113\u3125"

    aput-object v2, v1, v7

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 851
    const-string v0, "40"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u3106\u3121"

    aput-object v2, v1, v4

    const-string v2, "\u310a\u3121"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 852
    const-string v0, "41"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u3106\u311a"

    aput-object v2, v1, v4

    const-string v2, "\u310a\u311a"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 853
    const-string v0, "42"

    new-array v1, v8, [Ljava/lang/String;

    const-string v2, "\u3106\u311e"

    aput-object v2, v1, v4

    const-string v2, "\u3106\u3127"

    aput-object v2, v1, v5

    const-string v2, "\u310a\u311e"

    aput-object v2, v1, v6

    const-string v2, "\u310a\u3127"

    aput-object v2, v1, v7

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 854
    const-string v0, "423"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u3106\u3127\u3122"

    aput-object v2, v1, v4

    const-string v2, "\u310a\u3127\u3122"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 855
    const-string v0, "426"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u3106\u3127\u3123"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 856
    const-string v0, "427"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u3106\u3127\u311d"

    aput-object v2, v1, v4

    const-string v2, "\u310a\u3127\u311d"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 857
    const-string v0, "428"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u3106\u3127\u3120"

    aput-object v2, v1, v4

    const-string v2, "\u310a\u3127\u3120"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 858
    const-string v0, "429"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u3106\u3127\u3125"

    aput-object v2, v1, v4

    const-string v2, "\u310a\u3127\u3125"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 859
    const-string v0, "43"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u3106\u3122"

    aput-object v2, v1, v4

    const-string v2, "\u310a\u3122"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 860
    const-string v0, "44"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u3106\u311b"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 861
    const-string v0, "45"

    new-array v1, v7, [Ljava/lang/String;

    const-string v2, "\u3106\u311f"

    aput-object v2, v1, v4

    const-string v2, "\u3106\u3128"

    aput-object v2, v1, v5

    const-string v2, "\u310a\u311f"

    aput-object v2, v1, v6

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 862
    const-string v0, "453"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u310a\u3128\u3122"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 863
    const-string v0, "454"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u310a\u3128\u311b"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 864
    const-string v0, "455"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u310a\u3128\u311f"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 865
    const-string v0, "456"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u310a\u3128\u3123"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 866
    const-string v0, "459"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u310a\u3128\u3125"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 867
    const-string v0, "46"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u3106\u3123"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 868
    const-string v0, "47"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u310a\u311c"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 869
    const-string v0, "48"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u3106\u3120"

    aput-object v2, v1, v4

    const-string v2, "\u310a\u3120"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 870
    const-string v0, "49"

    new-array v1, v7, [Ljava/lang/String;

    const-string v2, "\u3106\u3124"

    aput-object v2, v1, v4

    const-string v2, "\u3106\u3125"

    aput-object v2, v1, v5

    const-string v2, "\u310a\u3124"

    aput-object v2, v1, v6

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 871
    const-string v0, "50"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u310e\u3121"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 872
    const-string v0, "51"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u310e\u311a"

    aput-object v2, v1, v4

    const-string v2, "\u3128\u311a"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 873
    const-string v0, "52"

    new-array v1, v7, [Ljava/lang/String;

    const-string v2, "\u310e\u311e"

    aput-object v2, v1, v4

    const-string v2, "\u3111\u3127"

    aput-object v2, v1, v5

    const-string v2, "\u3128\u311e"

    aput-object v2, v1, v6

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 874
    const-string v0, "520"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u3111\u3127\u3121"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 875
    const-string v0, "521"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u3111\u3127\u311a"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 876
    const-string v0, "523"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u3111\u3127\u3122"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 877
    const-string v0, "526"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u3111\u3127\u3123"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 878
    const-string v0, "527"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u3111\u3127\u311d"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 879
    const-string v0, "528"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u3111\u3127\u3120"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 880
    const-string v0, "529"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u3111\u3127\u3124"

    aput-object v2, v1, v4

    const-string v2, "\u3111\u3127\u3125"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 881
    const-string v0, "53"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u310e\u3122"

    aput-object v2, v1, v4

    const-string v2, "\u3128\u3122"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 882
    const-string v0, "54"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u3128\u311b"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 883
    const-string v0, "55"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u310e\u3128"

    aput-object v2, v1, v4

    const-string v2, "\u3128\u311f"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 884
    const-string v0, "551"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u310e\u3128\u311a"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 885
    const-string v0, "552"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u310e\u3128\u311e"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 886
    const-string v0, "553"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u310e\u3128\u3122"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 887
    const-string v0, "554"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u310e\u3128\u311b"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 888
    const-string v0, "555"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u310e\u3128\u311f"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 889
    const-string v0, "556"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u310e\u3128\u3123"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 890
    const-string v0, "559"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u310e\u3128\u3125"

    aput-object v2, v1, v4

    const-string v2, "\u310e\u3128\u3124"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 891
    const-string v0, "56"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u310e\u3123"

    aput-object v2, v1, v4

    const-string v2, "\u3128\u3123"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 892
    const-string v0, "57"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u310e\u311c"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 893
    const-string v0, "58"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u310e\u3120"

    aput-object v2, v1, v4

    const-string v2, "\u3111\u3129"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 894
    const-string v0, "583"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u3111\u3129\u3122"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 895
    const-string v0, "586"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u3111\u3129\u3123"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 896
    const-string v0, "587"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u3111\u3129\u311d"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 897
    const-string v0, "589"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u3111\u3129\u3125"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 898
    const-string v0, "59"

    new-array v1, v8, [Ljava/lang/String;

    const-string v2, "\u310e\u3124"

    aput-object v2, v1, v4

    const-string v2, "\u310e\u3125"

    aput-object v2, v1, v5

    const-string v2, "\u3128\u3124"

    aput-object v2, v1, v6

    const-string v2, "\u3128\u3125"

    aput-object v2, v1, v7

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 899
    const-string v0, "60"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u3114\u3121"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 900
    const-string v0, "61"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u3118\u311a"

    aput-object v2, v1, v4

    const-string v2, "\u3114\u311a"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 901
    const-string v0, "62"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u3118\u311e"

    aput-object v2, v1, v4

    const-string v2, "\u3114\u311e"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 902
    const-string v0, "63"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u3118\u3122"

    aput-object v2, v1, v4

    const-string v2, "\u3114\u3122"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 903
    const-string v0, "65"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u3114\u3128"

    aput-object v2, v1, v4

    const-string v2, "\u3118\u3128"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 904
    const-string v0, "652"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u3114\u3128\u311e"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 905
    const-string v0, "653"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u3114\u3128\u3122"

    aput-object v2, v1, v4

    const-string v2, "\u3118\u3128\u3122"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 906
    const-string v0, "654"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u3114\u3128\u311b"

    aput-object v2, v1, v4

    const-string v2, "\u3118\u3128\u311b"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 907
    const-string v0, "655"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u3114\u3128\u311f"

    aput-object v2, v1, v4

    const-string v2, "\u3118\u3128\u311f"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 908
    const-string v0, "656"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u3114\u3128\u3123"

    aput-object v2, v1, v4

    const-string v2, "\u3118\u3128\u3123"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 909
    const-string v0, "659"

    new-array v1, v7, [Ljava/lang/String;

    const-string v2, "\u3114\u3128\u3125"

    aput-object v2, v1, v4

    const-string v2, "\u3114\u3128\u3124"

    aput-object v2, v1, v5

    const-string v2, "\u3118\u3128\u3125"

    aput-object v2, v1, v6

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 910
    const-string v0, "66"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u3118\u3123"

    aput-object v2, v1, v4

    const-string v2, "\u3114\u3123"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 911
    const-string v0, "67"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u3118\u311c"

    aput-object v2, v1, v4

    const-string v2, "\u3114\u311c"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 912
    const-string v0, "68"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u3118\u3120"

    aput-object v2, v1, v4

    const-string v2, "\u3114\u3120"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 913
    const-string v0, "69"

    new-array v1, v8, [Ljava/lang/String;

    const-string v2, "\u3118\u3124"

    aput-object v2, v1, v4

    const-string v2, "\u3118\u3125"

    aput-object v2, v1, v5

    const-string v2, "\u3114\u3124"

    aput-object v2, v1, v6

    const-string v2, "\u3114\u3125"

    aput-object v2, v1, v7

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 914
    const-string v0, "70"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u3107\u3121"

    aput-object v2, v1, v4

    const-string v2, "\u310b\u3121"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 915
    const-string v0, "71"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u3107\u311a"

    aput-object v2, v1, v4

    const-string v2, "\u310b\u311a"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 916
    const-string v0, "72"

    new-array v1, v8, [Ljava/lang/String;

    const-string v2, "\u3107\u311e"

    aput-object v2, v1, v4

    const-string v2, "\u3107\u3127"

    aput-object v2, v1, v5

    const-string v2, "\u310b\u311e"

    aput-object v2, v1, v6

    const-string v2, "\u310b\u3127"

    aput-object v2, v1, v7

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 917
    const-string v0, "720"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u3107\u3127\u3121"

    aput-object v2, v1, v4

    const-string v2, "\u310b\u3127\u3121"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 918
    const-string v0, "723"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u3107\u3127\u3122"

    aput-object v2, v1, v4

    const-string v2, "\u310b\u3127\u3122"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 919
    const-string v0, "726"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u3107\u3127\u3123"

    aput-object v2, v1, v4

    const-string v2, "\u310b\u3127\u3123"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 920
    const-string v0, "727"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u3107\u3127\u311d"

    aput-object v2, v1, v4

    const-string v2, "\u310b\u3127\u311d"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 921
    const-string v0, "728"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u3107\u3127\u3120"

    aput-object v2, v1, v4

    const-string v2, "\u310b\u3127\u3120"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 922
    const-string v0, "729"

    new-array v1, v7, [Ljava/lang/String;

    const-string v2, "\u3107\u3127\u3125"

    aput-object v2, v1, v4

    const-string v2, "\u310b\u3127\u3124"

    aput-object v2, v1, v5

    const-string v2, "\u310b\u3127\u3125"

    aput-object v2, v1, v6

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 923
    const-string v0, "73"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u3107\u3122"

    aput-object v2, v1, v4

    const-string v2, "\u310b\u3122"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 924
    const-string v0, "74"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u3107\u311b"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 925
    const-string v0, "75"

    new-array v1, v8, [Ljava/lang/String;

    const-string v2, "\u3107\u311f"

    aput-object v2, v1, v4

    const-string v2, "\u3107\u3128"

    aput-object v2, v1, v5

    const-string v2, "\u310b\u311f"

    aput-object v2, v1, v6

    const-string v2, "\u310b\u3128"

    aput-object v2, v1, v7

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 926
    const-string v0, "753"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u310b\u3128\u3122"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 927
    const-string v0, "754"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u310b\u3128\u311b"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 928
    const-string v0, "759"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u310b\u3128\u3125"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 929
    const-string v0, "76"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u3107\u3123"

    aput-object v2, v1, v4

    const-string v2, "\u310b\u3123"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 930
    const-string v0, "77"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u310b\u311c"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 931
    const-string v0, "78"

    new-array v1, v7, [Ljava/lang/String;

    const-string v2, "\u3107\u3120"

    aput-object v2, v1, v4

    const-string v2, "\u310b\u3120"

    aput-object v2, v1, v5

    const-string v2, "\u310b\u3129"

    aput-object v2, v1, v6

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 932
    const-string v0, "787"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u310b\u3129\u311d"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 933
    const-string v0, "79"

    new-array v1, v8, [Ljava/lang/String;

    const-string v2, "\u3107\u3124"

    aput-object v2, v1, v4

    const-string v2, "\u3107\u3125"

    aput-object v2, v1, v5

    const-string v2, "\u310b\u3124"

    aput-object v2, v1, v6

    const-string v2, "\u310b\u3125"

    aput-object v2, v1, v7

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 934
    const-string v0, "80"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u310f\u3121"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 935
    const-string v0, "81"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u310f\u311a"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 936
    const-string v0, "82"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u310f\u311e"

    aput-object v2, v1, v4

    const-string v2, "\u3112\u3127"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 937
    const-string v0, "820"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u3112\u3127\u3121"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 938
    const-string v0, "821"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u3112\u3127\u311a"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 939
    const-string v0, "823"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u3112\u3127\u3122"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 940
    const-string v0, "826"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u3112\u3127\u3123"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 941
    const-string v0, "827"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u3112\u3127\u311d"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 942
    const-string v0, "828"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u3112\u3127\u3120"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 943
    const-string v0, "829"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u3112\u3127\u3124"

    aput-object v2, v1, v4

    const-string v2, "\u3112\u3127\u3125"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 944
    const-string v0, "83"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u310f\u3122"

    aput-object v2, v1, v4

    const-string v2, "\u3129\u3122"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 945
    const-string v0, "85"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u310f\u311f"

    aput-object v2, v1, v4

    const-string v2, "\u310f\u3128"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 946
    const-string v0, "851"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u310f\u3128\u311a"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 947
    const-string v0, "852"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u310f\u3128\u311e"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 948
    const-string v0, "853"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u310f\u3128\u3122"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 949
    const-string v0, "854"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u310f\u3128\u311b"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 950
    const-string v0, "855"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u310f\u3128\u311f"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 951
    const-string v0, "856"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u310f\u3128\u3123"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 952
    const-string v0, "859"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u310f\u3128\u3125"

    aput-object v2, v1, v4

    const-string v2, "\u310f\u3128\u3124"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 953
    const-string v0, "86"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u310f\u3123"

    aput-object v2, v1, v4

    const-string v2, "\u3129\u3123"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 954
    const-string v0, "87"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u3129\u311d"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 955
    const-string v0, "88"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u310f\u3120"

    aput-object v2, v1, v4

    const-string v2, "\u3112\u3129"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 956
    const-string v0, "883"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u3112\u3129\u3122"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 957
    const-string v0, "886"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u3112\u3129\u3123"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 958
    const-string v0, "887"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u3112\u3129\u311d"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 959
    const-string v0, "889"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u3112\u3129\u3125"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 960
    const-string v0, "89"

    new-array v1, v7, [Ljava/lang/String;

    const-string v2, "\u310f\u3124"

    aput-object v2, v1, v4

    const-string v2, "\u310f\u3125"

    aput-object v2, v1, v5

    const-string v2, "\u3129\u3125"

    aput-object v2, v1, v6

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 961
    const-string v0, "90"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u3115\u3121"

    aput-object v2, v1, v4

    const-string v2, "\u3119\u3121"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 962
    const-string v0, "91"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u3119\u311a"

    aput-object v2, v1, v4

    const-string v2, "\u3115\u311a"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 963
    const-string v0, "92"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u3119\u311e"

    aput-object v2, v1, v4

    const-string v2, "\u3115\u311e"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 964
    const-string v0, "93"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u3119\u3122"

    aput-object v2, v1, v4

    const-string v2, "\u3115\u3122"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 965
    const-string v0, "95"

    new-array v1, v7, [Ljava/lang/String;

    const-string v2, "\u3115\u311f"

    aput-object v2, v1, v4

    const-string v2, "\u3115\u3128"

    aput-object v2, v1, v5

    const-string v2, "\u3119\u3128"

    aput-object v2, v1, v6

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 966
    const-string v0, "951"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u3115\u3128\u311a"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 967
    const-string v0, "952"

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "\u3115\u3128\u311e"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 968
    const-string v0, "953"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u3115\u3128\u3122"

    aput-object v2, v1, v4

    const-string v2, "\u3119\u3128\u3122"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 969
    const-string v0, "954"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u3115\u3128\u311b"

    aput-object v2, v1, v4

    const-string v2, "\u3119\u3128\u311b"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 970
    const-string v0, "955"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u3115\u3128\u311f"

    aput-object v2, v1, v4

    const-string v2, "\u3119\u3128\u311f"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 971
    const-string v0, "956"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u3115\u3128\u3123"

    aput-object v2, v1, v4

    const-string v2, "\u3119\u3128\u3123"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 972
    const-string v0, "959"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u3115\u3128\u3124"

    aput-object v2, v1, v4

    const-string v2, "\u3119\u3128\u3125"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 973
    const-string v0, "96"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u3119\u3123"

    aput-object v2, v1, v4

    const-string v2, "\u3115\u3123"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 974
    const-string v0, "97"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u3119\u311c"

    aput-object v2, v1, v4

    const-string v2, "\u3115\u311c"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 975
    const-string v0, "98"

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u3119\u3120"

    aput-object v2, v1, v4

    const-string v2, "\u3115\u3120"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 976
    const-string v0, "99"

    new-array v1, v8, [Ljava/lang/String;

    const-string v2, "\u3119\u3124"

    aput-object v2, v1, v4

    const-string v2, "\u3119\u3125"

    aput-object v2, v1, v5

    const-string v2, "\u3115\u3124"

    aput-object v2, v1, v6

    const-string v2, "\u3115\u3125"

    aput-object v2, v1, v7

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImpl$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1
    return-void
.end method
