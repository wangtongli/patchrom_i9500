.class Lcom/touchtype_fluency/util/SwiftKeySessionImplTest$1;
.super Ljava/util/HashMap;
.source "SwiftKeySessionImplTest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/touchtype_fluency/util/SwiftKeySessionImplTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashMap",
        "<",
        "Ljava/lang/Character;",
        "[",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 23
    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    .line 24
    const/16 v0, 0x23

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    new-array v1, v7, [Ljava/lang/String;

    const-string v2, "\u02c7"

    aput-object v2, v1, v3

    const-string v2, "\u02cb"

    aput-object v2, v1, v4

    const-string v2, "\u02ca"

    aput-object v2, v1, v5

    const-string v2, "\u02d9"

    aput-object v2, v1, v6

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImplTest$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 25
    const/16 v0, 0x31

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u3105"

    aput-object v2, v1, v3

    const-string v2, "\u3109"

    aput-object v2, v1, v4

    const-string v2, "\u311a"

    aput-object v2, v1, v5

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImplTest$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 26
    const/16 v0, 0x32

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    new-array v1, v7, [Ljava/lang/String;

    const-string v2, "\u310d"

    aput-object v2, v1, v3

    const-string v2, "\u3110"

    aput-object v2, v1, v4

    const-string v2, "\u311e"

    aput-object v2, v1, v5

    const-string v2, "\u3127"

    aput-object v2, v1, v6

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImplTest$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 27
    const/16 v0, 0x33

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    new-array v1, v7, [Ljava/lang/String;

    const-string v2, "\u3113"

    aput-object v2, v1, v3

    const-string v2, "\u3117"

    aput-object v2, v1, v4

    const-string v2, "\u3122"

    aput-object v2, v1, v5

    const-string v2, "\u3126"

    aput-object v2, v1, v6

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImplTest$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 28
    const/16 v0, 0x34

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u3106"

    aput-object v2, v1, v3

    const-string v2, "\u310a"

    aput-object v2, v1, v4

    const-string v2, "\u311b"

    aput-object v2, v1, v5

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImplTest$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 29
    const/16 v0, 0x35

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    new-array v1, v7, [Ljava/lang/String;

    const-string v2, "\u310e"

    aput-object v2, v1, v3

    const-string v2, "\u3111"

    aput-object v2, v1, v4

    const-string v2, "\u311f"

    aput-object v2, v1, v5

    const-string v2, "\u3128"

    aput-object v2, v1, v6

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImplTest$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 30
    const/16 v0, 0x36

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "\u3114"

    aput-object v2, v1, v3

    const-string v2, "\u3118"

    aput-object v2, v1, v4

    const-string v2, "\u3123"

    aput-object v2, v1, v5

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImplTest$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    const/16 v0, 0x37

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    new-array v1, v7, [Ljava/lang/String;

    const-string v2, "\u3107"

    aput-object v2, v1, v3

    const-string v2, "\u310b"

    aput-object v2, v1, v4

    const-string v2, "\u311c"

    aput-object v2, v1, v5

    const-string v2, "\u311d"

    aput-object v2, v1, v6

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImplTest$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    const/16 v0, 0x38

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    new-array v1, v7, [Ljava/lang/String;

    const-string v2, "\u310f"

    aput-object v2, v1, v3

    const-string v2, "\u3112"

    aput-object v2, v1, v4

    const-string v2, "\u3120"

    aput-object v2, v1, v5

    const-string v2, "\u3129"

    aput-object v2, v1, v6

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImplTest$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    const/16 v0, 0x39

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    new-array v1, v7, [Ljava/lang/String;

    const-string v2, "\u3115"

    aput-object v2, v1, v3

    const-string v2, "\u3119"

    aput-object v2, v1, v4

    const-string v2, "\u3124"

    aput-object v2, v1, v5

    const-string v2, "\u3125"

    aput-object v2, v1, v6

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImplTest$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    const/16 v0, 0x30

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    new-array v1, v7, [Ljava/lang/String;

    const-string v2, "\u3108"

    aput-object v2, v1, v3

    const-string v2, "\u310c"

    aput-object v2, v1, v4

    const-string v2, "\u3121"

    aput-object v2, v1, v5

    const-string v2, "\u3116"

    aput-object v2, v1, v6

    invoke-virtual {p0, v0, v1}, Lcom/touchtype_fluency/util/SwiftKeySessionImplTest$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1
    return-void
.end method
