.class public Lcom/samsung/inputmethod/setting/SimeSetting;
.super Ljava/lang/Object;
.source "SimeSetting.java"


# static fields
#the value of this static final field might be set in the static constructor
.field static final synthetic $assertionsDisabled:Z = false

.field private static final ANDPY_CONFS_AUTO_CAPITALIZATION_KEY:Ljava/lang/String; = "samsungAutoCapitalization"

.field private static final ANDPY_CONFS_AUTO_FULL_STOP:Ljava/lang/String; = "samsungAutoFullStop"

.field private static final ANDPY_CONFS_CHAR_PREVIEW:Ljava/lang/String; = "samsungCharPreview"

.field private static final ANDPY_CONFS_CONVERT_KEY:Ljava/lang/String; = "samsungConvert"

.field private static final ANDPY_CONFS_COUNTRY_KEY:Ljava/lang/String; = "samsung_country"

.field private static final ANDPY_CONFS_DEFINE_KEY:Ljava/lang/String; = "samsungDefine"

.field private static final ANDPY_CONFS_ERROR_CORRECT:Ljava/lang/String; = "samsungErrorCorrect"

.field private static final ANDPY_CONFS_FLOATING_KEYBOARD:Ljava/lang/String; = "samsungFloatingKeyboard"

.field private static final ANDPY_CONFS_FULL_SENTENCE:Ljava/lang/String; = "samsungFullSentence"

.field private static final ANDPY_CONFS_FUZZY_ANGAN_KEY:Ljava/lang/String; = "samsungFuzzy_angan"

.field private static final ANDPY_CONFS_FUZZY_CHC_KEY:Ljava/lang/String; = "samsungFuzzy_chc"

.field private static final ANDPY_CONFS_FUZZY_ENGEN_KEY:Ljava/lang/String; = "samsungFuzzy_engen"

.field private static final ANDPY_CONFS_FUZZY_HF_KEY:Ljava/lang/String; = "samsungFuzzy_hf"

.field private static final ANDPY_CONFS_FUZZY_INGIN_KEY:Ljava/lang/String; = "samsungFuzzy_ingin"

.field private static final ANDPY_CONFS_FUZZY_NL_KEY:Ljava/lang/String; = "samsungFuzzy_nl"

.field private static final ANDPY_CONFS_FUZZY_RL_KEY:Ljava/lang/String; = "samsungFuzzy_rl"

.field private static final ANDPY_CONFS_FUZZY_SHS_KEY:Ljava/lang/String; = "samsungFuzzy_shs"

.field private static final ANDPY_CONFS_FUZZY_ZHZ_KEY:Ljava/lang/String; = "samsungFuzzy_zhz"

.field private static final ANDPY_CONFS_HANDWRITE_LINECOLOR_INDEX_KEY:Ljava/lang/String; = "samsunghandwrite_color_index"

.field private static final ANDPY_CONFS_HANDWRITE_LINECOLOR_KEY:Ljava/lang/String; = "samsunghandwrite_color"

.field private static final ANDPY_CONFS_HANDWRITE_LINEGRADIENT_ECOLOR_KEY:Ljava/lang/String; = "samsunghandwrite_linegradient_ecolor"

.field private static final ANDPY_CONFS_HANDWRITE_LINEGRADIENT_KEY:Ljava/lang/String; = "samsunghandwrite_linegradient"

.field private static final ANDPY_CONFS_HANDWRITE_LINEGRADIENT_MCOLOR_KEY:Ljava/lang/String; = "samsunghandwrite_linegradient_mcolor"

.field private static final ANDPY_CONFS_HANDWRITE_LINEGRADIENT_SCOLOR_KEY:Ljava/lang/String; = "samsunghandwrite_linegradient_scolor"

.field private static final ANDPY_CONFS_HANDWRITE_LINEWIDTH_INDEX_KEY:Ljava/lang/String; = "samsunghandwrite_width_index"

.field private static final ANDPY_CONFS_HANDWRITE_LINEWIDTH_KEY:Ljava/lang/String; = "samsunghandwrite_line"

.field private static final ANDPY_CONFS_HANDWRITE_MODE_KEY:Ljava/lang/String; = "samsunghandwrite_mode"

.field private static final ANDPY_CONFS_HANDWRITE_RECGTYPE_KEY:Ljava/lang/String; = "samsunghandwrite_type"

.field private static final ANDPY_CONFS_HANDWRITE_SPEED_KEY:Ljava/lang/String; = "samsunghandwrite_speed"

.field private static final ANDPY_CONFS_HANDWRITING_STYLE_KEY:Ljava/lang/String; = "samsungHandwritingStyle"

.field private static final ANDPY_CONFS_INPUTMODE_KEY:Ljava/lang/String; = "samsungLastInputMode"

.field private static final ANDPY_CONFS_KEYBOARD_TYPE_KEY:Ljava/lang/String; = "samsungKeyboardTypeInput"

.field private static final ANDPY_CONFS_KEYSOUND_KEY:Ljava/lang/String; = "samsungSound"

.field private static final ANDPY_CONFS_LANGUAGE_CHINESE_HK_KEY:Ljava/lang/String; = "samsungLanguageChineseHK"

.field private static final ANDPY_CONFS_LANGUAGE_CHINESE_KEY:Ljava/lang/String; = "samsungLanguageChinese"

.field private static final ANDPY_CONFS_LANGUAGE_CHINESE_TW_KEY:Ljava/lang/String; = "samsungLanguageChineseTW"

.field private static final ANDPY_CONFS_LANGUAGE_ENGLISH_KEY:Ljava/lang/String; = "samsungLanguageEnglish"

.field private static final ANDPY_CONFS_LANGUAGE_KOREAN_KEY:Ljava/lang/String; = "samsungLanguageKorean"

.field private static final ANDPY_CONFS_LATEST_SYMBS_KEY:Ljava/lang/String; = "samsungLatestSymbols"

.field private static final ANDPY_CONFS_LINK_TO_CONTACTS:Ljava/lang/String; = "samsungLinkToContacts"

.field private static final ANDPY_CONFS_MDB_PRIROTY:Ljava/lang/String; = "samsungMdbPrority"

.field private static final ANDPY_CONFS_MULTIMODAL_STATUS:Ljava/lang/String; = "samsungMultiModalStatus"

.field private static final ANDPY_CONFS_ONE_HAND_MODE:Ljava/lang/String; = "samsungOneHandMode"

.field private static final ANDPY_CONFS_POINTING_KEYBOARD:Ljava/lang/String; = "samsungPointingKeyboard"

.field private static final ANDPY_CONFS_PORTRAITINPUT_CHN_KEY:Ljava/lang/String; = "samsungPortraitInputChn"

.field private static final ANDPY_CONFS_PORTRAITINPUT_ENG_KEY:Ljava/lang/String; = "samsungPortraitInputEng"

.field private static final ANDPY_CONFS_PORTRAITINPUT_HK_KEY:Ljava/lang/String; = "samsungPortraitInputHK"

.field private static final ANDPY_CONFS_PORTRAITINPUT_KOR_KEY:Ljava/lang/String; = "samsungPortraitInputKor"

.field private static final ANDPY_CONFS_PORTRAITINPUT_TW_KEY:Ljava/lang/String; = "samsungPortraitInputTW"

.field private static final ANDPY_CONFS_PREDICTION_KEY:Ljava/lang/String; = "samsungPrediction"

.field private static final ANDPY_CONFS_RECENT_ONE_HAND_MODE:Ljava/lang/String; = "samsungRecentOneHandMode"

.field private static final ANDPY_CONFS_SHOWTOTURIAL_KEY:Ljava/lang/String; = "samsungime_showtoturial"

.field private static final ANDPY_CONFS_SPACE_INPUT_KEY:Ljava/lang/String; = "samsungSpaceInput"

.field private static final ANDPY_CONFS_SWIPE_ON_KEYBOARD:Ljava/lang/String; = "samsungSwipeOnKeyboard"

.field private static final ANDPY_CONFS_TABEDIT_123Symbol_KEY:Ljava/lang/String; = "samsungTabedit_123_symbol_checkbox"

.field private static final ANDPY_CONFS_TABEDIT_CANGJIE_QWERTY_KEY:Ljava/lang/String; = "samsungTabedit_cangjie_qwerty_checkbox"

.field private static final ANDPY_CONFS_TABEDIT_CHN_34_KEY:Ljava/lang/String; = "samsungTabedit_chn_34_checkbox"

.field private static final ANDPY_CONFS_TABEDIT_CHN_BOX_HANDWRITE_KEY:Ljava/lang/String; = "samsungTabedit_chn_box_handwrite_checkbox"

.field private static final ANDPY_CONFS_TABEDIT_CHN_FULL_HANDWRITE_KEY:Ljava/lang/String; = "samsungTabedit_chn_full_handwrite_checkbox"

.field private static final ANDPY_CONFS_TABEDIT_CHN_QWERTY_KEY:Ljava/lang/String; = "samsungTabedit_chn_qwerty_checkbox"

.field private static final ANDPY_CONFS_TABEDIT_CHN_STROKE_KEY:Ljava/lang/String; = "samsungTabedit_chn_stroke_checkbox"

.field private static final ANDPY_CONFS_TABEDIT_CLIPBOARD_KEY:Ljava/lang/String; = "samsungTabedit_clipboard_checkbox"

.field private static final ANDPY_CONFS_TABEDIT_EMOTICON_KEY:Ljava/lang/String; = "samsungTabedit_emoticon_checkbox"

.field private static final ANDPY_CONFS_TABEDIT_ENG_BOX_HANDWRITE_KEY:Ljava/lang/String; = "samsungTabedit_eng_box_handwrite_checkbox"

.field private static final ANDPY_CONFS_TABEDIT_ENG_FULL_HANDWRITE_KEY:Ljava/lang/String; = "samsungTabedit_eng_full_handwrite_checkbox"

.field private static final ANDPY_CONFS_TABEDIT_ENG_QWERTY_KEY:Ljava/lang/String; = "samsungTabedit_eng_qwerty_checkbox"

.field private static final ANDPY_CONFS_TABEDIT_HK_BOX_HANDWRITE_KEY:Ljava/lang/String; = "samsungTabedit_hk_half_box_checkbox"

.field private static final ANDPY_CONFS_TABEDIT_HK_FULL_HANDWRITE_KEY:Ljava/lang/String; = "samsungTabedit_hk_full_handwrite_checkbox"

.field private static final ANDPY_CONFS_TABEDIT_HK_STROKE_KEY:Ljava/lang/String; = "samsungTabedit_hk_stroke_checkbox"

.field private static final ANDPY_CONFS_TABEDIT_KOREAN_BOX_HANDWRITE_KEY:Ljava/lang/String; = "samsungTabedit_korean_box_handwrite_checkbox"

.field private static final ANDPY_CONFS_TABEDIT_KOREAN_FULL_HANDWRITE_KEY:Ljava/lang/String; = "samsungTabedit_korean_full_handwrite_checkbox"

.field private static final ANDPY_CONFS_TABEDIT_KOR_QWERTY_KEY:Ljava/lang/String; = "samsungTabedit_kor_qwerty_checkbox"

.field private static final ANDPY_CONFS_TABEDIT_TW_BOX_HANDWRITE_KEY:Ljava/lang/String; = "samsungTabedit_tw_half_box_checkbox"

.field private static final ANDPY_CONFS_TABEDIT_TW_FULL_HANDWRITE_KEY:Ljava/lang/String; = "samsungTabedit_tw_full_handwrite_checkbox"

.field private static final ANDPY_CONFS_TABEDIT_TW_STROKE_KEY:Ljava/lang/String; = "samsungTabedit_tw_stroke_checkbox"

.field private static final ANDPY_CONFS_TABEDIT_ZHUYIN_34_KEY:Ljava/lang/String; = "samsungTabedit_zhuyin_34_checkbox"

.field private static final ANDPY_CONFS_TABEDIT_ZHUYIN_QWERTY_KEY:Ljava/lang/String; = "samsungTabedit_zhuyin_qwerty_checkbox"

.field private static final ANDPY_CONFS_TABMODE_ORDER_KEY:Ljava/lang/String; = "samsungTabmode_order"

.field private static final ANDPY_CONFS_VIBRATE_KEY:Ljava/lang/String; = "samsungVibrate"

.field private static final ANDPY_CONFS_VOICE_INPUT_KEY:Ljava/lang/String; = "samsungVoiceInput"

.field private static final COUNTRY_CODE:Ljava/lang/String; = null

.field private static final DEBUG:Z = false

.field public static final FIRST_SWIFTKEY_DOWNLOAD_EXECUTION:Ljava/lang/String; = "first_swiftkey_download_execution"

.field public static final FIRST_SWIFTKEY_DOWNLOAD_LIST_EXECUTION:Ljava/lang/String; = "first_swiftkey_download_list_execution"

.field public static final INPUT_MODE_CHINESE:I = 0x1

.field public static final INPUT_MODE_ENGLISH:I = 0x0

.field public static final INPUT_MODE_HONGKONG:I = 0x4

.field public static final INPUT_MODE_KOREAN:I = 0x2

.field public static final INPUT_MODE_TAIWAN:I = 0x3

.field private static IS_BOX_EN:Z = false

.field private static final IS_BOX_EN_KEY:Ljava/lang/String; = "is_hwbox_en"

.field private static IS_BOX_KO:Z = false

.field private static final IS_BOX_KO_KEY:Ljava/lang/String; = "is_hwbox_ko"

.field private static IS_BOX_ZH_CN:Z = false

.field private static final IS_BOX_ZH_CN_KEY:Ljava/lang/String; = "is_hwbox_zh_cn"

.field private static IS_BOX_ZH_HK:Z = false

.field private static final IS_BOX_ZH_HK_KEY:Ljava/lang/String; = "is_hwbox_zh_hk"

.field private static IS_BOX_ZH_TW:Z = false

.field private static final IS_BOX_ZH_TW_KEY:Ljava/lang/String; = "is_hwbox_zh_tw"

.field private static IS_STROKE_ZH_CN:Z = false

.field private static final IS_STROKE_ZH_CN_KEY:Ljava/lang/String; = "is_stroke_zh_cn"

.field private static IS_STROKE_ZH_TW:Z = false

.field private static final IS_STROKE_ZH_TW_KEY:Ljava/lang/String; = "is_stroke_zh_tw"

.field public static final KEY_FLOATING_H_LOCATION_X:Ljava/lang/String; = "floating_h_location_x"

.field public static final KEY_FLOATING_H_LOCATION_Y:Ljava/lang/String; = "floating_h_location_y"

.field public static final KEY_FLOATING_LOCATION_X:Ljava/lang/String; = "floating_location_x"

.field public static final KEY_FLOATING_LOCATION_Y:Ljava/lang/String; = "floating_location_y"

.field private static MAX_LATEST_SYM_COUNT:I = 0x0

.field public static final SWIFTKEY_DOWNLOAD_EXECUTION:Ljava/lang/String; = "swiftkey_download_execution"

.field public static final SWIFTKEY_DOWNLOAD_LIST_EXECUTION:Ljava/lang/String; = "swiftkey_download_list_execution"

.field private static SYM_HISTORY_SPLIT_STR:Ljava/lang/String; = null

.field private static final TAG:Ljava/lang/String; = "SamsungIMESettings"

.field private static mAdapt:Z

.field private static mAutoCapitalizationPref:Z

.field private static mAutoFullStop:Z

.field private static mCharPreview:Z

.field private static mChineseAssociationPref:Z

.field private static mConvert:I

.field private static mCountry:Ljava/lang/String;

.field private static mDefine:Z

.field private static mFloatingKeyboard:Z

.field private static mFuzzyState:I

.field private static mFuzzyStateFlag:[Z

.field private static mHWModeIndex:I

.field private static mHandwritingStyle:I

.field private static mInstance:Lcom/samsung/inputmethod/setting/SimeSetting;

.field private static mIsLineGradient:Z

.field private static mIsTabletModel:Z

.field private static mKeySound:Z

.field private static mKeyboardType:I

.field private static mLanguageChinese:Z

.field private static mLanguageChineseHk:Z

.field private static mLanguageChineseTw:Z

.field private static mLanguageEnglish:Z

.field private static mLanguageKorean:Z

.field private static mLastInputMode:I

.field private static mLineColorIndex:I

.field private static mLineColors:[I

.field private static mLineGradientEndcolor:I

.field private static mLineGradientMiddlecolor:I

.field private static mLineGradientStartcolor:I

.field private static mLineWidth:[I

.field private static mLineWidthIndex:I

.field private static mLinkToContacts:Z

.field private static mMultiModalStatus:I

.field private static mPointingKeyboard:Z

.field private static mPortraitInputlist:[I

.field private static mPrediction:Z

.field private static mRecgType:I

.field private static mRefCount:I

.field private static mSharedPref:Landroid/content/SharedPreferences;

.field private static mSpaceInput:Z

.field private static mSpeed:I

.field private static mSwipeOnKeyboard:I

.field private static mSymHistory:Ljava/lang/String;

.field private static mSymHistoryDefaultVal:[Ljava/lang/String;

.field private static mSymHistoryList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static mTabEditList_Checkbox:[Z

.field private static mTabModeOrder:Ljava/lang/String;

.field private static mVibrate:Z

.field private static mVoiceInputOption:Z


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v1, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x5

    const/4 v2, 0x0

    .line 44
    const-class v0, Lcom/samsung/inputmethod/setting/SimeSetting;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/samsung/inputmethod/setting/SimeSetting;->$assertionsDisabled:Z

    .line 48
    const-string v0, "ro.csc.country_code"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/inputmethod/setting/SimeSetting;->COUNTRY_CODE:Ljava/lang/String;

    .line 200
    new-array v0, v6, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mLineColors:[I

    .line 204
    new-array v0, v4, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mLineWidth:[I

    .line 209
    new-array v0, v4, [I

    sput-object v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mPortraitInputlist:[I

    .line 218
    const/16 v0, 0x9

    new-array v0, v0, [Z

    sput-object v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mFuzzyStateFlag:[Z

    .line 255
    const/16 v0, 0x36

    sput v0, Lcom/samsung/inputmethod/setting/SimeSetting;->MAX_LATEST_SYM_COUNT:I

    .line 256
    const-string v0, "SPLT"

    sput-object v0, Lcom/samsung/inputmethod/setting/SimeSetting;->SYM_HISTORY_SPLIT_STR:Ljava/lang/String;

    .line 257
    const/16 v0, 0x36

    new-array v0, v0, [Ljava/lang/String;

    const-string v3, "\uff0c"

    aput-object v3, v0, v2

    const-string v3, "\u3002"

    aput-object v3, v0, v1

    const/4 v1, 0x2

    const-string v3, "\uff1f"

    aput-object v3, v0, v1

    const/4 v1, 0x3

    const-string v3, "\uff01"

    aput-object v3, v0, v1

    const-string v1, "\u3001"

    aput-object v1, v0, v6

    const-string v1, "\u2026\u2026"

    aput-object v1, v0, v4

    const/4 v1, 0x6

    const-string v3, "@"

    aput-object v3, v0, v1

    const/4 v1, 0x7

    const-string v3, "\uff1a"

    aput-object v3, v0, v1

    const/16 v1, 0x8

    const-string v3, "\uff1b"

    aput-object v3, v0, v1

    const/16 v1, 0x9

    const-string v3, "\uff06"

    aput-object v3, v0, v1

    const/16 v1, 0xa

    const-string v3, "^"

    aput-object v3, v0, v1

    const/16 v1, 0xb

    const-string v3, "~"

    aput-object v3, v0, v1

    const/16 v1, 0xc

    const-string v3, "\u201c\u201d"

    aput-object v3, v0, v1

    const/16 v1, 0xd

    const-string v3, "\u201c"

    aput-object v3, v0, v1

    const/16 v1, 0xe

    const-string v3, "\u201d"

    aput-object v3, v0, v1

    const/16 v1, 0xf

    const-string v3, "\uff08\uff09"

    aput-object v3, v0, v1

    const/16 v1, 0x10

    const-string v3, "\uff08"

    aput-object v3, v0, v1

    const/16 v1, 0x11

    const-string v3, "\uff09"

    aput-object v3, v0, v1

    const/16 v1, 0x12

    const-string v3, "*"

    aput-object v3, v0, v1

    const/16 v1, 0x13

    const-string v3, "#"

    aput-object v3, v0, v1

    const/16 v1, 0x14

    const-string v3, "%"

    aput-object v3, v0, v1

    const/16 v1, 0x15

    const-string v3, "+"

    aput-object v3, v0, v1

    const/16 v1, 0x16

    const-string v3, "-"

    aput-object v3, v0, v1

    const/16 v1, 0x17

    const-string v3, "_"

    aput-object v3, v0, v1

    const/16 v1, 0x18

    const-string v3, "="

    aput-object v3, v0, v1

    const/16 v1, 0x19

    const-string v3, "/"

    aput-object v3, v0, v1

    const/16 v1, 0x1a

    const-string v3, "\u00b7"

    aput-object v3, v0, v1

    const/16 v1, 0x1b

    const-string v3, "\u00a5"

    aput-object v3, v0, v1

    const/16 v1, 0x1c

    const-string v3, "$"

    aput-object v3, v0, v1

    const/16 v1, 0x1d

    const-string v3, "\u20a9"

    aput-object v3, v0, v1

    const/16 v1, 0x1e

    const-string v3, "\u00a3"

    aput-object v3, v0, v1

    const/16 v1, 0x1f

    const-string v3, "\u20ac"

    aput-object v3, v0, v1

    const/16 v1, 0x20

    const-string v3, "\u203b"

    aput-object v3, v0, v1

    const/16 v1, 0x21

    const-string v3, "\u00d7"

    aput-object v3, v0, v1

    const/16 v1, 0x22

    const-string v3, "\u00f7"

    aput-object v3, v0, v1

    const/16 v1, 0x23

    const-string v3, "\u00b0"

    aput-object v3, v0, v1

    const/16 v1, 0x24

    const-string v3, "\u300a\u300b"

    aput-object v3, v0, v1

    const/16 v1, 0x25

    const-string v3, "\u300a"

    aput-object v3, v0, v1

    const/16 v1, 0x26

    const-string v3, "\u300b"

    aput-object v3, v0, v1

    const/16 v1, 0x27

    const-string v3, "\uff5b\uff5d"

    aput-object v3, v0, v1

    const/16 v1, 0x28

    const-string v3, "\uff5b"

    aput-object v3, v0, v1

    const/16 v1, 0x29

    const-string v3, "\uff5d"

    aput-object v3, v0, v1

    const/16 v1, 0x2a

    const-string v3, "\u3010\u3011"

    aput-object v3, v0, v1

    const/16 v1, 0x2b

    const-string v3, "\u3010"

    aput-object v3, v0, v1

    const/16 v1, 0x2c

    const-string v3, "\u3011"

    aput-object v3, v0, v1

    const/16 v1, 0x2d

    const-string v3, "\uff1c\uff1e"

    aput-object v3, v0, v1

    const/16 v1, 0x2e

    const-string v3, "\uff1c"

    aput-object v3, v0, v1

    const/16 v1, 0x2f

    const-string v3, "\uff1e"

    aput-object v3, v0, v1

    const/16 v1, 0x30

    const-string v3, "\u300c\u300d"

    aput-object v3, v0, v1

    const/16 v1, 0x31

    const-string v3, "\u300c"

    aput-object v3, v0, v1

    const/16 v1, 0x32

    const-string v3, "\u300d"

    aput-object v3, v0, v1

    const/16 v1, 0x33

    const-string v3, "\u2018\u2019"

    aput-object v3, v0, v1

    const/16 v1, 0x34

    const-string v3, "\u2018"

    aput-object v3, v0, v1

    const/16 v1, 0x35

    const-string v3, "\u2019"

    aput-object v3, v0, v1

    sput-object v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mSymHistoryDefaultVal:[Ljava/lang/String;

    .line 267
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    sput-object v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mSymHistory:Ljava/lang/String;

    .line 268
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mSymHistoryList:Ljava/util/ArrayList;

    .line 270
    sput-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mInstance:Lcom/samsung/inputmethod/setting/SimeSetting;

    .line 272
    sput v2, Lcom/samsung/inputmethod/setting/SimeSetting;->mRefCount:I

    .line 274
    sput-boolean v2, Lcom/samsung/inputmethod/setting/SimeSetting;->mIsTabletModel:Z

    .line 276
    sput-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    .line 277
    sput-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mCountry:Ljava/lang/String;

    return-void

    :cond_0
    move v0, v2

    .line 44
    goto/16 :goto_0

    .line 200
    :array_0
    .array-data 0x4
        0x0t 0x0t 0xfft 0xfft
        0xfft 0x0t 0x0t 0xfft
        0x0t 0x80t 0x0t 0xfft
        0x13t 0x45t 0x8bt 0xfft
    .end array-data

    .line 204
    :array_1
    .array-data 0x4
        0x2t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
        0x7t 0x0t 0x0t 0x0t
        0x9t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method protected constructor <init>(Landroid/content/SharedPreferences;)V
    .locals 1
    .parameter "pref"

    .prologue
    .line 279
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 280
    sput-object p1, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    .line 281
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/inputmethod/setting/SimeSetting;->initConfs(Z)V

    .line 282
    return-void
.end method

.method public static GetSwipeOnKeyboard()I
    .locals 1

    .prologue
    .line 1784
    sget v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mSwipeOnKeyboard:I

    return v0
.end method

.method public static SaveLastInputInfo(I)V
    .locals 3
    .parameter "lastInputMode"

    .prologue
    .line 1757
    const-string v0, "samsungLastInputMode"

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/inputmethod/setting/SimeSetting;->savePrefrence(Ljava/lang/String;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1758
    sput p0, Lcom/samsung/inputmethod/setting/SimeSetting;->mLastInputMode:I

    .line 1760
    :cond_0
    const-string v0, "samsungPortraitInputEng"

    sget-object v1, Lcom/samsung/inputmethod/setting/SimeSetting;->mPortraitInputlist:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/inputmethod/setting/SimeSetting;->savePrefrence(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 1761
    const-string v0, "samsungPortraitInputChn"

    sget-object v1, Lcom/samsung/inputmethod/setting/SimeSetting;->mPortraitInputlist:[I

    const/4 v2, 0x1

    aget v1, v1, v2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/inputmethod/setting/SimeSetting;->savePrefrence(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 1762
    const-string v0, "samsungPortraitInputKor"

    sget-object v1, Lcom/samsung/inputmethod/setting/SimeSetting;->mPortraitInputlist:[I

    const/4 v2, 0x2

    aget v1, v1, v2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/inputmethod/setting/SimeSetting;->savePrefrence(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 1763
    const-string v0, "samsungPortraitInputTW"

    sget-object v1, Lcom/samsung/inputmethod/setting/SimeSetting;->mPortraitInputlist:[I

    const/4 v2, 0x3

    aget v1, v1, v2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/inputmethod/setting/SimeSetting;->savePrefrence(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 1764
    const-string v0, "samsungPortraitInputHK"

    sget-object v1, Lcom/samsung/inputmethod/setting/SimeSetting;->mPortraitInputlist:[I

    const/4 v2, 0x4

    aget v1, v1, v2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/inputmethod/setting/SimeSetting;->savePrefrence(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 1767
    return-void
.end method

.method public static SetAutoFullStop(Z)V
    .locals 1
    .parameter "v"

    .prologue
    .line 1186
    sget-boolean v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mAutoFullStop:Z

    if-ne v0, p0, :cond_0

    .line 1188
    :goto_0
    return-void

    .line 1187
    :cond_0
    sput-boolean p0, Lcom/samsung/inputmethod/setting/SimeSetting;->mAutoFullStop:Z

    goto :goto_0
.end method

.method public static SetErrorCorrect(Z)V
    .locals 2
    .parameter "v"

    .prologue
    .line 1222
    const-string v0, "samsungErrorCorrect"

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/inputmethod/setting/SimeSetting;->savePrefrence(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 1223
    return-void
.end method

.method public static SetFloatingKeyboard(Z)V
    .locals 1
    .parameter "v"

    .prologue
    .line 1213
    sget-boolean v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mFloatingKeyboard:Z

    if-ne v0, p0, :cond_0

    .line 1215
    :goto_0
    return-void

    .line 1214
    :cond_0
    sput-boolean p0, Lcom/samsung/inputmethod/setting/SimeSetting;->mFloatingKeyboard:Z

    goto :goto_0
.end method

.method public static SetLanguageChinese(Z)V
    .locals 1
    .parameter "v"

    .prologue
    .line 1702
    sget-boolean v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mLanguageChinese:Z

    if-ne v0, p0, :cond_0

    .line 1704
    :goto_0
    return-void

    .line 1703
    :cond_0
    sput-boolean p0, Lcom/samsung/inputmethod/setting/SimeSetting;->mLanguageChinese:Z

    goto :goto_0
.end method

.method public static SetLanguageChineseHk(Z)V
    .locals 1
    .parameter "v"

    .prologue
    .line 1721
    sget-boolean v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mLanguageChineseHk:Z

    if-ne v0, p0, :cond_0

    .line 1723
    :goto_0
    return-void

    .line 1722
    :cond_0
    sput-boolean p0, Lcom/samsung/inputmethod/setting/SimeSetting;->mLanguageChineseHk:Z

    goto :goto_0
.end method

.method public static SetLanguageChineseTw(Z)V
    .locals 1
    .parameter "v"

    .prologue
    .line 1712
    sget-boolean v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mLanguageChineseTw:Z

    if-ne v0, p0, :cond_0

    .line 1714
    :goto_0
    return-void

    .line 1713
    :cond_0
    sput-boolean p0, Lcom/samsung/inputmethod/setting/SimeSetting;->mLanguageChineseTw:Z

    goto :goto_0
.end method

.method public static SetLanguageEnglish(Z)V
    .locals 1
    .parameter "v"

    .prologue
    .line 1732
    sget-boolean v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mLanguageEnglish:Z

    if-ne v0, p0, :cond_0

    .line 1734
    :goto_0
    return-void

    .line 1733
    :cond_0
    sput-boolean p0, Lcom/samsung/inputmethod/setting/SimeSetting;->mLanguageEnglish:Z

    goto :goto_0
.end method

.method public static SetLanguageKorean(Z)V
    .locals 1
    .parameter "v"

    .prologue
    .line 1741
    sget-boolean v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mLanguageKorean:Z

    if-ne v0, p0, :cond_0

    .line 1743
    :goto_0
    return-void

    .line 1742
    :cond_0
    sput-boolean p0, Lcom/samsung/inputmethod/setting/SimeSetting;->mLanguageKorean:Z

    goto :goto_0
.end method

.method public static SetLinkToContacts(Z)V
    .locals 1
    .parameter "v"

    .prologue
    .line 1203
    sget-boolean v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mLinkToContacts:Z

    if-ne v0, p0, :cond_0

    .line 1205
    :goto_0
    return-void

    .line 1204
    :cond_0
    sput-boolean p0, Lcom/samsung/inputmethod/setting/SimeSetting;->mLinkToContacts:Z

    goto :goto_0
.end method

.method public static SetMdbPrority(Z)V
    .locals 2
    .parameter "v"

    .prologue
    .line 1230
    const-string v0, "samsungMdbPrority"

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/inputmethod/setting/SimeSetting;->savePrefrence(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 1231
    return-void
.end method

.method public static SetPointingKeyboard(Z)V
    .locals 1
    .parameter "v"

    .prologue
    .line 1208
    sget-boolean v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mPointingKeyboard:Z

    if-ne v0, p0, :cond_0

    .line 1210
    :goto_0
    return-void

    .line 1209
    :cond_0
    sput-boolean p0, Lcom/samsung/inputmethod/setting/SimeSetting;->mPointingKeyboard:Z

    goto :goto_0
.end method

.method public static SetSpaceKeyInputAssociate(Z)V
    .locals 1
    .parameter "v"

    .prologue
    .line 1141
    sget-boolean v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mSpaceInput:Z

    if-ne v0, p0, :cond_0

    .line 1143
    :goto_0
    return-void

    .line 1142
    :cond_0
    sput-boolean p0, Lcom/samsung/inputmethod/setting/SimeSetting;->mSpaceInput:Z

    goto :goto_0
.end method

.method public static SetSwipeOnKeyboard(I)V
    .locals 0
    .parameter "id"

    .prologue
    .line 1788
    sput p0, Lcom/samsung/inputmethod/setting/SimeSetting;->mSwipeOnKeyboard:I

    .line 1789
    return-void
.end method

.method public static SetVoiceInput(Z)V
    .locals 1
    .parameter "v"

    .prologue
    .line 1150
    sget-boolean v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mVoiceInputOption:Z

    if-ne v0, p0, :cond_0

    .line 1152
    :goto_0
    return-void

    .line 1151
    :cond_0
    sput-boolean p0, Lcom/samsung/inputmethod/setting/SimeSetting;->mVoiceInputOption:Z

    goto :goto_0
.end method

.method public static getAutoCapitalization()Z
    .locals 1

    .prologue
    .line 1193
    sget-boolean v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mAutoCapitalizationPref:Z

    return v0
.end method

.method public static getAutoFullStop()Z
    .locals 1

    .prologue
    .line 1170
    sget-boolean v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mAutoFullStop:Z

    return v0
.end method

.method public static getCharPreview()Z
    .locals 1

    .prologue
    .line 1157
    sget-boolean v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mCharPreview:Z

    return v0
.end method

.method public static getConvert()I
    .locals 1

    .prologue
    .line 1242
    sget v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mConvert:I

    return v0
.end method

.method private getCountry()Ljava/lang/String;
    .locals 6

    .prologue
    .line 593
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v0

    .line 594
    .local v0, am:Landroid/app/IActivityManager;
    const/4 v1, 0x0

    .line 596
    .local v1, curLocale:Ljava/util/Locale;
    :try_start_0
    invoke-interface {v0}, Landroid/app/IActivityManager;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget-object v1, v3, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 601
    :goto_0
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mCountry:Ljava/lang/String;

    if-nez v3, :cond_0

    .line 602
    invoke-virtual {v1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mCountry:Ljava/lang/String;

    .line 605
    :goto_1
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mCountry:Ljava/lang/String;

    return-object v3

    .line 597
    :catch_0
    move-exception v2

    .line 598
    .local v2, e:Landroid/os/RemoteException;
    const-string v3, "SamsungIMESettings"

    const-string v4, "initConfs curLocale throw exception"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 604
    .end local v2           #e:Landroid/os/RemoteException;
    :cond_0
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v4, "samsung_country"

    const-string v5, "CN"

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mCountry:Ljava/lang/String;

    goto :goto_1
.end method

.method public static getDefaultLangKeyPadMode(I)I
    .locals 1
    .parameter "settingLangVal"

    .prologue
    .line 1077
    const/4 v0, 0x0

    .line 1078
    .local v0, defaultKeyPadMode:I
    packed-switch p0, :pswitch_data_0

    .line 1097
    :goto_0
    return v0

    .line 1080
    :pswitch_0
    const/high16 v0, 0x1111

    .line 1081
    goto :goto_0

    .line 1083
    :pswitch_1
    const/high16 v0, 0x1202

    .line 1084
    goto :goto_0

    .line 1086
    :pswitch_2
    const/high16 v0, -0x6efb

    .line 1087
    goto :goto_0

    .line 1089
    :pswitch_3
    const/high16 v0, 0x2104

    .line 1090
    goto :goto_0

    .line 1092
    :pswitch_4
    const/high16 v0, 0x1203

    .line 1093
    goto :goto_0

    .line 1078
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_4
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public static getDefine()Z
    .locals 1

    .prologue
    .line 1251
    sget-boolean v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mDefine:Z

    return v0
.end method

.method public static getErrorCorrect()Z
    .locals 3

    .prologue
    .line 1218
    sget-object v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v1, "samsungErrorCorrect"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static getFloatingKeyboard()Z
    .locals 1

    .prologue
    .line 1182
    sget-boolean v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mFloatingKeyboard:Z

    return v0
.end method

.method public static getFloatingKeyboardXPosition(Z)I
    .locals 3
    .parameter "isHorizontal"

    .prologue
    const/4 v2, 0x0

    .line 1828
    if-eqz p0, :cond_0

    .line 1829
    sget-object v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v1, "floating_h_location_x"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 1831
    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v1, "floating_location_x"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    goto :goto_0
.end method

.method public static getFloatingKeyboardYPosition(Z)I
    .locals 3
    .parameter "isHorizontal"

    .prologue
    const/4 v2, 0x0

    .line 1821
    if-eqz p0, :cond_0

    .line 1822
    sget-object v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v1, "floating_h_location_y"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 1824
    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v1, "floating_location_y"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    goto :goto_0
.end method

.method public static getFullSentence()Z
    .locals 3

    .prologue
    .line 1234
    sget-object v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v1, "samsungFullSentence"

    sget-boolean v2, Lcom/samsung/inputmethod/comm/SimeFeature;->SIME_FULL_SENTENCE:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static getFuzzyPinyinState()I
    .locals 1

    .prologue
    .line 1394
    sget v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mFuzzyState:I

    return v0
.end method

.method public static getFuzzyPinyinState([Z)V
    .locals 2
    .parameter "state"

    .prologue
    .line 1385
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    const/16 v1, 0x9

    if-ge v0, v1, :cond_0

    .line 1386
    sget-object v1, Lcom/samsung/inputmethod/setting/SimeSetting;->mFuzzyStateFlag:[Z

    aget-boolean v1, v1, v0

    aput-boolean v1, p0, v0

    .line 1385
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1389
    :cond_0
    return-void
.end method

.method public static getHandwriteLine([I)V
    .locals 3
    .parameter "pt"

    .prologue
    .line 1286
    const/4 v0, 0x0

    sget-object v1, Lcom/samsung/inputmethod/setting/SimeSetting;->mLineWidth:[I

    sget v2, Lcom/samsung/inputmethod/setting/SimeSetting;->mLineWidthIndex:I

    aget v1, v1, v2

    aput v1, p0, v0

    .line 1287
    const/4 v0, 0x1

    sget-object v1, Lcom/samsung/inputmethod/setting/SimeSetting;->mLineColors:[I

    sget v2, Lcom/samsung/inputmethod/setting/SimeSetting;->mLineColorIndex:I

    aget v1, v1, v2

    aput v1, p0, v0

    .line 1288
    return-void
.end method

.method public static getHandwriteLineColor()I
    .locals 2

    .prologue
    .line 1296
    sget-object v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mLineColors:[I

    sget v1, Lcom/samsung/inputmethod/setting/SimeSetting;->mLineColorIndex:I

    aget v0, v0, v1

    return v0
.end method

.method public static getHandwriteLineColorIndex()I
    .locals 1

    .prologue
    .line 1302
    sget v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mLineColorIndex:I

    return v0
.end method

.method public static getHandwriteLineGradient([I)Z
    .locals 2
    .parameter "pt"

    .prologue
    .line 1681
    const/4 v0, 0x0

    sget v1, Lcom/samsung/inputmethod/setting/SimeSetting;->mLineGradientStartcolor:I

    aput v1, p0, v0

    .line 1682
    const/4 v0, 0x1

    sget v1, Lcom/samsung/inputmethod/setting/SimeSetting;->mLineGradientMiddlecolor:I

    aput v1, p0, v0

    .line 1683
    const/4 v0, 0x2

    sget v1, Lcom/samsung/inputmethod/setting/SimeSetting;->mLineGradientEndcolor:I

    aput v1, p0, v0

    .line 1684
    sget-boolean v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mIsLineGradient:Z

    return v0
.end method

.method public static getHandwriteLineWidth()I
    .locals 2

    .prologue
    .line 1291
    sget-object v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mLineWidth:[I

    sget v1, Lcom/samsung/inputmethod/setting/SimeSetting;->mLineWidthIndex:I

    aget v0, v0, v1

    return v0
.end method

.method public static getHandwriteLineWidthIndex()I
    .locals 1

    .prologue
    .line 1375
    sget v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mLineWidthIndex:I

    return v0
.end method

.method public static getHandwriteRecgType()I
    .locals 1

    .prologue
    .line 1673
    sget v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mRecgType:I

    return v0
.end method

.method public static getHandwritingStyle()I
    .locals 1

    .prologue
    .line 1277
    sget v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mHandwritingStyle:I

    return v0
.end method

.method public static getInstance(Landroid/content/SharedPreferences;)Lcom/samsung/inputmethod/setting/SimeSetting;
    .locals 1
    .parameter "pref"

    .prologue
    .line 285
    sget-object v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mInstance:Lcom/samsung/inputmethod/setting/SimeSetting;

    if-nez v0, :cond_0

    .line 286
    new-instance v0, Lcom/samsung/inputmethod/setting/SimeSetting;

    invoke-direct {v0, p0}, Lcom/samsung/inputmethod/setting/SimeSetting;-><init>(Landroid/content/SharedPreferences;)V

    sput-object v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mInstance:Lcom/samsung/inputmethod/setting/SimeSetting;

    .line 288
    :cond_0
    sget-boolean v0, Lcom/samsung/inputmethod/setting/SimeSetting;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    sget-object v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    invoke-virtual {p0, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 289
    :cond_1
    sget v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mRefCount:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mRefCount:I

    .line 290
    sget-object v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mInstance:Lcom/samsung/inputmethod/setting/SimeSetting;

    return-object v0
.end method

.method public static getKeySound()Z
    .locals 1

    .prologue
    .line 1101
    sget-boolean v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mKeySound:Z

    return v0
.end method

.method public static getKeyboardType()I
    .locals 1

    .prologue
    .line 1268
    sget v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mKeyboardType:I

    return v0
.end method

.method public static getLanguageChinese()Z
    .locals 1

    .prologue
    .line 1698
    sget-boolean v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mLanguageChinese:Z

    return v0
.end method

.method public static getLanguageChineseHk()Z
    .locals 1

    .prologue
    .line 1717
    sget-boolean v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mLanguageChineseHk:Z

    return v0
.end method

.method public static getLanguageChineseTw()Z
    .locals 1

    .prologue
    .line 1708
    sget-boolean v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mLanguageChineseTw:Z

    return v0
.end method

.method public static getLanguageEnglish()Z
    .locals 1

    .prologue
    .line 1728
    sget-boolean v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mLanguageEnglish:Z

    return v0
.end method

.method public static getLanguageKorean()Z
    .locals 1

    .prologue
    .line 1737
    sget-boolean v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mLanguageKorean:Z

    return v0
.end method

.method public static getLastInputMode()I
    .locals 1

    .prologue
    .line 1693
    sget v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mLastInputMode:I

    return v0
.end method

.method static getLayoutId(I)I
    .locals 5
    .parameter "languageId"

    .prologue
    .line 1635
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mPortraitInputlist:[I

    aget v2, v3, p0

    .line 1636
    .local v2, mode:I
    const/high16 v3, 0xf00

    and-int v0, v2, v3

    .line 1637
    .local v0, layout:I
    const/4 v1, 0x0

    .line 1638
    .local v1, layoutId:I
    sparse-switch v0, :sswitch_data_0

    .line 1662
    const/4 v1, 0x0

    .line 1665
    :goto_0
    return v1

    .line 1640
    :sswitch_0
    const/4 v1, 0x0

    .line 1641
    goto :goto_0

    .line 1643
    :sswitch_1
    const/4 v3, 0x1

    if-eq p0, v3, :cond_0

    const/4 v3, 0x3

    if-eq p0, v3, :cond_0

    const/4 v3, 0x4

    if-ne p0, v3, :cond_1

    :cond_0
    const/high16 v3, -0x1000

    and-int/2addr v3, v2

    const/high16 v4, 0x3000

    if-ne v3, v4, :cond_1

    .line 1645
    const/4 v1, 0x2

    goto :goto_0

    .line 1647
    :cond_1
    const/4 v1, 0x1

    .line 1649
    goto :goto_0

    .line 1651
    :sswitch_2
    const/4 v1, 0x3

    .line 1652
    goto :goto_0

    .line 1654
    :sswitch_3
    invoke-static {}, Lcom/samsung/inputmethod/softkeyboard/SimeSkbManager;->getKeyboardType()Lcom/samsung/inputmethod/softkeyboard/SimeSkbManager$KeyboardType;

    move-result-object v3

    sget-object v4, Lcom/samsung/inputmethod/softkeyboard/SimeSkbManager$KeyboardType;->TYPE_FLOATING:Lcom/samsung/inputmethod/softkeyboard/SimeSkbManager$KeyboardType;

    if-ne v3, v4, :cond_2

    invoke-static {p0}, Lcom/samsung/inputmethod/setting/SimeSetting;->isHWBoxMode(I)Z

    move-result v3

    if-nez v3, :cond_2

    .line 1656
    const/4 v1, 0x3

    goto :goto_0

    .line 1658
    :cond_2
    const/4 v1, 0x4

    .line 1660
    goto :goto_0

    .line 1638
    :sswitch_data_0
    .sparse-switch
        0x1000000 -> :sswitch_0
        0x2000000 -> :sswitch_1
        0x3000000 -> :sswitch_2
        0x4000000 -> :sswitch_3
    .end sparse-switch
.end method

.method public static getLinkToContacts()Z
    .locals 1

    .prologue
    .line 1174
    sget-boolean v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mLinkToContacts:Z

    return v0
.end method

.method public static getMdbPrority()Z
    .locals 3

    .prologue
    .line 1226
    sget-object v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v1, "samsungMdbPrority"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static getMultiModalStatus()I
    .locals 1

    .prologue
    .line 1128
    sget v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mMultiModalStatus:I

    return v0
.end method

.method public static getPointingKeyboard()Z
    .locals 1

    .prologue
    .line 1178
    sget-boolean v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mPointingKeyboard:Z

    return v0
.end method

.method public static getPortraitInputMode(I)I
    .locals 1
    .parameter "languageId"

    .prologue
    .line 1479
    sget-object v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mPortraitInputlist:[I

    aget v0, v0, p0

    return v0
.end method

.method public static getPrediction()Z
    .locals 1

    .prologue
    .line 1119
    sget-boolean v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mPrediction:Z

    return v0
.end method

.method public static getRecentOnehandMode()I
    .locals 3

    .prologue
    .line 1803
    sget-object v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v1, "samsungRecentOneHandMode"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static getShowToturial()Z
    .locals 3

    .prologue
    .line 1749
    sget-object v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v1, "samsungime_showtoturial"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static getSpaceKeyInputAssociate()Z
    .locals 1

    .prologue
    .line 1137
    sget-boolean v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mSpaceInput:Z

    return v0
.end method

.method public static getSpeed()I
    .locals 1

    .prologue
    .line 1259
    sget v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mSpeed:I

    return v0
.end method

.method public static getSymbHistoryList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 681
    sget-object v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mSymHistoryList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public static getTabBarOrder()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1063
    sget-object v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabModeOrder:Ljava/lang/String;

    return-object v0
.end method

.method public static getTabInputModeCheckedVaule()[Z
    .locals 1

    .prologue
    .line 1047
    sget-object v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    return-object v0
.end method

.method public static getVibrate()Z
    .locals 1

    .prologue
    .line 1110
    sget-boolean v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mVibrate:Z

    return v0
.end method

.method public static getVoiceInput()Z
    .locals 1

    .prologue
    .line 1146
    sget-boolean v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mVoiceInputOption:Z

    return v0
.end method

.method private initConfs(Z)V
    .locals 14
    .parameter "fromReset"

    .prologue
    const/4 v13, 0x3

    const/4 v12, 0x4

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 413
    invoke-direct {p0}, Lcom/samsung/inputmethod/setting/SimeSetting;->getCountry()Ljava/lang/String;

    move-result-object v0

    .line 415
    .local v0, country:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/inputmethod/service/xt9/SimeDecoderService;->imIsHKTWModel()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 416
    const-string v5, "HK"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 417
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v6, "samsungLanguageChinese"

    invoke-interface {v5, v6, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    sput-boolean v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mLanguageChinese:Z

    .line 418
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v6, "samsungLanguageChineseTW"

    invoke-interface {v5, v6, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    sput-boolean v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mLanguageChineseTw:Z

    .line 419
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v6, "samsungLanguageChineseHK"

    invoke-interface {v5, v6, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    sput-boolean v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mLanguageChineseHk:Z

    .line 420
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v6, "samsungLastInputMode"

    const/high16 v7, -0x6efb

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v5

    sput v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mLastInputMode:I

    .line 439
    :goto_0
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v6, "samsungLanguageEnglish"

    invoke-interface {v5, v6, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    sput-boolean v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mLanguageEnglish:Z

    .line 440
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v6, "samsungLanguageKorean"

    invoke-interface {v5, v6, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    sput-boolean v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mLanguageKorean:Z

    .line 441
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v6, "samsungVoiceInput"

    invoke-interface {v5, v6, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    sput-boolean v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mVoiceInputOption:Z

    .line 443
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v6, "samsungKeyboardTypeInput"

    invoke-interface {v5, v6, v9}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v5

    sput v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mKeyboardType:I

    .line 444
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v6, "samsungHandwritingStyle"

    invoke-interface {v5, v6, v9}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v5

    sput v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mHandwritingStyle:I

    .line 446
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v6, "samsungSound"

    invoke-interface {v5, v6, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    sput-boolean v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mKeySound:Z

    .line 447
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v6, "samsungVibrate"

    invoke-interface {v5, v6, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    sput-boolean v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mVibrate:Z

    .line 449
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v6, "samsungPrediction"

    invoke-interface {v5, v6, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    sput-boolean v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mPrediction:Z

    .line 450
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v6, "samsungAutoCapitalization"

    invoke-interface {v5, v6, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    sput-boolean v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mAutoCapitalizationPref:Z

    .line 451
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v6, "samsungSpaceInput"

    invoke-interface {v5, v6, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    sput-boolean v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mSpaceInput:Z

    .line 452
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v6, "samsungAutoFullStop"

    invoke-interface {v5, v6, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    sput-boolean v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mAutoFullStop:Z

    .line 453
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v6, "samsungLinkToContacts"

    invoke-interface {v5, v6, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    sput-boolean v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mLinkToContacts:Z

    .line 454
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v6, "samsungPointingKeyboard"

    invoke-interface {v5, v6, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    sput-boolean v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mPointingKeyboard:Z

    .line 455
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v6, "samsungFloatingKeyboard"

    invoke-interface {v5, v6, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    sput-boolean v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mFloatingKeyboard:Z

    .line 456
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v6, "samsungSwipeOnKeyboard"

    invoke-interface {v5, v6, v9}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v5

    sput v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mSwipeOnKeyboard:I

    .line 457
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v6, "samsungCharPreview"

    invoke-interface {v5, v6, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    sput-boolean v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mCharPreview:Z

    .line 459
    invoke-static {}, Lcom/samsung/inputmethod/comm/SimeEnvironment;->getInstance()Lcom/samsung/inputmethod/comm/SimeEnvironment;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/inputmethod/comm/SimeEnvironment;->getDims()I

    move-result v2

    .line 460
    .local v2, dims:I
    const/16 v5, 0x78

    if-ne v2, v5, :cond_5

    .line 462
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v6, "samsunghandwrite_width_index"

    invoke-interface {v5, v6, v11}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v5

    sput v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mLineWidthIndex:I

    .line 473
    :goto_1
    sget-boolean v5, Lcom/samsung/inputmethod/comm/SimeFeature;->SIME_MMKEY_DEFAULT_HANDWRITING:Z

    if-eqz v5, :cond_7

    .line 474
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v6, "samsungMultiModalStatus"

    invoke-interface {v5, v6, v11}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v5

    sput v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mMultiModalStatus:I

    .line 479
    :goto_2
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    .line 481
    .local v3, editor:Landroid/content/SharedPreferences$Editor;
    sget-boolean v5, Lcom/samsung/inputmethod/comm/SimeFeature;->SIME_HW_BOX_FEATURE:Z

    if-eqz v5, :cond_8

    const/16 v5, 0x140

    if-ne v2, v5, :cond_8

    .line 483
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v6, "samsunghandwrite_color_index"

    invoke-interface {v5, v6, v10}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v5

    sput v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mLineColorIndex:I

    .line 486
    sget v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mLineColorIndex:I

    if-ne v5, v12, :cond_0

    .line 487
    sput v10, Lcom/samsung/inputmethod/setting/SimeSetting;->mLineColorIndex:I

    .line 488
    const-string v5, "samsunghandwrite_color_index"

    sget v6, Lcom/samsung/inputmethod/setting/SimeSetting;->mLineColorIndex:I

    invoke-interface {v3, v5, v6}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 502
    :cond_0
    :goto_3
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v6, "samsunghandwrite_speed"

    const/16 v7, 0x2bc

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v5

    sput v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mSpeed:I

    .line 503
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mPortraitInputlist:[I

    sget-object v6, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v7, "samsungPortraitInputEng"

    const/high16 v8, 0x1111

    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v6

    aput v6, v5, v9

    .line 504
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mPortraitInputlist:[I

    sget-object v6, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v7, "samsungPortraitInputChn"

    const/high16 v8, 0x1202

    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v6

    aput v6, v5, v10

    .line 505
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mPortraitInputlist:[I

    sget-object v6, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v7, "samsungPortraitInputKor"

    const/high16 v8, 0x1203

    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v6

    aput v6, v5, v11

    .line 506
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mPortraitInputlist:[I

    sget-object v6, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v7, "samsungPortraitInputTW"

    const/high16 v8, 0x2104

    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v6

    aput v6, v5, v13

    .line 507
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mPortraitInputlist:[I

    sget-object v6, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v7, "samsungPortraitInputHK"

    const/high16 v8, -0x6efb

    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v6

    aput v6, v5, v12

    .line 508
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v6, "samsunghandwrite_type"

    invoke-interface {v5, v6, v11}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v5

    sput v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mRecgType:I

    .line 509
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v6, "samsunghandwrite_linegradient"

    invoke-interface {v5, v6, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    sput-boolean v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mIsLineGradient:Z

    .line 510
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v6, "samsunghandwrite_linegradient_scolor"

    const/high16 v7, -0x1

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v5

    sput v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mLineGradientStartcolor:I

    .line 511
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v6, "samsunghandwrite_linegradient_mcolor"

    const v7, -0xff0100

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v5

    sput v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mLineGradientMiddlecolor:I

    .line 512
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v6, "samsunghandwrite_linegradient_ecolor"

    const v7, -0xffff01

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v5

    sput v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mLineGradientEndcolor:I

    .line 513
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v6, "samsungDefine"

    invoke-interface {v5, v6, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    sput-boolean v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mDefine:Z

    .line 514
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v6, "samsungConvert"

    invoke-interface {v5, v6, v9}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v5

    sput v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mConvert:I

    .line 515
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v6, "samsunghandwrite_mode"

    invoke-interface {v5, v6, v9}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v5

    sput v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mHWModeIndex:I

    .line 518
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mFuzzyStateFlag:[Z

    sget-object v6, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v7, "samsungFuzzy_zhz"

    invoke-interface {v6, v7, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    aput-boolean v6, v5, v9

    .line 519
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mFuzzyStateFlag:[Z

    sget-object v6, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v7, "samsungFuzzy_chc"

    invoke-interface {v6, v7, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    aput-boolean v6, v5, v10

    .line 520
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mFuzzyStateFlag:[Z

    sget-object v6, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v7, "samsungFuzzy_shs"

    invoke-interface {v6, v7, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    aput-boolean v6, v5, v11

    .line 521
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mFuzzyStateFlag:[Z

    sget-object v6, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v7, "samsungFuzzy_nl"

    invoke-interface {v6, v7, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    aput-boolean v6, v5, v13

    .line 522
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mFuzzyStateFlag:[Z

    sget-object v6, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v7, "samsungFuzzy_rl"

    invoke-interface {v6, v7, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    aput-boolean v6, v5, v12

    .line 523
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mFuzzyStateFlag:[Z

    const/4 v6, 0x5

    sget-object v7, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v8, "samsungFuzzy_hf"

    invoke-interface {v7, v8, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    aput-boolean v7, v5, v6

    .line 524
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mFuzzyStateFlag:[Z

    const/4 v6, 0x6

    sget-object v7, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v8, "samsungFuzzy_angan"

    invoke-interface {v7, v8, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    aput-boolean v7, v5, v6

    .line 525
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mFuzzyStateFlag:[Z

    const/4 v6, 0x7

    sget-object v7, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v8, "samsungFuzzy_engen"

    invoke-interface {v7, v8, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    aput-boolean v7, v5, v6

    .line 526
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mFuzzyStateFlag:[Z

    const/16 v6, 0x8

    sget-object v7, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v8, "samsungFuzzy_ingin"

    invoke-interface {v7, v8, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    aput-boolean v7, v5, v6

    .line 529
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v6, "is_stroke_zh_cn"

    invoke-interface {v5, v6, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    sput-boolean v5, Lcom/samsung/inputmethod/setting/SimeSetting;->IS_STROKE_ZH_CN:Z

    .line 530
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v6, "is_stroke_zh_tw"

    invoke-interface {v5, v6, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    sput-boolean v5, Lcom/samsung/inputmethod/setting/SimeSetting;->IS_STROKE_ZH_TW:Z

    .line 532
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v6, "is_hwbox_en"

    invoke-interface {v5, v6, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    sput-boolean v5, Lcom/samsung/inputmethod/setting/SimeSetting;->IS_BOX_EN:Z

    .line 533
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v6, "is_hwbox_zh_cn"

    invoke-interface {v5, v6, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    sput-boolean v5, Lcom/samsung/inputmethod/setting/SimeSetting;->IS_BOX_ZH_CN:Z

    .line 534
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v6, "is_hwbox_ko"

    invoke-interface {v5, v6, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    sput-boolean v5, Lcom/samsung/inputmethod/setting/SimeSetting;->IS_BOX_KO:Z

    .line 535
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v6, "is_hwbox_zh_tw"

    invoke-interface {v5, v6, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    sput-boolean v5, Lcom/samsung/inputmethod/setting/SimeSetting;->IS_BOX_ZH_TW:Z

    .line 536
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v6, "is_hwbox_zh_hk"

    invoke-interface {v5, v6, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    sput-boolean v5, Lcom/samsung/inputmethod/setting/SimeSetting;->IS_BOX_ZH_HK:Z

    .line 538
    sput v9, Lcom/samsung/inputmethod/setting/SimeSetting;->mFuzzyState:I

    .line 539
    const/4 v4, 0x0

    .local v4, i:I
    :goto_4
    const/16 v5, 0x9

    if-ge v4, v5, :cond_9

    .line 541
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mFuzzyStateFlag:[Z

    aget-boolean v5, v5, v4

    if-eqz v5, :cond_1

    .line 542
    sget v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mFuzzyState:I

    shl-int v6, v10, v4

    or-int/2addr v5, v6

    sput v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mFuzzyState:I

    .line 539
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 421
    .end local v2           #dims:I
    .end local v3           #editor:Landroid/content/SharedPreferences$Editor;
    .end local v4           #i:I
    :cond_2
    const-string v5, "TW"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 422
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v6, "samsungLanguageChinese"

    invoke-interface {v5, v6, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    sput-boolean v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mLanguageChinese:Z

    .line 423
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v6, "samsungLanguageChineseTW"

    invoke-interface {v5, v6, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    sput-boolean v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mLanguageChineseTw:Z

    .line 424
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v6, "samsungLanguageChineseHK"

    invoke-interface {v5, v6, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    sput-boolean v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mLanguageChineseHk:Z

    .line 425
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v6, "samsungLastInputMode"

    const/high16 v7, 0x2104

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v5

    sput v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mLastInputMode:I

    goto/16 :goto_0

    .line 427
    :cond_3
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v6, "samsungLanguageChinese"

    invoke-interface {v5, v6, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    sput-boolean v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mLanguageChinese:Z

    .line 428
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v6, "samsungLanguageChineseTW"

    invoke-interface {v5, v6, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    sput-boolean v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mLanguageChineseTw:Z

    .line 429
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v6, "samsungLanguageChineseHK"

    invoke-interface {v5, v6, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    sput-boolean v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mLanguageChineseHk:Z

    .line 430
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v6, "samsungLastInputMode"

    const/high16 v7, 0x1102

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v5

    sput v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mLastInputMode:I

    goto/16 :goto_0

    .line 433
    :cond_4
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v6, "samsungLanguageChinese"

    invoke-interface {v5, v6, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    sput-boolean v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mLanguageChinese:Z

    .line 434
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v6, "samsungLanguageChineseTW"

    invoke-interface {v5, v6, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    sput-boolean v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mLanguageChineseTw:Z

    .line 435
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v6, "samsungLanguageChineseHK"

    invoke-interface {v5, v6, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    sput-boolean v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mLanguageChineseHk:Z

    .line 436
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v6, "samsungLastInputMode"

    const/high16 v7, 0x1102

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v5

    sput v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mLastInputMode:I

    goto/16 :goto_0

    .line 464
    .restart local v2       #dims:I
    :cond_5
    sget-boolean v5, Lcom/samsung/inputmethod/comm/SimeFeature;->SIME_HW_BOX_FEATURE:Z

    if-eqz v5, :cond_6

    const/16 v5, 0x140

    if-ne v2, v5, :cond_6

    .line 466
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v6, "samsunghandwrite_width_index"

    invoke-interface {v5, v6, v11}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v5

    sput v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mLineWidthIndex:I

    goto/16 :goto_1

    .line 470
    :cond_6
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v6, "samsunghandwrite_width_index"

    invoke-interface {v5, v6, v13}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v5

    sput v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mLineWidthIndex:I

    goto/16 :goto_1

    .line 476
    :cond_7
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v6, "samsungMultiModalStatus"

    invoke-interface {v5, v6, v9}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v5

    sput v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mMultiModalStatus:I

    goto/16 :goto_2

    .line 493
    .restart local v3       #editor:Landroid/content/SharedPreferences$Editor;
    :cond_8
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v6, "samsunghandwrite_color_index"

    invoke-interface {v5, v6, v9}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v5

    sput v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mLineColorIndex:I

    .line 496
    sget v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mLineColorIndex:I

    if-ne v5, v12, :cond_0

    .line 497
    sput v9, Lcom/samsung/inputmethod/setting/SimeSetting;->mLineColorIndex:I

    .line 498
    const-string v5, "samsunghandwrite_color_index"

    sget v6, Lcom/samsung/inputmethod/setting/SimeSetting;->mLineColorIndex:I

    invoke-interface {v3, v5, v6}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    goto/16 :goto_3

    .line 545
    .restart local v4       #i:I
    :cond_9
    sget v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mConvert:I

    if-gt v5, v11, :cond_a

    sget v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mConvert:I

    if-gez v5, :cond_b

    .line 546
    :cond_a
    sput v9, Lcom/samsung/inputmethod/setting/SimeSetting;->mConvert:I

    .line 548
    :cond_b
    sget v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mHWModeIndex:I

    if-gt v5, v10, :cond_c

    sget v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mHWModeIndex:I

    if-gez v5, :cond_d

    .line 549
    :cond_c
    sput v9, Lcom/samsung/inputmethod/setting/SimeSetting;->mHWModeIndex:I

    .line 551
    :cond_d
    invoke-static {}, Lcom/samsung/inputmethod/comm/SimeEnvironment;->getInstance()Lcom/samsung/inputmethod/comm/SimeEnvironment;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/inputmethod/comm/SimeEnvironment;->isTabletModel()Z

    move-result v5

    if-eqz v5, :cond_e

    .line 552
    sput-boolean v10, Lcom/samsung/inputmethod/setting/SimeSetting;->mIsTabletModel:Z

    .line 553
    invoke-direct {p0}, Lcom/samsung/inputmethod/setting/SimeSetting;->initTabtabData()V

    .line 556
    :cond_e
    sget-boolean v5, Lcom/samsung/inputmethod/comm/SimeFeature;->SIME_USE_TAB_SYMBOL:Z

    if-eqz v5, :cond_11

    .line 559
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mSymHistoryDefaultVal:[Ljava/lang/String;

    sget-object v6, Lcom/samsung/inputmethod/setting/SimeSetting;->SYM_HISTORY_SPLIT_STR:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/samsung/inputmethod/comm/SimeUtility;->getStringFromArray([Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 561
    .local v1, defaultVal:Ljava/lang/String;
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v6, "samsungLatestSymbols"

    invoke-interface {v5, v6, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    sput-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mSymHistory:Ljava/lang/String;

    .line 564
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mSymHistory:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_f

    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mSymHistory:Ljava/lang/String;

    sget-object v6, Lcom/samsung/inputmethod/setting/SimeSetting;->SYM_HISTORY_SPLIT_STR:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_10

    .line 567
    :cond_f
    const-string v5, "SamsungIMESettings"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "--mSymHistory reset--------original value="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/samsung/inputmethod/setting/SimeSetting;->mSymHistory:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 568
    invoke-virtual {v1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->copyValueOf([C)Ljava/lang/String;

    move-result-object v5

    sput-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mSymHistory:Ljava/lang/String;

    .line 570
    const-string v5, "samsungLatestSymbols"

    sget-object v6, Lcom/samsung/inputmethod/setting/SimeSetting;->mSymHistory:Ljava/lang/String;

    invoke-interface {v3, v5, v6}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 571
    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 573
    :cond_10
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mSymHistoryList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    .line 574
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mSymHistory:Ljava/lang/String;

    sget-object v6, Lcom/samsung/inputmethod/setting/SimeSetting;->SYM_HISTORY_SPLIT_STR:Ljava/lang/String;

    sget-object v7, Lcom/samsung/inputmethod/setting/SimeSetting;->mSymHistoryList:Ljava/util/ArrayList;

    invoke-static {v5, v6, v7}, Lcom/samsung/inputmethod/comm/SimeUtility;->getListFromString(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)Z

    .line 579
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mSymHistoryList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    sget v6, Lcom/samsung/inputmethod/setting/SimeSetting;->MAX_LATEST_SYM_COUNT:I

    if-ge v5, v6, :cond_11

    .line 581
    const-string v5, "SamsungIMESettings"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "--mSymHistory reset---original list= "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/samsung/inputmethod/setting/SimeSetting;->mSymHistoryList:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 582
    invoke-virtual {v1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->copyValueOf([C)Ljava/lang/String;

    move-result-object v5

    sput-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mSymHistory:Ljava/lang/String;

    .line 583
    const-string v5, "samsungLatestSymbols"

    sget-object v6, Lcom/samsung/inputmethod/setting/SimeSetting;->mSymHistory:Ljava/lang/String;

    invoke-interface {v3, v5, v6}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 584
    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 585
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mSymHistoryList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    .line 586
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mSymHistoryDefaultVal:[Ljava/lang/String;

    sget-object v6, Lcom/samsung/inputmethod/setting/SimeSetting;->mSymHistoryList:Ljava/util/ArrayList;

    invoke-static {v5, v6}, Lcom/samsung/inputmethod/comm/SimeUtility;->getListFromStringArray([Ljava/lang/String;Ljava/util/ArrayList;)Z

    .line 590
    .end local v1           #defaultVal:Ljava/lang/String;
    :cond_11
    return-void
.end method

.method private static initTabEditListSCHN()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 642
    const/4 v0, 0x0

    .line 643
    .local v0, index:I
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v0, 0x1

    .end local v0           #index:I
    .local v1, index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_chn_34_checkbox"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v0

    .line 644
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v0, v1, 0x1

    .end local v1           #index:I
    .restart local v0       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_chn_qwerty_checkbox"

    invoke-interface {v4, v5, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v1

    .line 645
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v0, 0x1

    .end local v0           #index:I
    .restart local v1       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_chn_stroke_checkbox"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v0

    .line 646
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v0, v1, 0x1

    .end local v1           #index:I
    .restart local v0       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_kor_qwerty_checkbox"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v1

    .line 648
    invoke-static {}, Lcom/samsung/inputmethod/comm/SimeEnvironment;->getInstance()Lcom/samsung/inputmethod/comm/SimeEnvironment;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/inputmethod/comm/SimeEnvironment;->isVoiceInstalled()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 649
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v0, 0x1

    .end local v0           #index:I
    .restart local v1       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_clipboard_checkbox"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v0

    .line 650
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v0, v1, 0x1

    .end local v1           #index:I
    .restart local v0       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_emoticon_checkbox"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v1

    .line 654
    :goto_0
    sget-boolean v3, Lcom/samsung/inputmethod/comm/SimeFeature;->SIME_SUPPORT_MULTI_FLOAT:Z

    if-eqz v3, :cond_2

    .line 655
    invoke-static {}, Lcom/samsung/inputmethod/softkeyboard/SimeSkbManager;->getKeyboardType()Lcom/samsung/inputmethod/softkeyboard/SimeSkbManager$KeyboardType;

    move-result-object v2

    .line 656
    .local v2, keyboardType:Lcom/samsung/inputmethod/softkeyboard/SimeSkbManager$KeyboardType;
    sget-object v3, Lcom/samsung/inputmethod/softkeyboard/SimeSkbManager$KeyboardType;->TYPE_NORMAL:Lcom/samsung/inputmethod/softkeyboard/SimeSkbManager$KeyboardType;

    if-ne v2, v3, :cond_1

    .line 657
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v0, 0x1

    .end local v0           #index:I
    .restart local v1       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_chn_full_handwrite_checkbox"

    invoke-interface {v4, v5, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v0

    .line 658
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v0, v1, 0x1

    .end local v1           #index:I
    .restart local v0       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_eng_full_handwrite_checkbox"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v1

    .line 659
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v0, 0x1

    .end local v0           #index:I
    .restart local v1       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_korean_full_handwrite_checkbox"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v0

    move v0, v1

    .line 676
    .end local v1           #index:I
    .end local v2           #keyboardType:Lcom/samsung/inputmethod/softkeyboard/SimeSkbManager$KeyboardType;
    .restart local v0       #index:I
    :goto_1
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v0, 0x1

    .end local v0           #index:I
    .restart local v1       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_eng_qwerty_checkbox"

    invoke-interface {v4, v5, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v0

    .line 677
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v0, v1, 0x1

    .end local v1           #index:I
    .restart local v0       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_123_symbol_checkbox"

    invoke-interface {v4, v5, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v1

    .line 678
    return-void

    .line 652
    :cond_0
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v0, 0x1

    .end local v0           #index:I
    .restart local v1       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_emoticon_checkbox"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v0

    move v0, v1

    .end local v1           #index:I
    .restart local v0       #index:I
    goto :goto_0

    .line 661
    .restart local v2       #keyboardType:Lcom/samsung/inputmethod/softkeyboard/SimeSkbManager$KeyboardType;
    :cond_1
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v0, 0x1

    .end local v0           #index:I
    .restart local v1       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_chn_box_handwrite_checkbox"

    invoke-interface {v4, v5, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v0

    .line 662
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v0, v1, 0x1

    .end local v1           #index:I
    .restart local v0       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_eng_box_handwrite_checkbox"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v1

    .line 663
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v0, 0x1

    .end local v0           #index:I
    .restart local v1       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_korean_box_handwrite_checkbox"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v0

    .end local v2           #keyboardType:Lcom/samsung/inputmethod/softkeyboard/SimeSkbManager$KeyboardType;
    :goto_2
    move v0, v1

    .end local v1           #index:I
    .restart local v0       #index:I
    goto :goto_1

    .line 666
    :cond_2
    invoke-static {}, Lcom/samsung/inputmethod/comm/SimeEnvironment;->getInstance()Lcom/samsung/inputmethod/comm/SimeEnvironment;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/inputmethod/comm/SimeEnvironment;->isLargeScreen()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 667
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v0, 0x1

    .end local v0           #index:I
    .restart local v1       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_chn_full_handwrite_checkbox"

    invoke-interface {v4, v5, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v0

    .line 668
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v0, v1, 0x1

    .end local v1           #index:I
    .restart local v0       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_eng_full_handwrite_checkbox"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v1

    .line 669
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v0, 0x1

    .end local v0           #index:I
    .restart local v1       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_korean_full_handwrite_checkbox"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v0

    move v0, v1

    .end local v1           #index:I
    .restart local v0       #index:I
    goto/16 :goto_1

    .line 671
    :cond_3
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v0, 0x1

    .end local v0           #index:I
    .restart local v1       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_chn_box_handwrite_checkbox"

    invoke-interface {v4, v5, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v0

    .line 672
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v0, v1, 0x1

    .end local v1           #index:I
    .restart local v0       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_eng_box_handwrite_checkbox"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v1

    .line 673
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v0, 0x1

    .end local v0           #index:I
    .restart local v1       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_korean_box_handwrite_checkbox"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v0

    goto :goto_2
.end method

.method private static initTabEditListTCHN()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 770
    invoke-static {}, Lcom/samsung/inputmethod/softkeyboard/SimeSkbManager;->getKeyboardType()Lcom/samsung/inputmethod/softkeyboard/SimeSkbManager$KeyboardType;

    move-result-object v2

    .line 771
    .local v2, keyboardType:Lcom/samsung/inputmethod/softkeyboard/SimeSkbManager$KeyboardType;
    const/4 v0, 0x0

    .line 773
    .local v0, index:I
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v0, 0x1

    .end local v0           #index:I
    .local v1, index:I
    aput-boolean v6, v3, v0

    .line 774
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->COUNTRY_CODE:Ljava/lang/String;

    if-eqz v3, :cond_5

    const-string v3, "HONG KONG"

    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->COUNTRY_CODE:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 775
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v0, v1, 0x1

    .end local v1           #index:I
    .restart local v0       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_cangjie_qwerty_checkbox"

    invoke-interface {v4, v5, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v1

    .line 776
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v0, 0x1

    .end local v0           #index:I
    .restart local v1       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_zhuyin_qwerty_checkbox"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v0

    .line 777
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v0, v1, 0x1

    .end local v1           #index:I
    .restart local v0       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_zhuyin_34_checkbox"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v1

    .line 778
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v0, 0x1

    .end local v0           #index:I
    .restart local v1       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_hk_stroke_checkbox"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v0

    .line 779
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v0, v1, 0x1

    .end local v1           #index:I
    .restart local v0       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_tw_stroke_checkbox"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v1

    .line 780
    sget-boolean v3, Lcom/samsung/inputmethod/comm/SimeFeature;->SIME_SUPPORT_MULTI_FLOAT:Z

    if-eqz v3, :cond_3

    .line 781
    sget-object v3, Lcom/samsung/inputmethod/softkeyboard/SimeSkbManager$KeyboardType;->TYPE_NORMAL:Lcom/samsung/inputmethod/softkeyboard/SimeSkbManager$KeyboardType;

    if-ne v2, v3, :cond_2

    .line 782
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v0, 0x1

    .end local v0           #index:I
    .restart local v1       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_hk_full_handwrite_checkbox"

    invoke-interface {v4, v5, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v0

    .line 783
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v0, v1, 0x1

    .end local v1           #index:I
    .restart local v0       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_tw_full_handwrite_checkbox"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v1

    .line 845
    :goto_0
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v0, 0x1

    .end local v0           #index:I
    .restart local v1       #index:I
    aput-boolean v6, v3, v0

    .line 846
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->COUNTRY_CODE:Ljava/lang/String;

    if-eqz v3, :cond_0

    const-string v3, "HONG KONG"

    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->COUNTRY_CODE:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    const-string v3, "TAIWAN"

    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->COUNTRY_CODE:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_10

    .line 848
    :cond_1
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v0, v1, 0x1

    .end local v1           #index:I
    .restart local v0       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_chn_qwerty_checkbox"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v1

    .line 849
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v0, 0x1

    .end local v0           #index:I
    .restart local v1       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_chn_34_checkbox"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v0

    .line 850
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v0, v1, 0x1

    .end local v1           #index:I
    .restart local v0       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_chn_stroke_checkbox"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v1

    .line 851
    sget-boolean v3, Lcom/samsung/inputmethod/comm/SimeFeature;->SIME_SUPPORT_MULTI_FLOAT:Z

    if-eqz v3, :cond_e

    .line 852
    sget-object v3, Lcom/samsung/inputmethod/softkeyboard/SimeSkbManager$KeyboardType;->TYPE_NORMAL:Lcom/samsung/inputmethod/softkeyboard/SimeSkbManager$KeyboardType;

    if-ne v2, v3, :cond_d

    .line 853
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v0, 0x1

    .end local v0           #index:I
    .restart local v1       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_chn_full_handwrite_checkbox"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v0

    move v0, v1

    .line 883
    .end local v1           #index:I
    .restart local v0       #index:I
    :goto_1
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v0, 0x1

    .end local v0           #index:I
    .restart local v1       #index:I
    aput-boolean v6, v3, v0

    .line 884
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v0, v1, 0x1

    .end local v1           #index:I
    .restart local v0       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_eng_qwerty_checkbox"

    invoke-interface {v4, v5, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v1

    .line 885
    sget-boolean v3, Lcom/samsung/inputmethod/comm/SimeFeature;->SIME_SUPPORT_MULTI_FLOAT:Z

    if-eqz v3, :cond_15

    .line 886
    sget-object v3, Lcom/samsung/inputmethod/softkeyboard/SimeSkbManager$KeyboardType;->TYPE_NORMAL:Lcom/samsung/inputmethod/softkeyboard/SimeSkbManager$KeyboardType;

    if-ne v2, v3, :cond_14

    .line 887
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v0, 0x1

    .end local v0           #index:I
    .restart local v1       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_eng_full_handwrite_checkbox"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v0

    move v0, v1

    .line 899
    .end local v1           #index:I
    .restart local v0       #index:I
    :goto_2
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v0, 0x1

    .end local v0           #index:I
    .restart local v1       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_kor_qwerty_checkbox"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v0

    .line 900
    sget-boolean v3, Lcom/samsung/inputmethod/comm/SimeFeature;->SIME_SUPPORT_MULTI_FLOAT:Z

    if-eqz v3, :cond_18

    .line 901
    sget-object v3, Lcom/samsung/inputmethod/softkeyboard/SimeSkbManager$KeyboardType;->TYPE_NORMAL:Lcom/samsung/inputmethod/softkeyboard/SimeSkbManager$KeyboardType;

    if-ne v2, v3, :cond_17

    .line 902
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v0, v1, 0x1

    .end local v1           #index:I
    .restart local v0       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_korean_full_handwrite_checkbox"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v1

    .line 913
    :goto_3
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v0, 0x1

    .end local v0           #index:I
    .restart local v1       #index:I
    aput-boolean v6, v3, v0

    .line 914
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v0, v1, 0x1

    .end local v1           #index:I
    .restart local v0       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_123_symbol_checkbox"

    invoke-interface {v4, v5, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v1

    .line 915
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v0, 0x1

    .end local v0           #index:I
    .restart local v1       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_emoticon_checkbox"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v0

    .line 916
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v0, v1, 0x1

    .end local v1           #index:I
    .restart local v0       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_clipboard_checkbox"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v1

    .line 918
    return-void

    .line 785
    :cond_2
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v0, 0x1

    .end local v0           #index:I
    .restart local v1       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_hk_half_box_checkbox"

    invoke-interface {v4, v5, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v0

    .line 786
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v0, v1, 0x1

    .end local v1           #index:I
    .restart local v0       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_tw_half_box_checkbox"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v1

    goto/16 :goto_0

    .line 789
    :cond_3
    invoke-static {}, Lcom/samsung/inputmethod/comm/SimeEnvironment;->getInstance()Lcom/samsung/inputmethod/comm/SimeEnvironment;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/inputmethod/comm/SimeEnvironment;->isLargeScreen()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 790
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v0, 0x1

    .end local v0           #index:I
    .restart local v1       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_hk_full_handwrite_checkbox"

    invoke-interface {v4, v5, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v0

    .line 791
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v0, v1, 0x1

    .end local v1           #index:I
    .restart local v0       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_tw_full_handwrite_checkbox"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v1

    goto/16 :goto_0

    .line 793
    :cond_4
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v0, 0x1

    .end local v0           #index:I
    .restart local v1       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_hk_half_box_checkbox"

    invoke-interface {v4, v5, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v0

    .line 794
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v0, v1, 0x1

    .end local v1           #index:I
    .restart local v0       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_tw_half_box_checkbox"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v1

    goto/16 :goto_0

    .line 797
    .end local v0           #index:I
    .restart local v1       #index:I
    :cond_5
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->COUNTRY_CODE:Ljava/lang/String;

    if-eqz v3, :cond_9

    const-string v3, "TAIWAN"

    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->COUNTRY_CODE:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 798
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v0, v1, 0x1

    .end local v1           #index:I
    .restart local v0       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_cangjie_qwerty_checkbox"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v1

    .line 799
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v0, 0x1

    .end local v0           #index:I
    .restart local v1       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_zhuyin_qwerty_checkbox"

    invoke-interface {v4, v5, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v0

    .line 800
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v0, v1, 0x1

    .end local v1           #index:I
    .restart local v0       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_zhuyin_34_checkbox"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v1

    .line 801
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v0, 0x1

    .end local v0           #index:I
    .restart local v1       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_hk_stroke_checkbox"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v0

    .line 802
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v0, v1, 0x1

    .end local v1           #index:I
    .restart local v0       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_tw_stroke_checkbox"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v1

    .line 803
    sget-boolean v3, Lcom/samsung/inputmethod/comm/SimeFeature;->SIME_SUPPORT_MULTI_FLOAT:Z

    if-eqz v3, :cond_7

    .line 804
    sget-object v3, Lcom/samsung/inputmethod/softkeyboard/SimeSkbManager$KeyboardType;->TYPE_NORMAL:Lcom/samsung/inputmethod/softkeyboard/SimeSkbManager$KeyboardType;

    if-ne v2, v3, :cond_6

    .line 805
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v0, 0x1

    .end local v0           #index:I
    .restart local v1       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_hk_full_handwrite_checkbox"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v0

    .line 806
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v0, v1, 0x1

    .end local v1           #index:I
    .restart local v0       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_tw_full_handwrite_checkbox"

    invoke-interface {v4, v5, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v1

    goto/16 :goto_0

    .line 808
    :cond_6
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v0, 0x1

    .end local v0           #index:I
    .restart local v1       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_hk_half_box_checkbox"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v0

    .line 809
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v0, v1, 0x1

    .end local v1           #index:I
    .restart local v0       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_tw_half_box_checkbox"

    invoke-interface {v4, v5, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v1

    goto/16 :goto_0

    .line 812
    :cond_7
    invoke-static {}, Lcom/samsung/inputmethod/comm/SimeEnvironment;->getInstance()Lcom/samsung/inputmethod/comm/SimeEnvironment;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/inputmethod/comm/SimeEnvironment;->isLargeScreen()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 813
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v0, 0x1

    .end local v0           #index:I
    .restart local v1       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_hk_full_handwrite_checkbox"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v0

    .line 814
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v0, v1, 0x1

    .end local v1           #index:I
    .restart local v0       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_tw_full_handwrite_checkbox"

    invoke-interface {v4, v5, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v1

    goto/16 :goto_0

    .line 816
    :cond_8
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v0, 0x1

    .end local v0           #index:I
    .restart local v1       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_hk_half_box_checkbox"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v0

    .line 817
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v0, v1, 0x1

    .end local v1           #index:I
    .restart local v0       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_tw_half_box_checkbox"

    invoke-interface {v4, v5, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v1

    goto/16 :goto_0

    .line 821
    .end local v0           #index:I
    .restart local v1       #index:I
    :cond_9
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v0, v1, 0x1

    .end local v1           #index:I
    .restart local v0       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_cangjie_qwerty_checkbox"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v1

    .line 822
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v0, 0x1

    .end local v0           #index:I
    .restart local v1       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_zhuyin_qwerty_checkbox"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v0

    .line 823
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v0, v1, 0x1

    .end local v1           #index:I
    .restart local v0       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_zhuyin_34_checkbox"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v1

    .line 824
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v0, 0x1

    .end local v0           #index:I
    .restart local v1       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_hk_stroke_checkbox"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v0

    .line 825
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v0, v1, 0x1

    .end local v1           #index:I
    .restart local v0       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_tw_stroke_checkbox"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v1

    .line 826
    sget-boolean v3, Lcom/samsung/inputmethod/comm/SimeFeature;->SIME_SUPPORT_MULTI_FLOAT:Z

    if-eqz v3, :cond_b

    .line 827
    sget-object v3, Lcom/samsung/inputmethod/softkeyboard/SimeSkbManager$KeyboardType;->TYPE_NORMAL:Lcom/samsung/inputmethod/softkeyboard/SimeSkbManager$KeyboardType;

    if-ne v2, v3, :cond_a

    .line 828
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v0, 0x1

    .end local v0           #index:I
    .restart local v1       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_hk_full_handwrite_checkbox"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v0

    .line 829
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v0, v1, 0x1

    .end local v1           #index:I
    .restart local v0       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_tw_full_handwrite_checkbox"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v1

    goto/16 :goto_0

    .line 831
    :cond_a
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v0, 0x1

    .end local v0           #index:I
    .restart local v1       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_hk_half_box_checkbox"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v0

    .line 832
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v0, v1, 0x1

    .end local v1           #index:I
    .restart local v0       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_tw_half_box_checkbox"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v1

    goto/16 :goto_0

    .line 835
    :cond_b
    invoke-static {}, Lcom/samsung/inputmethod/comm/SimeEnvironment;->getInstance()Lcom/samsung/inputmethod/comm/SimeEnvironment;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/inputmethod/comm/SimeEnvironment;->isLargeScreen()Z

    move-result v3

    if-eqz v3, :cond_c

    .line 836
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v0, 0x1

    .end local v0           #index:I
    .restart local v1       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_hk_full_handwrite_checkbox"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v0

    .line 837
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v0, v1, 0x1

    .end local v1           #index:I
    .restart local v0       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_tw_full_handwrite_checkbox"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v1

    goto/16 :goto_0

    .line 839
    :cond_c
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v0, 0x1

    .end local v0           #index:I
    .restart local v1       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_hk_half_box_checkbox"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v0

    .line 840
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v0, v1, 0x1

    .end local v1           #index:I
    .restart local v0       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_tw_half_box_checkbox"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v1

    goto/16 :goto_0

    .line 855
    :cond_d
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v0, 0x1

    .end local v0           #index:I
    .restart local v1       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_chn_box_handwrite_checkbox"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v0

    move v0, v1

    .end local v1           #index:I
    .restart local v0       #index:I
    goto/16 :goto_1

    .line 858
    :cond_e
    invoke-static {}, Lcom/samsung/inputmethod/comm/SimeEnvironment;->getInstance()Lcom/samsung/inputmethod/comm/SimeEnvironment;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/inputmethod/comm/SimeEnvironment;->isLargeScreen()Z

    move-result v3

    if-eqz v3, :cond_f

    .line 859
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v0, 0x1

    .end local v0           #index:I
    .restart local v1       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_chn_full_handwrite_checkbox"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v0

    move v0, v1

    .end local v1           #index:I
    .restart local v0       #index:I
    goto/16 :goto_1

    .line 861
    :cond_f
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v0, 0x1

    .end local v0           #index:I
    .restart local v1       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_chn_box_handwrite_checkbox"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v0

    move v0, v1

    .end local v1           #index:I
    .restart local v0       #index:I
    goto/16 :goto_1

    .line 865
    .end local v0           #index:I
    .restart local v1       #index:I
    :cond_10
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v0, v1, 0x1

    .end local v1           #index:I
    .restart local v0       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_chn_qwerty_checkbox"

    invoke-interface {v4, v5, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v1

    .line 866
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v0, 0x1

    .end local v0           #index:I
    .restart local v1       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_chn_34_checkbox"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v0

    .line 867
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v0, v1, 0x1

    .end local v1           #index:I
    .restart local v0       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_chn_stroke_checkbox"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v1

    .line 868
    sget-boolean v3, Lcom/samsung/inputmethod/comm/SimeFeature;->SIME_SUPPORT_MULTI_FLOAT:Z

    if-eqz v3, :cond_12

    .line 869
    sget-object v3, Lcom/samsung/inputmethod/softkeyboard/SimeSkbManager$KeyboardType;->TYPE_NORMAL:Lcom/samsung/inputmethod/softkeyboard/SimeSkbManager$KeyboardType;

    if-ne v2, v3, :cond_11

    .line 870
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v0, 0x1

    .end local v0           #index:I
    .restart local v1       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_chn_full_handwrite_checkbox"

    invoke-interface {v4, v5, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v0

    move v0, v1

    .end local v1           #index:I
    .restart local v0       #index:I
    goto/16 :goto_1

    .line 872
    :cond_11
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v0, 0x1

    .end local v0           #index:I
    .restart local v1       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_chn_box_handwrite_checkbox"

    invoke-interface {v4, v5, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v0

    move v0, v1

    .end local v1           #index:I
    .restart local v0       #index:I
    goto/16 :goto_1

    .line 875
    :cond_12
    invoke-static {}, Lcom/samsung/inputmethod/comm/SimeEnvironment;->getInstance()Lcom/samsung/inputmethod/comm/SimeEnvironment;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/inputmethod/comm/SimeEnvironment;->isLargeScreen()Z

    move-result v3

    if-eqz v3, :cond_13

    .line 876
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v0, 0x1

    .end local v0           #index:I
    .restart local v1       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_chn_full_handwrite_checkbox"

    invoke-interface {v4, v5, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v0

    move v0, v1

    .end local v1           #index:I
    .restart local v0       #index:I
    goto/16 :goto_1

    .line 878
    :cond_13
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v0, 0x1

    .end local v0           #index:I
    .restart local v1       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_chn_box_handwrite_checkbox"

    invoke-interface {v4, v5, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v0

    move v0, v1

    .end local v1           #index:I
    .restart local v0       #index:I
    goto/16 :goto_1

    .line 889
    :cond_14
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v0, 0x1

    .end local v0           #index:I
    .restart local v1       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_eng_box_handwrite_checkbox"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v0

    move v0, v1

    .end local v1           #index:I
    .restart local v0       #index:I
    goto/16 :goto_2

    .line 892
    :cond_15
    invoke-static {}, Lcom/samsung/inputmethod/comm/SimeEnvironment;->getInstance()Lcom/samsung/inputmethod/comm/SimeEnvironment;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/inputmethod/comm/SimeEnvironment;->isLargeScreen()Z

    move-result v3

    if-eqz v3, :cond_16

    .line 893
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v0, 0x1

    .end local v0           #index:I
    .restart local v1       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_eng_full_handwrite_checkbox"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v0

    move v0, v1

    .end local v1           #index:I
    .restart local v0       #index:I
    goto/16 :goto_2

    .line 895
    :cond_16
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v0, 0x1

    .end local v0           #index:I
    .restart local v1       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_eng_box_handwrite_checkbox"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v0

    move v0, v1

    .end local v1           #index:I
    .restart local v0       #index:I
    goto/16 :goto_2

    .line 904
    .end local v0           #index:I
    .restart local v1       #index:I
    :cond_17
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v0, v1, 0x1

    .end local v1           #index:I
    .restart local v0       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_korean_box_handwrite_checkbox"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v1

    goto/16 :goto_3

    .line 907
    .end local v0           #index:I
    .restart local v1       #index:I
    :cond_18
    invoke-static {}, Lcom/samsung/inputmethod/comm/SimeEnvironment;->getInstance()Lcom/samsung/inputmethod/comm/SimeEnvironment;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/inputmethod/comm/SimeEnvironment;->isLargeScreen()Z

    move-result v3

    if-eqz v3, :cond_19

    .line 908
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v0, v1, 0x1

    .end local v1           #index:I
    .restart local v0       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_korean_full_handwrite_checkbox"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v1

    goto/16 :goto_3

    .line 910
    .end local v0           #index:I
    .restart local v1       #index:I
    :cond_19
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v0, v1, 0x1

    .end local v1           #index:I
    .restart local v0       #index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "samsungTabedit_korean_box_handwrite_checkbox"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    aput-boolean v4, v3, v1

    goto/16 :goto_3
.end method

.method private initTabtabData()V
    .locals 8

    .prologue
    .line 610
    invoke-static {}, Lcom/samsung/inputmethod/comm/SimeEnvironment;->getInstance()Lcom/samsung/inputmethod/comm/SimeEnvironment;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/inputmethod/comm/SimeEnvironment;->isTabletModel()Z

    move-result v5

    if-nez v5, :cond_1

    .line 639
    :cond_0
    :goto_0
    return-void

    .line 612
    :cond_1
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v6, "samsungTabmode_order"

    const-string v7, ""

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    sput-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabModeOrder:Ljava/lang/String;

    .line 614
    invoke-static {}, Lcom/samsung/inputmethod/tabmode/SimeTabDataService;->isDialogListInit()Z

    move-result v5

    if-nez v5, :cond_2

    .line 615
    invoke-static {}, Lcom/samsung/inputmethod/tabmode/SimeTabDataService;->getInstance()Lcom/samsung/inputmethod/tabmode/SimeTabDataService;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/inputmethod/tabmode/SimeTabDataService;->initDialogModeList()V

    .line 617
    :cond_2
    invoke-static {}, Lcom/samsung/inputmethod/tabmode/SimeTabDataService;->getInstance()Lcom/samsung/inputmethod/tabmode/SimeTabDataService;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/inputmethod/tabmode/SimeTabDataService;->getDialogModeList()[[I

    move-result-object v5

    array-length v1, v5

    .line 618
    .local v1, dialogListSize:I
    new-array v5, v1, [Z

    sput-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    .line 620
    invoke-static {}, Lcom/samsung/inputmethod/service/xt9/SimeDecoderService;->imIsHKTWModel()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 621
    invoke-static {}, Lcom/samsung/inputmethod/setting/SimeSetting;->initTabEditListTCHN()V

    .line 627
    :goto_1
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabModeOrder:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 628
    const/4 v0, 0x0

    .line 629
    .local v0, checked:Z
    new-instance v4, Ljava/util/Vector;

    invoke-direct {v4}, Ljava/util/Vector;-><init>()V

    .line 630
    .local v4, modeList:Ljava/util/Vector;,"Ljava/util/Vector<Ljava/lang/Integer;>;"
    const/4 v2, 0x0

    .local v2, i:I
    :goto_2
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    array-length v5, v5

    if-ge v2, v5, :cond_5

    .line 631
    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    aget-boolean v0, v5, v2

    .line 632
    invoke-static {}, Lcom/samsung/inputmethod/tabmode/SimeTabDataService;->getInstance()Lcom/samsung/inputmethod/tabmode/SimeTabDataService;

    move-result-object v5

    invoke-virtual {v5, v2}, Lcom/samsung/inputmethod/tabmode/SimeTabDataService;->getDialogListMode(I)I

    move-result v3

    .line 633
    .local v3, mode:I
    if-eqz v0, :cond_3

    .line 634
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 630
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 623
    .end local v0           #checked:Z
    .end local v2           #i:I
    .end local v3           #mode:I
    .end local v4           #modeList:Ljava/util/Vector;,"Ljava/util/Vector<Ljava/lang/Integer;>;"
    :cond_4
    invoke-static {}, Lcom/samsung/inputmethod/setting/SimeSetting;->initTabEditListSCHN()V

    goto :goto_1

    .line 637
    .restart local v0       #checked:Z
    .restart local v2       #i:I
    .restart local v4       #modeList:Ljava/util/Vector;,"Ljava/util/Vector<Ljava/lang/Integer;>;"
    :cond_5
    invoke-static {v4}, Lcom/samsung/inputmethod/tabmode/SimeTabDataService;->encodeTabBarOrder(Ljava/util/Vector;)Ljava/lang/String;

    move-result-object v5

    sput-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabModeOrder:Ljava/lang/String;

    goto :goto_0
.end method

.method public static isHWBoxMode(I)Z
    .locals 1
    .parameter "languageId"

    .prologue
    .line 1316
    const/4 v0, 0x0

    .line 1318
    .local v0, hwMode:Z
    sparse-switch p0, :sswitch_data_0

    .line 1338
    :goto_0
    return v0

    .line 1320
    :sswitch_0
    sget-boolean v0, Lcom/samsung/inputmethod/setting/SimeSetting;->IS_BOX_EN:Z

    .line 1321
    goto :goto_0

    .line 1323
    :sswitch_1
    sget-boolean v0, Lcom/samsung/inputmethod/setting/SimeSetting;->IS_BOX_ZH_CN:Z

    .line 1324
    goto :goto_0

    .line 1326
    :sswitch_2
    sget-boolean v0, Lcom/samsung/inputmethod/setting/SimeSetting;->IS_BOX_KO:Z

    .line 1327
    goto :goto_0

    .line 1329
    :sswitch_3
    sget-boolean v0, Lcom/samsung/inputmethod/setting/SimeSetting;->IS_BOX_ZH_TW:Z

    .line 1330
    goto :goto_0

    .line 1332
    :sswitch_4
    sget-boolean v0, Lcom/samsung/inputmethod/setting/SimeSetting;->IS_BOX_ZH_HK:Z

    .line 1333
    goto :goto_0

    .line 1318
    :sswitch_data_0
    .sparse-switch
        0x10000 -> :sswitch_0
        0x20000 -> :sswitch_1
        0x30000 -> :sswitch_2
        0x40000 -> :sswitch_3
        0x50000 -> :sswitch_4
    .end sparse-switch
.end method

.method public static isStrokeZhCn()Z
    .locals 1

    .prologue
    .line 1840
    sget-boolean v0, Lcom/samsung/inputmethod/setting/SimeSetting;->IS_STROKE_ZH_CN:Z

    return v0
.end method

.method public static isStrokeZhTw()Z
    .locals 1

    .prologue
    .line 1849
    sget-boolean v0, Lcom/samsung/inputmethod/setting/SimeSetting;->IS_STROKE_ZH_TW:Z

    return v0
.end method

.method public static releaseInstance()V
    .locals 1

    .prologue
    .line 404
    sget v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mRefCount:I

    add-int/lit8 v0, v0, -0x1

    sput v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mRefCount:I

    .line 405
    sget v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mRefCount:I

    if-nez v0, :cond_0

    .line 406
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mInstance:Lcom/samsung/inputmethod/setting/SimeSetting;

    .line 408
    :cond_0
    return-void
.end method

.method public static resetSetting(Landroid/content/SharedPreferences;)V
    .locals 3
    .parameter "pref"

    .prologue
    .line 1771
    sget-object v1, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1773
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    .line 1774
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1776
    sget-object v1, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    invoke-static {v1}, Lcom/samsung/inputmethod/setting/SimeSetting;->getInstance(Landroid/content/SharedPreferences;)Lcom/samsung/inputmethod/setting/SimeSetting;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Lcom/samsung/inputmethod/setting/SimeSetting;->initConfs(Z)V

    .line 1778
    invoke-static {}, Lcom/samsung/inputmethod/setting/SimeSetting;->writeBack()V

    .line 1780
    return-void
.end method

.method public static saveFloatingKeyboardPosition(IIZ)V
    .locals 2
    .parameter "x"
    .parameter "y"
    .parameter "isHorizontal"

    .prologue
    .line 1807
    sget-object v1, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1809
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    if-eqz p2, :cond_0

    .line 1810
    const-string v1, "floating_h_location_x"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1811
    const-string v1, "floating_h_location_y"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1816
    :goto_0
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1818
    return-void

    .line 1813
    :cond_0
    const-string v1, "floating_location_x"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1814
    const-string v1, "floating_location_y"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    goto :goto_0
.end method

.method public static savePrefrence(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 4
    .parameter "key"
    .parameter "obj"

    .prologue
    .line 297
    const/4 v0, 0x1

    .line 300
    .local v0, bRet:Z
    :try_start_0
    sget-object v3, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 301
    .local v2, editor:Landroid/content/SharedPreferences$Editor;
    instance-of v3, p1, Ljava/lang/Integer;

    if-eqz v3, :cond_1

    .line 302
    check-cast p1, Ljava/lang/Integer;

    .end local p1
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-interface {v2, p0, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 312
    :goto_0
    if-eqz v0, :cond_0

    .line 313
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v0

    .line 321
    .end local v2           #editor:Landroid/content/SharedPreferences$Editor;
    :cond_0
    :goto_1
    return v0

    .line 303
    .restart local v2       #editor:Landroid/content/SharedPreferences$Editor;
    .restart local p1
    :cond_1
    instance-of v3, p1, Ljava/lang/Boolean;

    if-eqz v3, :cond_2

    .line 304
    check-cast p1, Ljava/lang/Boolean;

    .end local p1
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-interface {v2, p0, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 315
    .end local v2           #editor:Landroid/content/SharedPreferences$Editor;
    :catch_0
    move-exception v1

    .line 317
    .local v1, e:Ljava/lang/ClassCastException;
    invoke-virtual {v1}, Ljava/lang/ClassCastException;->printStackTrace()V

    .line 318
    const/4 v0, 0x0

    goto :goto_1

    .line 305
    .end local v1           #e:Ljava/lang/ClassCastException;
    .restart local v2       #editor:Landroid/content/SharedPreferences$Editor;
    .restart local p1
    :cond_2
    :try_start_1
    instance-of v3, p1, Ljava/lang/String;

    if-eqz v3, :cond_3

    .line 306
    check-cast p1, Ljava/lang/String;

    .end local p1
    invoke-interface {v2, p0, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;
    :try_end_1
    .catch Ljava/lang/ClassCastException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 310
    .restart local p1
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static saveRecentOnehandMode(I)V
    .locals 2
    .parameter "mode"

    .prologue
    .line 1797
    sget-object v1, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1798
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v1, "samsungRecentOneHandMode"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1799
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1800
    return-void
.end method

.method public static saveSymHistory()V
    .locals 3

    .prologue
    .line 699
    sget-boolean v1, Lcom/samsung/inputmethod/comm/SimeFeature;->SIME_USE_TAB_SYMBOL:Z

    if-eqz v1, :cond_1

    .line 700
    :goto_0
    sget-object v1, Lcom/samsung/inputmethod/setting/SimeSetting;->mSymHistoryList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    sget v2, Lcom/samsung/inputmethod/setting/SimeSetting;->MAX_LATEST_SYM_COUNT:I

    if-le v1, v2, :cond_0

    .line 701
    sget-object v1, Lcom/samsung/inputmethod/setting/SimeSetting;->mSymHistoryList:Ljava/util/ArrayList;

    sget-object v2, Lcom/samsung/inputmethod/setting/SimeSetting;->mSymHistoryList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_0

    .line 703
    :cond_0
    sget-object v1, Lcom/samsung/inputmethod/setting/SimeSetting;->mSymHistoryList:Ljava/util/ArrayList;

    sget-object v2, Lcom/samsung/inputmethod/setting/SimeSetting;->SYM_HISTORY_SPLIT_STR:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/samsung/inputmethod/comm/SimeUtility;->getStringFromList(Ljava/util/ArrayList;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/samsung/inputmethod/setting/SimeSetting;->mSymHistory:Ljava/lang/String;

    .line 708
    sget-object v1, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 709
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v1, "samsungLatestSymbols"

    sget-object v2, Lcom/samsung/inputmethod/setting/SimeSetting;->mSymHistory:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 710
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 712
    :cond_1
    return-void
.end method

.method public static saveToSymbHistory(Ljava/lang/String;)V
    .locals 2
    .parameter "newSymStr"

    .prologue
    .line 687
    sget-boolean v0, Lcom/samsung/inputmethod/comm/SimeFeature;->SIME_USE_TAB_SYMBOL:Z

    if-eqz v0, :cond_0

    .line 688
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 689
    sget-object v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mSymHistoryList:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 690
    sget-object v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mSymHistoryList:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 691
    invoke-static {}, Lcom/samsung/inputmethod/setting/SimeSetting;->saveSymHistory()V

    .line 694
    :cond_0
    return-void
.end method

.method public static setAutoCapitalization(Z)V
    .locals 1
    .parameter "v"

    .prologue
    .line 1198
    sget-boolean v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mAutoCapitalizationPref:Z

    if-ne v0, p0, :cond_0

    .line 1200
    :goto_0
    return-void

    .line 1199
    :cond_0
    sput-boolean p0, Lcom/samsung/inputmethod/setting/SimeSetting;->mAutoCapitalizationPref:Z

    goto :goto_0
.end method

.method public static setCharPreview(Z)V
    .locals 1
    .parameter "bCharPreview"

    .prologue
    .line 1161
    sget-boolean v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mCharPreview:Z

    if-ne v0, p0, :cond_0

    .line 1166
    :goto_0
    return-void

    .line 1165
    :cond_0
    sput-boolean p0, Lcom/samsung/inputmethod/setting/SimeSetting;->mCharPreview:Z

    goto :goto_0
.end method

.method public static setConvert(I)V
    .locals 1
    .parameter "v"

    .prologue
    .line 1246
    sget v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mConvert:I

    if-ne v0, p0, :cond_0

    .line 1248
    :goto_0
    return-void

    .line 1247
    :cond_0
    sput p0, Lcom/samsung/inputmethod/setting/SimeSetting;->mConvert:I

    goto :goto_0
.end method

.method public static setDefine(Z)V
    .locals 1
    .parameter "v"

    .prologue
    .line 1254
    sget-boolean v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mDefine:Z

    if-ne v0, p0, :cond_0

    .line 1256
    :goto_0
    return-void

    .line 1255
    :cond_0
    sput-boolean p0, Lcom/samsung/inputmethod/setting/SimeSetting;->mDefine:Z

    goto :goto_0
.end method

.method public static setFullSentence(Z)V
    .locals 2
    .parameter "v"

    .prologue
    .line 1238
    const-string v0, "samsungFullSentence"

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/inputmethod/setting/SimeSetting;->savePrefrence(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 1239
    return-void
.end method

.method public static setFuzzyPinyinState(ZI)V
    .locals 3
    .parameter "state"
    .parameter "num"

    .prologue
    .line 1400
    sget-object v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mFuzzyStateFlag:[Z

    aput-boolean p0, v0, p1

    .line 1401
    if-eqz p0, :cond_0

    .line 1402
    packed-switch p1, :pswitch_data_0

    .line 1431
    const-string v0, "SamsungIMESettings"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setFuzzyPinyinState switch num error num="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1469
    :goto_0
    return-void

    .line 1404
    :pswitch_0
    sget v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mFuzzyState:I

    or-int/lit8 v0, v0, 0x1

    sput v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mFuzzyState:I

    goto :goto_0

    .line 1407
    :pswitch_1
    sget v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mFuzzyState:I

    or-int/lit8 v0, v0, 0x2

    sput v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mFuzzyState:I

    goto :goto_0

    .line 1410
    :pswitch_2
    sget v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mFuzzyState:I

    or-int/lit8 v0, v0, 0x4

    sput v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mFuzzyState:I

    goto :goto_0

    .line 1413
    :pswitch_3
    sget v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mFuzzyState:I

    or-int/lit8 v0, v0, 0x8

    sput v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mFuzzyState:I

    goto :goto_0

    .line 1416
    :pswitch_4
    sget v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mFuzzyState:I

    or-int/lit8 v0, v0, 0x10

    sput v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mFuzzyState:I

    goto :goto_0

    .line 1419
    :pswitch_5
    sget v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mFuzzyState:I

    or-int/lit8 v0, v0, 0x20

    sput v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mFuzzyState:I

    goto :goto_0

    .line 1422
    :pswitch_6
    sget v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mFuzzyState:I

    or-int/lit8 v0, v0, 0x40

    sput v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mFuzzyState:I

    goto :goto_0

    .line 1425
    :pswitch_7
    sget v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mFuzzyState:I

    or-int/lit16 v0, v0, 0x80

    sput v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mFuzzyState:I

    goto :goto_0

    .line 1428
    :pswitch_8
    sget v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mFuzzyState:I

    or-int/lit16 v0, v0, 0x100

    sput v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mFuzzyState:I

    goto :goto_0

    .line 1436
    :cond_0
    packed-switch p1, :pswitch_data_1

    .line 1465
    const-string v0, "SamsungIMESettings"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setFuzzyPinyinState switch num error num="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1438
    :pswitch_9
    sget v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mFuzzyState:I

    and-int/lit8 v0, v0, -0x2

    sput v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mFuzzyState:I

    goto :goto_0

    .line 1441
    :pswitch_a
    sget v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mFuzzyState:I

    and-int/lit8 v0, v0, -0x3

    sput v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mFuzzyState:I

    goto :goto_0

    .line 1444
    :pswitch_b
    sget v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mFuzzyState:I

    and-int/lit8 v0, v0, -0x5

    sput v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mFuzzyState:I

    goto :goto_0

    .line 1447
    :pswitch_c
    sget v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mFuzzyState:I

    and-int/lit8 v0, v0, -0x9

    sput v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mFuzzyState:I

    goto :goto_0

    .line 1450
    :pswitch_d
    sget v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mFuzzyState:I

    and-int/lit8 v0, v0, -0x11

    sput v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mFuzzyState:I

    goto :goto_0

    .line 1453
    :pswitch_e
    sget v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mFuzzyState:I

    and-int/lit8 v0, v0, -0x21

    sput v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mFuzzyState:I

    goto/16 :goto_0

    .line 1456
    :pswitch_f
    sget v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mFuzzyState:I

    and-int/lit8 v0, v0, -0x41

    sput v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mFuzzyState:I

    goto/16 :goto_0

    .line 1459
    :pswitch_10
    sget v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mFuzzyState:I

    and-int/lit16 v0, v0, -0x81

    sput v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mFuzzyState:I

    goto/16 :goto_0

    .line 1462
    :pswitch_11
    sget v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mFuzzyState:I

    and-int/lit16 v0, v0, -0x101

    sput v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mFuzzyState:I

    goto/16 :goto_0

    .line 1402
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch

    .line 1436
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
    .end packed-switch
.end method

.method public static setHWBoxMode(IZ)V
    .locals 0
    .parameter "languageId"
    .parameter "isBoxMode"

    .prologue
    .line 1346
    sparse-switch p0, :sswitch_data_0

    .line 1371
    :goto_0
    return-void

    .line 1349
    :sswitch_0
    sput-boolean p1, Lcom/samsung/inputmethod/setting/SimeSetting;->IS_BOX_EN:Z

    goto :goto_0

    .line 1353
    :sswitch_1
    sput-boolean p1, Lcom/samsung/inputmethod/setting/SimeSetting;->IS_BOX_ZH_CN:Z

    goto :goto_0

    .line 1357
    :sswitch_2
    sput-boolean p1, Lcom/samsung/inputmethod/setting/SimeSetting;->IS_BOX_KO:Z

    goto :goto_0

    .line 1361
    :sswitch_3
    sput-boolean p1, Lcom/samsung/inputmethod/setting/SimeSetting;->IS_BOX_ZH_TW:Z

    goto :goto_0

    .line 1365
    :sswitch_4
    sput-boolean p1, Lcom/samsung/inputmethod/setting/SimeSetting;->IS_BOX_ZH_HK:Z

    goto :goto_0

    .line 1346
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x10000 -> :sswitch_0
        0x20000 -> :sswitch_1
        0x30000 -> :sswitch_2
        0x40000 -> :sswitch_3
        0x50000 -> :sswitch_4
    .end sparse-switch
.end method

.method public static setHandwriteLineColorIndex(I)V
    .locals 0
    .parameter "colorIndex"

    .prologue
    .line 1308
    sput p0, Lcom/samsung/inputmethod/setting/SimeSetting;->mLineColorIndex:I

    .line 1309
    return-void
.end method

.method public static setHandwriteLineGradient(ZIII)V
    .locals 0
    .parameter "en"
    .parameter "scolor"
    .parameter "mcolor"
    .parameter "ecolor"

    .prologue
    .line 1687
    sput-boolean p0, Lcom/samsung/inputmethod/setting/SimeSetting;->mIsLineGradient:Z

    .line 1688
    sput p1, Lcom/samsung/inputmethod/setting/SimeSetting;->mLineGradientStartcolor:I

    .line 1689
    sput p2, Lcom/samsung/inputmethod/setting/SimeSetting;->mLineGradientMiddlecolor:I

    .line 1690
    sput p3, Lcom/samsung/inputmethod/setting/SimeSetting;->mLineGradientEndcolor:I

    .line 1691
    return-void
.end method

.method public static setHandwriteLineWidthIndex(I)V
    .locals 0
    .parameter "colorIndex"

    .prologue
    .line 1379
    sput p0, Lcom/samsung/inputmethod/setting/SimeSetting;->mLineWidthIndex:I

    .line 1380
    return-void
.end method

.method public static setHandwriteRecgType(I)V
    .locals 0
    .parameter "type"

    .prologue
    .line 1677
    sput p0, Lcom/samsung/inputmethod/setting/SimeSetting;->mRecgType:I

    .line 1678
    return-void
.end method

.method public static setHandwritingStyle(I)V
    .locals 1
    .parameter "handwritingStyle"

    .prologue
    .line 1272
    sget v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mHandwritingStyle:I

    if-ne v0, p0, :cond_0

    .line 1274
    :goto_0
    return-void

    .line 1273
    :cond_0
    sput p0, Lcom/samsung/inputmethod/setting/SimeSetting;->mHandwritingStyle:I

    goto :goto_0
.end method

.method public static setKeySound(Z)V
    .locals 1
    .parameter "v"

    .prologue
    .line 1105
    sget-boolean v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mKeySound:Z

    if-ne v0, p0, :cond_0

    .line 1107
    :goto_0
    return-void

    .line 1106
    :cond_0
    sput-boolean p0, Lcom/samsung/inputmethod/setting/SimeSetting;->mKeySound:Z

    goto :goto_0
.end method

.method public static setKeyboardType(I)V
    .locals 1
    .parameter "keyboardType"

    .prologue
    .line 1281
    sget v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mKeyboardType:I

    if-ne v0, p0, :cond_0

    .line 1283
    :goto_0
    return-void

    .line 1282
    :cond_0
    sput p0, Lcom/samsung/inputmethod/setting/SimeSetting;->mKeyboardType:I

    goto :goto_0
.end method

.method public static setMultiModalStatus(I)V
    .locals 1
    .parameter "v"

    .prologue
    .line 1132
    sget v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mMultiModalStatus:I

    if-ne v0, p0, :cond_0

    .line 1134
    :goto_0
    return-void

    .line 1133
    :cond_0
    sput p0, Lcom/samsung/inputmethod/setting/SimeSetting;->mMultiModalStatus:I

    goto :goto_0
.end method

.method public static setPortraitInputMode(I)V
    .locals 3
    .parameter "mainMode"

    .prologue
    .line 1483
    const/high16 v2, 0xf

    and-int v0, p0, v2

    .line 1484
    .local v0, language:I
    const/4 v1, 0x0

    .line 1485
    .local v1, languageId:I
    sparse-switch v0, :sswitch_data_0

    .line 1502
    const/4 v1, 0x1

    .line 1505
    :goto_0
    sget-object v2, Lcom/samsung/inputmethod/setting/SimeSetting;->mPortraitInputlist:[I

    aput p0, v2, v1

    .line 1506
    return-void

    .line 1487
    :sswitch_0
    const/4 v1, 0x0

    .line 1488
    goto :goto_0

    .line 1490
    :sswitch_1
    const/4 v1, 0x2

    .line 1491
    goto :goto_0

    .line 1493
    :sswitch_2
    const/4 v1, 0x1

    .line 1494
    goto :goto_0

    .line 1496
    :sswitch_3
    const/4 v1, 0x4

    .line 1497
    goto :goto_0

    .line 1499
    :sswitch_4
    const/4 v1, 0x3

    .line 1500
    goto :goto_0

    .line 1485
    :sswitch_data_0
    .sparse-switch
        0x10000 -> :sswitch_0
        0x20000 -> :sswitch_2
        0x30000 -> :sswitch_1
        0x40000 -> :sswitch_4
        0x50000 -> :sswitch_3
    .end sparse-switch
.end method

.method static setPortraitInputMode(II)V
    .locals 4
    .parameter "layoutId"
    .parameter "languageId"

    .prologue
    .line 1510
    const/4 v0, 0x0

    .line 1511
    .local v0, newInputMode:I
    packed-switch p1, :pswitch_data_0

    .line 1619
    const-string v1, "SamsungIMESettings"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setPortraitInputMode error languageId="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1625
    :goto_0
    const/4 v1, 0x3

    if-ne p0, v1, :cond_1

    .line 1626
    const/4 v1, 0x0

    invoke-static {p1, v1}, Lcom/samsung/inputmethod/setting/SimeSetting;->setHWBoxMode(IZ)V

    .line 1631
    :cond_0
    :goto_1
    sget-object v1, Lcom/samsung/inputmethod/setting/SimeSetting;->mPortraitInputlist:[I

    aput v0, v1, p1

    .line 1632
    return-void

    .line 1513
    :pswitch_0
    packed-switch p0, :pswitch_data_1

    .line 1527
    :pswitch_1
    const/high16 v0, 0x1111

    .line 1528
    goto :goto_0

    .line 1515
    :pswitch_2
    const/high16 v0, 0x1111

    .line 1516
    goto :goto_0

    .line 1518
    :pswitch_3
    const/high16 v0, 0x1211

    .line 1519
    goto :goto_0

    .line 1521
    :pswitch_4
    const v0, 0x33010100

    .line 1522
    goto :goto_0

    .line 1524
    :pswitch_5
    const v0, 0x34010100

    .line 1525
    goto :goto_0

    .line 1533
    :pswitch_6
    packed-switch p0, :pswitch_data_2

    .line 1550
    const/high16 v0, 0x1102

    .line 1551
    goto :goto_0

    .line 1535
    :pswitch_7
    const/high16 v0, 0x1102

    .line 1536
    goto :goto_0

    .line 1538
    :pswitch_8
    const/high16 v0, 0x1202

    .line 1539
    goto :goto_0

    .line 1541
    :pswitch_9
    const/high16 v0, 0x3202

    .line 1542
    goto :goto_0

    .line 1544
    :pswitch_a
    const v0, 0x33020100

    .line 1545
    goto :goto_0

    .line 1547
    :pswitch_b
    const v0, 0x34020100

    .line 1548
    goto :goto_0

    .line 1555
    :pswitch_c
    packed-switch p0, :pswitch_data_3

    .line 1573
    const/high16 v0, 0x2104

    .line 1574
    goto :goto_0

    .line 1557
    :pswitch_d
    const/high16 v0, 0x2104

    .line 1558
    goto :goto_0

    .line 1560
    :pswitch_e
    const/high16 v0, 0x2204

    .line 1562
    goto :goto_0

    .line 1564
    :pswitch_f
    const/high16 v0, 0x3204

    .line 1565
    goto :goto_0

    .line 1567
    :pswitch_10
    const v0, 0x33040100

    .line 1568
    goto :goto_0

    .line 1570
    :pswitch_11
    const v0, 0x34040100

    .line 1571
    goto :goto_0

    .line 1578
    :pswitch_12
    packed-switch p0, :pswitch_data_4

    .line 1595
    const/high16 v0, -0x6efb

    .line 1596
    goto :goto_0

    .line 1580
    :pswitch_13
    const/high16 v0, -0x6efb

    .line 1581
    goto :goto_0

    .line 1583
    :pswitch_14
    const/high16 v0, 0x3205

    .line 1584
    goto :goto_0

    .line 1586
    :pswitch_15
    const/high16 v0, 0x3205

    .line 1587
    goto :goto_0

    .line 1589
    :pswitch_16
    const v0, 0x33050100

    .line 1590
    goto :goto_0

    .line 1592
    :pswitch_17
    const v0, 0x34050100

    .line 1593
    goto :goto_0

    .line 1600
    :pswitch_18
    packed-switch p0, :pswitch_data_5

    .line 1614
    :pswitch_19
    const/high16 v0, 0x1113

    .line 1615
    goto :goto_0

    .line 1602
    :pswitch_1a
    const/high16 v0, 0x1113

    .line 1603
    goto :goto_0

    .line 1605
    :pswitch_1b
    const/high16 v0, 0x1203

    .line 1606
    goto :goto_0

    .line 1608
    :pswitch_1c
    const v0, 0x33030100

    .line 1609
    goto :goto_0

    .line 1611
    :pswitch_1d
    const v0, 0x34030100

    .line 1612
    goto :goto_0

    .line 1627
    :cond_1
    const/4 v1, 0x4

    if-ne p0, v1, :cond_0

    .line 1628
    const/4 v1, 0x1

    invoke-static {p1, v1}, Lcom/samsung/inputmethod/setting/SimeSetting;->setHWBoxMode(IZ)V

    goto :goto_1

    .line 1511
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_6
        :pswitch_18
        :pswitch_c
        :pswitch_12
    .end packed-switch

    .line 1513
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_4
        :pswitch_5
    .end packed-switch

    .line 1533
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch

    .line 1555
    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
    .end packed-switch

    .line 1578
    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
    .end packed-switch

    .line 1600
    :pswitch_data_5
    .packed-switch 0x0
        :pswitch_1a
        :pswitch_1b
        :pswitch_19
        :pswitch_1c
        :pswitch_1d
    .end packed-switch
.end method

.method public static setPrediction(Z)V
    .locals 1
    .parameter "v"

    .prologue
    .line 1123
    sget-boolean v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mPrediction:Z

    if-ne v0, p0, :cond_0

    .line 1125
    :goto_0
    return-void

    .line 1124
    :cond_0
    sput-boolean p0, Lcom/samsung/inputmethod/setting/SimeSetting;->mPrediction:Z

    goto :goto_0
.end method

.method public static setShowToturial(Z)V
    .locals 2
    .parameter "v"

    .prologue
    .line 1746
    const-string v0, "samsungime_showtoturial"

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/inputmethod/setting/SimeSetting;->savePrefrence(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 1747
    return-void
.end method

.method public static setSpeed(I)V
    .locals 1
    .parameter "speed"

    .prologue
    .line 1263
    sget v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mSpeed:I

    if-ne v0, p0, :cond_0

    .line 1265
    :goto_0
    return-void

    .line 1264
    :cond_0
    sput p0, Lcom/samsung/inputmethod/setting/SimeSetting;->mSpeed:I

    goto :goto_0
.end method

.method public static setStrokeZhCn(Z)V
    .locals 1
    .parameter "isStroke"

    .prologue
    .line 1859
    sget-boolean v0, Lcom/samsung/inputmethod/setting/SimeSetting;->IS_STROKE_ZH_CN:Z

    if-eq v0, p0, :cond_0

    .line 1860
    sput-boolean p0, Lcom/samsung/inputmethod/setting/SimeSetting;->IS_STROKE_ZH_CN:Z

    .line 1862
    :cond_0
    return-void
.end method

.method public static setStrokeZhTw(Z)V
    .locals 1
    .parameter "isStroke"

    .prologue
    .line 1871
    sget-boolean v0, Lcom/samsung/inputmethod/setting/SimeSetting;->IS_STROKE_ZH_TW:Z

    if-eq v0, p0, :cond_0

    .line 1872
    sput-boolean p0, Lcom/samsung/inputmethod/setting/SimeSetting;->IS_STROKE_ZH_TW:Z

    .line 1874
    :cond_0
    return-void
.end method

.method public static setTabBarOrder(Ljava/lang/String;)V
    .locals 1
    .parameter "newOrder"

    .prologue
    .line 1067
    sget-boolean v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mIsTabletModel:Z

    if-nez v0, :cond_0

    .line 1070
    :goto_0
    return-void

    .line 1069
    :cond_0
    sput-object p0, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabModeOrder:Ljava/lang/String;

    goto :goto_0
.end method

.method public static setTabInputModeCheckedVaule([Z)Z
    .locals 3
    .parameter "newV"

    .prologue
    .line 1051
    sget-boolean v1, Lcom/samsung/inputmethod/setting/SimeSetting;->mIsTabletModel:Z

    if-eqz v1, :cond_0

    sget-object v1, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    array-length v1, v1

    array-length v2, p0

    if-eq v1, v2, :cond_1

    .line 1053
    :cond_0
    const/4 v1, 0x0

    .line 1058
    :goto_0
    return v1

    .line 1055
    :cond_1
    const/4 v0, 0x0

    .local v0, i:I
    :goto_1
    sget-object v1, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 1056
    sget-object v1, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    aget-boolean v2, p0, v0

    aput-boolean v2, v1, v0

    .line 1055
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1058
    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static setVibrate(Z)V
    .locals 1
    .parameter "v"

    .prologue
    .line 1114
    sget-boolean v0, Lcom/samsung/inputmethod/setting/SimeSetting;->mVibrate:Z

    if-ne v0, p0, :cond_0

    .line 1116
    :goto_0
    return-void

    .line 1115
    :cond_0
    sput-boolean p0, Lcom/samsung/inputmethod/setting/SimeSetting;->mVibrate:Z

    goto :goto_0
.end method

.method public static writeBack()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 324
    sget-object v1, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 325
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v1, "samsungVibrate"

    sget-boolean v2, Lcom/samsung/inputmethod/setting/SimeSetting;->mVibrate:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 326
    const-string v1, "samsungSound"

    sget-boolean v2, Lcom/samsung/inputmethod/setting/SimeSetting;->mKeySound:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 327
    const-string v1, "samsungPrediction"

    sget-boolean v2, Lcom/samsung/inputmethod/setting/SimeSetting;->mPrediction:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 328
    const-string v1, "samsungSpaceInput"

    sget-boolean v2, Lcom/samsung/inputmethod/setting/SimeSetting;->mSpaceInput:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 329
    const-string v1, "samsungAutoFullStop"

    sget-boolean v2, Lcom/samsung/inputmethod/setting/SimeSetting;->mAutoFullStop:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 330
    const-string v1, "samsungLinkToContacts"

    sget-boolean v2, Lcom/samsung/inputmethod/setting/SimeSetting;->mLinkToContacts:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 331
    const-string v1, "samsungPointingKeyboard"

    sget-boolean v2, Lcom/samsung/inputmethod/setting/SimeSetting;->mPointingKeyboard:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 332
    const-string v1, "samsungFloatingKeyboard"

    sget-boolean v2, Lcom/samsung/inputmethod/setting/SimeSetting;->mFloatingKeyboard:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 334
    const-string v1, "samsungCharPreview"

    sget-boolean v2, Lcom/samsung/inputmethod/setting/SimeSetting;->mCharPreview:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 335
    const-string v1, "samsungVoiceInput"

    sget-boolean v2, Lcom/samsung/inputmethod/setting/SimeSetting;->mVoiceInputOption:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 337
    const-string v1, "samsungSwipeOnKeyboard"

    sget v2, Lcom/samsung/inputmethod/setting/SimeSetting;->mSwipeOnKeyboard:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 338
    const-string v1, "samsungKeyboardTypeInput"

    sget v2, Lcom/samsung/inputmethod/setting/SimeSetting;->mKeyboardType:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 339
    const-string v1, "samsungHandwritingStyle"

    sget v2, Lcom/samsung/inputmethod/setting/SimeSetting;->mHandwritingStyle:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 341
    const-string v1, "samsungAutoCapitalization"

    sget-boolean v2, Lcom/samsung/inputmethod/setting/SimeSetting;->mAutoCapitalizationPref:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 342
    const-string v1, "samsungMultiModalStatus"

    sget v2, Lcom/samsung/inputmethod/setting/SimeSetting;->mMultiModalStatus:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 344
    const-string v1, "samsunghandwrite_color_index"

    sget v2, Lcom/samsung/inputmethod/setting/SimeSetting;->mLineColorIndex:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 345
    const-string v1, "samsunghandwrite_width_index"

    sget v2, Lcom/samsung/inputmethod/setting/SimeSetting;->mLineWidthIndex:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 346
    const-string v1, "samsunghandwrite_speed"

    sget v2, Lcom/samsung/inputmethod/setting/SimeSetting;->mSpeed:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 347
    const-string v1, "samsungPortraitInputEng"

    sget-object v2, Lcom/samsung/inputmethod/setting/SimeSetting;->mPortraitInputlist:[I

    aget v2, v2, v3

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 348
    const-string v1, "samsungPortraitInputChn"

    sget-object v2, Lcom/samsung/inputmethod/setting/SimeSetting;->mPortraitInputlist:[I

    aget v2, v2, v4

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 349
    const-string v1, "samsungPortraitInputKor"

    sget-object v2, Lcom/samsung/inputmethod/setting/SimeSetting;->mPortraitInputlist:[I

    aget v2, v2, v5

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 350
    const-string v1, "samsungPortraitInputTW"

    sget-object v2, Lcom/samsung/inputmethod/setting/SimeSetting;->mPortraitInputlist:[I

    aget v2, v2, v6

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 351
    const-string v1, "samsungPortraitInputHK"

    sget-object v2, Lcom/samsung/inputmethod/setting/SimeSetting;->mPortraitInputlist:[I

    aget v2, v2, v7

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 352
    const-string v1, "samsunghandwrite_type"

    sget v2, Lcom/samsung/inputmethod/setting/SimeSetting;->mRecgType:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 353
    const-string v1, "samsunghandwrite_linegradient"

    sget-boolean v2, Lcom/samsung/inputmethod/setting/SimeSetting;->mIsLineGradient:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 354
    const-string v1, "samsunghandwrite_linegradient_scolor"

    sget v2, Lcom/samsung/inputmethod/setting/SimeSetting;->mLineGradientStartcolor:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 355
    const-string v1, "samsunghandwrite_linegradient_mcolor"

    sget v2, Lcom/samsung/inputmethod/setting/SimeSetting;->mLineGradientMiddlecolor:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 356
    const-string v1, "samsunghandwrite_linegradient_ecolor"

    sget v2, Lcom/samsung/inputmethod/setting/SimeSetting;->mLineGradientEndcolor:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 357
    const-string v1, "samsungConvert"

    sget v2, Lcom/samsung/inputmethod/setting/SimeSetting;->mConvert:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 358
    const-string v1, "samsunghandwrite_mode"

    sget v2, Lcom/samsung/inputmethod/setting/SimeSetting;->mHWModeIndex:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 360
    const-string v1, "samsungDefine"

    sget-boolean v2, Lcom/samsung/inputmethod/setting/SimeSetting;->mDefine:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 362
    const-string v1, "samsungLanguageChinese"

    sget-boolean v2, Lcom/samsung/inputmethod/setting/SimeSetting;->mLanguageChinese:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 364
    const-string v1, "samsungLanguageChineseTW"

    sget-boolean v2, Lcom/samsung/inputmethod/setting/SimeSetting;->mLanguageChineseTw:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 365
    const-string v1, "samsungLanguageChineseHK"

    sget-boolean v2, Lcom/samsung/inputmethod/setting/SimeSetting;->mLanguageChineseHk:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 366
    const-string v1, "samsung_country"

    sget-object v2, Lcom/samsung/inputmethod/setting/SimeSetting;->mCountry:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 371
    const-string v1, "samsungLanguageEnglish"

    sget-boolean v2, Lcom/samsung/inputmethod/setting/SimeSetting;->mLanguageEnglish:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 372
    const-string v1, "samsungLanguageKorean"

    sget-boolean v2, Lcom/samsung/inputmethod/setting/SimeSetting;->mLanguageKorean:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 374
    const-string v1, "samsungFuzzy_zhz"

    sget-object v2, Lcom/samsung/inputmethod/setting/SimeSetting;->mFuzzyStateFlag:[Z

    aget-boolean v2, v2, v3

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 375
    const-string v1, "samsungFuzzy_chc"

    sget-object v2, Lcom/samsung/inputmethod/setting/SimeSetting;->mFuzzyStateFlag:[Z

    aget-boolean v2, v2, v4

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 376
    const-string v1, "samsungFuzzy_shs"

    sget-object v2, Lcom/samsung/inputmethod/setting/SimeSetting;->mFuzzyStateFlag:[Z

    aget-boolean v2, v2, v5

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 377
    const-string v1, "samsungFuzzy_nl"

    sget-object v2, Lcom/samsung/inputmethod/setting/SimeSetting;->mFuzzyStateFlag:[Z

    aget-boolean v2, v2, v6

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 378
    const-string v1, "samsungFuzzy_rl"

    sget-object v2, Lcom/samsung/inputmethod/setting/SimeSetting;->mFuzzyStateFlag:[Z

    aget-boolean v2, v2, v7

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 379
    const-string v1, "samsungFuzzy_hf"

    sget-object v2, Lcom/samsung/inputmethod/setting/SimeSetting;->mFuzzyStateFlag:[Z

    const/4 v3, 0x5

    aget-boolean v2, v2, v3

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 380
    const-string v1, "samsungFuzzy_angan"

    sget-object v2, Lcom/samsung/inputmethod/setting/SimeSetting;->mFuzzyStateFlag:[Z

    const/4 v3, 0x6

    aget-boolean v2, v2, v3

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 381
    const-string v1, "samsungFuzzy_engen"

    sget-object v2, Lcom/samsung/inputmethod/setting/SimeSetting;->mFuzzyStateFlag:[Z

    const/4 v3, 0x7

    aget-boolean v2, v2, v3

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 382
    const-string v1, "samsungFuzzy_ingin"

    sget-object v2, Lcom/samsung/inputmethod/setting/SimeSetting;->mFuzzyStateFlag:[Z

    const/16 v3, 0x8

    aget-boolean v2, v2, v3

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 384
    const-string v1, "is_stroke_zh_cn"

    sget-boolean v2, Lcom/samsung/inputmethod/setting/SimeSetting;->IS_STROKE_ZH_CN:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 385
    const-string v1, "is_stroke_zh_tw"

    sget-boolean v2, Lcom/samsung/inputmethod/setting/SimeSetting;->IS_STROKE_ZH_TW:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 387
    const-string v1, "is_hwbox_en"

    sget-boolean v2, Lcom/samsung/inputmethod/setting/SimeSetting;->IS_BOX_EN:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 388
    const-string v1, "is_hwbox_zh_cn"

    sget-boolean v2, Lcom/samsung/inputmethod/setting/SimeSetting;->IS_BOX_ZH_CN:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 389
    const-string v1, "is_hwbox_ko"

    sget-boolean v2, Lcom/samsung/inputmethod/setting/SimeSetting;->IS_BOX_KO:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 390
    const-string v1, "is_hwbox_zh_tw"

    sget-boolean v2, Lcom/samsung/inputmethod/setting/SimeSetting;->IS_BOX_ZH_TW:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 391
    const-string v1, "is_hwbox_zh_hk"

    sget-boolean v2, Lcom/samsung/inputmethod/setting/SimeSetting;->IS_BOX_ZH_HK:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 393
    invoke-static {}, Lcom/samsung/inputmethod/comm/SimeEnvironment;->getInstance()Lcom/samsung/inputmethod/comm/SimeEnvironment;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/inputmethod/comm/SimeEnvironment;->isTabletModel()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 394
    invoke-static {}, Lcom/samsung/inputmethod/setting/SimeSetting;->writeBackTabtabData()V

    .line 397
    :cond_0
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 398
    sget-boolean v1, Lcom/samsung/inputmethod/comm/SimeFeature;->SIME_USE_TAB_SYMBOL:Z

    if-eqz v1, :cond_1

    .line 399
    invoke-static {}, Lcom/samsung/inputmethod/setting/SimeSetting;->saveSymHistory()V

    .line 401
    :cond_1
    return-void
.end method

.method private static writeBackTabEditListSCHN()V
    .locals 6

    .prologue
    .line 715
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 716
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const/4 v1, 0x0

    .line 717
    .local v1, index:I
    const-string v4, "samsungTabedit_chn_34_checkbox"

    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v2, v1, 0x1

    .end local v1           #index:I
    .local v2, index:I
    aget-boolean v5, v5, v1

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 718
    const-string v4, "samsungTabedit_chn_qwerty_checkbox"

    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v2, 0x1

    .end local v2           #index:I
    .restart local v1       #index:I
    aget-boolean v5, v5, v2

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 719
    const-string v4, "samsungTabedit_chn_stroke_checkbox"

    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v2, v1, 0x1

    .end local v1           #index:I
    .restart local v2       #index:I
    aget-boolean v5, v5, v1

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 720
    const-string v4, "samsungTabedit_kor_qwerty_checkbox"

    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v2, 0x1

    .end local v2           #index:I
    .restart local v1       #index:I
    aget-boolean v5, v5, v2

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 722
    invoke-static {}, Lcom/samsung/inputmethod/comm/SimeEnvironment;->getInstance()Lcom/samsung/inputmethod/comm/SimeEnvironment;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/inputmethod/comm/SimeEnvironment;->isVoiceInstalled()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 723
    const-string v4, "samsungTabedit_clipboard_checkbox"

    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v2, v1, 0x1

    .end local v1           #index:I
    .restart local v2       #index:I
    aget-boolean v5, v5, v1

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 724
    const-string v4, "samsungTabedit_emoticon_checkbox"

    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v2, 0x1

    .end local v2           #index:I
    .restart local v1       #index:I
    aget-boolean v5, v5, v2

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 728
    :goto_0
    sget-boolean v4, Lcom/samsung/inputmethod/comm/SimeFeature;->SIME_SUPPORT_MULTI_FLOAT:Z

    if-eqz v4, :cond_2

    .line 729
    invoke-static {}, Lcom/samsung/inputmethod/softkeyboard/SimeSkbManager;->getKeyboardType()Lcom/samsung/inputmethod/softkeyboard/SimeSkbManager$KeyboardType;

    move-result-object v3

    .line 730
    .local v3, keyboardType:Lcom/samsung/inputmethod/softkeyboard/SimeSkbManager$KeyboardType;
    sget-object v4, Lcom/samsung/inputmethod/softkeyboard/SimeSkbManager$KeyboardType;->TYPE_NORMAL:Lcom/samsung/inputmethod/softkeyboard/SimeSkbManager$KeyboardType;

    if-ne v3, v4, :cond_1

    .line 731
    const-string v4, "samsungTabedit_chn_full_handwrite_checkbox"

    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v2, v1, 0x1

    .end local v1           #index:I
    .restart local v2       #index:I
    aget-boolean v5, v5, v1

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 733
    const-string v4, "samsungTabedit_eng_full_handwrite_checkbox"

    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v2, 0x1

    .end local v2           #index:I
    .restart local v1       #index:I
    aget-boolean v5, v5, v2

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 735
    const-string v4, "samsungTabedit_korean_full_handwrite_checkbox"

    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v2, v1, 0x1

    .end local v1           #index:I
    .restart local v2       #index:I
    aget-boolean v5, v5, v1

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move v1, v2

    .line 764
    .end local v2           #index:I
    .end local v3           #keyboardType:Lcom/samsung/inputmethod/softkeyboard/SimeSkbManager$KeyboardType;
    .restart local v1       #index:I
    :goto_1
    const-string v4, "samsungTabedit_eng_qwerty_checkbox"

    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v2, v1, 0x1

    .end local v1           #index:I
    .restart local v2       #index:I
    aget-boolean v5, v5, v1

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 765
    const-string v4, "samsungTabedit_123_symbol_checkbox"

    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v2, 0x1

    .end local v2           #index:I
    .restart local v1       #index:I
    aget-boolean v5, v5, v2

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 766
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 767
    return-void

    .line 726
    :cond_0
    const-string v4, "samsungTabedit_emoticon_checkbox"

    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v2, v1, 0x1

    .end local v1           #index:I
    .restart local v2       #index:I
    aget-boolean v5, v5, v1

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move v1, v2

    .end local v2           #index:I
    .restart local v1       #index:I
    goto :goto_0

    .line 738
    .restart local v3       #keyboardType:Lcom/samsung/inputmethod/softkeyboard/SimeSkbManager$KeyboardType;
    :cond_1
    const-string v4, "samsungTabedit_chn_box_handwrite_checkbox"

    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v2, v1, 0x1

    .end local v1           #index:I
    .restart local v2       #index:I
    aget-boolean v5, v5, v1

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 740
    const-string v4, "samsungTabedit_eng_box_handwrite_checkbox"

    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v2, 0x1

    .end local v2           #index:I
    .restart local v1       #index:I
    aget-boolean v5, v5, v2

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 742
    const-string v4, "samsungTabedit_korean_box_handwrite_checkbox"

    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v2, v1, 0x1

    .end local v1           #index:I
    .restart local v2       #index:I
    aget-boolean v5, v5, v1

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .end local v3           #keyboardType:Lcom/samsung/inputmethod/softkeyboard/SimeSkbManager$KeyboardType;
    :goto_2
    move v1, v2

    .end local v2           #index:I
    .restart local v1       #index:I
    goto :goto_1

    .line 747
    :cond_2
    invoke-static {}, Lcom/samsung/inputmethod/comm/SimeEnvironment;->getInstance()Lcom/samsung/inputmethod/comm/SimeEnvironment;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/inputmethod/comm/SimeEnvironment;->isLargeScreen()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 748
    const-string v4, "samsungTabedit_chn_full_handwrite_checkbox"

    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v2, v1, 0x1

    .end local v1           #index:I
    .restart local v2       #index:I
    aget-boolean v5, v5, v1

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 750
    const-string v4, "samsungTabedit_eng_full_handwrite_checkbox"

    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v2, 0x1

    .end local v2           #index:I
    .restart local v1       #index:I
    aget-boolean v5, v5, v2

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 752
    const-string v4, "samsungTabedit_korean_full_handwrite_checkbox"

    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v2, v1, 0x1

    .end local v1           #index:I
    .restart local v2       #index:I
    aget-boolean v5, v5, v1

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move v1, v2

    .end local v2           #index:I
    .restart local v1       #index:I
    goto :goto_1

    .line 755
    :cond_3
    const-string v4, "samsungTabedit_chn_box_handwrite_checkbox"

    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v2, v1, 0x1

    .end local v1           #index:I
    .restart local v2       #index:I
    aget-boolean v5, v5, v1

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 757
    const-string v4, "samsungTabedit_eng_box_handwrite_checkbox"

    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v2, 0x1

    .end local v2           #index:I
    .restart local v1       #index:I
    aget-boolean v5, v5, v2

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 759
    const-string v4, "samsungTabedit_korean_box_handwrite_checkbox"

    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v2, v1, 0x1

    .end local v1           #index:I
    .restart local v2       #index:I
    aget-boolean v5, v5, v1

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    goto :goto_2
.end method

.method private static writeBackTabEditListTCHN()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 934
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 935
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    invoke-static {}, Lcom/samsung/inputmethod/softkeyboard/SimeSkbManager;->getKeyboardType()Lcom/samsung/inputmethod/softkeyboard/SimeSkbManager$KeyboardType;

    move-result-object v3

    .line 936
    .local v3, keyboardType:Lcom/samsung/inputmethod/softkeyboard/SimeSkbManager$KeyboardType;
    const/4 v1, 0x0

    .line 938
    .local v1, index:I
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v2, v1, 0x1

    .end local v1           #index:I
    .local v2, index:I
    aput-boolean v6, v4, v1

    .line 940
    const-string v4, "samsungTabedit_cangjie_qwerty_checkbox"

    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v2, 0x1

    .end local v2           #index:I
    .restart local v1       #index:I
    aget-boolean v5, v5, v2

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 941
    const-string v4, "samsungTabedit_zhuyin_qwerty_checkbox"

    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v2, v1, 0x1

    .end local v1           #index:I
    .restart local v2       #index:I
    aget-boolean v5, v5, v1

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 943
    const-string v4, "samsungTabedit_zhuyin_34_checkbox"

    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v2, 0x1

    .end local v2           #index:I
    .restart local v1       #index:I
    aget-boolean v5, v5, v2

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 944
    const-string v4, "samsungTabedit_hk_stroke_checkbox"

    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v2, v1, 0x1

    .end local v1           #index:I
    .restart local v2       #index:I
    aget-boolean v5, v5, v1

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 945
    const-string v4, "samsungTabedit_tw_stroke_checkbox"

    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v2, 0x1

    .end local v2           #index:I
    .restart local v1       #index:I
    aget-boolean v5, v5, v2

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 946
    sget-boolean v4, Lcom/samsung/inputmethod/comm/SimeFeature;->SIME_SUPPORT_MULTI_FLOAT:Z

    if-eqz v4, :cond_1

    .line 947
    sget-object v4, Lcom/samsung/inputmethod/softkeyboard/SimeSkbManager$KeyboardType;->TYPE_NORMAL:Lcom/samsung/inputmethod/softkeyboard/SimeSkbManager$KeyboardType;

    if-ne v3, v4, :cond_0

    .line 948
    const-string v4, "samsungTabedit_hk_full_handwrite_checkbox"

    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v2, v1, 0x1

    .end local v1           #index:I
    .restart local v2       #index:I
    aget-boolean v5, v5, v1

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 950
    const-string v4, "samsungTabedit_tw_full_handwrite_checkbox"

    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v2, 0x1

    .end local v2           #index:I
    .restart local v1       #index:I
    aget-boolean v5, v5, v2

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 973
    :goto_0
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v2, v1, 0x1

    .end local v1           #index:I
    .restart local v2       #index:I
    aput-boolean v6, v4, v1

    .line 975
    const-string v4, "samsungTabedit_chn_qwerty_checkbox"

    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v2, 0x1

    .end local v2           #index:I
    .restart local v1       #index:I
    aget-boolean v5, v5, v2

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 976
    const-string v4, "samsungTabedit_chn_34_checkbox"

    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v2, v1, 0x1

    .end local v1           #index:I
    .restart local v2       #index:I
    aget-boolean v5, v5, v1

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 977
    const-string v4, "samsungTabedit_chn_stroke_checkbox"

    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v2, 0x1

    .end local v2           #index:I
    .restart local v1       #index:I
    aget-boolean v5, v5, v2

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 978
    sget-boolean v4, Lcom/samsung/inputmethod/comm/SimeFeature;->SIME_SUPPORT_MULTI_FLOAT:Z

    if-eqz v4, :cond_4

    .line 979
    sget-object v4, Lcom/samsung/inputmethod/softkeyboard/SimeSkbManager$KeyboardType;->TYPE_NORMAL:Lcom/samsung/inputmethod/softkeyboard/SimeSkbManager$KeyboardType;

    if-ne v3, v4, :cond_3

    .line 980
    const-string v4, "samsungTabedit_chn_full_handwrite_checkbox"

    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v2, v1, 0x1

    .end local v1           #index:I
    .restart local v2       #index:I
    aget-boolean v5, v5, v1

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move v1, v2

    .line 996
    .end local v2           #index:I
    .restart local v1       #index:I
    :goto_1
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v2, v1, 0x1

    .end local v1           #index:I
    .restart local v2       #index:I
    aput-boolean v6, v4, v1

    .line 998
    const-string v4, "samsungTabedit_eng_qwerty_checkbox"

    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v2, 0x1

    .end local v2           #index:I
    .restart local v1       #index:I
    aget-boolean v5, v5, v2

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 999
    sget-boolean v4, Lcom/samsung/inputmethod/comm/SimeFeature;->SIME_SUPPORT_MULTI_FLOAT:Z

    if-eqz v4, :cond_7

    .line 1000
    sget-object v4, Lcom/samsung/inputmethod/softkeyboard/SimeSkbManager$KeyboardType;->TYPE_NORMAL:Lcom/samsung/inputmethod/softkeyboard/SimeSkbManager$KeyboardType;

    if-ne v3, v4, :cond_6

    .line 1001
    const-string v4, "samsungTabedit_eng_full_handwrite_checkbox"

    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v2, v1, 0x1

    .end local v1           #index:I
    .restart local v2       #index:I
    aget-boolean v5, v5, v1

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move v1, v2

    .line 1017
    .end local v2           #index:I
    .restart local v1       #index:I
    :goto_2
    const-string v4, "samsungTabedit_kor_qwerty_checkbox"

    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v2, v1, 0x1

    .end local v1           #index:I
    .restart local v2       #index:I
    aget-boolean v5, v5, v1

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1018
    sget-boolean v4, Lcom/samsung/inputmethod/comm/SimeFeature;->SIME_SUPPORT_MULTI_FLOAT:Z

    if-eqz v4, :cond_a

    .line 1019
    sget-object v4, Lcom/samsung/inputmethod/softkeyboard/SimeSkbManager$KeyboardType;->TYPE_NORMAL:Lcom/samsung/inputmethod/softkeyboard/SimeSkbManager$KeyboardType;

    if-ne v3, v4, :cond_9

    .line 1020
    const-string v4, "samsungTabedit_korean_full_handwrite_checkbox"

    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v2, 0x1

    .end local v2           #index:I
    .restart local v1       #index:I
    aget-boolean v5, v5, v2

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1036
    :goto_3
    sget-object v4, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v2, v1, 0x1

    .end local v1           #index:I
    .restart local v2       #index:I
    aput-boolean v6, v4, v1

    .line 1038
    const-string v4, "samsungTabedit_123_symbol_checkbox"

    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v2, 0x1

    .end local v2           #index:I
    .restart local v1       #index:I
    aget-boolean v5, v5, v2

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1039
    const-string v4, "samsungTabedit_emoticon_checkbox"

    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v2, v1, 0x1

    .end local v1           #index:I
    .restart local v2       #index:I
    aget-boolean v5, v5, v1

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1040
    const-string v4, "samsungTabedit_clipboard_checkbox"

    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v2, 0x1

    .end local v2           #index:I
    .restart local v1       #index:I
    aget-boolean v5, v5, v2

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1041
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1042
    return-void

    .line 953
    :cond_0
    const-string v4, "samsungTabedit_hk_half_box_checkbox"

    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v2, v1, 0x1

    .end local v1           #index:I
    .restart local v2       #index:I
    aget-boolean v5, v5, v1

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 955
    const-string v4, "samsungTabedit_tw_half_box_checkbox"

    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v2, 0x1

    .end local v2           #index:I
    .restart local v1       #index:I
    aget-boolean v5, v5, v2

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    goto/16 :goto_0

    .line 960
    :cond_1
    invoke-static {}, Lcom/samsung/inputmethod/comm/SimeEnvironment;->getInstance()Lcom/samsung/inputmethod/comm/SimeEnvironment;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/inputmethod/comm/SimeEnvironment;->isLargeScreen()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 961
    const-string v4, "samsungTabedit_hk_full_handwrite_checkbox"

    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v2, v1, 0x1

    .end local v1           #index:I
    .restart local v2       #index:I
    aget-boolean v5, v5, v1

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 963
    const-string v4, "samsungTabedit_tw_full_handwrite_checkbox"

    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v2, 0x1

    .end local v2           #index:I
    .restart local v1       #index:I
    aget-boolean v5, v5, v2

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    goto/16 :goto_0

    .line 966
    :cond_2
    const-string v4, "samsungTabedit_hk_half_box_checkbox"

    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v2, v1, 0x1

    .end local v1           #index:I
    .restart local v2       #index:I
    aget-boolean v5, v5, v1

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 968
    const-string v4, "samsungTabedit_tw_half_box_checkbox"

    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v2, 0x1

    .end local v2           #index:I
    .restart local v1       #index:I
    aget-boolean v5, v5, v2

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    goto/16 :goto_0

    .line 983
    :cond_3
    const-string v4, "samsungTabedit_chn_box_handwrite_checkbox"

    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v2, v1, 0x1

    .end local v1           #index:I
    .restart local v2       #index:I
    aget-boolean v5, v5, v1

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move v1, v2

    .end local v2           #index:I
    .restart local v1       #index:I
    goto/16 :goto_1

    .line 987
    :cond_4
    invoke-static {}, Lcom/samsung/inputmethod/comm/SimeEnvironment;->getInstance()Lcom/samsung/inputmethod/comm/SimeEnvironment;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/inputmethod/comm/SimeEnvironment;->isLargeScreen()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 988
    const-string v4, "samsungTabedit_chn_full_handwrite_checkbox"

    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v2, v1, 0x1

    .end local v1           #index:I
    .restart local v2       #index:I
    aget-boolean v5, v5, v1

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move v1, v2

    .end local v2           #index:I
    .restart local v1       #index:I
    goto/16 :goto_1

    .line 991
    :cond_5
    const-string v4, "samsungTabedit_chn_box_handwrite_checkbox"

    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v2, v1, 0x1

    .end local v1           #index:I
    .restart local v2       #index:I
    aget-boolean v5, v5, v1

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move v1, v2

    .end local v2           #index:I
    .restart local v1       #index:I
    goto/16 :goto_1

    .line 1004
    :cond_6
    const-string v4, "samsungTabedit_eng_box_handwrite_checkbox"

    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v2, v1, 0x1

    .end local v1           #index:I
    .restart local v2       #index:I
    aget-boolean v5, v5, v1

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move v1, v2

    .end local v2           #index:I
    .restart local v1       #index:I
    goto/16 :goto_2

    .line 1008
    :cond_7
    invoke-static {}, Lcom/samsung/inputmethod/comm/SimeEnvironment;->getInstance()Lcom/samsung/inputmethod/comm/SimeEnvironment;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/inputmethod/comm/SimeEnvironment;->isLargeScreen()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 1009
    const-string v4, "samsungTabedit_eng_full_handwrite_checkbox"

    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v2, v1, 0x1

    .end local v1           #index:I
    .restart local v2       #index:I
    aget-boolean v5, v5, v1

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move v1, v2

    .end local v2           #index:I
    .restart local v1       #index:I
    goto/16 :goto_2

    .line 1012
    :cond_8
    const-string v4, "samsungTabedit_eng_box_handwrite_checkbox"

    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v2, v1, 0x1

    .end local v1           #index:I
    .restart local v2       #index:I
    aget-boolean v5, v5, v1

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move v1, v2

    .end local v2           #index:I
    .restart local v1       #index:I
    goto/16 :goto_2

    .line 1023
    .end local v1           #index:I
    .restart local v2       #index:I
    :cond_9
    const-string v4, "samsungTabedit_korean_box_handwrite_checkbox"

    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v2, 0x1

    .end local v2           #index:I
    .restart local v1       #index:I
    aget-boolean v5, v5, v2

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    goto/16 :goto_3

    .line 1027
    .end local v1           #index:I
    .restart local v2       #index:I
    :cond_a
    invoke-static {}, Lcom/samsung/inputmethod/comm/SimeEnvironment;->getInstance()Lcom/samsung/inputmethod/comm/SimeEnvironment;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/inputmethod/comm/SimeEnvironment;->isLargeScreen()Z

    move-result v4

    if-eqz v4, :cond_b

    .line 1028
    const-string v4, "samsungTabedit_korean_full_handwrite_checkbox"

    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v2, 0x1

    .end local v2           #index:I
    .restart local v1       #index:I
    aget-boolean v5, v5, v2

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    goto/16 :goto_3

    .line 1031
    .end local v1           #index:I
    .restart local v2       #index:I
    :cond_b
    const-string v4, "samsungTabedit_korean_box_handwrite_checkbox"

    sget-object v5, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabEditList_Checkbox:[Z

    add-int/lit8 v1, v2, 0x1

    .end local v2           #index:I
    .restart local v1       #index:I
    aget-boolean v5, v5, v2

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    goto/16 :goto_3
.end method

.method private static writeBackTabtabData()V
    .locals 3

    .prologue
    .line 921
    invoke-static {}, Lcom/samsung/inputmethod/comm/SimeEnvironment;->getInstance()Lcom/samsung/inputmethod/comm/SimeEnvironment;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/inputmethod/comm/SimeEnvironment;->isTabletModel()Z

    move-result v1

    if-nez v1, :cond_0

    .line 930
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    :goto_0
    return-void

    .line 923
    .end local v0           #editor:Landroid/content/SharedPreferences$Editor;
    :cond_0
    invoke-static {}, Lcom/samsung/inputmethod/service/xt9/SimeDecoderService;->imIsHKTWModel()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 924
    invoke-static {}, Lcom/samsung/inputmethod/setting/SimeSetting;->writeBackTabEditListTCHN()V

    .line 928
    :goto_1
    sget-object v1, Lcom/samsung/inputmethod/setting/SimeSetting;->mSharedPref:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 929
    .restart local v0       #editor:Landroid/content/SharedPreferences$Editor;
    const-string v1, "samsungTabmode_order"

    sget-object v2, Lcom/samsung/inputmethod/setting/SimeSetting;->mTabModeOrder:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    .line 926
    .end local v0           #editor:Landroid/content/SharedPreferences$Editor;
    :cond_1
    invoke-static {}, Lcom/samsung/inputmethod/setting/SimeSetting;->writeBackTabEditListSCHN()V

    goto :goto_1
.end method
