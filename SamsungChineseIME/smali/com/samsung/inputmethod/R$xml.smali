.class public final Lcom/samsung/inputmethod/R$xml;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/inputmethod/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "xml"
.end annotation


# static fields
.field public static final inputmode_list:I = 0x7f040000

.field public static final method:I = 0x7f040001

.field public static final picker_list:I = 0x7f040002

.field public static final settings:I = 0x7f040003

.field public static final settings_advanced:I = 0x7f040004

.field public static final settings_fuzzy:I = 0x7f040005

.field public static final settings_handwrite:I = 0x7f040006

.field public static final settings_language:I = 0x7f040007

.field public static final settings_personalize:I = 0x7f040008

.field public static final settings_tabtab:I = 0x7f040009

.field public static final settings_update:I = 0x7f04000a

.field public static final settings_update_setting:I = 0x7f04000b

.field public static final skb_cangjie:I = 0x7f04000c

.field public static final skb_cangjie34:I = 0x7f04000d

.field public static final skb_cangjie34_land:I = 0x7f04000e

.field public static final skb_cangjie_floating:I = 0x7f04000f

.field public static final skb_cangjie_land:I = 0x7f040010

.field public static final skb_cangjie_num:I = 0x7f040011

.field public static final skb_cangjie_num_land:I = 0x7f040012

.field public static final skb_cangjie_softkey_land:I = 0x7f040013

.field public static final skb_cm:I = 0x7f040014

.field public static final skb_cm_5row:I = 0x7f040015

.field public static final skb_cm_6row:I = 0x7f040016

.field public static final skb_emotion:I = 0x7f040017

.field public static final skb_emotion_floating:I = 0x7f040018

.field public static final skb_emotion_land:I = 0x7f040019

.field public static final skb_english_lower34:I = 0x7f04001a

.field public static final skb_english_lower34_land:I = 0x7f04001b

.field public static final skb_english_month:I = 0x7f04001c

.field public static final skb_english_month_land:I = 0x7f04001d

.field public static final skb_english_upper34:I = 0x7f04001e

.field public static final skb_english_upper34_land:I = 0x7f04001f

.field public static final skb_handwrite:I = 0x7f040020

.field public static final skb_handwrite_box:I = 0x7f040021

.field public static final skb_handwrite_box_land:I = 0x7f040022

.field public static final skb_handwrite_land:I = 0x7f040023

.field public static final skb_handwrite_num:I = 0x7f040024

.field public static final skb_handwrite_num_land:I = 0x7f040025

.field public static final skb_kor_34_toolbar:I = 0x7f040026

.field public static final skb_kor_pinyin34:I = 0x7f040027

.field public static final skb_kor_pinyin34_land:I = 0x7f040028

.field public static final skb_kor_qwerty:I = 0x7f040029

.field public static final skb_kor_qwerty_floating:I = 0x7f04002a

.field public static final skb_kor_qwerty_land:I = 0x7f04002b

.field public static final skb_kor_qwerty_num:I = 0x7f04002c

.field public static final skb_kor_qwerty_num_land:I = 0x7f04002d

.field public static final skb_kor_qwerty_softkey_land:I = 0x7f04002e

.field public static final skb_num_34:I = 0x7f04002f

.field public static final skb_num_34_land:I = 0x7f040030

.field public static final skb_numberdecimal:I = 0x7f040031

.field public static final skb_numberminus:I = 0x7f040032

.field public static final skb_numberonly:I = 0x7f040033

.field public static final skb_phone:I = 0x7f040034

.field public static final skb_phone_ctc:I = 0x7f040035

.field public static final skb_phone_hktw:I = 0x7f040036

.field public static final skb_phone_hktw_land:I = 0x7f040037

.field public static final skb_phone_land:I = 0x7f040038

.field public static final skb_phone_td:I = 0x7f040039

.field public static final skb_phone_td_floating:I = 0x7f04003a

.field public static final skb_phone_td_land:I = 0x7f04003b

.field public static final skb_pinyin34:I = 0x7f04003c

.field public static final skb_pinyin34_land:I = 0x7f04003d

.field public static final skb_qwerty:I = 0x7f04003e

.field public static final skb_qwerty_floating:I = 0x7f04003f

.field public static final skb_qwerty_hktw_num:I = 0x7f040040

.field public static final skb_qwerty_hktw_num_land:I = 0x7f040041

.field public static final skb_qwerty_land:I = 0x7f040042

.field public static final skb_qwerty_num:I = 0x7f040043

.field public static final skb_qwerty_num_land:I = 0x7f040044

.field public static final skb_qwerty_softkey_land:I = 0x7f040045

.field public static final skb_stroke34:I = 0x7f040046

.field public static final skb_stroke34_cangjie:I = 0x7f040047

.field public static final skb_stroke34_cangjie_land:I = 0x7f040048

.field public static final skb_stroke34_land:I = 0x7f040049

.field public static final skb_sym1:I = 0x7f04004a

.field public static final skb_sym1_34:I = 0x7f04004b

.field public static final skb_sym1_34_ctc:I = 0x7f04004c

.field public static final skb_sym1_34_land:I = 0x7f04004d

.field public static final skb_sym1_34_land_ctc:I = 0x7f04004e

.field public static final skb_sym1_ctc:I = 0x7f04004f

.field public static final skb_sym1_floating:I = 0x7f040050

.field public static final skb_sym1_land:I = 0x7f040051

.field public static final skb_sym1_num:I = 0x7f040052

.field public static final skb_sym1_num2:I = 0x7f040053

.field public static final skb_sym1_num2_land:I = 0x7f040054

.field public static final skb_sym1_num_land:I = 0x7f040055

.field public static final skb_sym1_tab_angle:I = 0x7f040056

.field public static final skb_sym1_tab_angle_land:I = 0x7f040057

.field public static final skb_sym1_tab_emotion:I = 0x7f040058

.field public static final skb_sym1_tab_emotion_land:I = 0x7f040059

.field public static final skb_sym1_tab_hf_angle:I = 0x7f04005a

.field public static final skb_sym1_tab_hf_angle_land:I = 0x7f04005b

.field public static final skb_sym1_tab_latest:I = 0x7f04005c

.field public static final skb_sym1_tab_latest_land:I = 0x7f04005d

.field public static final skb_sym1_unified:I = 0x7f04005e

.field public static final skb_sym1_unified_land:I = 0x7f04005f

.field public static final skb_sym2:I = 0x7f040060

.field public static final skb_sym2_34:I = 0x7f040061

.field public static final skb_sym2_34_land:I = 0x7f040062

.field public static final skb_sym2_ctc:I = 0x7f040063

.field public static final skb_sym2_floating:I = 0x7f040064

.field public static final skb_sym2_land:I = 0x7f040065

.field public static final skb_sym2_num:I = 0x7f040066

.field public static final skb_sym2_num2:I = 0x7f040067

.field public static final skb_sym2_num2_land:I = 0x7f040068

.field public static final skb_sym2_num_land:I = 0x7f040069

.field public static final skb_sym2_tab_angle:I = 0x7f04006a

.field public static final skb_sym2_tab_angle_land:I = 0x7f04006b

.field public static final skb_sym2_tab_emotion:I = 0x7f04006c

.field public static final skb_sym2_tab_emotion_land:I = 0x7f04006d

.field public static final skb_sym2_tab_hf_angle:I = 0x7f04006e

.field public static final skb_sym2_tab_hf_angle_land:I = 0x7f04006f

.field public static final skb_sym2_tab_latest:I = 0x7f040070

.field public static final skb_sym2_tab_latest_land:I = 0x7f040071

.field public static final skb_sym2_unified:I = 0x7f040072

.field public static final skb_sym2_unified_land:I = 0x7f040073

.field public static final skb_sym3:I = 0x7f040074

.field public static final skb_sym3_34:I = 0x7f040075

.field public static final skb_sym3_34_land:I = 0x7f040076

.field public static final skb_sym3_land:I = 0x7f040077

.field public static final skb_sym3_tab_angle:I = 0x7f040078

.field public static final skb_sym3_tab_angle_land:I = 0x7f040079

.field public static final skb_sym3_tab_emotion:I = 0x7f04007a

.field public static final skb_sym3_tab_emotion_land:I = 0x7f04007b

.field public static final skb_sym3_tab_hf_angle:I = 0x7f04007c

.field public static final skb_sym3_tab_hf_angle_land:I = 0x7f04007d

.field public static final skb_sym3_tab_latest:I = 0x7f04007e

.field public static final skb_sym3_tab_latest_land:I = 0x7f04007f

.field public static final skb_sym3_unified:I = 0x7f040080

.field public static final skb_sym3_unified_land:I = 0x7f040081

.field public static final skb_sym4:I = 0x7f040082

.field public static final skb_sym4_34:I = 0x7f040083

.field public static final skb_sym4_34_ctc:I = 0x7f040084

.field public static final skb_sym4_34_land:I = 0x7f040085

.field public static final skb_sym4_34_land_ctc:I = 0x7f040086

.field public static final skb_sym4_land:I = 0x7f040087

.field public static final skb_sym4_unified:I = 0x7f040088

.field public static final skb_sym4_unified_land:I = 0x7f040089

.field public static final skb_sym5_34:I = 0x7f04008a

.field public static final skb_sym5_34_ctc:I = 0x7f04008b

.field public static final skb_sym5_34_land:I = 0x7f04008c

.field public static final skb_sym5_34_land_ctc:I = 0x7f04008d

.field public static final skb_sym5_unified:I = 0x7f04008e

.field public static final skb_sym5_unified_land:I = 0x7f04008f

.field public static final skb_sym6_34:I = 0x7f040090

.field public static final skb_sym6_34_land:I = 0x7f040091

.field public static final skb_sym7_34:I = 0x7f040092

.field public static final skb_sym7_34_land:I = 0x7f040093

.field public static final skb_symbol34toolbar_1:I = 0x7f040094

.field public static final skb_symbol34toolbar_1_en:I = 0x7f040095

.field public static final skb_symbol34toolbar_2:I = 0x7f040096

.field public static final skb_symbol34toolbar_2_en:I = 0x7f040097

.field public static final skb_symbol34toolbar_3:I = 0x7f040098

.field public static final skb_symbol34toolbar_3_en:I = 0x7f040099

.field public static final skb_symbol34toolbar_4:I = 0x7f04009a

.field public static final skb_symbol34toolbar_4_en:I = 0x7f04009b

.field public static final skb_symbol34toolbar_5:I = 0x7f04009c

.field public static final skb_symbol34toolbar_5_en:I = 0x7f04009d

.field public static final skb_symbol34toolbar_6:I = 0x7f04009e

.field public static final skb_symbol34toolbar_6_en:I = 0x7f04009f

.field public static final skb_symbolphonetoolbar:I = 0x7f0400a0

.field public static final skb_template2:I = 0x7f0400a1

.field public static final skb_toolbar:I = 0x7f0400a2

.field public static final skb_year_time:I = 0x7f0400a3

.field public static final skb_year_time_floating:I = 0x7f0400a4

.field public static final skb_year_time_land:I = 0x7f0400a5

.field public static final skb_zhuyin:I = 0x7f0400a6

.field public static final skb_zhuyin34:I = 0x7f0400a7

.field public static final skb_zhuyin34_land:I = 0x7f0400a8

.field public static final skb_zhuyin_floating:I = 0x7f0400a9

.field public static final skb_zhuyin_land:I = 0x7f0400aa

.field public static final swipe_keyboard_list:I = 0x7f0400ab


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2947
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
