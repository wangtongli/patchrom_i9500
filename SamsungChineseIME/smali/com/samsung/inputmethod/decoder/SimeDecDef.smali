.class public Lcom/samsung/inputmethod/decoder/SimeDecDef;
.super Ljava/lang/Object;
.source "SimeDecDef.java"


# static fields
.field public static final DECODER_FAILED:I = 0x1

.field public static final DECODER_INPUT_INVALID:I = 0x3

.field public static final DECODER_PROCESS_DONE:I = 0x2

.field public static final DECODER_RES_EMPTY:I = 0x4

.field public static final DECODER_SUCCESS:I = 0x0

.field public static final INPUTMODE_BPMF:I = 0x0

.field public static final INPUTMODE_CANGJIE:I = 0x3

.field public static final INPUTMODE_ENG_HPD:I = 0x7

.field public static final INPUTMODE_ENG_HQD:I = 0x6

.field public static final INPUTMODE_ENG_HQR:I = 0x5

.field public static final INPUTMODE_KOR:I = 0x8

.field public static final INPUTMODE_NONE:I = -0x1

.field public static final INPUTMODE_PINYIN:I = 0x1

.field public static final INPUTMODE_QUICK_CANGJIE:I = 0x4

.field public static final INPUTMODE_STROKE:I = 0x2

.field public static final KEYBOARD_TYPE_34:I = 0x1

.field public static final KEYBOARD_TYPE_HW:I = 0x2

.field public static final KEYBOARD_TYPE_QWERTY:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
