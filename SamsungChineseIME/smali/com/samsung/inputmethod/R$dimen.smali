.class public final Lcom/samsung/inputmethod/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/inputmethod/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final balloon_function_key_height_plus:I = 0x7f0a00a8

.field public static final balloon_function_key_text_size:I = 0x7f0a00a4

.field public static final balloon_function_key_width_plus:I = 0x7f0a00a7

.field public static final balloon_normal_key_height_plus:I = 0x7f0a00a6

.field public static final balloon_normal_key_text_size:I = 0x7f0a00a3

.field public static final balloon_normal_key_width_plus:I = 0x7f0a00a5

.field public static final cand_area_height_land:I = 0x7f0a003e

.field public static final cand_area_height_portrait:I = 0x7f0a003c

.field public static final cand_area_num_height_land:I = 0x7f0a003d

.field public static final cand_arrow_up_down_width:I = 0x7f0a00b4

.field public static final cand_column_width:I = 0x7f0a00c7

.field public static final cand_expand_button_padding_left:I = 0x7f0a0027

.field public static final cand_expand_button_padding_top:I = 0x7f0a0026

.field public static final cand_expand_button_width:I = 0x7f0a0025

.field public static final cand_list_scroll_width:I = 0x7f0a00c8

.field public static final cand_margin_left_right:I = 0x7f0a0024

.field public static final cand_separator_padding:I = 0x7f0a00b5

.field public static final cand_seperator_width:I = 0x7f0a003f

.field public static final cand_text_size:I = 0x7f0a001b

.field public static final cand_text_size_vertical:I = 0x7f0a001f

.field public static final cand_text_size_vertical_small:I = 0x7f0a0020

.field public static final candview_background_padding:I = 0x7f0a0000

.field public static final composing_height:I = 0x7f0a0028

.field public static final composing_spell_height:I = 0x7f0a00ba

.field public static final composing_view_padding_bottom:I = 0x7f0a00bc

.field public static final composing_view_padding_left:I = 0x7f0a00bd

.field public static final composing_view_padding_right:I = 0x7f0a00be

.field public static final composing_view_padding_top:I = 0x7f0a00bb

.field public static final double_cell_width_land:I = 0x7f0a002d

.field public static final double_cell_width_portrait:I = 0x7f0a002a

.field public static final expand_cand_handwriting_mode_height:I = 0x7f0a00ca

.field public static final expand_cand_height:I = 0x7f0a00c9

.field public static final expand_cand_row_height:I = 0x7f0a00b9

.field public static final floating_cand_area_height_land:I = 0x7f0a005b

.field public static final floating_cand_area_height_portrait:I = 0x7f0a005a

.field public static final floating_cand_text_size:I = 0x7f0a00a2

.field public static final floating_double_cell_width_land:I = 0x7f0a0061

.field public static final floating_double_cell_width_portrait:I = 0x7f0a005e

.field public static final floating_expand_cand_height:I = 0x7f0a00cb

.field public static final floating_move_handler_height:I = 0x7f0a0059

.field public static final floating_single_cell_width_land:I = 0x7f0a0060

.field public static final floating_single_cell_width_portrait:I = 0x7f0a005d

.field public static final floating_skb_composing_window_padding:I = 0x7f0a0058

.field public static final floating_skb_height_land:I = 0x7f0a004f

.field public static final floating_skb_height_portrait:I = 0x7f0a004e

.field public static final floating_skb_key_height_land:I = 0x7f0a00ab

.field public static final floating_skb_key_height_portrait:I = 0x7f0a00aa

.field public static final floating_skb_num_height_land:I = 0x7f0a0051

.field public static final floating_skb_num_height_portrait:I = 0x7f0a0050

.field public static final floating_skb_padding_bottom:I = 0x7f0a0055

.field public static final floating_skb_padding_left:I = 0x7f0a0056

.field public static final floating_skb_padding_right:I = 0x7f0a0057

.field public static final floating_skb_padding_top:I = 0x7f0a0054

.field public static final floating_skb_width:I = 0x7f0a00ac

.field public static final floating_skb_width_land:I = 0x7f0a0053

.field public static final floating_skb_width_portrait:I = 0x7f0a0052

.field public static final floating_tab_area_height_land:I = 0x7f0a006f

.field public static final floating_tab_area_height_portrait:I = 0x7f0a006e

.field public static final floating_tab_indicator_width:I = 0x7f0a0070

.field public static final floating_whole_line_width_land:I = 0x7f0a0062

.field public static final floating_whole_line_width_portrait:I = 0x7f0a005f

.field public static final footnote_text_size:I = 0x7f0a001c

.field public static final handwrit_box_land_margin_bottom:I = 0x7f0a009e

.field public static final handwrit_box_land_margin_left:I = 0x7f0a009f

.field public static final handwrit_box_land_margin_right:I = 0x7f0a00a0

.field public static final handwrit_box_land_margin_top:I = 0x7f0a009d

.field public static final handwrit_box_land_padding_bottom:I = 0x7f0a00c2

.field public static final handwrit_box_land_padding_right:I = 0x7f0a00c1

.field public static final handwrit_box_marge_left:I = 0x7f0a00c6

.field public static final handwrit_box_margin_bottom:I = 0x7f0a009a

.field public static final handwrit_box_margin_left:I = 0x7f0a009b

.field public static final handwrit_box_margin_right:I = 0x7f0a009c

.field public static final handwrit_box_margin_top:I = 0x7f0a0099

.field public static final handwrit_box_padding_bottom:I = 0x7f0a00c0

.field public static final handwrit_box_padding_right:I = 0x7f0a00bf

.field public static final handwrite_dialog_text_width:I = 0x7f0a004a

.field public static final handwrite_dialog_text_width_land:I = 0x7f0a00a1

.field public static final keyboard_set_popup_padding_left:I = 0x7f0a0091

.field public static final keyboard_set_popup_padding_right:I = 0x7f0a0092

.field public static final keyboard_set_popup_table_row_margin_bottom:I = 0x7f0a008e

.field public static final keyboard_set_popup_table_row_margin_left:I = 0x7f0a008f

.field public static final keyboard_set_popup_table_row_margin_right:I = 0x7f0a0090

.field public static final keyboard_set_popup_table_row_margin_top:I = 0x7f0a008d

.field public static final keyboard_type_btn_height:I = 0x7f0a0089

.field public static final keyboard_type_btn_height_small:I = 0x7f0a008a

.field public static final keyboard_type_btn_margin_left:I = 0x7f0a0093

.field public static final keyboard_type_btn_margin_right:I = 0x7f0a0094

.field public static final keyboard_type_btn_text_size:I = 0x7f0a008b

.field public static final keyboard_type_btn_text_size_small:I = 0x7f0a008c

.field public static final label_fun_size:I = 0x7f0a0006

.field public static final label_fun_size_big:I = 0x7f0a000b

.field public static final label_fun_size_double_line:I = 0x7f0a000c

.field public static final label_fun_size_small:I = 0x7f0a000a

.field public static final label_keypad_normal:I = 0x7f0a0010

.field public static final label_keypad_normal_hktw:I = 0x7f0a00b1

.field public static final label_normal_double_line_size:I = 0x7f0a0040

.field public static final label_size:I = 0x7f0a0001

.field public static final label_size_34_email:I = 0x7f0a0018

.field public static final label_size_34_function:I = 0x7f0a0019

.field public static final label_size_big:I = 0x7f0a0005

.field public static final label_size_capital:I = 0x7f0a0003

.field public static final label_size_extra:I = 0x7f0a000d

.field public static final label_size_extra34:I = 0x7f0a000e

.field public static final label_size_extra_tw:I = 0x7f0a00b0

.field public static final label_size_handwrite_email:I = 0x7f0a0015

.field public static final label_size_handwrite_function:I = 0x7f0a0016

.field public static final label_size_hktw:I = 0x7f0a00af

.field public static final label_size_numberic:I = 0x7f0a00b2

.field public static final label_size_numberic_extra:I = 0x7f0a00b3

.field public static final label_size_numeric:I = 0x7f0a0011

.field public static final label_size_numeric_extra:I = 0x7f0a0014

.field public static final label_size_numeric_function:I = 0x7f0a0012

.field public static final label_size_numeric_land:I = 0x7f0a0013

.field public static final label_size_qwerty_email:I = 0x7f0a0017

.field public static final label_size_small:I = 0x7f0a0004

.field public static final label_size_symbol:I = 0x7f0a0007

.field public static final label_size_twoline_lang:I = 0x7f0a000f

.field public static final label_size_yeartime:I = 0x7f0a0008

.field public static final label_size_yeartime_land:I = 0x7f0a0009

.field public static final label_size_zhuyin:I = 0x7f0a0002

.field public static final label_symbol_34:I = 0x7f0a001a

.field public static final line_style_lpda:I = 0x7f0a0046

.field public static final line_style_rpda:I = 0x7f0a0047

.field public static final line_style_text_size:I = 0x7f0a0048

.field public static final linestylepicker_rpad:I = 0x7f0a0049

.field public static final max_move_count:I = 0x7f0a004b

.field public static final min_cand_height:I = 0x7f0a0023

.field public static final min_cand_width:I = 0x7f0a001e

.field public static final mini_pop_item_size:I = 0x7f0a001d

.field public static final mini_popup_cancel_height:I = 0x7f0a00b6

.field public static final mini_popup_cancel_width:I = 0x7f0a00b7

.field public static final mini_popup_text_size:I = 0x7f0a00b8

.field public static final onehand_button_margin_left:I = 0x7f0a0042

.field public static final onehand_button_margin_right:I = 0x7f0a0041

.field public static final onehand_container_key_height_portrait:I = 0x7f0a002f

.field public static final onehand_double_cell_width_portrait:I = 0x7f0a0044

.field public static final onehand_single_cell_width_portrait:I = 0x7f0a0043

.field public static final onehand_skbcontainer_padding_bottom:I = 0x7f0a0031

.field public static final onehand_skbcontainer_padding_left:I = 0x7f0a0032

.field public static final onehand_skbcontainer_padding_right:I = 0x7f0a0033

.field public static final onehand_skbcontainer_padding_top:I = 0x7f0a0030

.field public static final onehand_whole_line_width_portrait:I = 0x7f0a0045

.field public static final popup_softkey_height:I = 0x7f0a004d

.field public static final popup_softkey_width:I = 0x7f0a004c

.field public static final qwerty_key_horizontal_gap:I = 0x7f0a0064

.field public static final qwerty_key_vertical_gap:I = 0x7f0a0065

.field public static final single_cell_width_land:I = 0x7f0a002c

.field public static final single_cell_width_portrait:I = 0x7f0a0029

.field public static final skb_balloon_distance_button:I = 0x7f0a0063

.field public static final skb_balloon_height:I = 0x7f0a00c3

.field public static final skb_balloon_normal_text_size:I = 0x7f0a00c5

.field public static final skb_balloon_padding_width:I = 0x7f0a00c4

.field public static final skb_handwritekey_height_land:I = 0x7f0a0037

.field public static final skb_handwritekey_height_num_land:I = 0x7f0a003b

.field public static final skb_handwritekey_height_num_portrait:I = 0x7f0a003a

.field public static final skb_handwritekey_height_portrait:I = 0x7f0a0036

.field public static final skb_height_landscape:I = 0x7f0a00a9

.field public static final skb_height_portrait:I = 0x7f0a005c

.field public static final skb_key_height_land:I = 0x7f0a0035

.field public static final skb_key_height_num_land:I = 0x7f0a0039

.field public static final skb_key_height_num_portrait:I = 0x7f0a0038

.field public static final skb_key_height_portrait:I = 0x7f0a0034

.field public static final skb_padding_bottom:I = 0x7f0a0096

.field public static final skb_padding_left:I = 0x7f0a0097

.field public static final skb_padding_right:I = 0x7f0a0098

.field public static final skb_padding_top:I = 0x7f0a0095

.field public static final symb_text_size_vertical:I = 0x7f0a0021

.field public static final symb_text_size_vertical_small:I = 0x7f0a0022

.field public static final tab_area_height_land:I = 0x7f0a0072

.field public static final tab_area_height_portrait:I = 0x7f0a0071

.field public static final tab_arrow_width:I = 0x7f0a006b

.field public static final tab_editmore_width:I = 0x7f0a0069

.field public static final tab_indicator_width:I = 0x7f0a006c

.field public static final tab_margin_width:I = 0x7f0a006d

.field public static final tab_page_width:I = 0x7f0a006a

.field public static final tablayout_padding_top:I = 0x7f0a0067

.field public static final tabtitles_max_height:I = 0x7f0a0068

.field public static final tabtitles_text_size:I = 0x7f0a0066

.field public static final toolbar_image_width:I = 0x7f0a007a

.field public static final toolbar_page_landscape_width:I = 0x7f0a0075

.field public static final toolbar_page_width:I = 0x7f0a0074

.field public static final toolbar_page_width_floating:I = 0x7f0a0073

.field public static final toolbar_separator_width:I = 0x7f0a0079

.field public static final toolbar_symbols_landscape_width:I = 0x7f0a0078

.field public static final toolbar_symbols_width:I = 0x7f0a0077

.field public static final toolbar_symbols_width_floating:I = 0x7f0a0076

.field public static final toolbar_titles_text_size:I = 0x7f0a007b

.field public static final toolbar_titles_text_size_floating:I = 0x7f0a007c

.field public static final vertical_symbol_margin_left:I = 0x7f0a0080

.field public static final vertical_symbol_margin_left_floating:I = 0x7f0a0088

.field public static final vertical_symbol_margin_left_floating_land:I = 0x7f0a00ae

.field public static final vertical_symbol_margin_left_land:I = 0x7f0a0084

.field public static final vertical_symbol_margin_top:I = 0x7f0a007f

.field public static final vertical_symbol_margin_top_floating:I = 0x7f0a0087

.field public static final vertical_symbol_margin_top_floating_land:I = 0x7f0a00ad

.field public static final vertical_symbol_margin_top_land:I = 0x7f0a0083

.field public static final vertical_symbol_popup_height:I = 0x7f0a007d

.field public static final vertical_symbol_popup_height_floating:I = 0x7f0a0085

.field public static final vertical_symbol_popup_height_land:I = 0x7f0a0081

.field public static final vertical_symbol_width:I = 0x7f0a007e

.field public static final vertical_symbol_width_floating:I = 0x7f0a0086

.field public static final vertical_symbol_width_land:I = 0x7f0a0082

.field public static final whole_line_width_land:I = 0x7f0a002e

.field public static final whole_line_width_portrait:I = 0x7f0a002b


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 235
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
