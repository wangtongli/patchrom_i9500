.class public Lcom/samsung/inputmethod/SimeInputModeDef;
.super Ljava/lang/Object;
.source "SimeInputModeDef.java"


# static fields
.field public static final MASK_CASE:I = 0xf00000

.field public static final MASK_CASE_ANGLE:I = 0x600000

.field public static final MASK_CASE_EMOTION:I = 0x800000

.field public static final MASK_CASE_HFANGLE:I = 0x700000

.field public static final MASK_CASE_LATEST:I = 0x500000

.field public static final MASK_CASE_LOWER:I = 0x100000

.field public static final MASK_CASE_TEMPLOWER:I = 0x300000

.field public static final MASK_CASE_UPPER:I = 0x200000

.field public static final MASK_IM_NUMONLY_DECIMAL:I = 0x10

.field public static final MASK_IM_NUMONLY_MINUS:I = 0x20

.field public static final MASK_IM_NUMONLY_TYPE:I = 0xf0

.field public static final MASK_IM_TYPE:I = -0x10000000

.field public static final MASK_IM_TYPE_CANG:I = -0x70000000

.field public static final MASK_IM_TYPE_CLIPBOARD:I = -0x40000000

.field public static final MASK_IM_TYPE_DATETIME:I = -0x80000000

.field public static final MASK_IM_TYPE_NUM:I = 0x70000000

.field public static final MASK_IM_TYPE_NUMONLY:I = -0x50000000

.field public static final MASK_IM_TYPE_PHONE:I = 0x50000000

.field public static final MASK_IM_TYPE_QUICK_CANG:I = -0x60000000

.field public static final MASK_IM_TYPE_SPELLING:I = 0x10000000

.field public static final MASK_IM_TYPE_STROKE:I = 0x30000000

.field public static final MASK_IM_TYPE_SYMBOL:I = 0x60000000

.field public static final MASK_IM_TYPE_ZHUYIN:I = 0x20000000

.field public static final MASK_LANGUAGE:I = 0xf0000

.field public static final MASK_LANGUAGE_EN:I = 0x10000

.field public static final MASK_LANGUAGE_KO:I = 0x30000

.field public static final MASK_LANGUAGE_ZH_CN:I = 0x20000

.field public static final MASK_LANGUAGE_ZH_HK:I = 0x50000

.field public static final MASK_LANGUAGE_ZH_TW:I = 0x40000

.field public static final MASK_RECOGNIZE_TYPE:I = 0xf00

.field public static final MASK_RECOGNIZE_TYPE_ALL:I = 0x100

.field public static final MASK_RECOGNIZE_TYPE_ENGLISH:I = 0x600

.field public static final MASK_RECOGNIZE_TYPE_NUMBER:I = 0x400

.field public static final MASK_RECOGNIZE_TYPE_PINYIN:I = 0x200

.field public static final MASK_RECOGNIZE_TYPE_STROKE:I = 0x300

.field public static final MASK_RECOGNIZE_TYPE_SYMBOL:I = 0x500

.field public static final MASK_SKB_LAYOUT:I = 0xf000000

.field public static final MASK_SKB_LAYOUT_34:I = 0x2000000

.field public static final MASK_SKB_LAYOUT_ALL:I = 0x5000000

.field public static final MASK_SKB_LAYOUT_BOX:I = 0x4000000

.field public static final MASK_SKB_LAYOUT_FS:I = 0x3000000

.field public static final MASK_SKB_LAYOUT_QWERTY:I = 0x1000000

.field public static final MASK_SKB_TOOLBAR_SHOWSKB:I = 0x400000

.field public static final MASK_SKB_TYPE:I = 0xf

.field public static final MASK_SKB_TYPE_TOOLBAR:I = 0x1

.field public static final MASK_SYMBOL_EMOTION:I = 0x8000

.field public static final MASK_SYMBOL_NUM:I = 0xf000

.field public static final MASK_SYMBOL_NUM_1:I = 0x1000

.field public static final MASK_SYMBOL_NUM_2:I = 0x2000

.field public static final MASK_SYMBOL_NUM_3:I = 0x3000

.field public static final MASK_SYMBOL_NUM_4:I = 0x4000

.field public static final MASK_SYMBOL_NUM_5:I = 0x5000

.field public static final MASK_SYMBOL_NUM_6:I = 0x6000

.field public static final MASK_SYMBOL_NUM_7:I = 0x7000

.field public static final MODE_HKB_CHINESE:I = 0x20000

.field public static final MODE_HKB_ENGLISH:I = 0x10000

.field public static final MODE_SKB_CANGJIE_34:I = -0x6dfb0000

.field public static final MODE_SKB_CANGJIE_34_TOOLBAR:I = -0x6dfaffff

.field public static final MODE_SKB_CANGJIE_QW:I = -0x6efb0000

.field public static final MODE_SKB_CLIPBOARD:I = -0x3efe0000

.field public static final MODE_SKB_ENGLISH_34_LOWER:I = 0x12110000

.field public static final MODE_SKB_ENGLISH_34_LOWER_TOOLBAR:I = 0x12110001

.field public static final MODE_SKB_ENGLISH_34_TEMPUPPER:I = 0x12310000

.field public static final MODE_SKB_ENGLISH_34_TEMPUPPER_TOOLBAR:I = 0x12310001

.field public static final MODE_SKB_ENGLISH_34_UPPER:I = 0x12210000

.field public static final MODE_SKB_ENGLISH_34_UPPER_TOOLBAR:I = 0x12210001

.field public static final MODE_SKB_ENGLISH_QW_LOWER:I = 0x11110000

.field public static final MODE_SKB_ENGLISH_QW_TEMPUPPER:I = 0x11310000

.field public static final MODE_SKB_ENGLISH_QW_UPPER:I = 0x11210000

.field public static final MODE_SKB_HANDWRITE_BOX_CHN:I = 0x34020100

.field public static final MODE_SKB_HANDWRITE_BOX_CHN_123:I = 0x34020400

.field public static final MODE_SKB_HANDWRITE_BOX_CHN_HK:I = 0x34050100

.field public static final MODE_SKB_HANDWRITE_BOX_CHN_HK_123:I = 0x34050400

.field public static final MODE_SKB_HANDWRITE_BOX_CHN_TW:I = 0x34040100

.field public static final MODE_SKB_HANDWRITE_BOX_CHN_TW_123:I = 0x34040400

.field public static final MODE_SKB_HANDWRITE_BOX_ENG:I = 0x34010100

.field public static final MODE_SKB_HANDWRITE_BOX_ENG_123:I = 0x34010400

.field public static final MODE_SKB_HANDWRITE_BOX_KOR:I = 0x34030100

.field public static final MODE_SKB_HANDWRITE_BOX_KOR_123:I = 0x34030400

.field public static final MODE_SKB_HANDWRITE_FS_CHN:I = 0x33020100

.field public static final MODE_SKB_HANDWRITE_FS_CHN_123:I = 0x33020400

.field public static final MODE_SKB_HANDWRITE_FS_CHN_HK:I = 0x33050100

.field public static final MODE_SKB_HANDWRITE_FS_CHN_HK_123:I = 0x33050400

.field public static final MODE_SKB_HANDWRITE_FS_CHN_TW:I = 0x33040100

.field public static final MODE_SKB_HANDWRITE_FS_CHN_TW_123:I = 0x33040400

.field public static final MODE_SKB_HANDWRITE_FS_ENG:I = 0x33010100

.field public static final MODE_SKB_HANDWRITE_FS_ENG_123:I = 0x33010400

.field public static final MODE_SKB_HANDWRITE_FS_KOR:I = 0x33030100

.field public static final MODE_SKB_HANDWRITE_FS_KOR_123:I = 0x33030400

.field public static final MODE_SKB_KOR_34:I = 0x12030000

.field public static final MODE_SKB_KOR_34_TOOLBAR:I = 0x12030001

.field public static final MODE_SKB_KOR_34_TOOLBAR_SHOWSKB:I = 0x12430001

.field public static final MODE_SKB_KOR_QW_LOWER:I = 0x11130000

.field public static final MODE_SKB_KOR_QW_TEMPUPPER:I = 0x11330000

.field public static final MODE_SKB_KOR_QW_UPPER:I = 0x11230000

.field public static final MODE_SKB_MONTH:I = -0x7efff000

.field public static final MODE_SKB_MONTH_TOOLBAR:I = -0x7dffefff

.field public static final MODE_SKB_NUM_34:I = 0x72000000

.field public static final MODE_SKB_NUM_34_TOOLBAR:I = 0x72000001

.field public static final MODE_SKB_PHONE_NUM:I = 0x52000000

.field public static final MODE_SKB_PHONE_NUMONLY_BOTH:I = -0x4dffffd0

.field public static final MODE_SKB_PHONE_NUMONLY_DECIMAL:I = -0x4dfffff0

.field public static final MODE_SKB_PHONE_NUMONLY_SIGNED:I = -0x4dffffe0

.field public static final MODE_SKB_PHONE_NUM_TOOLBAR:I = 0x52000001

.field public static final MODE_SKB_PHONE_SYM:I = 0x52200000

.field public static final MODE_SKB_PHONE_SYM_TOOLBAR:I = 0x52200001

.field public static final MODE_SKB_PINYIN_34:I = 0x12020000

.field public static final MODE_SKB_PINYIN_34_TOOLBAR:I = 0x12020001

.field public static final MODE_SKB_PINYIN_QW:I = 0x11020000

.field public static final MODE_SKB_QUICK_CANG_QW:I = -0x5efb0000

.field public static final MODE_SKB_STROKE_34:I = 0x32020000

.field public static final MODE_SKB_STROKE_34_HK:I = 0x32050000

.field public static final MODE_SKB_STROKE_34_HK_TOOLBAR:I = 0x32050001

.field public static final MODE_SKB_STROKE_34_TOOLBAR:I = 0x32020001

.field public static final MODE_SKB_STROKE_34_TW:I = 0x32040000

.field public static final MODE_SKB_STROKE_34_TW_TOOLBAR:I = 0x32040001

.field public static final MODE_SKB_SYMBOL_ANGLE_1:I = 0x65601000

.field public static final MODE_SKB_SYMBOL_ANGLE_2:I = 0x65602000

.field public static final MODE_SKB_SYMBOL_ANGLE_3:I = 0x65603000

.field public static final MODE_SKB_SYMBOL_CN_34_1:I = 0x62021000

.field public static final MODE_SKB_SYMBOL_CN_34_1_TOOLBAR:I = 0x62021001

.field public static final MODE_SKB_SYMBOL_CN_34_2:I = 0x62022000

.field public static final MODE_SKB_SYMBOL_CN_34_2_TOOLBAR:I = 0x62022001

.field public static final MODE_SKB_SYMBOL_CN_34_3:I = 0x62023000

.field public static final MODE_SKB_SYMBOL_CN_34_3_TOOLBAR:I = 0x62023001

.field public static final MODE_SKB_SYMBOL_CN_34_4:I = 0x62024000

.field public static final MODE_SKB_SYMBOL_CN_34_4_TOOLBAR:I = 0x62024001

.field public static final MODE_SKB_SYMBOL_CN_34_5:I = 0x62025000

.field public static final MODE_SKB_SYMBOL_CN_34_5_TOOLBAR:I = 0x62025001

.field public static final MODE_SKB_SYMBOL_CN_34_6:I = 0x62026000

.field public static final MODE_SKB_SYMBOL_CN_34_6_TOOLBAR:I = 0x62026001

.field public static final MODE_SKB_SYMBOL_CN_QW_1:I = 0x61021000

.field public static final MODE_SKB_SYMBOL_CN_QW_2:I = 0x61022000

.field public static final MODE_SKB_SYMBOL_CN_QW_3:I = 0x61023000

.field public static final MODE_SKB_SYMBOL_CN_QW_4:I = 0x61024000

.field public static final MODE_SKB_SYMBOL_CN_QW_5:I = 0x61025000

.field public static final MODE_SKB_SYMBOL_EMOTION:I = 0x61008000

.field public static final MODE_SKB_SYMBOL_EMOTION_1:I = 0x65801000

.field public static final MODE_SKB_SYMBOL_EMOTION_2:I = 0x65802000

.field public static final MODE_SKB_SYMBOL_EMOTION_3:I = 0x65803000

.field public static final MODE_SKB_SYMBOL_EN_34_1:I = 0x62011000

.field public static final MODE_SKB_SYMBOL_EN_34_1_TOOLBAR:I = 0x62011001

.field public static final MODE_SKB_SYMBOL_EN_34_2:I = 0x62012000

.field public static final MODE_SKB_SYMBOL_EN_34_2_TOOLBAR:I = 0x62012001

.field public static final MODE_SKB_SYMBOL_EN_34_3:I = 0x62013000

.field public static final MODE_SKB_SYMBOL_EN_34_3_TOOLBAR:I = 0x62013001

.field public static final MODE_SKB_SYMBOL_EN_34_4:I = 0x62014000

.field public static final MODE_SKB_SYMBOL_EN_34_4_TOOLBAR:I = 0x62014001

.field public static final MODE_SKB_SYMBOL_EN_34_5:I = 0x62015000

.field public static final MODE_SKB_SYMBOL_EN_34_5_TOOLBAR:I = 0x62015001

.field public static final MODE_SKB_SYMBOL_EN_34_6:I = 0x62016000

.field public static final MODE_SKB_SYMBOL_EN_34_6_TOOLBAR:I = 0x62016001

.field public static final MODE_SKB_SYMBOL_EN_QW_1:I = 0x61011000

.field public static final MODE_SKB_SYMBOL_EN_QW_2:I = 0x61012000

.field public static final MODE_SKB_SYMBOL_EN_QW_3:I = 0x61013000

.field public static final MODE_SKB_SYMBOL_EN_QW_4:I = 0x61014000

.field public static final MODE_SKB_SYMBOL_EN_QW_5:I = 0x61015000

.field public static final MODE_SKB_SYMBOL_HFANGLE_1:I = 0x65701000

.field public static final MODE_SKB_SYMBOL_HFANGLE_2:I = 0x65702000

.field public static final MODE_SKB_SYMBOL_HFANGLE_3:I = 0x65703000

.field public static final MODE_SKB_SYMBOL_LATEST_1:I = 0x65501000

.field public static final MODE_SKB_SYMBOL_LATEST_2:I = 0x65502000

.field public static final MODE_SKB_SYMBOL_LATEST_3:I = 0x65503000

.field public static final MODE_SKB_YEAR_TIME:I = -0x7effe000

.field public static final MODE_SKB_YEAR_TIME_TOOLBAR:I = -0x7dffdfff

.field public static final MODE_SKB_ZHUYIN_34:I = 0x22040000

.field public static final MODE_SKB_ZHUYIN_34_TOOLBAR:I = 0x22040001

.field public static final MODE_SKB_ZHUYIN_QW:I = 0x21040000

.field public static final MODE_UNSET:I

.field static TAG:Ljava/lang/String;


# instance fields
.field DEBUG:Z

.field public value:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8
    const-string v0, "SimeInputModeDef"

    sput-object v0, Lcom/samsung/inputmethod/SimeInputModeDef;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 278
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    iput-boolean v0, p0, Lcom/samsung/inputmethod/SimeInputModeDef;->DEBUG:Z

    .line 276
    iput v0, p0, Lcom/samsung/inputmethod/SimeInputModeDef;->value:I

    .line 279
    iput v0, p0, Lcom/samsung/inputmethod/SimeInputModeDef;->value:I

    .line 280
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .parameter "modeVal"

    .prologue
    const/4 v0, 0x0

    .line 281
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    iput-boolean v0, p0, Lcom/samsung/inputmethod/SimeInputModeDef;->DEBUG:Z

    .line 276
    iput v0, p0, Lcom/samsung/inputmethod/SimeInputModeDef;->value:I

    .line 282
    iput p1, p0, Lcom/samsung/inputmethod/SimeInputModeDef;->value:I

    .line 283
    return-void
.end method

.method public static getHW123ModeState(I)Z
    .locals 3
    .parameter "mInputMode"

    .prologue
    .line 288
    const/4 v0, 0x0

    .line 289
    .local v0, bRet:Z
    and-int/lit16 v1, p0, 0xf00

    const/16 v2, 0x400

    if-ne v1, v2, :cond_0

    .line 291
    const/4 v0, 0x1

    .line 293
    :cond_0
    return v0
.end method

.method public static isDateTime(I)Z
    .locals 3
    .parameter "inputMode"

    .prologue
    .line 382
    const/4 v0, 0x0

    .line 383
    .local v0, bRet:Z
    const/high16 v1, -0x1000

    and-int/2addr v1, p0

    const/high16 v2, -0x8000

    if-ne v1, v2, :cond_0

    const/high16 v1, 0x6000

    and-int/2addr v1, p0

    const/16 v2, 0x2000

    if-ne v1, v2, :cond_0

    .line 385
    const/4 v0, 0x1

    .line 386
    :cond_0
    return v0
.end method

.method public static isLayoutMask(II)Z
    .locals 2
    .parameter "inputMode"
    .parameter "layoutId"

    .prologue
    .line 318
    const/4 v0, 0x0

    .line 319
    .local v0, bRet:Z
    const/high16 v1, 0xf00

    and-int/2addr v1, p0

    if-ne v1, p1, :cond_0

    .line 320
    const/4 v0, 0x1

    .line 321
    :cond_0
    return v0
.end method

.method public static isMonth(I)Z
    .locals 3
    .parameter "inputMode"

    .prologue
    .line 365
    const/4 v0, 0x0

    .line 366
    .local v0, bRet:Z
    const/high16 v1, -0x1000

    and-int/2addr v1, p0

    const/high16 v2, -0x8000

    if-ne v1, v2, :cond_0

    const/high16 v1, 0x6000

    and-int/2addr v1, p0

    const/16 v2, 0x1000

    if-ne v1, v2, :cond_0

    .line 368
    const/4 v0, 0x1

    .line 369
    :cond_0
    return v0
.end method

.method public static isPhone(I)Z
    .locals 3
    .parameter "inputMode"

    .prologue
    .line 398
    const/4 v0, 0x0

    .line 399
    .local v0, bRet:Z
    const/high16 v1, -0x1000

    and-int/2addr v1, p0

    const/high16 v2, 0x5000

    if-ne v1, v2, :cond_0

    .line 400
    const/4 v0, 0x1

    .line 401
    :cond_0
    return v0
.end method

.method public static isToolBARSHOWSkb(I)Z
    .locals 1
    .parameter "inputMode"

    .prologue
    .line 308
    const v0, 0x12430001

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isToolBARSkb(I)Z
    .locals 2
    .parameter "inputMode"

    .prologue
    const/4 v0, 0x1

    .line 299
    and-int/lit8 v1, p0, 0xf

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isToolbarSymbol(I)Z
    .locals 2
    .parameter "inputMode"

    .prologue
    .line 303
    invoke-static {p0}, Lcom/samsung/inputmethod/SimeInputModeDef;->isToolBARSkb(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/high16 v0, -0x1000

    and-int/2addr v0, p0

    const/high16 v1, 0x6000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static printInputMode(I)V
    .locals 2
    .parameter "inputmode"

    .prologue
    .line 425
    sparse-switch p0, :sswitch_data_0

    .line 466
    sget-object v0, Lcom/samsung/inputmethod/SimeInputModeDef;->TAG:Ljava/lang/String;

    const-string v1, "----Others---"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 469
    :goto_0
    return-void

    .line 427
    :sswitch_0
    sget-object v0, Lcom/samsung/inputmethod/SimeInputModeDef;->TAG:Ljava/lang/String;

    const-string v1, "----MODE_SKB_PINYIN_34_TOOLBAR---"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 430
    :sswitch_1
    sget-object v0, Lcom/samsung/inputmethod/SimeInputModeDef;->TAG:Ljava/lang/String;

    const-string v1, "----MODE_SKB_STROKE_34_TOOLBAR---"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 433
    :sswitch_2
    sget-object v0, Lcom/samsung/inputmethod/SimeInputModeDef;->TAG:Ljava/lang/String;

    const-string v1, "----MODE_SKB_KOR_34_TOOLBAR---"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 436
    :sswitch_3
    sget-object v0, Lcom/samsung/inputmethod/SimeInputModeDef;->TAG:Ljava/lang/String;

    const-string v1, "----MODE_SKB_ENGLISH_34_LOWER_TOOLBAR---"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 439
    :sswitch_4
    sget-object v0, Lcom/samsung/inputmethod/SimeInputModeDef;->TAG:Ljava/lang/String;

    const-string v1, "----MODE_SKB_ENGLISH_34_UPPER_TOOLBAR---"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 442
    :sswitch_5
    sget-object v0, Lcom/samsung/inputmethod/SimeInputModeDef;->TAG:Ljava/lang/String;

    const-string v1, "----MODE_SKB_ENGLISH_34_TEMPUPPER_TOOLBAR---"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 445
    :sswitch_6
    sget-object v0, Lcom/samsung/inputmethod/SimeInputModeDef;->TAG:Ljava/lang/String;

    const-string v1, "----MODE_SKB_SYMBOL_EN_34_1_TOOLBAR---"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 448
    :sswitch_7
    sget-object v0, Lcom/samsung/inputmethod/SimeInputModeDef;->TAG:Ljava/lang/String;

    const-string v1, "----MODE_SKB_SYMBOL_EN_34_2_TOOLBAR---"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 451
    :sswitch_8
    sget-object v0, Lcom/samsung/inputmethod/SimeInputModeDef;->TAG:Ljava/lang/String;

    const-string v1, "----MODE_SKB_SYMBOL_EN_34_3_TOOLBAR---"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 454
    :sswitch_9
    sget-object v0, Lcom/samsung/inputmethod/SimeInputModeDef;->TAG:Ljava/lang/String;

    const-string v1, "----MODE_SKB_SYMBOL_EN_34_4_TOOLBAR---"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 457
    :sswitch_a
    sget-object v0, Lcom/samsung/inputmethod/SimeInputModeDef;->TAG:Ljava/lang/String;

    const-string v1, "----MODE_SKB_SYMBOL_EN_34_5_TOOLBAR---"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 460
    :sswitch_b
    sget-object v0, Lcom/samsung/inputmethod/SimeInputModeDef;->TAG:Ljava/lang/String;

    const-string v1, "----MODE_SKB_SYMBOL_EN_34_6_TOOLBAR---"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 463
    :sswitch_c
    sget-object v0, Lcom/samsung/inputmethod/SimeInputModeDef;->TAG:Ljava/lang/String;

    const-string v1, "----MODE_SKB_MONTH_TOOLBAR---"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 425
    nop

    :sswitch_data_0
    .sparse-switch
        -0x7dffefff -> :sswitch_c
        0x12020001 -> :sswitch_0
        0x12030001 -> :sswitch_2
        0x12110001 -> :sswitch_3
        0x12210001 -> :sswitch_4
        0x12310001 -> :sswitch_5
        0x32020001 -> :sswitch_1
        0x62011001 -> :sswitch_6
        0x62012001 -> :sswitch_7
        0x62013001 -> :sswitch_8
        0x62014001 -> :sswitch_9
        0x62015001 -> :sswitch_a
        0x62016001 -> :sswitch_b
    .end sparse-switch
.end method


# virtual methods
.method public caseMask()I
    .locals 2

    .prologue
    .line 337
    iget v0, p0, Lcom/samsung/inputmethod/SimeInputModeDef;->value:I

    const/high16 v1, 0xf0

    and-int/2addr v0, v1

    return v0
.end method

.method public isDateTime()Z
    .locals 3

    .prologue
    .line 373
    const/4 v0, 0x0

    .line 374
    .local v0, bRet:Z
    invoke-virtual {p0}, Lcom/samsung/inputmethod/SimeInputModeDef;->typeMask()I

    move-result v1

    const/high16 v2, -0x8000

    if-ne v1, v2, :cond_0

    const/16 v1, 0x2000

    invoke-virtual {p0}, Lcom/samsung/inputmethod/SimeInputModeDef;->symbolNumType()I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 376
    const/4 v0, 0x1

    .line 378
    :cond_0
    return v0
.end method

.method public isMonth()Z
    .locals 3

    .prologue
    .line 357
    const/4 v0, 0x0

    .line 358
    .local v0, bRet:Z
    invoke-virtual {p0}, Lcom/samsung/inputmethod/SimeInputModeDef;->typeMask()I

    move-result v1

    const/high16 v2, -0x8000

    if-ne v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/samsung/inputmethod/SimeInputModeDef;->symbolNumType()I

    move-result v1

    const/16 v2, 0x1000

    if-ne v1, v2, :cond_0

    .line 360
    const/4 v0, 0x1

    .line 361
    :cond_0
    return v0
.end method

.method public isNumOnly()Z
    .locals 4

    .prologue
    .line 405
    const/4 v0, 0x0

    .line 406
    .local v0, bRet:Z
    invoke-virtual {p0}, Lcom/samsung/inputmethod/SimeInputModeDef;->isMonth()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/samsung/inputmethod/SimeInputModeDef;->isDateTime()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 407
    :cond_0
    const/4 v0, 0x1

    .line 408
    :cond_1
    iget-boolean v1, p0, Lcom/samsung/inputmethod/SimeInputModeDef;->DEBUG:Z

    if-eqz v1, :cond_2

    .line 409
    sget-object v1, Lcom/samsung/inputmethod/SimeInputModeDef;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "----isNumOnly ----bRet = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 410
    :cond_2
    return v0
.end method

.method public isNumSymModeOnly()Z
    .locals 4

    .prologue
    .line 414
    const/4 v0, 0x0

    .line 415
    .local v0, bRet:Z
    invoke-virtual {p0}, Lcom/samsung/inputmethod/SimeInputModeDef;->isPhone()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 416
    const/4 v0, 0x1

    .line 418
    :cond_0
    iget-boolean v1, p0, Lcom/samsung/inputmethod/SimeInputModeDef;->DEBUG:Z

    if-eqz v1, :cond_1

    .line 419
    sget-object v1, Lcom/samsung/inputmethod/SimeInputModeDef;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "----isNumSymModeOnly ----bRet = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 420
    :cond_1
    return v0
.end method

.method public isPhone()Z
    .locals 3

    .prologue
    .line 390
    const/4 v0, 0x0

    .line 391
    .local v0, bRet:Z
    invoke-virtual {p0}, Lcom/samsung/inputmethod/SimeInputModeDef;->typeMask()I

    move-result v1

    const/high16 v2, 0x5000

    if-ne v1, v2, :cond_0

    .line 392
    const/4 v0, 0x1

    .line 394
    :cond_0
    return v0
.end method

.method public isToolBARSkb()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 312
    iget v1, p0, Lcom/samsung/inputmethod/SimeInputModeDef;->value:I

    and-int/lit8 v1, v1, 0xf

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isValid()Z
    .locals 1

    .prologue
    .line 349
    iget v0, p0, Lcom/samsung/inputmethod/SimeInputModeDef;->value:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public languageMask()I
    .locals 2

    .prologue
    .line 329
    iget v0, p0, Lcom/samsung/inputmethod/SimeInputModeDef;->value:I

    const/high16 v1, 0xf

    and-int/2addr v0, v1

    return v0
.end method

.method public layoutMask()I
    .locals 2

    .prologue
    .line 325
    iget v0, p0, Lcom/samsung/inputmethod/SimeInputModeDef;->value:I

    const/high16 v1, 0xf00

    and-int/2addr v0, v1

    return v0
.end method

.method public recogType()I
    .locals 1

    .prologue
    .line 341
    iget v0, p0, Lcom/samsung/inputmethod/SimeInputModeDef;->value:I

    and-int/lit16 v0, v0, 0xf00

    return v0
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 353
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/inputmethod/SimeInputModeDef;->value:I

    .line 354
    return-void
.end method

.method public symbolNumType()I
    .locals 2

    .prologue
    .line 345
    iget v0, p0, Lcom/samsung/inputmethod/SimeInputModeDef;->value:I

    const v1, 0xf000

    and-int/2addr v0, v1

    return v0
.end method

.method public typeMask()I
    .locals 2

    .prologue
    .line 333
    iget v0, p0, Lcom/samsung/inputmethod/SimeInputModeDef;->value:I

    const/high16 v1, -0x1000

    and-int/2addr v0, v1

    return v0
.end method
