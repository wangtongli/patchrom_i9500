.class public final Lcom/samsung/inputmethod/R$array;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/inputmethod/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "array"
.end annotation


# static fields
.field public static final array_handwrite_color_value:I = 0x7f060007

.field public static final array_handwrite_line_value:I = 0x7f060006

.field public static final array_handwrite_speed:I = 0x7f060008

.field public static final array_handwrite_speed_value:I = 0x7f060009

.field public static final array_handwrite_type:I = 0x7f060014

.field public static final array_handwrite_type_value:I = 0x7f060015

.field public static final array_handwriting_style:I = 0x7f060019

.field public static final array_handwriting_style_value:I = 0x7f06001a

.field public static final array_language_dis_summary:I = 0x7f060018

.field public static final array_space_key_type:I = 0x7f060016

.field public static final array_space_key_type_value:I = 0x7f060017

.field public static final auto_update_frequency_list_preference:I = 0x7f060000

.field public static final auto_update_frequency_list_preference_value:I = 0x7f060001

.field public static final currency_popup_window_value:I = 0x7f060034

.field public static final entries_handwriting_mode:I = 0x7f060004

.field public static final entries_list_preference:I = 0x7f060002

.field public static final entryvalues_handwriting_mode:I = 0x7f060005

.field public static final entryvalues_list_preference:I = 0x7f060003

.field public static final keyboard_swipe_type_list_preference:I = 0x7f06001b

.field public static final keyboard_swipe_type_list_preference_value:I = 0x7f06001d

.field public static final keyboard_swipe_type_list_summary:I = 0x7f06001c

.field public static final keyboard_type_list_entries_en:I = 0x7f060026

.field public static final keyboard_type_list_entries_kor:I = 0x7f060024

.field public static final keyboard_type_list_entries_zh_cn:I = 0x7f06001e

.field public static final keyboard_type_list_entries_zh_hk:I = 0x7f060020

.field public static final keyboard_type_list_entries_zh_tw:I = 0x7f060022

.field public static final keyboard_type_list_entry_values_en:I = 0x7f060027

.field public static final keyboard_type_list_entry_values_kor:I = 0x7f060025

.field public static final keyboard_type_list_entry_values_zh_cn:I = 0x7f06001f

.field public static final keyboard_type_list_entry_values_zh_hk:I = 0x7f060021

.field public static final keyboard_type_list_entry_values_zh_tw:I = 0x7f060023

.field public static final keyboard_type_list_no_box_entries_en:I = 0x7f060030

.field public static final keyboard_type_list_no_box_entries_kor:I = 0x7f06002e

.field public static final keyboard_type_list_no_box_entries_zh_cn:I = 0x7f060028

.field public static final keyboard_type_list_no_box_entries_zh_hk:I = 0x7f06002a

.field public static final keyboard_type_list_no_box_entries_zh_tw:I = 0x7f06002c

.field public static final keyboard_type_list_no_box_entry_values_en:I = 0x7f060031

.field public static final keyboard_type_list_no_box_entry_values_kor:I = 0x7f06002f

.field public static final keyboard_type_list_no_box_entry_values_zh_cn:I = 0x7f060029

.field public static final keyboard_type_list_no_box_entry_values_zh_hk:I = 0x7f06002b

.field public static final keyboard_type_list_no_box_entry_values_zh_tw:I = 0x7f06002d

.field public static final keypad_type_list_preference:I = 0x7f06000a

.field public static final keypad_type_list_preference_1:I = 0x7f06000c

.field public static final keypad_type_list_preference_2:I = 0x7f06000e

.field public static final keypad_type_list_preference_3:I = 0x7f060010

.field public static final keypad_type_list_preference_value:I = 0x7f06000b

.field public static final keypad_type_list_preference_value_1:I = 0x7f06000d

.field public static final keypad_type_list_preference_value_2:I = 0x7f06000f

.field public static final keypad_type_list_preference_value_3:I = 0x7f060011

.field public static final keypad_type_list_summary_hk:I = 0x7f060013

.field public static final keypad_type_list_summary_tw:I = 0x7f060012

.field public static final tab_editlist_title:I = 0x7f060032

.field public static final tab_titles_def_checked:I = 0x7f060033


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
