.class public final Lcom/samsung/inputmethod/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/inputmethod/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final alternative_character_bg:I = 0x7f020000

.field public static final alternative_character_bg_p:I = 0x7f020001

.field public static final app_icon:I = 0x7f020002

.field public static final arrow_bg:I = 0x7f020003

.field public static final arrow_down:I = 0x7f020004

.field public static final arrow_down_floating:I = 0x7f020005

.field public static final arrow_left:I = 0x7f020006

.field public static final arrow_right:I = 0x7f020007

.field public static final arrow_up:I = 0x7f020008

.field public static final arrow_up_floating:I = 0x7f020009

.field public static final btn_keyboard_capslock_off:I = 0x7f02000a

.field public static final btn_keyboard_capslock_off_01:I = 0x7f02000b

.field public static final btn_keyboard_capslock_off_press:I = 0x7f02000c

.field public static final btn_keyboard_capslock_off_press_01:I = 0x7f02000d

.field public static final btn_keyboard_capslock_on:I = 0x7f02000e

.field public static final btn_keyboard_capslock_on_01:I = 0x7f02000f

.field public static final btn_keyboard_capslock_on_press:I = 0x7f020010

.field public static final btn_keyboard_capslock_on_press_01:I = 0x7f020011

.field public static final btn_keyboard_key_doubletab:I = 0x7f020012

.field public static final btn_keyboard_key_normal:I = 0x7f020013

.field public static final btn_keyboard_key_normal_01_v:I = 0x7f020014

.field public static final btn_keyboard_key_normal_02_v:I = 0x7f020015

.field public static final btn_keyboard_key_normal_03_v:I = 0x7f020016

.field public static final btn_keyboard_key_number:I = 0x7f020017

.field public static final btn_keyboard_key_press:I = 0x7f020018

.field public static final btn_keyboard_key_trans:I = 0x7f020019

.field public static final btn_keyboard_symbol_key:I = 0x7f02001a

.field public static final btn_keyboard_symbol_key_02:I = 0x7f02001b

.field public static final btn_keyboard_symbol_key_press:I = 0x7f02001c

.field public static final btn_round_more:I = 0x7f02001d

.field public static final btn_round_more_pressed:I = 0x7f02001e

.field public static final cand_arrow_left:I = 0x7f02001f

.field public static final cand_arrow_left_disabled:I = 0x7f020020

.field public static final cand_arrow_left_focus:I = 0x7f020021

.field public static final cand_arrow_right:I = 0x7f020022

.field public static final cand_arrow_right_disabled:I = 0x7f020023

.field public static final cand_arrow_right_focus:I = 0x7f020024

.field public static final cand_cell_bg:I = 0x7f020025

.field public static final cand_popup_horizontal_line:I = 0x7f020026

.field public static final candidate_balloon_bg:I = 0x7f020027

.field public static final candidate_feedback_background:I = 0x7f020028

.field public static final candidate_hl_bg:I = 0x7f020029

.field public static final candidate_phonebook_icon:I = 0x7f02002a

.field public static final candidate_predictive_popup_bg:I = 0x7f02002b

.field public static final clipboard_contents_imagbox:I = 0x7f02002c

.field public static final combined_candidate_bg:I = 0x7f02002d

.field public static final composing_area_cursor:I = 0x7f02002e

.field public static final composing_hl_bg:I = 0x7f02002f

.field public static final editmore_pressed:I = 0x7f020030

.field public static final floating_icon_quick_cangjie_off:I = 0x7f020031

.field public static final floating_icon_quick_cangjie_on:I = 0x7f020032

.field public static final floating_move_handler_bg:I = 0x7f020033

.field public static final hand_icon_keypad:I = 0x7f020034

.field public static final hand_icon_keypad_p:I = 0x7f020035

.field public static final handwriting_controll_bg:I = 0x7f020036

.field public static final handwriting_ic_back_space:I = 0x7f020037

.field public static final handwriting_ic_back_space_press:I = 0x7f020038

.field public static final handwriting_ic_china:I = 0x7f020039

.field public static final handwriting_ic_china_press:I = 0x7f02003a

.field public static final handwriting_ic_option:I = 0x7f02003b

.field public static final handwriting_ic_option_press:I = 0x7f02003c

.field public static final handwriting_ic_return:I = 0x7f02003d

.field public static final handwriting_ic_return_press:I = 0x7f02003e

.field public static final handwriting_ic_sip:I = 0x7f02003f

.field public static final handwriting_ic_sip_press:I = 0x7f020040

.field public static final handwriting_ic_space:I = 0x7f020041

.field public static final handwriting_ic_space_press:I = 0x7f020042

.field public static final handwriting_pen_thickness_01:I = 0x7f020043

.field public static final handwriting_pen_thickness_02:I = 0x7f020044

.field public static final handwriting_pen_thickness_03:I = 0x7f020045

.field public static final handwriting_pen_thickness_04:I = 0x7f020046

.field public static final handwriting_pen_thickness_05:I = 0x7f020047

.field public static final handwriting_pencolor_black:I = 0x7f020048

.field public static final handwriting_pencolor_blue:I = 0x7f020049

.field public static final handwriting_pencolor_brown:I = 0x7f02004a

.field public static final handwriting_pencolor_green:I = 0x7f02004b

.field public static final handwriting_pencolor_red:I = 0x7f02004c

.field public static final handwritng_panel_bg:I = 0x7f02004d

.field public static final handwritng_panel_bg01:I = 0x7f02004e

.field public static final handwritng_panel_bg01_h:I = 0x7f02004f

.field public static final handwritng_panel_bg_02:I = 0x7f020050

.field public static final handwritng_panel_bg_02_h:I = 0x7f020051

.field public static final handwritng_panel_floating:I = 0x7f020052

.field public static final hw_box_bg:I = 0x7f020053

.field public static final hwr_icon_arrow_h_off:I = 0x7f020054

.field public static final hwr_icon_arrow_h_on:I = 0x7f020055

.field public static final hwr_icon_arrow_off:I = 0x7f020056

.field public static final hwr_icon_arrow_on:I = 0x7f020057

.field public static final hwr_icon_arrow_press_h_off:I = 0x7f020058

.field public static final hwr_icon_arrow_press_h_on:I = 0x7f020059

.field public static final hwr_icon_arrow_press_off:I = 0x7f02005a

.field public static final hwr_icon_arrow_press_on:I = 0x7f02005b

.field public static final key_balloon_bg:I = 0x7f02005c

.field public static final keyboard_icon_del:I = 0x7f02005d

.field public static final keyboard_icon_enter:I = 0x7f02005e

.field public static final keyboard_icon_setting:I = 0x7f02005f

.field public static final keyboard_icon_space:I = 0x7f020060

.field public static final keyboard_move:I = 0x7f020061

.field public static final keyboard_set_popup_pointer_down:I = 0x7f020062

.field public static final keyboard_set_popup_pointer_up:I = 0x7f020063

.field public static final keyboard_set_popup_table_bg:I = 0x7f020064

.field public static final keyboard_suggest_strip_divider:I = 0x7f020065

.field public static final keyboard_textfield_selected:I = 0x7f020066

.field public static final keyboard_type_btn_bg:I = 0x7f020067

.field public static final keyboard_type_btn_bg_default:I = 0x7f020068

.field public static final keyboard_type_btn_bg_pressed:I = 0x7f020069

.field public static final keyboard_type_btn_bg_selected:I = 0x7f02006a

.field public static final keyboard_zoom:I = 0x7f02006b

.field public static final keypad_arrow_down:I = 0x7f02006c

.field public static final keypad_arrow_down_p:I = 0x7f02006d

.field public static final keypad_arrow_up:I = 0x7f02006e

.field public static final keypad_arrow_up_p:I = 0x7f02006f

.field public static final keypad_dial_num00:I = 0x7f020070

.field public static final keypad_dial_num00_h:I = 0x7f020071

.field public static final keypad_dial_num00_h_p:I = 0x7f020072

.field public static final keypad_dial_num00_p:I = 0x7f020073

.field public static final keypad_dial_num01:I = 0x7f020074

.field public static final keypad_dial_num01_h:I = 0x7f020075

.field public static final keypad_dial_num01_h_p:I = 0x7f020076

.field public static final keypad_dial_num01_p:I = 0x7f020077

.field public static final keypad_dial_num02:I = 0x7f020078

.field public static final keypad_dial_num02_h:I = 0x7f020079

.field public static final keypad_dial_num02_h_p:I = 0x7f02007a

.field public static final keypad_dial_num02_p:I = 0x7f02007b

.field public static final keypad_dial_num03:I = 0x7f02007c

.field public static final keypad_dial_num03_h:I = 0x7f02007d

.field public static final keypad_dial_num03_h_p:I = 0x7f02007e

.field public static final keypad_dial_num03_p:I = 0x7f02007f

.field public static final keypad_dial_num04:I = 0x7f020080

.field public static final keypad_dial_num04_h:I = 0x7f020081

.field public static final keypad_dial_num04_h_p:I = 0x7f020082

.field public static final keypad_dial_num04_p:I = 0x7f020083

.field public static final keypad_dial_num05:I = 0x7f020084

.field public static final keypad_dial_num05_h:I = 0x7f020085

.field public static final keypad_dial_num05_h_p:I = 0x7f020086

.field public static final keypad_dial_num05_p:I = 0x7f020087

.field public static final keypad_dial_num06:I = 0x7f020088

.field public static final keypad_dial_num06_h:I = 0x7f020089

.field public static final keypad_dial_num06_h_p:I = 0x7f02008a

.field public static final keypad_dial_num06_p:I = 0x7f02008b

.field public static final keypad_dial_num07:I = 0x7f02008c

.field public static final keypad_dial_num07_h:I = 0x7f02008d

.field public static final keypad_dial_num07_h_p:I = 0x7f02008e

.field public static final keypad_dial_num07_p:I = 0x7f02008f

.field public static final keypad_dial_num08:I = 0x7f020090

.field public static final keypad_dial_num08_h:I = 0x7f020091

.field public static final keypad_dial_num08_h_p:I = 0x7f020092

.field public static final keypad_dial_num08_p:I = 0x7f020093

.field public static final keypad_dial_num09:I = 0x7f020094

.field public static final keypad_dial_num09_h:I = 0x7f020095

.field public static final keypad_dial_num09_h_p:I = 0x7f020096

.field public static final keypad_dial_num09_p:I = 0x7f020097

.field public static final keypad_dial_num10:I = 0x7f020098

.field public static final keypad_dial_num10_h:I = 0x7f020099

.field public static final keypad_dial_num10_h_p:I = 0x7f02009a

.field public static final keypad_dial_num10_p:I = 0x7f02009b

.field public static final keypad_dial_num11:I = 0x7f02009c

.field public static final keypad_dial_num11_h:I = 0x7f02009d

.field public static final keypad_dial_num11_h_p:I = 0x7f02009e

.field public static final keypad_dial_num11_p:I = 0x7f02009f

.field public static final keypad_icon_chinesesymbol_01:I = 0x7f0200a0

.field public static final keypad_icon_chinesesymbol_01_p:I = 0x7f0200a1

.field public static final keypad_icon_del:I = 0x7f0200a2

.field public static final keypad_icon_del_01:I = 0x7f0200a3

.field public static final keypad_icon_del_01_p:I = 0x7f0200a4

.field public static final keypad_icon_del_02:I = 0x7f0200a5

.field public static final keypad_icon_del_02_p:I = 0x7f0200a6

.field public static final keypad_icon_del_03:I = 0x7f0200a7

.field public static final keypad_icon_del_03_p:I = 0x7f0200a8

.field public static final keypad_icon_del_p:I = 0x7f0200a9

.field public static final keypad_icon_englishsymbol_01:I = 0x7f0200aa

.field public static final keypad_icon_englishsymbol_01_p:I = 0x7f0200ab

.field public static final keypad_icon_enter:I = 0x7f0200ac

.field public static final keypad_icon_enter_p:I = 0x7f0200ad

.field public static final keypad_icon_handwriting:I = 0x7f0200ae

.field public static final keypad_icon_handwriting_abc:I = 0x7f0200af

.field public static final keypad_icon_handwriting_abc_press:I = 0x7f0200b0

.field public static final keypad_icon_handwriting_china:I = 0x7f0200b1

.field public static final keypad_icon_handwriting_china_press:I = 0x7f0200b2

.field public static final keypad_icon_handwriting_korean:I = 0x7f0200b3

.field public static final keypad_icon_handwriting_korean_press:I = 0x7f0200b4

.field public static final keypad_icon_handwriting_number:I = 0x7f0200b5

.field public static final keypad_icon_handwriting_number_press:I = 0x7f0200b6

.field public static final keypad_icon_handwriting_p:I = 0x7f0200b7

.field public static final keypad_icon_lock:I = 0x7f0200b8

.field public static final keypad_icon_lock_open:I = 0x7f0200b9

.field public static final keypad_icon_lock_open_p:I = 0x7f0200ba

.field public static final keypad_icon_lock_p:I = 0x7f0200bb

.field public static final keypad_icon_long_press:I = 0x7f0200bc

.field public static final keypad_icon_off:I = 0x7f0200bd

.field public static final keypad_icon_on:I = 0x7f0200be

.field public static final keypad_icon_quick_cangjie_off:I = 0x7f0200bf

.field public static final keypad_icon_quick_cangjie_off_floating:I = 0x7f0200c0

.field public static final keypad_icon_quick_cangjie_on:I = 0x7f0200c1

.field public static final keypad_icon_quick_cangjie_on_floating:I = 0x7f0200c2

.field public static final keypad_icon_return:I = 0x7f0200c3

.field public static final keypad_icon_return_p:I = 0x7f0200c4

.field public static final keypad_icon_search:I = 0x7f0200c5

.field public static final keypad_icon_search_p:I = 0x7f0200c6

.field public static final keypad_icon_setting:I = 0x7f0200c7

.field public static final keypad_icon_setting_p:I = 0x7f0200c8

.field public static final keypad_icon_shift:I = 0x7f0200c9

.field public static final keypad_icon_shift_lp:I = 0x7f0200ca

.field public static final keypad_icon_shift_off:I = 0x7f0200cb

.field public static final keypad_icon_shift_on:I = 0x7f0200cc

.field public static final keypad_icon_shift_p:I = 0x7f0200cd

.field public static final keypad_icon_shift_shift:I = 0x7f0200ce

.field public static final keypad_icon_shift_toolbar:I = 0x7f0200cf

.field public static final keypad_icon_space:I = 0x7f0200d0

.field public static final keypad_icon_space_01:I = 0x7f0200d1

.field public static final keypad_icon_space_01_p:I = 0x7f0200d2

.field public static final keypad_icon_space_02:I = 0x7f0200d3

.field public static final keypad_icon_space_02_p:I = 0x7f0200d4

.field public static final keypad_icon_space_bubble:I = 0x7f0200d5

.field public static final keypad_icon_space_p:I = 0x7f0200d6

.field public static final keypad_icon_sym_01:I = 0x7f0200d7

.field public static final keypad_icon_sym_02:I = 0x7f0200d8

.field public static final keypad_icon_sym_03:I = 0x7f0200d9

.field public static final keypad_icon_sym_04:I = 0x7f0200da

.field public static final keypad_icon_sym_05:I = 0x7f0200db

.field public static final keypad_icon_sym_06:I = 0x7f0200dc

.field public static final keypad_icon_sym_07:I = 0x7f0200dd

.field public static final keypad_icon_sym_08:I = 0x7f0200de

.field public static final keypad_icon_sym_09:I = 0x7f0200df

.field public static final keypad_icon_sym_10:I = 0x7f0200e0

.field public static final keypad_icon_sym_11:I = 0x7f0200e1

.field public static final keypad_icon_sym_12:I = 0x7f0200e2

.field public static final keypad_icon_sym_13:I = 0x7f0200e3

.field public static final keypad_icon_sym_14:I = 0x7f0200e4

.field public static final keypad_icon_sym_15:I = 0x7f0200e5

.field public static final keypad_icon_sym_16:I = 0x7f0200e6

.field public static final keypad_icon_sym_17:I = 0x7f0200e7

.field public static final keypad_icon_sym_18:I = 0x7f0200e8

.field public static final keypad_icon_sym_19:I = 0x7f0200e9

.field public static final keypad_icon_sym_20:I = 0x7f0200ea

.field public static final keypad_icon_sym_21:I = 0x7f0200eb

.field public static final keypad_icon_sym_22:I = 0x7f0200ec

.field public static final keypad_icon_sym_23:I = 0x7f0200ed

.field public static final keypad_icon_sym_24:I = 0x7f0200ee

.field public static final keypad_icon_sym_25:I = 0x7f0200ef

.field public static final keypad_icon_sym_26:I = 0x7f0200f0

.field public static final keypad_icon_sym_27:I = 0x7f0200f1

.field public static final keypad_icon_sym_28:I = 0x7f0200f2

.field public static final keypad_icon_sym_29:I = 0x7f0200f3

.field public static final keypad_icon_sym_30:I = 0x7f0200f4

.field public static final keypad_icon_sym_31:I = 0x7f0200f5

.field public static final keypad_icon_sym_32:I = 0x7f0200f6

.field public static final keypad_icon_sym_33:I = 0x7f0200f7

.field public static final keypad_icon_sym_34:I = 0x7f0200f8

.field public static final keypad_icon_sym_35:I = 0x7f0200f9

.field public static final keypad_icon_sym_36:I = 0x7f0200fa

.field public static final keypad_icon_sym_37:I = 0x7f0200fb

.field public static final keypad_icon_sym_38:I = 0x7f0200fc

.field public static final keypad_icon_sym_39:I = 0x7f0200fd

.field public static final keypad_icon_sym_40:I = 0x7f0200fe

.field public static final keypad_icon_sym_41:I = 0x7f0200ff

.field public static final keypad_icon_sym_42:I = 0x7f020100

.field public static final keypad_icon_sym_43:I = 0x7f020101

.field public static final keypad_icon_sym_44:I = 0x7f020102

.field public static final keypad_icon_sym_45:I = 0x7f020103

.field public static final keypad_icon_sym_46:I = 0x7f020104

.field public static final keypad_icon_sym_47:I = 0x7f020105

.field public static final keypad_icon_sym_48:I = 0x7f020106

.field public static final keypad_icon_sym_49:I = 0x7f020107

.field public static final keypad_icon_sym_50:I = 0x7f020108

.field public static final keypad_icon_sym_51:I = 0x7f020109

.field public static final keypad_icon_sym_52:I = 0x7f02010a

.field public static final keypad_icon_sym_53:I = 0x7f02010b

.field public static final keypad_icon_sym_54:I = 0x7f02010c

.field public static final keypad_icon_toggle:I = 0x7f02010d

.field public static final keypad_icon_toggle_ke:I = 0x7f02010e

.field public static final keypad_icon_toggle_ke_p:I = 0x7f02010f

.field public static final keypad_icon_toggle_p:I = 0x7f020110

.field public static final keypad_icon_tones:I = 0x7f020111

.field public static final keypad_icon_tones_h:I = 0x7f020112

.field public static final keypad_icon_tones_h_p:I = 0x7f020113

.field public static final keypad_icon_tones_p:I = 0x7f020114

.field public static final keypad_icon_voice:I = 0x7f020115

.field public static final keypad_icon_voice_p:I = 0x7f020116

.field public static final keypad_icon_xt9_off:I = 0x7f020117

.field public static final keypad_icon_xt9_off_h:I = 0x7f020118

.field public static final keypad_icon_xt9_on:I = 0x7f020119

.field public static final keypad_icon_xt9_on_h:I = 0x7f02011a

.field public static final keypad_num0:I = 0x7f02011b

.field public static final keypad_num0_p:I = 0x7f02011c

.field public static final keypad_num1:I = 0x7f02011d

.field public static final keypad_num1_p:I = 0x7f02011e

.field public static final keypad_num2:I = 0x7f02011f

.field public static final keypad_num2_p:I = 0x7f020120

.field public static final keypad_num3:I = 0x7f020121

.field public static final keypad_num3_p:I = 0x7f020122

.field public static final keypad_num4:I = 0x7f020123

.field public static final keypad_num4_p:I = 0x7f020124

.field public static final keypad_num5:I = 0x7f020125

.field public static final keypad_num5_p:I = 0x7f020126

.field public static final keypad_num6:I = 0x7f020127

.field public static final keypad_num6_p:I = 0x7f020128

.field public static final keypad_num7:I = 0x7f020129

.field public static final keypad_num7_p:I = 0x7f02012a

.field public static final keypad_num8:I = 0x7f02012b

.field public static final keypad_num8_p:I = 0x7f02012c

.field public static final keypad_num9:I = 0x7f02012d

.field public static final keypad_num9_p:I = 0x7f02012e

.field public static final lang_en_hkchn_34:I = 0x7f02012f

.field public static final lang_en_hkchn_hwr:I = 0x7f020130

.field public static final lang_en_hkchn_qwerty:I = 0x7f020131

.field public static final lang_en_ko_34:I = 0x7f020132

.field public static final lang_en_ko_hwr:I = 0x7f020133

.field public static final lang_en_ko_qwerty:I = 0x7f020134

.field public static final lang_en_schn_34:I = 0x7f020135

.field public static final lang_en_schn_hwr:I = 0x7f020136

.field public static final lang_en_schn_qwerty:I = 0x7f020137

.field public static final lang_en_twchn_34:I = 0x7f020138

.field public static final lang_en_twchn_hwr:I = 0x7f020139

.field public static final lang_en_twchn_qwerty:I = 0x7f02013a

.field public static final lang_hkchn_en_34:I = 0x7f02013b

.field public static final lang_hkchn_en_hwr:I = 0x7f02013c

.field public static final lang_hkchn_en_qwerty:I = 0x7f02013d

.field public static final lang_ko_en_34:I = 0x7f02013e

.field public static final lang_ko_en_hwr:I = 0x7f02013f

.field public static final lang_ko_en_qwerty:I = 0x7f020140

.field public static final lang_schn_en_34:I = 0x7f020141

.field public static final lang_schn_en_hwr:I = 0x7f020142

.field public static final lang_schn_en_qwerty:I = 0x7f020143

.field public static final lang_twchn_en_34:I = 0x7f020144

.field public static final lang_twchn_en_hwr:I = 0x7f020145

.field public static final lang_twchn_en_qwerty:I = 0x7f020146

.field public static final mini_popup_cell_bg:I = 0x7f020147

.field public static final mini_popup_cell_txt_color:I = 0x7f020148

.field public static final mini_popup_sym_cell_bg:I = 0x7f020149

.field public static final miniskb_bg:I = 0x7f02014a

.field public static final page_arrow_01_l:I = 0x7f02014b

.field public static final page_arrow_01_l_p:I = 0x7f02014c

.field public static final page_arrow_01_r:I = 0x7f02014d

.field public static final page_arrow_01_r_p:I = 0x7f02014e

.field public static final page_arrow_next:I = 0x7f02014f

.field public static final page_arrow_next_p:I = 0x7f020150

.field public static final page_arrow_pre:I = 0x7f020151

.field public static final page_arrow_pre_p:I = 0x7f020152

.field public static final page_b_p:I = 0x7f020153

.field public static final page_l_p:I = 0x7f020154

.field public static final page_r_p:I = 0x7f020155

.field public static final page_t_p:I = 0x7f020156

.field public static final pedictive_popup_bg:I = 0x7f020157

.field public static final pedictive_popup_bg_gray:I = 0x7f020158

.field public static final pedictive_popup_main_bg:I = 0x7f020159

.field public static final phonepad_key_icon_backspace:I = 0x7f02015a

.field public static final phonepad_key_icon_backspace_pressed:I = 0x7f02015b

.field public static final phonepad_key_icon_clean:I = 0x7f02015c

.field public static final phonepad_key_icon_clean_pressed:I = 0x7f02015d

.field public static final phonepad_key_icon_enter:I = 0x7f02015e

.field public static final phonepad_key_icon_enter_pressed:I = 0x7f02015f

.field public static final phonepad_key_icon_space:I = 0x7f020160

.field public static final phonepad_key_icon_space_pressed:I = 0x7f020161

.field public static final phonepad_key_icon_space_suggestion:I = 0x7f020162

.field public static final popup_bg_non_shadow:I = 0x7f020163

.field public static final popup_key_bg_option:I = 0x7f020164

.field public static final popup_key_bg_option_pressed:I = 0x7f020165

.field public static final popup_key_bg_pressed:I = 0x7f020166

.field public static final popup_key_icon_clipboard:I = 0x7f020167

.field public static final popup_key_icon_clipboard_pressed:I = 0x7f020168

.field public static final popup_key_icon_floating:I = 0x7f020169

.field public static final popup_key_icon_floating_pressed:I = 0x7f02016a

.field public static final popup_key_icon_hw_setting:I = 0x7f02016b

.field public static final popup_key_icon_hw_setting_pressed:I = 0x7f02016c

.field public static final popup_key_icon_hwr:I = 0x7f02016d

.field public static final popup_key_icon_hwr_pressed:I = 0x7f02016e

.field public static final popup_key_icon_ime:I = 0x7f02016f

.field public static final popup_key_icon_ime_pressed:I = 0x7f020170

.field public static final popup_key_icon_keyboard:I = 0x7f020171

.field public static final popup_key_icon_keyboard_pressed:I = 0x7f020172

.field public static final popup_key_icon_ocr:I = 0x7f020173

.field public static final popup_key_icon_ocr_pressed:I = 0x7f020174

.field public static final popup_key_icon_qr:I = 0x7f020175

.field public static final popup_key_icon_qr_pressed:I = 0x7f020176

.field public static final popup_key_icon_setting:I = 0x7f020177

.field public static final popup_key_icon_setting_pressed:I = 0x7f020178

.field public static final popup_key_icon_settings:I = 0x7f020179

.field public static final popup_key_icon_settings_pressed:I = 0x7f02017a

.field public static final popup_key_icon_voice:I = 0x7f02017b

.field public static final popup_key_icon_voice_pressed:I = 0x7f02017c

.field public static final popup_symbol_cell_bg:I = 0x7f02017d

.field public static final qwerty_arrow_l:I = 0x7f02017e

.field public static final qwerty_arrow_l_p:I = 0x7f02017f

.field public static final qwerty_arrow_r:I = 0x7f020180

.field public static final qwerty_arrow_r_p:I = 0x7f020181

.field public static final qwerty_icon_backspace:I = 0x7f020182

.field public static final qwerty_icon_backspace_pressed:I = 0x7f020183

.field public static final qwerty_icon_del:I = 0x7f020184

.field public static final qwerty_icon_del_bubble:I = 0x7f020185

.field public static final qwerty_icon_del_p:I = 0x7f020186

.field public static final qwerty_icon_del_zhuyin:I = 0x7f020187

.field public static final qwerty_icon_del_zhuyin_p:I = 0x7f020188

.field public static final qwerty_icon_enter:I = 0x7f020189

.field public static final qwerty_icon_enter_bubble:I = 0x7f02018a

.field public static final qwerty_icon_enter_p:I = 0x7f02018b

.field public static final qwerty_icon_handwriting:I = 0x7f02018c

.field public static final qwerty_icon_handwriting_p:I = 0x7f02018d

.field public static final qwerty_icon_lock:I = 0x7f02018e

.field public static final qwerty_icon_lock_p:I = 0x7f02018f

.field public static final qwerty_icon_long_press:I = 0x7f020190

.field public static final qwerty_icon_long_press_p:I = 0x7f020191

.field public static final qwerty_icon_return:I = 0x7f020192

.field public static final qwerty_icon_return_p:I = 0x7f020193

.field public static final qwerty_icon_search_bubble:I = 0x7f020194

.field public static final qwerty_icon_setting:I = 0x7f020195

.field public static final qwerty_icon_setting_p:I = 0x7f020196

.field public static final qwerty_icon_shift:I = 0x7f020197

.field public static final qwerty_icon_shift_bubble:I = 0x7f020198

.field public static final qwerty_icon_shift_p:I = 0x7f020199

.field public static final qwerty_icon_shift_shift:I = 0x7f02019a

.field public static final qwerty_icon_shift_shift_bg:I = 0x7f02019b

.field public static final qwerty_icon_space:I = 0x7f02019c

.field public static final qwerty_icon_space_bubble:I = 0x7f02019d

.field public static final qwerty_icon_space_p:I = 0x7f02019e

.field public static final qwerty_icon_space_zhuyin_email:I = 0x7f02019f

.field public static final qwerty_icon_space_zhuyin_email_p:I = 0x7f0201a0

.field public static final qwerty_icon_switch_off:I = 0x7f0201a1

.field public static final qwerty_icon_switch_off_h:I = 0x7f0201a2

.field public static final qwerty_icon_switch_on:I = 0x7f0201a3

.field public static final qwerty_icon_switch_on_h:I = 0x7f0201a4

.field public static final qwerty_icon_toggle:I = 0x7f0201a5

.field public static final qwerty_icon_toggle_ke:I = 0x7f0201a6

.field public static final qwerty_icon_toggle_ke_p:I = 0x7f0201a7

.field public static final qwerty_icon_toggle_p:I = 0x7f0201a8

.field public static final qwerty_icon_unlock:I = 0x7f0201a9

.field public static final qwerty_icon_unlock_p:I = 0x7f0201aa

.field public static final qwerty_key_icon_clipboard:I = 0x7f0201ab

.field public static final qwerty_key_icon_clipboard_pressed:I = 0x7f0201ac

.field public static final qwerty_key_icon_enter:I = 0x7f0201ad

.field public static final qwerty_key_icon_enter_pressed:I = 0x7f0201ae

.field public static final qwerty_key_icon_floating:I = 0x7f0201af

.field public static final qwerty_key_icon_floating_pressed:I = 0x7f0201b0

.field public static final qwerty_key_icon_hwr:I = 0x7f0201b1

.field public static final qwerty_key_icon_hwr_pressed:I = 0x7f0201b2

.field public static final qwerty_key_icon_hwr_quick:I = 0x7f0201b3

.field public static final qwerty_key_icon_hwr_quick_pressed:I = 0x7f0201b4

.field public static final qwerty_key_icon_hwr_setting:I = 0x7f0201b5

.field public static final qwerty_key_icon_hwr_setting_pressed:I = 0x7f0201b6

.field public static final qwerty_key_icon_ime:I = 0x7f0201b7

.field public static final qwerty_key_icon_ime_pressed:I = 0x7f0201b8

.field public static final qwerty_key_icon_keyboard:I = 0x7f0201b9

.field public static final qwerty_key_icon_keyboard_pressed:I = 0x7f0201ba

.field public static final qwerty_key_icon_ocr:I = 0x7f0201bb

.field public static final qwerty_key_icon_ocr_pressed:I = 0x7f0201bc

.field public static final qwerty_key_icon_qr:I = 0x7f0201bd

.field public static final qwerty_key_icon_qr_pressed:I = 0x7f0201be

.field public static final qwerty_key_icon_settings:I = 0x7f0201bf

.field public static final qwerty_key_icon_settings_pressed:I = 0x7f0201c0

.field public static final qwerty_key_icon_space_center:I = 0x7f0201c1

.field public static final qwerty_key_icon_space_center_pressed:I = 0x7f0201c2

.field public static final qwerty_key_icon_space_center_suggestion:I = 0x7f0201c3

.field public static final qwerty_key_icon_voice:I = 0x7f0201c4

.field public static final qwerty_key_icon_voice_pressed:I = 0x7f0201c5

.field public static final qwerty_popup_line_03:I = 0x7f0201c6

.field public static final qwerty_popup_line_04:I = 0x7f0201c7

.field public static final qwerty_space_language_arrow_left_press:I = 0x7f0201c8

.field public static final qwerty_space_language_arrow_right_press:I = 0x7f0201c9

.field public static final scroll_keypad_bg:I = 0x7f0201ca

.field public static final search_icon:I = 0x7f0201cb

.field public static final search_icon_bubble:I = 0x7f0201cc

.field public static final settings_drawable_more_button:I = 0x7f0201cd

.field public static final settings_header_icon_refresh:I = 0x7f0201ce

.field public static final settings_header_icon_refresh_disabled:I = 0x7f0201cf

.field public static final settings_header_icon_refresh_pressed:I = 0x7f0201d0

.field public static final settings_list_pen_thickness_01:I = 0x7f0201d1

.field public static final settings_list_pen_thickness_02:I = 0x7f0201d2

.field public static final settings_list_pen_thickness_03:I = 0x7f0201d3

.field public static final settings_list_pen_thickness_04:I = 0x7f0201d4

.field public static final settings_list_pen_thickness_05:I = 0x7f0201d5

.field public static final settings_list_pencolor_black:I = 0x7f0201d6

.field public static final settings_list_pencolor_blue:I = 0x7f0201d7

.field public static final settings_list_pencolor_brown:I = 0x7f0201d8

.field public static final settings_list_pencolor_green:I = 0x7f0201d9

.field public static final settings_list_pencolor_red:I = 0x7f0201da

.field public static final sip_btn_bubble_normal:I = 0x7f0201db

.field public static final sip_btn_sand_press:I = 0x7f0201dc

.field public static final sip_bubble_icon_space:I = 0x7f0201dd

.field public static final sip_button_longpress:I = 0x7f0201de

.field public static final sip_button_longpress_question:I = 0x7f0201df

.field public static final sip_button_longpress_setting:I = 0x7f0201e0

.field public static final sip_button_longpress_voice:I = 0x7f0201e1

.field public static final sip_button_n:I = 0x7f0201e2

.field public static final sip_button_p:I = 0x7f0201e3

.field public static final sip_button_right_bg:I = 0x7f0201e4

.field public static final sip_candidate_expanded_bg:I = 0x7f0201e5

.field public static final sip_clipboard_bg:I = 0x7f0201e6

.field public static final sip_field_bg:I = 0x7f0201e7

.field public static final sip_floating_keypad_handler:I = 0x7f0201e8

.field public static final sip_floating_keypad_handler_press:I = 0x7f0201e9

.field public static final sip_function_bg:I = 0x7f0201ea

.field public static final sip_hw_bg:I = 0x7f0201eb

.field public static final sip_hw_icon_1234:I = 0x7f0201ec

.field public static final sip_hw_icon_1234_press:I = 0x7f0201ed

.field public static final sip_hw_icon_3x4:I = 0x7f0201ee

.field public static final sip_hw_icon_3x4_press:I = 0x7f0201ef

.field public static final sip_hw_icon_qwerty:I = 0x7f0201f0

.field public static final sip_hw_icon_qwerty_press:I = 0x7f0201f1

.field public static final sip_hw_icon_sym:I = 0x7f0201f2

.field public static final sip_hw_icon_sym_press:I = 0x7f0201f3

.field public static final sip_hw_sidetab_left:I = 0x7f0201f4

.field public static final sip_hw_sidetab_right:I = 0x7f0201f5

.field public static final sip_hw_tabpopup_bg_left:I = 0x7f0201f6

.field public static final sip_hw_tabpopup_bg_right:I = 0x7f0201f7

.field public static final sip_hwr_bg:I = 0x7f0201f8

.field public static final sip_hwr_bg_h:I = 0x7f0201f9

.field public static final sip_icon_arrow_left:I = 0x7f0201fa

.field public static final sip_icon_arrow_right:I = 0x7f0201fb

.field public static final sip_icon_at:I = 0x7f0201fc

.field public static final sip_icon_at_press:I = 0x7f0201fd

.field public static final sip_icon_attachment:I = 0x7f0201fe

.field public static final sip_icon_attachment_press:I = 0x7f0201ff

.field public static final sip_icon_bubble_clipboard:I = 0x7f020200

.field public static final sip_icon_bubble_del:I = 0x7f020201

.field public static final sip_icon_bubble_enter:I = 0x7f020202

.field public static final sip_icon_bubble_l_01_00:I = 0x7f020203

.field public static final sip_icon_bubble_l_01_01:I = 0x7f020204

.field public static final sip_icon_bubble_l_01_02:I = 0x7f020205

.field public static final sip_icon_bubble_l_01_03:I = 0x7f020206

.field public static final sip_icon_bubble_l_01_04:I = 0x7f020207

.field public static final sip_icon_bubble_l_01_05:I = 0x7f020208

.field public static final sip_icon_bubble_l_01_06:I = 0x7f020209

.field public static final sip_icon_bubble_l_01_07:I = 0x7f02020a

.field public static final sip_icon_bubble_l_01_08:I = 0x7f02020b

.field public static final sip_icon_bubble_l_01_09:I = 0x7f02020c

.field public static final sip_icon_bubble_l_02_00:I = 0x7f02020d

.field public static final sip_icon_bubble_l_02_01:I = 0x7f02020e

.field public static final sip_icon_bubble_l_02_02:I = 0x7f02020f

.field public static final sip_icon_bubble_l_02_03:I = 0x7f020210

.field public static final sip_icon_bubble_l_02_04:I = 0x7f020211

.field public static final sip_icon_bubble_l_02_05:I = 0x7f020212

.field public static final sip_icon_bubble_l_02_06:I = 0x7f020213

.field public static final sip_icon_bubble_l_02_07:I = 0x7f020214

.field public static final sip_icon_bubble_l_02_08:I = 0x7f020215

.field public static final sip_icon_bubble_l_02_09:I = 0x7f020216

.field public static final sip_icon_bubble_p_01_00:I = 0x7f020217

.field public static final sip_icon_bubble_p_01_01:I = 0x7f020218

.field public static final sip_icon_bubble_p_01_02:I = 0x7f020219

.field public static final sip_icon_bubble_p_01_03:I = 0x7f02021a

.field public static final sip_icon_bubble_p_01_04:I = 0x7f02021b

.field public static final sip_icon_bubble_p_01_05:I = 0x7f02021c

.field public static final sip_icon_bubble_p_01_06:I = 0x7f02021d

.field public static final sip_icon_bubble_p_01_07:I = 0x7f02021e

.field public static final sip_icon_bubble_p_01_08:I = 0x7f02021f

.field public static final sip_icon_bubble_p_01_09:I = 0x7f020220

.field public static final sip_icon_bubble_p_02_00:I = 0x7f020221

.field public static final sip_icon_bubble_p_02_01:I = 0x7f020222

.field public static final sip_icon_bubble_p_02_02:I = 0x7f020223

.field public static final sip_icon_bubble_p_02_03:I = 0x7f020224

.field public static final sip_icon_bubble_p_02_04:I = 0x7f020225

.field public static final sip_icon_bubble_p_02_05:I = 0x7f020226

.field public static final sip_icon_bubble_p_02_06:I = 0x7f020227

.field public static final sip_icon_bubble_p_02_07:I = 0x7f020228

.field public static final sip_icon_bubble_p_02_08:I = 0x7f020229

.field public static final sip_icon_bubble_p_02_09:I = 0x7f02022a

.field public static final sip_icon_bubble_search:I = 0x7f02022b

.field public static final sip_icon_bubble_setting:I = 0x7f02022c

.field public static final sip_icon_bubble_shift:I = 0x7f02022d

.field public static final sip_icon_bubble_space:I = 0x7f02022e

.field public static final sip_icon_bubble_v_01_00:I = 0x7f02022f

.field public static final sip_icon_bubble_v_01_01:I = 0x7f020230

.field public static final sip_icon_bubble_v_01_02:I = 0x7f020231

.field public static final sip_icon_bubble_v_01_03:I = 0x7f020232

.field public static final sip_icon_bubble_v_01_04:I = 0x7f020233

.field public static final sip_icon_bubble_v_01_05:I = 0x7f020234

.field public static final sip_icon_bubble_v_01_06:I = 0x7f020235

.field public static final sip_icon_bubble_v_01_07:I = 0x7f020236

.field public static final sip_icon_bubble_v_01_08:I = 0x7f020237

.field public static final sip_icon_bubble_v_01_09:I = 0x7f020238

.field public static final sip_icon_bubble_v_02_00:I = 0x7f020239

.field public static final sip_icon_bubble_v_02_01:I = 0x7f02023a

.field public static final sip_icon_bubble_v_02_02:I = 0x7f02023b

.field public static final sip_icon_bubble_v_02_03:I = 0x7f02023c

.field public static final sip_icon_bubble_v_02_04:I = 0x7f02023d

.field public static final sip_icon_bubble_v_02_05:I = 0x7f02023e

.field public static final sip_icon_bubble_v_02_06:I = 0x7f02023f

.field public static final sip_icon_bubble_v_02_07:I = 0x7f020240

.field public static final sip_icon_bubble_v_02_08:I = 0x7f020241

.field public static final sip_icon_bubble_v_02_09:I = 0x7f020242

.field public static final sip_icon_bubble_voice:I = 0x7f020243

.field public static final sip_icon_clipboard:I = 0x7f020244

.field public static final sip_icon_clipboard_press:I = 0x7f020245

.field public static final sip_icon_del:I = 0x7f020246

.field public static final sip_icon_del_press:I = 0x7f020247

.field public static final sip_icon_emoticon:I = 0x7f020248

.field public static final sip_icon_emoticon_press:I = 0x7f020249

.field public static final sip_icon_enter:I = 0x7f02024a

.field public static final sip_icon_enter_press:I = 0x7f02024b

.field public static final sip_icon_full_handwriting:I = 0x7f02024c

.field public static final sip_icon_full_handwriting_press:I = 0x7f02024d

.field public static final sip_icon_handwriting:I = 0x7f02024e

.field public static final sip_icon_handwriting_press:I = 0x7f02024f

.field public static final sip_icon_setting:I = 0x7f020250

.field public static final sip_icon_setting_press:I = 0x7f020251

.field public static final sip_icon_shift:I = 0x7f020252

.field public static final sip_icon_shift_onetab:I = 0x7f020253

.field public static final sip_icon_shift_press:I = 0x7f020254

.field public static final sip_icon_spece_normal:I = 0x7f020255

.field public static final sip_icon_spece_press:I = 0x7f020256

.field public static final sip_icon_sym_full:I = 0x7f020257

.field public static final sip_icon_sym_full_dim:I = 0x7f020258

.field public static final sip_icon_sym_half:I = 0x7f020259

.field public static final sip_icon_sym_half_dim:I = 0x7f02025a

.field public static final sip_icon_tones_toggle_key:I = 0x7f02025b

.field public static final sip_icon_tones_toggle_key_dim:I = 0x7f02025c

.field public static final sip_icon_tones_toggle_key_h:I = 0x7f02025d

.field public static final sip_icon_tones_toggle_key_h_dim:I = 0x7f02025e

.field public static final sip_icon_tones_toggle_key_h_press:I = 0x7f02025f

.field public static final sip_icon_tones_toggle_key_press:I = 0x7f020260

.field public static final sip_icon_voice:I = 0x7f020261

.field public static final sip_icon_voice_press:I = 0x7f020262

.field public static final sip_key_bg_invisible:I = 0x7f020263

.field public static final sip_key_bg_normal:I = 0x7f020264

.field public static final sip_key_bg_pressed:I = 0x7f020265

.field public static final sip_keyboard_bubble:I = 0x7f020266

.field public static final sip_keyboard_bubble_center:I = 0x7f020267

.field public static final sip_keyboard_bubble_email:I = 0x7f020268

.field public static final sip_keyboard_bubble_left:I = 0x7f020269

.field public static final sip_keyboard_bubble_right:I = 0x7f02026a

.field public static final sip_keyboard_bubble_url:I = 0x7f02026b

.field public static final sip_keyboard_helpbubble:I = 0x7f02026c

.field public static final sip_keyboard_umlaut_press:I = 0x7f02026d

.field public static final sip_more_tab_bg:I = 0x7f02026e

.field public static final sip_more_tab_bg_02:I = 0x7f02026f

.field public static final sip_popup_cancel_xml:I = 0x7f020270

.field public static final sip_popup_del_01_n:I = 0x7f020271

.field public static final sip_popup_del_n:I = 0x7f020272

.field public static final sip_popup_del_p:I = 0x7f020273

.field public static final sip_popup_pinch_zoom_01:I = 0x7f020274

.field public static final sip_popup_pinch_zoom_02:I = 0x7f020275

.field public static final sip_popup_pinch_zoom_03:I = 0x7f020276

.field public static final sip_popup_pinch_zoom_04:I = 0x7f020277

.field public static final sip_qwerty_bg:I = 0x7f020278

.field public static final sip_qwerty_bubble:I = 0x7f020279

.field public static final sip_qwerty_ming_popupbg:I = 0x7f02027a

.field public static final sip_tab_bg:I = 0x7f02027b

.field public static final sip_tab_bg_01:I = 0x7f02027c

.field public static final sip_tab_bg_01_center:I = 0x7f02027d

.field public static final sip_tab_bg_01_left:I = 0x7f02027e

.field public static final sip_tab_bg_01_right:I = 0x7f02027f

.field public static final sip_tab_bg_02:I = 0x7f020280

.field public static final sip_tab_bg_h:I = 0x7f020281

.field public static final sip_tab_edit_line:I = 0x7f020282

.field public static final sip_tab_icon_3x4:I = 0x7f020283

.field public static final sip_tab_icon_3x4_focus:I = 0x7f020284

.field public static final sip_tab_icon_add:I = 0x7f020285

.field public static final sip_tab_icon_clipboard:I = 0x7f020286

.field public static final sip_tab_icon_clipboard_focus:I = 0x7f020287

.field public static final sip_tab_icon_emoticon:I = 0x7f020288

.field public static final sip_tab_icon_emoticon_focus:I = 0x7f020289

.field public static final sip_tab_icon_full_hwr:I = 0x7f02028a

.field public static final sip_tab_icon_full_hwr_focus:I = 0x7f02028b

.field public static final sip_tab_icon_qwerty:I = 0x7f02028c

.field public static final sip_tab_icon_qwerty_focus:I = 0x7f02028d

.field public static final sip_tab_icon_remove:I = 0x7f02028e

.field public static final sip_tab_icon_remove_dim:I = 0x7f02028f

.field public static final sip_tab_line:I = 0x7f020290

.field public static final sip_tab_more:I = 0x7f020291

.field public static final sip_tab_more_bg:I = 0x7f020292

.field public static final sip_tab_more_press:I = 0x7f020293

.field public static final sip_tutorial_kepad_03:I = 0x7f020294

.field public static final sip_tutorial_kepad_04:I = 0x7f020295

.field public static final sip_tutorial_kepad_05:I = 0x7f020296

.field public static final sip_xt9_bg:I = 0x7f020297

.field public static final sip_xt9_dropdown_down:I = 0x7f020298

.field public static final sip_xt9_dropdown_down_press:I = 0x7f020299

.field public static final sip_xt9_dropdown_up:I = 0x7f02029a

.field public static final sip_xt9_dropdown_up_press:I = 0x7f02029b

.field public static final sip_xt9_expandable_bg:I = 0x7f02029c

.field public static final sip_xt9_off:I = 0x7f02029d

.field public static final sip_xt9_on:I = 0x7f02029e

.field public static final sip_xt9_split:I = 0x7f02029f

.field public static final sw_btn_round_more:I = 0x7f0202a0

.field public static final sw_btn_round_more_02:I = 0x7f0202a1

.field public static final sw_btn_round_more_disable:I = 0x7f0202a2

.field public static final sw_btn_round_more_disable_02:I = 0x7f0202a3

.field public static final sw_btn_round_more_disable_focused:I = 0x7f0202a4

.field public static final sw_btn_round_more_disable_focused_02:I = 0x7f0202a5

.field public static final sw_btn_round_more_disable_pressed:I = 0x7f0202a6

.field public static final sw_btn_round_more_disable_pressed_02:I = 0x7f0202a7

.field public static final sw_btn_round_more_focused:I = 0x7f0202a8

.field public static final sw_btn_round_more_focused_02:I = 0x7f0202a9

.field public static final sw_btn_round_more_pressed:I = 0x7f0202aa

.field public static final sw_btn_round_more_pressed_02:I = 0x7f0202ab

.field public static final sym_tab_view_bg:I = 0x7f0202ac

.field public static final tab_arrow_bg:I = 0x7f0202ad

.field public static final tab_arrow_left:I = 0x7f0202ae

.field public static final tab_arrow_right:I = 0x7f0202af

.field public static final textfield_disabled:I = 0x7f0202b0

.field public static final textinput_candidate_created_icon:I = 0x7f0202b1

.field public static final textinput_keypad_arrow_left:I = 0x7f0202b2

.field public static final textinput_keypad_arrow_right:I = 0x7f0202b3

.field public static final textinput_keypad_full_symbol:I = 0x7f0202b4

.field public static final textinput_keypad_half_symbol:I = 0x7f0202b5

.field public static final textinput_keypad_virtual_keypad:I = 0x7f0202b6

.field public static final textinput_letters:I = 0x7f0202b7

.field public static final textinput_onehand_arrow_left_n:I = 0x7f0202b8

.field public static final textinput_onehand_arrow_left_p:I = 0x7f0202b9

.field public static final textinput_onehand_arrow_right_n:I = 0x7f0202ba

.field public static final textinput_onehand_arrow_right_p:I = 0x7f0202bb

.field public static final textinput_panel:I = 0x7f0202bc

.field public static final textinput_panel_bg:I = 0x7f0202bd

.field public static final textinput_panel_bg_kor:I = 0x7f0202be

.field public static final textinput_panel_bg_sym:I = 0x7f0202bf

.field public static final textinput_predictive_01:I = 0x7f0202c0

.field public static final textinput_predictive_btn_close:I = 0x7f0202c1

.field public static final textinput_predictive_btn_close_d:I = 0x7f0202c2

.field public static final textinput_predictive_btn_close_floating:I = 0x7f0202c3

.field public static final textinput_predictive_btn_close_floating_p:I = 0x7f0202c4

.field public static final textinput_predictive_btn_close_p:I = 0x7f0202c5

.field public static final textinput_predictive_btn_more:I = 0x7f0202c6

.field public static final textinput_predictive_btn_more_d:I = 0x7f0202c7

.field public static final textinput_predictive_btn_more_floating:I = 0x7f0202c8

.field public static final textinput_predictive_btn_more_floating_p:I = 0x7f0202c9

.field public static final textinput_predictive_btn_more_p:I = 0x7f0202ca

.field public static final textinput_predictive_popup_bg:I = 0x7f0202cb

.field public static final textinput_predictive_popup_line:I = 0x7f0202cc

.field public static final textinput_predictive_popup_line_vertical:I = 0x7f0202cd

.field public static final textinput_predictive_popup_more_left:I = 0x7f0202ce

.field public static final textinput_predictive_popup_more_right:I = 0x7f0202cf

.field public static final textinput_predictive_popup_next:I = 0x7f0202d0

.field public static final textinput_predictive_popup_previous:I = 0x7f0202d1

.field public static final tollbar_icon_clipboard:I = 0x7f0202d2

.field public static final toolbar_background_dark:I = 0x7f0202d3

.field public static final toolbar_background_focused_dark:I = 0x7f0202d4

.field public static final toolbar_background_focused_light:I = 0x7f0202d5

.field public static final toolbar_background_light:I = 0x7f0202d6

.field public static final toolbar_background_pressed_dark:I = 0x7f0202d7

.field public static final toolbar_background_pressed_light:I = 0x7f0202d8

.field public static final toolbar_background_selected_dark:I = 0x7f0202d9

.field public static final toolbar_background_selected_light:I = 0x7f0202da

.field public static final toolbar_icon_clipboard_dim:I = 0x7f0202db

.field public static final toolbar_icon_floating:I = 0x7f0202dc

.field public static final toolbar_icon_floating_dim:I = 0x7f0202dd

.field public static final toolbar_icon_voice:I = 0x7f0202de

.field public static final toolbar_icon_voice_dim:I = 0x7f0202df

.field public static final toolbar_phonepad_key_icon_settings:I = 0x7f0202e0

.field public static final toolbar_phonepad_key_icon_settings_dim:I = 0x7f0202e1

.field public static final toolbar_qwerty_key_icon_keyboard:I = 0x7f0202e2

.field public static final toolbar_qwerty_key_icon_keyboard_dim:I = 0x7f0202e3

.field public static final toolbar_qwerty_key_icon_ocr:I = 0x7f0202e4

.field public static final toolbar_qwerty_key_icon_ocr_dim:I = 0x7f0202e5

.field public static final turorial:I = 0x7f0202e6

.field public static final tutorial_temp:I = 0x7f0202e7

.field public static final tw_divider_ab_holo_dark:I = 0x7f0202e8

.field public static final tw_divider_ab_holo_light:I = 0x7f0202e9

.field public static final voice_caution:I = 0x7f0202ea

.field public static final voice_ic_mic_dialog:I = 0x7f0202eb

.field public static final voice_ic_suggest_strip_microphone:I = 0x7f0202ec

.field public static final voice_ic_suggest_strip_microphone_swipe:I = 0x7f0202ed

.field public static final voice_ime_background:I = 0x7f0202ee

.field public static final voice_input_icon:I = 0x7f0202ef

.field public static final voice_input_icon_p:I = 0x7f0202f0

.field public static final voice_keyboard_suggest_strip:I = 0x7f0202f1

.field public static final voice_mic_slash:I = 0x7f0202f2

.field public static final voice_ok_cancel:I = 0x7f0202f3

.field public static final voice_speak_now_level0:I = 0x7f0202f4

.field public static final voice_speak_now_level1:I = 0x7f0202f5

.field public static final voice_speak_now_level2:I = 0x7f0202f6

.field public static final voice_speak_now_level3:I = 0x7f0202f7

.field public static final voice_speak_now_level4:I = 0x7f0202f8

.field public static final voice_speak_now_level5:I = 0x7f0202f9

.field public static final voice_speak_now_level6:I = 0x7f0202fa


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 619
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
