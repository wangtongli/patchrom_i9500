.class public final Lcom/samsung/inputmethod/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/inputmethod/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final Chinese:I = 0x7f0b0014

.field public static final English:I = 0x7f0b0015

.field public static final HK:I = 0x7f0b0016

.field public static final Korean:I = 0x7f0b0017

.field public static final TW:I = 0x7f0b0019

.field public static final accessibility_description_clipboard:I = 0x7f0b02a7

.field public static final accessibility_description_close:I = 0x7f0b02ac

.field public static final accessibility_description_delete:I = 0x7f0b029e

.field public static final accessibility_description_enter:I = 0x7f0b02a0

.field public static final accessibility_description_expand:I = 0x7f0b02ab

.field public static final accessibility_description_handwriting:I = 0x7f0b02a8

.field public static final accessibility_description_keyboard:I = 0x7f0b02a9

.field public static final accessibility_description_range_change_letters:I = 0x7f0b02a6

.field public static final accessibility_description_range_change_numbers:I = 0x7f0b02a5

.field public static final accessibility_description_range_change_symbols:I = 0x7f0b02a4

.field public static final accessibility_description_search:I = 0x7f0b02a1

.field public static final accessibility_description_settings:I = 0x7f0b029f

.field public static final accessibility_description_shift:I = 0x7f0b02a3

.field public static final accessibility_description_space:I = 0x7f0b02a2

.field public static final accessibility_description_voice:I = 0x7f0b02aa

.field public static final add:I = 0x7f0b002b

.field public static final alert:I = 0x7f0b002c

.field public static final attention:I = 0x7f0b026b

.field public static final browser:I = 0x7f0b002d

.field public static final call_assistant:I = 0x7f0b002e

.field public static final call_callback:I = 0x7f0b002f

.field public static final call_car:I = 0x7f0b0030

.field public static final call_company_main:I = 0x7f0b0031

.field public static final call_custom:I = 0x7f0b0032

.field public static final call_fax_home:I = 0x7f0b0033

.field public static final call_fax_work:I = 0x7f0b0034

.field public static final call_home:I = 0x7f0b0035

.field public static final call_isdn:I = 0x7f0b0036

.field public static final call_main:I = 0x7f0b0037

.field public static final call_mms:I = 0x7f0b0038

.field public static final call_mobile:I = 0x7f0b0039

.field public static final call_other:I = 0x7f0b003a

.field public static final call_other_fax:I = 0x7f0b003b

.field public static final call_pager:I = 0x7f0b003e

.field public static final call_radio:I = 0x7f0b003f

.field public static final call_telex:I = 0x7f0b0040

.field public static final call_tty_tdd:I = 0x7f0b0041

.field public static final call_work:I = 0x7f0b0042

.field public static final call_work_mobile:I = 0x7f0b0043

.field public static final call_work_pager:I = 0x7f0b0044

.field public static final cancel:I = 0x7f0b0045

.field public static final cand_udb_del:I = 0x7f0b0046

.field public static final checkbox_string:I = 0x7f0b0047

.field public static final clearall:I = 0x7f0b004b

.field public static final clearall_traditional:I = 0x7f0b004c

.field public static final commonphrase:I = 0x7f0b0048

.field public static final compare:I = 0x7f0b0049

.field public static final confirm:I = 0x7f0b004a

.field public static final delete:I = 0x7f0b004d

.field public static final delimiter:I = 0x7f0b004e

.field public static final delimiter_qwerty:I = 0x7f0b0050

.field public static final delimiter_tchinese:I = 0x7f0b004f

.field public static final dialog_title_list_preference:I = 0x7f0b0053

.field public static final edit:I = 0x7f0b0054

.field public static final email:I = 0x7f0b0055

.field public static final email_custom:I = 0x7f0b0056

.field public static final email_home:I = 0x7f0b0057

.field public static final email_mobile:I = 0x7f0b0058

.field public static final email_other:I = 0x7f0b0059

.field public static final email_work:I = 0x7f0b005a

.field public static final facebook_oauth_url:I = 0x7f0b000b

.field public static final fail_to_download:I = 0x7f0b0267

.field public static final fail_to_download_language_list:I = 0x7f0b0268

.field public static final fail_to_enable_language:I = 0x7f0b0269

.field public static final gmail_access_token_url:I = 0x7f0b0008

.field public static final gmail_authorize_url:I = 0x7f0b0009

.field public static final gmail_request_token_url:I = 0x7f0b0007

.field public static final gmail_user_info_url:I = 0x7f0b000a

.field public static final ime_mode_choose:I = 0x7f0b0010

.field public static final ime_name:I = 0x7f0b000f

.field public static final ime_setting_activity_package:I = 0x7f0b029d

.field public static final ime_settings_activity_name:I = 0x7f0b0011

.field public static final inputMethod:I = 0x7f0b0012

.field public static final inputString:I = 0x7f0b005c

.field public static final input_language_check:I = 0x7f0b005b

.field public static final keyboard_set_popup_pointer_down_description:I = 0x7f0b02bd

.field public static final keyboard_set_popup_pointer_up_description:I = 0x7f0b02bc

.field public static final keyboard_type_box_hw:I = 0x7f0b02ba

.field public static final keyboard_type_full_hw:I = 0x7f0b02b9

.field public static final keyboard_type_handwriting:I = 0x7f0b02bb

.field public static final keyboard_type_qwerty:I = 0x7f0b02ad

.field public static final keyboard_type_qwerty_cangjie:I = 0x7f0b02af

.field public static final keyboard_type_qwerty_cj:I = 0x7f0b02bf

.field public static final keyboard_type_qwerty_en:I = 0x7f0b02b1

.field public static final keyboard_type_qwerty_korean:I = 0x7f0b02b2

.field public static final keyboard_type_qwerty_pinyin:I = 0x7f0b02ae

.field public static final keyboard_type_qwerty_py:I = 0x7f0b02be

.field public static final keyboard_type_qwerty_zhuyin:I = 0x7f0b02b0

.field public static final keyboard_type_qwerty_zy:I = 0x7f0b02c0

.field public static final keyboard_type_stroke:I = 0x7f0b02b8

.field public static final keyboard_type_twelve_keys:I = 0x7f0b02b3

.field public static final keyboard_type_twelve_keys_en:I = 0x7f0b02b6

.field public static final keyboard_type_twelve_keys_korean:I = 0x7f0b02b7

.field public static final keyboard_type_twelve_keys_pinyin:I = 0x7f0b02b4

.field public static final keyboard_type_twelve_keys_py:I = 0x7f0b02c1

.field public static final keyboard_type_twelve_keys_zhuyin:I = 0x7f0b02b5

.field public static final keyboard_type_twelve_keys_zy:I = 0x7f0b02c2

.field public static final language:I = 0x7f0b0018

.field public static final language_list_update_complete:I = 0x7f0b026a

.field public static final later:I = 0x7f0b0263

.field public static final month_medium_april:I = 0x7f0b0212

.field public static final month_medium_august:I = 0x7f0b0216

.field public static final month_medium_december:I = 0x7f0b021a

.field public static final month_medium_february:I = 0x7f0b0210

.field public static final month_medium_january:I = 0x7f0b020f

.field public static final month_medium_july:I = 0x7f0b0215

.field public static final month_medium_june:I = 0x7f0b0214

.field public static final month_medium_march:I = 0x7f0b0211

.field public static final month_medium_may:I = 0x7f0b0213

.field public static final month_medium_november:I = 0x7f0b0219

.field public static final month_medium_october:I = 0x7f0b0218

.field public static final month_medium_september:I = 0x7f0b0217

.field public static final no_internet_connection:I = 0x7f0b0247

.field public static final ok:I = 0x7f0b005d

.field public static final opendial:I = 0x7f0b005e

.field public static final openmedia:I = 0x7f0b005f

.field public static final openphonebook:I = 0x7f0b0060

.field public static final operationcommand:I = 0x7f0b0061

.field public static final personalization_partner_tag:I = 0x7f0b0000

.field public static final personalization_server_base_url:I = 0x7f0b0001

.field public static final personalization_server_delete_url:I = 0x7f0b0002

.field public static final personalization_title:I = 0x7f0b0234

.field public static final personalize_already_running:I = 0x7f0b0250

.field public static final personalize_busy:I = 0x7f0b0259

.field public static final personalize_completed:I = 0x7f0b0254

.field public static final personalize_delete_failed:I = 0x7f0b024b

.field public static final personalize_delete_succeeded:I = 0x7f0b0249

.field public static final personalize_delremote_service_title:I = 0x7f0b0243

.field public static final personalize_delremote_subtitle:I = 0x7f0b0244

.field public static final personalize_delremote_title:I = 0x7f0b023a

.field public static final personalize_facebook_subtitle:I = 0x7f0b023b

.field public static final personalize_facebook_title:I = 0x7f0b0235

.field public static final personalize_failed:I = 0x7f0b024a

.field public static final personalize_finished:I = 0x7f0b025a

.field public static final personalize_gmail_subtitle:I = 0x7f0b023c

.field public static final personalize_gmail_title:I = 0x7f0b0237

.field public static final personalize_in_progress:I = 0x7f0b0253

.field public static final personalize_language_data_cleared:I = 0x7f0b024c

.field public static final personalize_language_data_not_cleared:I = 0x7f0b024d

.field public static final personalize_last_deleted:I = 0x7f0b0256

.field public static final personalize_last_personalised:I = 0x7f0b0255

.field public static final personalize_last_personalised_no_messages:I = 0x7f0b0261

.field public static final personalize_messaging_subtitle:I = 0x7f0b0240

.field public static final personalize_overloaded:I = 0x7f0b024e

.field public static final personalize_privacy_policy_content:I = 0x7f0b0246

.field public static final personalize_privacy_preference_title:I = 0x7f0b0245

.field public static final personalize_process_failed:I = 0x7f0b0257

.field public static final personalize_process_failed_no_date:I = 0x7f0b0258

.field public static final personalize_register_google_account_for_facebook:I = 0x7f0b024f

.field public static final personalize_sms_subtitle:I = 0x7f0b023f

.field public static final personalize_sms_title:I = 0x7f0b0239

.field public static final personalize_sources:I = 0x7f0b022c

.field public static final personalize_starting:I = 0x7f0b0251

.field public static final personalize_succeeded:I = 0x7f0b0248

.field public static final personalize_try_wifi:I = 0x7f0b0252

.field public static final personalize_twitter_subtitle:I = 0x7f0b023d

.field public static final personalize_twitter_title:I = 0x7f0b0238

.field public static final personalize_use_personalised_data_key:I = 0x7f0b022e

.field public static final personalize_use_personalised_data_summary:I = 0x7f0b0242

.field public static final personalize_use_personalised_data_title:I = 0x7f0b0241

.field public static final personalize_weibo_subtitle:I = 0x7f0b023e

.field public static final personalize_weibo_title:I = 0x7f0b0236

.field public static final points_empty:I = 0x7f0b0062

.field public static final portrait_mode_1:I = 0x7f0b0066

.field public static final portrait_mode_2:I = 0x7f0b0067

.field public static final portrait_mode_3:I = 0x7f0b0068

.field public static final portrait_mode_4:I = 0x7f0b0069

.field public static final predictive_text_title:I = 0x7f0b025f

.field public static final pref_clear_personalised_data_dialog_msg:I = 0x7f0b025d

.field public static final pref_clear_personalised_data_key:I = 0x7f0b0228

.field public static final pref_clear_personalised_data_summary:I = 0x7f0b025c

.field public static final pref_clear_personalised_data_title:I = 0x7f0b025b

.field public static final pref_personalize_contacts:I = 0x7f0b022a

.field public static final pref_personalize_delremote:I = 0x7f0b022b

.field public static final pref_personalize_facebook:I = 0x7f0b0231

.field public static final pref_personalize_gmail:I = 0x7f0b0230

.field public static final pref_personalize_menu_title:I = 0x7f0b0233

.field public static final pref_personalize_sms:I = 0x7f0b0229

.field public static final pref_personalize_sources_category_key:I = 0x7f0b022d

.field public static final pref_personalize_twitter:I = 0x7f0b0232

.field public static final pref_personalize_weibo:I = 0x7f0b022f

.field public static final pref_predictive_disable_tip:I = 0x7f0b0260

.field public static final s_month_1:I = 0x7f0b006a

.field public static final s_month_10:I = 0x7f0b006b

.field public static final s_month_11:I = 0x7f0b006c

.field public static final s_month_12:I = 0x7f0b006d

.field public static final s_month_2:I = 0x7f0b006e

.field public static final s_month_3:I = 0x7f0b006f

.field public static final s_month_4:I = 0x7f0b0070

.field public static final s_month_5:I = 0x7f0b0071

.field public static final s_month_6:I = 0x7f0b0072

.field public static final s_month_7:I = 0x7f0b0073

.field public static final s_month_8:I = 0x7f0b0074

.field public static final s_month_9:I = 0x7f0b0075

.field public static final samsungime_help:I = 0x7f0b0076

.field public static final samsungime_help_summary:I = 0x7f0b0077

.field public static final samsungime_help_summary_key:I = 0x7f0b013d

.field public static final saveadd:I = 0x7f0b0078

.field public static final savemodify:I = 0x7f0b0079

.field public static final selectlanguage:I = 0x7f0b007a

.field public static final selectorder:I = 0x7f0b007b

.field public static final self_define_off:I = 0x7f0b007c

.field public static final setting:I = 0x7f0b007d

.field public static final setting_about_Category_key:I = 0x7f0b013e

.field public static final setting_about_key:I = 0x7f0b013f

.field public static final setting_about_title:I = 0x7f0b007e

.field public static final setting_about_unknown:I = 0x7f0b007f

.field public static final setting_adapt_define_key:I = 0x7f0b0140

.field public static final setting_adapt_define_title:I = 0x7f0b0080

.field public static final setting_adapt_key:I = 0x7f0b0141

.field public static final setting_adapt_title:I = 0x7f0b0081

.field public static final setting_advanced:I = 0x7f0b0082

.field public static final setting_advanced_key:I = 0x7f0b0142

.field public static final setting_advanced_summary_key:I = 0x7f0b0143

.field public static final setting_auto_capitalization_key:I = 0x7f0b0144

.field public static final setting_auto_capitalization_summary:I = 0x7f0b0083

.field public static final setting_auto_capitalization_title:I = 0x7f0b0084

.field public static final setting_auto_fullstop_key:I = 0x7f0b0145

.field public static final setting_auto_fullstop_summary:I = 0x7f0b0085

.field public static final setting_auto_fullstop_title:I = 0x7f0b0086

.field public static final setting_char_preview_key:I = 0x7f0b0146

.field public static final setting_char_preview_title:I = 0x7f0b0087

.field public static final setting_character_preview_key:I = 0x7f0b0147

.field public static final setting_character_preview_title:I = 0x7f0b0088

.field public static final setting_chinese_association_key:I = 0x7f0b0148

.field public static final setting_chinese_association_title:I = 0x7f0b02c4

.field public static final setting_clean_user_data_confirm:I = 0x7f0b0089

.field public static final setting_clean_user_data_key:I = 0x7f0b0149

.field public static final setting_clean_user_data_title:I = 0x7f0b008a

.field public static final setting_convert_key:I = 0x7f0b014a

.field public static final setting_convert_title:I = 0x7f0b008b

.field public static final setting_db_update_action:I = 0x7f0b008d

.field public static final setting_db_update_action_key:I = 0x7f0b014c

.field public static final setting_db_update_auto_key:I = 0x7f0b014d

.field public static final setting_db_update_auto_title:I = 0x7f0b008e

.field public static final setting_db_update_auto_wlan_key:I = 0x7f0b014e

.field public static final setting_db_update_auto_wlan_summary:I = 0x7f0b0090

.field public static final setting_db_update_auto_wlan_title:I = 0x7f0b008f

.field public static final setting_db_update_cancel:I = 0x7f0b00a1

.field public static final setting_db_update_category_key:I = 0x7f0b0151

.field public static final setting_db_update_category_title:I = 0x7f0b0094

.field public static final setting_db_update_cdb_entertainment:I = 0x7f0b0098

.field public static final setting_db_update_cdb_sports:I = 0x7f0b0099

.field public static final setting_db_update_charge_attention_msg1:I = 0x7f0b00a3

.field public static final setting_db_update_charge_attention_msg2:I = 0x7f0b00a4

.field public static final setting_db_update_charge_attention_title:I = 0x7f0b00a2

.field public static final setting_db_update_connect_fail:I = 0x7f0b00a5

.field public static final setting_db_update_download_failed:I = 0x7f0b00a6

.field public static final setting_db_update_download_finished:I = 0x7f0b00a7

.field public static final setting_db_update_download_timeout:I = 0x7f0b00a8

.field public static final setting_db_update_downloading:I = 0x7f0b00a9

.field public static final setting_db_update_english_dict_key:I = 0x7f0b0155

.field public static final setting_db_update_english_dict_title:I = 0x7f0b009d

.field public static final setting_db_update_hkchinese_dict_key:I = 0x7f0b0153

.field public static final setting_db_update_hkchinese_dict_title:I = 0x7f0b009b

.field public static final setting_db_update_hotwords:I = 0x7f0b0095

.field public static final setting_db_update_key:I = 0x7f0b014b

.field public static final setting_db_update_last_update_time:I = 0x7f0b00a0

.field public static final setting_db_update_more_hotwords:I = 0x7f0b0096

.field public static final setting_db_update_network_down:I = 0x7f0b00aa

.field public static final setting_db_update_no_network_msg:I = 0x7f0b00ac

.field public static final setting_db_update_no_network_title:I = 0x7f0b00ab

.field public static final setting_db_update_no_selection:I = 0x7f0b00ad

.field public static final setting_db_update_notice_key:I = 0x7f0b0150

.field public static final setting_db_update_notice_summary:I = 0x7f0b0092

.field public static final setting_db_update_notice_title:I = 0x7f0b0091

.field public static final setting_db_update_popular_dict_key:I = 0x7f0b0156

.field public static final setting_db_update_popular_dict_title:I = 0x7f0b009e

.field public static final setting_db_update_reset:I = 0x7f0b00ae

.field public static final setting_db_update_restore_default:I = 0x7f0b009f

.field public static final setting_db_update_restore_key:I = 0x7f0b0157

.field public static final setting_db_update_restore_to_default_confirm:I = 0x7f0b00af

.field public static final setting_db_update_retry:I = 0x7f0b00b0

.field public static final setting_db_update_schinese_dict_key:I = 0x7f0b0154

.field public static final setting_db_update_schinese_dict_title:I = 0x7f0b009c

.field public static final setting_db_update_setting:I = 0x7f0b0093

.field public static final setting_db_update_setting_key:I = 0x7f0b014f

.field public static final setting_db_update_sogou_hotwords:I = 0x7f0b0097

.field public static final setting_db_update_title:I = 0x7f0b008c

.field public static final setting_db_update_twchinese_dict_key:I = 0x7f0b0152

.field public static final setting_db_update_twchinese_dict_title:I = 0x7f0b009a

.field public static final setting_db_update_version_notice:I = 0x7f0b00b1

.field public static final setting_db_update_version_same:I = 0x7f0b00b2

.field public static final setting_define_content_key:I = 0x7f0b0158

.field public static final setting_define_content_titile:I = 0x7f0b00b3

.field public static final setting_define_key:I = 0x7f0b0159

.field public static final setting_define_key_title:I = 0x7f0b00b4

.field public static final setting_define_main_key:I = 0x7f0b015a

.field public static final setting_disabled:I = 0x7f0b00b5

.field public static final setting_enabled:I = 0x7f0b00b6

.field public static final setting_feedback_Category_key:I = 0x7f0b015b

.field public static final setting_feedback_title:I = 0x7f0b00b7

.field public static final setting_fuzzy_pinyin_ang_an_key:I = 0x7f0b015c

.field public static final setting_fuzzy_pinyin_ang_an_title:I = 0x7f0b00b8

.field public static final setting_fuzzy_pinyin_ch_c_key:I = 0x7f0b015d

.field public static final setting_fuzzy_pinyin_ch_c_title:I = 0x7f0b021b

.field public static final setting_fuzzy_pinyin_eng_en_key:I = 0x7f0b015e

.field public static final setting_fuzzy_pinyin_eng_en_title:I = 0x7f0b021c

.field public static final setting_fuzzy_pinyin_h_f_key:I = 0x7f0b015f

.field public static final setting_fuzzy_pinyin_h_f_title:I = 0x7f0b021d

.field public static final setting_fuzzy_pinyin_h_l_key:I = 0x7f0b0160

.field public static final setting_fuzzy_pinyin_h_l_title:I = 0x7f0b021e

.field public static final setting_fuzzy_pinyin_iang_ian_key:I = 0x7f0b0161

.field public static final setting_fuzzy_pinyin_iang_ian_title:I = 0x7f0b021f

.field public static final setting_fuzzy_pinyin_ing_in_key:I = 0x7f0b0162

.field public static final setting_fuzzy_pinyin_ing_in_title:I = 0x7f0b0220

.field public static final setting_fuzzy_pinyin_input_key:I = 0x7f0b0163

.field public static final setting_fuzzy_pinyin_input_title:I = 0x7f0b0221

.field public static final setting_fuzzy_pinyin_k_g_key:I = 0x7f0b0164

.field public static final setting_fuzzy_pinyin_k_g_title:I = 0x7f0b0222

.field public static final setting_fuzzy_pinyin_n_l_key:I = 0x7f0b0165

.field public static final setting_fuzzy_pinyin_n_l_title:I = 0x7f0b0223

.field public static final setting_fuzzy_pinyin_r_l_key:I = 0x7f0b0166

.field public static final setting_fuzzy_pinyin_r_l_title:I = 0x7f0b0224

.field public static final setting_fuzzy_pinyin_sh_s_key:I = 0x7f0b0167

.field public static final setting_fuzzy_pinyin_sh_s_title:I = 0x7f0b0225

.field public static final setting_fuzzy_pinyin_uang_uan_key:I = 0x7f0b0168

.field public static final setting_fuzzy_pinyin_uang_uan_title:I = 0x7f0b0226

.field public static final setting_fuzzy_pinyin_zh_z_key:I = 0x7f0b0169

.field public static final setting_fuzzy_pinyin_zh_z_title:I = 0x7f0b0227

.field public static final setting_general_title:I = 0x7f0b00b9

.field public static final setting_handwrite:I = 0x7f0b00ba

.field public static final setting_handwrite_color_key:I = 0x7f0b016c

.field public static final setting_handwrite_color_main_key:I = 0x7f0b016d

.field public static final setting_handwrite_color_titile:I = 0x7f0b00bb

.field public static final setting_handwrite_line_key:I = 0x7f0b016e

.field public static final setting_handwrite_line_titile:I = 0x7f0b00bc

.field public static final setting_handwrite_linegradient_color_end:I = 0x7f0b00bd

.field public static final setting_handwrite_linegradient_color_key:I = 0x7f0b016f

.field public static final setting_handwrite_linegradient_color_middle:I = 0x7f0b00be

.field public static final setting_handwrite_linegradient_color_start:I = 0x7f0b00bf

.field public static final setting_handwrite_linegradient_color_titile:I = 0x7f0b00c0

.field public static final setting_handwrite_linegradient_en_key:I = 0x7f0b0170

.field public static final setting_handwrite_linegradient_titile:I = 0x7f0b00c1

.field public static final setting_handwrite_speed_key:I = 0x7f0b0171

.field public static final setting_handwrite_speed_titile:I = 0x7f0b00c2

.field public static final setting_handwrite_summary:I = 0x7f0b00c4

.field public static final setting_handwrite_summary_key:I = 0x7f0b0172

.field public static final setting_handwrite_type_key:I = 0x7f0b0173

.field public static final setting_handwrite_type_title:I = 0x7f0b02c7

.field public static final setting_handwriting_mode_key:I = 0x7f0b0174

.field public static final setting_handwriting_mode_title:I = 0x7f0b00c5

.field public static final setting_handwriting_style_caoshu:I = 0x7f0b00ca

.field public static final setting_handwriting_style_huaisu:I = 0x7f0b00c9

.field public static final setting_handwriting_style_huangtj:I = 0x7f0b00c7

.field public static final setting_handwriting_style_huizong:I = 0x7f0b00c6

.field public static final setting_handwriting_style_key:I = 0x7f0b0175

.field public static final setting_handwriting_style_titile:I = 0x7f0b00c3

.field public static final setting_handwriting_style_wangxz:I = 0x7f0b00c8

.field public static final setting_key:I = 0x7f0b0176

.field public static final setting_keyboard_swipe_key:I = 0x7f0b0178

.field public static final setting_keyboard_swipe_key_title:I = 0x7f0b0179

.field public static final setting_keyboard_swipe_no_action_title:I = 0x7f0b017d

.field public static final setting_keyboard_swipe_pointing_sweep_title:I = 0x7f0b017a

.field public static final setting_keyboard_swipe_pointingkeyboard_title:I = 0x7f0b017b

.field public static final setting_keyboard_swipe_sweep_keyboard_title:I = 0x7f0b017c

.field public static final setting_keypad_type_key:I = 0x7f0b0177

.field public static final setting_keypad_type_title:I = 0x7f0b00cb

.field public static final setting_language:I = 0x7f0b00cc

.field public static final setting_language_chinese:I = 0x7f0b00cd

.field public static final setting_language_chinese_hk:I = 0x7f0b00ce

.field public static final setting_language_chinese_hk_title:I = 0x7f0b00cf

.field public static final setting_language_chinese_title:I = 0x7f0b00d0

.field public static final setting_language_chinese_tw:I = 0x7f0b00d1

.field public static final setting_language_chinese_tw_title:I = 0x7f0b00d2

.field public static final setting_language_english:I = 0x7f0b00d3

.field public static final setting_language_english_title:I = 0x7f0b00d4

.field public static final setting_language_korean:I = 0x7f0b00d5

.field public static final setting_language_korean_title:I = 0x7f0b00d6

.field public static final setting_language_summary:I = 0x7f0b00d7

.field public static final setting_language_summary_key:I = 0x7f0b016a

.field public static final setting_language_symplify_chinese_title:I = 0x7f0b00d8

.field public static final setting_link_to_contacts_key:I = 0x7f0b016b

.field public static final setting_link_to_contacts_summary:I = 0x7f0b00d9

.field public static final setting_link_to_contacts_title:I = 0x7f0b00da

.field public static final setting_one_hand_mode_key:I = 0x7f0b00dc

.field public static final setting_one_hand_mode_summary:I = 0x7f0b00de

.field public static final setting_one_hand_mode_title:I = 0x7f0b00dd

.field public static final setting_others_key:I = 0x7f0b017e

.field public static final setting_others_title:I = 0x7f0b00db

.field public static final setting_peroid_shortcut_summary:I = 0x7f0b0064

.field public static final setting_peroid_shortcut_title:I = 0x7f0b0063

.field public static final setting_pointing_keyboard_key:I = 0x7f0b017f

.field public static final setting_pointing_keyboard_summary:I = 0x7f0b00df

.field public static final setting_pointing_keyboard_title:I = 0x7f0b00e0

.field public static final setting_portrait_chinese_input_mode_key:I = 0x7f0b0180

.field public static final setting_portrait_english_input_mode_key:I = 0x7f0b0181

.field public static final setting_portrait_hongkong_input_mode_key:I = 0x7f0b0182

.field public static final setting_portrait_input_mode_chinese_title:I = 0x7f0b00e1

.field public static final setting_portrait_input_mode_chinese_twhk_title:I = 0x7f0b00e2

.field public static final setting_portrait_input_mode_english_title:I = 0x7f0b00e3

.field public static final setting_portrait_input_mode_hongkong_title:I = 0x7f0b00e4

.field public static final setting_portrait_input_mode_key:I = 0x7f0b02c3

.field public static final setting_portrait_input_mode_korean_title:I = 0x7f0b00e5

.field public static final setting_portrait_input_mode_summary:I = 0x7f0b0065

.field public static final setting_portrait_input_mode_taiwan_title:I = 0x7f0b00e6

.field public static final setting_portrait_input_mode_title:I = 0x7f0b02c5

.field public static final setting_portrait_korean_input_mode_key:I = 0x7f0b0183

.field public static final setting_portrait_taiwan_input_mode_key:I = 0x7f0b0184

.field public static final setting_prediction_key:I = 0x7f0b0185

.field public static final setting_prediction_title:I = 0x7f0b00e7

.field public static final setting_predictive_text:I = 0x7f0b00e8

.field public static final setting_predictive_text_key:I = 0x7f0b0186

.field public static final setting_qwerty_correct_error_key:I = 0x7f0b0187

.field public static final setting_qwerty_correct_summary:I = 0x7f0b00e9

.field public static final setting_qwerty_correct_title:I = 0x7f0b00ea

.field public static final setting_reset_confirm:I = 0x7f0b00eb

.field public static final setting_reset_setting_key:I = 0x7f0b0188

.field public static final setting_reset_setting_title:I = 0x7f0b00ec

.field public static final setting_simple:I = 0x7f0b00ed

.field public static final setting_simple_traditional:I = 0x7f0b00ee

.field public static final setting_sound_key:I = 0x7f0b0189

.field public static final setting_sound_key_title:I = 0x7f0b00ef

.field public static final setting_sound_keypress_key:I = 0x7f0b018a

.field public static final setting_sound_keypress_title:I = 0x7f0b00f0

.field public static final setting_space_input_key:I = 0x7f0b018b

.field public static final setting_space_input_title:I = 0x7f0b00f1

.field public static final setting_space_key_option:I = 0x7f0b02c6

.field public static final setting_space_key_option_association:I = 0x7f0b02c9

.field public static final setting_space_key_option_association_title:I = 0x7f0b02cb

.field public static final setting_space_key_option_candidate:I = 0x7f0b02c8

.field public static final setting_space_key_option_candidate_title:I = 0x7f0b02ca

.field public static final setting_space_key_option_key:I = 0x7f0b018c

.field public static final setting_traditional:I = 0x7f0b00f2

.field public static final setting_version_title:I = 0x7f0b00f3

.field public static final setting_vibrate_key:I = 0x7f0b018d

.field public static final setting_vibrate_keypress_key:I = 0x7f0b018e

.field public static final setting_vibrate_keypress_title:I = 0x7f0b00f4

.field public static final setting_vibrate_title:I = 0x7f0b00f5

.field public static final setting_voice_input_key:I = 0x7f0b018f

.field public static final setting_voice_input_summary:I = 0x7f0b00f6

.field public static final setting_voice_input_title:I = 0x7f0b00f7

.field public static final startcalculator:I = 0x7f0b00f8

.field public static final string_empty:I = 0x7f0b00f9

.field public static final string_exist:I = 0x7f0b00fa

.field public static final successfully_download:I = 0x7f0b0266

.field public static final symbol_chinese:I = 0x7f0b00fb

.field public static final symbol_normal_list:I = 0x7f0b0025

.field public static final symbol_unified:I = 0x7f0b00fc

.field public static final tabEdit_Toast_Notification:I = 0x7f0b0298

.field public static final tab_edit_list_item_0:I = 0x7f0b026d

.field public static final tab_edit_list_item_1:I = 0x7f0b026e

.field public static final tab_edit_list_item_10:I = 0x7f0b0277

.field public static final tab_edit_list_item_11:I = 0x7f0b0278

.field public static final tab_edit_list_item_12:I = 0x7f0b0279

.field public static final tab_edit_list_item_13:I = 0x7f0b027a

.field public static final tab_edit_list_item_14:I = 0x7f0b027b

.field public static final tab_edit_list_item_15:I = 0x7f0b027c

.field public static final tab_edit_list_item_16:I = 0x7f0b027d

.field public static final tab_edit_list_item_17:I = 0x7f0b027e

.field public static final tab_edit_list_item_18:I = 0x7f0b027f

.field public static final tab_edit_list_item_19:I = 0x7f0b0280

.field public static final tab_edit_list_item_2:I = 0x7f0b026f

.field public static final tab_edit_list_item_20:I = 0x7f0b0281

.field public static final tab_edit_list_item_21:I = 0x7f0b0282

.field public static final tab_edit_list_item_22:I = 0x7f0b0283

.field public static final tab_edit_list_item_23:I = 0x7f0b0284

.field public static final tab_edit_list_item_24:I = 0x7f0b0285

.field public static final tab_edit_list_item_25:I = 0x7f0b0286

.field public static final tab_edit_list_item_3:I = 0x7f0b0270

.field public static final tab_edit_list_item_4:I = 0x7f0b0271

.field public static final tab_edit_list_item_5:I = 0x7f0b0272

.field public static final tab_edit_list_item_6:I = 0x7f0b0273

.field public static final tab_edit_list_item_7:I = 0x7f0b0274

.field public static final tab_edit_list_item_8:I = 0x7f0b0275

.field public static final tab_edit_list_item_9:I = 0x7f0b0276

.field public static final tab_edit_textview:I = 0x7f0b0296

.field public static final tab_list_tag_others:I = 0x7f0b0289

.field public static final tab_list_tag_simplified:I = 0x7f0b0288

.field public static final tab_list_tag_tools:I = 0x7f0b028a

.field public static final tab_list_tag_traditional:I = 0x7f0b0287

.field public static final tabedit_toolbar_title:I = 0x7f0b0297

.field public static final tabtitle_123_symbol:I = 0x7f0b028c

.field public static final tabtitle_cangjie:I = 0x7f0b0292

.field public static final tabtitle_emoticon:I = 0x7f0b028b

.field public static final tabtitle_hw_chn:I = 0x7f0b028d

.field public static final tabtitle_hw_eng:I = 0x7f0b028e

.field public static final tabtitle_hw_hk:I = 0x7f0b0290

.field public static final tabtitle_hw_kor:I = 0x7f0b028f

.field public static final tabtitle_hw_tw:I = 0x7f0b0291

.field public static final tabtitle_stroke_hk:I = 0x7f0b0294

.field public static final tabtitle_stroke_tw:I = 0x7f0b0295

.field public static final tabtitle_zhuyin:I = 0x7f0b0293

.field public static final tip_to_move_to_next_page:I = 0x7f0b00fd

.field public static final tip_to_move_to_prev_page:I = 0x7f0b00fe

.field public static final tip_to_open_keyboard:I = 0x7f0b00ff

.field public static final tip_to_start_typing:I = 0x7f0b0100

.field public static final tip_to_switch_ime_type:I = 0x7f0b0101

.field public static final tip_to_switch_symbol_type:I = 0x7f0b0102

.field public static final tip_to_switch_to_12key:I = 0x7f0b0103

.field public static final tip_to_switch_to_bhandwrite:I = 0x7f0b0104

.field public static final tip_to_switch_to_fhandwrite:I = 0x7f0b0105

.field public static final tip_to_switch_to_qwerty:I = 0x7f0b0106

.field public static final title_setting_handwriting_mode:I = 0x7f0b0107

.field public static final toast_track_invalid:I = 0x7f0b0108

.field public static final toggle_123_sym_one_line:I = 0x7f0b0190

.field public static final toggle_123mode_off:I = 0x7f0b0191

.field public static final toggle_123mode_on:I = 0x7f0b0192

.field public static final toggle_34num_123:I = 0x7f0b020b

.field public static final toggle_34num_keyboard:I = 0x7f0b020c

.field public static final toggle_cangjie_34:I = 0x7f0b0193

.field public static final toggle_cangjie_34_value:I = 0x7f0b0194

.field public static final toggle_cangjie_quick_qw:I = 0x7f0b0195

.field public static final toggle_cangjie_quick_qw_value:I = 0x7f0b0196

.field public static final toggle_cangjie_qw:I = 0x7f0b0197

.field public static final toggle_cangjie_qw_value:I = 0x7f0b0198

.field public static final toggle_cangjie_stroke_34:I = 0x7f0b0199

.field public static final toggle_cangjie_stroke_34_value:I = 0x7f0b019a

.field public static final toggle_cn_cand:I = 0x7f0b019b

.field public static final toggle_email_symbol:I = 0x7f0b019c

.field public static final toggle_enter_done:I = 0x7f0b019d

.field public static final toggle_enter_go:I = 0x7f0b019e

.field public static final toggle_enter_next:I = 0x7f0b019f

.field public static final toggle_enter_search:I = 0x7f0b01a0

.field public static final toggle_enter_send:I = 0x7f0b01a1

.field public static final toggle_handwrite_abc_mode:I = 0x7f0b0206

.field public static final toggle_handwrite_chn_mode:I = 0x7f0b0208

.field public static final toggle_handwrite_hk_mode:I = 0x7f0b0209

.field public static final toggle_handwrite_kor_mode:I = 0x7f0b0207

.field public static final toggle_handwrite_tw_mode:I = 0x7f0b020a

.field public static final toggle_hwr_box_pointing_off:I = 0x7f0b020e

.field public static final toggle_hwr_box_pointing_on:I = 0x7f0b020d

.field public static final toggle_language_cn:I = 0x7f0b01a2

.field public static final toggle_language_cn_hk:I = 0x7f0b01a3

.field public static final toggle_language_cn_qk_hk:I = 0x7f0b01a4

.field public static final toggle_language_cn_tw:I = 0x7f0b01a5

.field public static final toggle_language_en:I = 0x7f0b01a6

.field public static final toggle_language_ko:I = 0x7f0b01a7

.field public static final toggle_lowercase:I = 0x7f0b01a8

.field public static final toggle_mm_clipboard:I = 0x7f0b01a9

.field public static final toggle_mm_clipboard_voice:I = 0x7f0b01b2

.field public static final toggle_mm_comma:I = 0x7f0b01ae

.field public static final toggle_mm_comma_voice:I = 0x7f0b01b6

.field public static final toggle_mm_exclamation:I = 0x7f0b01b0

.field public static final toggle_mm_exclamation_voice:I = 0x7f0b01b8

.field public static final toggle_mm_floating:I = 0x7f0b01ba

.field public static final toggle_mm_floating_voice:I = 0x7f0b01bb

.field public static final toggle_mm_handwriting:I = 0x7f0b01aa

.field public static final toggle_mm_handwriting_voice:I = 0x7f0b01b3

.field public static final toggle_mm_ocr:I = 0x7f0b01ab

.field public static final toggle_mm_ocr_voice:I = 0x7f0b01b4

.field public static final toggle_mm_question:I = 0x7f0b01af

.field public static final toggle_mm_question_voice:I = 0x7f0b01b7

.field public static final toggle_mm_quotation:I = 0x7f0b01b1

.field public static final toggle_mm_quotation_voice:I = 0x7f0b01b9

.field public static final toggle_mm_setting:I = 0x7f0b01ac

.field public static final toggle_mm_setting_voice:I = 0x7f0b01b5

.field public static final toggle_mm_voice:I = 0x7f0b01ad

.field public static final toggle_phone_sym:I = 0x7f0b01bc

.field public static final toggle_pinyin_34:I = 0x7f0b01bd

.field public static final toggle_row_cn:I = 0x7f0b01be

.field public static final toggle_row_cn_hk:I = 0x7f0b01bf

.field public static final toggle_row_cn_hk_no_voice:I = 0x7f0b01c0

.field public static final toggle_row_cn_no_voice:I = 0x7f0b01c1

.field public static final toggle_row_cn_tw:I = 0x7f0b01c2

.field public static final toggle_row_cn_tw_no_voice:I = 0x7f0b01c3

.field public static final toggle_row_email:I = 0x7f0b01c4

.field public static final toggle_row_email_no_voice:I = 0x7f0b0029

.field public static final toggle_row_en:I = 0x7f0b01c5

.field public static final toggle_row_en_no_voice:I = 0x7f0b01c6

.field public static final toggle_row_ko:I = 0x7f0b01c7

.field public static final toggle_row_ko_no_voice:I = 0x7f0b01c8

.field public static final toggle_row_url:I = 0x7f0b01c9

.field public static final toggle_row_url_no_voice:I = 0x7f0b002a

.field public static final toggle_space_cn:I = 0x7f0b01ca

.field public static final toggle_space_cn_hk:I = 0x7f0b01cb

.field public static final toggle_space_cn_hk_vaule:I = 0x7f0b01cc

.field public static final toggle_space_cn_tw:I = 0x7f0b01cd

.field public static final toggle_space_cn_tw_value:I = 0x7f0b01ce

.field public static final toggle_space_cn_value:I = 0x7f0b01cf

.field public static final toggle_space_en:I = 0x7f0b01d0

.field public static final toggle_space_en_value:I = 0x7f0b01d1

.field public static final toggle_space_ko:I = 0x7f0b01d2

.field public static final toggle_space_ko_value:I = 0x7f0b01d3

.field public static final toggle_spcae_fs:I = 0x7f0b01d4

.field public static final toggle_stroke_34:I = 0x7f0b01d5

.field public static final toggle_symbol_cangjie:I = 0x7f0b01d6

.field public static final toggle_symbol_english:I = 0x7f0b01d7

.field public static final toggle_symbol_korean:I = 0x7f0b01d8

.field public static final toggle_symbol_lock:I = 0x7f0b01d9

.field public static final toggle_symbol_pinyin:I = 0x7f0b01da

.field public static final toggle_symbol_stroke:I = 0x7f0b01db

.field public static final toggle_symbol_unlock:I = 0x7f0b01dc

.field public static final toggle_symbol_zhuyin:I = 0x7f0b01dd

.field public static final toggle_t9:I = 0x7f0b01de

.field public static final toggle_templowercase:I = 0x7f0b01df

.field public static final toggle_tone:I = 0x7f0b01e0

.field public static final toggle_toolbar_abc_lower_hl:I = 0x7f0b01e1

.field public static final toggle_toolbar_abc_lower_normal:I = 0x7f0b01e2

.field public static final toggle_toolbar_abc_tempupper_hl:I = 0x7f0b01e3

.field public static final toggle_toolbar_abc_tempupper_normal:I = 0x7f0b01e4

.field public static final toggle_toolbar_abc_upper_hl:I = 0x7f0b01e5

.field public static final toggle_toolbar_abc_upper_normal:I = 0x7f0b01e6

.field public static final toggle_toolbar_chn_pinyin_hl:I = 0x7f0b01e7

.field public static final toggle_toolbar_chn_pinyin_normal:I = 0x7f0b01e8

.field public static final toggle_toolbar_chn_stroke_hl:I = 0x7f0b01e9

.field public static final toggle_toolbar_chn_stroke_normal:I = 0x7f0b01ea

.field public static final toggle_toolbar_en_lower_hl:I = 0x7f0b01eb

.field public static final toggle_toolbar_en_lower_normal:I = 0x7f0b01ec

.field public static final toggle_toolbar_en_tempupper_hl:I = 0x7f0b01ed

.field public static final toggle_toolbar_en_tempupper_normal:I = 0x7f0b01ee

.field public static final toggle_toolbar_en_upper_hl:I = 0x7f0b01ef

.field public static final toggle_toolbar_en_upper_normal:I = 0x7f0b01f0

.field public static final toggle_toolbar_input_eng_only:I = 0x7f0b01f1

.field public static final toggle_toolbar_input_language_both:I = 0x7f0b01f2

.field public static final toggle_toolbar_input_language_chn:I = 0x7f0b01f3

.field public static final toggle_toolbar_input_language_kor:I = 0x7f0b01f4

.field public static final toggle_toolbar_input_num_only:I = 0x7f0b01f5

.field public static final toggle_toolbar_input_numsym_only:I = 0x7f0b01f6

.field public static final toggle_toolbar_kor_hl:I = 0x7f0b01f7

.field public static final toggle_toolbar_num_hl:I = 0x7f0b01f8

.field public static final toggle_toolbar_num_normal:I = 0x7f0b01f9

.field public static final toggle_toolbar_sym_hl:I = 0x7f0b01fa

.field public static final toggle_toolbar_sym_normal:I = 0x7f0b01fb

.field public static final toggle_uppercase:I = 0x7f0b01fc

.field public static final toggle_url_prefix:I = 0x7f0b01fd

.field public static final toggle_url_suffix:I = 0x7f0b01fe

.field public static final toggle_url_symbol:I = 0x7f0b01ff

.field public static final toggle_voice_setting_off:I = 0x7f0b003d

.field public static final toggle_voice_setting_on:I = 0x7f0b003c

.field public static final toggle_zhuyin:I = 0x7f0b0200

.field public static final toggle_zhuyin_34:I = 0x7f0b0201

.field public static final toggle_zhuyin_34_value:I = 0x7f0b0202

.field public static final toggle_zhuyin_stroke_34:I = 0x7f0b0203

.field public static final toggle_zhuyin_stroke_34_value:I = 0x7f0b0204

.field public static final toggle_zhuyin_value:I = 0x7f0b0205

.field public static final toolbar_symbols_angle:I = 0x7f0b029a

.field public static final toolbar_symbols_common:I = 0x7f0b0299

.field public static final toolbar_symbols_emotion:I = 0x7f0b029c

.field public static final toolbar_symbols_half_angle:I = 0x7f0b029b

.field public static final toturial_content:I = 0x7f0b02cc

.field public static final toturial_sliding_content:I = 0x7f0b0052

.field public static final toturial_title:I = 0x7f0b0051

.field public static final touch_to_continue:I = 0x7f0b0109

.field public static final touchtype_login_success_url:I = 0x7f0b0003

.field public static final track:I = 0x7f0b010a

.field public static final twitter_access_token_url:I = 0x7f0b0005

.field public static final twitter_authorize_url:I = 0x7f0b0006

.field public static final twitter_request_token_url:I = 0x7f0b0004

.field public static final update:I = 0x7f0b0262

.field public static final update_in_progress:I = 0x7f0b0264

.field public static final update_language_dialog_msg:I = 0x7f0b0265

.field public static final update_language_list_dialog_msg:I = 0x7f0b026c

.field public static final use_predictive_text:I = 0x7f0b025e

.field public static final version:I = 0x7f0b0013

.field public static final voice_audio_error:I = 0x7f0b010b

.field public static final voice_error:I = 0x7f0b010c

.field public static final voice_initializing:I = 0x7f0b010d

.field public static final voice_input:I = 0x7f0b010e

.field public static final voice_listening:I = 0x7f0b010f

.field public static final voice_network_error:I = 0x7f0b0110

.field public static final voice_no_match:I = 0x7f0b0111

.field public static final voice_not_installed:I = 0x7f0b0112

.field public static final voice_punctuation_hint:I = 0x7f0b0113

.field public static final voice_server_error:I = 0x7f0b0114

.field public static final voice_speech_timeout:I = 0x7f0b0115

.field public static final voice_swipe_hint:I = 0x7f0b0116

.field public static final voice_too_much_speech:I = 0x7f0b0117

.field public static final voice_warning_how_to_turn_off:I = 0x7f0b0118

.field public static final voice_warning_locale_not_supported:I = 0x7f0b0119

.field public static final voice_warning_may_not_understand:I = 0x7f0b011a

.field public static final voice_warning_title:I = 0x7f0b011b

.field public static final voice_working:I = 0x7f0b011c

.field public static final weibo_access_token_url:I = 0x7f0b000d

.field public static final weibo_authorize_url:I = 0x7f0b000e

.field public static final weibo_request_token_url:I = 0x7f0b000c

.field public static final word_123:I = 0x7f0b001a

.field public static final word_34_english:I = 0x7f0b011d

.field public static final word_34_kor:I = 0x7f0b011e

.field public static final word_34_pinyin:I = 0x7f0b011f

.field public static final word_34_stroke:I = 0x7f0b0120

.field public static final word_FS_kor:I = 0x7f0b0127

.field public static final word_Qwerty_english:I = 0x7f0b012f

.field public static final word_Qwerty_kor:I = 0x7f0b0130

.field public static final word_Qwerty_pinyin:I = 0x7f0b0131

.field public static final word_abc_lower:I = 0x7f0b0132

.field public static final word_abc_tempupper:I = 0x7f0b0133

.field public static final word_abc_upper:I = 0x7f0b0134

.field public static final word_changetoKorean:I = 0x7f0b0122

.field public static final word_changetochinese:I = 0x7f0b0121

.field public static final word_cn:I = 0x7f0b001b

.field public static final word_cn_hk:I = 0x7f0b001c

.field public static final word_cn_tw:I = 0x7f0b001d

.field public static final word_en:I = 0x7f0b001e

.field public static final word_eng_lower:I = 0x7f0b0135

.field public static final word_eng_tempupper:I = 0x7f0b0136

.field public static final word_eng_upper:I = 0x7f0b0137

.field public static final word_enter_done:I = 0x7f0b0123

.field public static final word_enter_go:I = 0x7f0b0124

.field public static final word_enter_next:I = 0x7f0b0125

.field public static final word_enter_send:I = 0x7f0b0126

.field public static final word_fullscreenhandwriting:I = 0x7f0b0128

.field public static final word_hw_recognize_mode_all:I = 0x7f0b0129

.field public static final word_hw_recognize_mode_cn_stroke:I = 0x7f0b012a

.field public static final word_hw_recognize_mode_py:I = 0x7f0b012b

.field public static final word_indi_cn:I = 0x7f0b001f

.field public static final word_indi_en:I = 0x7f0b0020

.field public static final word_indi_hk:I = 0x7f0b0021

.field public static final word_indi_ko:I = 0x7f0b0022

.field public static final word_indi_tw:I = 0x7f0b0023

.field public static final word_ko:I = 0x7f0b0024

.field public static final word_kor:I = 0x7f0b0139

.field public static final word_number:I = 0x7f0b013c

.field public static final word_number_character:I = 0x7f0b012c

.field public static final word_phone_sym_pause:I = 0x7f0b012d

.field public static final word_phone_sym_wait:I = 0x7f0b012e

.field public static final word_pinyin:I = 0x7f0b0026

.field public static final word_pinyin_ab:I = 0x7f0b013a

.field public static final word_stroke:I = 0x7f0b0027

.field public static final word_stroke_ab:I = 0x7f0b013b

.field public static final word_symbol:I = 0x7f0b0138

.field public static final word_word_think:I = 0x7f0b0028


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1577
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
