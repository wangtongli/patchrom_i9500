.class public final Lcom/samsung/inputmethod/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/inputmethod/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final active_candidate_color:I = 0x7f080005

.field public static final active_mini_popup_text_color:I = 0x7f08001c

.field public static final adapter_color_list:I = 0x7f08002f

.field public static final balloon_color:I = 0x7f080003

.field public static final balloon_font_color:I = 0x7f08002c

.field public static final candidate_bg_color:I = 0x7f080007

.field public static final candidate_color:I = 0x7f080004

.field public static final candidate_color_list:I = 0x7f080030

.field public static final candidate_color_p:I = 0x7f080006

.field public static final candidate_normal:I = 0x7f08000e

.field public static final candidate_other:I = 0x7f080010

.field public static final candidate_recommended:I = 0x7f08000f

.field public static final candidate_separator_color:I = 0x7f080019

.field public static final color1:I = 0x7f080014

.field public static final color2:I = 0x7f080015

.field public static final color201:I = 0x7f080017

.field public static final color202:I = 0x7f080018

.field public static final color_basic:I = 0x7f080013

.field public static final color_extra:I = 0x7f080016

.field public static final composing_color:I = 0x7f08000b

.field public static final composing_color_hl:I = 0x7f08000c

.field public static final composing_color_idle:I = 0x7f08000d

.field public static final footnote_color:I = 0x7f080009

.field public static final footnote_dim_color:I = 0x7f08000a

.field public static final function_key_color:I = 0x7f08002a

.field public static final function_key_color_p:I = 0x7f08002b

.field public static final key_color_list:I = 0x7f080031

.field public static final keyboard_type_btn_txt_color:I = 0x7f080026

.field public static final keyboard_type_btn_txt_color_highlight:I = 0x7f080027

.field public static final label_color:I = 0x7f080000

.field public static final label_color_hl0:I = 0x7f080001

.field public static final label_color_p:I = 0x7f080002

.field public static final label_disable_color:I = 0x7f080020

.field public static final label_disable_tabedit_list_separator:I = 0x7f080021

.field public static final label_focus:I = 0x7f080011

.field public static final label_yellow:I = 0x7f080012

.field public static final labeltext_shadow_color:I = 0x7f08001f

.field public static final longlabel_color:I = 0x7f080028

.field public static final longlabel_color_p:I = 0x7f080029

.field public static final mini_popup_separator_color1:I = 0x7f08001d

.field public static final mini_popup_separator_color2:I = 0x7f08001e

.field public static final mini_popup_text_color:I = 0x7f08001b

.field public static final mini_popup_text_color_list:I = 0x7f080032

.field public static final mini_popup_text_color_p:I = 0x7f08001a

.field public static final mm_mini_popup_text_color:I = 0x7f08002d

.field public static final numberic_text_color_extra:I = 0x7f08002e

.field public static final recommended_candidate_color:I = 0x7f080008

.field public static final tab_title_active_color:I = 0x7f080022

.field public static final tab_title_normal_color:I = 0x7f080023

.field public static final tab_title_shadow_active_color:I = 0x7f080025

.field public static final tab_title_shadow_normal_color:I = 0x7f080024


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 134
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
