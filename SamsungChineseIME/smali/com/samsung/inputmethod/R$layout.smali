.class public final Lcom/samsung/inputmethod/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/inputmethod/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final bubble_text:I = 0x7f030000

.field public static final candidates_container:I = 0x7f030001

.field public static final candidates_container_vertical:I = 0x7f030002

.field public static final candidates_container_vertical_scroll:I = 0x7f030003

.field public static final combined_candidates_container:I = 0x7f030004

.field public static final db_manager_prefrence:I = 0x7f030005

.field public static final definemain:I = 0x7f030006

.field public static final defineview:I = 0x7f030007

.field public static final expand_candidates_container:I = 0x7f030008

.field public static final expand_pinyin_container:I = 0x7f030009

.field public static final floating_candidate_preview:I = 0x7f03000a

.field public static final floating_container:I = 0x7f03000b

.field public static final floating_input_panel:I = 0x7f03000c

.field public static final keyboard_set_popup:I = 0x7f03000d

.field public static final mini_popup:I = 0x7f03000e

.field public static final mode_list_item:I = 0x7f03000f

.field public static final mode_list_separator:I = 0x7f030010

.field public static final onehand_container:I = 0x7f030011

.field public static final personalize_privacy_preference_layout:I = 0x7f030012

.field public static final personalize_webview:I = 0x7f030013

.field public static final popup_languagedownload_guide:I = 0x7f030014

.field public static final popup_languagelist_guide:I = 0x7f030015

.field public static final progress:I = 0x7f030016

.field public static final recognition_status:I = 0x7f030017

.field public static final row_candidates_container:I = 0x7f030018

.field public static final settings_layout_more_button:I = 0x7f030019

.field public static final skb_container:I = 0x7f03001a

.field public static final swiftkey_preference_language_list:I = 0x7f03001b

.field public static final tabarrow:I = 0x7f03001c

.field public static final tabedit_list:I = 0x7f03001d

.field public static final tabmode_container:I = 0x7f03001e

.field public static final tabpage:I = 0x7f03001f

.field public static final toolbar_container:I = 0x7f030020

.field public static final toolbar_page:I = 0x7f030021

.field public static final toolbar_symbols:I = 0x7f030022

.field public static final tutorial:I = 0x7f030023

.field public static final voice_punctuation_hint:I = 0x7f030024

.field public static final voice_swipe_hint:I = 0x7f030025

.field public static final webview:I = 0x7f030026


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1530
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
