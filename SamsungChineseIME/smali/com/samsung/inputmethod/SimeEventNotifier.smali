.class public interface abstract Lcom/samsung/inputmethod/SimeEventNotifier;
.super Ljava/lang/Object;
.source "SimeEventNotifier.java"


# static fields
.field public static final EVENT_CONFIGURATION_CHANGE:I = 0x4

.field public static final EVENT_DESTROY:I = 0x5

.field public static final EVENT_FINISH_INPUT:I = 0x1

.field public static final EVENT_FINISH_INPUTVIEW:I = 0x3

.field public static final EVENT_HW_GESTURE:I = 0x6

.field public static final EVENT_PICK_DEFAULT_CAND:I = 0x7

.field public static final EVENT_START_INPUT:I = 0x0

.field public static final EVENT_START_INPUTVIEW:I = 0x2


# virtual methods
.method public abstract handleEvent(ILjava/lang/Object;Ljava/lang/Object;)Z
.end method
