.class public final Lcom/samsung/inputmethod/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/inputmethod/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final DefineAddButton:I = 0x7f0c0020

.field public static final DefineDeleteButton:I = 0x7f0c001f

.field public static final DefineEditButton:I = 0x7f0c001e

.field public static final DeleteButtonLeft:I = 0x7f0c0025

.field public static final ImageView01:I = 0x7f0c001d

.field public static final InputContent:I = 0x7f0c0022

.field public static final LeftView:I = 0x7f0c0024

.field public static final SaveButton:I = 0x7f0c0026

.field public static final TextContent:I = 0x7f0c0021

.field public static final ViewLeft:I = 0x7f0c001c

.field public static final alpha_floatable:I = 0x7f0c0058

.field public static final alpha_view1:I = 0x7f0c0059

.field public static final alpha_view2:I = 0x7f0c005a

.field public static final arrow_down_btn:I = 0x7f0c0004

.field public static final arrow_left_btn:I = 0x7f0c0001

.field public static final arrow_right_btn:I = 0x7f0c0005

.field public static final arrow_right_position:I = 0x7f0c0002

.field public static final arrow_up_btn:I = 0x7f0c0003

.field public static final bottom_btn:I = 0x7f0c003c

.field public static final bottom_position:I = 0x7f0c003b

.field public static final button:I = 0x7f0c0053

.field public static final button_group:I = 0x7f0c0010

.field public static final button_text:I = 0x7f0c0054

.field public static final cancel:I = 0x7f0c0016

.field public static final candidate_flipper:I = 0x7f0c0006

.field public static final candidate_flipper_vertical:I = 0x7f0c000b

.field public static final candidate_view1:I = 0x7f0c0007

.field public static final candidate_view2:I = 0x7f0c0008

.field public static final candidatesArea:I = 0x7f0c0030

.field public static final candidates_container:I = 0x7f0c0000

.field public static final candidates_list_main:I = 0x7f0c0029

.field public static final candidates_vertical_scroll:I = 0x7f0c000c

.field public static final candidates_vertical_scroll2:I = 0x7f0c005c

.field public static final candidates_vertical_scroll_container:I = 0x7f0c005b

.field public static final col_candidate:I = 0x7f0c000d

.field public static final col_candidate_2:I = 0x7f0c005d

.field public static final combined_candidates_container:I = 0x7f0c000e

.field public static final commonphrase:I = 0x7f0c0018

.field public static final composing_view:I = 0x7f0c002e

.field public static final confirmbox:I = 0x7f0c0085

.field public static final content_view_image:I = 0x7f0c004d

.field public static final content_view_progress:I = 0x7f0c004f

.field public static final content_view_text:I = 0x7f0c004e

.field public static final dbName:I = 0x7f0c0011

.field public static final dbUpdateDate:I = 0x7f0c0012

.field public static final dim_tabbar_bg:I = 0x7f0c0074

.field public static final downloadlanguagelistpopuptext:I = 0x7f0c004c

.field public static final downloadlanguagepopuptext:I = 0x7f0c004a

.field public static final edit_tab_checkbox:I = 0x7f0c003f

.field public static final edit_tab_item_title:I = 0x7f0c003e

.field public static final enable:I = 0x7f0c0064

.field public static final expand_candidates_container:I = 0x7f0c0027

.field public static final expand_candidates_scroll:I = 0x7f0c0028

.field public static final expand_layout:I = 0x7f0c002a

.field public static final facebook:I = 0x7f0c008e

.field public static final floatingPanel:I = 0x7f0c002f

.field public static final floating_container:I = 0x7f0c002d

.field public static final gmail:I = 0x7f0c008d

.field public static final handwrite_box:I = 0x7f0c005e

.field public static final icon_group:I = 0x7f0c0061

.field public static final image:I = 0x7f0c0051

.field public static final imageView1:I = 0x7f0c008b

.field public static final inputArea:I = 0x7f0c0031

.field public static final keyboard_set_popup_pointer_down:I = 0x7f0c0036

.field public static final keyboard_set_popup_pointer_up:I = 0x7f0c0032

.field public static final keyboard_set_popup_table:I = 0x7f0c0033

.field public static final keyboard_set_popup_table_row_one:I = 0x7f0c0034

.field public static final keyboard_set_popup_table_row_two:I = 0x7f0c0035

.field public static final langauge_name:I = 0x7f0c0062

.field public static final latestsymb_view:I = 0x7f0c005f

.field public static final layout:I = 0x7f0c0046

.field public static final left_button:I = 0x7f0c0044

.field public static final left_layout:I = 0x7f0c0043

.field public static final linearLayout1:I = 0x7f0c0087

.field public static final main_image:I = 0x7f0c0050

.field public static final menu:I = 0x7f0c0017

.field public static final mini_popup_container:I = 0x7f0c0037

.field public static final myListItem:I = 0x7f0c003d

.field public static final next_btn:I = 0x7f0c000a

.field public static final normal_layout:I = 0x7f0c0045

.field public static final one_hand_layout:I = 0x7f0c0040

.field public static final operationcommand:I = 0x7f0c0019

.field public static final previous_btn:I = 0x7f0c0009

.field public static final privacy_text_view:I = 0x7f0c0047

.field public static final progress:I = 0x7f0c0052

.field public static final progress_bar_personalize:I = 0x7f0c0049

.field public static final progress_precent:I = 0x7f0c0014

.field public static final progress_waiting:I = 0x7f0c0013

.field public static final radioButton1:I = 0x7f0c008a

.field public static final right_button:I = 0x7f0c0042

.field public static final right_layout:I = 0x7f0c0041

.field public static final right_position:I = 0x7f0c0039

.field public static final rihgt_btn:I = 0x7f0c003a

.field public static final row_candidates_container:I = 0x7f0c0055

.field public static final row_for_cells:I = 0x7f0c0056

.field public static final row_separator:I = 0x7f0c0057

.field public static final row_spell:I = 0x7f0c002c

.field public static final spell_scroll:I = 0x7f0c002b

.field public static final spinner1:I = 0x7f0c001b

.field public static final spinnerorder:I = 0x7f0c0023

.field public static final statusbar:I = 0x7f0c0063

.field public static final symbol_list_main:I = 0x7f0c0060

.field public static final tab_arrow:I = 0x7f0c0065

.field public static final tab_arrow_btn:I = 0x7f0c0068

.field public static final tab_arrow_left:I = 0x7f0c006d

.field public static final tab_arrow_left_separator:I = 0x7f0c0067

.field public static final tab_arrow_right:I = 0x7f0c0071

.field public static final tab_arrow_right_separator:I = 0x7f0c0069

.field public static final tab_icon:I = 0x7f0c0079

.field public static final tab_inputmode_container:I = 0x7f0c006c

.field public static final tab_left_indicator:I = 0x7f0c0066

.field public static final tab_margin:I = 0x7f0c0078

.field public static final tab_mode_edit:I = 0x7f0c006e

.field public static final tab_mode_edit_icon:I = 0x7f0c0070

.field public static final tab_mode_edit_seperator:I = 0x7f0c006f

.field public static final tab_mode_list:I = 0x7f0c006b

.field public static final tab_page:I = 0x7f0c0075

.field public static final tab_page_main:I = 0x7f0c0076

.field public static final tab_right_indicator:I = 0x7f0c006a

.field public static final tab_separator:I = 0x7f0c007a

.field public static final tab_title:I = 0x7f0c0077

.field public static final table_main:I = 0x7f0c0038

.field public static final tabs_bar_main:I = 0x7f0c0073

.field public static final tabs_bar_scroll:I = 0x7f0c0072

.field public static final text:I = 0x7f0c001a

.field public static final textView1:I = 0x7f0c0088

.field public static final textView2:I = 0x7f0c0089

.field public static final title_group:I = 0x7f0c000f

.field public static final tool_bar_page:I = 0x7f0c007e

.field public static final tool_bar_page_main:I = 0x7f0c007f

.field public static final tool_bar_scroll:I = 0x7f0c007c

.field public static final tool_bar_view:I = 0x7f0c007d

.field public static final toolbar_container:I = 0x7f0c007b

.field public static final toolbar_icon:I = 0x7f0c0080

.field public static final toolbar_separator:I = 0x7f0c0082

.field public static final toolbar_title:I = 0x7f0c0081

.field public static final toturial_slidingandflick:I = 0x7f0c0083

.field public static final trace_popup_chk_box:I = 0x7f0c004b

.field public static final tutorial_move_keyboard_imageview:I = 0x7f0c0084

.field public static final twitter:I = 0x7f0c008f

.field public static final update:I = 0x7f0c0015

.field public static final webview_personalize:I = 0x7f0c0048

.field public static final weibo:I = 0x7f0c008c

.field public static final wv:I = 0x7f0c0086


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1384
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
