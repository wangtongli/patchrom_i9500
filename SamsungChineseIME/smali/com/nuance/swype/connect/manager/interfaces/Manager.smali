.class public interface abstract Lcom/nuance/swype/connect/manager/interfaces/Manager;
.super Ljava/lang/Object;
.source "Manager.java"


# virtual methods
.method public abstract delayFirstStart()J
.end method

.method public abstract deregister()V
.end method

.method public abstract destroy()V
.end method

.method public abstract getDependencies()[Ljava/lang/String;
.end method

.method public abstract getManagerName()Ljava/lang/String;
.end method

.method public abstract getManagerStartState()Lcom/nuance/swype/connect/manager/AbstractCommandManager$ManagerState;
.end method

.method public abstract init()V
.end method

.method public abstract postStart()V
.end method

.method public abstract postUpgrade()V
.end method

.method public abstract prepareForUpgrade()V
.end method

.method public abstract rebind()V
.end method

.method public abstract restart()V
.end method

.method public abstract start()V
.end method
