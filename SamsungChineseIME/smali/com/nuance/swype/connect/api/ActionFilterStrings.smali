.class public Lcom/nuance/swype/connect/api/ActionFilterStrings;
.super Ljava/lang/Object;
.source "ActionFilterStrings.java"


# static fields
.field public static final ACTION_ACCOUNT_STATUS_CHANGE:Ljava/lang/String; = "com.nuance.swype.connect.ACCOUNT_STATUS_CHANGE"

.field public static final ACTION_DATA_AVAILABLE:Ljava/lang/String; = "com.nuance.swype.connect.DATA_AVAILABLE"

.field public static final ACTION_DISPLAY_DIALOG:Ljava/lang/String; = "com.nuance.swype.connect.DISPLAY_DIALOG"

.field public static final ACTION_NOTIFICATION_SEND_FILTER:Ljava/lang/String; = "com.nuance.android.util.ConnectNotification.NOTIFICATION_FILTER"

.field public static final ACTION_REDIRECT:Ljava/lang/String; = "com.nuance.swype.connect.REDIRECT"

.field public static final KEY_BLOCK_DISPLAY:Ljava/lang/String; = "BLOCK_DISPLAY"

.field public static final KEY_BLOCK_NOTIFICATION:Ljava/lang/String; = "BLOCK_NOTIFICATION"

.field public static final KEY_BLOCK_REDIRECT:Ljava/lang/String; = "BLOCK_REDIRECT"

.field public static final KEY_STATUS:Ljava/lang/String; = "STATUS"

.field public static final TYPE_ACCOUNT_DATA:Ljava/lang/String; = "com.nuance.swype.connect.TYPE_ACCOUNT_DATA"

.field public static final TYPE_ACCOUNT_DELETED:Ljava/lang/String; = "com.nuance.swype.connect.TYPE_ACCOUNT_DELETED"

.field public static final TYPE_ACCOUNT_INVALID_CODE:Ljava/lang/String; = "com.nuance.swype.connect.TYPE_ACCOUNT_INVALID_CODE"

.field public static final TYPE_ACCOUNT_VERIFIED:Ljava/lang/String; = "com.nuance.swype.connect.TYPE_ACCOUNT_VERIFIED"

.field public static final TYPE_ALM_DATA:Ljava/lang/String; = "com.nuance.swype.connect.ALM_DATA"

.field public static final TYPE_DICTIONARY_DOWNLOAD_DATA:Ljava/lang/String; = "com.nuance.swype.connect.TYPE_DICTIONARY_DOWNLOAD_DATA"

.field public static final TYPE_LANGUAGE_DOWNLOAD_DATA:Ljava/lang/String; = "com.nuance.swype.connect.TYPE_LANGUAGE_DOWNLOAD_DATA"

.field public static final TYPE_NOTIFICATION_ALM_INSTALLED:Ljava/lang/String; = "com.nuance.swype.input.Updates.ALM_INSTALLED"

.field public static final TYPE_TERMS_OF_SERVICE:Ljava/lang/String; = "com.nuance.swype.connect.TYPE_TERMS_OF_SERVICE"

.field public static final TYPE_UPDATE_DATA:Ljava/lang/String; = "com.nuance.swype.connect.TYPE_UPDATE_DATA"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
