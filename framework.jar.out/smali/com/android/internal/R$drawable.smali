.class public final Lcom/android/internal/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final alert_dark_frame:I = 0x1080000

.field public static final alert_light_frame:I = 0x1080001

.field public static final appce_ic_music:I = 0x10800d1

.field public static final arrow_down_float:I = 0x1080002

.field public static final arrow_up_float:I = 0x1080003

.field public static final bottom_bar:I = 0x108009a

.field public static final btn_check_off:I = 0x10800f1

.field public static final btn_code_lock_default_holo:I = 0x1080125

.field public static final btn_code_lock_touched_holo:I = 0x1080127

.field public static final btn_default:I = 0x1080004

.field public static final btn_default_small:I = 0x1080005

.field public static final btn_dialog:I = 0x1080017

.field public static final btn_dropdown:I = 0x1080006

.field public static final btn_minus:I = 0x1080007

.field public static final btn_plus:I = 0x1080008

.field public static final btn_radio:I = 0x1080009

.field public static final btn_star:I = 0x108000a

.field public static final btn_star_big_off:I = 0x108000b

.field public static final btn_star_big_on:I = 0x108000c

.field public static final button_onoff_indicator_off:I = 0x108000e

.field public static final button_onoff_indicator_on:I = 0x108000d

.field public static final cab_background_top_holo_dark:I = 0x1080221

.field public static final call_contact:I = 0x1080223

.field public static final checkbox_off_background:I = 0x108000f

.field public static final checkbox_on_background:I = 0x1080010

.field public static final clock_dial:I = 0x1080224

.field public static final clock_hand_hour:I = 0x1080225

.field public static final clock_hand_minute:I = 0x1080226

.field public static final compass_arrow:I = 0x108023b

.field public static final compass_base:I = 0x108023c

.field public static final container_notification_icon:I = 0x108023e

.field public static final create_contact:I = 0x1080240

.field public static final dark_header:I = 0x10800a5

.field public static final dec_progress_level_list:I = 0x1080243

.field public static final dec_security_policy_error_icon:I = 0x1080244

.field public static final dec_security_policy_icon:I = 0x1080245

.field public static final default_wallpaper:I = 0x108024b

.field public static final dialog_frame:I = 0x1080011

.field public static final dialog_holo_dark_frame:I = 0x10800b2

.field public static final dialog_holo_light_frame:I = 0x10800b3

.field public static final divider_horizontal_bright:I = 0x1080012

.field public static final divider_horizontal_dark:I = 0x1080014

.field public static final divider_horizontal_dim_dark:I = 0x1080015

.field public static final divider_horizontal_textfield:I = 0x1080013

.field public static final edit_text:I = 0x1080016

.field public static final editbox_background:I = 0x1080018

.field public static final editbox_background_normal:I = 0x1080019

.field public static final editbox_dropdown_dark_frame:I = 0x108001a

.field public static final editbox_dropdown_light_frame:I = 0x108001b

.field public static final emo_im_angel:I = 0x1080287

.field public static final emo_im_cool:I = 0x1080288

.field public static final emo_im_crying:I = 0x1080289

.field public static final emo_im_embarrassed:I = 0x108028a

.field public static final emo_im_foot_in_mouth:I = 0x108028b

.field public static final emo_im_happy:I = 0x108028c

.field public static final emo_im_kissing:I = 0x108028d

.field public static final emo_im_laughing:I = 0x108028e

.field public static final emo_im_lips_are_sealed:I = 0x108028f

.field public static final emo_im_money_mouth:I = 0x1080290

.field public static final emo_im_sad:I = 0x1080291

.field public static final emo_im_surprised:I = 0x1080292

.field public static final emo_im_tongue_sticking_out:I = 0x1080293

.field public static final emo_im_undecided:I = 0x1080294

.field public static final emo_im_winking:I = 0x1080295

.field public static final emo_im_wtf:I = 0x1080296

.field public static final emo_im_yelling:I = 0x1080297

.field public static final enc_progress_level_list:I = 0x1080298

.field public static final enc_security_policy_error_icon:I = 0x1080299

.field public static final enc_security_policy_icon:I = 0x108029a

.field public static final expander_close_holo_dark:I = 0x10802b4

.field public static final expander_ic_maximized:I = 0x10802b9

.field public static final expander_ic_minimized:I = 0x10802ba

.field public static final expander_open_holo_dark:I = 0x10802bb

.field public static final gallery_thumb:I = 0x108001c

.field public static final gradation_indi_bg:I = 0x10802d8

.field public static final handler_01_area:I = 0x10802dc

.field public static final handler_02_area:I = 0x10802dd

.field public static final homescreen_menu_noti_bg:I = 0x10802e1

.field public static final hotspot_home:I = 0x10802e2

.field public static final hotspot_roaming:I = 0x10802e3

.field public static final hover_g_ic_back_big:I = 0x10802e4

.field public static final hover_g_ic_call_big:I = 0x10802e5

.field public static final hover_g_ic_menu_big:I = 0x10802e6

.field public static final hover_g_ic_quick_big:I = 0x10802e7

.field public static final hover_g_ic_rotate:I = 0x10802e8

.field public static final hover_g_key_back:I = 0x10802e9

.field public static final hover_g_key_menu:I = 0x10802ea

.field public static final hover_ic_point:I = 0x10802eb

.field public static final hover_ic_point_w:I = 0x10802ec

.field public static final hover_line:I = 0x10802ed

.field public static final hover_line_w:I = 0x10802ee

.field public static final hover_popup_bg:I = 0x10802ef

.field public static final hover_popup_bg_bottom:I = 0x10802f0

.field public static final hover_popup_bg_bottom_w:I = 0x10802f1

.field public static final hover_popup_bg_w:I = 0x10802f2

.field public static final hover_tooltip_bg:I = 0x10802f3

.field public static final hover_tooltip_bg_w:I = 0x10802f4

.field public static final ic_action_assist_generic:I = 0x10802f8

.field public static final ic_audio_alarm:I = 0x10802fc

.field public static final ic_audio_alarm_mute:I = 0x10802fd

.field public static final ic_audio_bt:I = 0x10802fe

.field public static final ic_audio_bt_mute:I = 0x10802ff

.field public static final ic_audio_notification:I = 0x1080300

.field public static final ic_audio_notification_mute:I = 0x1080301

.field public static final ic_audio_notification_vibrate:I = 0x1080302

.field public static final ic_audio_phone:I = 0x1080303

.field public static final ic_audio_ring_notif:I = 0x1080304

.field public static final ic_audio_ring_notif_mute:I = 0x1080305

.field public static final ic_audio_ring_notif_vibrate:I = 0x1080306

.field public static final ic_audio_system:I = 0x1080307

.field public static final ic_audio_system_mute:I = 0x1080308

.field public static final ic_audio_system_vibrate:I = 0x1080309

.field public static final ic_audio_vol:I = 0x108030a

.field public static final ic_audio_vol_mute:I = 0x108030c

.field public static final ic_bright:I = 0x108030e

.field public static final ic_btn_speak_now:I = 0x10800a4

.field public static final ic_bullet_key_permission:I = 0x108031a

.field public static final ic_contact_picture:I = 0x108032a

.field public static final ic_delete:I = 0x108001d

.field public static final ic_dialog_alert:I = 0x1080027

.field public static final ic_dialog_alert_holo_dark:I = 0x108032d

.field public static final ic_dialog_dialer:I = 0x1080028

.field public static final ic_dialog_email:I = 0x1080029

.field public static final ic_dialog_info:I = 0x108009b

.field public static final ic_dialog_map:I = 0x108002a

.field public static final ic_dialog_usb:I = 0x1080333

.field public static final ic_emergency:I = 0x1080334

.field public static final ic_input_add:I = 0x108002b

.field public static final ic_input_delete:I = 0x108002c

.field public static final ic_input_get:I = 0x108002d

.field public static final ic_jog_dial_sound_off:I = 0x1080341

.field public static final ic_jog_dial_sound_on:I = 0x1080342

.field public static final ic_jog_dial_unlock:I = 0x1080343

.field public static final ic_jog_dial_vibrate_on:I = 0x1080344

.field public static final ic_lock_airplane_mode:I = 0x1080346

.field public static final ic_lock_airplane_mode_off:I = 0x1080347

.field public static final ic_lock_idle_alarm:I = 0x108002e

.field public static final ic_lock_idle_charging:I = 0x108001e

.field public static final ic_lock_idle_lock:I = 0x108001f

.field public static final ic_lock_idle_low_battery:I = 0x1080020

.field public static final ic_lock_lock:I = 0x108002f

.field public static final ic_lock_power_off:I = 0x1080030

.field public static final ic_lock_silent_mode:I = 0x1080031

.field public static final ic_lock_silent_mode_off:I = 0x1080032

.field public static final ic_lockscreen_alarm:I = 0x108034b

.field public static final ic_lockscreen_camera:I = 0x108034f

.field public static final ic_lockscreen_silent:I = 0x1080369

.field public static final ic_lockscreen_sim1:I = 0x108036e

.field public static final ic_lockscreen_sim2:I = 0x108036f

.field public static final ic_lockscreen_unlock:I = 0x1080378

.field public static final ic_lockscreen_unlock_phantom:I = 0x108037b

.field public static final ic_maps_indicator_current_position_anim:I = 0x108037d

.field public static final ic_media_ff:I = 0x1080021

.field public static final ic_media_next:I = 0x1080022

.field public static final ic_media_pause:I = 0x1080023

.field public static final ic_media_play:I = 0x1080024

.field public static final ic_media_previous:I = 0x1080025

.field public static final ic_media_rew:I = 0x1080026

.field public static final ic_media_route_disabled_holo_dark:I = 0x1080387

.field public static final ic_media_route_on_holo_dark:I = 0x1080393

.field public static final ic_media_stop:I = 0x1080395

.field public static final ic_menu_add:I = 0x1080033

.field public static final ic_menu_agenda:I = 0x1080034

.field public static final ic_menu_always_landscape_portrait:I = 0x1080035

.field public static final ic_menu_archive:I = 0x1080399

.field public static final ic_menu_call:I = 0x1080036

.field public static final ic_menu_camera:I = 0x1080037

.field public static final ic_menu_cc:I = 0x10803a1

.field public static final ic_menu_close_clear_cancel:I = 0x1080038

.field public static final ic_menu_compass:I = 0x1080039

.field public static final ic_menu_crop:I = 0x108003a

.field public static final ic_menu_day:I = 0x108003b

.field public static final ic_menu_delete:I = 0x108003c

.field public static final ic_menu_directions:I = 0x108003d

.field public static final ic_menu_edit:I = 0x108003e

.field public static final ic_menu_gallery:I = 0x108003f

.field public static final ic_menu_goto:I = 0x10803b2

.field public static final ic_menu_help:I = 0x1080040

.field public static final ic_menu_info_details:I = 0x1080041

.field public static final ic_menu_manage:I = 0x1080042

.field public static final ic_menu_mapmode:I = 0x1080043

.field public static final ic_menu_month:I = 0x1080044

.field public static final ic_menu_more:I = 0x1080045

.field public static final ic_menu_moreoverflow_normal_holo_dark:I = 0x10803bd

.field public static final ic_menu_my_calendar:I = 0x1080046

.field public static final ic_menu_mylocation:I = 0x1080047

.field public static final ic_menu_myplaces:I = 0x1080048

.field public static final ic_menu_play_clip:I = 0x10803c3

.field public static final ic_menu_preferences:I = 0x1080049

.field public static final ic_menu_recent_history:I = 0x108004a

.field public static final ic_menu_report_image:I = 0x108004b

.field public static final ic_menu_revert:I = 0x108004c

.field public static final ic_menu_rotate:I = 0x108004d

.field public static final ic_menu_save:I = 0x108004e

.field public static final ic_menu_search:I = 0x108004f

.field public static final ic_menu_send:I = 0x1080050

.field public static final ic_menu_set_as:I = 0x1080051

.field public static final ic_menu_share:I = 0x1080052

.field public static final ic_menu_slideshow:I = 0x1080053

.field public static final ic_menu_sort_alphabetically:I = 0x108009c

.field public static final ic_menu_sort_by_size:I = 0x108009d

.field public static final ic_menu_today:I = 0x1080054

.field public static final ic_menu_upload:I = 0x1080055

.field public static final ic_menu_upload_you_tube:I = 0x1080056

.field public static final ic_menu_view:I = 0x1080057

.field public static final ic_menu_week:I = 0x1080058

.field public static final ic_menu_zoom:I = 0x1080059

.field public static final ic_notification_clear_all:I = 0x108005a

.field public static final ic_notification_ime_default:I = 0x10803d5

.field public static final ic_notification_overlay:I = 0x108005b

.field public static final ic_notify_wifidisplay:I = 0x10803df

.field public static final ic_partial_secure:I = 0x108005c

.field public static final ic_popup_disk_full:I = 0x108005d

.field public static final ic_popup_reminder:I = 0x108005e

.field public static final ic_popup_sync:I = 0x108005f

.field public static final ic_search_category_default:I = 0x1080060

.field public static final ic_secure:I = 0x1080061

.field public static final ic_settings_language:I = 0x10803e8

.field public static final ic_suggestions_add:I = 0x10803e9

.field public static final ic_suggestions_delete:I = 0x10803ea

.field public static final ic_text_dot:I = 0x10803ec

.field public static final ic_volume:I = 0x10803f1

.field public static final indicator_code_lock_drag_direction_green_up:I = 0x10803fc

.field public static final indicator_code_lock_drag_direction_red_up:I = 0x10803fd

.field public static final indicator_code_lock_point_area_default_holo:I = 0x1080400

.field public static final indicator_code_lock_point_area_green_holo:I = 0x1080404

.field public static final indicator_code_lock_point_area_red_holo:I = 0x1080406

.field public static final indicator_input_error:I = 0x1080407

.field public static final jog_dial_arrow_long_left_green:I = 0x108040d

.field public static final jog_dial_arrow_long_right_red:I = 0x1080410

.field public static final jog_dial_arrow_short_left_and_right:I = 0x1080413

.field public static final jog_dial_bg:I = 0x1080415

.field public static final jog_dial_dimple:I = 0x1080416

.field public static final jog_dial_dimple_dim:I = 0x1080417

.field public static final jog_tab_bar_left_generic:I = 0x108041f

.field public static final jog_tab_bar_left_unlock:I = 0x1080420

.field public static final jog_tab_bar_right_generic:I = 0x1080428

.field public static final jog_tab_bar_right_sound_off:I = 0x1080429

.field public static final jog_tab_bar_right_sound_on:I = 0x108042a

.field public static final jog_tab_left_generic:I = 0x1080430

.field public static final jog_tab_left_unlock:I = 0x1080433

.field public static final jog_tab_right_generic:I = 0x1080439

.field public static final jog_tab_right_sound_off:I = 0x108043c

.field public static final jog_tab_right_sound_on:I = 0x108043d

.field public static final jog_tab_target_gray:I = 0x108043e

.field public static final jog_tab_target_green:I = 0x108043f

.field public static final jog_tab_target_yellow:I = 0x1080441

.field public static final keyguard_bubble_bubble:I = 0x108044a

.field public static final keyguard_bubble_cloud:I = 0x108044b

.field public static final keyguard_bubble_grass:I = 0x108044c

.field public static final keyguard_bubble_left:I = 0x108044d

.field public static final keyguard_bubble_left_bottom:I = 0x108044e

.field public static final keyguard_bubble_right_bottom:I = 0x108044f

.field public static final keyguard_bubble_sky:I = 0x1080450

.field public static final keyguard_default_wallpaper:I = 0x1080451

.field public static final keyguard_flare_hexagon_blue:I = 0x1080465

.field public static final keyguard_flare_hexagon_green:I = 0x1080466

.field public static final keyguard_flare_hexagon_orange:I = 0x1080467

.field public static final keyguard_flare_hoverlight:I = 0x1080468

.field public static final keyguard_flare_light_00040:I = 0x1080469

.field public static final keyguard_flare_long:I = 0x108046a

.field public static final keyguard_flare_particle:I = 0x108046b

.field public static final keyguard_flare_rainbow:I = 0x108046c

.field public static final keyguard_flare_ring:I = 0x108046d

.field public static final keyguard_flare_vignetting:I = 0x108046e

.field public static final keyguard_lockscreen_ic_cmas:I = 0x1080476

.field public static final keyguard_lockscreen_ic_cmas_sprint:I = 0x1080477

.field public static final keyguard_lockscreen_ic_cmas_vzw:I = 0x1080478

.field public static final keyguard_lockscreen_ic_voice:I = 0x1080479

.field public static final keyguard_lockscreen_ic_voice_check:I = 0x108047a

.field public static final keyguard_lockscreen_ic_voice_fail:I = 0x108047b

.field public static final keyguard_lockscreen_voice_level0:I = 0x108047d

.field public static final keyguard_lockscreen_voice_level1:I = 0x108047e

.field public static final keyguard_lockscreen_voice_level10:I = 0x108047f

.field public static final keyguard_lockscreen_voice_level11:I = 0x1080480

.field public static final keyguard_lockscreen_voice_level12:I = 0x1080481

.field public static final keyguard_lockscreen_voice_level13:I = 0x1080482

.field public static final keyguard_lockscreen_voice_level14:I = 0x1080483

.field public static final keyguard_lockscreen_voice_level15:I = 0x1080484

.field public static final keyguard_lockscreen_voice_level16:I = 0x1080485

.field public static final keyguard_lockscreen_voice_level17:I = 0x1080486

.field public static final keyguard_lockscreen_voice_level18:I = 0x1080487

.field public static final keyguard_lockscreen_voice_level19:I = 0x1080488

.field public static final keyguard_lockscreen_voice_level2:I = 0x1080489

.field public static final keyguard_lockscreen_voice_level20:I = 0x108048a

.field public static final keyguard_lockscreen_voice_level3:I = 0x108048b

.field public static final keyguard_lockscreen_voice_level4:I = 0x108048c

.field public static final keyguard_lockscreen_voice_level5:I = 0x108048d

.field public static final keyguard_lockscreen_voice_level6:I = 0x108048e

.field public static final keyguard_lockscreen_voice_level7:I = 0x108048f

.field public static final keyguard_lockscreen_voice_level8:I = 0x1080490

.field public static final keyguard_lockscreen_voice_level9:I = 0x1080491

.field public static final keyguard_navigation_add:I = 0x1080492

.field public static final keyguard_navigation_add_current:I = 0x1080493

.field public static final keyguard_navigation_current:I = 0x1080494

.field public static final keyguard_navigation_home:I = 0x1080495

.field public static final keyguard_navigation_home_current:I = 0x1080496

.field public static final keyguard_navigation_normal:I = 0x1080497

.field public static final keyguard_unlock_circle_1:I = 0x10804a0

.field public static final keyguard_unlock_circle_2:I = 0x10804a1

.field public static final keyguard_unlock_circle_3:I = 0x10804a2

.field public static final keyguard_unlock_circle_arrow_1:I = 0x10804a3

.field public static final keyguard_unlock_circle_circleview_in:I = 0x10804a4

.field public static final keyguard_unlockscreen_lock_01:I = 0x10804a5

.field public static final keyguard_unlockscreen_lock_02:I = 0x10804a6

.field public static final keyguard_unlockscreen_lock_03:I = 0x10804a7

.field public static final keyguard_unlockscreen_lock_04:I = 0x10804a8

.field public static final keyguard_unlockscreen_lock_05:I = 0x10804a9

.field public static final keyguard_unlockscreen_lock_06:I = 0x10804aa

.field public static final keyguard_unlockscreen_lock_07:I = 0x10804ab

.field public static final keyguard_unlockscreen_lock_08:I = 0x10804ac

.field public static final keyguard_unlockscreen_lock_09:I = 0x10804ad

.field public static final keyguard_unlockscreen_lock_10:I = 0x10804ae

.field public static final keyguard_unlockscreen_lock_11:I = 0x10804af

.field public static final keyguard_unlockscreen_lock_12:I = 0x10804b0

.field public static final keyguard_unlockscreen_lock_13:I = 0x10804b1

.field public static final keyguard_unlockscreen_lock_14:I = 0x10804b2

.field public static final keyguard_unlockscreen_lock_15:I = 0x10804b3

.field public static final keyguard_unlockscreen_lock_16:I = 0x10804b4

.field public static final keyguard_unlockscreen_lock_17:I = 0x10804b5

.field public static final keyguard_unlockscreen_lock_18:I = 0x10804b6

.field public static final keyguard_unlockscreen_lock_19:I = 0x10804b7

.field public static final keyguard_unlockscreen_lock_20:I = 0x10804b8

.field public static final keyguard_unlockscreen_lock_21:I = 0x10804b9

.field public static final keyguard_unlockscreen_lock_22:I = 0x10804ba

.field public static final keyguard_unlockscreen_lock_23:I = 0x10804bb

.field public static final keyguard_unlockscreen_lock_24:I = 0x10804bc

.field public static final keyguard_unlockscreen_lock_25:I = 0x10804bd

.field public static final keyguard_unlockscreen_lock_26:I = 0x10804be

.field public static final keyguard_unlockscreen_lock_27:I = 0x10804bf

.field public static final keyguard_unlockscreen_lock_28:I = 0x10804c0

.field public static final kg_widget_bg_padded:I = 0x10804cb

.field public static final list_selector_background:I = 0x1080062

.field public static final list_selector_pressed_holo_dark:I = 0x1080503

.field public static final loading_tile_android:I = 0x108053b

.field public static final lock_ic_locked:I = 0x108053f

.field public static final lock_widget_page_light:I = 0x1080540

.field public static final magnified_region_frame:I = 0x1080545

.field public static final maps_google_logo:I = 0x1080546

.field public static final menu_background:I = 0x1080548

.field public static final menu_frame:I = 0x1080063

.field public static final menu_full_frame:I = 0x1080064

.field public static final menuitem_background:I = 0x1080065

.field public static final multi_window_header_ic_fix:I = 0x1080569

.field public static final multi_window_header_ic_fix_press:I = 0x108056a

.field public static final mw_border_bg:I = 0x1080575

.field public static final mw_border_bg_focus:I = 0x1080576

.field public static final mw_full_titlebar:I = 0x1080579

.field public static final mw_minimize_titlebar:I = 0x108057a

.field public static final mw_titlebar:I = 0x108057b

.field public static final mw_window_popup_bg_out:I = 0x1080588

.field public static final mw_window_popup_bg_pressed:I = 0x1080589

.field public static final no_tile_256:I = 0x108058b

.field public static final notification_bg:I = 0x108058c

.field public static final notification_bg_low:I = 0x108058d

.field public static final notification_template_icon_bg:I = 0x1080c9f

.field public static final notification_template_icon_low_bg:I = 0x1080ca0

.field public static final overscroll_edge:I = 0x10805c2

.field public static final overscroll_glow:I = 0x10805c3

.field public static final picture_emergency:I = 0x10805e9

.field public static final picture_frame:I = 0x1080066

.field public static final platlogo:I = 0x10805ec

.field public static final platlogo_alt:I = 0x10805ed

.field public static final pointer_ic_resize_ho_center:I = 0x10805f0

.field public static final pointer_ic_resize_leftbottom:I = 0x10805f1

.field public static final pointer_ic_resize_lefttop:I = 0x10805f2

.field public static final pointer_ic_resize_vcenter:I = 0x10805f3

.field public static final popup_bottom_bright:I = 0x1080600

.field public static final popup_bottom_dark:I = 0x1080601

.field public static final popup_bottom_medium:I = 0x1080602

.field public static final popup_center_bright:I = 0x1080603

.field public static final popup_center_dark:I = 0x1080604

.field public static final popup_full_bright:I = 0x1080606

.field public static final popup_full_dark:I = 0x1080607

.field public static final popup_top_bright:I = 0x108060e

.field public static final popup_top_dark:I = 0x108060f

.field public static final presence_audio_away:I = 0x10800af

.field public static final presence_audio_busy:I = 0x10800b0

.field public static final presence_audio_online:I = 0x10800b1

.field public static final presence_away:I = 0x1080067

.field public static final presence_busy:I = 0x1080068

.field public static final presence_invisible:I = 0x1080069

.field public static final presence_offline:I = 0x108006a

.field public static final presence_online:I = 0x108006b

.field public static final presence_video_away:I = 0x10800ac

.field public static final presence_video_busy:I = 0x10800ad

.field public static final presence_video_online:I = 0x10800ae

.field public static final progress_horizontal:I = 0x108006c

.field public static final progress_indeterminate_horizontal:I = 0x108006d

.field public static final quickcontact_badge_overlay_dark:I = 0x1080637

.field public static final radiobutton_off_background:I = 0x108006e

.field public static final radiobutton_on_background:I = 0x108006f

.field public static final reflectionmap:I = 0x108066a

.field public static final resize_arrow_left:I = 0x108066b

.field public static final resize_arrow_left_red:I = 0x108066c

.field public static final resize_arrow_right:I = 0x108066d

.field public static final resize_arrow_right_red:I = 0x108066e

.field public static final resizing_handler_orange:I = 0x108066f

.field public static final resizing_handler_red:I = 0x1080670

.field public static final reticle:I = 0x1080673

.field public static final screen_background_dark:I = 0x1080098

.field public static final screen_background_dark_transparent:I = 0x10800a9

.field public static final screen_background_light:I = 0x1080099

.field public static final screen_background_light_transparent:I = 0x10800aa

.field public static final scrubber_control_disabled_holo:I = 0x108067b

.field public static final scrubber_control_selector_holo:I = 0x108067f

.field public static final scrubber_progress_horizontal_holo_dark:I = 0x1080681

.field public static final search_spinner:I = 0x108068b

.field public static final select_handle_left_down:I = 0x1080695

.field public static final select_handle_left_up:I = 0x1080696

.field public static final select_handle_para_bottom:I = 0x1080697

.field public static final select_handle_para_left:I = 0x1080698

.field public static final select_handle_para_right:I = 0x1080699

.field public static final select_handle_para_top:I = 0x108069a

.field public static final select_handle_right_down:I = 0x108069b

.field public static final select_handle_right_up:I = 0x108069c

.field public static final settings_facedetection:I = 0x108069d

.field public static final settings_facedetection_dim:I = 0x108069e

.field public static final settings_facedetection_nor:I = 0x108069f

.field public static final smart_scroll_fail_01:I = 0x10806ab

.field public static final smart_scroll_fail_02:I = 0x10806b1

.field public static final smart_scroll_fail_03:I = 0x10806b4

.field public static final smart_scroll_fail_04:I = 0x10806b9

.field public static final smart_scroll_fail_05:I = 0x10806bc

.field public static final smart_scroll_fail_06:I = 0x10806bf

.field public static final smart_scroll_fail_07:I = 0x10806c2

.field public static final smart_scroll_fail_08:I = 0x10806c5

.field public static final smart_scroll_fail_09:I = 0x10806c8

.field public static final smart_scroll_init_01:I = 0x10806cb

.field public static final smart_scroll_initializing_01:I = 0x10806cc

.field public static final smart_scroll_initializing_04:I = 0x10806cf

.field public static final spinner_background:I = 0x1080070

.field public static final spinner_dropdown_background:I = 0x1080071

.field public static final star_big_off:I = 0x1080073

.field public static final star_big_on:I = 0x1080072

.field public static final star_off:I = 0x1080075

.field public static final star_on:I = 0x1080074

.field public static final stat_notify_allsharecast:I = 0x10806fb

.field public static final stat_notify_call_mute:I = 0x1080076

.field public static final stat_notify_car_mode:I = 0x10806fc

.field public static final stat_notify_chat:I = 0x1080077

.field public static final stat_notify_disabled:I = 0x10806fd

.field public static final stat_notify_disk_full:I = 0x10806fe

.field public static final stat_notify_display_mode:I = 0x10806ff

.field public static final stat_notify_error:I = 0x1080078

.field public static final stat_notify_missed_call:I = 0x108007f

.field public static final stat_notify_more:I = 0x1080079

.field public static final stat_notify_rssi_in_range:I = 0x1080702

.field public static final stat_notify_sdcard:I = 0x108007a

.field public static final stat_notify_sdcard_prepare:I = 0x10800ab

.field public static final stat_notify_sdcard_usb:I = 0x108007b

.field public static final stat_notify_sim_toolkit:I = 0x1080703

.field public static final stat_notify_sync:I = 0x108007c

.field public static final stat_notify_sync_error:I = 0x1080705

.field public static final stat_notify_sync_noanim:I = 0x108007d

.field public static final stat_notify_voicemail:I = 0x108007e

.field public static final stat_notify_wifi_in_range:I = 0x1080708

.field public static final stat_sys_adb:I = 0x1080709

.field public static final stat_sys_battery:I = 0x108070a

.field public static final stat_sys_battery_charge:I = 0x1080718

.field public static final stat_sys_battery_unknown:I = 0x1080726

.field public static final stat_sys_data_bluetooth:I = 0x1080080

.field public static final stat_sys_data_usb:I = 0x1080727

.field public static final stat_sys_download:I = 0x1080081

.field public static final stat_sys_download_done:I = 0x1080082

.field public static final stat_sys_gps_on:I = 0x1080731

.field public static final stat_sys_headset:I = 0x1080083

.field public static final stat_sys_mifi_on:I = 0x1080732

.field public static final stat_sys_mifi_on_num01:I = 0x1080733

.field public static final stat_sys_mifi_on_num02:I = 0x1080734

.field public static final stat_sys_mifi_on_num03:I = 0x1080735

.field public static final stat_sys_mifi_on_num04:I = 0x1080736

.field public static final stat_sys_mifi_on_num05:I = 0x1080737

.field public static final stat_sys_mifi_on_num06:I = 0x1080738

.field public static final stat_sys_mifi_on_num07:I = 0x1080739

.field public static final stat_sys_mifi_on_num08:I = 0x108073a

.field public static final stat_sys_mifi_on_num09:I = 0x108073b

.field public static final stat_sys_mifi_on_num10:I = 0x108073c

.field public static final stat_sys_phone_call:I = 0x1080084
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final stat_sys_phone_call_forward:I = 0x1080085
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final stat_sys_phone_call_on_hold:I = 0x1080086
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final stat_sys_secure:I = 0x1080747

.field public static final stat_sys_secure_autounlock:I = 0x1080748

.field public static final stat_sys_speakerphone:I = 0x1080087

.field public static final stat_sys_tether_bluetooth:I = 0x1080753

.field public static final stat_sys_tether_general:I = 0x1080754

.field public static final stat_sys_tether_newcohotspot:I = 0x1080755

.field public static final stat_sys_tether_tmohotspot:I = 0x1080756

.field public static final stat_sys_tether_usb:I = 0x1080757

.field public static final stat_sys_tether_wifi:I = 0x1080758

.field public static final stat_sys_tether_wifi_sprint:I = 0x1080759

.field public static final stat_sys_tether_wifi_verizon:I = 0x108075a

.field public static final stat_sys_throttled:I = 0x108075b

.field public static final stat_sys_upload:I = 0x1080088

.field public static final stat_sys_upload_done:I = 0x1080089

.field public static final stat_sys_voice:I = 0x1080762

.field public static final stat_sys_vp_phone_call:I = 0x10800a7
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final stat_sys_vp_phone_call_on_hold:I = 0x10800a8
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final stat_sys_warning:I = 0x108008a

.field public static final stat_sys_wifi_connected:I = 0x1080763

.field public static final stat_sys_wifi_disabled:I = 0x1080764

.field public static final stat_sys_wifi_dummy:I = 0x1080765

.field public static final stat_sys_wifi_enabled:I = 0x1080766

.field public static final stat_sys_wifi_no_network:I = 0x1080767

.field public static final stat_sys_wifi_not_connected:I = 0x1080768

.field public static final stat_sys_wifi_p2p_connected:I = 0x1080769

.field public static final stat_sys_wifi_p2p_on:I = 0x108076a

.field public static final status_bar_background:I = 0x108076b

.field public static final status_bar_item_app_background:I = 0x108008b

.field public static final status_bar_item_background:I = 0x108008c

.field public static final sym_action_call:I = 0x108008d

.field public static final sym_action_chat:I = 0x108008e

.field public static final sym_action_email:I = 0x108008f

.field public static final sym_app_on_sd_unavailable_icon:I = 0x1080787

.field public static final sym_call_incoming:I = 0x1080090

.field public static final sym_call_missed:I = 0x1080091

.field public static final sym_call_outgoing:I = 0x1080092

.field public static final sym_contact_card:I = 0x1080094

.field public static final sym_def_app_icon:I = 0x1080093

.field public static final sym_keyboard_return_holo:I = 0x10807a1

.field public static final sym_keyboard_shift:I = 0x10807a4

.field public static final sym_keyboard_shift_locked:I = 0x10807a5

.field public static final tab_bottom_left:I = 0x10807a8

.field public static final tab_bottom_left_v4:I = 0x10807a9

.field public static final tab_bottom_right:I = 0x10807aa

.field public static final tab_bottom_right_v4:I = 0x10807ab

.field public static final tab_indicator_v4:I = 0x10807b2

.field public static final text_cursor_holo_dark:I = 0x10800b4

.field public static final text_cursor_holo_light:I = 0x10800b5

.field public static final text_edit_paste_window:I = 0x10807c5

.field public static final text_edit_side_paste_window:I = 0x10807c6

.field public static final text_select_handle_left:I = 0x10807c8

.field public static final text_select_handle_middle:I = 0x10807c9

.field public static final text_select_handle_middle_reverse:I = 0x10807ca

.field public static final text_select_handle_right:I = 0x10807cb

.field public static final title_bar:I = 0x1080095

.field public static final title_bar_medium:I = 0x10807ff

.field public static final title_bar_tall:I = 0x10800a6

.field public static final toast_frame:I = 0x1080096

.field public static final toolkit_bg_1:I = 0x1080803

.field public static final toolkit_bg_2:I = 0x1080804

.field public static final toolkit_bg_3:I = 0x1080805

.field public static final toolkit_bg_4:I = 0x1080806

.field public static final toolkit_bg_5:I = 0x1080807

.field public static final toolkit_tab1:I = 0x1080808

.field public static final toolkit_tab1_active:I = 0x1080809

.field public static final toolkit_tab2:I = 0x108080b

.field public static final toolkit_tab2_active:I = 0x108080c

.field public static final toolkit_tab3:I = 0x108080e

.field public static final toolkit_tab3_active:I = 0x108080f

.field public static final toolkit_tab4:I = 0x1080811

.field public static final toolkit_tab4_active:I = 0x1080812

.field public static final toolkit_tab5:I = 0x1080814

.field public static final toolkit_tab5_active:I = 0x1080815

.field public static final tw_action_bar_icon_find_holo_light:I = 0x1080824

.field public static final tw_action_bar_icon_share_holo_light:I = 0x1080825

.field public static final tw_action_bar_icon_websearch_holo_light:I = 0x1080828

.field public static final tw_action_popup_clipboard_holo_dark:I = 0x1080838

.field public static final tw_action_popup_clipboard_holo_light:I = 0x1080839

.field public static final tw_action_popup_copy_holo_dark:I = 0x108083a

.field public static final tw_action_popup_copy_holo_light:I = 0x108083b

.field public static final tw_action_popup_cut_holo_dark:I = 0x108083c

.field public static final tw_action_popup_cut_holo_light:I = 0x108083d

.field public static final tw_action_popup_find_holo_dark:I = 0x1080869

.field public static final tw_action_popup_find_holo_light:I = 0x108086a

.field public static final tw_action_popup_frame_holo_dark:I = 0x108086b

.field public static final tw_action_popup_frame_holo_light:I = 0x108086c

.field public static final tw_action_popup_paste_holo_dark:I = 0x108086f

.field public static final tw_action_popup_paste_holo_light:I = 0x1080870

.field public static final tw_action_popup_selectall_holo_dark:I = 0x1080871

.field public static final tw_action_popup_selectall_holo_light:I = 0x1080872

.field public static final tw_action_popup_share_holo_dark:I = 0x1080873

.field public static final tw_action_popup_share_holo_light:I = 0x1080874

.field public static final tw_action_popup_toast_frame_holo_dark:I = 0x10800b8

.field public static final tw_action_popup_translate_holo_dark:I = 0x1080875

.field public static final tw_action_popup_translate_holo_light:I = 0x1080876

.field public static final tw_action_popup_websearch_holo_dark:I = 0x1080877

.field public static final tw_action_popup_websearch_holo_light:I = 0x1080878

.field public static final tw_btn_check_holo_dark:I = 0x1080692

.field public static final tw_btn_check_holo_light:I = 0x1080693

.field public static final tw_btn_checkcontainer_holo_dark:I = 0x108079a

.field public static final tw_btn_checkcontainer_holo_light:I = 0x108079b

.field public static final tw_btn_checkmark_holo_dark:I = 0x10807a2

.field public static final tw_btn_checkmark_holo_light:I = 0x10807a3

.field public static final tw_btn_default_holo_dark:I = 0x10806ae

.field public static final tw_btn_default_holo_light:I = 0x10806af

.field public static final tw_btn_radio_holo_dark:I = 0x10806b5

.field public static final tw_btn_radio_holo_light:I = 0x10806b6

.field public static final tw_clipboard_button_background:I = 0x10808e3

.field public static final tw_clipboard_button_pressed:I = 0x10808e6

.field public static final tw_clipboard_button_selector:I = 0x10808e7

.field public static final tw_clipboard_noitem:I = 0x10808fc

.field public static final tw_device_options_divider:I = 0x1080905

.field public static final tw_device_options_mute:I = 0x1080906

.field public static final tw_device_options_off:I = 0x1080907

.field public static final tw_device_options_on:I = 0x1080908

.field public static final tw_device_options_sound:I = 0x1080909

.field public static final tw_device_options_vibrate:I = 0x108090a

.field public static final tw_dialog_bottom_holo_dark_1:I = 0x108090d

.field public static final tw_edit_text_holo_dark:I = 0x10806dc

.field public static final tw_edit_text_holo_light:I = 0x10806dd

.field public static final tw_ic_lock_airplane_mode:I = 0x108097f

.field public static final tw_ic_lock_airplane_mode_off:I = 0x1080980

.field public static final tw_ic_lock_data_disable:I = 0x1080981

.field public static final tw_ic_lock_data_enable:I = 0x1080982

.field public static final tw_ic_lock_power_off:I = 0x1080983

.field public static final tw_ic_lock_restart:I = 0x1080984

.field public static final tw_ic_menu_clipboard_holo_dark:I = 0x10800b7

.field public static final tw_ic_menu_paste_holo_dark:I = 0x10800b6

.field public static final tw_ic_settings_accessibility:I = 0x10809a9

.field public static final tw_offload_dialog_top_holo_dark:I = 0x1080a1d

.field public static final tw_popup_button_background_holo_dark:I = 0x1080a52

.field public static final tw_progress_horizontal_holo_red:I = 0x1080b0d

.field public static final tw_progress_primary_holo_red:I = 0x1080b16

.field public static final tw_scrubber_control_disabled_holo_red:I = 0x1080b5b

.field public static final tw_scrubber_control_focused_holo_red:I = 0x1080b5f

.field public static final tw_scrubber_control_holo_red:I = 0x1080b63

.field public static final tw_scrubber_control_pressed_holo_red:I = 0x1080b67

.field public static final tw_scrubber_control_selector_holo_init:I = 0x1080b6e

.field public static final tw_scrubber_control_selector_holo_red:I = 0x1080b70

.field public static final tw_switch_activation:I = 0x1080bb9

.field public static final tw_switch_activation_dim:I = 0x1080bba

.field public static final tw_switch_activation_holo_dark:I = 0x1080bbc

.field public static final tw_switch_activation_holo_dark_dim:I = 0x1080bbd

.field public static final tw_switch_activation_holo_dark_pressed:I = 0x1080bbe

.field public static final tw_switch_activation_holo_light:I = 0x1080bbf

.field public static final tw_switch_activation_holo_light_dim:I = 0x1080bc0

.field public static final tw_switch_activation_holo_light_pressed:I = 0x1080bc1

.field public static final tw_switch_activation_press:I = 0x1080bc2

.field public static final tw_switch_disabled:I = 0x1080bcd

.field public static final tw_switch_disabled_dim:I = 0x1080bce

.field public static final tw_switch_disabled_holo_dark:I = 0x1080bcf

.field public static final tw_switch_disabled_holo_dark_dim:I = 0x1080bd0

.field public static final tw_switch_disabled_holo_dark_pressed:I = 0x1080bd1

.field public static final tw_switch_disabled_holo_light:I = 0x1080bd2

.field public static final tw_switch_disabled_holo_light_dim:I = 0x1080bd3

.field public static final tw_switch_disabled_holo_light_pressed:I = 0x1080bd4

.field public static final tw_switch_disabled_press:I = 0x1080bd5

.field public static final tw_switch_off_holo_dark:I = 0x1080bd8

.field public static final tw_switch_off_holo_dark_dim:I = 0x1080bd9

.field public static final tw_switch_off_holo_dark_dim_kor:I = 0x1080bda

.field public static final tw_switch_off_holo_dark_kor:I = 0x1080bdb

.field public static final tw_switch_off_holo_dark_pressed:I = 0x1080bdc

.field public static final tw_switch_off_holo_dark_pressed_kor:I = 0x1080bdd

.field public static final tw_switch_off_holo_light:I = 0x1080bde

.field public static final tw_switch_off_holo_light_dim:I = 0x1080bdf

.field public static final tw_switch_off_holo_light_dim_kor:I = 0x1080be0

.field public static final tw_switch_off_holo_light_kor:I = 0x1080be1

.field public static final tw_switch_off_holo_light_pressed:I = 0x1080be2

.field public static final tw_switch_off_holo_light_pressed_kor:I = 0x1080be3

.field public static final tw_switch_on_holo_dark:I = 0x1080be4

.field public static final tw_switch_on_holo_dark_dim:I = 0x1080be5

.field public static final tw_switch_on_holo_dark_dim_kor:I = 0x1080be6

.field public static final tw_switch_on_holo_dark_kor:I = 0x1080be7

.field public static final tw_switch_on_holo_dark_pressed:I = 0x1080be8

.field public static final tw_switch_on_holo_dark_pressed_kor:I = 0x1080be9

.field public static final tw_switch_on_holo_light:I = 0x1080bea

.field public static final tw_switch_on_holo_light_dim:I = 0x1080beb

.field public static final tw_switch_on_holo_light_dim_kor:I = 0x1080bec

.field public static final tw_switch_on_holo_light_kor:I = 0x1080bed

.field public static final tw_switch_on_holo_light_pressed:I = 0x1080bee

.field public static final tw_switch_on_holo_light_pressed_kor:I = 0x1080bef

.field public static final tw_text_edit_paste_window:I = 0x108083f

.field public static final tw_text_select_handle_left:I = 0x1080840

.field public static final tw_text_select_handle_left_top:I = 0x1080841

.field public static final tw_text_select_handle_middle:I = 0x1080842

.field public static final tw_text_select_handle_right:I = 0x1080843

.field public static final tw_text_select_handle_right_top:I = 0x1080844

.field public static final tw_textfield_activated_holo_dark:I = 0x1080845

.field public static final tw_textfield_activated_holo_light:I = 0x1080846

.field public static final tw_textfield_default_holo_dark:I = 0x1080847

.field public static final tw_textfield_default_holo_light:I = 0x1080848

.field public static final tw_textfield_disabled_focused_holo_dark:I = 0x1080849

.field public static final tw_textfield_disabled_focused_holo_light:I = 0x108084a

.field public static final tw_textfield_disabled_holo_dark:I = 0x108084b

.field public static final tw_textfield_disabled_holo_light:I = 0x108084c

.field public static final tw_textfield_focused_holo_dark:I = 0x108084d

.field public static final tw_textfield_focused_holo_light:I = 0x108084e

.field public static final tw_textfield_multiline_activated_holo_dark:I = 0x108084f

.field public static final tw_textfield_multiline_activated_holo_light:I = 0x1080850

.field public static final tw_textfield_multiline_default_holo_dark:I = 0x1080851

.field public static final tw_textfield_multiline_default_holo_light:I = 0x1080852

.field public static final tw_textfield_multiline_disabled_focused_holo_dark:I = 0x1080853

.field public static final tw_textfield_multiline_disabled_focused_holo_light:I = 0x1080854

.field public static final tw_textfield_multiline_disabled_holo_dark:I = 0x1080855

.field public static final tw_textfield_multiline_disabled_holo_light:I = 0x1080856

.field public static final tw_textfield_multiline_focused_holo_dark:I = 0x1080857

.field public static final tw_textfield_multiline_focused_holo_light:I = 0x1080858

.field public static final tw_textfield_multiline_pressed_holo_dark:I = 0x1080859

.field public static final tw_textfield_multiline_pressed_holo_light:I = 0x108085a

.field public static final tw_textfield_pressed_holo_dark:I = 0x108085b

.field public static final tw_textfield_pressed_holo_light:I = 0x108085c

.field public static final tw_textfield_search_default_holo_dark:I = 0x108085d

.field public static final tw_textfield_search_default_holo_light:I = 0x108085e

.field public static final tw_textfield_search_right_default_holo_dark:I = 0x108085f

.field public static final tw_textfield_search_right_default_holo_light:I = 0x1080860

.field public static final tw_textfield_search_right_selected_holo_dark:I = 0x1080861

.field public static final tw_textfield_search_right_selected_holo_light:I = 0x1080862

.field public static final tw_textfield_search_selected_holo_dark:I = 0x1080863

.field public static final tw_textfield_search_selected_holo_light:I = 0x1080864

.field public static final tw_textfield_searchview_holo_dark:I = 0x1080865

.field public static final tw_textfield_searchview_holo_light:I = 0x1080866

.field public static final tw_textfield_searchview_right_holo_dark:I = 0x1080867

.field public static final tw_textfield_searchview_right_holo_light:I = 0x1080868

.field public static final unknown_image:I = 0x1080c64

.field public static final unlock_default:I = 0x1080c65

.field public static final unlock_halo:I = 0x1080c66

.field public static final unlock_ring:I = 0x1080c67

.field public static final unlock_wave:I = 0x1080c68

.field public static final usb_android:I = 0x1080c69

.field public static final usb_android_connected:I = 0x1080c6a

.field public static final vpn_connected:I = 0x1080c6c

.field public static final vpn_disconnected:I = 0x1080c6d

.field public static final zoom_plate:I = 0x1080097

.field public static final zzz_city_zone:I = 0x1080c6e

.field public static final zzz_home_zone:I = 0x1080c6f

.field public static final zzz_stat_sys_no_sim:I = 0x1080c71


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12260
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
