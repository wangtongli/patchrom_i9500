.class public final Lcom/android/internal/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final accessibility_touch_slop:I = 0x105006c

.field public static final action_bar_stacked_max_height:I = 0x1050053

.field public static final action_bar_stacked_tab_max_width:I = 0x1050054

.field public static final action_button_min_width:I = 0x1050052

.field public static final app_icon_size:I = 0x1050000

.field public static final config_minScalingSpan:I = 0x1050009

.field public static final config_minScalingTouchMajor:I = 0x105000a

.field public static final config_prefDialogWidth:I = 0x1050007

.field public static final config_viewConfigurationTouchSlop:I = 0x1050008

.field public static final default_app_widget_padding_bottom:I = 0x1050051

.field public static final default_app_widget_padding_left:I = 0x105004e

.field public static final default_app_widget_padding_right:I = 0x1050050

.field public static final default_app_widget_padding_top:I = 0x105004f

.field public static final default_gap:I = 0x1050048

.field public static final dialog_min_width_major:I = 0x1050003

.field public static final dialog_min_width_minor:I = 0x1050004

.field public static final dropdownitem_icon_width:I = 0x105004b

.field public static final dropdownitem_text_padding_left:I = 0x1050049

.field public static final endEffect_text_size:I = 0x10500ea

.field public static final fastscroll_overlay_size:I = 0x1050015

.field public static final fastscroll_thumb_height:I = 0x1050017

.field public static final fastscroll_thumb_width:I = 0x1050016

.field public static final fling_gesture_max_x_velocity:I = 0x1050087

.field public static final fling_gesture_min_y_velocity:I = 0x1050088

.field public static final keyguard_avatar_frame_shadow_radius:I = 0x1050072

.field public static final keyguard_avatar_frame_stroke_width:I = 0x1050071

.field public static final keyguard_avatar_size:I = 0x1050073

.field public static final keyguard_lockscreen_application_shortcut_badge_fontsize_default:I = 0x105008c

.field public static final keyguard_lockscreen_application_shortcut_badge_fontsize_small:I = 0x105008d

.field public static final keyguard_lockscreen_application_shortcut_badge_icon_centerXOffset:I = 0x105008e

.field public static final keyguard_lockscreen_application_shortcut_badge_icon_topToTopOffset:I = 0x105008f

.field public static final keyguard_lockscreen_application_shortcut_icon_height:I = 0x105008b

.field public static final keyguard_lockscreen_application_shortcut_icon_width:I = 0x105008a

.field public static final keyguard_lockscreen_application_shortcut_padding_width:I = 0x1050089

.field public static final keyguard_lockscreen_first_border:I = 0x1050084

.field public static final keyguard_lockscreen_second_border:I = 0x1050085

.field public static final keyguard_navigation_padding:I = 0x1050091

.field public static final kg_edge_swipe_region_size:I = 0x1050075

.field public static final kg_small_widget_height:I = 0x1050077

.field public static final kg_squashed_layout_threshold:I = 0x1050076

.field public static final kg_widget_pager_bottom_padding:I = 0x105006a

.field public static final kg_widget_pager_horizontal_padding:I = 0x1050068

.field public static final kg_widget_pager_top_padding:I = 0x1050069

.field public static final multiwindow_center_bar_docking_margin:I = 0x10500ac

.field public static final multiwindow_center_bar_fling_size:I = 0x10500ab

.field public static final multiwindow_def_height:I = 0x10500a6

.field public static final multiwindow_def_width:I = 0x10500a5

.field public static final multiwindow_default_center_bar_inner_padding:I = 0x10500aa

.field public static final multiwindow_default_center_bar_width:I = 0x10500a9

.field public static final multiwindow_gap:I = 0x10500a4

.field public static final multiwindow_minimized_height:I = 0x10500a8

.field public static final multiwindow_minimized_width:I = 0x10500a7

.field public static final navigation_bar_height:I = 0x105000d

.field public static final navigation_bar_height_landscape:I = 0x105000e

.field public static final navigation_bar_width:I = 0x105000f

.field public static final notification_large_icon_height:I = 0x1050006

.field public static final notification_large_icon_width:I = 0x1050005

.field public static final notification_subtext_size:I = 0x1050058

.field public static final notification_text_size:I = 0x1050056

.field public static final notification_title_text_size:I = 0x1050057

.field public static final password_keyboard_spacebar_vertical_correction:I = 0x105001b

.field public static final preference_fragment_padding_bottom:I = 0x1050028

.field public static final preference_fragment_padding_side:I = 0x1050029

.field public static final search_view_preferred_width:I = 0x1050037

.field public static final status_bar_height:I = 0x105000c

.field public static final status_bar_icon_size:I = 0x1050010

.field public static final system_bar_icon_size:I = 0x1050013

.field public static final textview_error_popup_default_width:I = 0x105004c

.field public static final thumbnail_height:I = 0x1050001

.field public static final thumbnail_width:I = 0x1050002

.field public static final toast_y_offset:I = 0x105000b

.field public static final tw_action_bar_stacked_max_height:I = 0x10500cd

.field public static final tw_action_bar_up_margin:I = 0x10500cc

.field public static final tw_clipboard_animation_delay:I = 0x10500f1

.field public static final tw_clipboard_animation_time:I = 0x10500f0

.field public static final tw_clipboard_appear_delay:I = 0x10500ef

.field public static final tw_clipboard_appear_time:I = 0x10500ee

.field public static final tw_clipboard_dialog_height:I = 0x10500fb

.field public static final tw_clipboard_dialog_height_land:I = 0x1050109

.field public static final tw_clipboard_dialog_scroll_height:I = 0x10500fc

.field public static final tw_clipboard_dialog_scroll_height_land:I = 0x105010a

.field public static final tw_clipboard_html_image_height:I = 0x10500df

.field public static final tw_clipboard_html_image_height_land:I = 0x10500e3

.field public static final tw_clipboard_html_image_max:I = 0x10500f2

.field public static final tw_clipboard_html_image_width:I = 0x10500de

.field public static final tw_clipboard_html_image_width_land:I = 0x10500e2

.field public static final tw_clipboard_item_count_in_screen:I = 0x10500fd

.field public static final tw_clipboard_item_count_in_screen_land:I = 0x105010b

.field public static final tw_clipboard_item_gap:I = 0x10500f9

.field public static final tw_clipboard_item_gap_land:I = 0x1050107

.field public static final tw_clipboard_item_height:I = 0x10500f7

.field public static final tw_clipboard_item_height_land:I = 0x1050105

.field public static final tw_clipboard_item_layout_height:I = 0x10500f5

.field public static final tw_clipboard_item_layout_height_land:I = 0x1050103

.field public static final tw_clipboard_item_layout_width:I = 0x10500f4

.field public static final tw_clipboard_item_layout_width_land:I = 0x1050102

.field public static final tw_clipboard_item_left_padding:I = 0x10500f3

.field public static final tw_clipboard_item_top_gap:I = 0x10500f8

.field public static final tw_clipboard_item_top_gap_land:I = 0x1050106

.field public static final tw_clipboard_item_width:I = 0x10500f6

.field public static final tw_clipboard_item_width_land:I = 0x1050104

.field public static final tw_clipboard_land_height:I = 0x10500d9

.field public static final tw_clipboard_last_item_padding:I = 0x1050101

.field public static final tw_clipboard_last_item_padding_land:I = 0x105010f

.field public static final tw_clipboard_outside_count:I = 0x10500fe

.field public static final tw_clipboard_outside_count_land:I = 0x105010c

.field public static final tw_clipboard_panel_land_height:I = 0x10500db

.field public static final tw_clipboard_panel_port_height:I = 0x10500da

.field public static final tw_clipboard_port_height:I = 0x10500d8

.field public static final tw_clipboard_screen_width:I = 0x10500ff

.field public static final tw_clipboard_screen_width_land:I = 0x105010d

.field public static final tw_clipboard_text_max_line:I = 0x1050100

.field public static final tw_clipboard_text_max_line_land:I = 0x105010e

.field public static final tw_clipitem_height:I = 0x10500dd

.field public static final tw_clipitem_height_land:I = 0x10500e1

.field public static final tw_clipitem_width:I = 0x10500dc

.field public static final tw_clipitem_width_land:I = 0x10500e0

.field public static final tw_flashboard_panel_height:I = 0x10500ad

.field public static final tw_option_menu_height:I = 0x10500cf

.field public static final tw_option_menu_maxheight:I = 0x10500d0

.field public static final tw_smartswitcher_thumbnail_height:I = 0x10500af

.field public static final tw_smartswitcher_thumbnail_width:I = 0x10500ae

.field public static final volume_panel_top:I = 0x105004d

.field public static final webmagnifier_height:I = 0x10500bc

.field public static final webmagnifier_tail_height:I = 0x10500be

.field public static final webmagnifier_tail_width:I = 0x10500bd

.field public static final webmagnifier_width:I = 0x10500bb

.field public static final webselect_land_content_height:I = 0x10500c0

.field public static final webselect_popup_land_content_height:I = 0x10500c1

.field public static final webselect_port_content_height:I = 0x10500bf


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11962
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
